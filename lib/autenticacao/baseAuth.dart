import 'dart:async';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';

/// Modificado por: Diogo Roosarnezi
/// Data Modificado: 14/08
/// ModificaÃ§Ã£o: RemoÃ§Ã£o da classe abstrata
/// Motivo: Com a evoluÃ§Ã£o do programa notou-se a falta de necessidade desta classe.

enum TipoLogin { google, comum }

class Auth {
  static final FirebaseAuth firebaseAuth = FirebaseAuth.instance;
  static final GoogleSignIn googleSignIn = new GoogleSignIn();

  //static final FacebookLogin facebookLogin = new FacebookLogin();

  static Future<FirebaseUser> createUser(String email, String password) async {
    FirebaseUser user = await firebaseAuth.createUserWithEmailAndPassword(
        email: email, password: password);
    return user;
  }

  static Future<FirebaseUser> currentUser() async {
    FirebaseUser user = await firebaseAuth.currentUser();
    return user != null ? user : null;
  }

  static Future<void> signOut() async {
    return firebaseAuth.signOut();
  }

  static Future<FirebaseUser> handleSign(
      TipoLogin tipo, String email, String password) async {
    FirebaseUser user;
/*
    GoogleSignInAccount googleUser = await Auth.googleSignIn.signIn();
    GoogleSignInAuthentication googleAuth = await googleUser.authentication;
    //email = googleUser.email;
    //email = googleAuth.idToken;
    //password = googleAuth.accessToken;

    final AuthCredential credential = GoogleAuthProvider.getCredential(
      accessToken: googleAuth.accessToken,
      idToken: googleAuth.idToken,
    );*/

    switch (tipo) {
      case TipoLogin.google:

        //Descarta google
        FirebaseUser docId = await firebaseAuth.signInWithEmailAndPassword(
            email: email, password: password);
        user = docId;

        //user = await firebaseAuth.signInWithCredential(credential);

        //user = await firebaseAuth.signInWithGoogle(accessToken: password, idToken: email);
        break;
      /*case TipoLogin.facebook:
        user = await firebaseAuth.signInWithFacebook(accessToken: email);
        break;*/
      case TipoLogin.comum:
        FirebaseUser docId = await firebaseAuth.signInWithEmailAndPassword(
            email: email, password: password);
        user = docId;
        break;
    }
    return user;
  }
}
