import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:redesipapp/classes.dart';
import 'package:redesipapp/style/primary_button.dart';

class AddProdutosScreen extends StatefulWidget {
  /// New elements will appear at the start
  AddProdutosScreen({
    this.ideUsu,
  });

  final String ideUsu;

  @override
  State<StatefulWidget> createState() => new AddProdutosState();
}

class AddProdutosState extends State<AddProdutosScreen> {
  static final GlobalKey<FormState> formKey = new GlobalKey<FormState>();
  static final GlobalKey<FormFieldState<String>> txt001 =
      new GlobalKey<FormFieldState<String>>();
  static final GlobalKey<FormFieldState<String>> txt002 =
      new GlobalKey<FormFieldState<String>>();
  static final GlobalKey<FormFieldState<String>> txt003 =
      new GlobalKey<FormFieldState<String>>();
  static final GlobalKey<FormFieldState<String>> txt004 =
      new GlobalKey<FormFieldState<String>>();
  static final GlobalKey<FormFieldState<String>> txt005 =
      new GlobalKey<FormFieldState<String>>();
  static final GlobalKey<FormFieldState<String>> txt006 =
      new GlobalKey<FormFieldState<String>>();
  static final GlobalKey<FormFieldState<String>> txt007 =
      new GlobalKey<FormFieldState<String>>();
  static final GlobalKey<FormFieldState<String>> txt008 =
      new GlobalKey<FormFieldState<String>>();
  static final GlobalKey<FormFieldState<String>> txt009 =
      new GlobalKey<FormFieldState<String>>();
  static final GlobalKey<FormFieldState<String>> txt010 =
      new GlobalKey<FormFieldState<String>>();
  static final GlobalKey<FormFieldState<String>> txt01 =
      new GlobalKey<FormFieldState<String>>();

  //AddProdutosState({Key key}) : super(key: key);

  List<Produtos> l = new List<Produtos>();

  void sync() async {
    String sigUfs = "";
    print('lista');
    l.forEach((data) async {
      // this is an anonymous function : a function without name
      print('lista foreach');
      await Firestore.instance.collection("produtos").add({
        "codigo": data.codPro,
        "nome": data.nomIte,
        "preco": data.preco,
        "decimais": data.temDec,
        "deletado": data.deletado
      });
      //});
    });
  }

  Future<Null> setCidades() async {
    /*l.add(new Produtos('', 1166, 'PAO FRANCES KG', 12.4, 'S', false, 0));
    l.add(new Produtos('', 7896183202187,
        'LEITE LONGA VIDA QUATA INTEGRAL 1 LT', 2.65, 'S', false, 0));
    l.add(new Produtos('', 7896641000317, 'ARROZ DONO DA CASA TIPO 1 5KG',
        12.58, 'S', false, 0));
    l.add(new Produtos('', 1013, 'LOMBO GROSSO KG', 14.99, 'S', false, 0));
    l.add(new Produtos('', 1020, 'CONTRA FILE KG', 26.9, 'S', false, 0));
    l.add(new Produtos(
        '', 1122, 'FILE DE FRANGO AVIVAR KG', 12.98, 'S', false, 0));
    l.add(new Produtos(
        '', 7896702100024, 'CAFE DO PRODUTOR 500G', 13.9, 'S', false, 0));
    l.add(new Produtos('', 1025, 'COXAO MOLE KG', 26.5, 'S', false, 0));
    l.add(new Produtos('', 1016, 'MUSCULO DIANTEIRO KG', 15.98, 'S', false, 0));
    l.add(new Produtos(
        '', 7898935964039, 'ACUCAR CRISTAL DELTA 5 KG', 8.11, 'S', false, 0));
    l.add(new Produtos('', 1033, 'PERNIL DESOSSADO KG', 11.69, 'S', false, 0));
    l.add(new Produtos('', 1058, 'PRESUNTO SADIA KG', 27.5, 'S', false, 0));
    l.add(new Produtos(
        '', 1042, 'COXA E SOBRECOXA DE FRANGO KG', 6.39, 'S', false, 0));
    l.add(new Produtos('', 1024, 'COXAO DURO KG', 21.9, 'S', false, 0));
    l.add(new Produtos('', 33, 'ACEM KG', 14.99, 'S', false, 0));
    l.add(new Produtos(
        '', 7896036090244, 'OLEO SOJA Concordia  900ML', 3.75, 'S', false, 0));
    l.add(new Produtos('', 1031, 'PATINHO KG', 22.9, 'S', false, 0));
    l.add(new Produtos(
        '', 7898104391260, 'OVOS SAO JOAO VERMELHO DZ', 6.4, 'S', false, 0));
    l.add(new Produtos('', 7894900531008, 'AGUA MINERAL CRYSTAL C/GAS 500ML',
        1.59, 'S', false, 0));
    l.add(new Produtos('', 7896051111016,
        'LEITE ITAMBE LONGA VIDA INTEGRAL 1LT', 3.17, 'S', false, 0));
    l.add(new Produtos('', 1022, 'ALCATRA KG', 26.9, 'S', false, 0));
    l.add(new Produtos(
        '', 7898104391277, 'OVOS SAO JOAO BRANCO DZ', 6.15, 'S', false, 0));
    l.add(new Produtos('', 1026, 'PICANHA KG', 26.9, 'S', false, 0));
    l.add(new Produtos(
        '', 7896590801232, 'LEITE CEMIL INTEGRAL  1 LT', 3.95, 'S', false, 0));
    l.add(new Produtos('', 1053, 'LINGUICA PURA KG', 18.5, 'S', false, 0));
    l.add(new Produtos('', 1014, 'PALETA DE 2 KG', 20, 'S', false, 0));
    l.add(new Produtos('', 7898955715048,
        'FARINHA DE MANDIOCA BIJU FARIMINAS 500G', 3.89, 'S', false, 0));
    l.add(new Produtos(
        '', 40232809934, 'CAFE TRUVILLE 500G', 9.19, 'S', false, 0));
    l.add(new Produtos('', 7896550750020, 'ARROZ VASCONCELOS AGULHINHA 5KG',
        15.98, 'S', false, 0));
    l.add(new Produtos('', 8, 'MASSA P/QUIBE KG', 14.9, 'S', false, 0));
    l.add(new Produtos('', 7896641000027, 'ARROZ DO PADRE TIPO 1 POL 2KG', 7.1,
        'S', false, 0));
    l.add(new Produtos('', 1011, 'PEITO DE VACA KG', 14.68, 'S', false, 0));
    l.add(new Produtos('', 1018, 'COSTELA KG', 12.99, 'S', false, 0));
    l.add(new Produtos(
        '', 7896641000010, 'ARROZ DO PADRE 5KG T 1', 13.95, 'S', false, 0));
    l.add(new Produtos('', 1015, 'PALETA DE 1  KG', 21.9, 'S', false, 0));
    l.add(
        new Produtos('', 1034, 'COSTELINHA DE PORCO KG', 15.29, 'S', false, 0));
    l.add(new Produtos(
        '', 7898935964022, 'ACUCAR CRISTAL DELTA 2KG', 3.81, 'S', false, 0));
    l.add(new Produtos('', 7893000482401,
        'FILE PEITO CONGELADO SADIA 1 KG BANDJ', 12.55, 'S', false, 0));
    l.add(new Produtos(
        '', 7891080000119, 'FARINHA TRIGO SOL 1KG', 3.4, 'S', false, 0));
    l.add(new Produtos('', 1062, 'APRESUNTADO SADIA KG', 22, 'S', false, 0));
    l.add(new Produtos(
        '', 7896702100017, 'CAFE DO PRODUTOR 250G', 7, 'S', false, 0));
    l.add(new Produtos('', 7897315500096, 'FEIJAO LIGEIRINHO CARIOC TP1 1KG',
        3.17, 'S', false, 0));
    l.add(new Produtos('', 1010, 'CUPIM KG', 19.9, 'S', false, 0));
    l.add(new Produtos('', 7894900530001, 'AGUA MINERAL CRYSTAL S/GAS 500ML',
        1.2, 'S', false, 0));
    l.add(new Produtos('', 7894900530032, 'AGUA MINERAL CRYSTAL S/ GAS 1,5L',
        2.55, 'S', false, 0));
    l.add(new Produtos(
        '', 7897256729587, 'LEITE TIPO C CALU 1L', 4, 'S', false, 0));
    l.add(new Produtos('', 7896183202194,
        'LEITE LONGA VIDA QUATA DESNATADO 1 LT', 2.73, 'S', false, 0));
    l.add(new Produtos(
        '', 1195, 'QUEIJO MINAS PADRAO SCALA KG', 38.5, 'S', false, 0));
    l.add(new Produtos('', 1054, 'LINGUICA DE FRANGO KG', 18.5, 'S', false, 0));
    l.add(
        new Produtos('', 2231, 'AGUA MINERAL SERRANA 20LT', 10, 'S', false, 0));
    l.add(
        new Produtos('', 94, 'QUEIJO FRESCAL RINCAO KG', 26.5, 'S', false, 0));
    l.add(
        new Produtos('', 1050, 'LINGUICA CALAB PERD KG', 21.65, 'S', false, 0));
    l.add(new Produtos('', 7896550720047, 'FEIJAO VASCONCELOS PRETO 1 KG', 5.6,
        'S', false, 0));
    l.add(new Produtos('', 7896098900253, 'DETERGENTE LIQUIDO YPE CLEAR 500ML',
        2.05, 'S', false, 0));
    l.add(new Produtos('', 1035, 'LOMBO PORCO KG', 13.3, 'S', false, 0));
    l.add(new Produtos(
        '', 7897315500072, 'FEIJAO JOAO PEDRO PRETO 1KG', 5.16, 'S', false, 0));
    l.add(new Produtos('', 1076, 'SALSICHA AVIVAR KG', 6.19, 'S', false, 0));
    l.add(new Produtos('', 7896054905223, 'EXTRATO TOMATE DAJUDA LT 850G', 8.29,
        'S', false, 0));
    l.add(new Produtos('', 7891000065440, 'LEITE CONDENSADO MOCA TP 395G ', 6.3,
        'S', false, 0));
    l.add(new Produtos(
        '', 7893000394209, 'MARGARINA QUALY 500G C/SAL', 7.15, 'S', false, 0));
    l.add(new Produtos(
        '', 2229, 'AGUA MINERAL TAQUARAL  20LT ', 12, 'S', false, 0));
    l.add(
        new Produtos('', 100, 'LINGUICA CALAB SADIA KG', 20.2, 'S', false, 0));
    l.add(new Produtos('', 7896021314744,
        'MACARRAO COM OVOS PARAFUSO DON SAPORE 500G', 2.29, 'S', false, 0));
    l.add(new Produtos(
        '', 7897013810046, 'MANTEIGA RADIO C/SAL 500G', 21, 'S', false, 0));
    l.add(new Produtos(
        '', 7896036094983, 'EXTRATO ELEFANTE LT 340GR', 4.68, 'S', false, 0));
    l.add(new Produtos('', 1046, 'BACON EXTRA ADEEL', 15.5, 'S', false, 0));
    l.add(new Produtos('', 1030, 'FIGADO DE VACA KG', 13.5, 'S', false, 0));
    l.add(new Produtos('', 1021, 'FILE MIGNON KG', 29, 'S', false, 0));
    l.add(new Produtos('', 53, 'ALMONDEGA KG', 14.9, 'S', false, 0));
    l.add(new Produtos(
        '', 7896183210106, 'LEITE CONDENSADO GLORIA 395', 4.11, 'S', false, 0));
    l.add(new Produtos('', 7, 'PAO FRANCES INTEGRAL KG', 13.9, 'S', false, 0));
    l.add(new Produtos('', 99, 'BACON SADIA KG', 18.15, 'S', false, 0));
    l.add(new Produtos(
        '', 1057, 'LINGUICA TOSCANA SADIA KG', 17.85, 'S', false, 0));
    l.add(new Produtos('', 7896098900208, 'DETERGENTE LIQUIDO YPE NEUTRO 500ML',
        2.05, 'S', false, 0));
    l.add(
        new Produtos('', 7896523100166, 'OREGANO FA 6G', 8.71, 'S', false, 0));
    l.add(new Produtos(
        '', 7896183201159, 'CREME LEITE QUATA 200G', 2.45, 'S', false, 0));
    l.add(new Produtos(
        '', 7894900530056, 'AGUA MINERAL CRYSTAL 5L', 9.6, 'S', false, 0));
    l.add(new Produtos('', 1006, 'COXINHA DA ASA ', 10.4, 'S', false, 0));
    l.add(new Produtos(
        '', 7896083800025, 'AGUA SANITARIA Q BOA 2L', 6.09, 'S', false, 0));
    l.add(new Produtos('', 7896641000126, 'FEIJAO DO PADRE CARIOC TP1 1KG',
        3.26, 'S', false, 0));
    l.add(new Produtos(
        '', 7898312043333, 'BATATA PRE FRITA AVIVAR 400G', 4.6, 'S', false, 0));
    l.add(new Produtos(
        '', 7897013810053, 'MANTEIGA RADIO C/SAL 200G', 8.95, 'S', false, 0));
    l.add(new Produtos('', 1126, 'PEIXINHO KG', 20, 'S', false, 0));
    l.add(new Produtos('', 1038, 'FRANGO SADIA  KG', 6.9, 'S', false, 0));
    l.add(new Produtos('', 7898949924197, 'FEIJAO VASCONCELOS NOBRE 1KG', 4.29,
        'S', false, 0));
    l.add(new Produtos('', 7896035911311, 'FARINHA MANDIOCA BIJU AMAFIL 500G',
        4.07, 'S', false, 0));
    l.add(new Produtos('', 1178, 'PESCOCO KG', 4.65, 'S', false, 0));
    l.add(new Produtos('', 24, 'PANCETA KG', 13.3, 'S', false, 0));
    l.add(new Produtos(
        '', 7893000394117, 'MARGARINA QUALY 250G C/SAL', 3.99, 'S', false, 0));
    l.add(new Produtos('', 23, 'LINGUICA TOSCANA KG', 11.8, 'S', false, 0));
    l.add(new Produtos('', 1019, 'FRALDINHA KG', 22, 'S', false, 0));
    l.add(new Produtos('', 5601252231164,
        'AZEITE EXTRA VIRGEM GALLO PORTUGAL VD 500ML', 29.4, 'S', false, 0));
    l.add(new Produtos('', 7891080147951, 'FARINHA DE TRIGO PRIMOR TRAD 1KG',
        3.4, 'S', false, 0));
    l.add(new Produtos(
        '', 7898347080013, 'MASSA LASANHA BOTELHO 500G', 8.6, 'S', false, 0));
    l.add(new Produtos(
        '', 1200, 'PEITO DE PERU DEF.SADIA KG', 53.9, 'S', false, 0));
    l.add(new Produtos(
        '', 7896590801256, 'LEITE CEMIL DESNATADO 1LT', 3.95, 'S', false, 0));
    l.add(new Produtos('', 7898016920022, 'PALMITO IMPERADOR INTEIRO VD 300GR',
        18.6, 'S', false, 0));
    l.add(new Produtos('', 7896828000017, 'AGUA COCO KERO COCO 200ML - 5980',
        2.75, 'S', false, 0));
    l.add(new Produtos(
        '', 7894000050034, 'MAIONESE HELLMANN`S 500G', 8.55, 'S', false, 0));
    l.add(new Produtos('', 1028, 'MACA DA PERNA KG', 21, 'S', false, 0));
    l.add(new Produtos('', 7896021314737,
        'MACARRAO COM OVOS PADRE NOSSO DON SAPORE 500G', 2.22, 'S', false, 0));
    l.add(new Produtos('', 7896292303003,
        'MOLHO TOMATE REFOGADO PREDILECTA TRAD. SC 340G', 1.39, 'S', false, 0));
    l.add(new Produtos('', 7896002366304,
        'PAO PULLMAN 12 GRAOS FIT 350G -23004', 8.4, 'S', false, 0));
    l.add(new Produtos(
        '', 7891000100103, 'LEITE CONDENSADO MOCA 395G', 6.85, 'S', false, 0));
    l.add(new Produtos('', 7891700033640, 'CATCHUP ARISCO TRADICIONAL 390G',
        8.35, 'S', false, 0));
    l.add(new Produtos('', 7897323600146, 'SODA CAUSTICA SOL CONC 96-99% 1KG',
        17.75, 'S', false, 0));
    l.add(new Produtos('', 1043, 'ASA DE FRANGO KG', 12.4, 'S', false, 0));
    l.add(new Produtos(
        '', 7898280080149, 'SAL REFINADO UNIAO 1KG', 1.12, 'S', false, 0));
    l.add(new Produtos('', 1071, 'FRANGO A PASSARINHO', 9.99, 'S', false, 0));
    l.add(new Produtos('', 7896002366267,
        'PAO PULLMAN VITAGRAO 12 GRAOS 500G -23005', 9.45, 'S', false, 0));
    l.add(new Produtos(
        '', 1100, 'SALAMINHO ITALIANO SADIA KG', 83.3, 'S', false, 0));
    l.add(new Produtos('', 7892300006690, 'FARINHA MILHO SINHA BIJU 500G', 2.3,
        'S', false, 0));
    l.add(
        new Produtos('', 7898925037026, 'CARVAO REIS 5 KG', 16, 'S', false, 0));
    l.add(new Produtos('', 112, 'MANTEIGA VACA KG', 10, 'S', false, 0));
    l.add(new Produtos('', 7896760901342,
        'OLEO COMPOSTO E AZEITE FAISAO 500 ML', 5, 'S', false, 0));
    l.add(new Produtos(
        '', 7893000488953, 'ASA TULIPA FGO SADIA 1KG', 20, 'S', false, 0));
    l.add(new Produtos('', 1041, 'PEITO DE FRANGO KG', 8.59, 'S', false, 0));
    l.add(new Produtos(
        '', 1052, 'LINGUICA DE PORCO APIMENTADA  KG', 18.5, 'S', false, 0));
    l.add(new Produtos('', 7896205722013,
        'MACARRAO BASILAR SEMOLADO ESPAG N-2 500G', 2.94, 'S', false, 0));
    l.add(new Produtos('', 7896036095904, 'MOLHO POMAROLA TRADICIONAL 340G SC',
        2.65, 'S', false, 0));
    l.add(new Produtos(
        '', 7898215151784, 'CREME LEITE PIRACANJUBA 200G', 2.9, 'S', false, 0));
    l.add(new Produtos(
        '', 7896083800018, 'AGUA SANITARIA Q BOA 1L', 3.45, 'S', false, 0));
    l.add(new Produtos(
        '', 7897534800144, 'AGUA SANITARIA START 2000ML', 4.8, 'S', false, 0));
    l.add(new Produtos('', 7896205722198,
        'MACARRAO BASILAR PARAFUSO SEMOLADO 500G', 2.89, 'S', false, 0));
    l.add(new Produtos('', 5601252106103,
        'AZEITE DE OLIVA GALLO PORTUGAL 500ML VD', 27.4, 'S', false, 0));
    l.add(new Produtos(
        '', 7898016920152, 'PALMITO IMPERADOR  300G', 13.5, 'S', false, 0));
    l.add(new Produtos('', 7896102509410,
        'MOLHO PRONTO QUERO TRADICIONAL SC 340G', 1.89, 'S', false, 0));
    l.add(new Produtos('', 7896550720030, 'FEIJAO VASCONCELOS NOBRE 1KG', 4.29,
        'S', false, 0));
    l.add(new Produtos('', 7896495000129, 'DESENTUPIDOR DIABO VERDE 300G', 14.9,
        'S', false, 0));
    l.add(new Produtos(
        '', 2230, 'AGUA LINDOYA MINERAL 20LT ', 13, 'S', false, 0));
    l.add(new Produtos('', 7891150056381,
        'AMACIANTE COMFORT CLASSIC  LV2 PG 1,7L', 12.95, 'S', false, 0));
    l.add(new Produtos(
        '', 7892727450038, 'CARVAO BRASIL 3 KG', 11.5, 'S', false, 0));
    l.add(
        new Produtos('', 1040, 'FRANGO INTEIRO SEARA KG', 6.9, 'S', false, 0));
    l.add(new Produtos('', 7898905153715, 'EXTRATO TOMATE BONARE NOVO 340G',
        2.14, 'S', false, 0));
    l.add(new Produtos('', 1017, 'MUSCULO TRASEIRO KG', 20, 'S', false, 0));
    l.add(new Produtos(
        '', 7896105500094, 'AGUA SANITARIA CANDURA 2LT', 4.59, 'S', false, 0));
    l.add(new Produtos(
        '', 7898099001342, 'SODA CAUSTICA YARA 950GR', 10.7, 'S', false, 0));
    l.add(new Produtos('', 7894904015429, 'CORACAO DE FRANGO SEARA BANDEJA 1KG',
        12, 'S', false, 0));
    l.add(new Produtos(
        '', 7892727450052, 'CARVAO BRASIL 5 KG', 16, 'S', false, 0));
    l.add(new Produtos('', 7896590817080, 'LEITE CEMIL SEMIDESNATADO TP 1L',
        3.95, 'S', false, 0));
    l.add(new Produtos('', 7898312042909, 'PAO DE QUEIJO CONG. AVIVAR 300G',
        3.9, 'S', false, 0));
    l.add(new Produtos('', 7897895800081, 'SACO LIXO BLACK & FORT 30L C/50',
        15.5, 'S', false, 0));
    l.add(new Produtos('', 7896051111528, 'LEITE ITAMBE SEMIDESNATADO TP 1L',
        3.09, 'S', false, 0));
    l.add(new Produtos('', 7893000949355,
        'NUGGETS SADIA FRANGO TRADICIONAL 300G', 10.05, 'S', false, 0));
    l.add(new Produtos('', 7896205722105,
        'MACARRAO BASILAR SEMOLADO ESPAQUETE 2 1K', 5.99, 'S', false, 0));
    l.add(new Produtos(
        '', 7892300001480, 'FUBA MIMOSO FINO SINHA 1KG', 2.51, 'S', false, 0));
    l.add(new Produtos('', 7898598810209,
        'AZEITE DE OLIVA  EXTRA VIRGEM TOZZI 500 ML', 20.45, 'S', false, 0));
    l.add(new Produtos(
        '', 7898925037019, 'CARVAO REIS 3 KG', 11.5, 'S', false, 0));
    l.add(new Produtos(
        '', 7622300119621, 'FERMENTO PO ROYAL 100GR', 3.35, 'S', false, 0));
    l.add(new Produtos('', 7898604230014,
        'COPO DESCARTAVEL COPOPLAST BRANCO. 200ML', 3.8, 'S', false, 0));
    l.add(new Produtos('', 7896641000744, 'CAFE DO PADRE EXTRA FORTE 500G',
        10.7, 'S', false, 0));
    l.add(new Produtos(
        '', 7896102582239, 'MILHO VERDE QUERO 200G SC', 2.16, 'S', false, 0));
    l.add(new Produtos('', 7896102501155, 'MILHO VERDE QUERO LT 280G PD 200G',
        2.09, 'S', false, 0));
    l.add(new Produtos('', 7896030892530,
        'COPO DESCARTAVEL COPOBRAS TRANS. 200ML 100UN', 4.55, 'S', false, 0));
    l.add(new Produtos('', 7893000069350, 'LINGUICA MISTA SADIA FININHA 240G',
        7.1, 'S', false, 0));
    l.add(new Produtos('', 1118, 'CHAMPIGNON INTEIRO KG', 37.8, 'S', false, 0));
    l.add(new Produtos('', 7896641000751, 'CAFE DO PADRE EXTRA FORTE 250G', 5.4,
        'S', false, 0));
    l.add(new Produtos('', 7896098900239, 'DETERGENTE LIQUIDO YPE COCO 500ML',
        2.05, 'S', false, 0));
    l.add(new Produtos(
        '', 7897534800236, 'AGUA SANITARIA START 1000ML', 2.55, 'S', false, 0));
    l.add(new Produtos('', 7896051115014, 'LEITE CONDENSADO ITAMBE TP 395G',
        5.7, 'S', false, 0));
    l.add(new Produtos(
        '', 7896100013018, 'SAL REFINADO MOC 1KG', 1.9, 'S', false, 0));
    l.add(new Produtos(
        '', 7894000010014, 'AMIDO MILHO MAIZENA 200G', 4.15, 'S', false, 0));
    l.add(new Produtos('', 7896051111764, 'LEITE ITAMBE SEMI DESNATADO 1LT TP',
        5.05, 'S', false, 0));
    l.add(new Produtos('', 7893000488878, 'ASA FRANGO SADIA APERITIVO  KG',
        16.4, 'S', false, 0));
    l.add(new Produtos(
        '', 7898902230068, 'AGUA COCO NOSSO COCO 1LT', 8.15, 'S', false, 0));
    l.add(new Produtos(
        '', 7622300862282, 'DIAMANTE NEGRO 20GR', 1.75, 'S', false, 0));
    l.add(new Produtos(
        '', 7894000050027, 'MAIONESE HELLMANNS 250G', 5.35, 'S', false, 0));
    l.add(new Produtos('', 7896205744404,
        'MACARRAO BASILAR SEMOLA LASANHA 500G', 7.9, 'S', false, 0));
    l.add(new Produtos(
        '', 7893000382961, 'MARGARINA QUALY S/SAL 250G', 3.9, 'S', false, 0));
    l.add(
        new Produtos('', 16, 'LINGUICA CALABRESA AURORA', 18.3, 'S', false, 0));
    l.add(new Produtos('', 7897895800074, 'SACO LIXO BLACK & FORT 15L C/80',
        15.5, 'S', false, 0));
    l.add(new Produtos('', 7896051115038, 'LEITE CONDENSADO ITAMBE 395G LT',
        5.15, 'S', false, 0));
    l.add(new Produtos(
        '', 7896036090602, 'OLEO LIZA GIRASSOL 900ML', 8.8, 'S', false, 0));
    l.add(new Produtos(
        '', 1039, 'FRANGO CAIPIRA NHO BENTO KG', 15.25, 'S', false, 0));
    l.add(new Produtos('', 7896036095096,
        'MOLHO TOMATE POMAROLA TRADIC CX 260G', 2.78, 'S', false, 0));
    l.add(new Produtos('', 7896629650121,
        'COALHADA INTEGRAL CANTO DE MINAS 900G', 7.6, 'S', false, 0));
    l.add(new Produtos('', 7898224530020, 'FERMENTO EM PO TRISANTI 250 GR ',
        5.82, 'S', false, 0));
    l.add(new Produtos('', 7896021314720, 'MACARRAO ESPAGUETE DON SAPORE 500G',
        2.55, 'S', false, 0));
    l.add(new Produtos('', 7896828000482, 'AGUA COCO KERO COCO 330ML- 5981',
        5.45, 'S', false, 0));
    l.add(
        new Produtos('', 7897434088208, 'PAO CONGELADO', 29.9, 'S', false, 0));
    l.add(new Produtos('', 7896094910904, 'ADOCANTE LIQUIDO ZERO CAL 100ML',
        4.49, 'S', false, 0));
    l.add(new Produtos('', 7896226501239, 'PAO DE QUEIJO TRADICAO MINEIRA 2KG',
        28.9, 'S', false, 0));
    l.add(new Produtos('', 7897664130036,
        'DETERGENTE LIQUIDO MINUANO NEUTRO 500ML', 1.99, 'S', false, 0));
    l.add(new Produtos(
        '', 7896028014494, 'LEITE COCO MENINA 200ML', 2.86, 'S', false, 0));
    l.add(new Produtos('', 7896205744039,
        'MACARRAO BASILAR SEMOLA FININHO N.1 500G', 2.99, 'S', false, 0));
    l.add(new Produtos('', 7898909755915, 'CHAMPIGNON TOZZI INTEIRO SACHE 80GR',
        3.99, 'S', false, 0));
    l.add(new Produtos('', 7896102502763, 'CATCHUP QUERO TRADICIONAL 200 GR',
        3.95, 'S', false, 0));
    l.add(new Produtos(
        '', 7892300000285, 'OLEO SINHA CANOLA 900ML', 7.89, 'S', false, 0));
    l.add(new Produtos('', 7891024194102,
        'DESINFETANTE PINHO SOL ORIGINAL 500ML', 6.1, 'S', false, 0));
    l.add(new Produtos('', 7898067980112,
        'FARINHA MANDIOCA MANZI TEMPERADA 500G', 5.45, 'S', false, 0));
    l.add(new Produtos('', 7898073358240,
        'AZEITE DE OLIVA MISTO 33% LISBOA 500ML', 9, 'S', false, 0));
    l.add(new Produtos(
        '', 7896685200223, 'COQUETEL DA ROCHA 900ML', 10.75, 'S', false, 0));
    l.add(new Produtos('', 7897602400078, 'FEIJAO CARIOCA CASA GRANDE 1 K',
        3.49, 'S', false, 0));
    l.add(new Produtos('', 7894650002117,
        'DESINFETANTE PATO POWER MANCHAS 500ML', 13, 'S', false, 0));
    l.add(new Produtos('', 7896102583182,
        'MAIONESE HEINZ OVOS GALINHA CAIPIRA 390G', 10.35, 'S', false, 0));
    l.add(new Produtos(
        '', 7894000000350, 'CALDO KNORR GALINHA 114G', 3.6, 'S', false, 0));
    l.add(new Produtos('', 7896094910959, 'ADOCANTE LIQUIDO ZERO CAL 200ML',
        7.16, 'S', false, 0));
    l.add(new Produtos('', 7896855901417, 'VINHO PERGOLA TINTO SUAVE 1L', 24.85,
        'S', false, 0));
    l.add(new Produtos('', 7894321235226, 'AGUA COCO KERO COCO 200ML - 32812',
        2.66, 'S', false, 0));
    l.add(new Produtos('', 7894904015092, 'COXINHA DA ASA SEARA BANDJ 1KG',
        9.49, 'S', false, 0));
    l.add(new Produtos(
        '', 7896102503708, 'KETCHUP HEINZ 397G ', 11.85, 'S', false, 0));
    l.add(new Produtos('', 7891167011748,
        'ATUM RALADO GOMES DA COSTA EM OLEO 170G', 5.85, 'S', false, 0));
    l.add(new Produtos('', 7891150012363, 'CALDO KNORR CARNE REALCA A COR 114',
        3.6, 'S', false, 0));
    l.add(new Produtos('', 7896780900196, 'VINHO TINTO SUAVE CANCAO 750ML',
        15.4, 'S', false, 0));
    l.add(new Produtos('', 7891024194607, 'DESINFETANTE PINHO SOL 1 LITRO ',
        9.2, 'S', false, 0));
    l.add(new Produtos(
        '', 7894904018161, 'CORACAO BANDEJA SEARA 1KG', 22.7, 'S', false, 0));
    l.add(new Produtos('', 7896098900215, 'DETERGENTE LIQUIDO YPE MACA 500ML',
        2.05, 'S', false, 0));
    l.add(new Produtos('', 7893333325000,
        'QUEIJO CREAM CHEESE PHILADELPHIA ORIG 150G', 8.9, 'S', false, 0));
    l.add(new Produtos('', 7898939720082,
        'COPO DESCART TRANSP CRISTALCOPO 200ML C/100', 3.7, 'S', false, 0));
    l.add(new Produtos('', 7898119630071,
        'FARINHA DE MANDIOCA DO FORNINHO 500 GR', 7.35, 'S', false, 0));
    l.add(new Produtos(
        '', 7896656300068, 'FUBA MIMOSO PONTAL 500G', 1.19, 'S', false, 0));
    l.add(new Produtos(
        '', 7896105500018, 'AGUA SANITARIA CANDURA 1L', 2.89, 'S', false, 0));
    l.add(new Produtos(
        '', 7894000010021, 'AMIDO MILHO MAIZENA 500G', 8.65, 'S', false, 0));
    l.add(new Produtos(
        '', 7896523104638, 'OREGANO FA JUNCO 20GR', 4.18, 'S', false, 0));
    l.add(new Produtos(
        '', 7896028014487, 'LEITE COCO MENINA 500ML', 7.15, 'S', false, 0));
    l.add(new Produtos(
        '', 7893000383005, 'MARGARINA QUALY S/ SAL 500G', 7.15, 'S', false, 0));
    l.add(new Produtos('', 7898347080228,
        'PAO DE ALHO BOTELHO C/QUEIJO TRAD 300G', 8.55, 'S', false, 0));
    l.add(new Produtos('', 7896205744046,
        'MACARRAO BASILAR SEMOLA ESPAG N.2 500G', 2.99, 'S', false, 0));
    l.add(new Produtos('', 7896036093085,
        'OLEO COMPOSTO MARIA TRADICIONAL 200 ML', 5.97, 'S', false, 0));
    l.add(new Produtos(
        '', 7896035950372, 'AMIDO DE MILHO AMAFIL 1KG', 5.95, 'S', false, 0));
    l.add(new Produtos('', 7896094911000,
        'ADOCANTE LIQUIDO ZERO CAL 100ML ASPARTAM', 10.4, 'S', false, 0));
    l.add(new Produtos('', 7893000079298, 'MAGARINA VEG.CREMOSA C/S QUALY 1KG',
        13.65, 'S', false, 0));
    l.add(new Produtos('', 7894000050737, 'MAIONESE HELLMANN`S LIGHT 250G',
        5.79, 'S', false, 0));
    l.add(new Produtos(
        '', 7891000251539, 'CALDO MAGGI GALINHA 114G', 3.35, 'S', false, 0));
    l.add(new Produtos(
        '', 7897534817197, 'DESINFETANTE AZULIM WAVE 1L', 4.4, 'S', false, 0));
    l.add(new Produtos('', 7897534811096,
        'DESINFETANTE DETERGEL LAVANDA START 2L', 19.8, 'S', false, 0));
    l.add(new Produtos(
        '', 7893000514973, 'MARGARINA QUALY LIGHT 250G', 3.6, 'S', false, 0));
    l.add(new Produtos('', 7896021314256,
        'MACARRAO ESPAGUETE 8 DON SAPORE 500G', 2.45, 'S', false, 0));
    l.add(new Produtos('', 7892840256449, 'BATATA  LAYS CLASSICA 96GR 32191',
        6.5, 'S', false, 0));
    l.add(new Produtos('', 7896098900222, 'DETERGENTE LIQUIDO YPE LIMAO 500ML',
        2.05, 'S', false, 0));
    l.add(new Produtos('', 7896102503715, 'MOLHO MOSTARDA HEINZ QUERO 255G',
        12.5, 'S', false, 0));
    l.add(new Produtos(
        '', 7898067980167, 'FARINHA MANDIOCA MANZI 500G', 4.65, 'S', false, 0));
    l.add(new Produtos('', 7896040703734,
        'LIMPADOR CASA E PERFUME BELA FLORE 1L', 7.4, 'S', false, 0));
    l.add(new Produtos(
        '', 7896183901776, 'FAROFA MILHO ZAELI 500G', 3.69, 'S', false, 0));
    l.add(new Produtos('', 7891155002437, 'COPO NADIR FIGUEREDO BAR 10 1800231',
        3.35, 'S', false, 0));
    l.add(new Produtos('', 7891000081501,
        'BEBIDA ACHOCOLATADA NESCAU NESTLE 1L', 9.2, 'S', false, 0));
    l.add(new Produtos('', 7896656300334,
        'FARINHA DE MANDIOCA PREMIUM PONTAL 500G', 7.1, 'S', false, 0));
    l.add(new Produtos(
        '', 7893333224006, 'FERMENTO PO ROYAL 100G', 2.2, 'S', false, 0));
    l.add(new Produtos(
        '', 7894321822020, 'ATUM COQUEIRO RALADO 170G', 5.88, 'S', false, 0));
    l.add(new Produtos('', 7896035921082,
        'AMIDO DE MANDIOCA ESPECIAL AMID+MAIS 1KG', 8.65, 'S', false, 0));
    l.add(new Produtos('', 7896021314751,
        'MACARRAO COM OVOS PENA DON SAPORE 500G', 2.45, 'S', false, 0));
    l.add(new Produtos(
        '', 7896327511427, 'CALDO DE CARNE APTI 1KG', 7.99, 'S', false, 0));
    l.add(new Produtos(
        '', 7896327511410, 'CALDO DE GALINHA APTI 1KG', 7.99, 'S', false, 0));
    l.add(new Produtos('', 7897664130319,
        'DETERGENTE LIQUIDO MINUANO MARINE 500ML', 1.99, 'S', false, 0));
    l.add(new Produtos('', 7892840256548,
        'BATATA  LAYS  CLASSICA CROCANTE 50G 32192', 3.75, 'S', false, 0));
    l.add(new Produtos(
        '', 7894321219820, 'AVEIA EM FLOCOS QUAKER 200G', 4.18, 'S', false, 0));
    l.add(new Produtos(
        '', 7896102509144, 'MOSTARDA QUERO 190 GR', 3.55, 'S', false, 0));
    l.add(new Produtos('', 7896205755325,
        'MACARRAO BASILAR LAMEN GALINHA CAIPIRA 80G', 1.1, 'S', false, 0));
    l.add(new Produtos('', 7896656302048,
        'FAROFA PONTAL PRONTA SABOR BACON 350G', 5.3, 'S', false, 0));
    l.add(new Produtos('', 7894900550054, 'BEBIDA DEL VALLE FRUT UVA GF 450ML',
        1.9, 'S', false, 0));
    l.add(new Produtos('', 7896051111511,
        'LEITE ITAMBE SEMI DESNAT CALCIO1LT TP', 5.05, 'S', false, 0));
    l.add(new Produtos('', 5601252100804, 'AZEITE EXTRA VIRGEM GALLO 500 ML',
        30.3, 'S', false, 0));
    l.add(new Produtos(
        '',
        7891035215537,
        'DESENGORDURANTE VEJA COZINHA C/EXTR LIMAO 500ML',
        10.09,
        'S',
        false,
        0));
    l.add(new Produtos('', 7892840808013, 'GATORADE FRUTAS CITRICAS 500ML',
        4.99, 'S', false, 0));
    l.add(new Produtos('', 7897000800814, 'DESINFETANTE LYSOFORM SUAVE  1L',
        9.95, 'S', false, 0));
    l.add(new Produtos(
        '', 7896656300099, 'FUBA DE CANJICA PONTAL 1KG', 2.95, 'S', false, 0));
    l.add(new Produtos('', 7896205756377,
        'MACARRAO BASILAR SEMOLADO GRAVATA 500 G', 2.95, 'S', false, 0));
    l.add(new Produtos('', 7896102502299,
        'MOLHO PRONTO QUERO BOLONHESA SC 340GR', 2.95, 'S', false, 0));
    l.add(new Produtos('', 7894321219721, 'AVEIA EM FLOCOS  FINOS QUAKER 200G',
        4.18, 'S', false, 0));
    l.add(new Produtos('', 7891167011953,
        'ATUM RALADO GOMES DA COSTA MOLHO TOMATE 170G', 5.85, 'S', false, 0));
    l.add(new Produtos(
        '', 7622300119652, 'FERMENTO ROYAL 250GR', 7.3, 'S', false, 0));
    l.add(
        new Produtos('', 1036, 'PE E ORELHA DE PORCO KG', 7.99, 'S', false, 0));
    l.add(new Produtos('', 7896216100626,
        'MASSA LASANHA FACIL PRE- COZIDA 400GR', 7.05, 'S', false, 0));
    l.add(new Produtos('', 7896036094969, 'MOLHO POMAROLA TRADIC 350G LT', 3.95,
        'S', false, 0));
    l.add(new Produtos('', 7898949924388, 'ARROZ INTEGRAL VASCONCELOS 1KG',
        4.58, 'S', false, 0));
    l.add(new Produtos('', 7898347080020,
        'MASSA PASTEL BOTELHO ROLO CUMPRIDA 500G', 6.7, 'S', false, 0));
    l.add(new Produtos('', 7898347080143, 'MASSA PARA PASTEL BOTELHO 500G', 6.7,
        'S', false, 0));
    l.add(new Produtos('', 7891167011731,
        'ATUM GOMES DA COSTA SOLIDO EM OLEO 170 GR', 8.9, 'S', false, 0));
    l.add(new Produtos('', 7896102507171,
        'MOLHO PRONTO QUERO MANJERICAO SC 340G', 2.4, 'S', false, 0));
    l.add(new Produtos('', 7898913382022, 'PAO DE ALHO SO MINAS PICANTE 300G',
        8.49, 'S', false, 0));
    l.add(new Produtos('', 7897534800588, 'DESINFETANTE VOREL FLORAL 2000ML',
        5.05, 'S', false, 0));
    l.add(new Produtos('', 1023, 'LAGARTO KG', 21.9, 'S', false, 0));
    l.add(new Produtos('', 7891150016859,
        'CALDO KNORR GANINHA 114GR LV12 PG 10', 3.1, 'S', false, 0));
    l.add(new Produtos('', 7891040150021,
        'ESPONJA 3M BUCHA SCOTCH BRITE COZINHA', 1.65, 'S', false, 0));
    l.add(new Produtos('', 7897000800180,
        'DESINF. LYSOFORM SUAVE ODOR BRUTO 500ML', 4.85, 'S', false, 0));
    l.add(new Produtos('', 7896102586060,
        'MOLHO PRONTO QUERO ERVAS FINAS SC 340G', 2.4, 'S', false, 0));
    l.add(new Produtos('', 7896205775811,
        'MACARRAO BASILAR SEMOLADO 9 MINUT BAVETI 500GR', 2.99, 'S', false, 0));
    l.add(new Produtos(
        '', 7896656300051, 'FARINHA MILHO PONTAL 500G', 3.4, 'S', false, 0));
    l.add(new Produtos(
        '', 7898909755359, 'CHAMPIGNON TOZZI 100GR', 5.85, 'S', false, 0));
    l.add(new Produtos('', 7896205755349, 'MACARRAO BASILAR LAMEN CARNE 80G',
        1.1, 'S', false, 0));
    l.add(new Produtos('', 3, 'QUEIJO MUSSARELA BOLA KG', 37.8, 'S', false, 0));
    l.add(new Produtos('', 7896102502909, 'CATCHUP QUERO TRADICIONAL TP 300G',
        3.25, 'S', false, 0));
    l.add(new Produtos('', 7898930340746,
        'COPO DESCARTAVEL COPOPLAST CRIST 500ML C/50', 7.55, 'S', false, 0));
    l.add(new Produtos('', 7896035911113, 'FARINHA MANDIOCA AMAFIL TORRADA 1KG',
        5.55, 'S', false, 0));
    l.add(new Produtos(
        '', 7896004400075, 'LEITE COCO SOCOCO 200ML', 5.55, 'S', false, 0));
    l.add(new Produtos('', 7891155003663, 'COPO AMERICANO C/24 UN NADIR', 1.05,
        'S', false, 0));
    l.add(new Produtos(
        '', 7892300030015, 'OLEO MILHO SINHA 900ML', 7.25, 'S', false, 0));
    l.add(new Produtos('', 7896523100395, 'CORANTE JUNCO VERM/MORANG 10ML', 7.2,
        'S', false, 0));
    l.add(new Produtos(
        '', 7896102509434, 'MAIONESE QUERO SACHE 200 G ', 1.79, 'S', false, 0));
    l.add(new Produtos('', 7896021314249,
        'MACARRAO PADRE NOSSO SEMOLA DON SAPORE 500G', 2.65, 'S', false, 0));
    l.add(new Produtos('', 7896656302017,
        'FAROFA PONTAL PRONTA SABOR PIMENTA 350G', 5.3, 'S', false, 0));
    l.add(new Produtos('', 7894900556056,
        'BEBIDA DEL VALLE FRUT LARANJA GF 450ML', 1.9, 'S', false, 0));
    l.add(new Produtos(
        '', 7891000250174, 'CALDO MAGGI GALINHA 57G', 1.6, 'S', false, 0));
    l.add(new Produtos('', 7891022080025,
        'DESINFETANTE PINHO BRIL PLUS SILV 500ML', 3.45, 'S', false, 0));
    l.add(new Produtos('', 7892141951494, 'COPO DESCART TOPFORM 50ML C/100',
        2.05, 'S', false, 0));
    l.add(new Produtos(
        '', 7896054903151, 'MAIONESE DAJUDA 200GR SC', 1.7, 'S', false, 0));
    l.add(new Produtos('', 7891022080049,
        'DESINFETANTE PINHO BRIL SILVESTRE PLUS 1L', 6.8, 'S', false, 0));
    l.add(new Produtos('', 7896036092934,
        'OLEO COMPOSTO MARIA TRADICIONAL 500ML', 9.99, 'S', false, 0));
    l.add(new Produtos(
        '', 7891700206921, 'MAIONESE ARISCO 500/491GR', 4.95, 'S', false, 0));
    l.add(new Produtos('', 7896183905507,
        'FAROFA MANDIOCA SABOR CALABRESA ZAELI 500G', 4.9, 'S', false, 0));
    l.add(new Produtos('', 7893321154704, 'FARINHA MANDIOCA BIJU MANE 500G',
        9.5, 'S', false, 0));
    l.add(new Produtos(
        '', 7898067980105, 'FARINHA ROSCA MANZI 500G', 4.65, 'S', false, 0));
    l.add(new Produtos('', 7897414310046,
        'BANHA ARTESANAL SUINA EBENEZER 500ML', 9.3, 'S', false, 0));
    l.add(new Produtos(
        '', 7896656300075, 'FUBA MIMOSO PONTAL 1KG', 2.65, 'S', false, 0));
    l.add(new Produtos('', 7891024037690, 'DESINFETANTE PINHO SOL LAVANDA 1L',
        9.2, 'S', false, 0));
    l.add(new Produtos('', 7898409957970, 'FERMENTO BIOLOGICO FLEISCHMANN UN',
        2, 'S', false, 0));
    l.add(new Produtos('', 7896205744183,
        'MACARRAO BASILAR PADRE NOSSO SEMOLA 500G', 2.99, 'S', false, 0));
    l.add(new Produtos(
        '', 7898407352197, 'AMIDO MILHO MILHENA 500 GR', 4.45, 'S', false, 0));
    l.add(new Produtos('', 7891167011755, 'ATUM SOLIDO GOMES DA COSTA NAT 120G',
        8.9, 'S', false, 0));
    l.add(new Produtos('', 7897534817210, 'DESINFETANTE AZULIM FLORATA 1L', 4.4,
        'S', false, 0));
    l.add(new Produtos('', 7891167011779,
        'ATUM RALADO GOMES DA COSTA LIGHT 170G', 5.85, 'S', false, 0));
    l.add(new Produtos(
        '',
        7891035800061,
        'DESINFETANTE VEJA X-14 TIRA LIMO C/ PULV 500ML 30%',
        17.5,
        'S',
        false,
        0));
    l.add(new Produtos('', 7894650013908, 'DESENGORDURANTE MR MUSCULO 500ML',
        8.75, 'S', false, 0));
    l.add(new Produtos(
        '', 7896025802889, 'MOLHO INGLES CEPERA 1010 ML', 5.79, 'S', false, 0));
    l.add(new Produtos(
        '', 7898922974072, 'KETCHUP FLAMBOYANT 400GR', 4.29, 'S', false, 0));
    l.add(new Produtos(
        '', 7896523104379, 'CANELA DA CHINA PO FA 30G', 2.45, 'S', false, 0));
    l.add(new Produtos(
        '', 7894000030470, 'MAIONESE HELLMANNS 200G', 5.69, 'S', false, 0));
    l.add(new Produtos(
        '',
        7898422746216,
        'SHAMPOO CLEAR ANTICASPA MEN DUAL EFFECT 2X1 200 ML',
        16.97,
        'S',
        false,
        0));
    l.add(new Produtos('', 7898921567077, 'BATATA CONG. BEM BRASIL 400G', 5.65,
        'S', false, 0));
    l.add(new Produtos('', 7896102507188, 'MOLHO PRONTO QUERO PIZZA SC 340G',
        2.4, 'S', false, 0));
    l.add(new Produtos(
        '', 7896007811311, 'MOLHO ALHO KENKO 150ML', 3.35, 'S', false, 0));
    l.add(new Produtos('', 7891024193280, 'DESINFETANTE PINHO SOL FLORAL 500ML',
        5.58, 'S', false, 0));
    l.add(new Produtos('', 7896102502756, 'CATCHUP QUERO TRADICIONAL 400G',
        5.45, 'S', false, 0));
    l.add(new Produtos(
        '',
        7891035800245,
        'DESENGORDURANTE VEJA COZINHA C/EXTR LIMAO LV500 PG',
        7.99,
        'S',
        false,
        0));
    l.add(new Produtos('', 7896656302031,
        'FAROFA PONTAL PRONTA SABOR GALINHA 350G', 5.3, 'S', false, 0));
    l.add(new Produtos('', 7896183201579,
        'LEITE LONGA VIDA QUATA ZERO LACTOSE 1 LT', 3.95, 'S', false, 0));
    l.add(new Produtos(
        '', 7896102584189, 'MAIONESE HEINZ 215G', 7.89, 'S', false, 0));
    l.add(new Produtos('', 7896034300345, 'VINHO TINTO SUAVE DI BARTOLO 750ML',
        15.55, 'S', false, 0));
    l.add(new Produtos(
        '', 7897315500041, 'FEIJAO JOAO PEDRO JALO 1KG', 5.15, 'S', false, 0));
    l.add(new Produtos(
        '', 1085, 'MORTADELA AURORA  DEFUMADA  KG', 16.8, 'S', false, 0));
    l.add(new Produtos(
        '', 7892300026629, 'FUBA MIMOSO FINO SINHA 500G', 2.19, 'S', false, 0));
    l.add(new Produtos(
        '', 7891167031883, 'ATUM RALADO EM OLEO 88 170GR', 5.1, 'S', false, 0));
    l.add(new Produtos('', 7897534800564, 'DESINFETANTE VOREL EUCALIPTO 2L',
        5.05, 'S', false, 0));
    l.add(new Produtos('', 7898024830184,
        'DESINFETANTE VEJA  X-14 TIRA-LIMO REFIL 500ML', 14.99, 'S', false, 0));
    l.add(new Produtos('', 7896205744138,
        'MACARRAO BASILAR SEMOLA ESPIRAL 500G', 2.99, 'S', false, 0));
    l.add(new Produtos(
        '', 7896090122707, 'LA DE ACO ASSOLAN 60G C/8UN', 2.1, 'S', false, 0));
    l.add(new Produtos(
        '', 7896943000015, 'FARINHA DE MILHO ARAXA 1KG', 7.35, 'S', false, 0));
    l.add(new Produtos('', 7896205722068,
        'MACARRAO BASILAR PADRE NOSSO SEMOLADO 500GR', 2.89, 'S', false, 0));
    l.add(new Produtos('', 7891022850031, 'ESPONJA DE ACO Q LUSTROS 60G C/ 8UN',
        1.79, 'S', false, 0));
    l.add(new Produtos(
        '', 7896523104362, 'CANELA CASCA FA JUNCO 30GR', 4.59, 'S', false, 0));
    l.add(new Produtos('', 7891000251515, 'CALDO MAGGI CARNE 12 TABL 114G',
        3.35, 'S', false, 0));
    l.add(new Produtos('', 7896205722211,
        'MACARRAO BASILAR PENNE SEMOLADO 500G', 2.65, 'S', false, 0));
    l.add(new Produtos(
        '', 7898922974089, 'MOSTARDA FLAMBOYANT 180GR', 3.25, 'S', false, 0));
    l.add(new Produtos('', 7891132019069,
        'TEMPERO COMPLETO SABOR AMI S/PIM 1KG', 12.75, 'S', false, 0));
    l.add(new Produtos('', 7891000102640, 'AVEIA EM FLOCOS FINOS NESTLE 200G',
        4.15, 'S', false, 0));
    l.add(new Produtos('', 7891150012370, 'CALDO KNORR CARNE REALCA A COR 19G',
        0.65, 'S', false, 0));
    l.add(new Produtos('', 7896685600016,
        'FARINHA DE MANDIOCA BIJU DONA CHICA 500G', 5.9, 'S', false, 0));
    l.add(new Produtos('', 7894321822129, 'ATUM COQUEIRO ATUM RALADO 160GR',
        5.9, 'S', false, 0));
    l.add(new Produtos('', 7896275921101, 'BANHA USO CULINARIO FRIMESA 450G',
        5.89, 'S', false, 0));
    l.add(new Produtos(
        '', 7894000000213, 'CALDO KNORR GALINHA 19G', 0.6, 'S', false, 0));
    l.add(new Produtos('', 742832582902, 'PAO DE QUEIJO TRADICAO MINEIRA 500G',
        5.59, 'S', false, 0));
    l.add(new Produtos('', 7891024193006,
        'DESINFETANTE PINHO SOL CITRUS LAVANDA 500ML', 5.58, 'S', false, 0));
    l.add(new Produtos('', 7896656300228, 'FARINHA MANDIOCA BIJU PONTAL 500G',
        5.35, 'S', false, 0));
    l.add(new Produtos(
        '', 7896025800106, 'AZEITE DE DENDE CEPERA 100ML', 5.3, 'S', false, 0));
    l.add(new Produtos(
        '', 7896523101064, 'CANELA EM CASCA FA 15G', 2.65, 'S', false, 0));
    l.add(new Produtos('', 7891150047662, 'CALDO KNORR CARNE 57G LV 6 PG 5 ',
        1.75, 'S', false, 0));
    l.add(new Produtos('', 7896814900314, 'FARINHA MANDIOCA PERDIZES 500G',
        5.15, 'S', false, 0));
    l.add(new Produtos(
        '',
        7891000102336,
        'MAGGI TEMPORO GALINHA CAIPIRA E UMA BOLSA PARA ASS',
        4.99,
        'S',
        false,
        0));
    l.add(new Produtos(
        '', 7891000249932, 'CALDO MAGGI BACON 6 TABL 57G', 1.6, 'S', false, 0));
    l.add(new Produtos('', 7896007800025, 'MOLHO SOJA AJI NO SHOYU 500ML', 9.53,
        'S', false, 0));
    l.add(new Produtos('', 7896292304222,
        'MOLHO DE TOMATE PREDILECTA PIZZA 340GR', 1.9, 'S', false, 0));
    l.add(new Produtos('', 7896292304215,
        'MOLHO DE TOMATE PREDILECTA ERVAS FINAS 340G', 1.9, 'S', false, 0));
    l.add(new Produtos('', 7896256040258,
        'FARINHA DE TRIGO INTEGRAL KODILAR 1 KG', 9.45, 'S', false, 0));
    l.add(new Produtos(
        '', 7896102501254, 'MILHO VERDE QUERO 2KG', 9, 'S', false, 0));
    l.add(new Produtos(
        '', 7897013809064, 'MANTEIGA RADIO C/SAL 200G', 8.95, 'S', false, 0));
    l.add(new Produtos(
        '', 7898264716309, 'FARINHEIRA BRANCA ERCAPLAST', 8.9, 'S', false, 0));
    l.add(new Produtos('', 7891167011717, 'ATUM PEDACO GOMES DA COSTA 170G',
        8.8, 'S', false, 0));
    l.add(new Produtos('', 7891167011724,
        'ATUM RALADO GOMES DA COSTA PEDACOS NATURAL 120G', 8.8, 'S', false, 0));
    l.add(new Produtos('', 7897534826168,
        'DESINFETANTE AZULIM MARINER START 1 LT', 4.4, 'S', false, 0));
    l.add(new Produtos(
        '', 7896943000206, 'FAROFA PICANHA ARAXA 300G', 4.4, 'S', false, 0));
    l.add(new Produtos(
        '', 7894000033808, 'CALDO KNORR COSTELA 57G', 1.75, 'S', false, 0));
    l.add(new Produtos('', 7896205722204,
        'MACARRAO BASILAR AVE MARIA SEMOLADO 500G', 2.89, 'S', false, 0));
    l.add(new Produtos(
        '', 7894321823096, 'ATUM COQUEIRO PEDACO 170G', 8.55, 'S', false, 0));
    l.add(new Produtos('', 7894900550061, 'BEBIDA DEL VALLE FRUT UVA GF 1,5L',
        4.19, 'S', false, 0));
    l.add(new Produtos(
        '', 7896656300082, 'FUBA DE CANJICA PONTAL 500G', 1.35, 'S', false, 0));
    l.add(new Produtos('', 7896007811021, 'MOLHO SOJA AJI NO SHOYU PEQ 150ML',
        2.69, 'S', false, 0));
    l.add(new Produtos(
        '', 7897256700241, 'LEITE TIPO C CALU 1L UPITAM', 4, 'S', false, 0));
    l.add(new Produtos(
        '', 7891024194652, 'DESINFETANTE PINHO SOL 1 LT', 7.99, 'S', false, 0));
    l.add(new Produtos(
        '', 7898905153579, 'EXTRATO TOMATE BONARE 340G', 2.6, 'S', false, 0));
    l.add(new Produtos('', 7896256040449,
        'FARELO DE AVEIA NATURAL LIFE KODILAR 500GR', 7.75, 'S', false, 0));
    l.add(new Produtos(
        '', 7896110100814, 'SAL GROSSO LEBRE 1KG', 2.49, 'S', false, 0));
    l.add(new Produtos('', 7891150047648, 'CALDO KNORR GALINHA 57G LV6 PG5',
        1.75, 'S', false, 0));
    l.add(new Produtos(
        '', 7894904793082, 'MOELA FRANGO SEARA BANDEIJA', 6.99, 'S', false, 0));
    l.add(new Produtos('', 7891000582305, 'CANJAO MAGGI ARROZ E LEGUMES 200G',
        6.95, 'S', false, 0));
    l.add(new Produtos('', 7891022101492,
        'DESINFETANTE PINHO BRIL MARINE 500ML', 3.45, 'S', false, 0));
    l.add(new Produtos(
        '', 7896523100210, 'CANELA EM PO FA 15G', 1.69, 'S', false, 0));
    l.add(new Produtos('', 7898347080044,
        'MASSA PASTEL BOTELHO DISCO REDONDA MD 500G', 6.7, 'S', false, 0));
    l.add(new Produtos(
        '', 7891000528907, 'CALDO MAGGI CARNE C/6UN 63GR', 1.6, 'S', false, 0));
    l.add(new Produtos('', 7896035911137,
        'FARINHA MANDIOCA AMAFIL TORRADA 500G', 3.1, 'S', false, 0));
    l.add(new Produtos('', 22, 'SOBRECOXA SADIA', 18.9, 'S', false, 0));
    l.add(new Produtos('', 7898930340012,
        'COPO DESCARTAVEL COPOPLAST 50ML C/100', 2.05, 'S', false, 0));
    l.add(new Produtos('', 7896205744251,
        'MACARRAO BASILAR SEMOLA AVE MARIA 500G', 2.99, 'S', false, 0));
    l.add(new Produtos('', 7896205744619,
        'MACARRAO BASILAR SEMOLA ESP. N.2 1KG', 5.8, 'S', false, 0));
    l.add(new Produtos(
        '', 7896523101781, 'CANELEIRO PO JUNCO 30G', 2.9, 'S', false, 0));
    l.add(new Produtos('', 7891167011960,
        'ATUM GOMES DA COSTA RALADO C/MOLHO PICANTE 170G', 5.7, 'S', false, 0));
    l.add(new Produtos(
        '',
        7891040230112,
        'ESPONJA 3M BUCHA SCOTCH BRITE  MULTIUSO 3X1 GRATIS',
        5.6,
        'S',
        false,
        0));
    l.add(new Produtos('', 28, 'LINGUICA FRANGO CHUR KG', 11.2, 'S', false, 0));
    l.add(new Produtos('', 7897534817463,
        'DESINFETANTE AZULIM GEL LAVANDA START 500G', 5.48, 'S', false, 0));
    l.add(new Produtos('', 1066, 'MEDALHAO KG', 19, 'S', false, 0));
    l.add(new Produtos('', 7896090122615, 'PALHA DE ACO ASSOLAN GROSSA Nº 2',
        1.3, 'S', false, 0));
    l.add(new Produtos('', 7891035285240, 'DESINFETANTE VEJA PINHO 480ML', 4.99,
        'S', false, 0));
    l.add(new Produtos('', 7896205756384,
        'MACARRAO BASILAR SEMOLADO TORTILHONE 500GR', 2.49, 'S', false, 0));
    l.add(new Produtos(
        '', 26, 'LINGUICA TOSCANA PERDIGAO KG', 17, 'S', false, 0));
    l.add(new Produtos('', 7897000800456, 'DESINF. LYSOFORM BRUTO 500ML', 4.85,
        'S', false, 0));
    l.add(new Produtos('', 7896102504088,
        'MOLHO PRONTO QUERO NAPOLITANO SC 340GR', 2.4, 'S', false, 0));
    l.add(new Produtos(
        '', 7891000250150, 'CALDO MAGGI CARNE 6 TABL 57G', 1.6, 'S', false, 0));
    l.add(new Produtos(
        '', 7896523158136, 'SAL ROSA DO HIMALAIA FA 60G', 4.7, 'S', false, 0));
    l.add(new Produtos('', 7897534817203, 'DESINFETANTE AZULIM NEON START 1 LT',
        4.4, 'S', false, 0));
    l.add(new Produtos('', 7897534825598, 'DESINFETANTE AZULIM VIOLETTE 1L',
        4.4, 'S', false, 0));
    l.add(new Produtos('', 7894321219929, 'FARINHA DE AVEIA QUAKER 200G', 4.18,
        'S', false, 0));
    l.add(new Produtos('', 7898901478409,
        'COPO DESCARTAVEL TERMOPOT CRISTAL 50 ML', 2.05, 'S', false, 0));
    l.add(new Produtos('', 7896273100836,
        'COQUETEL ALCOOLICO DOBARRIL BLUEBERRY 500ML', 3.99, 'S', false, 0));
    l.add(new Produtos('', 7896051121817, 'COALHADA INTEGRAL ITAMBE 130G', 1.99,
        'S', false, 0));
    l.add(new Produtos('', 7891150055070, 'AMIDO MILHO MAIZENA LV 200G PG 150',
        3.9, 'S', false, 0));
    l.add(new Produtos(
        '', 7891150016866, 'CALDO KNORR CARNE 114GR', 3.6, 'S', false, 0));
    l.add(new Produtos('', 7896256080582, 'TEMPERO COMPLETO KODILAR CHUR 500G',
        3.5, 'S', false, 0));
    l.add(new Produtos('', 7891022101249,
        'DESINFETANTE KALIPTO EUCALIPTO 750ML', 3.5, 'S', false, 0));
    l.add(new Produtos('', 7896159010013, 'FEIJAO CARIOCA T1 VASC PATUREBA 1KG',
        3.49, 'S', false, 0));
    l.add(new Produtos('', 1079, 'APRESUNTADO SEARA KG', 17.6, 'S', false, 0));
    l.add(new Produtos('', 7891323075287,
        'CORANTE LIQUIDO XADREZ VERMELHO 50ML', 3.3, 'S', false, 0));
    l.add(new Produtos(
        '', 7898920943865, 'COPO PLASTICO KING 1UN 290ML', 3.2, 'S', false, 0));
    l.add(new Produtos('', 7896205744220,
        'MACARRAO BASILAR SEMOLA ARGOLINHA 500G', 2.99, 'S', false, 0));
    l.add(new Produtos('', 7896205744053,
        'MACARRAO BASILAR SEMOLA FURADINHO 4 500G', 2.99, 'S', false, 0));
    l.add(new Produtos('', 7896205756407,
        'MACARRAO BASILAR SEMOLADO NHOQUE 500GR', 2.89, 'S', false, 0));
    l.add(new Produtos('', 7896523100418, 'LINGUA DE SOGRA JUNCO C/ 10UN', 2.85,
        'S', false, 0));
    l.add(new Produtos('', 7896183910334, 'FAROFA MANDIOCA E MILHO ZAELI 300G',
        2.59, 'S', false, 0));
    l.add(new Produtos('', 7898505140009,
        'COPO DESCARTAVEL TOTAL PLAST BRANCO 50ML', 2.05, 'S', false, 0));
    l.add(new Produtos(
        '', 7896256060348, 'CANELA EM PO KODILAR 10G', 2, 'S', false, 0));
    l.add(new Produtos('', 7894900552058,
        'BEBIDA DEL VALLE FRUT TANGERINA GF 450ML', 1.9, 'S', false, 0));
    l.add(new Produtos(
        '', 7891000249871, 'CALDO MAGGI COSTELA 57G', 1.8, 'S', false, 0));
    l.add(new Produtos('', 7892300000933,
        'FARINHA MILHO SINHA FLOCAO CUSCUZ 500G', 1.79, 'S', false, 0));
    l.add(new Produtos(
        '', 7891132006373, 'CALDO SAZON CARNE 9G', 1.73, 'S', false, 0));
    l.add(new Produtos('', 7891000250112,
        'CALDO MAGGI CARNE C/6UN 57G LV6 E PG5', 1.6, 'S', false, 0));
    */

    sync();
    return null;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Enviando Produtos'),
        ),
        body: SingleChildScrollView(
            child: Container(
                child: PrimaryButton(
                    icon: Icon(
                      Icons.save,
                      color: Colors.white,
                      size: 40.0,
                    ),
                    onPressed: () async {
                      await setCidades();
                    }))));
  }
}
