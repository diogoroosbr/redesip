import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:redesipapp/classes.dart';
import 'package:redesipapp/pages/mainPage.dart';
import 'package:redesipapp/style/theme.dart'; //_view.dart';

class PopFiltro extends StatefulWidget {
  PopFiltro({
    Key key,
    this.listaTotal,
    this.title,
    this.titlePadding: const EdgeInsets.fromLTRB(24.0, 24.0, 24.0, 0.0),
    this.contentPadding: const EdgeInsets.fromLTRB(0.0, 12.0, 0.0, 16.0),
    this.semanticLabel,
  })  : assert(titlePadding != null),
        assert(contentPadding != null),
        super(key: key);

  /// The (optional) title of the dialog is displayed in a large font at the top
  /// of the dialog.
  ///
  /// Typically a [Text] widget.
  final Widget title;
  final List<FiltroLocal> listaTotal;

  /// Padding around the title.
  ///
  /// If there is no title, no padding will be provided.
  ///
  /// By default, this provides the recommend Material Design padding of 24
  /// pixels around the left, top, and right edges of the title.
  ///
  /// See [contentPadding] for the conventions regarding padding between the
  /// [title] and the [children].
  final EdgeInsetsGeometry titlePadding;

  /// The (optional) content of the dialog is displayed in a
  /// [SingleChildScrollView] underneath the title.
  ///
  /// Typically a list of [SimpleDialogOption]s.

  /// Padding around the content.
  ///
  /// By default, this is 12 pixels on the top and 16 pixels on the bottom. This
  /// is intended to be combined with children that have 24 pixels of padding on
  /// the left and right, and 8 pixels of padding on the top and bottom, so that
  /// the content ends up being indented 20 pixels from the title, 24 pixels
  /// from the bottom, and 24 pixels from the sides.
  ///
  /// The [SimpleDialogOption] widget uses such padding.
  ///
  /// If there is no [title], the [contentPadding] should be adjusted so that
  /// the top padding ends up being 24 pixels.
  final EdgeInsetsGeometry contentPadding;

  /// The semantic label of the dialog used by accessibility frameworks to
  /// announce screen transitions when the dialog is opened and closed.
  ///
  /// If this label is not provided, a semantic label will be infered from the
  /// [title] if it is not null.  If there is no title, the label will be taken
  /// from [MaterialLocalizations.dialogLabel].
  ///
  /// See also:
  ///
  ///  * [SemanticsConfiguration.isRouteName], for a description of how this
  ///    value is used.
  final String semanticLabel;

  @override
  PopFiltroState createState() => new PopFiltroState();
}

class PopFiltroState extends State<PopFiltro> {
  List<Widget> children;
  Timestamp dataInicial = MainPage.filtroAnalise.isEmpty
      ? null
      : MainPage.filtroAnalise.marcadaInicio;
  Timestamp dataFinal = MainPage.filtroAnalise.isEmpty
      ? null
      : MainPage.filtroAnalise.marcadaFinal;
  bool filtrarData = MainPage.filtroAnalise.isEmpty
      ? false
      : MainPage.filtroAnalise.dataAtivado;
  List<Produtos> listaFiltrada;
  bool filtrarCultura = MainPage.filtroAnalise.isEmpty
      ? false
      : MainPage.filtroAnalise.culturaAtivado;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final List<Widget> body = <Widget>[];
    String label = widget.semanticLabel;
    body.add(Container(
        color: Tema.corPicker,
        child: Column(
          children: <Widget>[
            new Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.fromLTRB(8.0, 0.0, 0.0, 0.0),
                  child: new Text(
                    'Filtrar por Data',
                    style: TextStyle(fontSize: 18.5, color: Colors.white),
                  ),
                ),
                new Container(
                    padding: EdgeInsets.all(1.0),
                    child: Switch(
                      value: filtrarData,
                      activeColor: Colors.deepOrange[200],
                      activeTrackColor: Tema.corPrimaria,
                      onChanged: (bool valor) {
                        setState(() {
                          filtrarData = !filtrarData;
                          if (dataInicial != null) {
                            dataInicial = dataInicial;
                          } else {
                            dataInicial = Timestamp.now();
                          }
                          if (dataFinal != null) {
                            dataFinal = dataFinal;
                          } else {
                            dataFinal = Timestamp.now();
                          }
                        });
                      },
                    ))
              ],
            ),
          ],
        )));

    body.add(Container(
        color: Tema.corCard,
        child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              new Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.all(5.0),
                        ),
                        Text('Data Inicial',
                            style: TextStyle(
                                color: filtrarData
                                    ? Colors.black
                                    : Colors.grey[500])),
                        IconButton(
                            icon: Icon(Icons.calendar_today,
                                size: 30.0,
                                color: filtrarData
                                    ? Colors.black
                                    : Colors.grey[500]),
                            onPressed: filtrarData == false
                                ? null
                                : () {
                                    showDatePicker(
                                            context: context,
                                            initialDate: filtrarData
                                                ? dataInicial
                                                : DateTime.now(),
                                            firstDate: DateTime.now().subtract(
                                                Duration(days: 360 * 100)),
                                            lastDate: DateTime.now()
                                                .add(Duration(days: 360 * 100)))
                                        .then((d) {
                                      if (d != null) {
                                        setState(() {
                                          dataInicial = Timestamp.fromDate(d);
                                          if (dataFinal.toDate().isBefore(dataInicial.toDate()))
                                            dataFinal = dataInicial;
                                        });
                                      }
                                    });
                                  }),
                        Text(
                          dataInicial != null
                              ? dataInicial.toDate().day.toString().padLeft(2, '0') +
                                  '/' +
                                  dataInicial.toDate().month.toString().padLeft(2, '0') +
                                  '/' +
                                  dataInicial.toDate().year.toString()
                              : DateTime.now().day.toString().padLeft(2, '0') +
                                  '/' +
                                  DateTime.now()
                                      .month
                                      .toString()
                                      .padLeft(2, '0') +
                                  '/' +
                                  DateTime.now().year.toString(),
                          style: TextStyle(
                              color: filtrarData
                                  ? Colors.black
                                  : Colors.grey[500]),
                        )
                      ],
                    ),
                    Text(
                      'Até',
                      style: TextStyle(
                          fontSize: 20.0,
                          color: filtrarData ? Colors.black : Colors.grey[500]),
                    ),
                    Column(
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.all(5.0),
                        ),
                        Text(
                          'Data Final',
                          style: TextStyle(
                              color: filtrarData
                                  ? Colors.black
                                  : Colors.grey[500]),
                        ),
                        IconButton(
                            icon: Icon(
                              Icons.calendar_today,
                              size: 30.0,
                              color:
                                  filtrarData ? Colors.black : Colors.grey[500],
                            ),
                            onPressed: filtrarData == false
                                ? null
                                : () {
                                    showDatePicker(
                                            context: context,
                                            initialDate: filtrarData
                                                ? dataFinal
                                                : DateTime.now(),
                                            firstDate: filtrarData
                                                ? dataInicial
                                                : DateTime.now().subtract(
                                                    Duration(days: 360 * 100)),
                                            lastDate: DateTime.now()
                                                .add(Duration(days: 360 * 100)))
                                        .then((d) {
                                      if (d != null) {
                                        setState(() {
                                          dataFinal = Timestamp.fromDate(d);
                                        });
                                      }
                                    });
                                  }),
                        Text(
                          dataFinal != null
                              ? dataFinal.toDate().day.toString().padLeft(2, '0') +
                                  '/' +
                                  dataFinal.toDate().month.toString().padLeft(2, '0') +
                                  '/' +
                                  dataFinal.toDate().year.toString()
                              : DateTime.now().day.toString().padLeft(2, '0') +
                                  '/' +
                                  DateTime.now()
                                      .month
                                      .toString()
                                      .padLeft(2, '0') +
                                  '/' +
                                  DateTime.now().year.toString(),
                          style: TextStyle(
                              color: filtrarData
                                  ? Colors.black
                                  : Colors.grey[500]),
                        )
                      ],
                    ),
                  ])
            ])));

    body.add(Padding(
      padding: EdgeInsets.all(5.0),
    ));

    body.add(Container(
        color: Tema.corPicker,
        child: Padding(
          padding: EdgeInsets.fromLTRB(8.0, 0.0, 0.0, 0.0),
          child: Column(
            children: <Widget>[
              new Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  new Text('Filtrar por cultura',
                      style: TextStyle(fontSize: 18.5, color: Colors.white)),
                  new Container(
                      padding: EdgeInsets.all(1.0),
                      child: Switch(
                        value: filtrarCultura,
                        activeColor: Colors.deepOrange[200],
                        activeTrackColor: Tema.corPrimaria,
                        onChanged: (bool valor) {
                          setState(() {
                            filtrarCultura = !filtrarCultura;
                          });
                        },
                      ))
                ],
              ),
            ],
          ),
        )));

    body.add(Padding(
      padding: EdgeInsets.all(5.0),
    ));

    if (listadoFiltro != null)
      body.add(Expanded(
          child: StaggeredGridView.countBuilder(
        padding: EdgeInsets.all(2.0),
        crossAxisCount: 3,
        itemCount: widget.listaTotal.length,
        itemBuilder: (context, i) => new Container(
            margin: EdgeInsets.all(2.0),
            decoration: BoxDecoration(
                border: Border.all(color: Colors.black, width: 1.3),
                borderRadius: BorderRadius.circular(10.0),
                color: widget.listaTotal[i].seleciona
                    ? Tema.corPrimaria
                    : Colors.white),
            child: new FlatButton(
              child: Text(widget.listaTotal[i].item.nomIte,
                  style: TextStyle(
                      color: !filtrarCultura
                          ? Colors.grey[500]
                          : (widget.listaTotal[i].seleciona
                              ? Colors.white
                              : Colors.black))),
              onPressed: (!filtrarCultura || filtrarCultura == null)
                  ? null
                  : () {
                      setState(() {
                        widget.listaTotal[i].seleciona =
                            !widget.listaTotal[i].seleciona;
                      });
                    },
            )),
        staggeredTileBuilder: (int index) => new StaggeredTile.count(
            widget.listaTotal[index].item.nomIte.length <= 8 ? 1 : 2, 0.5),
      )));
    body.add(Container(
        decoration: BoxDecoration(
            border: Border(top: BorderSide(width: 1.5)), color: Tema.corPicker),
        padding: EdgeInsets.all(5.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            FlatButton(
                child: Text('Limpar'),
                color: Colors.grey[400],
                disabledColor: Tema.corPicker,
                disabledTextColor: Tema.corPicker,
                onPressed: (!widget.listaTotal.any((filt) => filt.seleciona) &&
                        filtrarData == false &&
                        filtrarCultura == false)
                    ? null
                    : () {
                        setState(() {
                          dataInicial = null;
                          dataFinal = null;
                          filtrarData = false;
                          widget.listaTotal.forEach((c) {
                            c.seleciona = false;
                          });
                          filtrarCultura = false;
                        });
                      }),
            Row(
              children: <Widget>[
                FlatButton(
                  padding: EdgeInsets.all(2.0),
                  child: Text('Cancelar'),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                ),
                FlatButton(
                  padding: EdgeInsets.all(2.0),
                  child: Row(
                    children: <Widget>[
                      Text(
                        'Confirmar',
                        style: TextStyle(color: Colors.white),
                      ),
                    ],
                  ),
                  onPressed: () async {
                    setState(() {
                      listaFiltrada = new List<Produtos>();
                      if (filtrarCultura == true) {
                        widget.listaTotal.forEach((f) {
                          if (f.seleciona) listaFiltrada.add(f.item);
                        });
                      }
                      if ((filtrarData == false || filtrarData == null) &&
                          listaFiltrada.isEmpty) {
                        MainPage.filtroAnalise = new FiltroProcesso.vazio();
                      } else {
                        MainPage.filtroAnalise = new FiltroProcesso(
                          filtrarData,
                          dataInicial,
                          dataFinal,
                          listaFiltrada,
                          //null,
                          filtrarCultura,
                        );
                      }
                      Navigator.pop(context);
                    });
                  },
                ),
              ],
            )
          ],
        )));
    Widget dialogChild = new ConstrainedBox(
      constraints: const BoxConstraints(minWidth: 280.0),
      child: new Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: body,
      ),
    );

    if (label != null)
      dialogChild = new Semantics(
        namesRoute: true,
        label: label,
        child: dialogChild,
      );
    return new Dialog(child: dialogChild);
  }

  List<Widget> listadoFiltro() {
    List<Widget> lista = new List<Widget>();
    if (widget.listaTotal.isNotEmpty || widget.listaTotal == null) {
      widget.listaTotal.forEach((l) {
        lista.add(
          Container(
              decoration: BoxDecoration(
                  border: Border.all(color: Colors.black),
                  borderRadius: BorderRadius.circular(10.0),
                  color: l.seleciona ? Tema.corPrimaria : Colors.white),
              child: new FlatButton(
                child: Text(
                  l.item.nomIte,
                  style: TextStyle(
                      color: l.seleciona ? Colors.white : Colors.black),
                ),
                onPressed: () {
                  setState(() {
                    l.seleciona = !l.seleciona;
                  });
                },
              )),
        );
      });
    }
    return lista;
  }
}
