import 'package:flutter/material.dart';


class FaceButton extends StatelessWidget {
  FaceButton({Key key, this.text, this.height, this.onPressed}) : super(key: key);

  final String text;
  final double height;
  final VoidCallback onPressed;

  @override
  Widget build(BuildContext context) {
    return new ConstrainedBox(
      constraints: BoxConstraints.expand(height:64.0 , width:327.0 ),
      child: new RaisedButton(
          child: new ListTile(
             title: Text(text, style: new TextStyle(color: Colors.white,fontSize: 20.0)),
             leading: Image.asset('images/facebookIcon.png', width:(40.0) ,) ,
          ),  
          shape: new RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(45.0)),
          color: Color(0xff3B5999),
          onPressed: onPressed),
    );
  }
}