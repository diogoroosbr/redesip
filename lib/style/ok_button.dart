import 'package:flutter/material.dart';
import 'package:redesipapp/style/theme.dart';

class OkButton extends StatelessWidget {
  OkButton({Key key, this.height, this.onPressed, this.icon}) : super(key: key);

  final Icon icon;
  final double height;
  final VoidCallback onPressed;

  @override
  Widget build(BuildContext context) {
    return new ConstrainedBox(
      constraints: BoxConstraints.expand(height: 50.0, width: 200.0),
      child: new RaisedButton(
          child: const Icon(
            Icons.done,
            color: Colors.white,
            size: 50.0,
          ),
          shape: new RoundedRectangleBorder(
              borderRadius: BorderRadius.horizontal()),
          color: Tema.corPrimaria,
          onPressed: onPressed),
    );
  }
}

/* 
  Widget build(BuildContext context) {
    return new RaisedButton(
      
      child: new Text(text, style: new TextStyle(color: Colors.white, fontSize: 20.0)),
      shape: new RoundedRectangleBorder(
          borderRadius: BorderRadius.horizontal()),
      color: Colors.blue,
      textColor: Colors.black87,
      onPressed: onPressed);
  } */
