import 'package:flutter/material.dart';

class Tema {
  //static const corPrimaria = const Color.fromRGBO(85, 130, 0, 1.0);
  static const corPrimaria = Color.fromRGBO(0, 50, 76, 1.0);
  static const swatchPrimario = Colors.blueGrey;
  //static const swatchPrimario = Colors.lightGreen;
  //static const corSegundaria = const Color.fromRGBO(190, 190, 0, 1.0);
  static const corSegundaria = const Color.fromRGBO(194, 0, 0, 1.0);
  static const corAlerta = Colors.greenAccent;
  static const corCard = Colors.white70;
  static const corPicker = Colors.orangeAccent;
  static const corDesativ = Colors.grey;

  static const TextStyle estiloAlerta =
      TextStyle(color: corPrimaria, fontSize: 15.0);
}
