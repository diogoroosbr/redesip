import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:redesipapp/classes.dart';
import 'package:redesipapp/style/theme.dart'; //d_view.dart';

class DialogFiltro extends StatefulWidget {
  DialogFiltro({
    Key key,
    this.title,
    this.titlePadding: const EdgeInsets.fromLTRB(24.0, 24.0, 24.0, 0.0),
    this.contentPadding: const EdgeInsets.fromLTRB(0.0, 12.0, 0.0, 16.0),
    this.semanticLabel,
  })  : assert(titlePadding != null),
        assert(contentPadding != null),
        super(key: key);

  /// The (optional) title of the dialog is displayed in a large font at the top
  /// of the dialog.
  ///
  /// Typically a [Text] widget.
  final Widget title;

  /// Padding around the title.
  ///
  /// If there is no title, no padding will be provided.
  ///
  /// By default, this provides the recommend Material Design padding of 24
  /// pixels around the left, top, and right edges of the title.
  ///
  /// See [contentPadding] for the conventions regarding padding between the
  /// [title] and the [children].
  final EdgeInsetsGeometry titlePadding;

  /// The (optional) content of the dialog is displayed in a
  /// [SingleChildScrollView] underneath the title.
  ///
  /// Typically a list of [SimpleDialogOption]s.

  /// Padding around the content.
  ///
  /// By default, this is 12 pixels on the top and 16 pixels on the bottom. This
  /// is intended to be combined with children that have 24 pixels of padding on
  /// the left and right, and 8 pixels of padding on the top and bottom, so that
  /// the content ends up being indented 20 pixels from the title, 24 pixels
  /// from the bottom, and 24 pixels from the sides.
  ///
  /// The [SimpleDialogOption] widget uses such padding.
  ///
  /// If there is no [title], the [contentPadding] should be adjusted so that
  /// the top padding ends up being 24 pixels.
  final EdgeInsetsGeometry contentPadding;

  /// The semantic label of the dialog used by accessibility frameworks to
  /// announce screen transitions when the dialog is opened and closed.
  ///
  /// If this label is not provided, a semantic label will be infered from the
  /// [title] if it is not null.  If there is no title, the label will be taken
  /// from [MaterialLocalizations.dialogLabel].
  ///
  /// See also:
  ///
  ///  * [SemanticsConfiguration.isRouteName], for a description of how this
  ///    value is used.
  final String semanticLabel;

  @override
  DialogFiltroState createState() => new DialogFiltroState();
}

class DialogFiltroState extends State<DialogFiltro> {
  static bool aplicarCancelarC = false;
  List<FiltroLocal> listaCulturaFiltroPopUp = new List<FiltroLocal>();
  static List<FiltroLocal> listaCulturaFiltro = new List<FiltroLocal>();
  @override
  void initState() {
    super.initState();
    if (listaCulturaFiltro.isEmpty) {
      Firestore.instance.collection('produtos').getDocuments().then((c) {
        setState(() {
          c.documents.map((item) {
            listaCulturaFiltro.add(new FiltroLocal(
                new Produtos(
                    item.documentID,
                    item.data['codigo'],
                    item.data['nome'],
                    item.data['preco'],
                    item.data['decimais'],
                    item.data['deletado'],
                    0),
                false));
            listaCulturaFiltroPopUp.add(new FiltroLocal(
                new Produtos(
                    item.documentID,
                    item.data['codigo'],
                    item.data['nome'],
                    item.data['preco'],
                    item.data['decimais'],
                    item.data['deletado'],
                    0),
                false));
          }).toList();
        });
      });
    }
    listaCulturaFiltro.forEach((filtro) {
      listaCulturaFiltroPopUp
          .add(new FiltroLocal(filtro.item, filtro.seleciona));
    });
  }

  @override
  Widget build(BuildContext context) {
    final List<Widget> body = <Widget>[];
    String label = widget.semanticLabel;

    if (widget.title != null) {
      body.add(new Padding(
          padding: widget.titlePadding,
          child: new DefaultTextStyle(
            style: Theme.of(context).textTheme.title,
            child: new Semantics(
                namesRoute: true,
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      widget.title,
                      FlatButton(
                        disabledTextColor: Colors.white,
                        textColor: Tema.corAlerta,
                        child: Text(
                          "Limpar",
                          textScaleFactor: 1.2,
                        ),
                        onPressed: filtroVazio()
                            ? () {
                                setState(() {
                                  limpaFiltro();
                                });
                              }
                            : null,
                      ),
                    ])),
          )));
    } else {
      switch (defaultTargetPlatform) {
        case TargetPlatform.iOS:
          label = widget.semanticLabel;
          break;
        case TargetPlatform.android:
        case TargetPlatform.fuchsia:
          label = widget.semanticLabel ??
              MaterialLocalizations.of(context)?.dialogLabel;
      }
    }

    /*   body.add(new Flexible(
          child: new SingleChildScrollView(
        padding: widget.contentPadding,
        child: new ListBody(children: listadoFiltro()),
      ))); */
    body.add(Padding(
      padding: EdgeInsets.all(10.0),
    ));

    body.add(Expanded(
        child: listaCulturaFiltroPopUp.isEmpty
            ? new Center(
                child: new Container(
                    height: 100.0,
                    width: 100.0,
                    child: new CircularProgressIndicator()))
            : StaggeredGridView.countBuilder(
                padding: EdgeInsets.all(2.0),
                crossAxisCount: 3,
                itemCount: listaCulturaFiltroPopUp.length,
                itemBuilder: (context, i) => new Container(
                    margin: EdgeInsets.all(2.0),
                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.black, width: 1.3),
                        borderRadius: BorderRadius.circular(10.0),
                        color: listaCulturaFiltroPopUp[i].seleciona
                            ? Tema.corPrimaria
                            : Colors.white),
                    child: new FlatButton(
                      child: Text(
                        listaCulturaFiltroPopUp[i].item.nomIte,
                        style: TextStyle(
                            color: listaCulturaFiltroPopUp[i].seleciona
                                ? Colors.white
                                : Colors.black),
                      ),
                      onPressed: () {
                        setState(() {
                          listaCulturaFiltroPopUp[i].seleciona =
                              !listaCulturaFiltroPopUp[i].seleciona;
                        });
                      },
                    )),
                staggeredTileBuilder: (int index) => new StaggeredTile.count(
                    listaCulturaFiltroPopUp[index].item.nomIte.length <= 8
                        ? 1
                        : 2,
                    0.5),
              )));

    body.add(Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: <Widget>[
        FlatButton(
          textColor: Tema.corSegundaria,
          child: Text(
            'Cancelar',
            textScaleFactor: 1.2,
          ),
          onPressed: () {
            aplicarCancelarC = aplicarCancelar(false);
            Navigator.of(context).pop(false);
          },
        ),
        FlatButton(
          textColor: Tema.corPrimaria,
          child: Text(
            'Aplicar',
            textScaleFactor: 1.2,
          ),
          onPressed: () {
            aplicarCancelarC = aplicarCancelar(true);
            listaCulturaFiltro.clear();
            listaCulturaFiltroPopUp.forEach((f) {
              listaCulturaFiltro.add(new FiltroLocal(f.item, f.seleciona));
            });
            Navigator.of(context).pop(false);
          },
        )
      ],
    ));

    Widget dialogChild = new ConstrainedBox(
      constraints: const BoxConstraints(minWidth: 280.0, maxHeight: 510.0),
      child: new Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: body,
      ),
    );

    if (label != null)
      dialogChild = new Semantics(
        namesRoute: true,
        label: label,
        child: dialogChild,
      );
    return new Dialog(child: dialogChild);
  }

  void limpaFiltro() {
    listaCulturaFiltroPopUp.forEach((f) {
      f.seleciona = false;
    });
  }

  static bool aplicarCancelar(bool value) {
    return value;
  }

  bool filtroVazio() {
    bool retorno;
    int cont = 0;
    listaCulturaFiltroPopUp.forEach((f) {
      if (f.seleciona) cont++;
    });
    cont > 0 ? retorno = true : retorno = false;
    return retorno;
  }

  List<Widget> listadoFiltro() {
    List<Widget> lista = new List<Widget>();
    if (listaCulturaFiltroPopUp.isNotEmpty) {
      listaCulturaFiltroPopUp.forEach((l) {
        lista.add(new ListTile(
          title: Text(l.item.nomIte),
          leading: new Checkbox(
            value: l.seleciona,
            activeColor: Colors.amber,
            onChanged: (bool valor) {},
          ),
          onTap: () {
            setState(() {
              l.seleciona = !l.seleciona;
            });
          },
        ));
      });
    }
    return lista;
  }
}
