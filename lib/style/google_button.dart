import 'package:flutter/material.dart';


class GoogleButton extends StatelessWidget {
  GoogleButton({Key key, this.text, this.height, this.onPressed}) : super(key: key);

  final String text;
  final double height;
  final VoidCallback onPressed;

  @override
  Widget build(BuildContext context) {
    return new ConstrainedBox(
      constraints: BoxConstraints.expand(height:64.0 , width:327.0 ),
      child: new RaisedButton(
          child: new ListTile(
             title: Text(text, style: new TextStyle(color: Colors.grey,fontSize: 20.0)),
             leading: Image.asset('images/googleIcon.png', width:(50.0)) ,
          ),  
          shape: new RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(45.0)),
          color: Color(0xffffffff),
          onPressed: onPressed),
    );
  }
}