import 'package:flutter/material.dart';
import 'package:redesipapp/style/theme.dart'; //l.dart';

class PrimaryButton extends StatelessWidget {
  PrimaryButton({this.text, this.height, this.onPressed, this.icon, this.cor});

  final Color cor;
  final Icon icon;
  final String text;
  final double height;
  final VoidCallback onPressed;

  @override
  Widget build(BuildContext context) {
    return new ConstrainedBox(
      constraints: BoxConstraints.expand(height: 50.0, width: 150.0),
      child: new RaisedButton(
          child: text == null
              ? icon
              : new Text(text,
                  style: new TextStyle(color: Colors.white, fontSize: 23.0)),
          shape: new RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(15.0)),
          color: cor != null ? cor : Tema.corPrimaria,
          onPressed: onPressed),
    );
  }
}

/*
  Widget build(BuildContext context) {
    return new RaisedButton(

      child: new Text(text, style: new TextStyle(color: Colors.white, fontSize: 20.0)),
      shape: new RoundedRectangleBorder(
          borderRadius: BorderRadius.horizontal()),
      color: Colors.blue,
      textColor: Colors.black87,
      onPressed: onPressed);
  } */
