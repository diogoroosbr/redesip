import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:redesipapp/autenticacao/baseAuth.dart';
import 'package:redesipapp/pages/login/loginUsuario.dart';
import 'package:redesipapp/pages/mainPage.dart';
import 'package:redesipapp/pages/usuario/insereDadosPessoais.dart';

class RootPage extends StatefulWidget {
  RootPage({Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => new _RootPageState();
}

class _RootPageState extends State<RootPage> {
  //Firestore db = Firestore.instance;
  FirebaseUser fu; //Preenchido depois da chamada do getFirUsr
  String emaUsu = "";
  String priNom = "";
  String ultNom = "";
  String docIde = "";
  String ultAce = "";
  String temCad = "";
  String strPag = "";

  Future<FirebaseUser> getFirUsr() async {
    await Auth.currentUser().then((usuario) {
      if ((usuario != null)) {
        fu = usuario;
        strPag = "home";
      } else {
        strPag = "login";
      }
    });
    return fu;
  }

  /// Confere se o usuário que logou anteriormente (se for o caso) tem dados
  /// pessoais preenchidos. Se não logou, armazena apenas o último acesso (se
  /// for o caso) para decidir se mostra ou não o splashscreen
  Future<String> getUltAce() async {
    // Mesmo não tendo dados na memória (troca de celular), pode ter no banco
    await Firestore.instance
        .collection('usuarios')
        .where('EMAUSU', isEqualTo: fu.email)
        .getDocuments()
        .then((ideDoc) {
      bool temCad2 = ideDoc.documents.isEmpty;
      temCad = "N";
      if (!temCad2) {
        temCad = "S";
        docIde = ideDoc.documents.first.documentID;
        emaUsu = ideDoc.documents.first.data['EMAUSU'];
        priNom = ideDoc.documents.first.data['PRINOM'];
        ultNom = ideDoc.documents.first.data['ULTNOM'];
        ultAce = ideDoc.documents.first.data['ULTACE'];
      }
    });
    return ultAce;
  }

  @override
  Widget build(BuildContext context) {
    print('start page: ' + strPag + ' - temCad: ' + temCad);
    Scaffold(
      backgroundColor: Colors.white,
    );
    if (strPag.contains("home")) {
      if (temCad.contains("N")) {
        print('vaiPara InsereDados');
        return InsereDadosPage(
          ideUsu: fu,
          isEditing: false,
        );
      }
      if (temCad.contains("S")) {
        print('vaiPara MainApp');
        return new MainApp(
          idUsuario: docIde,
        );
      }
    }
    if (strPag.contains("login") && temCad.contains("N")) {
      print('vaiPara Login');
      return new LoginUsuarioPage();
    } else if (strPag.contains("login") && temCad.contains("S")) {
      print('vaiPara PaginaBranca');
      return Scaffold(
        backgroundColor: Colors.white,
      );
    }
  }

  @override
  initState() {
    super.initState();
    emaUsu = "";
    priNom = "";
    ultNom = "";
    docIde = "";
    ultAce = "";
    temCad = "";
    strPag = "";

    // Confere se tem registro (já acessou) do usuário no dispositivo
    getFirUsr().whenComplete(() async {
      if (fu != null && fu.email != null) {
        //Se já acessou (tem registro), confere se tem dados pessoais
        await getUltAce().then((x) async {
          String dt = new DateFormat('dd/MM/yyyy').format(new DateTime.now());
          if (x == null || x.length <= 1 || !x.contains(dt)) {
            if (x == null || x.length <= 1) {
              temCad = "N";
            }
            if (x.length > 2 && !x.contains(dt)) {
              temCad = "S";
            }
            // Tem dados pessoais mas o último acesso é vazio ou <= ontem
            /*showFutureDialog(
                context: context,
                child: new Scaffold(
                  backgroundColor: Colors.white,
                  body: new Center(
                    child: new Image.asset('images/Mediador.png'),
                  ),
                ),
                future: new Future.delayed(const Duration(seconds: 2))
                    .whenComplete(() {
                  setState(() {});
                }));*/

          }
          if (x != null && x.contains(dt)) {
            // Pula o splash e vai direto para o Main
            temCad = "S";
          }

          // Se tem dados pessoais e o último acesso foi hoje, não mostra splash
        });
      } else {
        // Se nunca acessou no dispositivo, mostra o splash e define a página
        // para login
        temCad = "N";
        /*showFutureDialog(
            context: context,
            child: new Scaffold(
              backgroundColor: Colors.white,
              body: new Center(
                child: new Image.asset('images/Mediador.png'),
              ),
            ),
            future:
                new Future.delayed(const Duration(seconds: 2)).whenComplete(() {
              setState(() {});
            }));*/
      }
    });

    //Verifica se o usuário já teve acesso (offline) no dispositivo.
    //Em caso negativo, irá a tela de login no Widget
    /*getUsuAtu().then((e) {
      emaUsu = e;

    });*/
  }

  var normal = new TextStyle(
    color: Colors.white,
    //fontWeight: FontWeight.w800,
    fontFamily: 'Roboto',
    fontSize: 16.0,
    fontWeight: FontWeight.normal,
  );

  var skip = new TextStyle(
    color: Colors.lightBlueAccent,
    //fontWeight: FontWeight.w800,
    fontFamily: 'Roboto',
    fontSize: 18.0,
    fontWeight: FontWeight.normal,
  );

  Future<String> addPar() async {
    Firestore.instance
        .collection("E010PAR")
        .document("") //nomUsu)
        .setData({'PRINOM': 'Sicrano', 'SOBNOM': 'Oliveira'})
        .then((docRef) {})
        .catchError((error) {
          print("Error adding document: " + error);
        });
    return "";
  }

  Future<String> setPar() async {
    Transaction transaction = new Transaction(0, Firestore.instance);
    DocumentReference ref =
        Firestore.instance.collection('usuarios').document('E010TIP');
    DocumentSnapshot documentSnapshot = await ref.get();
    Map<String, dynamic> data = documentSnapshot.data;
    data['key2'] = 'val2';
    await transaction.set(ref, data);
    return "";
  }
}
