import 'dart:async';

import 'package:flutter/material.dart';
import 'package:redesipapp/autenticacao/baseAuth.dart';
import 'package:redesipapp/pages/login/cadastro.dart';
import 'package:redesipapp/servicos.dart';
import 'package:redesipapp/style/primary_button.dart';
import 'package:redesipapp/style/second_button.dart';
import 'package:redesipapp/style/theme.dart';

/// Autor(es): Diogo Roosarnezi
/// Criação: 08/08
/// Função: Cria a tela de Login do Usuário

String msgErr = '';

class LoginUsuarioPage extends StatelessWidget {
  LoginUsuarioPage({Key key}) : super(key: key);
  static const String routeName = "/loginUsuario";

  @override
  Widget build(BuildContext contexto) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 0.0,
        ),
        backgroundColor: Colors.white,
        body: Center(
            child: SingleChildScrollView(
                child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            new Image.asset('images/redesip.png', width: (150.0)),
            new Text(
              'Rede SIP CRM',
              style: TextStyle(
                  fontSize: 22, decorationStyle: TextDecorationStyle.double),
            ),
            LoginForm()
          ],
        ))));
  }
}

class LoginForm extends StatefulWidget {
  @override
  LoginFormState createState() {
    return LoginFormState();
  }
}

class LoginFormState extends State<LoginForm> {
  final _formKey = GlobalKey<FormState>();

  String _email;
  String _password;
  String documentId;

  final FocusNode _emaFocus = FocusNode();
  final FocusNode _senFocus = FocusNode();

  _fieldFocusChange(
      BuildContext contexto, FocusNode currentFocus, FocusNode nextFocus) {
    currentFocus.unfocus();
    FocusScope.of(contexto).requestFocus(nextFocus);
  }

  Future<Null> _prcFrm(BuildContext c) async {
    try {
      if (_formKey.currentState.validate()) {
        _formKey.currentState.save();
        Scaffold.of(c).showSnackBar(SnackBar(
            content: Text('Realizando login'), duration: Duration(seconds: 2)));
        await Servicos.logaPrograma(c, TipoLogin.comum, _email, _password)
            .then((e) {
          Scaffold.of(c).showSnackBar(SnackBar(
            content: Text(e),
            duration: Duration(seconds: 4),
          ));
        });
      }
    } catch (e) {}
  }

  @override
  Widget build(BuildContext contexto) {
    return Container(
        margin: EdgeInsets.all(20.0),
        child: Form(
          key: _formKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(bottom: 10),
                child: TextFormField(
                  textCapitalization: TextCapitalization.none,
                  textInputAction: TextInputAction.next,
                  focusNode: _emaFocus,
                  onFieldSubmitted: (term) {
                    _fieldFocusChange(contexto, _emaFocus, _senFocus);
                  },
                  autofocus: false,
                  decoration: new InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Email',
                      errorStyle: Tema.estiloAlerta),
                  validator: (value) =>
                      value.isEmpty ? 'E-mail Obrigatório' : null,
                  onSaved: (value) => _email = value,
                ),
              ),
              Padding(
                padding: EdgeInsets.only(bottom: 10),
                child: TextFormField(
                  textCapitalization: TextCapitalization.none,
                  textInputAction: TextInputAction.done,
                  focusNode: _senFocus,
                  onFieldSubmitted: (term) {
                    _prcFrm(contexto);
                  },
                  decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Senha',
                      errorStyle: Tema.estiloAlerta),
                  obscureText: true,
                  validator: (value) =>
                      value.isEmpty ? 'Senha Obrigatória' : null,
                  onSaved: (value) => _password = value,
                ),
              ),
              padded(
                child: Column(
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      mainAxisSize: MainAxisSize.max,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        SecondButton(
                          onPressed: () => Navigator.push(
                                contexto,
                                MaterialPageRoute(
                                    builder: (contexto) => CadastroPage()),
                              ),
                          text: 'Cadastrar',
                        ),
                        Padding(
                          padding: EdgeInsets.only(right: 5.0),
                        ),
                        PrimaryButton(
                          onPressed: () => _prcFrm(contexto),
                          text: 'Entrar',
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ));
  }
}

Widget padded({Widget child}) {
  return new Padding(
    padding: EdgeInsets.symmetric(vertical: 8.0),
    child: child,
  );
}

Widget contained({Widget child}) {
  return new Container(
    padding: EdgeInsets.symmetric(vertical: 8.0),
    alignment: Alignment.center,
    child: child,
  );
}
