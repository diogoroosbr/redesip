import 'package:flutter/material.dart';
import 'package:redesipapp/autenticacao/baseAuth.dart';
import 'package:redesipapp/pages/login/cadastro.dart';
import 'package:redesipapp/pages/login/loginUsuario.dart';
import 'package:redesipapp/servicos.dart';
import 'package:redesipapp/style/google_button.dart';
import 'package:redesipapp/style/primary_button.dart';

String msgErr = '';

/// Autor(es): Diogo Roosernando Farnezi Brito
/// Criação:
/// Função: Cria a tela inicial do programa para o usuário que não está logado
/// e o direciona para as telas de cadastro ou de Login.
/// Parâmetros: auth, title, onSignIN

class LoginPageScaffold2 extends StatelessWidget {
  LoginPageScaffold2({Key key}) : super(key: key);
  static const String routeName = "/login";

  final String title = 'Login';

  @override
  Widget build(BuildContext contexto) {
    print('aqui');
    return new Scaffold(
      body: LoginPage2(),
      backgroundColor: Colors.white, //Colors.red[300],
    );
  }
}

class LoginPage2 extends StatefulWidget {
  LoginPage2({Key key}) : super(key: key);
  static const String routeName = "/login";

  final String title = 'Login';

  @override
  _LoginPageState2 createState() => new _LoginPageState2();
}

class _LoginPageState2 extends State<LoginPage2> {
  @override
  Widget build(BuildContext contexto) {
    return OrientationBuilder(
      builder: (context, orientation) {
        return GridView.count(
            // Create a grid with 2 columns in portrait mode, or 3 columns in
            // landscape mode.
            crossAxisCount: orientation == Orientation.portrait ? 1 : 1,
            // Generate 100 Widgets that display their index in the List
            children: <Widget>[
              //new Image.asset('images/Mediador.png', width: (400.0)),

              new Container(
                  padding: const EdgeInsets.all(4.0),
                  child: new Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: botoesIniciais(contexto)))
            ]);
      },

      /*new Center(
            child: new Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
          new Image.asset('images/Mediador.png', width: (400.0)),
          new Container(
              padding: const EdgeInsets.all(4.0),
              child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: botoesIniciais(contexto))),
        ]))*/
    );
  }

  /// Autor: Diogo Roos
  /// Função: Constrói os botões que aparecem na primeira tela

  List<Widget> botoesIniciais(BuildContext contexto) {
    return [
      Container(
        alignment: Alignment.center,
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            new Text(
              'Rede SIP CRM',
              style: TextStyle(
                  fontSize: 28, decorationStyle: TextDecorationStyle.double),
            ),
            new Container(
                padding: EdgeInsets.only(top: 20.0),
                child: new PrimaryButton(
                  text: ('Login'),
                  onPressed: () {
                    Navigator.push(
                      contexto,
                      new MaterialPageRoute(
                          builder: (contexto) => LoginUsuarioPage()),
                    );
                  },
                )),
            contained(
                child: new PrimaryButton(
              text: ('Cadastrar'),
              onPressed: () {
                Navigator.push(
                  contexto,
                  MaterialPageRoute(builder: (contexto) => CadastroPage()),
                );
              },
            )),
          ],
        ),
      ),
      Center(
          child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            width: 130.0,
            height: 2.0,
            child: new Divider(
              height: 2.0,
              color: Colors.black,
            ),
          ),
          Container(
              margin: EdgeInsets.fromLTRB(10.0, 0.0, 10.0, 0.0),
              child: new Text(
                'Ou',
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.black,
                    fontSize: 20.0),
                textAlign: TextAlign.center,
              )),
          Container(
            width: 130.0,
            child: new Divider(
              color: Colors.black,
            ),
          ),
        ],
      )),
      contained(
        child: new GoogleButton(
            text: ('Entrar com o Google'),
            height: 64.0,
            onPressed: () {
              Servicos.logaPrograma(contexto, TipoLogin.google, '', '')
                  .then((e) {
                if ((e != null) && (e.contains("badly formatted"))) {
                  Scaffold.of(contexto).showSnackBar(SnackBar(
                    content: Text('Formato de e-mail inválido.'),
                    duration: Duration(seconds: 10),
                  ));
                }
                if ((e != null) && (e.contains("servInvalid"))) {
                  Scaffold.of(contexto).showSnackBar(SnackBar(
                    content: Text(
                        'Falha na comunicação com Google. Por favor, tente mais tarde.'),
                    duration: Duration(seconds: 10),
                  ));
                }
                if ((e != null) && (e.contains("incorreto"))) {
                  Scaffold.of(contexto).showSnackBar(SnackBar(
                    content: Text('Dados inválidos.'),
                    duration: Duration(seconds: 4),
                  ));
                }
                if ((e != null) && (e.contains("done"))) {
                  Scaffold.of(contexto).showSnackBar(SnackBar(
                    content: Text('Acessando...'),
                    duration: Duration(seconds: 14),
                  ));
                }
              });
              setState(() {});
            }),
      ),
      /*contained(
          child: new FaceButton(
              text: ('Entrar com o Facebook'),
              height: 64.0,
              onPressed: () {
                Servicos.logaPrograma(contexto, TipoLogin.facebook, '', '');
                setState(() {});
              })),*/
    ];
  }

  Widget contained({Widget child}) {
    return new Container(
      padding: EdgeInsets.symmetric(vertical: 8.0),
      alignment: Alignment.center,
      child: child,
    );
  }
}
