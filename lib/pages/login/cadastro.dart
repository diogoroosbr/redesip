import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:redesipapp/autenticacao/baseAuth.dart';
import 'package:redesipapp/pages/usuario/insereDadosPessoais.dart';
import 'package:redesipapp/style/primary_button.dart';
import 'package:redesipapp/style/theme.dart';

/// Autor(es): Diogo Roos
/// Criação: 02/08/2018
/// Função: Cria a pagina de cadastro de usuario do firebase
/// Parâmetros: auth(baseauth)

class CadastroPage extends StatefulWidget {
  CadastroPage({Key key}) : super(key: key);
  static const String routeName = "/cadastro";
  @override
  _CadastroPageState createState() => new _CadastroPageState();
}

class _CadastroPageState extends State<CadastroPage> {
  final chaveScaf = new GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(
          elevation: 0.0,
          backgroundColor: Colors.white,
        ),
        body: Center(
            child: SingleChildScrollView(
                child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            //new Image.asset('images/Mediador.png', width: (150.0)),
            CadastroForm()
          ],
        ))));
  }
}

class CadastroForm extends StatefulWidget {
  @override
  CadastroFormState createState() {
    return CadastroFormState();
  }
}

class CadastroFormState extends State<CadastroForm> {
  final _formKeyCad = GlobalKey<FormState>();

  String _email;
  String _passwordTempo;
  String _password;

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.all(20.0),
        child: Form(
          key: _formKeyCad,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              new Image.asset(
                'images/redesip.png',
                width: (150.0),
              ),
              new Text(
                'Rede SIP CRM',
                style: TextStyle(
                    fontSize: 22, decorationStyle: TextDecorationStyle.double),
              ),
              Padding(
                padding: EdgeInsets.only(bottom: 10, top: 10),
                child: TextFormField(
                  decoration: new InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Email',
                      errorStyle: Tema.estiloAlerta),
                  validator: (value) =>
                      value.isEmpty ? 'E-mail Obrigatório' : null,
                  onSaved: (value) => _email = value.trim(),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(bottom: 10),
                child: TextFormField(
                  key: new Key('password'),
                  decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Senha',
                      errorStyle: Tema.estiloAlerta),
                  obscureText: true,
                  validator: (value) {
                    if (value.isEmpty) return 'Senha Obrigatória';
                    _passwordTempo = value;
                  },
                  onSaved: (value) => _password = value,
                ),
              ),
              Padding(
                padding: EdgeInsets.only(bottom: 10),
                child: TextFormField(
                  decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Confirmar Senha',
                      errorStyle: Tema.estiloAlerta),
                  obscureText: true,
                  validator: (value) {
                    if (value != _passwordTempo)
                      return 'As senhas devem ser equivalentes';
                  },
                  onSaved: (value) => _password = value,
                ),
              ),
              padded(
                  child: Column(
                children: <Widget>[
                  PrimaryButton(
                    onPressed: () {
                      try {
                        if (_formKeyCad.currentState.validate()) {
                          _formKeyCad.currentState.save();
                          Firestore.instance
                              .collection('usuarios')
                              .where('usuario', isEqualTo: _email)
                              .getDocuments()
                              .then((contagem) {
                            if (contagem.documents.isEmpty) {
                              Auth.createUser(_email, _password).then((user) {
                                Navigator.pushAndRemoveUntil(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => InsereDadosPage(
                                              ideUsu: user,
                                              isEditing: false,
                                            )),
                                    ((Route<dynamic> route) => false));
                              }).catchError((e) {
                                if ((e is PlatformException)) {
                                  if ((e as PlatformException)
                                      .message
                                      .contains(" 6 ")) {
                                    Scaffold.of(context).showSnackBar(SnackBar(
                                        content: Text(
                                            'Erro: a senha deve ter no mínimo 6 caracteres'),
                                        duration: Duration(seconds: 5)));
                                  }
                                  if ((e as PlatformException)
                                      .message
                                      .contains("already in use")) {
                                    Scaffold.of(context).showSnackBar(SnackBar(
                                        content: Text(
                                            'Erro: e-mail já está registrado'),
                                        duration: Duration(seconds: 5)));
                                  }
                                } else {
                                  Scaffold.of(context).showSnackBar(SnackBar(
                                      content: Text(
                                          'Entre em contato e informe a mensagem: ' +
                                              (e as PlatformException).message),
                                      duration: Duration(seconds: 5)));
                                }
                              });
                            } else {
                              Scaffold.of(context).showSnackBar(SnackBar(
                                  content: Text(
                                      'E-mail já está cadastrado em outra conta'),
                                  duration: Duration(seconds: 5)));
                            }
                          });
                          Scaffold.of(context).showSnackBar(SnackBar(
                            content: Text('Realizando cadastro'),
                            duration: Duration(seconds: 2),
                          ));
                        }
                      } catch (e) {
                        Scaffold.of(context).showSnackBar(SnackBar(
                            content:
                                Text('Usuário não encontrado, ou incorreto')));
                      }
                    },
                    text: 'Cadastrar',
                  ),
                ],
              )),
            ],
          ),
        ));
  }

  Widget padded({Widget child}) {
    return new Padding(
      padding: EdgeInsets.symmetric(vertical: 8.0),
      child: child,
    );
  }
}
