import 'dart:core';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:redesipapp/classes.dart';
import 'package:redesipapp/servicos.dart';
import 'package:redesipapp/style/primary_button.dart';
//servicos.dart';

/// Autor(es): Diogo Roos
/// Criação: 03/08/2018
/// Função: Editar e marcar como deletado uma análise já cadastrada
/// Parâmetros: Análise selecionado na page listaAnalise

class EditarPagamentoPage extends StatefulWidget {
  EditarPagamentoPage(
      {Key key, this.pagamentoSelecionado /*, this.localSelecionado*/});
  static const String routeName = "/editarPagamentoPage";
  final Pagamentos pagamentoSelecionado;
  //final Local localSelecionado;
  @override
  EditarPagamentoState createState() {
    EditarPagamentoState.prcSel = pagamentoSelecionado;
    //EditarAnaliseState.locSel = localSelecionado;
    return EditarPagamentoState();
  }
}

class EditarPagamentoState extends State<EditarPagamentoPage> {
  final _formKey = GlobalKey<FormState>();
  bool isSaving = false;
  static Pagamentos prcSel;

  List<String> indDel = ['Não', 'Sim'];

  String indDelSel = prcSel != null ? prcSel.deletado ? 'Sim' : 'Não' : 'Não';

  final nomParCtr = TextEditingController(
      text:
          '' /*prcSel != null && prcSel.nomPar.length > 0 ? prcSel.nomPar : ''*/
      );

  final nomeCtrl =
      TextEditingController(text: prcSel != null ? prcSel.desPag : "");

  MoneyMaskedTextController percentualCtrl = prcSel != null
      ? MoneyMaskedTextController(
          initialValue: prcSel.percentual != 0 ? prcSel.percentual : 0.0,
        )
      : new MoneyMaskedTextController();

  final FocusNode _nomeFoco = FocusNode();
  final FocusNode _percentualFoco = FocusNode();

  _fieldFocusChange(
      BuildContext context, FocusNode currentFocus, FocusNode nextFocus) {
    currentFocus.unfocus();
    FocusScope.of(context).requestFocus(nextFocus);
  }

  @override
  void initState() {
    super.initState();

    if (prcSel != null) {
      if (prcSel.deletado) {
        indDelSel = "Sim";
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title:
            prcSel != null ? Text('Editar Pagamento') : Text('Novo Pagamento'),
      ),
      body: SingleChildScrollView(
        child: Container(
          key: _formKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Column(children: [formularioCompleto()]),
              _botaoConfirma(),
              prcSel == null ? Text('') : _botaoDeletar(),
            ],
          ),
        ),
      ),
    );
  }

  Widget _botaoDeletar() {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 16.0),
      child: PrimaryButton(
        icon: Icon(
          Icons.delete,
          color: Colors.white,
          size: 40.0,
        ),
        onPressed: () {
          showDialog(
            context: context,
            builder: (context) => AlertDialog(
                  title: new Text('Alerta'),
                  content: new Text('Deseja realmente apagar?'),
                  actions: <Widget>[
                    new FlatButton(
                      onPressed: () => Navigator.of(context).pop(),
                      child: new Text('Não'),
                    ),
                    new FlatButton(
                      onPressed: () async {
                        Pagamentos _analise = await Servicos.editPagamento(
                            new Pagamentos(
                                prcSel.idePrc,
                                prcSel.desPag,
                                prcSel.percentual,
                                indDelSel == "Não" ? false : true));

                        Navigator.of(context).pop();
                        Navigator.of(context).pop(_analise);
                      },
                      child: new Text('Sim'),
                    ),
                  ],
                ),
          );
        },
      ),
    );
  }

  Widget formularioCompleto() {
    return Column(children: <Widget>[
      new ListTile(
        leading: const Icon(Icons.account_box),
        title: TextFormField(
            enabled: prcSel != null ? false : true,
            controller: nomeCtrl,
            keyboardType: TextInputType.text,
            textInputAction: TextInputAction.next,
            textCapitalization: TextCapitalization.characters,
            focusNode: _nomeFoco,
            onFieldSubmitted: (term) {
              _fieldFocusChange(context, _nomeFoco, _percentualFoco);
            },
            decoration: InputDecoration(
                labelText: 'Descrição', labelStyle: TextStyle(fontSize: 18.0)),
            validator: (value) {
              if (value.isEmpty) {
                return 'Digite descrição do pagamento';
              }
            }),
      ),
      new ListTile(
        leading: const Icon(Icons.monetization_on),
        title: TextFormField(
          controller: percentualCtrl,
          keyboardType:
              TextInputType.numberWithOptions(decimal: true, signed: false),
          textInputAction: TextInputAction.done,
          focusNode: _percentualFoco,
          decoration: InputDecoration(labelText: 'Percentual de desconto'),
          validator: (value) {
            if (value.isEmpty) {
              return 'Informe o percentual de desconto';
            }
          },
        ),
      ),
      ListTile(
          leading: const Icon(Icons.delete_outline),
          title: DropdownButton(
              value: indDelSel,
              items: indDel.map((String val) {
                return new DropdownMenuItem<String>(
                  value: val,
                  child: new Text('Inativo:  ' + val),
                );
              }).toList(),
              onChanged: (newVal) {
                this.setState(() {
                  indDelSel = newVal;
                });
              })),
    ]);
  }

  Widget _botaoConfirma() {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 16.0),
      child: PrimaryButton(
        icon: Icon(
          Icons.done,
          color: Colors.white,
          size: 50.0,
        ),
        onPressed: isSaving ? null : () => checkAndSubmit(),
      ),
    );
  }

  void checkAndSubmit() async {
    setState(() {
      isSaving = true;
    });

    //Verifica se a condicação de pagamento foi selecionada
    if ((prcSel != null) && (prcSel.desPag.length <= 0)) {
      isSaving = false;
      showDialog(
        context: context,
        builder: (context) => new AlertDialog(
              title: new Text('Alerta'),
              content: new Text(
                  'Informe a descrição da condição (exemplo: 10D, 10/20/30D, etc)'),
              actions: <Widget>[
                new FlatButton(
                  onPressed: () => Navigator.of(context).pop(),
                  child: new Text('OK'),
                ),
              ],
            ),
      );
    } else if ((percentualCtrl.text.length <= 0) ||
        (double.parse(percentualCtrl.text.replaceAll(",", ".")) > 100)) {
      isSaving = false;
      showDialog(
        context: context,
        builder: (context) => new AlertDialog(
              title: new Text('Alerta'),
              content: new Text('Corrija o percentual'),
              actions: <Widget>[
                new FlatButton(
                  onPressed: () => Navigator.of(context).pop(),
                  child: new Text('OK'),
                ),
              ],
            ),
      );
    } else {
      //Criar novo negócio
      if (prcSel == null) {
        Pagamentos n = Pagamentos(
            '',
            nomeCtrl.text,
            double.parse(percentualCtrl.text.replaceAll(",", ".")),
            indDelSel == "Sim" ? true : false);
        Servicos.addPagamento(n).then((t) {
          if (t != null) {
            //Inseriu com sucesso
            Navigator.of(context).pop(t);
          }
        });
      }
      //Editar negócio existente
      if (prcSel != null) {
        //Abriu tela por um negócio existente
        Pagamentos n = Pagamentos(
            prcSel.idePrc,
            nomeCtrl.text,
            double.parse(percentualCtrl.text.replaceAll(",", ".")),
            indDelSel == "Sim" ? true : false);
        await Servicos.editPagamento(n).then((neg) {
          Navigator.of(context).pop(neg);
        });
      }
    }
  }
}
