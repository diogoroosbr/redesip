import 'dart:core';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_cupertino_date_picker/flutter_cupertino_date_picker.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:intl/intl.dart';
import 'package:redesipapp/classes.dart';
import 'package:redesipapp/servicos.dart';
import 'package:redesipapp/style/autocomplete_textfield.dart';
import 'package:redesipapp/style/primary_button.dart';
import 'package:redesipapp/uteis/data_table.dart';
//servicos.dart';

class EditarOfertaPage extends StatefulWidget {
  EditarOfertaPage({Key key, this.ofertaSelecionada /*, this.localSelecionado*/});
  static const String routeName = "/editarofertaPage";
  final Ofertas ofertaSelecionada;
  //final Local localSelecionado;
  @override
  EditarOfertaState createState() {
    EditarOfertaState.prcSel = ofertaSelecionada;
    return EditarOfertaState();
  }
}

class EditarOfertaState extends State<EditarOfertaPage> {
  final _formKey = GlobalKey<FormState>();
  bool isSaving = false;
  static Ofertas prcSel;

  List<String> indDel = ['Não', 'Sim'];

  String indDelSel = prcSel != null ? prcSel.deletado ? 'Sim' : 'Não' : 'Não';

  final nomParCtr = TextEditingController(text: '' /*prcSel != null && prcSel.nomPar.length > 0 ? prcSel.nomPar : ''*/
      );

  final nomeCtrl = TextEditingController(text: prcSel != null ? prcSel.desOfe : "");

  List<Produtos> lstIte = new List<Produtos>();
  List<OfertasItens> itensCarrinho = new List<OfertasItens>();
  Produtos objProduto;
  int idxRow;
  GlobalKey key = new GlobalKey<AutoCompleteTextFieldState<Produtos>>();
  AutoCompleteTextField<Produtos> autoCompleteItem = AutoCompleteTextField<Produtos>();

  DateTime dataInicio = new DateTime.now();
  final inicioCtrl = new MaskedTextController(mask: '00/00/0000');

  DateTime dataLimite = new DateTime.now();
  final vencimentoCtrl = new MaskedTextController(mask: '00/00/0000');

  MoneyMaskedTextController percentualCtrl = prcSel != null
      ? MoneyMaskedTextController(
          initialValue: prcSel.percentual != 0 ? prcSel.percentual : 0.0,
        )
      : new MoneyMaskedTextController();

  final FocusNode _nomeFoco = FocusNode();
  final FocusNode _percentualFoco = FocusNode();
  final FocusNode _inicioFoco = FocusNode();
  final FocusNode _vencimentoFoco = FocusNode();

  _fieldFocusChange(BuildContext context, FocusNode currentFocus, FocusNode nextFocus) {
    currentFocus.unfocus();
    FocusScope.of(context).requestFocus(nextFocus);
  }

  @override
  void initState() {
    super.initState();

    if (prcSel != null) {
      if (prcSel.deletado) {
        indDelSel = "Sim";
      }

      vencimentoCtrl.updateText(DateFormat("dd/MM/yyyy").format(prcSel.vencimento.toDate()));

      inicioCtrl.updateText(DateFormat("dd/MM/yyyy").format(prcSel.inicio.toDate()));

      populaPedidoItens().then((e) async {
        //await calculaTotalItens();
        setState(() {});
      });

    }



    populaItens().then((e) async {
      autoCompleteItem = new AutoCompleteTextField<Produtos>(
        positionTop: MediaQuery.of(context).size.height * 0.10,
        style: TextStyle(fontSize: 12.0, color: Colors.black),

        decoration: new InputDecoration(
          hintText: "Produto",
          icon: new Icon(
            Icons.search,
          ),
        ),
        itemSubmitted: (item) => setState(() {
              objProduto = item;
            }),
        key: key,
        //textCapitalization: TextCapitalization.characters,
        suggestions: lstIte,
        itemBuilder: (context, suggestion) => new Container(
              child: Padding(
                  child: new Text(
                    suggestion.nomIte,
                    style: TextStyle(fontSize: 10),
                  ),
                  padding: EdgeInsets.all(8.0)),
            ),
        itemSorter: (a, b) => a.nomIte == b.nomIte ? 0 : a.nomIte.length > b.nomIte.length ? -1 : 1,

        submitOnSuggestionTap: true, clearOnSubmit: false,
        itemFilter: (suggestion, input) => suggestion.nomIte.toLowerCase().contains(input.toLowerCase()), //_listaProdutos(),
      );
    });
  }

  Future<Null> populaPedidoItens() async {
    await Firestore.instance
        .collection('ofertas_itens')
        .where('oferta', isEqualTo: prcSel.idePrc)
        .getDocuments()
        .then((snapshot) {

      if ((itensCarrinho == null) || (itensCarrinho.length == 0)) {
        snapshot.documents.map((DocumentSnapshot document) {
          itensCarrinho
              .add(new OfertasItens.map(document.data, document.documentID));
        }).toList();
      } else {
        print('vazio');
      }
    });
    return;
  }

  // Popula itens e, se a tela for para editar uma oportunidade existente (modo
  // edição), então já seleciona o item da oportunidade, caso contrário pede para
  // selecionar
  Future<Null> populaItens() async {
    await Firestore.instance.collection('produtos').where('deletado', isEqualTo: false).getDocuments().then((snapshot) {
      if ((lstIte == null) || (lstIte.length == 0)) {
        snapshot.documents.map((DocumentSnapshot document) {
          lstIte.add(new Produtos.map(document.data, document.documentID));
        }).toList();
      }
    });
    return;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: prcSel != null ? Text('Editar oferta') : Text('Novo oferta'),
      ),
      body: SingleChildScrollView(
        child: Container(
          key: _formKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Column(children: [formularioCompleto()]),
              _botaoConfirma(),
              prcSel == null ? Text('') : _botaoDeletar(),
            ],
          ),
        ),
      ),
    );
  }

  Widget _botaoDeletar() {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 16.0),
      child: PrimaryButton(
        icon: Icon(
          Icons.delete,
          color: Colors.white,
          size: 40.0,
        ),
        onPressed: () {
          showDialog(
            context: context,
            builder: (context) => AlertDialog(
                  title: new Text('Alerta'),
                  content: new Text('Deseja realmente apagar?'),
                  actions: <Widget>[
                    new FlatButton(
                      onPressed: () => Navigator.of(context).pop(),
                      child: new Text('Não'),
                    ),
                    new FlatButton(
                      onPressed: () async {
                        Ofertas _analise = await Servicos.editOferta(
                            new Ofertas(prcSel.idePrc, prcSel.desOfe, prcSel.percentual, prcSel.inicio, prcSel.vencimento, indDelSel == "Não" ? false : true), itensCarrinho);

                        Navigator.of(context).pop();
                        Navigator.of(context).pop(_analise);
                      },
                      child: new Text('Sim'),
                    ),
                  ],
                ),
          );
        },
      ),
    );
  }

  Widget formularioCompleto() {
    return Column(children: <Widget>[
      new ListTile(
        leading: const Icon(Icons.account_box),
        title: TextFormField(
            enabled: prcSel != null ? false : true,
            controller: nomeCtrl,
            autofocus: true,
            keyboardType: TextInputType.text,
            textInputAction: TextInputAction.next,
            textCapitalization: TextCapitalization.characters,
            focusNode: _nomeFoco,
            onFieldSubmitted: (term) {
              _fieldFocusChange(context, _nomeFoco, _percentualFoco);
            },
            decoration: InputDecoration(labelText: 'Descrição', labelStyle: TextStyle(fontSize: 18.0)),
            validator: (value) {
              if (value.isEmpty) {
                return 'Digite descrição da oferta';
              }
            }),
      ),
      new ListTile(
        leading: const Icon(Icons.monetization_on),
        title: TextFormField(
          controller: percentualCtrl,
          keyboardType: TextInputType.numberWithOptions(decimal: true, signed: false),
          textInputAction: TextInputAction.done,
          focusNode: _percentualFoco,
          decoration: InputDecoration(labelText: 'Percentual de desconto'),
          validator: (value) {
            if (value.isEmpty) {
              return 'Informe o percentual de desconto';
            }
          },
        ),
      ),
      new ListTile(
          leading: const Icon(Icons.date_range),
          title: new Row(children: <Widget>[
            Container(
                width: MediaQuery.of(context).size.width * 0.30,
                child: InkWell(
                    child: TextFormField(
                      controller: inicioCtrl,
                      textInputAction: TextInputAction.next,
                      enabled: false,
                      decoration: InputDecoration(labelText: 'Início', labelStyle: TextStyle(fontSize: 16.0), contentPadding: EdgeInsets.only(top: 24)),
                    ),
                    onTap: () async {
                      DatePicker.showDatePicker(context,
                          cancel: new Text('Voltar'),
                          confirm: new Text('Pronto'),
                          showTitleActions: true,
                          minYear: DateTime.now().year,
                          maxYear: DateTime.now().year + 1,
                          initialYear: DateTime.now().year,
                          initialMonth: DateTime.now().month - 1,
                          initialDate: DateTime.now().day + 1,
                          locale: 'pt',
                          dateFormat: 'dd-mmm-yyyy', onConfirm: (year, month, date) {
                        inicioCtrl.updateText(("00" + date.toString()).substring(date.toString().length) +
                            '/' +
                            ("00" + month.toString()).substring(month.toString().length) +
                            '/' +
                            year.toString());

                        dataInicio = DateFormat("dd/MM/yyyy").parse(inicioCtrl.text);
                      });
                      setState(() {});
                    }))
          ])),
      new ListTile(
          leading: const Icon(Icons.date_range),
          title: new Row(children: <Widget>[
            Container(
                width: MediaQuery.of(context).size.width * 0.30,
                child: InkWell(
                    child: TextFormField(
                      controller: vencimentoCtrl,
                      textInputAction: TextInputAction.next,
                      enabled: false,
                      decoration: InputDecoration(labelText: 'Vencimento', labelStyle: TextStyle(fontSize: 16.0), contentPadding: EdgeInsets.only(top: 24)),
                    ),
                    onTap: () async {
                      DatePicker.showDatePicker(context,
                          cancel: new Text('Voltar'),
                          confirm: new Text('Pronto'),
                          showTitleActions: true,
                          minYear: DateTime.now().year,
                          maxYear: DateTime.now().year + 1,
                          initialYear: DateTime.now().year,
                          initialMonth: DateTime.now().month - 1,
                          initialDate: DateTime.now().day + 1,
                          locale: 'pt',
                          dateFormat: 'dd-mmm-yyyy', onConfirm: (year, month, date) {
                        vencimentoCtrl.updateText(("00" + date.toString()).substring(date.toString().length) +
                            '/' +
                            ("00" + month.toString()).substring(month.toString().length) +
                            '/' +
                            year.toString());

                        dataLimite = DateFormat("dd/MM/yyyy").parse(vencimentoCtrl.text);
                      });
                      setState(() {});
                    }))
          ])),
      ListTile(
          leading: const Icon(Icons.delete_outline),
          title: DropdownButton(
              value: indDelSel,
              items: indDel.map((String val) {
                return new DropdownMenuItem<String>(
                  value: val,
                  child: new Text('Inativo:  ' + val),
                );
              }).toList(),
              onChanged: (newVal) {
                this.setState(() {
                  indDelSel = newVal;
                });
              })),
      Container(color: Colors.black12, width: MediaQuery.of(context).size.width, padding: new EdgeInsets.all(10.0), child: new Text('Itens')),
      Container(
          height: 60.0,
          color: Colors.grey[200],
          child: Row(children: <Widget>[
             Container(
                  width: MediaQuery.of(context).size.width * 0.8,
                  padding: EdgeInsets.all(2),
                  child: autoCompleteItem == null ? Text('Carregando...') : autoCompleteItem,
                ),
            Container(
                    //padding: EdgeInsets.all(2),
                //width: 40,
                    child: Padding(
                        padding: const EdgeInsets.symmetric(vertical: 0.0),
                        child: CupertinoButton(
                            child: Icon(
                              Icons.add_shopping_cart,
                              color: Colors.blueAccent,
                              size: 30.0,
                            ),
                            onPressed: () {
                              adicionaItem();
                            }))),

          ])),
      Container(
          width: MediaQuery.of(context).size.width * 0.98,
          height: 300,
          child: itensCarrinho.length > 0
              ? new SingleChildScrollView(
            child: new CustomDataTable(
              columns: <CustomDataColumn>[
                new CustomDataColumn(label: Text('Produto')),
                new CustomDataColumn(label: Text('Apagar')),
              ],
              rows: _createRows(),
            ),
          )
              : new Text('')),
    ]);
  }

  List<CustomDataRow> _createRows() {
    idxRow = 0;
    List<CustomDataRow> linha = new List<CustomDataRow>();
    linha.clear();
    List<CustomDataCell> colunas = new List<CustomDataCell>();
    colunas.clear();
    itensCarrinho.forEach((item) {
      colunas.add(new CustomDataCell(new Text(item.produto)));
      colunas.add(new CustomDataCell(Material(
          child: InkWell(
        onTap: () {
          //itensCarrinho.removeAt(idxRow - 1);
          itensCarrinho.removeWhere((p) => p.produto == item.produto);
          --idxRow;
          setState(() {});
        },
        child: Container(width: 32, color: Colors.red[200], child: new Icon(Icons.delete)),
      ))));

      CustomDataRow dr = new CustomDataRow.byIndex(cells: colunas, index: idxRow);
      colunas = new List<CustomDataCell>();
      colunas.clear();
      linha.add(dr);
      idxRow++;
    });
    setState(() {});
    return linha;
  }

  void adicionaItem() {
    if ((objProduto == null) || (objProduto.nomIte.length <= 0)) {
      showDialog(
          context: context,
          builder: (context) => new AlertDialog(title: Text('Erro'), content: Text("Informe o produto"), actions: <Widget>[
                new FlatButton(
                    child: Text('Ok'),
                    onPressed: () {
                      Navigator.pop(context);
                    }),
              ]));
    } else {
      itensCarrinho.add(new OfertasItens(
        '',
        '',
        objProduto.idePrc,
        objProduto.nomIte,
        0,
      ));
      autoCompleteItem.clear();
      setState(() {});
    }
  }

  Widget _botaoConfirma() {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 16.0),
      child: PrimaryButton(
        icon: Icon(
          Icons.done,
          color: Colors.white,
          size: 50.0,
        ),
        onPressed: isSaving ? null : () => checkAndSubmit(),
      ),
    );
  }

  void checkAndSubmit() async {
    setState(() {
      isSaving = true;
    });

    //Verifica se a condicação de oferta foi selecionada
    if ((prcSel != null) && (prcSel.desOfe.length <= 0)) {
      isSaving = false;
      showDialog(
        context: context,
        builder: (context) => new AlertDialog(
              title: new Text('Alerta'),
              content: new Text("Informe a descrição da oferta (exemplo: Semana da carne)"),
              actions: <Widget>[
                new FlatButton(
                  onPressed: () => Navigator.of(context).pop(),
                  child: new Text('OK'),
                ),
              ],
            ),
      );
    } else if ((percentualCtrl.text.length <= 0) || (double.parse(percentualCtrl.text.replaceAll(",", ".")) > 100)) {
      isSaving = false;
      showDialog(
        context: context,
        builder: (context) => new AlertDialog(
              title: new Text('Alerta'),
              content: new Text('Corrija o percentual'),
              actions: <Widget>[
                new FlatButton(
                  onPressed: () => Navigator.of(context).pop(),
                  child: new Text('OK'),
                ),
              ],
            ),
      );
    } else {
      //Criar novo negócio
      if (prcSel == null) {
        Ofertas n = Ofertas('', nomeCtrl.text, double.parse(percentualCtrl.text.replaceAll(",", ".")), Timestamp.fromDate(dataInicio), Timestamp.fromDate(dataLimite),
            indDelSel == "Sim" ? true : false);
        var o = await Servicos.addOferta(n, itensCarrinho);
        if (o != null) {
          //Inseriu com sucesso
          Navigator.of(context).pop(o);
        } else {
          showDialog(
            context: context,
            builder: (context) => new AlertDialog(
                  title: new Text('Alerta'),
                  content: new Text('Houve um erro. Tente novamente ou contate o administrador.'),
                  actions: <Widget>[
                    new FlatButton(
                      onPressed: () => Navigator.of(context).pop(),
                      child: new Text('OK'),
                    ),
                  ],
                ),
          );
        }
      }
      //Editar negócio existente
      if (prcSel != null) {
        //Abriu tela por um negócio existente
        Ofertas n = Ofertas(prcSel.idePrc, nomeCtrl.text, double.parse(percentualCtrl.text.replaceAll(",", ".")), Timestamp.fromDate(dataInicio), Timestamp.fromDate(dataLimite),
            indDelSel == "Sim" ? true : false);
        await Servicos.editOferta(n, itensCarrinho).then((neg) {
          Navigator.of(context).pop(neg);
        });
      }
    }
  }
}
