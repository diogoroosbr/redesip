import 'dart:core';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:redesipapp/classes.dart';
import 'package:redesipapp/servicos.dart';
import 'package:redesipapp/style/primary_button.dart';
//servicos.dart';

/// Autor(es): Diogo Roos
/// Criação: 03/08/2018
/// Função: Editar e marcar como deletado uma análise já cadastrada
/// Parâmetros: Análise selecionado na page listaAnalise

class EditarClientePage extends StatefulWidget {
  EditarClientePage(
      {Key key, this.clienteSelecionado /*, this.localSelecionado*/});
  static const String routeName = "/editarClientePage";
  final Clientes clienteSelecionado;
  //final Local localSelecionado;
  @override
  EditarClienteState createState() {
    EditarClienteState.prcSel = clienteSelecionado;
    //EditarAnaliseState.locSel = localSelecionado;
    return EditarClienteState();
  }
}

class EditarClienteState extends State<EditarClientePage> {
  final _formKey = GlobalKey<FormState>();
  bool isSaving = false;
  static Clientes prcSel;

  List<String> indDel = ['Não', 'Sim'];

  String indDelSel = prcSel != null ? prcSel.deletado ? 'Sim' : 'Não' : 'Não';

  final nomParCtr = TextEditingController(
      text:
          '' /*prcSel != null && prcSel.nomPar.length > 0 ? prcSel.nomPar : ''*/
      );

  List<EstadosSemCep> lstEst = new List<EstadosSemCep>();
  EstadosSemCep dropDownEstado;

  List<Cidades> lstCid = new List<Cidades>();
  Cidades dropDownCidade; // = 'Selecione';

  final telefoneCtrl = new MaskedTextController(
      mask: '(00)0000-00000',
      text: prcSel != null && prcSel.telefone.toString().length > 0
          ? prcSel.telefone
          : null);
  bool isCpf = prcSel != null && prcSel.cpfcnpj.length <= 11 ? true : false;
  final cpfController = new MaskedTextController(
      text:
          prcSel != null && prcSel.cpfcnpj.length <= 11 ? prcSel.cpfcnpj : null,
      mask: '000.000.000-00');
  final cnpjController = new MaskedTextController(
      text:
          prcSel != null && prcSel.cpfcnpj.length > 11 ? prcSel.cpfcnpj : null,
      mask: '00.000.000/0000-00');

  final razaoSocialCtrl =
      TextEditingController(text: prcSel != null ? prcSel.razaosocial : "");
  final nomeFantasiaCtrl =
      TextEditingController(text: prcSel != null ? prcSel.nomefantasia : "");
  final responsavelCtrl =
      TextEditingController(text: prcSel != null ? prcSel.responsavel : "");
  final enderecoCtrl =
      TextEditingController(text: prcSel != null ? prcSel.endereco : "");

  final gpsLatCtrl = new TextEditingController(text: '');
  final gpsLonCtrl = new TextEditingController(text: '');

  final FocusNode _listaEstadosFoco = FocusNode();
  final FocusNode _cpfCnpFoc = FocusNode();
  final FocusNode _telefoneFoco = FocusNode();
  final FocusNode _indDelSelFoco = FocusNode();
  final FocusNode _responsavelFoco = FocusNode();
  final FocusNode _razaoSocialFoco = FocusNode();
  final FocusNode _nomeFantasiaFoco = FocusNode();
  final FocusNode _enderecoFoco = FocusNode();
  final FocusNode _gpsLatFoco = FocusNode();
  final FocusNode _gpsLonFoco = FocusNode();

  _fieldFocusChange(
      BuildContext context, FocusNode currentFocus, FocusNode nextFocus) {
    currentFocus.unfocus();
    FocusScope.of(context).requestFocus(nextFocus);
  }

  @override
  void initState() {
    super.initState();

    if (prcSel != null) {
      if (prcSel.deletado) {
        indDelSel = "Sim";
      }
      if(prcSel.gps != null) {
        gpsLatCtrl.text = prcSel.gps != null ? prcSel.gps.latitude.toString() : '';
        gpsLonCtrl.text = prcSel.gps != null ? prcSel.gps.longitude.toString() : '';
      }
      populaEstadosSemCep().then((estados) {
        dropDownEstado = lstEst.singleWhere((e) => e.nomEst == prcSel.estado);
        populaCidades().then((e) {
          dropDownCidade = lstCid.singleWhere((e) => e.nomCid == prcSel.cidade);
        });
      });
    } else {
      populaEstadosSemCep();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: prcSel != null ? Text('Editar Cliente') : Text('Novo Cliente'),
      ),
      body: SingleChildScrollView(
        child: Container(
          key: _formKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Column(children: [formularioCompleto()]),
              _botaoConfirma(),
              prcSel == null ? Text('') : _botaoDeletar(),
            ],
          ),
        ),
      ),
    );
  }

  Widget formularioCompleto() {
    return Column(children: <Widget>[
      prcSel != null && prcSel.codigo > 0
          ? Text('Código: ' + prcSel.codigo.toString())
          : Text(''),
      new ListTile(
        leading: const Icon(Icons.account_box),
        title: TextFormField(
            controller: razaoSocialCtrl,
            keyboardType: TextInputType.text,
            textInputAction: TextInputAction.next,
            textCapitalization: TextCapitalization.characters,
            focusNode: _razaoSocialFoco,
            onFieldSubmitted: (term) {
              _fieldFocusChange(context, _razaoSocialFoco, _nomeFantasiaFoco);
            },
            decoration: InputDecoration(
                labelText: 'Razão social',
                labelStyle: TextStyle(fontSize: 18.0)),
            validator: (value) {
              if (value.isEmpty) {
                return 'Digite a razão social do cliente';
              }
            }),
      ),
      new ListTile(
        leading: const Icon(
          Icons.keyboard_arrow_right,
          color: Colors.white,
        ),
        title: TextFormField(
            controller: nomeFantasiaCtrl,
            textCapitalization: TextCapitalization.characters,
            textInputAction: TextInputAction.next,
            focusNode: _nomeFantasiaFoco,
            onFieldSubmitted: (term) {
              _fieldFocusChange(context, _nomeFantasiaFoco, _cpfCnpFoc);
            },
            decoration: InputDecoration(
                labelText: 'Nome fantasia',
                labelStyle: TextStyle(fontSize: 18.0)),
            validator: (value) {
              if (value.isEmpty) {
                return 'Qual o nome fantasia (apelido) deste cliente?';
              }
            }),
      ),
      new ListTile(
          leading: new Text(''),
          title: new Column(
            children: <Widget>[
              new RadioListTile(
                title: new Text('Pessoa jurídica'),
                groupValue: isCpf,
                value: false,
                onChanged: (bool e) => seletor(e),
              ),
              new RadioListTile(
                title: new Text('Pessoa fisíca'),
                groupValue: isCpf,
                value: true,
                onChanged: (bool e) => seletor(e),
              ),
            ],
          )),
      new Column(
        children: campoCpfCnp(),
      ),
      new ListTile(
        leading: const Icon(Icons.account_box),
        title: TextFormField(
            controller: responsavelCtrl,
            keyboardType: TextInputType.text,
            textInputAction: TextInputAction.next,
            textCapitalization: TextCapitalization.words,
            focusNode: _responsavelFoco,
            onFieldSubmitted: (term) {
              _fieldFocusChange(context, _responsavelFoco, _telefoneFoco);
            },
            decoration: InputDecoration(
                labelText: 'Responsável',
                labelStyle: TextStyle(fontSize: 18.0)),
            validator: (value) {
              if (value.isEmpty) {
                return 'Digite o nome do responsável pelo cliente';
              }
            }),
      ),
      new ListTile(
        leading: const Icon(Icons.account_box),
        title: TextFormField(
            controller: telefoneCtrl,
            keyboardType: TextInputType.phone,
            textInputAction: TextInputAction.next,
            focusNode: _telefoneFoco,
            onFieldSubmitted: (term) {
              _fieldFocusChange(context, _telefoneFoco, _listaEstadosFoco);
            },
            decoration: InputDecoration(
                labelText: 'Telefone', labelStyle: TextStyle(fontSize: 18.0)),
            validator: (value) {
              if (value.isEmpty) {
                return 'Digite o telefone do cliente';
              }
            }),
      ),
      ListTile(
        leading: const Icon(Icons.gps_fixed),
        title: _listaEstados(),
      ),
      ListTile(
        leading: const Icon(Icons.location_on),
        title: _listaCidades(),
      ),
      new ListTile(
        leading: const Icon(Icons.account_box),
        title: TextFormField(
            controller: enderecoCtrl,
            keyboardType: TextInputType.text,
            textInputAction: TextInputAction.next,
            textCapitalization: TextCapitalization.words,
            focusNode: _enderecoFoco,
            onFieldSubmitted: (term) {
              _fieldFocusChange(context, _enderecoFoco, _gpsLatFoco);
            },
            decoration: InputDecoration(
                labelText: 'Endereço', labelStyle: TextStyle(fontSize: 18.0)),
            validator: (value) {
              if (value.isEmpty) {
                return 'Digite o endereço completo do cliente';
              }
            }),
      ),
      new ListTile(
        leading: const Icon(Icons.gps_fixed),
        title: TextFormField(
            controller: gpsLatCtrl,
            keyboardType: TextInputType.numberWithOptions(decimal: true),
            textInputAction: TextInputAction.next,
            focusNode: _gpsLatFoco,
            onFieldSubmitted: (term) {
              _fieldFocusChange(context, _gpsLatFoco, _gpsLonFoco);
            },
            decoration: InputDecoration(
                labelText: 'Latitude', labelStyle: TextStyle(fontSize: 18.0)),
            validator: (value) {
              if (value.isEmpty) {
                return 'Digite a latitude do endereço';
              }
            }),
      ),
      new ListTile(
        leading: const Icon(Icons.gps_fixed),
        title: TextFormField(
            controller: gpsLonCtrl,
            keyboardType: TextInputType.numberWithOptions(decimal: true),
            textInputAction: TextInputAction.next,
            focusNode: _gpsLonFoco,
            onFieldSubmitted: (term) {
              _fieldFocusChange(context, _gpsLonFoco, _indDelSelFoco);
            },
            decoration: InputDecoration(
                labelText: 'Logintude', labelStyle: TextStyle(fontSize: 18.0)),
            validator: (value) {
              if (value.isEmpty) {
                return 'Digite a longitude do endereço';
              }
            }),
      ),
      ListTile(
          leading: const Icon(Icons.delete_outline),
          title: DropdownButton(
              value: indDelSel,
              items: indDel.map((String val) {
                return new DropdownMenuItem<String>(
                  value: val,
                  child: new Text('Inativo:  ' + val),
                );
              }).toList(),
              onChanged: (newVal) {
                this.setState(() {
                  indDelSel = newVal;
                });
              })),
    ]);
  }

  List<Widget> campoCpfCnp() {
    if (isCpf) {
      return [
        new ListTile(
          leading: Text(''),
          title: TextFormField(
              controller: cpfController,
              textInputAction: TextInputAction.next,
              focusNode: _cpfCnpFoc,
              onFieldSubmitted: (term) {
                _fieldFocusChange(context, _cpfCnpFoc, _responsavelFoco);
              },
              decoration: InputDecoration(labelText: 'CPF'),
              keyboardType: TextInputType.number,
              validator: (value) {
                if (value.isEmpty || value.length < 14) {
                  return 'Informe seu CPF';
                }
              }),
        )
      ];
    }
    return [
      new ListTile(
        leading: Text(''),
        title: TextFormField(
            controller: cnpjController,
            textInputAction: TextInputAction.next,
            focusNode: _cpfCnpFoc,
            onFieldSubmitted: (term) {
              _fieldFocusChange(context, _cpfCnpFoc, _responsavelFoco);
            },
            decoration: InputDecoration(labelText: 'CNPJ'),
            keyboardType: TextInputType.number,
            validator: (value) {
              if (value.isEmpty || value.length < 18) {
                return 'Informe seu CNPJ';
              }
            }),
      )
    ];
  }

  /// Autor: Diogo Roos
  /// Função: Criar o botão Deletar
  /// Parâmetros: -
  Widget _botaoDeletar() {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 16.0),
      child: PrimaryButton(
        icon: Icon(
          Icons.delete,
          color: Colors.white,
          size: 40.0,
        ),
        onPressed: () {
          showDialog(
            context: context,
            builder: (context) => AlertDialog(
                  title: new Text('Alerta'),
                  content: new Text('Deseja realmente apagar?'),
                  actions: <Widget>[
                    new FlatButton(
                      onPressed: () => Navigator.of(context).pop(),
                      child: new Text('Não'),
                    ),
                    new FlatButton(
                      onPressed: () async {
                        Clientes _analise = await Servicos.editCliente(
                            new Clientes(
                                prcSel.idePrc,
                                prcSel.codigo,
                                prcSel.razaosocial,
                                prcSel.nomefantasia,
                                prcSel.telefone,
                                prcSel.responsavel,
                                cpfController.text.length > 0
                                    ? (cpfController.text)
                                    : (cnpjController.text),
                                prcSel.estado,
                                prcSel.cidade,
                                prcSel.endereco,
                                indDelSel == "Não" ? false : true,
                                prcSel.gps,
                            ));

                        Navigator.of(context).pop();
                        Navigator.of(context).pop(_analise);
                      },
                      child: new Text('Sim'),
                    ),
                  ],
                ),
          );
        },
      ),
    );
  }

  Widget _botaoConfirma() {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 16.0),
      child: PrimaryButton(
        icon: Icon(
          Icons.done,
          color: Colors.white,
          size: 50.0,
        ),
        onPressed: isSaving ? null : () => checkAndSubmit(),
      ),
    );
  }

  void checkAndSubmit() async {
    setState(() {
      isSaving = true;
    });

    //Verifica se a condicação de pagamento foi selecionada
    //if (.toLowerCase().contains("negócio")) {

    /*showDialog(
      context: context,
      builder: (context) => new AlertDialog(
            title: new Text('Alerta'),
            content: new Text('Informe o nome do parceiro'),
            actions: <Widget>[
              new FlatButton(
                onPressed: () => Navigator.of(context).pop(),
                child: new Text('OK'),
              ),
            ],
          ),
    );*/

    String cpfcnpj = cpfController.text.length > 0
        ? (cpfController.text)
        : (cnpjController.text);

    //Criar novo negócio
    if (prcSel == null) {
      Clientes n = Clientes(
          '',
          0,
          razaoSocialCtrl.text,
          nomeFantasiaCtrl.text,
          telefoneCtrl.text,
          responsavelCtrl.text,
          cpfcnpj,
          dropDownEstado.nomEst,
          dropDownCidade.nomCid,
          enderecoCtrl.text,
          indDelSel == "Sim" ? true : false,
          GeoPoint(0, 0)
      );
      Servicos.addCliente(n).then((t) {
        if (t != null) {
          isSaving = false;
          //Inseriu com sucesso
          Navigator.of(context).pop(t);
        }
      });
    }
    //Editar negócio existente
    if (prcSel != null) {
      //Abriu tela por um negócio existente
      Clientes n = Clientes(
          prcSel.idePrc,
          prcSel.codigo,
          razaoSocialCtrl.text,
          nomeFantasiaCtrl.text,
          telefoneCtrl.text,
          responsavelCtrl.text,
          cpfcnpj,
          dropDownEstado.nomEst,
          dropDownCidade.nomCid,
          enderecoCtrl.text,
          indDelSel == "Sim" ? true : false,
          gpsLatCtrl.text.length > 0 && gpsLonCtrl.text.length > 0 ? GeoPoint(double.parse(gpsLatCtrl.text), double.parse(gpsLonCtrl.text)) : null,
      );
      await Servicos.editCliente(n).then((neg) {
        isSaving = false;
        Navigator.of(context).pop(neg);
      });
    }
  }

  Future<Null> populaEstadosSemCep() async {
    //Carrega estados apenas uma vez
    if ((lstEst == null) || (lstEst.length <= 0)) {
      await Firestore.instance
          .collection('E001EST')
          .where('CODPAI', isEqualTo: '0055')
          .getDocuments()
          .then((snapshot) {
        if (lstEst.length == 0) {
          snapshot.documents.map((DocumentSnapshot document) {
            lstEst.add(new EstadosSemCep.map(document.data));
          }).toList();
          lstEst.sort((a, b) => a.nomEst.compareTo(b.nomEst));
          lstEst.forEach((obj) {
            if (prcSel != null && obj.sigUfs == prcSel.estado) {
              dropDownEstado = obj;
            }
          });
          setState(() {});
        }
      });
    }
    return;
  }

  ///Popula cidades e seleciona a do local como padrão
  Future<Null> populaCidades() async {
    if (dropDownEstado != null) {
      await Firestore.instance
          .collection('E001CID')
          .where('CODPAI', isEqualTo: '0055')
          .where('SIGUFS', isEqualTo: dropDownEstado.sigUfs)
          .getDocuments()
          .then((snapshot) {
        if (lstCid.length == 0) {
          snapshot.documents.map((DocumentSnapshot document) {
            lstCid.add(new Cidades.map(document.data));
          }).toList();
          lstCid.sort((a, b) => a.nomCid.compareTo(b.nomCid));
          lstCid.forEach((obj) {
            if (prcSel != null && obj.nomCid == prcSel.cidade) {
              dropDownCidade = obj;
            }
          });
        }
      });
    }
    return;
  }

  Widget _listaEstados() {
    if (lstEst.isEmpty) {
      return const Text('Carregando');
    }

    return DropdownButton<EstadosSemCep>(
      hint: Text(
          "Selecione o estado"), //_chgLoc ? Text(dropDownEstado.nomEst) : Text("Selecione o estado"),
      value: dropDownEstado,
      onChanged: (EstadosSemCep novoEstado) {
        dropDownEstado = novoEstado;
        lstCid = new List<Cidades>();
        dropDownCidade = null;
        setState(() {});
      },
      items: lstEst.map((EstadosSemCep estado) {
        //Substitui o objeto pelo recebido no método
        return DropdownMenuItem(
          child: Text(estado
              .nomEst), //_chgLoc ? Text(dropDownEstado.nomEst) : Text(estado.nomEst),
          value: estado,
        );
      }).toList(),
    );
    //}
  }

  Widget _listaCidades() {
    if (dropDownEstado != null) {
      return StreamBuilder<QuerySnapshot>(
        stream: Firestore.instance
            .collection('E001CID')
            .where('CODPAI', isEqualTo: '0055')
            .where('SIGUFS', isEqualTo: dropDownEstado.sigUfs)
            .orderBy('NOMCID')
            .snapshots(),
        builder: (context, snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.waiting:
              return new Text('Carregando');
            case ConnectionState.active:
              {
                if (!snapshot.hasData || snapshot.data.documents.length == 0) {
                  return Text("Não há cidades cadastradas");
                }
                if (lstCid.length == 0) {
                  snapshot.data.documents.map((DocumentSnapshot document) {
                    lstCid.add(new Cidades.map(document.data));
                  }).toList();
                  //lstCid.sort((a, b) => a.nomCid.compareTo(b.nomCid));
                  lstCid.forEach((cidade) {
                    //Se for edição de uma oportunidade, considera a cidade da oportunidade
                    if (prcSel != null && cidade.nomCid == prcSel.cidade) {
                      dropDownCidade = cidade;
                    }
                  });
                }

                return DropdownButton<Cidades>(
                  hint: Text("Selecione a cidade"),
                  value: dropDownCidade,
                  onChanged: (Cidades novaCidade) {
                    setState(() {
                      dropDownCidade = novaCidade;
                    });
                  },
                  items: lstCid.map((Cidades cidade) {
                    return DropdownMenuItem(
                      child: Container(
                        width: MediaQuery.of(context).size.width * 0.65,
                        child: Text(
                          cidade.nomCid,
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                      value: cidade,
                    );
                  }).toList(),
                );
              }
            case ConnectionState.done:
              break;
            case ConnectionState.none:
              return Text("none");
          }
        },
      );
    }
  }

  void seletor(e) {
    setState(() {
      if (e == true) {
        isCpf = true;
        cnpjController.clear();
      } else {
        isCpf = false;
        cpfController.clear();
      }
    });
  }
}
