import 'dart:core';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:redesipapp/classes.dart';
import 'package:redesipapp/pages/cadastros/editarPagamento.dart';
import 'package:redesipapp/servicos.dart';

class ListarPagamentosPage extends StatefulWidget {
  ListarPagamentosPage(
      {Key key, this.pagamentoSelecionado /*, this.localSelecionado*/});
  static const String routeName = "/listarPagamentosPage";
  final Pagamentos pagamentoSelecionado;
  //final Local localSelecionado;
  @override
  ListarPagamentosState createState() {
    ListarPagamentosState.prcSel = pagamentoSelecionado;
    //EditarAnaliseState.locSel = localSelecionado;
    return ListarPagamentosState();
  }
}

class ListarPagamentosState extends State<ListarPagamentosPage> {
  final _formKey = GlobalKey<FormState>();
  bool isSaving = false;
  static Pagamentos prcSel;
  List<Pagamentos> listaPagamentos = new List<Pagamentos>();
  List<Pagamentos> listaPagamentosFiltro = new List<Pagamentos>();
  String pesquisa = '';
  TextEditingController ctrPesq = new TextEditingController();

  List<String> indDel = ['Não', 'Sim'];

  String indDelSel = prcSel != null ? prcSel.deletado ? 'Sim' : 'Não' : 'Não';

  final nomParCtr = TextEditingController(
      text:
          '' /*prcSel != null && prcSel.nomPar.length > 0 ? prcSel.nomPar : ''*/
      );

  final FocusNode _qtdFocus = FocusNode();
  final FocusNode _vlrFocus = FocusNode();
  final FocusNode _datLimFoc = FocusNode();
  final FocusNode _horLimFoc = FocusNode();

  _fieldFocusChange(
      BuildContext context, FocusNode currentFocus, FocusNode nextFocus) {
    currentFocus.unfocus();
    FocusScope.of(context).requestFocus(nextFocus);
  }

  @override
  void initState() {
    super.initState();

    if (prcSel != null) {
      if (prcSel.deletado) {
        indDelSel = "Sim";
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Pagamentos'),
      ),
      body: _buildLista(),
      floatingActionButton: FloatingActionButton(
        child: Icon(
          Icons.add,
          color: Colors.white,
        ),
        onPressed: () {
          Navigator.pushReplacement(
              context,
              new MaterialPageRoute(
                  builder: (context) => new EditarPagamentoPage()));
        },
      ),
    );
  }

  Query busca = Firestore.instance.collection('pagamentos')
      //.where('deletado', isEqualTo: false)
      ;

  Widget _buildLista() {
    return new Column(children: <Widget>[
      Padding(
          padding: EdgeInsets.all(0.0),
          child: Column(children: <Widget>[
            TextField(
                controller: ctrPesq,
                onChanged: (String valor) {
                  setState(() {
                    pesquisa = valor;
                  });
                },
                decoration: InputDecoration(
                    hintText: 'Procurar...',
                    prefixIcon: Padding(
                      padding: EdgeInsets.all(0.0),
                      child: Icon(Icons.search),
                    ),
                    suffixIcon: Padding(
                      padding: EdgeInsets.all(0.0),
                      child: IconButton(
                          icon: Icon(Icons.clear),
                          onPressed: () {
                            setState(() {
                              pesquisa = '';
                              ctrPesq.text = '';
                            });
                          }),
                    ),
                    filled: true,
                    fillColor: Colors.white))
          ])),
      Expanded(
        child: new StreamBuilder(
            stream: busca.snapshots(),
            builder: (context, snapshot) {
              if (!snapshot.hasData) return Text('Carregando...');
              if (!snapshot.hasData ||
                  (snapshot.data.documents as List<DocumentSnapshot>).isEmpty)
                return Text('Não há pagamentos');
              listaPagamentos = new List<Pagamentos>();
              snapshot.data.documents.map((DocumentSnapshot document) {
                listaPagamentos.add(
                    new Pagamentos.map(document.data, document.documentID));
              }).toList();

              if (pesquisa.isNotEmpty) {
                listaPagamentosFiltro = listaPagamentos
                    .where((analise) => analise.desPag
                        .toLowerCase()
                        .contains(pesquisa.toLowerCase()))
                    .toList();
              }

              if (pesquisa.isEmpty) {
                listaPagamentosFiltro = listaPagamentos.toList();
              }

              return RefreshIndicator(
                  onRefresh: () async {
                    setState(() {
                      listaPagamentos = new List<Pagamentos>();
                    });
                    return null;
                  },
                  child: new ListView.builder(
                      itemCount: (listaPagamentosFiltro.length) + 1,
                      padding: const EdgeInsets.only(top: 10.0),
                      itemExtent: 60.0,
                      itemBuilder: (context, i) {
                        if (i >= (listaPagamentosFiltro.length)) {
                          //Card para botao float
                          return new Card(
                            elevation: 0.0,
                            color: Colors.transparent,
                            child: Text(''),
                          );
                        }

                        Pagamentos _prc = listaPagamentosFiltro[i];

                        String qtd = "";

                        String txtOpoOuNeg = _prc.desPag.toString();

                        //70% da largura da tela - aplicado quando tem estado/cidade vizinhos
                        double c_width =
                            MediaQuery.of(context).size.width * 0.7;

                        return new GestureDetector(
                            child: Card(
                                color: Colors.white,
                                child: Padding(
                                    padding: EdgeInsets.all(2.0),
                                    child: new Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.stretch,
                                        children: <Widget>[
                                          Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Text(
                                                txtOpoOuNeg,
                                                softWrap: true,
                                                style: TextStyle(
                                                    fontSize: 15.0,
                                                    color: Colors.red[800],
                                                    fontWeight:
                                                        FontWeight.bold),
                                              ),
                                              Container(
                                                width: c_width,
                                                child: Text(
                                                    _prc.percentual.toString() +
                                                        '% de desconto',
                                                    softWrap: true,
                                                    style: TextStyle(
                                                        fontSize: 12.0)),
                                              ),
                                            ],
                                          ),

                                          // Botões dentro do card
                                          Container(
                                              width: 90.0,
                                              child: Column(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceEvenly,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.end,
                                                  children: <Widget>[
                                                    Column(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .center,
                                                        children: <Widget>[
                                                          _prc.deletado == false
                                                              ? IconButton(
                                                                  icon: Icon(
                                                                    Icons
                                                                        .delete,
                                                                    color: Colors
                                                                        .grey,
                                                                  ),
                                                                  onPressed:
                                                                      _prc.deletado !=
                                                                              true
                                                                          ? () {
                                                                              showDialog(
                                                                                context: context,
                                                                                builder: (context) => new AlertDialog(
                                                                                      title: Text('Excluir'),
                                                                                      content: Text("Deseja realmente inativar?"),
                                                                                      actions: <Widget>[
                                                                                        new FlatButton(
                                                                                            child: Text('Não'),
                                                                                            onPressed: () {
                                                                                              Navigator.pop(context);
                                                                                            }),
                                                                                        new FlatButton(
                                                                                          child: Text('Sim'),
                                                                                          onPressed: () async {
                                                                                            _prc.deletado = true;
                                                                                            await Servicos.editPagamento(_prc);
                                                                                            setState(() {
                                                                                              listaPagamentos = new List<Pagamentos>();
                                                                                            });
                                                                                            Navigator.of(context).pop();
                                                                                          },
                                                                                        )
                                                                                      ],
                                                                                    ),
                                                                              );
                                                                            }
                                                                          : null,
                                                                )
                                                              : IconButton(
                                                                  icon: Icon(
                                                                    Icons
                                                                        .check_circle,
                                                                    color: _prc.deletado ==
                                                                            false
                                                                        ? Colors
                                                                            .grey
                                                                        : Colors
                                                                            .yellow,
                                                                  ),
                                                                  onPressed:
                                                                      _prc.deletado ==
                                                                              false
                                                                          ? null
                                                                          : () {
                                                                              showDialog(
                                                                                builder: (context) => AlertDialog(
                                                                                      title: Text('Reativar?'),
                                                                                      actions: <Widget>[
                                                                                        new FlatButton(
                                                                                          child: Icon(
                                                                                            Icons.check_circle,
                                                                                            color: Colors.green,
                                                                                          ),
                                                                                          onPressed: () {
                                                                                            setState(() {
                                                                                              Servicos.editPagamento(new Pagamentos(_prc.idePrc, _prc.desPag, _prc.percentual, false)).then((a) {
                                                                                                //editada = _prc.idePrc;
                                                                                                //tabController.index = 1;
                                                                                              });
                                                                                              Navigator.pop(context);
                                                                                              //doAnimation();
                                                                                            });
                                                                                          },
                                                                                        ),
                                                                                        FlatButton(
                                                                                          child: Icon(Icons.cancel),
                                                                                          onPressed: () => Navigator.pop(context),
                                                                                        )
                                                                                      ],
                                                                                    ),
                                                                                context: context,
                                                                              );
                                                                            },
                                                                ),
                                                          Container(
                                                            width: 0.0,
                                                            height: 0.0,
                                                          )
                                                        ]),

                                                    //TODO Comentar todo o botão abaixo?
                                                  ]))
                                        ]))),
                            onTap: () {
                              Navigator.push(
                                  context,
                                  new MaterialPageRoute(
                                      builder: (BuildContext context) =>
                                          new EditarPagamentoPage(
                                            pagamentoSelecionado: _prc,
                                          ))).then((prod) {
                                setState(() {
                                  if (prod != null) {
                                    if ((prod as Pagamentos).deletado) {
                                      listaPagamentos = new List<Pagamentos>();
                                    } else {
                                      _prc.desPag = (prod as Pagamentos).desPag;
                                      //editada = _prc.idePrc;
                                      //doAnimation();
                                    }
                                  }
                                });
                              });
                            });
                      }));
            }),
      ),
    ]);
  }
}
