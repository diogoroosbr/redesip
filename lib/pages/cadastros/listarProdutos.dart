import 'dart:core';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:intl/intl.dart';
import 'package:redesipapp/classes.dart';
import 'package:redesipapp/pages/cadastros/editarProduto.dart';
import 'package:redesipapp/servicos.dart';

class ListarProdutosPage extends StatefulWidget {
  ListarProdutosPage(
      {Key key, this.produtoSelecionado, this.usuario /*, this.localSelecionado*/});
  static const String routeName = "/listarProdutosPage";
  Produtos produtoSelecionado;
  Usuario usuario;
  //final Local localSelecionado;
  @override
  ListarProdutosState createState() {
    ListarProdutosState.prcSel = produtoSelecionado;
    //EditarAnaliseState.locSel = localSelecionado;
    return ListarProdutosState();
  }
}

class ListarProdutosState extends State<ListarProdutosPage> {
  final _formKey = GlobalKey<FormState>();
  bool isSaving = false;
  static Produtos prcSel;
  List<Produtos> listaProdutos = new List<Produtos>();
  List<Produtos> listaProdutosFiltro = new List<Produtos>();
  String pesquisa = '';
  TextEditingController ctrPesq = new TextEditingController();

  NumberFormat moneyFormatter = new NumberFormat();

  List<String> indDel = ['Não', 'Sim'];

  String indDelSel = prcSel != null ? prcSel.deletado ? 'Sim' : 'Não' : 'Não';

  final nomParCtr = TextEditingController(
      text:
          '' /*prcSel != null && prcSel.nomPar.length > 0 ? prcSel.nomPar : ''*/
      );

  final FocusNode _qtdFocus = FocusNode();
  final FocusNode _vlrFocus = FocusNode();
  final FocusNode _datLimFoc = FocusNode();
  final FocusNode _horLimFoc = FocusNode();

  _fieldFocusChange(
      BuildContext context, FocusNode currentFocus, FocusNode nextFocus) {
    currentFocus.unfocus();
    FocusScope.of(context).requestFocus(nextFocus);
  }

  @override
  void initState() {
    super.initState();

    moneyFormatter.significantDigitsInUse = true;

    if (prcSel != null) {
      if (prcSel.deletado) {
        indDelSel = "Sim";
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Produtos'),
      ),
      body: _buildLista(),
      floatingActionButton:
      widget.usuario.admin
          ? FloatingActionButton(
            child: Icon(
              Icons.add,
              color: Colors.white,
            ),
            onPressed: () {
              Navigator.pushReplacement(
                  context,
                  new MaterialPageRoute(
                      builder: (context) => new EditarProdutoPage()));
            },
          )
          : null,
    );
  }

  Query busca = Firestore.instance.collection('produtos')
      //.where('deletado', isEqualTo: false)
      ;

  Widget _buildLista() {
    return new Column(children: <Widget>[
      Padding(
          padding: EdgeInsets.all(0.0),
          child: Column(children: <Widget>[
            TextField(
                controller: ctrPesq,
                onChanged: (String valor) {
                  setState(() {
                    pesquisa = valor;
                  });
                },
                decoration: InputDecoration(
                    hintText: 'Procurar...',
                    prefixIcon: Padding(
                      padding: EdgeInsets.all(0.0),
                      child: Icon(Icons.search),
                    ),
                    suffixIcon: Padding(
                      padding: EdgeInsets.all(0.0),
                      child: IconButton(
                          icon: Icon(Icons.clear),
                          onPressed: () {
                            setState(() {
                              pesquisa = '';
                              ctrPesq.text = '';
                            });
                          }),
                    ),
                    filled: true,
                    fillColor: Colors.white))
          ])),
      Expanded(
        child: new StreamBuilder(
            stream: busca.snapshots(),
            builder: (context, snapshot) {
              if (!snapshot.hasData) return Text('Carregando...');
              if (!snapshot.hasData ||
                  (snapshot.data.documents as List<DocumentSnapshot>).isEmpty)
                return Text('Não há produtos');
              listaProdutos = new List<Produtos>();
              snapshot.data.documents.map((DocumentSnapshot document) {
                listaProdutos
                    .add(new Produtos.map(document.data, document.documentID));
              }).toList();

              if (pesquisa.isNotEmpty) {
                listaProdutosFiltro = listaProdutos
                    .where((analise) => analise.nomIte
                        .toLowerCase()
                        .contains(pesquisa.toLowerCase()))
                    .toList();
              }

              if (pesquisa.isEmpty) {
                listaProdutosFiltro = listaProdutos.toList();
              }

              return RefreshIndicator(
                  onRefresh: () async {
                    setState(() {
                      listaProdutos = new List<Produtos>();
                    });
                    return null;
                  },
                  child: new ListView.builder(
                      itemCount: (listaProdutosFiltro.length) + 1,
                      padding: const EdgeInsets.only(top: 10.0),
                      itemExtent: 60.0,
                      itemBuilder: (context, i) {
                        if (i >= (listaProdutosFiltro.length)) {
                          //Card para botao float
                          return new Card(
                            elevation: 0.0,
                            color: Colors.transparent,
                            child: Text(''),
                          );
                        }

                        Produtos _prc = listaProdutosFiltro[i];

                        String qtd = "";

                        MoneyMaskedTextController vlrCtr =
                            new MoneyMaskedTextController();
                        vlrCtr.text = _prc.preco.toStringAsFixed(2);

                        //String vlrCtr = stringToDouble(_prc.preco.toString()).toString(); //.toDouble().toString();

                        String txtOpoOuNeg = _prc.nomIte.toString();

                        //70% da largura da tela - aplicado quando tem estado/cidade vizinhos
                        double c_width =
                            MediaQuery.of(context).size.width * 0.7;

                        double cntProduto =
                            MediaQuery.of(context).size.width * 0.6;

                        return new GestureDetector(
                            child: Card(
                                color: Colors.white,
                                child: Padding(
                                    padding: EdgeInsets.all(2.0),
                                    child: new Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.stretch,
                                        children: <Widget>[
                                          Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Container(
                                                width: cntProduto,
                                                child: Text(
                                                  txtOpoOuNeg,
                                                  softWrap: true,
                                                  style: TextStyle(
                                                      fontSize: 11.0,
                                                      color: Colors.red[800],
                                                      fontWeight:
                                                          FontWeight.bold),
                                                ),
                                              ),
                                              Container(
                                                width: c_width,
                                                child: Text(vlrCtr.text,
                                                    softWrap: true,
                                                    style: TextStyle(
                                                        fontSize: 12.0)),
                                              ),
                                            ],
                                          ),

                                          // Botões dentro do card
                                          Container(
                                              width: 90.0,
                                              child: Column(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceEvenly,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.end,
                                                  children: <Widget>[
                                                    Column(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .center,
                                                        children: <Widget>[
                                                          _prc.deletado == false
                                                              ? IconButton(
                                                                  icon: Icon(
                                                                    Icons
                                                                        .delete,
                                                                    color: Colors
                                                                        .grey,
                                                                  ),
                                                                  onPressed:
                                                                      _prc.deletado != true && widget.usuario.admin
                                                                          ? () {
                                                                              showDialog(
                                                                                context: context,
                                                                                builder: (context) => new AlertDialog(
                                                                                      title: Text('Excluir'),
                                                                                      content: Text("Deseja realmente inativar?"),
                                                                                      actions: <Widget>[
                                                                                        new FlatButton(
                                                                                            child: Text('Não'),
                                                                                            onPressed: () {
                                                                                              Navigator.pop(context);
                                                                                            }),
                                                                                        new FlatButton(
                                                                                          child: Text('Sim'),
                                                                                          onPressed: () async {
                                                                                            _prc.deletado = true;
                                                                                            await Servicos.editProduto(_prc);
                                                                                            setState(() {
                                                                                              listaProdutos = new List<Produtos>();
                                                                                            });
                                                                                            Navigator.of(context).pop();
                                                                                          },
                                                                                        )
                                                                                      ],
                                                                                    ),
                                                                              );
                                                                            }
                                                                          : null,
                                                                )
                                                              : IconButton(
                                                                  icon: Icon(
                                                                    Icons
                                                                        .check_circle,
                                                                    color: _prc.deletado ==
                                                                            false
                                                                        ? Colors
                                                                            .grey
                                                                        : Colors
                                                                            .yellow,
                                                                  ),
                                                                  onPressed:
                                                                    widget.usuario.admin ?
                                                                      _prc.deletado == false
                                                                          ? null
                                                                          : () {
                                                                              showDialog(
                                                                                builder: (context) => AlertDialog(
                                                                                      title: Text('Reativar?'),
                                                                                      actions: <Widget>[
                                                                                        new FlatButton(
                                                                                          child: Icon(
                                                                                            Icons.check_circle,
                                                                                            color: Colors.green,
                                                                                          ),
                                                                                          onPressed: () {
                                                                                            setState(() {
                                                                                              Servicos.editProduto(new Produtos(_prc.idePrc, _prc.codPro, _prc.nomIte, _prc.preco, _prc.temDec, false, 0)).then((a) {
                                                                                                //editada = _prc.idePrc;
                                                                                                //tabController.index = 1;
                                                                                              });
                                                                                              Navigator.pop(context);
                                                                                              //doAnimation();
                                                                                            });
                                                                                          },
                                                                                        ),
                                                                                        FlatButton(
                                                                                          child: Icon(Icons.cancel),
                                                                                          onPressed: () => Navigator.pop(context),
                                                                                        )
                                                                                      ],
                                                                                    ),
                                                                                context: context,
                                                                              );
                                                                            }
                                                                    : null,
                                                                ),
                                                          Container(
                                                            width: 0.0,
                                                            height: 0.0,
                                                          )
                                                        ]),

                                                    //TODO Comentar todo o botão abaixo?
                                                  ]))
                                        ]))),
                            onTap: () {
                              Navigator.push(
                                  context,
                                  new MaterialPageRoute(
                                      builder: (BuildContext context) =>
                                          new EditarProdutoPage(
                                            produtoSelecionado: _prc,
                                          ))).then((prod) {
                                setState(() {
                                  if (prod != null) {
                                    if ((prod as Produtos).deletado) {
                                      listaProdutos = new List<Produtos>();
                                    } else {
                                      _prc.nomIte = (prod as Produtos).nomIte;
                                      //editada = _prc.idePrc;
                                      //doAnimation();
                                    }
                                  }
                                });
                              });
                            });
                      }));
            }),
      ),
    ]);
  }
}
