import 'dart:core';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:redesipapp/classes.dart';
import 'package:redesipapp/servicos.dart';
import 'package:redesipapp/style/primary_button.dart';
//servicos.dart';

/// Autor(es): Diogo Roos
/// Criação: 03/08/2018
/// Função: Editar e marcar como deletado uma análise já cadastrada
/// Parâmetros: Análise selecionado na page listaAnalise

class EditarProdutoPage extends StatefulWidget {
  EditarProdutoPage(
      {Key key, this.produtoSelecionado /*, this.localSelecionado*/});
  static const String routeName = "/editarProdutoPage";
  final Produtos produtoSelecionado;
  //final Local localSelecionado;
  @override
  EditarProdutoState createState() {
    EditarProdutoState.prcSel = produtoSelecionado;
    //EditarAnaliseState.locSel = localSelecionado;
    return EditarProdutoState();
  }
}

class EditarProdutoState extends State<EditarProdutoPage> {
  final _formKey = GlobalKey<FormState>();
  bool isSaving = false;
  static Produtos prcSel;

  List<String> indDel = ['Não', 'Sim'];

  String indDelSel = prcSel != null ? prcSel.deletado ? 'Sim' : 'Não' : 'Não';

  final nomParCtr = TextEditingController(
      text:
          '' /*prcSel != null && prcSel.nomPar.length > 0 ? prcSel.nomPar : ''*/
      );

  final nomeCtrl =
      TextEditingController(text: prcSel != null ? prcSel.nomIte : "");
  final codigoCtrl = TextEditingController(
      text: prcSel != null ? prcSel.codPro.toString() : "");

  final precoCtrl = prcSel != null
      ? MoneyMaskedTextController(
          initialValue: prcSel.preco != 0 ? prcSel.preco : 0)
      : new MoneyMaskedTextController();

  final FocusNode _nomeFoco = FocusNode();
  final FocusNode _codigoFoco = FocusNode();
  final FocusNode _precoFoco = FocusNode();

  _fieldFocusChange(
      BuildContext context, FocusNode currentFocus, FocusNode nextFocus) {
    currentFocus.unfocus();
    FocusScope.of(context).requestFocus(nextFocus);
  }

  @override
  void initState() {
    super.initState();

    if (prcSel != null) {
      if (prcSel.deletado) {
        indDelSel = "Sim";
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: prcSel != null ? Text('Editar Produto') : Text('Novo Produto'),
      ),
      body: SingleChildScrollView(
        child: Container(
          key: _formKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Column(children: [formularioCompleto()]),
              _botaoConfirma(),
              prcSel == null ? Text('') : _botaoDeletar(),
            ],
          ),
        ),
      ),
    );
  }

  Widget _botaoDeletar() {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 16.0),
      child: PrimaryButton(
        icon: Icon(
          Icons.delete,
          color: Colors.white,
          size: 40.0,
        ),
        onPressed: () {
          showDialog(
            context: context,
            builder: (context) => AlertDialog(
                  title: new Text('Alerta'),
                  content: new Text('Deseja realmente apagar?'),
                  actions: <Widget>[
                    new FlatButton(
                      onPressed: () => Navigator.of(context).pop(),
                      child: new Text('Não'),
                    ),
                    new FlatButton(
                      onPressed: () async {
                        Produtos _analise = await Servicos.editProduto(
                            new Produtos(
                                prcSel.idePrc,
                                prcSel.codPro,
                                prcSel.nomIte,
                                prcSel.preco,
                                prcSel.temDec,
                                indDelSel == "Não" ? false : true,
                                0));

                        Navigator.of(context).pop();
                        Navigator.of(context).pop(_analise);
                      },
                      child: new Text('Sim'),
                    ),
                  ],
                ),
          );
        },
      ),
    );
  }

  Widget formularioCompleto() {
    return Column(children: <Widget>[
      new ListTile(
        leading: const Icon(Icons.account_box),
        title: TextFormField(
            controller: nomeCtrl,
            keyboardType: TextInputType.multiline,
            maxLines: 2,
            //keyboardType: TextInputType.text,
            textInputAction: TextInputAction.next,
            textCapitalization: TextCapitalization.characters,
            focusNode: _nomeFoco,
            onFieldSubmitted: (term) {
              _fieldFocusChange(context, _nomeFoco, _codigoFoco);
            },
            decoration: InputDecoration(
                labelText: 'Nome do produto',
                labelStyle: TextStyle(fontSize: 18.0)),
            validator: (value) {
              if (value.isEmpty) {
                return 'Digite o nome (descrição) do produto';
              }
            }),
      ),
      new ListTile(
        leading: const Icon(
          Icons.keyboard_arrow_right,
          color: Colors.white,
        ),
        title: TextFormField(
            controller: codigoCtrl,
            keyboardType: TextInputType.number,
            textInputAction: TextInputAction.done,
            focusNode: _codigoFoco,
            onFieldSubmitted: (term) {
              _fieldFocusChange(context, _codigoFoco, _precoFoco);
            },
            decoration: InputDecoration(
                labelText: 'Código barra',
                labelStyle: TextStyle(fontSize: 18.0)),
            validator: (value) {
              if (value.isEmpty) {
                return 'Informe o código de barra';
              }
            }),
      ),
      new ListTile(
        leading: const Icon(
          Icons.monetization_on,
          color: Colors.white,
        ),
        title: TextFormField(
            controller: precoCtrl,
            keyboardType: TextInputType.number,
            textInputAction: TextInputAction.done,
            focusNode: _precoFoco,
            decoration: InputDecoration(
                labelText: 'Valor sem desconto',
                labelStyle: TextStyle(fontSize: 18.0)),
            validator: (value) {
              if (value.isEmpty) {
                return 'Informe o valor do produto';
              }
            }),
      ),
      ListTile(
          leading: const Icon(Icons.delete_outline),
          title: DropdownButton(
              value: indDelSel,
              items: indDel.map((String val) {
                return new DropdownMenuItem<String>(
                  value: val,
                  child: new Text('Inativo:  ' + val),
                );
              }).toList(),
              onChanged: (newVal) {
                this.setState(() {
                  indDelSel = newVal;
                });
              })),
    ]);
  }

  Widget _botaoConfirma() {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 16.0),
      child: PrimaryButton(
        icon: Icon(
          Icons.done,
          color: Colors.white,
          size: 50.0,
        ),
        onPressed: isSaving ? null : () => checkAndSubmit(),
      ),
    );
  }

  void checkAndSubmit() async {
    setState(() {
      isSaving = true;
    });

    //Verifica se a condicação de pagamento foi selecionada
    if (nomeCtrl.text.length <= 0) {
      isSaving = false;
      showDialog(
        context: context,
        builder: (context) => new AlertDialog(
              title: new Text('Alerta'),
              content: new Text('Informe o nome do produto'),
              actions: <Widget>[
                new FlatButton(
                  onPressed: () => Navigator.of(context).pop(),
                  child: new Text('OK'),
                ),
              ],
            ),
      );
    } else if (codigoCtrl.text.length <= 0) {
      isSaving = false;
      showDialog(
        context: context,
        builder: (context) => new AlertDialog(
              title: new Text('Erro'),
              content: new Text('Informe o código do produto'),
              actions: <Widget>[
                new FlatButton(
                  onPressed: () => Navigator.of(context).pop(),
                  child: new Text('OK'),
                ),
              ],
            ),
      );
    } else if (precoCtrl.text.length <= 0) {
      isSaving = false;
      showDialog(
        context: context,
        builder: (context) => new AlertDialog(
              title: new Text('Erro'),
              content: new Text('Informe o preço do produto'),
              actions: <Widget>[
                new FlatButton(
                  onPressed: () => Navigator.of(context).pop(),
                  child: new Text('OK'),
                ),
              ],
            ),
      );
    } else {
      //Criar novo negócio
      if (prcSel == null) {
        Produtos n = Produtos(
            '',
            int.parse(codigoCtrl.text),
            nomeCtrl.text,
            double.parse(precoCtrl.text.replaceAll(",", ".")),
            'S',
            indDelSel == "Sim" ? true : false,
            0);
        Servicos.addProduto(n).then((t) {
          if (t != null) {
            //Inseriu com sucesso
            Navigator.of(context).pop(t);
          }
        });
      }
      //Editar negócio existente
      if (prcSel != null) {
        //Abriu tela por um negócio existente
        Produtos n = Produtos(
            prcSel.idePrc,
            int.parse(codigoCtrl.text),
            nomeCtrl.text,
            double.parse(precoCtrl.text.replaceAll(",", ".")),
            'S',
            indDelSel == "Sim" ? true : false,
            0);
        await Servicos.editProduto(n).then((neg) {
          Navigator.of(context).pop(neg);
        });
      }
    }
  }
}
