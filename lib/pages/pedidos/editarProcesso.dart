import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:redesipapp/classes.dart';

class EditarProcessoPage extends StatefulWidget {
  EditarProcessoPage({
    this.ideUsu,
    this.proc,
    this.evtAdd,
  });
  final Processos proc;
  static const String routeName = "/editarProcessoPage";
  final String ideUsu;
  final ValueChanged<bool> evtAdd;

  @override
  EditarProcessoState createState() {
    EditarProcessoState.proc = proc;
    return EditarProcessoState();
  }
}

class Cidades {
  final String desReg; //Descrição da região
  final String lstEst; //Nome do estado
  final int codIbg;
  final String nomCid; //Nome da cidade
  Cidades(this.codIbg, this.desReg, this.lstEst, this.nomCid);
}

class EditarProcessoState extends State<EditarProcessoPage> {
  static Processos proc;
  static final GlobalKey<FormState> formKey = new GlobalKey<FormState>();
  static final GlobalKey<FormFieldState<String>> txt001 =
      new GlobalKey<FormFieldState<String>>();
  static final GlobalKey<FormFieldState<String>> txt002 =
      new GlobalKey<FormFieldState<String>>();
  static final GlobalKey<FormFieldState<String>> txt003 =
      new GlobalKey<FormFieldState<String>>();
  static final GlobalKey<FormFieldState<String>> txt004 =
      new GlobalKey<FormFieldState<String>>();
  static final GlobalKey<FormFieldState<String>> txt005 =
      new GlobalKey<FormFieldState<String>>();
  static final GlobalKey<FormFieldState<String>> txt006 =
      new GlobalKey<FormFieldState<String>>();
  static final GlobalKey<FormFieldState<String>> txt007 =
      new GlobalKey<FormFieldState<String>>();
  static final GlobalKey<FormFieldState<String>> txt008 =
      new GlobalKey<FormFieldState<String>>();
  static final GlobalKey<FormFieldState<String>> txt009 =
      new GlobalKey<FormFieldState<String>>();
  static final GlobalKey<FormFieldState<String>> txt010 =
      new GlobalKey<FormFieldState<String>>();
  static final GlobalKey<FormFieldState<String>> txt01 =
      new GlobalKey<FormFieldState<String>>();

  List<String> lstCid = ['Selecione'];
  String nomCidSel = 'Selecione';

  List<String> nomIte = [
    'Qual é o item?',
    'Soja',
    'Milho',
    'Feijão',
    'Sorgo',
    'Outro'
  ];
  String nomIteSel = 'Qual é o item?';

  List<String> tipOpo = [
    'Qual é a oportunidade?',
    'Venda',
    'Compra',
    'Troca',
    'Sugestão de pacote'
  ];
  String tipOpoSel = 'Qual é a oportunidade?';

  //Tipo de entrega para troca
  List<String> lstEst = [
    'Selecione',
    'Acre',
    'Alagoas',
    'Amapá',
    'Amazonas',
    'Bahia',
    'Ceará',
    'Distrito Federal',
    'Espírito Santo',
    'Goiás',
    'Maranhão',
    'Mato Grosso',
    'Mato Grosso do Sul',
    'Minas Gerais',
    'Pará',
    'Paraíba',
    'Paraná',
    'Pernambuco',
    'Piauí',
    'Rio de Janeiro',
    'Rio Grande do Norte',
    'Rio Grande do Sul',
    'Rondônia',
    'Roraima',
    'Santa Catarina',
    'São Paulo',
    'Sergipe',
    'Tocantins'
  ];
  String lstEstSel = 'Selecione';

  //Locais
  List<String> tipLoc = [
    'Local de entrega',
    'Local de produção',
    'No fabricante'
  ];
  String tipLocSel = 'Local de entrega';
  //Tipo de entrega para compra
  bool pnlVen = false;
  bool pnlCpr = false;
  bool pnlTrc = false;
  bool pnlBtn = false;

  @override
  void initState() {
    super.initState();
    //lstCid.add("Selecione");
  }

  void _handleTap() {
    //print('edit event');
    widget.evtAdd(true);
  }

  Future<Null> getLstCid(String lstEst) async {
    //Se o estado selecionado for algum válido, carrega a lista de cidades
    lstCid = ['Selecione'];
    if (!lstEstSel.contains('Selecione')) {
      await Firestore.instance
          .collection("E001CID")
          .where('SIGUFS', isEqualTo: lstEst)
          .getDocuments()
          .then((e) async {
        e.documents.forEach((h) async {
          lstCid.add(h.data["NOMCID"]);
        });
        setState(() {});
      });
    }
    //lstCid.removeAt(0);
  }

  @override
  Widget build(BuildContext context) {
    if (tipOpoSel.toLowerCase().contains('venda')) {
      pnlCpr = false;
      pnlTrc = false;
      pnlVen = true;
      pnlBtn = true;
    } else if (tipOpoSel.toLowerCase().contains('compra')) {
      pnlVen = false;
      pnlTrc = false;
      pnlCpr = true;
      pnlBtn = true;
    } else if (tipOpoSel.toLowerCase().contains('troca')) {
      pnlVen = false;
      pnlCpr = false;
      pnlTrc = true;
      pnlBtn = true;
    } else {
      pnlVen = false;
      pnlCpr = false;
      pnlTrc = false;
      pnlBtn = false;
    }

    Color habBtn() {
      if (pnlBtn)
        return Colors.blue;
      else
        return Colors.grey;
    }

    //A lista é criada aqui porque a atualização onChange do Dropdown atualiza o status
    List<Widget> submitWidgets() {
      if (pnlVen) {
        return [
          new TextFormField(
            key: txt001,
            autocorrect: false,
            keyboardType: TextInputType.text,
            style: Theme.of(context).textTheme.body2,
            decoration:
                new InputDecoration(labelText: 'Item (produto, serviço, ...)'),
            validator: (val) => val.isEmpty ? 'Informe o nome do item' : null,
          ),
          new TextFormField(
            key: txt002,
            keyboardType: TextInputType.number,
            style: Theme.of(context).textTheme.body2,
            decoration: new InputDecoration(labelText: 'Quantidade do item'),
            validator: (val) => val.isEmpty
                ? 'Informe a quantidade do item a ser vendido'
                : null,
          ),
          new TextFormField(
            key: txt003,
            keyboardType: TextInputType.number,
            style: Theme.of(context).textTheme.body2,
            decoration:
                new InputDecoration(labelText: 'Valor unitário desejado'),
            validator: (val) =>
                val.isEmpty ? 'Informe o valor de uma unidade' : null,
          ),
          new DropdownButton(
              value: lstEstSel,
              items: lstEst.map((String val) {
                return new DropdownMenuItem<String>(
                  value: val,
                  child: new Text(val),
                );
              }).toList(),
              hint: Text("Estado de entrega"),
              key: txt004,
              onChanged: (newVal) {
                setState(() {
                  lstEstSel = newVal.toString();
                });
                getLstCid(newVal);
              }),
          new DropdownButton(
              value: nomCidSel,
              items: lstCid.map((String val) {
                return new DropdownMenuItem<String>(
                  value: val,
                  child: new Text(val),
                );
              }).toList(),
              hint: Text("Selecione"),
              key: txt005,
              onChanged: (newVal) {
                setState(() {
                  nomCidSel = newVal.toString();
                });
              }),
        ];
      }
      if (pnlTrc) {
        return [
          new TextFormField(
            key: txt001,
            style: Theme.of(context).textTheme.body2,
            decoration: new InputDecoration(labelText: 'Produto'),
            validator: (val) => val.isEmpty ? 'Informe o produto' : null,
          ),
          new TextFormField(
            key: txt002,
            style: Theme.of(context).textTheme.body2,
            decoration: new InputDecoration(labelText: 'Quantidade sacas'),
            validator: (val) =>
                val.isEmpty ? 'Informe a quantidade de sacas' : null,
          ),
          new TextFormField(
            //key: txt003,
            style: Theme.of(context).textTheme.body2,
            decoration: new InputDecoration(labelText: 'Valor total'),
            validator: (val) =>
                val.isEmpty ? 'Por qual valor você pretende vender?' : null,
          ),
          new DropdownButton(
              value: tipLocSel,
              items: tipLoc.map((String val) {
                return new DropdownMenuItem<String>(
                  value: val,
                  child: new Text(val),
                  //key: txt001,
                );
              }).toList(),
              hint: Text("Local de entrega"),
              onChanged: (newVal) {
                this.setState(() {
                  tipLocSel = newVal;
                });
              }),
        ];
      }
      if (pnlCpr) {
        return [
          new TextFormField(
            key: txt001,
            style: Theme.of(context).textTheme.body2,
            decoration: new InputDecoration(labelText: 'Produto'),
            validator: (val) => val.isEmpty ? 'Informe o produto' : null,
          ),
          new TextFormField(
            key: txt002,
            style: Theme.of(context).textTheme.body2,
            decoration: new InputDecoration(labelText: 'Quantidade sacas'),
            validator: (val) =>
                val.isEmpty ? 'Informe a quantidade de sacas' : null,
          ),
          new TextFormField(
            key: txt003,
            style: Theme.of(context).textTheme.body2,
            decoration: new InputDecoration(labelText: 'Valor total'),
            validator: (val) =>
                val.isEmpty ? 'Informe o valor para esta quantidade' : null,
          ),
        ];
      }

      return [
        new Opacity(
          opacity: 0.0,
        )
      ];
    }

    return new Scaffold(
      body: Padding(
        padding: new EdgeInsets.all(16.0),
        child: new Form(
          key: formKey,
          autovalidate: false,
          child: new ListView(
            children: [
              new DropdownButton(
                  value: tipOpoSel,
                  items: tipOpo.map((String val) {
                    return new DropdownMenuItem<String>(
                      value: val,
                      child: new Text(val),
                      //key: txt001,
                    );
                  }).toList(),
                  hint: Text("De que é a oportunidade?"),
                  onChanged: (newVal) {
                    this.setState(() {
                      tipOpoSel = newVal;
                    });
                  }),
              new Column(children: submitWidgets())
            ],
          ),
        ),
      ),
      floatingActionButton: new FloatingActionButton(
        backgroundColor: habBtn(),
        child: new Icon(Icons.save),
        onPressed: () {
          print('cidade ' + nomCidSel);
          if (pnlBtn) {
            bool valid;
            if (lstEstSel.toLowerCase().contains("selecione")) {
              valid = false;
              showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return new AlertDialog(
                      title: new Text('Erro'),
                      content:
                          new Text('Selecione o estado onde entregará o item.'),
                      actions: <Widget>[
                        new FlatButton(
                          onPressed: () => Navigator.of(context).pop(false),
                          child: new Text('Ok, farei isto.'),
                        )
                      ],
                    );
                  });
            } else if (nomCidSel.toLowerCase().contains("selecione")) {
              valid = false;
              showDialog(
                  context: context,
                  builder: (BuildContext ctx) {
                    return new AlertDialog(
                      title: new Text('Erro'),
                      content:
                          new Text('Informe a cidade onde entregará o item.'),
                      actions: <Widget>[
                        new FlatButton(
                          onPressed: () => Navigator.of(context).pop(false),
                          child: new Text('Certo, vou informar.'),
                        )
                      ],
                    );
                  });
            } else {
              valid = true;
            }

            if ((pnlVen) && (valid)) {
              if (formKey.currentState.validate()) {
                var res001, res002, res003, res004, res005 = "";
                if (txt001 != null && txt001.currentState != null)
                  res001 = txt001.currentState.value;
                if (txt002 != null && txt002.currentState != null)
                  res002 = txt002.currentState.value;
                if (txt003 != null && txt003.currentState != null)
                  res003 = txt003.currentState.value;

                Firestore.instance
                    .runTransaction((Transaction transaction) async {
                  CollectionReference ref2 = Firestore.instance
                      .collection("E100OPO/CTV/" + widget.ideUsu);
                  await ref2.add({
                    "TIPPRC": "VENDA",
                    "NOMPRO": res001,
                    "QTDSAC": strToDou(res002),
                    "VLRSAC": strToDou(res003),
                    "ESTENT": lstEstSel,
                    "CIDENT": nomCidSel.toString(),
                    //STATUS: C-CADASTRADO, A-ANDAMENTO, F-FINALIZADO
                    "STAPRC": "C",
                    "IDENEG": null
                  });
                });

                _handleTap();
                Navigator.pop(context); //Fecha esta janela
              }
            }
          }
        },
      ),
    );
  }

  double strToDou(String x) {
    double ret = 0.0;
    if ((x != null) && (x.length > 0)) {
      ret = double.parse(x);
    }
    return ret;
  }
}
