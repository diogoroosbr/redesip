import 'dart:async';
import 'dart:core';
import 'dart:io';
import 'dart:ui';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:flutter_cupertino_date_picker/flutter_cupertino_date_picker.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:image/image.dart' as Img;
import 'package:intl/intl.dart';
import 'package:open_file/open_file.dart';
import 'package:pdf/pdf.dart';
import 'package:redesipapp/classes.dart';
import 'package:redesipapp/pages/mainPage.dart';
import 'package:redesipapp/servicos.dart';
import 'package:redesipapp/style/autocomplete_textfield.dart';
import 'package:redesipapp/style/primary_button.dart';
import 'package:redesipapp/style/theme.dart';
import 'package:redesipapp/uteis/data_table.dart';
//servicos.dart';

/// Autor(es): Diogo Roos
/// Criação: 03/08/2018
/// Função: Editar e marcar como deletado uma análise já cadastrada
/// Parâmetros: Análise selecionado na page listaAnalise

class EditarNegocioPage extends StatefulWidget {
  EditarNegocioPage({Key key, this.processoSelecionado /*, this.localSelecionado*/});
  static const String routeName = "/editarNegocioPage";
  final Processos processoSelecionado;
  //final Local localSelecionado;
  @override
  EditarNegocioState createState() {
    EditarNegocioState.prcSel = processoSelecionado;
    //EditarAnaliseState.locSel = localSelecionado;
    return EditarNegocioState();
  }
}

class EditarNegocioState extends State<EditarNegocioPage> {
  final _formKey = GlobalKey<FormState>();
  GlobalKey key = new GlobalKey<AutoCompleteTextFieldState<Produtos>>();
  GlobalKey keyc = new GlobalKey<AutoCompleteTextFieldState<Clientes>>();
  GlobalKey keyp = new GlobalKey<AutoCompleteTextFieldState<Pagamentos>>();
  bool isSaving = false;
  static Processos prcSel;

  //Máscara para quantidade sem dígitos
  QtdMaskedTextController qtdIntController =
      /*negSel != null
      ? QtdMaskedTextController(
          initialValue: prcSel.uniMed == "Sacas"
              ? int.tryParse(prcSel.qtdIte.toString().replaceAll(".0", ""))
              : 0,
        )
      :*/
      new QtdMaskedTextController();

  //Máscara para quantidade com dígitos (KG)
  MoneyMaskedTextController quantidadeController =
      /*negSel != null
      ? MoneyMaskedTextController(
          initialValue: prcSel.uniMed != "Sacas" ? prcSel.qtdIte : 0.0,
        )
      :*/
      new MoneyMaskedTextController();

  MoneyMaskedTextController precoItensComDescCtrl = prcSel != null
      ? MoneyMaskedTextController(decimalSeparator: ',', thousandSeparator: '.', initialValue: prcSel.valortotal != 0 ? prcSel.valortotal : 0)
      : new MoneyMaskedTextController();

  MoneyMaskedTextController precoItensSemDescCtrl = new MoneyMaskedTextController();

  final auxVlrCtrl = MoneyMaskedTextController(decimalSeparator: ',', thousandSeparator: '.');

  final dataController = new MaskedTextController(mask: '00/00/0000');

  DateTime dataLimite = new DateTime.now();

  final nomParCtr = TextEditingController(text: '' /*prcSel != null && prcSel.nomPar.length > 0 ? prcSel.nomPar : ''*/);

  final FocusNode _qtdFocus = FocusNode();
  final FocusNode _observacaoFoco = FocusNode();
  final FocusNode _datNasFoc = FocusNode();

  _fieldFocusChange(BuildContext context, FocusNode currentFocus, FocusNode nextFocus) {
    currentFocus.unfocus();
    FocusScope.of(context).requestFocus(nextFocus);
  }

  //Indicativo para quando alterar o estado quando a tela vier de uma edição
  bool _estAlt = false;

  List<ProcessosItem> itensCarrinho = new List<ProcessosItem>();
  List<OfertasItens> lstItensEmOferta = new List<OfertasItens>();

  List<Produtos> lstIte = new List<Produtos>();
  Produtos objProduto;
  int idxRow;
  AutoCompleteTextField<Produtos> autoCompleteItem = AutoCompleteTextField<Produtos>();

  List<Clientes> lstCli = new List<Clientes>();
  Clientes dropDownCli;
  AutoCompleteTextField<Clientes> autoCompleteClientes = AutoCompleteTextField<Clientes>();

  List<Pagamentos> lstPag = new List<Pagamentos>();
  Pagamentos dropDownPag;
  AutoCompleteTextField<Pagamentos> autoCompletePagamentos = AutoCompleteTextField<Pagamentos>();

  List<String> lstHoras = ['', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16'];
  String dropDownHoras = prcSel != null ? prcSel.horaEntrega : '';

  List<String> lstMinutos = ['', '00', '15', '30', '45'];
  String dropDownMinutos = prcSel != null ? prcSel.minutoEntrega : '';

  final observacaoCtrl = TextEditingController(text: prcSel != null ? prcSel.observacao : "");

  bool pnlVen = false;
  bool pnlCpr = false;
  bool pnlTrc = false;
  bool pnlBtn = false;

  List<String> horLim = new List<String>.generate(24, (int i) => (i).toString());
  String horLimSel = "0";

  double valorTotal = 0.0;

  EditarNegocioState() {}

  @override
  void initState() {
    super.initState();

    populaClientes().whenComplete(() {
      autoCompleteClientes = new AutoCompleteTextField<Clientes>(
        decoration: new InputDecoration(hintText: "Cliente", suffixIcon: new Icon(Icons.search)),
        itemSubmitted: (item) => setState(() {
              dropDownCli = item;
            }),
        key: keyc,
        //textCapitalization: TextCapitalization.characters,
        suggestions: lstCli,
        itemBuilder: (context, suggestion) => new Container(
              child: Padding(
                  child: new Text(
                    suggestion.nomefantasia,
                    style: TextStyle(fontSize: 10),
                  ),
                  padding: EdgeInsets.all(8.0)),
            ),
        itemSorter: (a, b) => a.nomefantasia == b.nomefantasia ? 0 : a.nomefantasia.length > b.nomefantasia.length ? -1 : 1,

        submitOnSuggestionTap: true, clearOnSubmit: false,
        itemFilter: (suggestion, input) => suggestion.nomefantasia.toLowerCase().contains(input.toLowerCase()),
      );
      //Depois de montar o autocomplete, checa se cliente faz parte da lista (caso
      //não esteja inativo) e atribui o valor ao campo
      if ((prcSel != null) && (prcSel.cliente.length > 0)) {
        lstCli.forEach((f) {
          if (f.nomefantasia == prcSel.cliente) {
            //TODO parou aqui
            dropDownCli = f;
            autoCompleteClientes.decoration = new InputDecoration(hintText: prcSel.cliente);
            //autoCompleteClientes.createState();
            //autoCompleteClientes.textField.controller.text = prcSel.cliente;
          }
        });
      }
      setState(() {});
    });

    populaPagamentos().whenComplete(() {});

    populaOfertasAbertas();

    populaItens().whenComplete(() {
      autoCompleteItem = new AutoCompleteTextField<Produtos>(
        positionTop: MediaQuery.of(context).size.height * 0.10,
        style: TextStyle(fontSize: 12.0, color: Colors.black),

        decoration: new InputDecoration(
          hintText: "Produto",
          icon: new Icon(
            Icons.search,
          ),
        ),
        itemSubmitted: (item) => setState(() {
              objProduto = item;
            }),
        key: key,
        //textCapitalization: TextCapitalization.characters,
        suggestions: lstIte,
        itemBuilder: (context, suggestion) => new Container(
              child: Padding(
                  child: new Text(
                    suggestion.nomIte,
                    style: TextStyle(fontSize: 10),
                  ),
                  padding: EdgeInsets.all(8.0)),
            ),
        itemSorter: (a, b) => a.nomIte == b.nomIte ? 0 : a.nomIte.length > b.nomIte.length ? -1 : 1,

        submitOnSuggestionTap: true, clearOnSubmit: false,
        itemFilter: (suggestion, input) => suggestion.nomIte.toLowerCase().contains(input.toLowerCase()), //_listaProdutos(),
      );
    });

    if (prcSel != null) {
      print('tem');
      populaPedidoItens().then((e) async {
        await calculaTotalItens();
        setState(() {});
      });

      dataController.updateText(
        prcSel != null && prcSel.dataEntrega != null
            ? DateFormat("dd/MM/yyyy").format(prcSel.dataEntrega.toDate())
            : DateFormat('dd/MM/yyyy').format(DateTime.now().add(const Duration(days: 1))),
      );
    } else {
      print('nao tem');
      setState(() {});
    }
  }

  Future<Null> populaPedidoItens() async {
    await Firestore.instance.collection('pedidos_itens').where('pedido', isEqualTo: prcSel.idePrc).getDocuments().then((snapshot) {
      if ((itensCarrinho == null) || (itensCarrinho.length == 0)) {
        snapshot.documents.map((DocumentSnapshot document) {
          itensCarrinho.add(new ProcessosItem.map(document.data, document.documentID));
        }).toList();
      } else {
        print('vazio');
      }
    });
    return;
  }

  // Popula itens e, se a tela for para editar uma oportunidade existente (modo
  // edição), então já seleciona o item da oportunidade, caso contrário pede para
  // selecionar
  Future<Null> populaItens() async {
    await Firestore.instance.collection('produtos').where('deletado', isEqualTo: false).getDocuments().then((snapshot) {
      if ((lstIte == null) || (lstIte.length == 0)) {
        snapshot.documents.map((DocumentSnapshot document) {
          lstIte.add(new Produtos.map(document.data, document.documentID));
        }).toList();
      }
    });
    return;
  }

  Future<Null> populaClientes() async {
    await Firestore.instance.collection('clientes').where('deletado', isEqualTo: false).getDocuments().then((snapshot) {
      if ((lstCli == null) || (lstCli.length == 0)) {
        snapshot.documents.map((DocumentSnapshot document) {
          lstCli.add(new Clientes.map(document.data, document.documentID));
        }).toList();
      }
    });
    return;
  }

  Future<Null> populaOfertasAbertas() async {
    await Firestore.instance
        .collection('ofertas')
        .where('deletado', isEqualTo: false)
        .where('inicio', isLessThanOrEqualTo: Timestamp.now())
        .where('vencimento', isGreaterThanOrEqualTo: Timestamp.now())
        .getDocuments()
        .then((snapshot) {
      if ((lstItensEmOferta == null) || (lstItensEmOferta.length == 0)) {
        snapshot.documents.map((DocumentSnapshot ofe) async {
          await Firestore.instance.collection('ofertas_itens').where('oferta', isEqualTo: ofe.documentID).getDocuments().then((snapshot) {
            snapshot.documents.map((DocumentSnapshot d) {
              lstItensEmOferta.add(new OfertasItens(d.documentID, d.data['oferta'], d.data['idproduto'], d.data['produto'], ofe.data['percentual']));
            }).toList();
          });
        }).toList();
      }
    });
    return;
  }

  Future<Null> populaPagamentos() async {
    await Firestore.instance.collection('pagamentos').where('deletado', isEqualTo: false).getDocuments().then((snapshot) {
      if ((lstPag == null) || (lstPag.length == 0)) {
        snapshot.documents.map((DocumentSnapshot document) {
          lstPag.add(new Pagamentos.map(document.data, document.documentID));
        }).toList();
      }
      if ((prcSel != null) && (prcSel.condicao.length > 0)) {
        lstPag.forEach((o) {
          dropDownPag = lstPag.singleWhere((e) => e.desPag == prcSel.nomecondicao);
        });
      }
    });
    return;
  }

  Future<Null> removeEntregasAgendadas(String horaParaBusca) async {
    lstMinutos = ['', '00', '15', '30', '45'];
    dropDownMinutos = "";
    await Firestore.instance.collection('pedidos').where("dataentrega", isEqualTo: dataLimite).where("horaentrega", isEqualTo: horaParaBusca).getDocuments().then((snapshot) {
      snapshot.documents.map((DocumentSnapshot document) {
        //Não considera a agenda (minutos) de entrega do pedido em questão para poder salvar a agenda atual em caso de edição
        if ((prcSel != null) && (document.data['numero'] != prcSel.numero)) {
          lstMinutos.remove(document.data['minutoentrega']);
        } else {
          lstMinutos.remove(document.data['minutoentrega']);
        }
        //setState(() {});
      }).toList();
    });
  }

  Widget titleSection = Container(
    padding: const EdgeInsets.all(32.0),
    child: Row(
      children: [
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                padding: const EdgeInsets.only(bottom: 8.0),
                child: Text(
                  'Oeschinen Lake Campground',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Text(
                'Kandersteg, Switzerland',
                style: TextStyle(
                  color: Colors.grey[500],
                ),
              ),
            ],
          ),
        ),
        Icon(
          Icons.star,
          color: Colors.red[500],
        ),
        Text('41'),
      ],
    ),
  );

  //Colocar pra baixo
  List<Widget> _loadWdgNeg(BuildContext context) {
    List<Widget> compraOuVenda = [
      Container(color: Colors.black12, width: MediaQuery.of(context).size.width, padding: new EdgeInsets.all(10.0), child: new Text('Dados do pedido')),
      ListTile(
        leading: Icon(Icons.person_pin),
        title: autoCompleteClientes == null ? Text('Carregando...') : autoCompleteClientes,
      ),
      ListTile(leading: Icon(Icons.account_balance_wallet), title: _listaPagamentos()),
      new ListTile(
        leading: const Icon(Icons.more),
        title: TextFormField(
          controller: observacaoCtrl,
          maxLines: 4,
          keyboardType: TextInputType.text,
          textInputAction: TextInputAction.next,
          textCapitalization: TextCapitalization.sentences,
          focusNode: _observacaoFoco,
          onFieldSubmitted: (term) {
            _fieldFocusChange(context, _observacaoFoco, _datNasFoc);
          },
          decoration: InputDecoration(labelText: 'Observação', labelStyle: TextStyle(fontSize: 18.0)),
        ),
      ),
      ListTile(
        leading: const Icon(Icons.date_range),
        title: new Row(children: <Widget>[
          Container(
              width: MediaQuery.of(context).size.width * 0.30,
              child: InkWell(
                  child: TextFormField(
                    controller: dataController,
                    textInputAction: TextInputAction.next,
                    focusNode: _datNasFoc,
                    enabled: false,
                    decoration: InputDecoration(labelText: 'Entrega', labelStyle: TextStyle(fontSize: 16.0), contentPadding: EdgeInsets.only(top: 24)),
                    validator: (value) {
                      if (value.isEmpty || dataLimite.isBefore(DateTime.now())) {
                        Scaffold.of(context).showSnackBar(SnackBar(
                            content: Text(
                              'Informe uma data para entrega.',
                              style: TextStyle(color: Colors.white),
                            ),
                            backgroundColor: Tema.corAlerta,
                            duration: Duration(seconds: 10)));
                        return;
                      }
                    },
                  ),
                  onTap: () async {
                    dropDownHoras = "";
                    dropDownMinutos = "";
                    DatePicker.showDatePicker(context,
                        cancel: new Text('Voltar'),
                        confirm: new Text('Pronto'),
                        showTitleActions: true,
                        minYear: DateTime.now().year,
                        maxYear: DateTime.now().year + 1,
                        initialYear: DateTime.now().year,
                        initialMonth: DateTime.now().month - 1,
                        initialDate: DateTime.now().day + 1,
                        locale: 'pt',
                        dateFormat: 'dd-mmm-yyyy', onConfirm: (year, month, date) {
                      dataController.updateText(
                          ("00" + date.toString()).substring(date.toString().length) + '/' + ("00" + month.toString()).substring(month.toString().length) + '/' + year.toString());

                      dataLimite = DateFormat("dd/MM/yyyy").parse(dataController.text);
                    });
                    setState(() {});
                  })),
          Container(
              padding: EdgeInsets.only(top: 35, left: 20),
              child: Row(children: <Widget>[
                DropdownButton(
                    isDense: true,
                    value: dropDownHoras,
                    items: lstHoras.map((String val) {
                      //TODO Implementar busca do próximo horário disponível no dia
                      return new DropdownMenuItem<String>(
                        value: val,
                        child: new Text(val),
                      );
                    }).toList(),
                    onChanged: (newVal) async {
                      dropDownHoras = "";
                      setState(() {});
                      await removeEntregasAgendadas(newVal);
                      dropDownHoras = newVal;
                      setState(() {});
                    }),
                Text(" : "),
                dropDownHoras == ""
                    ? Text('')
                    : DropdownButton(
                        isDense: true,
                        value: dropDownMinutos,
                        items: lstMinutos.map((String val) {
                          if (val != null) {
                            return new DropdownMenuItem<String>(
                              value: val,
                              child: new Text(val),
                            );
                          }
                        }).toList(),
                        onChanged: (newVal) {
                          if (newVal != null) {
                            dropDownMinutos = newVal;
                            setState(() {});
                          }
                        })
              ])),
        ]),
      ),
      Container(color: Colors.black12, width: MediaQuery.of(context).size.width, padding: new EdgeInsets.all(10.0), child: new Text('Dados do item')),
      Container(
          height: 320.0,
          color: Colors.grey[200],
          child: Column(children: <Widget>[
            Container(
              padding: EdgeInsets.all(2),
              child: autoCompleteItem == null ? Text('Carregando...') : autoCompleteItem,
            ),
            ListTile(
              contentPadding: EdgeInsets.only(left: 40),
              title: Container(
                padding: EdgeInsets.only(left: 0),
                child: new TextFormField(
                  controller: quantidadeController,
                  keyboardType: TextInputType.numberWithOptions(decimal: true, signed: false),
                  textInputAction: TextInputAction.done,
                  focusNode: _qtdFocus,
                  decoration: InputDecoration(labelText: 'Quantidade'),
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Qual a quantidade para este item?';
                    }
                  },
                ),
              ),
              trailing: Padding(
                  padding: EdgeInsets.all(2),
                  child: Padding(
                      padding: const EdgeInsets.symmetric(vertical: 0.0),
                      child: CupertinoButton(
                          child: Icon(
                            Icons.add_shopping_cart,
                            color: Colors.blueAccent,
                            size: 30.0,
                          ),
                          onPressed: () {
                            adicionaItem();
                          }))),
            ),
            Container(
                width: MediaQuery.of(context).size.width * 0.98,
                height: 200,
                child: itensCarrinho.length > 0
                    ? new SingleChildScrollView(
                        child: new CustomDataTable(
                          columns: <CustomDataColumn>[
                            new CustomDataColumn(label: Text('Produto')),
                            new CustomDataColumn(label: Text('Qtd')),
                            new CustomDataColumn(label: Text('Vlr')),
                            new CustomDataColumn(label: Text('Total')),
                            new CustomDataColumn(label: Text('Apagar')),
                          ],
                          rows: _createRows(),
                        ),
                      )
                    : new Text('')),
          ])),
      Container(color: Colors.black12, width: MediaQuery.of(context).size.width, padding: new EdgeInsets.all(10.0), child: new Text('Totais')),
      Container(
          padding: EdgeInsets.only(left: 40),
          child: TextField(
            decoration: InputDecoration(prefixText: 'Qtd. Itens: ', filled: false, prefixStyle: TextStyle(color: Colors.black)),
            enabled: false,
            controller: qtdIntController,
          )),
      Container(
          padding: EdgeInsets.only(left: 40),
          child: TextField(
            decoration: InputDecoration(prefixText: 'Total sem desconto: ', filled: false, prefixStyle: TextStyle(color: Colors.black)),
            enabled: false,
            controller: precoItensSemDescCtrl,
          )),
      Container(
          padding: EdgeInsets.only(left: 40),
          child: TextField(
            decoration: InputDecoration(prefixText: 'Total com desconto: ', filled: false, prefixStyle: TextStyle(color: Colors.black)),
            enabled: false,
            controller: precoItensComDescCtrl,
          )),
    ];

    return compraOuVenda;
  }

  List<CustomDataRow> _createRows() {
    idxRow = 0;
    List<CustomDataRow> linha = new List<CustomDataRow>();
    linha.clear();
    List<CustomDataCell> colunas = new List<CustomDataCell>();
    colunas.clear();
    itensCarrinho.forEach((item) async {
      double p = stringToDouble(item.precoTotal.toString());
      double precounit = stringToDouble(item.precounit.toString());

      colunas.add(new CustomDataCell(new Text(item.produto)));
      colunas.add(new CustomDataCell(new Text(item.quantidade.toString())));
      colunas.add(new CustomDataCell(new Text(precounit.toString())));
      colunas.add(new CustomDataCell(new Text(p.toString())));
      colunas.add(new CustomDataCell(Material(
          child: InkWell(
        onTap: () {
          //setState(() {
          itensCarrinho.removeAt(idxRow - 1);
          --idxRow;
          calculaTotalItens();
          //});
        },
        child: Container(width: 32, color: Colors.red[200], child: new Icon(Icons.delete)),
      ))));

      CustomDataRow dr = new CustomDataRow.byIndex(cells: colunas, index: idxRow);
      colunas = new List<CustomDataCell>();
      colunas.clear();
      linha.add(dr);
      idxRow++;
    });
    return linha;
  }

  void adicionaItem() {
    if ((dropDownPag == null) || (dropDownPag.desPag.length <= 0)) {
      isSaving = false;
      showDialog(
        context: context,
        builder: (context) => new AlertDialog(
              title: new Text('Erro'),
              content: new Text('Primeiro informe a condição do pagamento'),
              actions: <Widget>[
                new FlatButton(
                  onPressed: () => Navigator.of(context).pop(),
                  child: new Text('OK'),
                ),
              ],
            ),
      );
    }
    if ((objProduto == null) || (objProduto.nomIte.length <= 0)) {
      showDialog(
          context: context,
          builder: (context) => new AlertDialog(title: Text('Erro'), content: Text("Informe o produto"), actions: <Widget>[
                new FlatButton(
                    child: Text('Ok'),
                    onPressed: () {
                      Navigator.pop(context);
                    }),
              ]));
    } else if ((quantidadeController == null) || (quantidadeController.text == "0,00")) {
      showDialog(
          context: context,
          builder: (context) => new AlertDialog(title: Text('Erro'), content: Text("Informe a quantidade"), actions: <Widget>[
                new FlatButton(
                    child: Text('Ok'),
                    onPressed: () {
                      Navigator.pop(context);
                    }),
              ]));
    } else {
      double qtd = stringToDouble(quantidadeController.text);

      double preco = stringToDouble(objProduto.preco.toStringAsFixed(2));

      double total = preco * qtd;

      itensCarrinho.add(new ProcessosItem(
        '',
        '',
        objProduto.nomIte,
        qtd,
        double.parse(total.toStringAsFixed(2)),
        preco,
        //auxVlrCtrl.text,
      ));
      autoCompleteItem.clear();
      quantidadeController.updateValue(0.0);
      calculaTotalItens();
      //setState(() {});
    }
  }

  Future<Null> calculaTotalItens() async {
    precoItensComDescCtrl.text = "0.00";
    precoItensSemDescCtrl.text = "0.00";
    qtdIntController.text = "0";
    bool teveDesconto = false;
    double vlr = 0.00;
    double vlrSemDesc = 0.00;
    double desconto = 0.00;
    if ((itensCarrinho != null) && (itensCarrinho.length > 0)) {

      itensCarrinho.forEach((item) {
        OfertasItens pro = lstItensEmOferta.firstWhere((o) => o.produto == item.produto);
        if(pro != null) {
          teveDesconto = true;
          vlr += (item.precoTotal * (pro.percentual / 100));
        } else {
          vlr += item.precoTotal; // * item.quantidade;
        }
      });

      vlrSemDesc = vlr;

      if (!teveDesconto) {
        if ((dropDownPag != null) && (dropDownPag.percentual > 0)) {
          desconto = (vlr * (dropDownPag.percentual / 100));
          vlr = vlr - desconto;
          //vlr = stringToDouble(vlr.toStringAsFixed(2));
        }
      }

      vlr = double.parse(vlr.toStringAsFixed(2));

      precoItensSemDescCtrl.text = vlrSemDesc > 0 ? vlrSemDesc.toStringAsFixed(2) : '0.00';
      precoItensComDescCtrl.text = vlr > 0 ? vlr.toStringAsFixed(2) : '0.00';
      qtdIntController.text = itensCarrinho.length.toString();
    }
    //Atualiza independente se calculou, pois há objetos que chamam este método e
    //para não atualizar duas vezes (uma ao alterar o objeto e outra ao calcular),
    //sempre seta o estado, independente se calculou
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> formByTip = new List<Widget>();

    return Scaffold(
      appBar: AppBar(
        title: prcSel != null ? Text('Editar Pedido') : Text('Novo Pedido'),
      ),
      body: SingleChildScrollView(
        child: Container(
          key: _formKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Column(children: _loadWdgNeg(context)),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  _botaoConfirma(),
                  Padding(
                    padding: EdgeInsets.only(right: 5.0),
                  ),
                  prcSel == null ? Text('') : _botaoDeletar(),
                ],
              ),
              prcSel == null ? Text('') : _botaoImprimir(),
            ],
          ),
        ),
      ),
    );
  }

  Widget _botaoDeletar() {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 16.0),
      child: PrimaryButton(
        icon: Icon(
          Icons.delete,
          color: Colors.white,
          size: 40.0,
        ),
        onPressed: () {
          showDialog(
            context: context,
            builder: (context) => AlertDialog(
                  title: new Text('Alerta'),
                  content: new Text('Deseja realmente apagar?'),
                  actions: <Widget>[
                    new FlatButton(
                      onPressed: () => Navigator.of(context).pop(),
                      child: new Text('Não'),
                    ),
                    new FlatButton(
                      onPressed: () async {
                        Processos _analise = await Servicos.editProcesso(
                            new Processos(
                              prcSel.idePrc,
                              prcSel.numero,
                              prcSel.cliente,
                              prcSel.observacao,
                              prcSel.condicao,
                              prcSel.nomecondicao,
                              prcSel.perdesc,
                              prcSel.status,
                              prcSel.datGer,
                              prcSel.datApr,
                              prcSel.dataEntrega,
                              prcSel.horaEntrega,
                              prcSel.minutoEntrega,
                              true,
                              prcSel.novoValor,
                              prcSel.aprovador,
                              prcSel.usuario,
                              prcSel.nomeusuario,
                              prcSel.valortotal,
                              prcSel.itens,
                              prcSel.idcliente,
                              //false
                            ),
                            null);

                        Navigator.of(context).pop();
                        Navigator.of(context).pop(_analise);
                      },
                      child: new Text('Sim'),
                    ),
                  ],
                ),
          );
        },
      ),
    );
  }

  Widget _botaoConfirma() {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 16.0),
      child: PrimaryButton(
        icon: Icon(
          Icons.done,
          color: Colors.white,
          size: 50.0,
        ),
        onPressed: isSaving ? null : () => checkAndSubmit(),
      ),
    );
  }

  Widget _botaoImprimir() {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 16.0),
      child: PrimaryButton(
        icon: Icon(
          Icons.print,
          color: Colors.white,
          size: 50.0,
        ),
        onPressed: isSaving ? null : () => geraPdf(),
      ),
    );
  }

  Future<File> get _localFile async {
    final path = await Diretorio().localPath;
    return File('$path/redesip.pdf');
  }

  Future<Null> geraPdf() async {
    final pdf = new PdfDocument(deflate: zlib.encode);
    final page = new PdfPage(pdf, pageFormat: PdfPageFormat.letter);
    final g = page.getGraphics();
    final font = new PdfFont.helvetica(pdf);
    final fontTtf = await rootBundle.load('fonts/Anton-Regular.ttf'); //await Diretorio().fonte;
    PdfTtfFont ttf = new PdfTtfFont(pdf, fontTtf
        /*(new Io.File(await Diretorio().localPath + "/Anton-Regular.ttf")
                .readAsBytesSync() as Uint8List)
            .buffer
            .asByteData()*/
        );

    final top = page.pageFormat.height;
    final largura = page.pageFormat.width;

    ByteData png = await rootBundle.load("images/redesip.png");
    Codec codec = await instantiateImageCodec(png.buffer.asUint8List());
    var frameInfo = await codec.getNextFrame();
    var xxx = await frameInfo.image.toByteData(format: ImageByteFormat.png);
    var image2 = frameInfo.image;

    var bytes = await image2.toByteData();

    PdfImage image = new PdfImage(pdf, image: bytes.buffer.asUint8List(), width: image2.width, height: image2.height);
    g.drawImage(image, 5.0, top - 130.0, 120.0);

    /// Gráficos (criação independente por conta do fillpath)
    g.setColor(new PdfColor(0.0, 0.0, 0.0));
    g.drawRect(0.0, top - 120.0, largura, 1.0);
    g.drawRect(0.0, 40.0, largura, 1.0);
    g.fillPath();

    /// Cabeçalho
    g.setColor(new PdfColor(0.3, 0.3, 0.3));
    g.drawString(ttf, 12.0, "JJGC SUPERMERCADOS LTDA", 50.0 * PdfPageFormat.mm, top - 10.0 * PdfPageFormat.mm);
    g.drawString(ttf, 12.0, "CNPJ: 19.192.939/0001-23", 50.0 * PdfPageFormat.mm, top - 20.0 * PdfPageFormat.mm);
    g.drawString(ttf, 12.0, "Rua Castro Alves, 170 - N. SRA. DA ABADIA - Uberaba/MG, Brasil", 50.0 * PdfPageFormat.mm, top - 30.0 * PdfPageFormat.mm);
    g.drawString(ttf, 12.0, "CEP: 38.025-380", 50.0 * PdfPageFormat.mm, top - 40.0 * PdfPageFormat.mm);

    String sTipPrc = "Cliente: " + prcSel.cliente;
    /*if (prc.tipPrc.toLowerCase().contains('neg')) {
      sTipPrc += "de " + prc.tipPrc;
    } else {
      sTipPrc += "da oportunidade de " + prc.tipPrc;
    }*/
    g.drawString(font, 16.0, sTipPrc, 210, top - 135.0);

    /// Retangulo cinza
    g.setColor(new PdfColor(0.8, 0.8, 0.8));
    g.drawRect(0, top - 260, largura, 20);
    g.fillPath();

    g.setColor(new PdfColor(0.8, 0.8, 0.8));
    g.drawRect(280, 45, 1, 500);
    g.fillPath();

    /// Continua cor preto
    g.setColor(new PdfColor(0.0, 0.0, 0.0));
    g.fillPath();

    String sConfirma = "Pedido: " + prcSel.numero.toString();

    String sGuarde = prcSel.status == 'ABE' ? 'Status: Aguardando aprovação' : prcSel.status == 'APR' ? 'Status: Aprovado' : 'Status: Entregue';

    String sContato = "Condição de pagamento: " + prcSel.nomecondicao + " / Valor total (R\$): " + precoItensComDescCtrl.text;

    String sObservacao = "Observacão: " + prcSel.observacao;

    //g.drawString(font, 12.0, sConfirma, 10, top - 160.0);
    sGuarde = sConfirma + " - " + sGuarde;
    g.drawString(font, 12.0, sGuarde, 10, top - 180.0);
    g.drawString(font, 12.0, sContato, 10, top - 195.0);
    if (sObservacao.length > 100) {
      // Linha 1
      g.drawString(font, 12.0, sObservacao.substring(0, 100), 10, top - 215.0);
      sObservacao = sObservacao.substring(101, sObservacao.length);
      // Linha 2
      if ((sObservacao.length > 0) && (sObservacao.length <= 100)) {
        g.drawString(font, 12.0, "   " + sObservacao.substring(0, sObservacao.length), 10, top - 225.0);
        sObservacao = "";
      } else if (sObservacao.length > 0) {
        g.drawString(font, 12.0, "   " + sObservacao.substring(0, 100), 10, top - 225.0);
        sObservacao = sObservacao.substring(101, sObservacao.length);
      }
      // Linha 3
      if ((sObservacao.length > 0) && (sObservacao.length <= 100)) {
        g.drawString(font, 12.0, "   " + sObservacao.substring(0, sObservacao.length), 10, top - 230.0);
        sObservacao = "";
      } else if (sObservacao.length > 0) {
        g.drawString(font, 12.0, "   " + sObservacao.substring(0, 100), 10, top - 230.0);
        sObservacao = sObservacao.substring(101, sObservacao.length);
      }
    } else {
      g.drawString(font, 12.0, sObservacao, 10, top - 215.0);
    }

    int posicao = top.toInt() - 270;
    itensCarrinho.forEach((i) {
      String sProduto = i.produto;
      auxVlrCtrl.text = i.precoTotal.toStringAsFixed(2);
      g.drawString(font, 8.0, sProduto, 2.0, posicao.toDouble());
      g.drawString(font, 8.0, i.quantidade.toString(), 225.0, posicao.toDouble());
      g.drawString(font, 8.0, auxVlrCtrl.text, 255.0, posicao.toDouble());
      posicao = posicao - 10;
    });
    //g.drawString(font, 16.0, "Identificação única do pedido", 190.0, top - 230);

    /*if (prc.tipPrc.toLowerCase().contains("neg")) {
      sQtdRel += "oportunidades relacionadas: " + prc.qtdRlc.toString() + ".";
    } else {
      sQtdRel += "Negócio relacionado: " + prc.ideNeg + ".";
    }*/
    g.drawString(font, 11.0, 'Produto', 3.0, top - 255);
    g.drawString(font, 11.0, 'Qtde.', 222.0, top - 255);
    g.drawString(font, 11.0, 'Vlr.', 262.0, top - 255);
    g.drawString(font, 11.0, 'Produto', 300.0, top - 255);
    g.drawString(font, 11.0, 'Qtde.', 556.0, top - 255);
    g.drawString(font, 11.0, 'Vlr.', 590.0, top - 255);

    String aVarAux = DateFormat('dd/MM/yyyy HH:mm').format(new DateTime.now());

    /// Rodapé
    g.setColor(new PdfColor(0.0, 0.0, 0.0));
    g.fillPath();
    g.drawString(
        font,
        11.0,
        "Data e hora local da visualização: " + aVarAux /*+ " / Identificação da impressão: "*/
        ,
        5.0 * PdfPageFormat.mm,
        15.0);

    final file = await _localFile;

    file.writeAsBytesSync(pdf.save());
    OpenFile.open(file.path);
  }

  void checkAndSubmit() async {
    setState(() {
      isSaving = true;
    });

    String dateWithT = DateFormat('yyyyMMdd').format(dataLimite);
    //+ 'T' + ("00" + horLimSel.toString()).substring(horLimSel.toString().length) + ':00:00';
    Timestamp datLim = Timestamp.fromDate(dataLimite);

    //Verifica se a condicação de pagamento foi selecionada
    if ((autoCompleteClientes == null) || (autoCompleteClientes.textField.controller.text.length <= 0)) {
      isSaving = false;
      showDialog(
        context: context,
        builder: (context) => new AlertDialog(
              title: new Text('Erro'),
              content: new Text('Informe o cliente'),
              actions: <Widget>[
                new FlatButton(
                  onPressed: () => Navigator.of(context).pop(),
                  child: new Text('OK'),
                ),
              ],
            ),
      );
    } else if ((dropDownPag == null) || (dropDownPag.desPag.length <= 0)) {
      isSaving = false;
      showDialog(
        context: context,
        builder: (context) => new AlertDialog(
              title: new Text('Erro'),
              content: new Text('Informe a condição do pagamento'),
              actions: <Widget>[
                new FlatButton(
                  onPressed: () => Navigator.of(context).pop(),
                  child: new Text('OK'),
                ),
              ],
            ),
      );
    } else if ((itensCarrinho == null) || (itensCarrinho.length <= 0)) {
      isSaving = false;
      showDialog(
        context: context,
        builder: (context) => new AlertDialog(
              title: new Text('Erro'),
              content: new Text('Informe ao menos um produto'),
              actions: <Widget>[
                new FlatButton(
                  onPressed: () => Navigator.of(context).pop(),
                  child: new Text('OK'),
                ),
              ],
            ),
      );
    } else if ((dropDownMinutos == null) || (dropDownMinutos.length <= 0)) {
      isSaving = false;
      showDialog(
        context: context,
        builder: (context) => new AlertDialog(
              title: new Text('Erro'),
              content: new Text('Informe a hora/minuto de entrega'),
              actions: <Widget>[
                new FlatButton(
                  onPressed: () => Navigator.of(context).pop(),
                  child: new Text('OK'),
                ),
              ],
            ),
      );
    } else {
      //Criar novo negócio
      if (prcSel == null) {
        Processos n = Processos(
          '',
          0,
          autoCompleteClientes.textField.controller.text,
          observacaoCtrl.text,
          dropDownPag.idePrc,
          dropDownPag.desPag,
          dropDownPag.percentual,
          STAPRC.ABE.toString().split('.')[1],
          Timestamp.now(),
          null,
          datLim,
          dropDownHoras,
          dropDownMinutos,
          false,
          false,
          '',
          MainPage.usuario.usuario,
          MainPage.usuario.primeiroNome,
          stringToDouble(precoItensComDescCtrl.text.replaceAll(",", ".")),
          int.parse(qtdIntController.text),
          dropDownCli.idePrc,
          //false
        );
        Servicos.addProcesso(n, itensCarrinho).then((t) {
          if (t != null) {
            //Inseriu com sucesso
            Navigator.of(context).pop(t);
          }
        });
      }
      //Editar negócio existente
      if (prcSel != null) {
        //Abriu tela por um negócio existente
        Processos n = Processos(
          prcSel.idePrc,
          prcSel.numero,
          autoCompleteClientes.textField.controller.text,
          observacaoCtrl.text,
          dropDownPag.idePrc,
          dropDownPag.desPag,
          dropDownPag.percentual,
          prcSel.status,
          prcSel.datGer,
          prcSel.datApr,
          datLim,
          dropDownHoras,
          dropDownMinutos,
          false,
          prcSel.novoValor,
          prcSel.aprovador,
          prcSel.usuario,
          prcSel.nomeusuario,
          double.parse(precoItensComDescCtrl.text.replaceAll(",", ".")),
          int.parse(qtdIntController.text),
          dropDownCli.idePrc,
          //false
        );
        await Servicos.editProcesso(n, itensCarrinho).then((neg) {
          Navigator.of(context).pop(neg);
        });
      }
    }
  }

  Widget _listaPagamentos() {
    if (lstPag.isEmpty) {
      return const Text('Carregando');
    }

    return DropdownButton<Pagamentos>(
      hint: Text("Condição de pagamento"), //_chgLoc ? Text(dropDownEstado.nomEst) : Text("Selecione o estado"),
      value: dropDownPag,
      onChanged: (Pagamentos novoPag) {
        dropDownPag = novoPag;
        calculaTotalItens();
        //setState(() {});
      },
      items: lstPag.map((Pagamentos pag) {
        //Substitui o objeto pelo recebido no método
        return DropdownMenuItem(
          child: Text(pag.desPag), //_chgLoc ? Text(dropDownEstado.nomEst) : Text(estado.nomEst),
          value: pag,
        );
      }).toList(),
    );
    //}
  }

  // Converte uma string para double, reduzindo os decimais para 2 dígitos
  double stringToDouble(String s1) {
    String s3 = "";
    if ((s1 != null) && (s1.length > 0) && (s1.contains(","))) {
      s1 = s1.replaceAll(".", "");
      s1 = s1.replaceAll(",", ".");
      s3 = double.parse(s1).toStringAsFixed(2);
      return double.parse(s3);
    }
    num x = s1.lastIndexOf(".");
    String inicio = s1.substring(0, x);
    String decimal = s1.substring(x, s1.length);
    if (inicio.contains(".")) {
      s1 = inicio.replaceAll(".", "") + decimal;
    }
    s3 = s1;
    return double.parse(s3);
  }
}
