import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:intl/intl.dart';
import 'package:redesipapp/classes.dart';
import 'package:redesipapp/pages/mainPage.dart';
import 'package:redesipapp/servicos.dart';
import 'package:redesipapp/style/primary_button.dart';

/// Autor(es): Diogo Roosarnezi
/// Criação: 14/08
/// Função: mostra ao usuário as informações para que ele possa conferir
/// Parâmetros: Usuario usuario

class ConfirmaDadosPage extends StatelessWidget {
  static Usuario usuario;
  final bool isCpf = usuario.nroCpf.toString().length <= 11;
  final primeiroNomeController =
      TextEditingController(text: ConfirmaDadosPage.usuario.primeiroNome);
  final segundoNomeController =
      TextEditingController(text: ConfirmaDadosPage.usuario.ultimoNome);
  final cpfController = new MaskedTextController(
      text: ConfirmaDadosPage.usuario.nroCpf.toString(),
      mask: '000.000.000-00');
  final cnpjController = new MaskedTextController(
      text: ConfirmaDadosPage.usuario.nroCnp.toString(),
      mask: '00.000.000/0000-00');
  final telefoneController = new MaskedTextController(
      mask: '(00)0000-00000',
      text: ConfirmaDadosPage.usuario.telefone.toString());
  final emailController =
      new TextEditingController(text: ConfirmaDadosPage.usuario.email);
  final dataController = new MaskedTextController(
      text: DateFormat('dd/MM/yyyy').format(ConfirmaDadosPage.usuario.dataNasc.toDate()),
      mask: '00/00/0000');
  final DateTime dataNascimento = ConfirmaDadosPage.usuario.dataNasc.toDate();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: new AppBar(
          title: Text('Confirmando dados pessoais'),
        ),
        body:
            /*OrientationBuilder(
          builder: (context, orientation) {
            return GridView.count(
              // Create a grid with 2 columns in portrait mode, or 3 columns in
              // landscape mode.
              crossAxisCount: orientation == Orientation.portrait ? 2 : 3,
              // Generate 100 Widgets that display their index in the List
              children: <Widget>[*/
            SingleChildScrollView(
          child: (new Column(
            children: <Widget>[
              new ListTile(
                leading: const Icon(Icons.account_box),
                title: TextFormField(
                    enabled: false,
                    controller: primeiroNomeController,
                    decoration: InputDecoration(
                        labelText: 'Primeiro nome',
                        labelStyle: TextStyle(fontSize: 18.0)),
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Digite seu nome';
                      }
                    }),
              ),
              new ListTile(
                leading: const Icon(
                  Icons.keyboard_arrow_right,
                  color: Colors.white,
                ),
                title: TextFormField(
                    enabled: false,
                    controller: segundoNomeController,
                    decoration: InputDecoration(
                        labelText: 'Segundo nome',
                        labelStyle: TextStyle(fontSize: 18.0)),
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Digite seu Sobrenome';
                      }
                    }),
              ),
              new Column(
                children: formDados(),
              ),
              new ListTile(
                leading: new Icon(
                  Icons.calendar_today,
                ),
                title: new TextFormField(
                  controller: dataController,
                  enabled: false,
                  decoration: InputDecoration(
                      labelText: 'Data de Nascimento',
                      labelStyle: TextStyle(fontSize: 16.0)),
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Informe sua data de nascimento';
                    }
                  },
                ),
              ),
              new ListTile(
                leading: new Icon(Icons.local_phone),
                title: TextFormField(
                    enabled: false,
                    controller: telefoneController,
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                        labelText: 'Telefone',
                        labelStyle: TextStyle(fontSize: 18.0)),
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Digite um telefone para contato';
                      }
                    }),
              ),
              new ListTile(
                leading: new Icon(Icons.email),
                title: TextFormField(
                    enabled: false,
                    controller: emailController,
                    decoration: InputDecoration(
                        labelText: 'E-mail para contato',
                        labelStyle: TextStyle(fontSize: 18.0)),
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Digite um e-mail para contato';
                      }
                    }),
              ),
              Padding(
                padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 4.0),
                child: new Text(
                  'Observe atentamente os dados informados e confirme ou corrija.',
                  style: TextStyle(fontSize: 16.0),
                ),
              ),
              new Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  PrimaryButton(
                    icon: Icon(
                      Icons.backspace,
                      color: Colors.white,
                      size: 40.0,
                    ),
                    onPressed: () async {
                      Navigator.pop(context);
                    },
                  ),
                  Padding(
                    padding: EdgeInsets.all(5.0),
                  ),
                  PrimaryButton(
                    icon: Icon(
                      Icons.done,
                      color: Colors.white,
                      size: 50.0,
                    ),
                    onPressed: () async {
                      int cpfCnpj = isCpf
                          ? int.parse(_substituiCaractere(cpfController.text))
                          : int.parse(_substituiCaractere(cnpjController.text));
                      String pesquisa = isCpf ? 'NROCPF' : 'NROCNP';
                      var lista = await Firestore.instance
                          .collection('administradores')
                          .where(pesquisa, isEqualTo: cpfCnpj)
                          .getDocuments();

                      Servicos.addUsuario(new Usuario(
                              '',
                              ConfirmaDadosPage.usuario.email,
                              primeiroNomeController.text,
                              segundoNomeController.text,
                              int.parse(
                                  _substituiCaractere(cpfController.text)),
                              int.parse(
                                  _substituiCaractere(cnpjController.text)),
                              dataNascimento as Timestamp,
                              emailController.text,
                              int.parse(
                                  _substituiCaractere(telefoneController.text)),
                              lista.documents.isNotEmpty,
                              '',
                              false,
                              //'',
                              ''))
                          .then((docId) {
                        Navigator.pushAndRemoveUntil(
                            context,
                            MaterialPageRoute(
                                builder: (context) =>
                                    MainApp(idUsuario: docId)),
                            ((Route<dynamic> route) => false));
                      });
                    },
                  )
                ],
              ),
            ],
          )),
        )
        //]);
        //}),
        );
  }

  /// Autor: Diogo Roos
  /// Função: Decide qual o controlador será usado. CPF ou CNPJ

  List<Widget> formDados() {
    if (isCpf) {
      return [
        new ListTile(
          enabled: false,
          leading: Text(''),
          title: TextFormField(
              controller: cpfController,
              decoration: InputDecoration(labelText: 'CPF'),
              keyboardType: TextInputType.number,
              validator: (value) {
                if (value.isEmpty) {
                  return 'Informe seu CPF';
                }
              }),
        )
      ];
    }
    return [
      new ListTile(
        leading: Text(''),
        title: TextFormField(
            enabled: false,
            controller: cnpjController,
            decoration: InputDecoration(labelText: 'CNPJ'),
            keyboardType: TextInputType.number,
            validator: (value) {
              if (value.isEmpty) {
                return 'Informe seu CNPJ';
              }
            }),
      )
    ];
  }

  String _substituiCaractere(String str) {
    return str
        .replaceAll('.', '')
        .replaceAll('-', '')
        .replaceAll('/', '')
        .replaceAll('(', '')
        .replaceAll(')', '');
  }
}
