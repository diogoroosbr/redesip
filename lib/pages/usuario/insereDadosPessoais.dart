import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:intl/intl.dart';
import 'package:redesipapp/classes.dart';
import 'package:redesipapp/pages/mainPage.dart';
import 'package:redesipapp/pages/usuario/confirmaDados.dart';
import 'package:redesipapp/servicos.dart';
import 'package:redesipapp/style/primary_button.dart';
import 'package:redesipapp/style/theme.dart';

enum FormType { cpf, cnpj }

/// Autor(es):Diogo Roos
/// Criação: 25/07/2018
/// Função: cria a página de cadastro de dados pessoais e cria
/// um registro dentro do banco de dados.
/// Parâmetros: Id do usuário

class InsereDadosPage extends StatelessWidget {
  InsereDadosPage({
    Key key,
    this.ideUsu,
    this.isEditing,
  }) : super(key: key);
  static const String routeName = "/insereDadosPessoais";
  final FirebaseUser ideUsu;
  final bool isEditing;

  @override
/*   InsereDadosPageState createState() {
    InsereDadosPageState.user = ideUsu;
    return InsereDadosPageState();
  } */
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: AppBar(title: Text('Cadastro Pessoal')),
        body: FormularioInsere(
          ideUsu: ideUsu,
          isEditing: isEditing,
        ));
  }
}

class FormularioInsere extends StatefulWidget {
  FormularioInsere({
    Key key,
    this.ideUsu,
    this.isEditing,
  }) : super(key: key);

  final FirebaseUser ideUsu;
  final bool isEditing;

  @override
  State<StatefulWidget> createState() {
    InsereDadosPageState.user = ideUsu;
    return InsereDadosPageState();
  }
}

class InsereDadosPageState extends State<FormularioInsere> {
  static FirebaseUser user;
  final _formKey = new GlobalKey<FormState>();
  final primeiroNomeController = TextEditingController(
      text: user != null ? user.displayName : MainPage.usuario.primeiroNome);
  final segundoNomeController = TextEditingController(
      text: user != null ? user.displayName : MainPage.usuario.ultimoNome);
  final cpfController = new MaskedTextController(
      text: user != null ? null : MainPage.usuario.nroCpf.toString(),
      mask: '000.000.000-00');
  final cnpjController = new MaskedTextController(
      text: user != null ? null : MainPage.usuario.nroCnp.toString(),
      mask: '00.000.000/0000-00');
  final telefoneController = new MaskedTextController(
      mask: '(00)0000-00000',
      text: user != null
          ? user.phoneNumber
          : MainPage.usuario.telefone.toString());
  final emailController = new TextEditingController(
      text: user != null ? user.email : MainPage.usuario.email);
  final dataController = new MaskedTextController(
      text: MainPage.usuario != null
          ? DateFormat('dd/MM/yyyy').format(MainPage.usuario.dataNasc.toDate())
          : new DateFormat('dd/MM/yyyy')
              .format(DateTime.now().subtract(const Duration(days: 18 * 365))),
      mask: '00/00/0000');

  Timestamp dataNascimento = MainPage.usuario != null
      ? MainPage.usuario.dataNasc
      : DateTime.now().subtract(const Duration(days: 30 * 365));

  static bool reverse = false;
  bool isCpf =
      user != null ? true : MainPage.usuario.nroCpf.toString().length <= 11;

  DateTime datSel = new DateTime.now().subtract(Duration(days: 365 * 17));
  DateTime datAux = new DateTime.now().subtract(Duration(days: 365 * 17));

  final FocusNode _priNomFoc = FocusNode();
  final FocusNode _segNomFoc = FocusNode();
  final FocusNode _tipPesFoc = FocusNode();
  final FocusNode _cpfCnpFoc = FocusNode();
  final FocusNode _datNasFoc = FocusNode();
  final FocusNode _nroTelFoc = FocusNode();
  final FocusNode _emaUsuFoc = FocusNode();

  _fieldFocusChange(
      BuildContext context, FocusNode currentFocus, FocusNode nextFocus) {
    currentFocus.unfocus();
    FocusScope.of(context).requestFocus(nextFocus);
  }

  @override
  initState() {
    super.initState();
    reverse = false;
  }

  @override
  Widget build(BuildContext context) {
    if (widget.isEditing) {
      telefoneController.addListener(() {
        reverse = true;
      });
      emailController.addListener(() {
        reverse = true;
      });
    }

    return new Form(
        key: _formKey,
        child: SingleChildScrollView(
          reverse: reverse,
          child: Column(
            children: <Widget>[
              new ListTile(
                leading: const Icon(Icons.account_box),
                title: TextFormField(
                    controller: primeiroNomeController,
                    textCapitalization: TextCapitalization.words,
                    textInputAction: TextInputAction.next,
                    focusNode: _priNomFoc,
                    onFieldSubmitted: (term) {
                      _fieldFocusChange(context, _priNomFoc, _segNomFoc);
                    },
                    decoration: InputDecoration(
                        labelText: 'Primeiro Nome',
                        labelStyle: TextStyle(fontSize: 18.0)),
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Digite seu nome';
                      }
                    }),
              ),
              new ListTile(
                leading: const Icon(
                  Icons.keyboard_arrow_right,
                  color: Colors.white,
                ),
                title: TextFormField(
                    controller: segundoNomeController,
                    textCapitalization: TextCapitalization.words,
                    textInputAction: TextInputAction.next,
                    focusNode: _segNomFoc,
                    onFieldSubmitted: (term) {
                      _fieldFocusChange(context, _segNomFoc, _cpfCnpFoc);
                    },
                    decoration: InputDecoration(
                        labelText: 'Último Nome',
                        labelStyle: TextStyle(fontSize: 18.0)),
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Digite seu sobrenome';
                      }
                    }),
              ),
              new Column(
                children: campoCpfCnp(),
              ),
              new ListTile(
                leading: new Icon(
                  Icons.calendar_today,
                ),
                title: new TextFormField(
                  controller: dataController,
                  textInputAction: TextInputAction.next,
                  //focusNode: _datNasFoc,
                  //onFieldSubmitted: (term) {
                  //  _fieldFocusChange(context, _datNasFoc, _nroTelFoc);
                  //},
                  enabled: false,
                  decoration: InputDecoration(
                      labelText: 'Data Nascimento',
                      labelStyle: TextStyle(fontSize: 16.0)),
                  validator: (value) {
                    if (value.isEmpty ||
                        dataNascimento.toDate().isAfter(DateTime.now()
                            .subtract(const Duration(days: 18 * 365)))) {
                      String mensagem;
                      value.isEmpty
                          ? mensagem = 'Informe sua data de nascimento'
                          : mensagem =
                              'O usuário deve ter no mínimo dezoito anos';
                      Scaffold.of(context).showSnackBar(SnackBar(
                          content: Text(
                            mensagem,
                            style: TextStyle(color: Colors.white),
                          ),
                          backgroundColor: Tema.corAlerta,
                          duration: Duration(seconds: 10)));
                      return mensagem;
                    }
                  },
                ),
                onTap: () async {
                  print('data ' + dataNascimento.toString());
                  FocusScope.of(context).requestFocus(new FocusNode());
                  await new Future.delayed(
                      Duration(milliseconds: 200),
                      () => showDatePicker(
                                  initialDatePickerMode: DatePickerMode.year,
                                  context: context,
                                  initialDate: dataNascimento.toDate(),
                                  firstDate: DateTime.now().subtract(
                                      const Duration(days: 100 * 365)),
                                  lastDate: DateTime.now()
                                      .subtract(const Duration(days: 17 * 365)))
                              .then((DateTime value) {
                            if (value != null) {
                              dataController.text =
                                  DateFormat('dd/MM/yyyy').format(value);
                              dataNascimento = Timestamp.fromDate(value);
                              _fieldFocusChange(
                                  context, _datNasFoc, _nroTelFoc);
                              setState(() {});
                            }
                          }));
                },
              ),
              new ListTile(
                leading: new Icon(Icons.local_phone),
                title: TextFormField(
                    controller: telefoneController,
                    textInputAction: TextInputAction.next,
                    focusNode: _nroTelFoc,
                    onFieldSubmitted: (term) {
                      _fieldFocusChange(context, _nroTelFoc, _emaUsuFoc);
                    },
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                        labelText: 'Telefone',
                        labelStyle: TextStyle(fontSize: 18.0)),
                    validator: (value) {
                      if (value.isEmpty || value.length < 13) {
                        return 'Digite um telefone válido';
                      }
                    }),
              ),
              new ListTile(
                leading: new Icon(Icons.alternate_email),
                title: TextFormField(
                    keyboardType: TextInputType.emailAddress,
                    controller: emailController,
                    textInputAction: TextInputAction.done,
                    focusNode: _emaUsuFoc,
                    decoration: InputDecoration(
                        labelText: 'E-mail para contato',
                        labelStyle: TextStyle(fontSize: 18.0)),
                    validator: (value) {
                      if (value.isEmpty /*  || value.contains('@' + '.') */) {
                        return 'Digite um e-mail válido';
                      }
                    }),
              ),
              new Column(children: [
                salvaOuEdita(),
              ]),
            ],
          ),
        ));
  }

  Widget padded({Widget child}) {
    return new Padding(
      padding: EdgeInsets.symmetric(vertical: 8.0),
      child: child,
    );
  }

  /// Autor: Diogo Roos
  /// Função: Decide qual o controlador será usado. CPF ou CNPJ

  List<Widget> campoCpfCnp() {
    if (isCpf) {
      return [
        new ListTile(
          leading: Text(''),
          title: TextFormField(
              controller: cpfController,
              textInputAction: TextInputAction.done,
              focusNode: _cpfCnpFoc,
              //onFieldSubmitted: (term) {
              //  _fieldFocusChange(context, _cpfCnpFoc, _datNasFoc);
              //},
              decoration: InputDecoration(labelText: 'CPF'),
              keyboardType: TextInputType.number,
              validator: (value) {
                if (value.isEmpty || value.length < 14) {
                  return 'Informe seu CPF';
                }
              }),
        )
      ];
    }
    return [
      new ListTile(
        leading: Text(''),
        title: TextFormField(
            enabled: !widget.isEditing ? true : !MainPage.usuario.admin,
            controller: cnpjController,
            textInputAction: TextInputAction.done,
            focusNode: _cpfCnpFoc,
            //onFieldSubmitted: (term) {
            //  _fieldFocusChange(context, _cpfCnpFoc, _datNasFoc);
            //},
            decoration: InputDecoration(labelText: 'CNPJ'),
            keyboardType: TextInputType.number,
            validator: (value) {
              if (value.isEmpty || value.length < 18) {
                return 'Informe seu CNPJ';
              }
            }),
      )
    ];
  }

  /// Modificado por: Diogo Roos
  /// Data Modificado : 08/08/2018
  /// Modificação: Parametro Local
  /// Motivo: Chamada da função para editar o local

  Widget salvaOuEdita() {
    if (widget.isEditing == false) {
      return Padding(
        padding: const EdgeInsets.symmetric(vertical: 16.0),
        child: PrimaryButton(
          icon: Icon(
            Icons.done,
            color: Colors.white,
            size: 50.0,
          ),
          onPressed: () async {
            if (_formKey.currentState.validate()) {
              ConfirmaDadosPage.usuario = new Usuario(
                '',
                widget.ideUsu.email,
                primeiroNomeController.text,
                segundoNomeController.text,
                //int.parse(_substituiCaractere(cpfController.text)),
                cpfController.text.length > 0
                    ? int.parse(_substituiCaractere(cpfController.text))
                    : 0,
                cnpjController.text.length > 0
                    ? int.parse(_substituiCaractere(cnpjController.text))
                    : 0,
                dataNascimento as Timestamp,
                emailController.text,
                int.parse(_substituiCaractere(telefoneController.text)),
                false,
                '',
                false,
                //''
                ''
              );

              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => ConfirmaDadosPage()),
              );
            }
          },
        ),
      );
    } else {
      return Padding(
        padding: const EdgeInsets.symmetric(vertical: 16.0),
        child: PrimaryButton(
          icon: Icon(
            Icons.done,
            color: Colors.white,
            size: 50.0,
          ),
          onPressed: () async {
            if (_formKey.currentState.validate()) {
              await Servicos.editUsuario(
                  new Usuario(
                    MainPage.usuario.docId,
                    MainPage.usuario.usuario,
                    primeiroNomeController.text,
                    segundoNomeController.text,
                    int.parse(_substituiCaractere(cpfController.text)),
                    int.parse(_substituiCaractere(cnpjController.text)),
                    dataNascimento as Timestamp,
                    emailController.text,
                    int.parse(_substituiCaractere(telefoneController.text)),
                    MainPage.usuario.admin,
                    MainPage.usuario.token,
                    MainPage.usuario.tutorial,
                    //''
                    MainPage.versao
                  ),
                  context);
              Navigator.of(context).pop(true);
            }
          },
        ),
      );
    }
  }

  void seletor(e) {
    setState(() {
      if (e == true) {
        isCpf = true;
        cnpjController.clear();
      } else {
        isCpf = false;
        cpfController.clear();
      }
    });
  }

  String mostraData = ('Data');
}

String _substituiCaractere(String str) {
  if (str == null || str.length == 0) {
    str = "";
  }
  return str
      .replaceAll('.', '')
      .replaceAll('-', '')
      .replaceAll('/', '')
      .replaceAll('(', '')
      .replaceAll(')', '');
}
