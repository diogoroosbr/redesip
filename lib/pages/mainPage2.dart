import 'dart:io' as Io;
import 'dart:ui';

import 'package:flutter/services.dart' show ByteData, rootBundle;
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:image/image.dart' as Img;
import 'package:intl/intl.dart';
import 'package:open_file/open_file.dart';
import 'package:pdf/pdf.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:redesipapp/EnviaProdutos.dart';
import 'package:redesipapp/autenticacao/baseAuth.dart';
import 'package:redesipapp/classes.dart';
import 'package:redesipapp/pages/cadastros/editarCliente.dart';
import 'package:redesipapp/pages/cadastros/editarProduto.dart';
import 'package:redesipapp/pages/cadastros/listarClientes.dart';
import 'package:redesipapp/pages/cadastros/listarPagamentos.dart';
import 'package:redesipapp/pages/cadastros/listarProdutos.dart';
import 'package:redesipapp/pages/login/cadastro.dart';
import 'package:redesipapp/pages/login/loginUsuario.dart';
import 'package:redesipapp/pages/pedidos/editarNegocio.dart';
import 'package:redesipapp/pages/usuario/insereDadosPessoais.dart';
import 'package:redesipapp/servicos.dart';
import 'package:redesipapp/style/theme.dart';
//import 'package:permission/permission.dart';
//import 'package:simple_permissions/simple_permissions.dart';
//import 'package:redesipapp/pages/pedidos//editarProcesso.dart';

enum STAPRC { ABE, APR, ENT }
enum OrderBy { desativado, crescente, decrescente }

String editada;

/// Autor(es): Diogo Roosernando
/// Criação: 16/08/2018
/// Função: Montar a tela do Representante
/// Parâmetros: idUsuario

class MainApp2 extends StatelessWidget {
  MainApp2({Key key, this.idUsuario}) : super(key: key);
  final String idUsuario;

  @override
  Widget build(BuildContext context) {
    /*Navigator.pushReplacement(
        context,
        new MaterialPageRoute(
            builder: (context) => new MainApp(
              idUsuario: idUsuario,
            )));
    */

    return new MaterialApp(
      title: 'Rede SIP CRM',
      locale: const Locale('pt', 'BR'),
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      supportedLocales: [
        //const Locale('pt', 'BR'),
        //const Locale('en', 'US'),
        const Locale('pt', 'BR'),
      ],
      home: new MainPage2(
        idUsuario: idUsuario,
        title: 'Rede SIP CRM',
      ),
      theme: new ThemeData(primarySwatch: Tema.swatchPrimario, primaryColor: Tema.corPrimaria),
      routes: <String, WidgetBuilder>{
        MainPage2.routeName: (BuildContext context) => new MainPage2(),
        //EditarProcessoPage.routeName: (BuildContext context) => new EditarProcessoPage(),
        InsereDadosPage.routeName: (BuildContext context) => new InsereDadosPage(),
        //LoginPage.routeName: (BuildContext context) => new LoginPage(),
        //LoginUsuarioPage.routeName: (BuildContext context) => new LoginUsuarioPage(),
        CadastroPage.routeName: (BuildContext context) => new CadastroPage(),
      },
    );
  }
}

class MainPage2 extends StatefulWidget {
  MainPage2({Key key, this.title, this.idUsuario}) : super(key: key);
  static const String routeName = "/mainPage";
  static FiltroProcesso filtroAnalise = new FiltroProcesso.vazio();
  final String title;
  final String idUsuario;
  static Usuario usuario;

  @override
  MainPageState2 createState() => new MainPageState2();
}

class MainPageState2 extends State<MainPage2> with TickerProviderStateMixin {
  TabController tabController;
  AnimationController controller;
  Animation<Color> animation;

  bool isOpened = false;
  AnimationController _animationController;
  Animation<Color> _buttonColor;
  Animation<double> _animateIcon;
  Animation<double> _translateButton;
  Curve _curve = Curves.easeOut;
  double _fabHeight = 56.0;

  List<Processos> listaPedido = new List<Processos>();
  List<Processos> listaPedidoFiltro = new List<Processos>();
  List<Processos> listaPedidoFiltro2 = new List<Processos>();
  List<Processos> listaPedidoFiltro3 = new List<Processos>();
  //List<Local> _listaLocal = new List<Local>();
  final FirebaseMessaging _firebaseMessaging = new FirebaseMessaging();
  List<FiltroLocal> listaTotal = new List<FiltroLocal>();
  //List<Usuario> listaFaltando = new List<Usuario>();
  String pesquisa = '';
  TextEditingController ctrPesq = new TextEditingController();

  List<ProcessosItem> itensCarrinho = new List<ProcessosItem>();
  final auxVlrCtrl = MoneyMaskedTextController(decimalSeparator: ',', thousandSeparator: '.');
  MoneyMaskedTextController precoItensComDescCtrl = new MoneyMaskedTextController();
  MoneyMaskedTextController precoItensSemDescCtrl = new MoneyMaskedTextController();

  NumberFormat moneyFormatter = new NumberFormat();
  MoneyMaskedTextController valorController =
      /*negSel != null
      ? MoneyMaskedTextController(
          initialValue: prcSel.uniMed != "Sacas" ? prcSel.qtdIte : 0.0,
        )
      :*/
      new MoneyMaskedTextController();
  //String _local = '';
  //Local _local;
  OrderBy orderByCul;
  OrderBy orderByData;
  OrderBy orderByLoc;

  String turno = "";

  /// Autor: Diogo Roosernando
  /// Função: Realiza a animação de quando a situalçao de uma análise é alterada

  void doAnimation() {
    controller = new AnimationController(duration: const Duration(milliseconds: 2000), vsync: this);
    animation = ColorTween(begin: Tema.corAlerta, end: Colors.white).animate(controller)
      ..addListener(() {
        if (this.animation.isCompleted) {
          editada = '';
          controller.reset();
        }
        setState(() {});
      });
    controller.forward();
  }

  @override
  void initState() {
    moneyFormatter.significantDigitsInUse = true;

    _animationController = AnimationController(vsync: this, duration: Duration(milliseconds: 500))
      ..addListener(() {
        setState(() {});
      });

    _animateIcon = Tween<double>(begin: 0.0, end: 1.0).animate(_animationController);

    _buttonColor = ColorTween(
      begin: Colors.blue,
      end: Colors.red,
    ).animate(CurvedAnimation(
      parent: _animationController,
      curve: Interval(
        0.00,
        1.00,
        curve: Curves.linear,
      ),
    ));

    _translateButton = Tween<double>(
      begin: _fabHeight,
      end: -14.0,
    ).animate(CurvedAnimation(
      parent: _animationController,
      curve: Interval(
        0.0,
        0.75,
        curve: _curve,
      ),
    ));

    super.initState();

    if ((DateTime.now().hour >= 600) && (DateTime.now().hour < 1200)) {
      turno = "desCacDay";
    }
    if ((DateTime.now().hour >= 1200) && (DateTime.now().hour < 1800)) {
      turno = "desCacTar";
    }
    if (((DateTime.now().hour >= 1800) && (DateTime.now().hour < 2300)) || (DateTime.now().hour < 600)) {
      turno = "desCacNig";
    }

    //SimplePermissions.requestPermission(Permission.WriteExternalStorage);

    PermissionHandler().requestPermissions([PermissionGroup.storage]);

    /*if (_local == null) {
      _local = new Local('', '', '', '', '', '', '', '', '', false, '', false);
      //new Local('', '', '', '', '', '', '', '', '', '', false, '', false);
    }*/

    tabController = new TabController(initialIndex: 0, vsync: this, length: 3);
    Firestore.instance.collection('produtos').getDocuments().then((c) {
      setState(() {
        c.documents.map((prod) {
          if (listaTotal.length < c.documents.length)
            listaTotal.add(new FiltroLocal(
                new Produtos(prod.documentID, prod.data['codigo'], prod.data['nome'], prod.data['preco'], prod.data['unidade'], prod.data['deletado'], 0),
                MainPage2.filtroAnalise.listCultura.any((p) => p.nomIte == prod.data['nome'].toString())));
        }).toList();
        listaTotal.sort((a, b) => a.item.nomIte.compareTo(b.item.nomIte));
      });
    });
    //_local.fazenda = 'Adicionar local';
    orderByCul = OrderBy.desativado;
    orderByData = OrderBy.desativado;
    orderByLoc = OrderBy.desativado;

    controller = new AnimationController(duration: const Duration(seconds: 2), vsync: this);
    animation = ColorTween(begin: Tema.corAlerta, end: Colors.white).animate(controller)
      ..addListener(() {
        if (this.animation.isCompleted) {
          editada = '';
          controller.reset();
        }
        setState(() {});
      });
  }

  bool visivel = false;

  animate() {
    if (!isOpened) {
      _animationController.forward();
    } else {
      _animationController.reverse();
    }
    isOpened = !isOpened;
  }

  Widget toggle() {
    return Container(
      child: FloatingActionButton(
        heroTag: null,
        backgroundColor: _buttonColor.value,
        onPressed: animate,
        tooltip: 'Opções',
        child: AnimatedIcon(
          icon: AnimatedIcons.menu_close,
          progress: _animateIcon,
        ),
      ),
    );
  }

  Widget flatButtonNovoPedido() {
    return Container(
      child: FloatingActionButton(
        heroTag: null,
        backgroundColor: Colors.indigoAccent,
        onPressed: () {
          Navigator.push(context, new MaterialPageRoute(builder: (context) => new EditarNegocioPage()));
        },
        tooltip: 'Novo Pedido',
        child: Icon(Icons.add_shopping_cart),
      ),
    );
  }

  Widget flatButtonProduto() {
    return Container(
      child: FloatingActionButton(
        heroTag: null,
        backgroundColor: Colors.purple,
        onPressed: () {
          Navigator.push(
              context,
              new MaterialPageRoute(
                  builder: (context) => new EditarProdutoPage(
                        produtoSelecionado: null,
                      )));
        },
        tooltip: 'Novo Produto',
        child: Icon(Icons.note_add),
      ),
    );
  }

  Widget flatButtonCliente() {
    return Container(
      child: FloatingActionButton(
        heroTag: null,
        backgroundColor: Colors.lime,
        onPressed: () {
          Navigator.push(
              context,
              new MaterialPageRoute(
                  builder: (context) => new EditarClientePage(
                        clienteSelecionado: null,
                      )));
        },
        tooltip: 'Novo Cliente',
        child: Icon(
          Icons.person_add,
        ),
      ),
    );
  }

  @override
  dispose() {
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (MainPage2.usuario == null) {
      Firestore.instance.collection('usuarios').document(widget.idUsuario).get().then((u) async {
        setState(() {
          MainPage2.usuario = new Usuario.map(u.data, widget.idUsuario);
        });
        //_listaLocal = await Servicos.listadeLocaL();

        //if (_listaLocal.isNotEmpty) {
        //_local.fazenda = await Servicos.nomeLocalPrincipal();
        /*await Servicos.nomeLocalPrincipal().then((obj) {
            _local = obj;
          });*/
        //Define o local inicial sempre como "Todos";````
        //_local.fazenda = "Todos";
        //}
        setState(() {});
      });

      return _buildScaffold(
          new Center(child: new Container(height: 100.0, width: 100.0, child: new CircularProgressIndicator())),
          null, //FlatButton(child: Text('data'),)
          '');
    }

    if (MainPage2.usuario.token == null || MainPage2.usuario.token.length == 0) {
      _firebaseMessaging.getToken().then((t) {
        MainPage2.usuario.token = t;
        Servicos.editUsuario(MainPage2.usuario, context);
      });
    }

    /*
    listaFaltando.clear();
    Firestore.instance
        .collection('usuarios')
        .where('INDREP', isEqualTo: false)
        //.where('INDRES', isEqualTo: '')
        .getDocuments()
        .then((us) {
      us.documents.map((use) {
        listaFaltando.add(new Usuario.map(use.data, use.documentID));
      }).toList();
    });
    */

    return _buildScaffold(
        new Column(
          children: <Widget>[
            Container(
                height: 40.0,
                color: Tema.corPrimaria,
                child: TabBar(indicatorColor: Colors.orange, indicator: BoxDecoration(color: Colors.orange), controller: tabController, tabs: [
                  new Text(
                    'Em aberto',
                    style: TextStyle(fontSize: 13.5),
                  ),
                  new Text('Aprovados', style: TextStyle(fontSize: 13.5)),
                  new Text('Entregues', style: TextStyle(fontSize: 13.5)),
                  //new Text('Finalizado', style: TextStyle(fontSize: 13.5)),
                ])),
            Padding(
                padding: EdgeInsets.all(0.0),
                child: Column(
                  children: <Widget>[
                    TextField(
                        controller: ctrPesq,
                        onChanged: (String valor) {
                          setState(() {
                            pesquisa = valor;
                          });
                        },
                        decoration: InputDecoration(
                            hintText: 'Procurar...',
                            prefixIcon: Padding(
                              padding: EdgeInsets.all(0.0),
                              child: Icon(Icons.search),
                            ),
                            suffixIcon: Padding(
                              padding: EdgeInsets.all(0.0),
                              child: IconButton(
                                  icon: Icon(Icons.clear),
                                  onPressed: () {
                                    setState(() {
                                      pesquisa = '';
                                      ctrPesq.text = '';
                                    });
                                  }),
                            ),
                            filled: true,
                            fillColor: Colors.white)),
                    Padding(
                      padding: EdgeInsets.fromLTRB(0.0, 1.0, 0.0, 1.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          RaisedButton(
                            shape: new RoundedRectangleBorder(borderRadius: BorderRadius.circular(45.0)),
                            color: orderByCul == OrderBy.desativado ? Colors.white : Tema.corPicker,
                            onPressed: () {
                              setState(() {
                                if (orderByCul == OrderBy.desativado) {
                                  orderByCul = OrderBy.crescente;
                                  orderByLoc = OrderBy.desativado;
                                  orderByData = OrderBy.desativado;
                                } else if (orderByCul == OrderBy.crescente) {
                                  orderByCul = OrderBy.decrescente;
                                  orderByLoc = OrderBy.desativado;
                                  orderByData = OrderBy.desativado;
                                } else {
                                  orderByCul = OrderBy.desativado;
                                }
                              });
                            },
                            child: Row(mainAxisAlignment: MainAxisAlignment.start, children: [
                              Text('Produto'),
                              orderByCul == OrderBy.crescente
                                  ? Icon(Icons.arrow_drop_down)
                                  : (orderByCul == OrderBy.decrescente ? Icon(Icons.arrow_drop_up) : Icon(Icons.arrow_drop_up, color: Colors.white))
                            ]),
                          ),
                          RaisedButton(
                            shape: new RoundedRectangleBorder(borderRadius: BorderRadius.circular(45.0)),
                            color: orderByData == OrderBy.desativado ? Colors.white : Tema.corPicker,
                            onPressed: () {
                              setState(() {
                                if (orderByData == OrderBy.desativado) {
                                  orderByData = OrderBy.crescente;
                                  orderByLoc = OrderBy.desativado;
                                  orderByCul = OrderBy.desativado;
                                } else if (orderByData == OrderBy.crescente) {
                                  orderByData = OrderBy.decrescente;
                                  orderByLoc = OrderBy.desativado;
                                  orderByCul = OrderBy.desativado;
                                } else {
                                  orderByData = OrderBy.desativado;
                                }
                              });
                            },
                            child: Row(children: [
                              Text('Pagamento'),
                              orderByData == OrderBy.crescente
                                  ? Icon(Icons.arrow_drop_down)
                                  : (orderByData == OrderBy.decrescente ? Icon(Icons.arrow_drop_up) : Icon(Icons.arrow_drop_up, color: Colors.white))
                            ]),
                          ),
                          RaisedButton(
                            shape: new RoundedRectangleBorder(borderRadius: BorderRadius.circular(45.0)),
                            color: orderByLoc == OrderBy.desativado ? Colors.white : Tema.corPicker,
                            onPressed: () {
                              setState(() {
                                if (orderByLoc == OrderBy.desativado) {
                                  orderByLoc = OrderBy.crescente;
                                  orderByCul = OrderBy.desativado;
                                  orderByData = OrderBy.desativado;
                                } else if (orderByLoc == OrderBy.crescente) {
                                  orderByLoc = OrderBy.decrescente;
                                  orderByCul = OrderBy.desativado;
                                  orderByData = OrderBy.desativado;
                                } else {
                                  orderByLoc = OrderBy.desativado;
                                }
                              });
                            },
                            child: Row(children: [
                              Text('Cliente'),
                              orderByLoc == OrderBy.crescente
                                  ? Icon(Icons.arrow_drop_down)
                                  : (orderByLoc == OrderBy.decrescente ? Icon(Icons.arrow_drop_up) : Icon(Icons.arrow_drop_up, color: Colors.white))
                            ]),
                          ),
//                          ConstrainedBox(
////                            child: RaisedButton(
////                              shape: new RoundedRectangleBorder(
////                                  borderRadius: BorderRadius.circular(45.0)),
////                              child: Icon(
////                                Icons.filter_list,
////                                color: Colors.black,
////                              ),
////                              color: MainPage2.filtroAnalise.isEmpty
////                                  ? Colors.white
////                                  : Tema.corPicker,
////                              onPressed: () {
////                                showDialog(
////                                  context: context,
////                                  builder: (context) => new PopFiltro(
////                                        listaTotal: listaTotal,
////                                      ),
////                                ).then((a) {
////                                  setState(() {
////                                    listaTotal.forEach((c) => c.seleciona =
////                                        MainPage2.filtroAnalise.listCultura.any(
////                                            (l) => c.item.nomIte == l.nomIte));
////                                  });
////                                });
////                              },
////                            ),
////                            constraints: BoxConstraints(maxWidth: 60.0),
////                          )
                        ],
                      ),
                    )
                  ],
                )),
            Expanded(
              child: TabBarView(controller: tabController, children: <Widget>[_buildLista(STAPRC.ABE), _buildLista(STAPRC.APR), _buildLista(STAPRC.ENT)]),
            ),
          ],
        ),
        MainPage2.usuario.admin
            ? Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  MainPage2.usuario.admin
                      ? Transform(
                          transform: Matrix4.translationValues(
                            0.0,
                            _translateButton.value * 3.0,
                            0.0,
                          ),
                          child: flatButtonCliente(),
                        )
                      : null,
                  MainPage2.usuario.admin
                      ? Transform(
                          transform: Matrix4.translationValues(
                            0.0,
                            _translateButton.value * 2.0,
                            0.0,
                          ),
                          child: flatButtonProduto(),
                        )
                      : null,
                  Transform(
                    transform: Matrix4.translationValues(
                      0.0,
                      _translateButton.value,
                      0.0,
                    ),
                    child: flatButtonNovoPedido(),
                  ),
                  toggle(),
                ],
              )

            /*
        FloatingActionButton(
                onPressed: () {
                  Navigator.push(
                      context,
                      new MaterialPageRoute(
                          builder: (context) => new EditarNegocioPage(
                              */ /*localSelecionado: _local.fazenda == 'Todos'
                                      ? stringLocalPrincipal()
                                      : _local,*/ /*
                              ))).then((prc) {
                    if (prc != null) {
                      setState(() {
                        listaPedido = new List<Processos>();
                        editada = (prc as Processos).idePrc;
                        if ((prc as Processos).status == "ABE") {
                          tabController.index = 0;
                        }
                        if ((prc as Processos).status == "APR") {
                          tabController.index = 1;
                        }
                        doAnimation();
                      });
                    }
                  });
                },
                tooltip: 'Novo pedido',
                child: new Icon(Icons.business_center))
                */

            //CLIENTES
            : FloatingActionButton(
                //Editar processo/análise
                onPressed: () async {
                  Navigator.push(
                      context,
                      new MaterialPageRoute(
                          builder: (context) => new EditarNegocioPage(
                              /*localSelecionado: _local.fazenda == 'Todos'
                                      ? stringLocalPrincipal()
                                      : _local,*/
                              ))).then((analise) {
                    if (analise != null) {
                      setState(() {
                        listaPedido = new List<Processos>();
                        editada = (analise as Processos).idePrc;
                        tabController.index = 0;
                        doAnimation();
                      });
                    }
                  });
                },
                tooltip: 'Novo pedido 2',
                child: new Icon(Icons.add),
              ),
        MainPage2.usuario.primeiroNome);
  }

  Widget _buildScaffold(Widget body, Widget button, String nomeUsuTitle) {
    return new Scaffold(
      appBar: new AppBar(
        elevation: 1.0,
        //title: new Text(widget.title),
        title: new Text(nomeUsuTitle),
        centerTitle: true,
        actions: <Widget>[
          PopupMenuButton(
            onSelected: (valor) {
              if (valor == 'Clientes') {
                //Servicos.corrigeIDCliente();
                Navigator.push(context, new MaterialPageRoute(builder: (context) => new ListarClientesPage()));
              }
              if (valor == 'Produtos') {
                Navigator.push(
                    context,
                    new MaterialPageRoute(
                        builder: (context) => new ListarProdutosPage(
                              usuario: MainPage2.usuario,
                            )));
              }
              if (valor == 'Condições Pgtos.') {
                Navigator.push(context, new MaterialPageRoute(builder: (context) => new ListarPagamentosPage()));
              }
              if (valor == 'Upload Produtos') {
                Navigator.push(context, new MaterialPageRoute(builder: (context) => new AddProdutosScreen()));
              }
              if (valor == 'Minha conta') {
                Navigator.push(context, new MaterialPageRoute(builder: (context) => new InsereDadosPage(isEditing: true))).then((x) async {
                  if (x != null) {
                    showDialog(
                        context: context,
                        builder: (context) => AlertDialog(
                              title: Text('Dados editados com sucesso.'),
                              actions: <Widget>[
                                FlatButton(
                                  child: Text('Ok'),
                                  onPressed: () => Navigator.of(context).pop(),
                                )
                              ],
                            ));
                  }
                });
              } else if (valor == "Sair") {
                MainPage2.filtroAnalise = new FiltroProcesso.vazio();
                MainPage2.usuario = null;
                Auth.signOut();
                //Navigator.pop(context);
                Navigator.pushReplacement(context, new MaterialPageRoute(builder: (context) => new LoginUsuarioPage()));
              }
            },
            itemBuilder: (context) {
              List<PopupMenuItem> lista = new List<PopupMenuItem>();
              lista.add(PopupMenuItem<String>(
                value: 'Minha conta',
                child: Text('Minha conta'),
              ));
              MainPage2.usuario.admin
                  ? lista.add(PopupMenuItem<String>(
                      value: 'Clientes',
                      child: Text('Clientes'),
                    ))
                  : null;
              //MainPage2.usuario.admin ? //Comentado em 18/04/2019 - 22:56
              lista.add(PopupMenuItem<String>(
                value: 'Produtos',
                child: Text('Produtos'),
              ));
              //: null;
              MainPage2.usuario.admin
                  ? lista.add(PopupMenuItem<String>(
                      value: 'Condições Pgtos.',
                      child: Text('Condições Pgtos.'),
                    ))
                  : null;
//              lista.add(PopupMenuItem<String>(
//                value: 'Enviar Produtos',
//                child: Text('Enviar Produtos'),
//              ));
              lista.add(PopupMenuItem<String>(value: 'Sair', child: Text('Sair')));
              return lista;
            },
          ),
        ],
      ),
      body: body,
      floatingActionButton: button,
      bottomNavigationBar: MainPage2.filtroAnalise.isEmpty
          ? null
          : RaisedButton(
              color: Tema.corPicker,
              child: Text('Limpar Filtros'),
              onPressed: () {
                setState(() {
                  MainPage2.filtroAnalise = new FiltroProcesso.vazio();
                  listaTotal.forEach((c) {
                    c.seleciona = false;
                  });
                });
              },
            ),
    );
  }

  /// Autor: Diogo Roosernando
  /// Função: monta as listas dependendo de qual a situação da analise
  /// Parâmetros: situacao (enum)

  Widget _buildLista(STAPRC situacao) {
    Query busca;
    if (!MainPage2.usuario.admin) {
      busca = Firestore.instance.collection('pedidos').where('deletado', isEqualTo: false).where('usuario', isEqualTo: MainPage2.usuario.usuario);
    } else if (MainPage2.usuario.admin) {
      // Como o representante pode gerar OPORTUNIDADES E NEGÓCIOS, é importante
      // gravar na mesma tabela, caso contrário deveria fazer duas buscas na tela
      busca = Firestore.instance.collection('pedidos').where('deletado', isEqualTo: false)
          //.where('CODUID', isEqualTo: MainPage2.usuario.usuario)
          ;
    }

    return new Container(
      child: new StreamBuilder(
          stream: busca.snapshots(),
          builder: (context, snapshot) {
            if (!snapshot.hasData) return Text('Carregando...');
            //if (!snapshot.hasData || (snapshot.data.documents as List<DocumentSnapshot>).isEmpty)
//              return Image.asset(
//                'images/' + turno + '.png',
//                fit: BoxFit.fill,
//              );
            //if ((MainPage2.usuario.admin)) {
            listaPedido = new List<Processos>();
            snapshot.data.documents.map((DocumentSnapshot document) {
              listaPedido.add(new Processos.map(document.data, document.documentID));
            }).toList();

            listaPedidoFiltro = listaPedido.where((analise) => analise.status == STAPRC.ABE.toString().split('.')[1]).toList();
            listaPedidoFiltro2 = listaPedido.where((analise) => analise.status == STAPRC.APR.toString().split('.')[1]).toList();
            listaPedidoFiltro3 = listaPedido.where((analise) => analise.status == STAPRC.ENT.toString().split('.')[1]).toList();
            //}

            if ((pesquisa.isNotEmpty || MainPage2.filtroAnalise.listCultura.isNotEmpty) && situacao == STAPRC.ABE) {
              listaPedidoFiltro = listaPedido
                  .where((analise) =>
                      (pesquisa.isEmpty ||
                          analise.status == STAPRC.ABE.toString().split('.')[1] &&
                              (analise.cliente.toLowerCase().contains(pesquisa.toLowerCase()) || analise.condicao.toLowerCase().contains(pesquisa.toLowerCase())) ||
                          (MainPage2.usuario.admin && (analise.usuario.toLowerCase()).contains(pesquisa.toLowerCase()))) &&
                      ((MainPage2.filtroAnalise.listCultura.isEmpty || (MainPage2.filtroAnalise.listCultura.any((c) => c.nomIte == analise.cliente)))))
                  .toList();
            } else if ((pesquisa.isNotEmpty || MainPage2.filtroAnalise.dataAtivado || MainPage2.filtroAnalise.listCultura.isNotEmpty) && situacao == STAPRC.APR) {
              listaPedidoFiltro2 = listaPedido
                  .where((analise) =>
                      analise.status == STAPRC.APR.toString().split('.')[1] &&
                      (pesquisa.isEmpty ||
                          (analise.cliente.toLowerCase().contains(pesquisa.toLowerCase()) || analise.condicao.toLowerCase().contains(pesquisa.toLowerCase())) ||
                          (MainPage2.usuario.admin && (analise.usuario.toLowerCase()).contains(pesquisa.toLowerCase()))) &&
                      (!MainPage2.filtroAnalise.dataAtivado ||
                          (analise.datGer.toDate().add(Duration(days: 1)).isAfter(MainPage2.filtroAnalise.marcadaInicio.toDate()) &&
                              analise.datGer.toDate().subtract(Duration(days: 1)).isBefore(MainPage2.filtroAnalise.marcadaFinal.toDate()))) &&
                      (MainPage2.filtroAnalise.listCultura.isEmpty || (MainPage2.filtroAnalise.listCultura.any((c) => c.nomIte == analise.cliente))))
                  .toList();
            } else if ((pesquisa.isNotEmpty || MainPage2.filtroAnalise.dataAtivado || MainPage2.filtroAnalise.listCultura.isNotEmpty) && situacao == STAPRC.ENT) {
              listaPedidoFiltro3 = listaPedido
                  .where((analise) =>
                      analise.status == STAPRC.ENT.toString().split('.')[1] &&
                      (pesquisa.isEmpty ||
                          (analise.cliente.toLowerCase().contains(pesquisa.toLowerCase()) || analise.condicao.toLowerCase().contains(pesquisa.toLowerCase())) ||
                          (MainPage2.usuario.admin && (analise.usuario.toLowerCase()).contains(pesquisa.toLowerCase()))) &&
                      (!MainPage2.filtroAnalise.dataAtivado ||
                          (analise.datGer.toDate().add(Duration(days: 1)).isAfter(MainPage2.filtroAnalise.marcadaInicio.toDate()) &&
                              analise.datGer.toDate().subtract(Duration(days: 1)).isBefore(MainPage2.filtroAnalise.marcadaFinal.toDate()))) &&
                      (MainPage2.filtroAnalise.listCultura.isEmpty || (MainPage2.filtroAnalise.listCultura.any((c) => c.nomIte == analise.cliente))))
                  .toList();
            } else {
              listaPedidoFiltro = listaPedido.where((analise) => analise.status == STAPRC.ABE.toString().split('.')[1]).toList();
              listaPedidoFiltro2 = listaPedido.where((analise) => analise.status == STAPRC.APR.toString().split('.')[1]).toList();
              listaPedidoFiltro3 = listaPedido.where((analise) => analise.status == STAPRC.ENT.toString().split('.')[1]).toList();
            }

            if (orderByCul != OrderBy.desativado) {
              if (orderByCul == OrderBy.decrescente) {
                listaPedidoFiltro.sort((a, b) => b.cliente.compareTo(a.cliente));
                listaPedidoFiltro2.sort((a, b) => b.cliente.compareTo(a.cliente));
                listaPedidoFiltro3.sort((a, b) => b.cliente.compareTo(a.cliente));
              } else {
                listaPedidoFiltro.sort((a, b) => a.cliente.compareTo(b.cliente));
                listaPedidoFiltro2.sort((a, b) => a.cliente.compareTo(b.cliente));
                listaPedidoFiltro3.sort((a, b) => a.cliente.compareTo(b.cliente));
              }
            }
            if (orderByData != OrderBy.desativado && situacao != STAPRC.ABE) {
              if (orderByData == OrderBy.decrescente) {
                if (situacao == STAPRC.APR) {
                  listaPedidoFiltro2.sort((a, b) => a == null ? 0 : b.datApr.compareTo(a.datApr));
                } else if (situacao == STAPRC.ENT) {
                  listaPedidoFiltro3.sort((a, b) => a == null ? 0 : b.datApr.compareTo(a.datApr));
                } else {
                  listaPedidoFiltro3.sort((a, b) => a == null ? 0 : b.datGer.compareTo(a.datGer));
                }
              } else {
                if (situacao == STAPRC.APR) {
                  listaPedidoFiltro2.sort((a, b) => a == null ? 0 : a.datApr.compareTo(b.datApr));
                } else if (situacao == STAPRC.ENT) {
                  listaPedidoFiltro3.sort((a, b) => a == null ? 0 : a.datApr.compareTo(b.datApr));
                } else {
                  listaPedidoFiltro3.sort((a, b) => a == null ? 0 : a.datGer.compareTo(b.datGer));
                }
              }
            }

            if (orderByLoc == OrderBy.desativado && orderByData == OrderBy.desativado && orderByCul == OrderBy.desativado) {
              listaPedidoFiltro.sort((a, b) => b.datGer.compareTo(a.datGer));
              listaPedidoFiltro2.sort((a, b) => b.datGer.compareTo(a.datGer));
              listaPedidoFiltro3.sort((a, b) => b.datApr.compareTo(a.datApr));
            }

            if (orderByLoc != OrderBy.desativado) {
              if (orderByLoc == OrderBy.decrescente) {
                listaPedidoFiltro.sort((a, b) => b.condicao.compareTo(a.condicao));
                listaPedidoFiltro2.sort((a, b) => b.condicao.compareTo(a.condicao));
                listaPedidoFiltro3.sort((a, b) => b.condicao.compareTo(a.condicao));
              } else {
                listaPedidoFiltro.sort((a, b) => a.condicao.compareTo(b.condicao));
                listaPedidoFiltro2.sort((a, b) => a.condicao.compareTo(b.condicao));
                listaPedidoFiltro3.sort((a, b) => a.condicao.compareTo(b.condicao));
              }
            }
            if (situacao == STAPRC.ABE && listaPedidoFiltro.isEmpty) {
              return GestureDetector(
                onTap: () async {
                  setState(() {
                    listaPedido = new List<Processos>();
                  });
                },
                //child: Image.asset('images/' + turno + '.png')
              );
            } else if (situacao == STAPRC.APR && listaPedidoFiltro2.isEmpty) {
              return GestureDetector(
                onTap: () {
                  setState(() {
                    listaPedido = new List<Processos>();
                  });
                },
                //child: Image.asset('images/' + turno + '.png')
              );
            } else if (situacao == STAPRC.ENT && listaPedidoFiltro3.isEmpty) {
              return GestureDetector(
                onTap: () {
                  setState(() {
                    listaPedido = new List<Processos>();
                  });
                },
                //child: Image.asset('images/' + turno + '.png')
              );
            }

            return RefreshIndicator(
                onRefresh: () async {
                  setState(() {
                    listaPedido = new List<Processos>();
                  });
                  return null;
                },
                child: new ListView.builder(
                    itemCount: (situacao == STAPRC.ABE ? listaPedidoFiltro.length : (situacao == STAPRC.APR ? listaPedidoFiltro2.length : listaPedidoFiltro3.length)) + 1,
                    padding: const EdgeInsets.only(top: 10.0),
                    itemExtent: 100.0,
                    itemBuilder: (context, i) {
                      if (i >= (situacao == STAPRC.ABE ? listaPedidoFiltro.length : (situacao == STAPRC.APR ? listaPedidoFiltro2.length : listaPedidoFiltro3.length))) {
                        //Card para botao float
                        return new Card(
                          elevation: 0.0,
                          color: Colors.transparent,
                          child: Text(''),
                        );
                      }

                      Processos _prc = situacao == STAPRC.ABE ? listaPedidoFiltro[i] : (situacao == STAPRC.APR ? listaPedidoFiltro2[i] : listaPedidoFiltro3[i]);

                      /*
                      Usuario usuario;
                      Firestore.instance
                          .collection('usuarios')
                          //.where('NOMUSU', isEqualTo: _analise.codUid)
                          //.getDocuments()
                          .document(
                              _prc.codUid == null ? _prc.codMid : _prc.codUid)
                          .get()
                          .then((u) {
                        usuario = new Usuario.map(
                            u.data, u.documentID); //Usuario do processo
                      });
                      */

                      String qtd = "";
                      /*if (_prc.uniMed == "Sacas") {
                        qtd = NumberFormat("###,###.###")
                            .format(_prc.qtdIte)
                            .replaceAll(",", ".");
                      }
                      if (_prc.uniMed == "Quilos") {
                        qtd = NumberFormat("###,###.###", "pt-br")
                            .format(_prc.qtdIte);
                      } else {
                        qtd = NumberFormat("###,###.###", "pt-br")
                            .format(_prc.vlrMed);
                      }*/

                      /*String txtOpoOuNeg =
                          _prc.nomIte.toLowerCase().contains('neg')
                              ? _prc.nomIte +
                                  ' de ' +
                                  DateFormat('dd/MM/yyyy').format(_prc.datGer)
                              : _prc.tipPrc + ' para ' + _prc.nomIte;*/
                      valorController.updateValue(_prc.valortotal);
                      String textoClienteCard = _prc.numero.toString() + ' - ' + _prc.cliente;

                      String textoObservacaoCard = _prc.observacao;

                      if (textoObservacaoCard.length > 130) {
                        textoObservacaoCard = textoObservacaoCard.substring(0, 130) + "...";
                      }

                      //70% da largura da tela - aplicado quando tem estado/cidade vizinhos
                      double width070 = MediaQuery.of(context).size.width * 0.7;
                      double width080 = MediaQuery.of(context).size.width * 0.80;
                      double width090 = MediaQuery.of(context).size.width * 0.90;

                      /*int num =
                          int.parse(valorController.text.replaceAll(",", ""));
                      moneyFormatter.minimumFractionDigits = 2;
                      moneyFormatter.significantDigitsInUse = true;
                      final newString = moneyFormatter.format(num);*/

                      return new GestureDetector(
                          child: Card(
                              color: _prc.idePrc == editada ? animation.value : Colors.white,
                              child: Padding(
                                  padding: EdgeInsets.all(2.0),
                                  child: new Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, crossAxisAlignment: CrossAxisAlignment.stretch, children: <Widget>[
                                    Column(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Container(
                                          width: situacao == STAPRC.ABE ? width070 : situacao == STAPRC.APR ? width080 : width090,
                                          child: Text(
                                            textoClienteCard,
                                            style: TextStyle(fontSize: 15.0, color: Colors.red[800], fontWeight: FontWeight.bold),
                                          ),
                                        ),
                                        Container(
                                          width: situacao == STAPRC.ABE ? width070 : situacao == STAPRC.APR ? width080 : width090,
                                          child: Text(textoObservacaoCard, style: TextStyle(fontSize: 10.0)),
                                        ),
                                        Container(
                                            width: situacao == STAPRC.ABE ? width070 : situacao == STAPRC.APR ? width080 : width090,
                                            child: new Text(
                                              DateFormat("dd/MM/yyyy").format(_prc.datGer.toDate()) +
                                                  ' por ' +
                                                  _prc.nomeusuario +
                                                  ' - ' +
                                                  _prc.nomecondicao +
                                                  ' R\$ ' +
                                                  valorController.text,
                                              style: TextStyle(fontSize: 11.0, color: Colors.blue[800], fontWeight: FontWeight.bold),
                                              softWrap: true,
                                            ))
                                      ],
                                    ),

                                    // Botões do card
                                    situacao == STAPRC.ABE
                                        ? Container(
                                            //width: 110.0,
                                            child: Row(mainAxisAlignment: MainAxisAlignment.spaceAround, crossAxisAlignment: CrossAxisAlignment.center, children: <Widget>[
                                            Row(
                                              mainAxisAlignment: MainAxisAlignment.center,
                                              children:
                                                  situacao == STAPRC.ABE && !MainPage2.usuario.admin ? miniBotaoDeletarCard(situacao, _prc) : miniBotaoAprovarCard(situacao, _prc),
                                            ),

                                            //TODO Comentar todo o botão abaixo?
                                          ]))
                                        : (situacao != STAPRC.ENT) && (_prc.usuario == MainPage2.usuario.docId || MainPage2.usuario.admin)
                                            ? miniBotaoEntregar(situacao, _prc)
                                            : Text('')
                                  ]))),
                          //
                          //
                          //
                          onTap: (situacao == STAPRC.ABE)
                              ? () {
                                  Navigator.push(
                                      context,
                                      new MaterialPageRoute(
                                          builder: (BuildContext context) => new EditarNegocioPage(
                                                processoSelecionado: _prc,
                                              ))).then((retorno) {
                                    setState(() {
                                      if (retorno != null) {
                                        if ((retorno as Processos).deletado) {
                                          listaPedido = new List<Processos>();
                                        } else {
                                          _prc.cliente = (retorno as Processos).cliente;
                                          _prc.condicao = (retorno as Processos).condicao;
                                          editada = _prc.idePrc;
                                          doAnimation();
                                        }
                                      }
                                    });
                                  });
                                }
                              : // TODO listar PDF
                              () async => await calculaTotalItensPDF(_prc)); //geraPDF(_prc));
                    }));
          }),
    );
  }

  List<Widget> miniBotaoDeletarCard(STAPRC situacao, Processos prc) {
    return [
      IconButton(
        icon: Icon(
          Icons.delete,
          color: Colors.grey,
        ),
        onPressed: prc.status != "APR" || MainPage2.usuario.admin
            ? () {
                showDialog(
                  context: context,
                  builder: (context) => new AlertDialog(
                        title: Text('Excluir'),
                        content: Text("Deseja realmente excluir?"),
                        actions: <Widget>[
                          new FlatButton(
                              child: Text('Não'),
                              onPressed: () {
                                Navigator.pop(context);
                              }),
                          new FlatButton(
                            child: Text('Sim'),
                            onPressed: () async {
                              prc.deletado = true;
                              await Servicos.editProcesso(prc, null);
                              setState(() {
                                listaPedido = new List<Processos>();
                              });
                              Navigator.of(context).pop();
                            },
                          )
                        ],
                      ),
                );
              }
            : null,
      )
    ];
  }

  List<Widget> miniBotaoAprovarCard(STAPRC situacao, Processos prc) {
    return [
      IconButton(
        icon: Icon(
          Icons.check_circle,
          color: situacao == STAPRC.APR ? Colors.grey : Colors.green,
        ),
        onPressed: !MainPage2.usuario.admin || situacao == STAPRC.APR
            ? null
            : () {
                showDialog(
                  builder: (context) => AlertDialog(
                        title: Text('Aprovar?'),
                        actions: <Widget>[
                          new FlatButton(
                            child: Icon(
                              Icons.check_circle,
                              color: Colors.green,
                            ),
                            onPressed: () {
                              setState(() {
                                Servicos.editProcesso(
                                        new Processos(
                                          prc.idePrc,
                                          prc.numero,
                                          prc.cliente,
                                          prc.observacao,
                                          prc.condicao,
                                          prc.nomecondicao,
                                          prc.perdesc,
                                          STAPRC.APR.toString().split('.')[1],
                                          prc.datGer,
                                          Timestamp.now(),
                                          prc.dataEntrega,
                                          prc.horaEntrega,
                                          prc.minutoEntrega,
                                          false,
                                          prc.novoValor,
                                          MainPage2.usuario.usuario,
                                          prc.usuario,
                                          prc.nomeusuario,
                                          prc.valortotal,
                                          prc.itens,
                                          '',
                                          //false
                                        ),
                                        null)
                                    .then((a) {
                                  editada = prc.idePrc;
                                  tabController.index = 1;
                                });
                                Navigator.pop(context);
                                doAnimation();
                              });
                            },
                          ),
                          FlatButton(
                            child: Icon(Icons.cancel),
                            onPressed: () => Navigator.pop(context),
                          )
                        ],
                      ),
                  context: context,
                );
              },
      ),
      miniBotaoDeletarCard(situacao, prc).first,
    ];
  }

  Widget miniBotaoEntregar(STAPRC situacao, Processos prc) {
    return IconButton(
      icon: Icon(
        Icons.directions_car,
        color: Colors.red,
      ),
      onPressed: prc.usuario == MainPage2.usuario.docId || MainPage2.usuario.admin
          ? () {
              showDialog(
                context: context,
                builder: (context) => new AlertDialog(
                      title: Text('Pedido entregue'),
                      content: Text("Este pedido realmente foi entregue?"),
                      actions: <Widget>[
                        new FlatButton(
                            child: Text('Não'),
                            onPressed: () {
                              Navigator.pop(context);
                            }),
                        new FlatButton(
                          child: Text('Sim'),
                          onPressed: () async {
                            //prc.entregue = true;
                            prc.status = STAPRC.ENT.toString().split('.')[1];
                            await Servicos.editProcesso(prc, null);
                            setState(() {
                              listaPedido = new List<Processos>();
                            });
                            Navigator.of(context).pop();
                          },
                        )
                      ],
                    ),
              );
            }
          : null,
    );
  }

  Future<Io.File> get _localFile async {
    final path = await Diretorio().localPath;
    return Io.File('$path/redesip.pdf');
  }

  Future<Null> geraPDF(Processos prcSel) async {
    final pdf = new PdfDocument(deflate: Io.zlib.encode);
    final page = new PdfPage(pdf, pageFormat: PdfPageFormat.letter);
    final g = page.getGraphics();
    final font = new PdfFont.helvetica(pdf);
    final fontTtf = await rootBundle.load('fonts/Anton-Regular.ttf');
    PdfTtfFont ttf = new PdfTtfFont(pdf, fontTtf
        /*(new Io.File(await Diretorio().localPath + "/Anton-Regular.ttf")
                .readAsBytesSync() as Uint8List)
            .buffer
            .asByteData()*/
        );

    final top = page.pageFormat.height;
    final largura = page.pageFormat.width;

    //Img.Image img = Img.decodeImage(new Io.File('test.webp').readAsBytesSync());

    /*
    final filePath = '${await Diretorio().localPath}/image_cache/Mediador.png';
    var f1 = new Io.File(filePath);
    print('aqui1 ' + f1.exists().toString());
    */

    ByteData png = await rootBundle.load("images/redesip.png");
    Codec codec = await instantiateImageCodec(png.buffer.asUint8List());
    var frameInfo = await codec.getNextFrame();
    var xxx = await frameInfo.image.toByteData(format: ImageByteFormat.png);
    var image2 = frameInfo.image;

    var bytes = await image2.toByteData();

    PdfImage image = new PdfImage(pdf, image: bytes.buffer.asUint8List(), width: image2.width, height: image2.height);
    g.drawImage(image, 5.0, top - 130.0, 120.0);

    /// Gráficos (criação independente por conta do fillpath)
    g.setColor(new PdfColor(0.0, 0.0, 0.0));
    g.drawRect(0.0, top - 120.0, largura, 1.0);
    g.drawRect(0.0, 40.0, largura, 1.0);
    g.fillPath();

    /// Cabeçalho
    g.setColor(new PdfColor(0.3, 0.3, 0.3));
    g.drawString(ttf, 12.0, "JJGC SUPERMERCADOS LTDA", 50.0 * PdfPageFormat.mm, top - 10.0 * PdfPageFormat.mm);
    g.drawString(ttf, 12.0, "CNPJ: 19.192.939/0001-23", 50.0 * PdfPageFormat.mm, top - 20.0 * PdfPageFormat.mm);
    g.drawString(ttf, 12.0, "Rua Castro Alves, 170 - N. SRA. DA ABADIA - Uberaba/MG, Brasil", 50.0 * PdfPageFormat.mm, top - 30.0 * PdfPageFormat.mm);
    g.drawString(ttf, 12.0, "CEP: 38.025-380", 50.0 * PdfPageFormat.mm, top - 40.0 * PdfPageFormat.mm);

    String sTipPrc = "Cliente: " + prcSel.cliente;
    g.drawString(font, 12.0, sTipPrc, 10, top - 135.0);

    Clientes cli = new Clientes(prcSel.idcliente, 0, '', '', '', '', '', '', '', '', false, null);
    cli = await Servicos.buscaClientePorID(cli.idePrc);
    String sEndereco = "Endereco: " + cli.endereco;
    if (cli.telefone.length > 1) {
      sEndereco = sEndereco + ' - ' + cli.telefone;
    }
    sEndereco = sEndereco + ' - ' + cli.cidade;
    g.drawString(font, 12.0, sEndereco, 10, top - 150.0);

    /// Retangulo cinza
    g.setColor(new PdfColor(0.8, 0.8, 0.8));
    g.drawRect(0, top - 260, largura, 20);
    g.fillPath();

    g.setColor(new PdfColor(0.8, 0.8, 0.8));
    g.drawRect(280, 45, 1, 500);
    g.fillPath();

    /// Continua cor preto
    g.setColor(new PdfColor(0.0, 0.0, 0.0));
    g.fillPath();

    String sConfirma = "Pedido: " + prcSel.numero.toString();

    String sGuarde = prcSel.status == 'ABE' ? 'Status: Aguardando aprovação' : prcSel.status == 'APR' ? 'Status: Aprovado' : 'Status: Entregue';

    String sContato = "Condição de pagamento: " + prcSel.nomecondicao + " / Valor total (R\$): " + precoItensComDescCtrl.text;

    String sObservacao = "Observacão: " + prcSel.observacao;

    //g.drawString(font, 12.0, sConfirma, 10, top - 160.0);
    sGuarde = sConfirma + " - " + sGuarde;
    g.drawString(font, 12.0, sGuarde, 10, top - 175.0);
    g.drawString(font, 12.0, sContato, 10, top - 190.0);
    if (sObservacao.length > 100) {
      // Linha 1
      g.drawString(font, 12.0, sObservacao.substring(0, 100), 10, top - 205.0);
      sObservacao = sObservacao.substring(101, sObservacao.length);
      // Linha 2
      if ((sObservacao.length > 0) && (sObservacao.length <= 100)) {
        g.drawString(font, 12.0, "   " + sObservacao.substring(0, sObservacao.length), 10, top - 2015.0);
        sObservacao = "";
      } else if (sObservacao.length > 0) {
        g.drawString(font, 12.0, "   " + sObservacao.substring(0, 100), 10, top - 215.0);
        sObservacao = sObservacao.substring(101, sObservacao.length);
      }
      // Linha 3
      if ((sObservacao.length > 0) && (sObservacao.length <= 100)) {
        g.drawString(font, 12.0, "   " + sObservacao.substring(0, sObservacao.length), 10, top - 220.0);
        sObservacao = "";
      } else if (sObservacao.length > 0) {
        g.drawString(font, 12.0, "   " + sObservacao.substring(0, 100), 10, top - 220.0);
        sObservacao = sObservacao.substring(101, sObservacao.length);
      }
    } else {
      g.drawString(font, 12.0, sObservacao, 10, top - 205.0);
    }

    int posicao = top.toInt() - 270;
    itensCarrinho.forEach((i) {
      String sProduto = i.produto;
      auxVlrCtrl.text = i.precoTotal.toStringAsFixed(2);
      g.drawString(font, 8.0, sProduto, 2.0, posicao.toDouble());
      g.drawString(font, 8.0, i.quantidade.toString(), 225.0, posicao.toDouble());
      g.drawString(font, 8.0, auxVlrCtrl.text, 255.0, posicao.toDouble());
      posicao = posicao - 10;
    });
    //g.drawString(font, 16.0, "Identificação única do pedido", 190.0, top - 230);

    /*if (prc.tipPrc.toLowerCase().contains("neg")) {
      sQtdRel += "oportunidades relacionadas: " + prc.qtdRlc.toString() + ".";
    } else {
      sQtdRel += "Negócio relacionado: " + prc.ideNeg + ".";
    }*/
    g.drawString(ttf, 11.0, 'Produto', 3.0, top - 255);
    g.drawString(ttf, 11.0, 'Qtde.', 222.0, top - 255);
    g.drawString(ttf, 11.0, 'Vlr.', 262.0, top - 255);
    g.drawString(ttf, 11.0, 'Produto', 300.0, top - 255);
    g.drawString(ttf, 11.0, 'Qtde.', 556.0, top - 255);
    g.drawString(ttf, 11.0, 'Vlr.', 590.0, top - 255);

    String aVarAux = DateFormat('dd/MM/yyyy HH:mm').format(new DateTime.now());

    /// Rodapé
    g.setColor(new PdfColor(0.0, 0.0, 0.0));
    g.fillPath();
    g.drawString(
        font,
        11.0,
        "Data e hora local da visualização: " + aVarAux /*+ " / Identificação da impressão: "*/
        ,
        5.0 * PdfPageFormat.mm,
        15.0);

    final file = await _localFile;

    file.writeAsBytesSync(pdf.save());
    OpenFile.open(file.path);
  }

  Future<Null> calculaTotalItensPDF(Processos prc) async {
    precoItensComDescCtrl.text = "0.00";
    precoItensSemDescCtrl.text = "0.00";
    double vlr = 0.00;
    double vlrSemDesc = 0.00;
    double desconto = 0.00;

    await populaPedidoItens(prc);

    if ((itensCarrinho != null) && (itensCarrinho.length > 0)) {
      itensCarrinho.forEach((item) {
        vlr += item.precoTotal; // * item.quantidade;
      });

      vlrSemDesc = vlr;

      if ((prc != null) && (prc.perdesc > 0)) {
        desconto = (vlr * (prc.perdesc / 100));
        vlr = vlr - desconto;
        //vlr = stringToDouble(vlr.toStringAsFixed(2));
      }

      vlr = double.parse(vlr.toStringAsFixed(2));

      precoItensSemDescCtrl.text = vlrSemDesc > 0 ? vlrSemDesc.toStringAsFixed(2) : '0.00';
      precoItensComDescCtrl.text = vlr > 0 ? vlr.toStringAsFixed(2) : '0.00';
    }
    //Atualiza independente se calculou, pois há objetos que chamam este método e
    //para não atualizar duas vezes (uma ao alterar o objeto e outra ao calcular),
    //sempre seta o estado, independente se calculou
    //setState(() {});
    geraPDF(prc);
  }

  Future<Null> populaPedidoItens(Processos prcSel) async {
    itensCarrinho = List<ProcessosItem>();
    await Firestore.instance.collection('pedidos_itens').where('pedido', isEqualTo: prcSel.idePrc).getDocuments().then((snapshot) {
      if ((itensCarrinho == null) || (itensCarrinho.length == 0)) {
        snapshot.documents.map((DocumentSnapshot document) {
          itensCarrinho.add(new ProcessosItem.map(document.data, document.documentID));
        }).toList();
      }
    });
    return;
  }
}
