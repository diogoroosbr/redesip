import 'dart:async';
import 'dart:core';
import 'dart:ui';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:flutter_cupertino_date_picker/flutter_cupertino_date_picker.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:intl/intl.dart';
import 'package:redesipapp/classes.dart';
import 'package:redesipapp/pages/mainPage.dart';
import 'package:redesipapp/servicos.dart';
import 'package:redesipapp/style/autocomplete_textfield.dart';
import 'package:redesipapp/style/primary_button.dart';
import 'package:redesipapp/style/theme.dart';
import 'package:geolocator/geolocator.dart';

class NovaAgendaPage extends StatefulWidget {
  NovaAgendaPage({Key key, this.agenda /*, this.localSelecionado*/});
  static const String routeName = "/NovaAgendaPage";
  final Agendas agenda;
  //final Local localSelecionado;
  @override
  NovaAgendaState createState() {
    NovaAgendaState.prcSel = agenda;
    //EditarAnaliseState.locSel = localSelecionado;
    return NovaAgendaState();
  }
}

class NovaAgendaState extends State<NovaAgendaPage> {
  final _formKey = GlobalKey<FormState>();
  GlobalKey key = new GlobalKey<AutoCompleteTextFieldState<Produtos>>();
  GlobalKey keyc = new GlobalKey<AutoCompleteTextFieldState<Clientes>>();
  GlobalKey keyp = new GlobalKey<AutoCompleteTextFieldState<Pagamentos>>();
  bool isSaving = false;
  static Agendas prcSel;

  final auxVlrCtrl = MoneyMaskedTextController(decimalSeparator: ',', thousandSeparator: '.');

  final dataController = new MaskedTextController(mask: '00/00/0000');

  Timestamp dataLimite = new Timestamp.now();

  final nomParCtr = TextEditingController(text: '' /*prcSel != null && prcSel.nomPar.length > 0 ? prcSel.nomPar : ''*/);

  final FocusNode _observacaoFoco = FocusNode();
  final FocusNode _datNasFoc = FocusNode();

  _fieldFocusChange(BuildContext context, FocusNode currentFocus, FocusNode nextFocus) {
    currentFocus.unfocus();
    FocusScope.of(context).requestFocus(nextFocus);
  }

  //Indicativo para quando alterar o estado quando a tela vier de uma edição
  //bool _estAlt = false;

  List<Clientes> lstCli = new List<Clientes>();
  Clientes dropDownCli;
  AutoCompleteTextField<Clientes> autoCompleteClientes = AutoCompleteTextField<Clientes>();

  final observacaoCtrl = TextEditingController(text: prcSel != null ? prcSel.descricao : "");

  bool pnlVen = false;
  bool pnlCpr = false;
  bool pnlTrc = false;
  bool pnlBtn = false;

  double valorTotal = 0.0;

  NovaAgendaState() {}

  @override
  void initState() {
    super.initState();

    populaClientes().whenComplete(() {
      autoCompleteClientes = new AutoCompleteTextField<Clientes>(
        decoration: new InputDecoration(hintText: "Cliente", suffixIcon: new Icon(Icons.search)),
        itemSubmitted: (item) => setState(() {
              dropDownCli = item;
            }),
        key: keyc,
        //textCapitalization: TextCapitalization.characters,
        suggestions: lstCli,
        itemBuilder: (context, suggestion) => new Container(
              child: Padding(
                  child: new Text(
                    suggestion.nomefantasia,
                    style: TextStyle(fontSize: 10),
                  ),
                  padding: EdgeInsets.all(8.0)),
            ),
        itemSorter: (a, b) => a.nomefantasia == b.nomefantasia ? 0 : a.nomefantasia.length > b.nomefantasia.length ? -1 : 1,

        submitOnSuggestionTap: true, clearOnSubmit: false,
        itemFilter: (suggestion, input) => suggestion.nomefantasia.toLowerCase().contains(input.toLowerCase()),
      );
      //Depois de montar o autocomplete, checa se cliente faz parte da lista (caso
      //não esteja inativo) e atribui o valor ao campo
      if ((prcSel != null) && (prcSel.cliente.length > 0)) {
        lstCli.forEach((f) {
          if (f.nomefantasia == prcSel.cliente) {
            dropDownCli = f;
            autoCompleteClientes.decoration = new InputDecoration(hintText: prcSel.cliente);
            //autoCompleteClientes.createState();
            //autoCompleteClientes.textField.controller.text = prcSel.cliente;
          }
        });
      }
      setState(() {});
    });

    if (prcSel != null) {
      print('tem');

      dataController.updateText(
        prcSel != null && prcSel.datMar != null
            ? DateFormat("dd/MM/yyyy").format(prcSel.datMar.toDate())
            : DateFormat('dd/MM/yyyy').format(DateTime.now().add(const Duration(days: 1))),
      );
    } else {
      print('nao tem');
      setState(() {});
    }
  }

  Future<Null> populaClientes() async {
    await Firestore.instance.collection('clientes').where('deletado', isEqualTo: false).getDocuments().then((snapshot) {
      if ((lstCli == null) || (lstCli.length == 0)) {
        snapshot.documents.map((DocumentSnapshot document) {
          lstCli.add(new Clientes.map(document.data, document.documentID));
        }).toList();
      }
    });
    return;
  }

  Widget titleSection = Container(
    padding: const EdgeInsets.all(32.0),
    child: Row(
      children: [
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                padding: const EdgeInsets.only(bottom: 8.0),
                child: Text(
                  'Teste1',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Text(
                'Teste2',
                style: TextStyle(
                  color: Colors.grey[500],
                ),
              ),
            ],
          ),
        ),
        Icon(
          Icons.star,
          color: Colors.red[500],
        ),
        Text('41'),
      ],
    ),
  );

  //Colocar pra baixo
  List<Widget> _loadWdgNeg(BuildContext context) {
    List<Widget> compraOuVenda = [
      Container(color: Colors.black12, width: MediaQuery.of(context).size.width, padding: new EdgeInsets.all(10.0), child: new Text('Dados da agenda')),
      ListTile(
        leading: Icon(Icons.person_pin),
        title: autoCompleteClientes == null ? Text('Carregando...') : autoCompleteClientes,
      ),
      /*ListTile(
          leading: Icon(Icons.account_balance_wallet),
          title: _listaPagamentos()),*/
      new ListTile(
        leading: const Icon(Icons.more),
        title: TextFormField(
          controller: observacaoCtrl,
          maxLines: 4,
          keyboardType: TextInputType.text,
          textInputAction: TextInputAction.next,
          textCapitalization: TextCapitalization.sentences,
          focusNode: _observacaoFoco,
          onFieldSubmitted: (term) {
            _fieldFocusChange(context, _observacaoFoco, _datNasFoc);
          },
          decoration: InputDecoration(labelText: 'Observação', labelStyle: TextStyle(fontSize: 18.0)),
        ),
      ),
      ListTile(
        leading: const Icon(Icons.date_range),
        title: new Row(children: <Widget>[
          Container(
              width: MediaQuery.of(context).size.width * 0.30,
              child: InkWell(
                  child: TextFormField(
                    controller: dataController,
                    textInputAction: TextInputAction.next,
                    focusNode: _datNasFoc,
                    enabled: false,
                    decoration: InputDecoration(labelText: 'Data', labelStyle: TextStyle(fontSize: 16.0), contentPadding: EdgeInsets.only(top: 24)),
                    validator: (value) {
                      if (value.isEmpty || dataLimite.toDate().isBefore(DateTime.now())) {
                        Scaffold.of(context).showSnackBar(SnackBar(
                            content: Text(
                              'Informe uma data para agenda.',
                              style: TextStyle(color: Colors.white),
                            ),
                            backgroundColor: Tema.corAlerta,
                            duration: Duration(seconds: 10)));
                        return;
                      }
                    },
                  ),
                  onTap: () async {
                    //dropDownHoras = "";
                    //dropDownMinutos = "";
                    DatePicker.showDatePicker(context,
                        cancel: new Text('Voltar'),
                        confirm: new Text('Pronto'),
                        showTitleActions: true,
                        minYear: DateTime.now().year,
                        maxYear: DateTime.now().year + 1,
                        initialYear: DateTime.now().year,
                        initialMonth: DateTime.now().month ,
                        initialDate: DateTime.now().day,
                        locale: 'pt',
                        dateFormat: 'dd-mmm-yyyy', onConfirm: (year, month, date) {
                      dataController.updateText(
                          ("00" + date.toString()).substring(date.toString().length) + '/' + ("00" + month.toString()).substring(month.toString().length) + '/' + year.toString());

                      dataLimite = Timestamp.fromDate(new DateTime(year, month, date));
                    });
                    setState(() {});
                  })),
        ]),
      ),
      Container(
          padding: EdgeInsets.only(left: 40, top: 20),
          child: Text(''),
      )
    ];

    return compraOuVenda;
  }

  @override
  Widget build(BuildContext context) {
    //List<Widget> formByTip = new List<Widget>();

    return Scaffold(
      appBar: AppBar(
        title: prcSel != null ? Text('Editar agenda') : Text('Nova agenda'),
      ),
      body: SingleChildScrollView(
        child: Container(
          key: _formKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Column(children: _loadWdgNeg(context)),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  _botaoConfirma(),
                  Padding(
                    padding: EdgeInsets.only(right: 5.0),
                  ),
                  prcSel == null ? Text('') : _botaoDeletar(),
                ],
              ),
              prcSel == null ? Text('') : _botaoAtender(),
              //prcSel == null ? Text('') : _botaoImprimir(),
            ],
          ),
        ),
      ),
    );
  }

  Widget _botaoDeletar() {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 16.0),
      child: PrimaryButton(
        icon: Icon(
          Icons.delete,
          color: Colors.white,
          size: 40.0,
        ),
        onPressed: () {
          showDialog(
            context: context,
            builder: (context) => AlertDialog(
                  title: new Text('Alerta'),
                  content: new Text('Deseja realmente apagar?'),
                  actions: <Widget>[
                    new FlatButton(
                      onPressed: () => Navigator.of(context).pop(),
                      child: new Text('Não'),
                    ),
                    new FlatButton(
                      onPressed: () async {
                        Agendas _analise = await Servicos.editAgenda(new Agendas(prcSel.ideAge, prcSel.status, prcSel.datGer, prcSel.datMar, prcSel.datApo, prcSel.tipAge,
                            prcSel.idcliente, prcSel.cliente, prcSel.descricao, prcSel.localAtendimento, MainPage.usuario.docId, MainPage.usuario.primeiroNome));
                        Navigator.of(context).pop();
                        Navigator.of(context).pop(_analise);
                      },
                      child: new Text('Sim'),
                    ),
                  ],
                ),
          );
        },
      ),
    );
  }

  Widget _botaoConfirma() {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 16.0),
      child: PrimaryButton(
        icon: Icon(
          Icons.done,
          color: Colors.white,
          size: 50.0,
        ),
        onPressed: isSaving ? null : () => checkAndSubmit(),
      ),
    );
  }

  Widget _botaoAtender() {
    return Padding(
        padding: const EdgeInsets.symmetric(vertical: 16.0),
        child: ConstrainedBox(
          constraints: BoxConstraints.expand(height: 50.0, width: 150.0),
          child: RaisedButton(
            color: Colors.lightGreen,
            shape: new RoundedRectangleBorder(borderRadius: BorderRadius.circular(15.0)),
            child: new Text(
              'Atender',
              style: TextStyle(fontSize: 22),
            ),
            onPressed: isSaving ? null : () => atender(),
          ),
        ));
  }

  void atender() async {
    //Geolocator geolocator = Geolocator()..forceAndroidLocationManager = true;
    //GeolocationStatus geolocationStatus = await geolocator.checkGeolocationPermissionStatus();

    Position position = await Geolocator().getCurrentPosition(desiredAccuracy: LocationAccuracy.high);

    Agendas n = Agendas(prcSel.ideAge, 'ATE', prcSel.datGer, prcSel.datMar, Timestamp.now(), prcSel.tipAge, prcSel.idcliente, prcSel.cliente,
        observacaoCtrl.text, GeoPoint(position.latitude, position.longitude), prcSel.usuGer, prcSel.nomeUsuario);

    Servicos.buscaClientePorID(dropDownCli.idePrc).then((e) async {

      if (e.gps == null || (e.gps.latitude == 0 && e.gps.longitude == 0)) {
        e.gps = GeoPoint(position.latitude, position.longitude);
        await Servicos.editCliente(e);

        Servicos.editAgenda(n).then((t) {
          if (t != null) {
            Navigator.of(context).pop(t);
          } else {
            showDialog(
                context: context,
                builder: (BuildContext b) {
                  Container(height: 200, width: 200, child: new Text('Erro ao atender agenda.'));
                });
          }
        });
      } else {

        double distanceInMeters = await Geolocator().distanceBetween(position.latitude, position.longitude, e.gps.latitude, e.gps.longitude);

        if (distanceInMeters > 200) {
          showDialog(
              context: context,
              builder: (BuildContext b) {
                Container(height: 200, width: 200, child: new Text('Você está muito longe .'));
              });
        } else {
          Servicos.editAgenda(n).then((t) {
            if (t != null) {
              Navigator.of(context).pop(t);
            } else {
              showDialog(
                  context: context,
                  builder: (BuildContext b) {
                    Container(height: 200, width: 200, child: new Text('Erro ao atender agenda.'));
                  });
            }
          });
        }
      }
    });
  }

  /*Widget _botaoImprimir() {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 16.0),
      child: PrimaryButton(
        icon: Icon(
          Icons.print,
          color: Colors.white,
          size: 50.0,
        ),
        onPressed: isSaving ? null : () => geraPdf(),
      ),
    );
  }*/

  void checkAndSubmit() async {
    setState(() {
      isSaving = true;
    });

    //String dateWithT = DateFormat('yyyyMMdd').format(dataLimite.toDate());
    //+ 'T' + ("00" + horLimSel.toString()).substring(horLimSel.toString().length) + ':00:00';
    //DateTime datLim = DateTime.tryParse(dateWithT);

    //Verifica se a condicação de pagamento foi selecionada
    if ((autoCompleteClientes == null) || (autoCompleteClientes.textField.controller.text.length <= 0)) {
      isSaving = false;
      showDialog(
        context: context,
        builder: (context) => new AlertDialog(
              title: new Text('Erro'),
              content: new Text('Informe o cliente'),
              actions: <Widget>[
                new FlatButton(
                  onPressed: () => Navigator.of(context).pop(),
                  child: new Text('OK'),
                ),
              ],
            ),
      );
    } else {
      //Criar novo negócio
      if (prcSel == null) {
        Agendas n = Agendas('', 'ABE', Timestamp.now(), dataLimite, null, 'P', dropDownCli.idePrc, dropDownCli.nomefantasia, observacaoCtrl.text, GeoPoint(0.0, 0.0),
            MainPage.usuario.docId, MainPage.usuario.primeiroNome);
        Servicos.addAgenda(n).then((t) {
          if (t != null) {
            //Inseriu com sucesso
            Navigator.of(context).pop(t);
          } else {
            showDialog(
                context: context,
                builder: (BuildContext b) {
                  Container(height: 200, width: 200, child: new Text('Erro ao inserir agenda.'));
                });
          }
        });
      }
      //Editar negócio existente
      if (prcSel != null) {
        //Abriu tela por um negócio existente
        Agendas n = Agendas(prcSel.ideAge, prcSel.status, prcSel.datGer, prcSel.datMar, prcSel.datApo, 'P', prcSel.idcliente, prcSel.cliente, observacaoCtrl.text, GeoPoint(0.0, 0.0),
            prcSel.usuGer, prcSel.nomeUsuario);

        Servicos.editAgenda(n).then((t) {
          if (t != null) {
            //Inseriu com sucesso
            Navigator.of(context).pop(t);
          } else {
            showDialog(
                context: context,
                builder: (BuildContext b) {
                  Container(height: 200, width: 200, child: new Text('Erro ao editar agenda.'));
                });
          }
        });
      }
    }
  }

  // Converte uma string para double, reduzindo os decimais para 2 dígitos
  double stringToDouble(String s1) {
    String s3 = "";
    if ((s1 != null) && (s1.length > 0) && (s1.contains(","))) {
      s1 = s1.replaceAll(".", "");
      s1 = s1.replaceAll(",", ".");
      s3 = double.parse(s1).toStringAsFixed(2);
      return double.parse(s3);
    }
    num x = s1.lastIndexOf(".");
    String inicio = s1.substring(0, x);
    String decimal = s1.substring(x, s1.length);
    if (inicio.contains(".")) {
      s1 = inicio.replaceAll(".", "") + decimal;
    }
    s3 = s1;
    return double.parse(s3);
  }
}
