import 'dart:io' as Io;
import 'dart:ui';

import 'package:flutter/services.dart' show ByteData, rootBundle;
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_calendar_carousel/classes/event.dart';
import 'package:flutter_calendar_carousel/classes/event_list.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:image/image.dart' as Img;
import 'package:intl/intl.dart';
import 'package:open_file/open_file.dart';
import 'package:pdf/pdf.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:redesipapp/EnviaProdutos.dart';
import 'package:redesipapp/autenticacao/baseAuth.dart';
import 'package:redesipapp/classes.dart';
import 'package:redesipapp/pages/agenda/novaAgenda.dart';
import 'package:redesipapp/pages/cadastros/editarCliente.dart';
import 'package:redesipapp/pages/cadastros/editarProduto.dart';
import 'package:redesipapp/pages/cadastros/listarClientes.dart';
import 'package:redesipapp/pages/cadastros/listarPagamentos.dart';
import 'package:redesipapp/pages/cadastros/listarProdutos.dart';
import 'package:redesipapp/pages/login/cadastro.dart';
import 'package:redesipapp/pages/login/loginUsuario.dart';
import 'package:redesipapp/pages/pedidos/editarNegocio.dart';
import 'package:redesipapp/pages/usuario/insereDadosPessoais.dart';
import 'package:redesipapp/servicos.dart';
import 'package:redesipapp/style/theme.dart';
import 'package:flutter_calendar_carousel/flutter_calendar_carousel.dart' show CalendarCarousel;

import 'cadastros/listarOferta.dart';
//import 'package:permission/permission.dart';
//import 'package:simple_permissions/simple_permissions.dart';
//import 'package:redesipapp/pages/pedidos//editarProcesso.dart';

enum STAPRC { ABE, APR, ENT }
enum OrderBy { desativado, crescente, decrescente }

String editada;

class MainApp extends StatelessWidget {
  MainApp({Key key, this.idUsuario}) : super(key: key);
  final String idUsuario;

  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Rede SIP CRM',
      locale: const Locale('pt', 'BR'),
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      supportedLocales: [
        const Locale('pt', 'BR'),
      ],
      home: new MainPage(
        idUsuario: idUsuario,
        title: 'Rede SIP CRM',
      ),
      theme: new ThemeData(primarySwatch: Tema.swatchPrimario, primaryColor: Tema.corPrimaria),
      routes: <String, WidgetBuilder>{
        MainPage.routeName: (BuildContext context) => new MainPage(),
        InsereDadosPage.routeName: (BuildContext context) => new InsereDadosPage(),
        CadastroPage.routeName: (BuildContext context) => new CadastroPage(),
      },
    );
  }
}

class MainPage extends StatefulWidget {
  MainPage({Key key, this.title, this.idUsuario}) : super(key: key);
  static const String routeName = "/mainPage";
  static FiltroProcesso filtroAnalise = new FiltroProcesso.vazio();
  final String title;
  final String idUsuario;
  static Usuario usuario;
  static String versao = "1.0.0.0";

  @override
  MainPageState createState() => new MainPageState();
}

class MainPageState extends State<MainPage> with TickerProviderStateMixin {
  //TabController tabController;
  int _page = 0;
  PageController tabController2;
  AnimationController controller;
  Animation<Color> animation;

  Query busca;

  bool isOpened = false;
  AnimationController _animationController;
  Animation<Color> _buttonColor;
  Animation<double> _animateIcon;
  Animation<double> _translateButton;
  Curve _curve = Curves.easeOut;
  double _fabHeight = 56.0;

  List<Processos> listaPedido = new List<Processos>();
  List<Processos> listaPedidoFiltro = new List<Processos>();
  List<Processos> listaPedidoFiltro2 = new List<Processos>();
  List<Processos> listaPedidoFiltro3 = new List<Processos>();
  //List<Local> _listaLocal = new List<Local>();

  List<Agendas> listaAgenda = new List<Agendas>();
  DateTime _currentDate;
  EventList<Event> datasAgendadas = new EventList<Event>();

  final FirebaseMessaging _firebaseMessaging = new FirebaseMessaging();
  List<FiltroLocal> listaTotal = new List<FiltroLocal>();
  //List<Usuario> listaFaltando = new List<Usuario>();
  String pesquisa = '';
  TextEditingController ctrPesq = new TextEditingController();

  List<ProcessosItem> itensCarrinho = new List<ProcessosItem>();
  final auxVlrCtrl = MoneyMaskedTextController(decimalSeparator: ',', thousandSeparator: '.');
  MoneyMaskedTextController precoItensComDescCtrl = new MoneyMaskedTextController();
  MoneyMaskedTextController precoItensSemDescCtrl = new MoneyMaskedTextController();

  NumberFormat moneyFormatter = new NumberFormat();
  MoneyMaskedTextController valorController = new MoneyMaskedTextController();

  Widget listaPedidosBanco;

  bool pedidosAbertos;
  bool pedidosEntregues;
  bool pedidosAprovados;

  STAPRC situacao = STAPRC.ABE;

  String turno = "";

  void doAnimation() {
    controller = new AnimationController(duration: const Duration(milliseconds: 2000), vsync: this);
    animation = ColorTween(begin: Tema.corAlerta, end: Colors.white).animate(controller)
      ..addListener(() {
        if (this.animation.isCompleted) {
          editada = '';
          controller.reset();
        }
        setState(() {});
      });
    controller.forward();
  }

  @override
  void initState() {
    pedidosAbertos = true;
    pedidosEntregues = false;
    pedidosAprovados = false;

    moneyFormatter.significantDigitsInUse = true;

    _animationController = AnimationController(vsync: this, duration: Duration(milliseconds: 500))
      ..addListener(() {
        setState(() {});
      });

    _animateIcon = Tween<double>(begin: 0.0, end: 1.0).animate(_animationController);

    _buttonColor = ColorTween(
      begin: Colors.blue,
      end: Colors.red,
    ).animate(CurvedAnimation(
      parent: _animationController,
      curve: Interval(
        0.00,
        1.00,
        curve: Curves.linear,
      ),
    ));

    _translateButton = Tween<double>(
      begin: _fabHeight,
      end: -14.0,
    ).animate(CurvedAnimation(
      parent: _animationController,
      curve: Interval(
        0.0,
        0.75,
        curve: _curve,
      ),
    ));

    //if (MainPage.usuario == null) {
    Firestore.instance.collection('usuarios').document(widget.idUsuario).get().then((u) async {
      MainPage.usuario = new Usuario.map(u.data, widget.idUsuario);

      Servicos.buscaAgendas(MainPage.usuario).then((l) {
        listaAgenda = l;
        listaAgenda.forEach((f) {
          datasAgendadas.add(DateTime(f.datMar.toDate().year, f.datMar.toDate().month, f.datMar.toDate().day),
              new Event(date: DateTime(f.datMar.toDate().year, f.datMar.toDate().month, f.datMar.toDate().day), title: f.cliente, icon: _eventIcon));
        });
        setState(() {});
      });
    });

    //}

    super.initState();

    PermissionHandler().checkServiceStatus(PermissionGroup.storage).then((e) {
      if(e.value == 0 || e.value == 3) {
        PermissionHandler().requestPermissions([PermissionGroup.storage]);
      }
    });

    tabController2 = new PageController(initialPage: _page); //new TabController(initialIndex: 0, vsync: this, length: 2);

    controller = new AnimationController(duration: const Duration(seconds: 2), vsync: this);
    animation = ColorTween(begin: Tema.corAlerta, end: Colors.white).animate(controller)
      ..addListener(() {
        if (this.animation.isCompleted) {
          editada = '';
          controller.reset();
        }
        setState(() {});
      });
  }

  bool visivel = false;

  animate() {
    if (!isOpened) {
      _animationController.forward();
    } else {
      _animationController.reverse();
    }
    isOpened = !isOpened;
  }

  Widget toggle() {
    return Container(
      child: FloatingActionButton(
        heroTag: null,
        backgroundColor: _buttonColor.value,
        onPressed: animate,
        tooltip: 'Opções',
        child: AnimatedIcon(
          icon: AnimatedIcons.menu_close,
          progress: _animateIcon,
        ),
      ),
    );
  }

  Widget flatButtonNovoPedido() {
    return Container(
      child: FloatingActionButton(
        heroTag: null,
        backgroundColor: Colors.indigoAccent,
        onPressed: () {
          Navigator.push(context, new MaterialPageRoute(builder: (context) => new EditarNegocioPage()));
        },
        tooltip: 'Novo Pedido',
        child: Icon(Icons.add_shopping_cart),
      ),
    );
  }

  Widget flatButtonProduto() {
    return Container(
      child: FloatingActionButton(
        heroTag: null,
        backgroundColor: Colors.purple,
        onPressed: () {
          Navigator.push(
              context,
              new MaterialPageRoute(
                  builder: (context) => new EditarProdutoPage(
                        produtoSelecionado: null,
                      )));
        },
        tooltip: 'Novo Produto',
        child: Icon(Icons.note_add),
      ),
    );
  }

  Widget flatButtonPromocao() {
    return Container(
      child: FloatingActionButton(
        heroTag: null,
        backgroundColor: Colors.purple,
        onPressed: () {
          //A FAZER lista de promoções com produtos
//          Navigator.push(
//              context,
//              new MaterialPageRoute(
//                  builder: (context) => new EditarProdutoPage(
//                    produtoSelecionado: null,
//                  )));
        },
        tooltip: 'Promoções',
        child: Icon(Icons.note_add),
      ),
    );
  }

  Widget flatButtonCliente() {
    return Container(
      child: FloatingActionButton(
        heroTag: null,
        backgroundColor: Colors.lime,
        onPressed: () {
          Navigator.push(
              context,
              new MaterialPageRoute(
                  builder: (context) => new EditarClientePage(
                        clienteSelecionado: null,
                      )));
        },
        tooltip: 'Novo Cliente',
        child: Icon(
          Icons.person_add,
        ),
      ),
    );
  }

  @override
  dispose() {
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (MainPage.usuario == null) {
      /*Firestore.instance.collection('usuarios').document(widget.idUsuario).get().then((u) async {
        setState(() {
          MainPage.usuario = new Usuario.map(u.data, widget.idUsuario);
        });
        setState(() {});
      });*/

      return _buildScaffold(new Center(child: new Container(height: 100.0, width: 100.0, child: new CircularProgressIndicator())), '');
    }

    if (MainPage.usuario.token == null || MainPage.usuario.token.length == 0) {
      _firebaseMessaging.getToken().then((t) {
        MainPage.usuario.token = t;
      });
    }

    //Atualiza dados do usuario
    MainPage.usuario.ultAce = new Timestamp.now();
    MainPage.usuario.versao = MainPage.versao;
    Servicos.editUsuario(MainPage.usuario, context);

    return _buildScaffold(
        new Column(children: <Widget>[
          Expanded(
            child:
        new PageView(
            controller: tabController2,
            onPageChanged: (newPage){
              setState((){
                this._page = newPage;
              });
            },
            children: <Widget>[
              _buildAgenda(),
              _buildLista()
              ]),
            /*TabBarView(controller: tabController2, children: <Widget>[
              _buildAgenda(),
              _buildLista()
              //TabBarView(controller: tabController, children: <Widget>[_buildLista(STAPRC.ABE), _buildLista(STAPRC.APR), _buildLista(STAPRC.ENT)])
              //),
            ]),*/
          ),
        ]),
        MainPage.usuario.primeiroNome);
  }

  Widget _buildScaffold(Widget body, /*Widget button,*/ String nomeUsuTitle) {
    return new Scaffold(
        appBar: new AppBar(
          elevation: 1.0,
          //title: new Text(widget.title),
          title: new Text(nomeUsuTitle),
          centerTitle: true,
          actions: <Widget>[
            PopupMenuButton(
              onSelected: (valor) {
                if (valor == 'Clientes') {
                  //Servicos.corrigeIDCliente();
                  Navigator.push(context, new MaterialPageRoute(builder: (context) => new ListarClientesPage()));
                }
                if (valor == 'Produtos') {
                  Navigator.push(
                      context,
                      new MaterialPageRoute(
                          builder: (context) => new ListarProdutosPage(
                                usuario: MainPage.usuario,
                              )));
                }
                if (valor == 'Condições Pgtos.') {
                  Navigator.push(context, new MaterialPageRoute(builder: (context) => new ListarPagamentosPage()));
                }
                if (valor == 'Ofertas') {
                  Navigator.push(context, new MaterialPageRoute(builder: (context) => new ListarOfertasPage()));
                }
                if (valor == 'Upload Produtos') {
                  Navigator.push(context, new MaterialPageRoute(builder: (context) => new AddProdutosScreen()));
                }
                if (valor == 'Minha conta') {
                  Navigator.push(context, new MaterialPageRoute(builder: (context) => new InsereDadosPage(isEditing: true))).then((x) async {
                    if (x != null) {
                      showDialog(
                          context: context,
                          builder: (context) => AlertDialog(
                                title: Text('Dados editados com sucesso.'),
                                actions: <Widget>[
                                  FlatButton(
                                    child: Text('Ok'),
                                    onPressed: () => Navigator.of(context).pop(),
                                  )
                                ],
                              ));
                    }
                  });
                } else if (valor == "Sair") {
                  MainPage.filtroAnalise = new FiltroProcesso.vazio();
                  MainPage.usuario = null;
                  Auth.signOut();
                  //Navigator.pop(context);
                  Navigator.pushReplacement(context, new MaterialPageRoute(builder: (context) => new LoginUsuarioPage()));
                }
              },
              itemBuilder: (context) {
                List<PopupMenuItem> lista = new List<PopupMenuItem>();
                lista.add(PopupMenuItem<String>(
                  value: 'Minha conta',
                  child: Text('Minha conta'),
                ));
                MainPage.usuario.admin
                    ? lista.add(PopupMenuItem<String>(
                        value: 'Clientes',
                        child: Text('Clientes'),
                      ))
                    : null;
                //MainPage.usuario.admin ? //Comentado em 18/04/2019 - 22:56
                lista.add(PopupMenuItem<String>(
                  value: 'Produtos',
                  child: Text('Produtos'),
                ));
                //: null;
                MainPage.usuario.admin
                    ? lista.add(PopupMenuItem<String>(
                        value: 'Condições Pgtos.',
                        child: Text('Condições Pgtos.'),
                      ))
                    : null;
                MainPage.usuario.admin
                    ? lista.add(PopupMenuItem<String>(
                  value: 'Ofertas',
                  child: Text('Ofertas'),
                ))
                    : null;
                lista.add(PopupMenuItem<String>(value: 'Sair', child: Text('Sair')));
                return lista;
              },
            ),
          ],
        ),
        body: body,
        //floatingActionButton: button,

        //FAZER TABBAR SLIDE MUDAR O BOTAO

        bottomNavigationBar: BottomNavigationBar(
          onTap: (int i) {
            tabController2.animateToPage(i, duration: const Duration(milliseconds: 500),curve: Curves.easeInOut);
            setState(() {});
          },
          selectedItemColor: Colors.deepOrange,
          unselectedItemColor: Colors.black38,
          currentIndex: _page, //tabController2.index,
          type: BottomNavigationBarType.fixed,
          showUnselectedLabels: true,
          items: [
            BottomNavigationBarItem(
              icon: Icon(Icons.schedule, ),
              title: new Text('Agenda'),
            ),
            BottomNavigationBarItem(icon: Icon(Icons.shopping_cart, ), title: new Text('Pedidos')),
          ],
        ));
  }

  /*EventList<Event> datasAgendadas = new EventList<Event>(
    events: {
      new DateTime(2019, 5, 3): [
        new Event(
          date: new DateTime(2019, 5, 3),
          title: 'Event 1',
          icon: _eventIcon,
        ),
        new Event(
          date: new DateTime(2019, 5, 3),
          title: 'Event 2',
          icon: _eventIcon,
        ),
        new Event(
          date: new DateTime(2019, 5, 3),
          title: 'Event 3',
          icon: _eventIcon,
        ),
      ],
    },
  );*/

  static Widget _eventIcon = new Container(
    //decoration: new BoxDecoration(color: Colors.white, borderRadius: BorderRadius.all(Radius.circular(1000)), border: Border.all(color: Colors.blue, width: 1.0)),
    child: new Icon(
      Icons.person,
      //color: Colors.blueAccent,
    ),
  );

  Widget _buildAgenda() {
    if (_currentDate == null) _currentDate = new DateTime.now();

    double width070 = MediaQuery.of(context).size.width * 0.7;
    double width080 = MediaQuery.of(context).size.width * 0.80;
    double width090 = MediaQuery.of(context).size.width * 0.90;

    List<Agendas> agendasDoDia = listaAgenda.where((p) => DateFormat('dd/MM/yyyy').format(p.datMar.toDate()) == DateFormat('dd/MM/yyyy').format(_currentDate)).toList();

    return new SingleChildScrollView(
        child: Container(
            height: MediaQuery.of(context).size.height + 100,
            child: new Column(mainAxisAlignment: MainAxisAlignment.start, mainAxisSize: MainAxisSize.max, crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
              CalendarCarousel<Event>(
                locale: "pt_BR", headerMargin: EdgeInsets.all(0.0),
                onDayPressed: (DateTime date, List<Event> events) {
                  _currentDate = date;
                  setState(() {});
                  //events.forEach((event) => print('evento ' + event.title));
                },
                weekendTextStyle: TextStyle(
                  color: Colors.red,
                ),
                thisMonthDayBorderColor: Colors.grey,
                weekFormat: false,
                //markedDates: l,
                markedDatesMap: datasAgendadas,
                customGridViewPhysics: NeverScrollableScrollPhysics(),
                //markedDateIconBuilder: (event) {
                //return _eventIcon; //event.cliente;
                //},
                markedDateIconBuilder: (event) {
                  return event.icon;
                },
                markedDateShowIcon: true,
                markedDateIconMaxShown: 2,
                //minSelectedDate: _currentDate,
                height: MediaQuery.of(context).size.height * 0.62,
                selectedDateTime: _currentDate,
                daysHaveCircularBorder: false,
                markedDateIconMargin: 21,
                markedDateIconOffset: 3,
                nextMonthDayBorderColor: Colors.black12,
                prevMonthDayBorderColor: Colors.black12,
              ),
              Container(
                  height: 300, //MediaQuery.of(context).size.height * 0.2,
                  child: Stack(fit: StackFit.expand, overflow: Overflow.visible, children: [
                    RefreshIndicator(
                        onRefresh: () async {
                          Servicos.buscaAgendas(MainPage.usuario).then((obj) {
                            listaAgenda = obj;
                            agendasDoDia = listaAgenda.where((p) => DateFormat('dd/MM/yyyy').format(p.datMar.toDate()) == DateFormat('dd/MM/yyyy').format(_currentDate)).toList();
                            listaAgenda.forEach((f) {
                              datasAgendadas.add(DateTime(f.datMar.toDate().year, f.datMar.toDate().month, f.datMar.toDate().day),
                                  new Event(date: DateTime(f.datMar.toDate().year, f.datMar.toDate().month, f.datMar.toDate().day), title: f.cliente, icon: _eventIcon));
                            });
                            setState(() {});
                          });
                          return null;
                        },
                        child: agendasDoDia.isEmpty
                            ? new Padding(padding: EdgeInsets.only(left: 10), child: Text('Nada programado neste dia'))
                            : new ListView.builder(
                                itemCount: agendasDoDia.length,
                                //padding: const EdgeInsets.only(top: 10.0),
                                itemExtent: 100.0,
                                itemBuilder: (context, i) {
                                  //setState(() {});
                                  if (i >= agendasDoDia.length) {
                                    //Card para botao float
                                    return new Card(
                                      elevation: 0.0,
                                      color: Colors.transparent,
                                      child: Text(''),
                                    );
                                  }

                                  Agendas _agenda = agendasDoDia[i];

                                  String textoDescricaoCard = _agenda.descricao;

                                  if (textoDescricaoCard.length > 130) {
                                    textoDescricaoCard = textoDescricaoCard.substring(0, 130) + "...";
                                  }

                                  return new GestureDetector(
                                    child: Card(
                                        color: _agenda.status == 'ABE' ? Colors.red : Colors.greenAccent,
                                        child: Padding(
                                            padding: EdgeInsets.all(1.0),
                                            child: new Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, crossAxisAlignment: CrossAxisAlignment.stretch, children: <Widget>[
                                              Column(
                                                mainAxisAlignment: MainAxisAlignment.center,
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: <Widget>[
                                                  Container(
                                                    width: width090,
                                                    child: Text(
                                                      _agenda.cliente,
                                                      style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.bold),
                                                    ),
                                                  ),
                                                  Container(
                                                    width: width090,
                                                    child: Text(textoDescricaoCard, style: TextStyle(fontSize: 10.0)),
                                                  ),
                                                  Container(
                                                      width: width090,
                                                      child: new Text(
                                                        'Por ' + _agenda.nomeUsuario,
                                                        style: TextStyle(fontSize: 12.0, color: Colors.white, fontWeight: FontWeight.bold),
                                                        softWrap: true,
                                                      ))
                                                ],
                                              ),
                                            ]))),
                                    onTap: () {
                                      if (_agenda.status == 'ABE') {
                                        Navigator.push(context, new MaterialPageRoute(builder: (context) => new NovaAgendaPage(agenda: _agenda))).then((ag) {
                                          Servicos.buscaAgendas(MainPage.usuario).then((obj) {
                                            listaAgenda = obj;
                                            agendasDoDia = listaAgenda
                                                .where((p) => DateFormat('dd/MM/yyyy').format(p.datMar.toDate()) == DateFormat('dd/MM/yyyy').format(_currentDate))
                                                .toList();
                                            listaAgenda.forEach((f) {
                                              datasAgendadas.add(
                                                  DateTime(f.datMar.toDate().year, f.datMar.toDate().month, f.datMar.toDate().day),
                                                  new Event(
                                                      date: DateTime(f.datMar.toDate().year, f.datMar.toDate().month, f.datMar.toDate().day), title: f.cliente, icon: _eventIcon));
                                            });
                                            setState(() {});
                                          });
                                        });
                                      }
                                    },
                                  );
                                })),
                    //),
                    Positioned(
                      top: -20, //MediaQuery.of(context).size.height * 0.275 - 12,
                      left: MediaQuery.of(context).size.width - 70,
                      child: FloatingActionButton(
                        heroTag: null,
                        backgroundColor: Colors.indigoAccent,
                        onPressed: () {
                          // parou aqui construir tela de criar agenda
                          Navigator.push(context, new MaterialPageRoute(builder: (context) => new NovaAgendaPage())).then((ag) {
                            Servicos.buscaAgendas(MainPage.usuario).then((obj) {
                              listaAgenda = obj;
                              agendasDoDia = listaAgenda.where((p) => DateFormat('dd/MM/yyyy').format(p.datMar.toDate()) == DateFormat('dd/MM/yyyy').format(_currentDate)).toList();
                              listaAgenda.forEach((f) {
                                datasAgendadas.add(DateTime(f.datMar.toDate().year, f.datMar.toDate().month, f.datMar.toDate().day),
                                    new Event(date: DateTime(f.datMar.toDate().year, f.datMar.toDate().month, f.datMar.toDate().day), title: f.cliente, icon: _eventIcon));
                              });
                              setState(() {});
                            });
                          });
                        },
                        tooltip: 'Nova agenda',
                        child: Icon(Icons.schedule),
                      ),
                    ),
                  ])),
            ])));
  }

  //ultimo removido parametro STAPRC situacao para uso global
  Widget _buildLista() {
    if (!MainPage.usuario.admin) {
      busca = Firestore.instance.collection('pedidos').where('deletado', isEqualTo: false).where('usuario', isEqualTo: MainPage.usuario.usuario);
    } else if (MainPage.usuario.admin) {
      // Como o representante pode gerar OPORTUNIDADES E NEGÓCIOS, é importante
      // gravar na mesma tabela, caso contrário deveria fazer duas buscas na tela
      busca = Firestore.instance.collection('pedidos').where('deletado', isEqualTo: false);
    }

    listaPedidosBanco = listaPedidos(busca, situacao);

    return new Column(children: <Widget>[pesquisaFiltro(), listaPedidosBanco]);
  }

  Widget pesquisaFiltro() {
    return Padding(
        padding: EdgeInsets.all(0.0),
        child: Column(
          children: <Widget>[
            TextField(
                controller: ctrPesq,
                onChanged: (String valor) {
                  setState(() {
                    pesquisa = valor;
                  });
                },
                decoration: InputDecoration(
                    hintText: 'Procurar...',
                    prefixIcon: Padding(
                      padding: EdgeInsets.all(0.0),
                      child: Icon(Icons.search),
                    ),
                    suffixIcon: Padding(
                      padding: EdgeInsets.all(0.0),
                      child: IconButton(
                          icon: Icon(Icons.clear),
                          onPressed: () {
                            setState(() {
                              pesquisa = '';
                              ctrPesq.text = '';
                            });
                          }),
                    ),
                    filled: true,
                    fillColor: Colors.white)),
            Padding(
              padding: EdgeInsets.fromLTRB(0.0, 1.0, 0.0, 1.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  RaisedButton(
                    shape: new RoundedRectangleBorder(borderRadius: BorderRadius.circular(45.0)),
                    color: pedidosAbertos ? Colors.white : Tema.corPicker,
                    onPressed: () {
                      //listaPedidosBanco = new Text('aaaa');

                      //listaPedido = listaPedidoFiltro;// = listaPedido.where((p) => p.status == STAPRC.ABE.toString().split('.')[1]).toList();
                      //_buildLista(STAPRC.ABE);
                      situacao = STAPRC.ABE;
                      _buildLista();
                      pedidosAbertos = true;
                      pedidosEntregues = false;
                      pedidosAprovados = false;
                      //aqui
                      setState(() {});
                    },
                    child: Row(mainAxisAlignment: MainAxisAlignment.start, children: [
                      Text('Abertos'),
                      //pedidosAbertos == OrderBy.crescente ? Icon(Icons.arrow_drop_down) : (pedidosAbertos == OrderBy.decrescente ? Icon(Icons.arrow_drop_up) : Icon(Icons.arrow_drop_up, color: Colors.white))
                    ]),
                  ),
                  RaisedButton(
                    shape: new RoundedRectangleBorder(borderRadius: BorderRadius.circular(45.0)),
                    color: pedidosAprovados ? Colors.white : Tema.corPicker,
                    onPressed: () {
                      situacao = STAPRC.APR;
                      _buildLista();
                      //listaPedidoFiltro = listaPedido.where((p) => p.status == STAPRC.ABE.toString().split('.')[1]).toList();
                      //listaPedidoFiltro2 = listaPedido.where((p) => p.status == STAPRC.APR.toString().split('.')[1]).toList();
                      //listaPedidoFiltro3 = listaPedido.where((p) => p.status == STAPRC.ENT.toString().split('.')[1]).toList();

                      //listaPedidos(busca, STAPRC.APR);
                      pedidosEntregues = false;
                      pedidosAbertos = false;
                      pedidosAprovados = true;
                      setState(() {});
                    },
                    child: Row(children: [
                      Text('Aprovados'),
                      //pedidosEntregues == OrderBy.crescente ? Icon(Icons.arrow_drop_down) : (pedidosEntregues == OrderBy.decrescente ? Icon(Icons.arrow_drop_up) : Icon(Icons.arrow_drop_up, color: Colors.white))
                    ]),
                  ),
                  RaisedButton(
                    shape: new RoundedRectangleBorder(borderRadius: BorderRadius.circular(45.0)),
                    color: pedidosEntregues ? Colors.white : Tema.corPicker,
                    onPressed: () {
                      situacao = STAPRC.ENT;
                      _buildLista();
                      //listaPedidos(busca, STAPRC.ENT);
                      pedidosAprovados = false;
                      pedidosAbertos = false;
                      pedidosEntregues = true;
                      setState(() {});
                    },
                    child: Row(children: [
                      Text('Entregues'),
                      //pedidosAprovados == OrderBy.crescente ? Icon(Icons.arrow_drop_down) : (pedidosAprovados == OrderBy.decrescente ? Icon(Icons.arrow_drop_up) : Icon(Icons.arrow_drop_up, color: Colors.white))
                    ]),
                  ),
                ],
              ),
            ),
          ],
        ));
  }

  Widget floatActionButtonPedido() {
    return Positioned(
      top: -10,
      left: MediaQuery.of(context).size.width / 2.3,
      width: 48,
      height: 48,
      child: FloatingActionButton(
          heroTag: null,
          backgroundColor: Colors.indigoAccent,
          onPressed: () {
            /*MainPage.usuario.admin
                ? Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      MainPage.usuario.admin
                          ? Transform(
                              transform: Matrix4.translationValues(
                                0.0,
                                _translateButton.value * 3.0,
                                0.0,
                              ),
                              child: flatButtonCliente(),
                            )
                          : null,
                      MainPage.usuario.admin
                          ? Transform(
                              transform: Matrix4.translationValues(
                                0.0,
                                _translateButton.value * 2.0,
                                0.0,
                              ),
                              child: flatButtonProduto(),
                            )
                          : null,
                      MainPage.usuario.admin
                          ? Transform(
                              transform: Matrix4.translationValues(
                                0.0,
                                _translateButton.value * 4.0,
                                0.0,
                              ),
                              child: flatButtonPromocao(),
                            )
                          : null,
                      Transform(
                        transform: Matrix4.translationValues(
                          0.0,
                          _translateButton.value,
                          0.0,
                        ),
                        child: flatButtonNovoPedido(),
                      ),
                      toggle(),
                    ],
                  )

                //CLIENTES
                : FloatingActionButton(
                    //Editar processo/análise
                    onPressed: () async {
                    */
                      Navigator.push(context, new MaterialPageRoute(builder: (context) => new EditarNegocioPage())).then((negocio) {
                        if (negocio != null) {
                          setState(() {
                            listaPedido = new List<Processos>();
                            editada = (negocio as Processos).idePrc;
                            //tabController.index = 0;
                            listaPedidos(busca, STAPRC.ABE);
                            doAnimation();
                          });
                        }
                      });
                    },
                    tooltip: 'Novo pedido',
                    child: new Icon(Icons.add),
                  )//;
          //}),
    );
    //]));
  }

  Widget listaPedidos(Query busca, STAPRC situacao) {
    return Container(
      height: MediaQuery.of(context).size.height - 240,
      width: MediaQuery.of(context).size.width,
      child: new StreamBuilder(
          stream: busca.snapshots(),
          builder: (context, snapshot) {
            if (!snapshot.hasData) return Text('Carregando...');

            listaPedido = new List<Processos>();
            snapshot.data.documents.map((DocumentSnapshot document) {
              listaPedido.add(new Processos.map(document.data, document.documentID));
            }).toList();

            listaPedidoFiltro = listaPedido.where((p) => p.status == STAPRC.ABE.toString().split('.')[1]).toList();
            listaPedidoFiltro2 = listaPedido.where((p) => p.status == STAPRC.APR.toString().split('.')[1]).toList();
            listaPedidoFiltro3 = listaPedido.where((p) => p.status == STAPRC.ENT.toString().split('.')[1]).toList();

            if (situacao == STAPRC.ABE && listaPedidoFiltro.isEmpty) {
              return GestureDetector(
                onTap: () async {
                  setState(() {
                    listaPedido = new List<Processos>();
                  });
                },
              );
            } else if (situacao == STAPRC.APR && listaPedidoFiltro2.isEmpty) {
              return GestureDetector(
                onTap: () {
                  setState(() {
                    listaPedido = new List<Processos>();
                  });
                },
              );
            } else if (situacao == STAPRC.ENT && listaPedidoFiltro3.isEmpty) {
              return GestureDetector(
                onTap: () {
                  setState(() {
                    listaPedido = new List<Processos>();
                  });
                },
              );
            }

            return Container(
                height: 3, //MediaQuery.of(context).size.height * 0.2,
                child: Stack(fit: StackFit.loose, overflow: Overflow.visible, children: [
                  RefreshIndicator(
                      onRefresh: () async {
                        setState(() {
                          listaPedido = new List<Processos>();
                        });
                        return null;
                      },
                      child: new ListView.builder(
                          itemCount: (situacao == STAPRC.ABE ? listaPedidoFiltro.length : (situacao == STAPRC.APR ? listaPedidoFiltro2.length : listaPedidoFiltro3.length)) + 1,
                          padding: const EdgeInsets.only(top: 10.0),
                          itemExtent: 100.0,
                          itemBuilder: (context, i) {
                            if (i >= (situacao == STAPRC.ABE ? listaPedidoFiltro.length : (situacao == STAPRC.APR ? listaPedidoFiltro2.length : listaPedidoFiltro3.length))) {
                              //Card para botao float
                              return new Card(
                                elevation: 0.0,
                                color: Colors.transparent,
                                child: Text(''),
                              );
                            }

                            Processos _prc = situacao == STAPRC.ABE ? listaPedidoFiltro[i] : (situacao == STAPRC.APR ? listaPedidoFiltro2[i] : listaPedidoFiltro3[i]);

                            String qtd = "";
                            valorController.updateValue(_prc.valortotal);
                            String textoClienteCard = _prc.numero.toString() + ' - ' + _prc.cliente;

                            String textoObservacaoCard = _prc.observacao;

                            if (textoObservacaoCard.length > 130) {
                              textoObservacaoCard = textoObservacaoCard.substring(0, 130) + "...";
                            }

                            //70% da largura da tela - aplicado quando tem estado/cidade vizinhos
                            double width070 = MediaQuery.of(context).size.width * 0.7;
                            double width080 = MediaQuery.of(context).size.width * 0.80;
                            double width090 = MediaQuery.of(context).size.width * 0.90;

                            return new GestureDetector(
                                child: Card(
                                    color: _prc.idePrc == editada ? animation.value : Colors.white,
                                    child: Padding(
                                        padding: EdgeInsets.all(1.0),
                                        child: new Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, crossAxisAlignment: CrossAxisAlignment.stretch, children: <Widget>[
                                          Column(
                                            mainAxisAlignment: MainAxisAlignment.center,
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Container(
                                                width: situacao == STAPRC.ABE ? width070 : situacao == STAPRC.APR ? width080 : width090,
                                                child: Text(
                                                  textoClienteCard,
                                                  style: TextStyle(fontSize: 15.0, color: Colors.red[800], fontWeight: FontWeight.bold),
                                                ),
                                              ),
                                              Container(
                                                width: situacao == STAPRC.ABE ? width070 : situacao == STAPRC.APR ? width080 : width090,
                                                child: Text(textoObservacaoCard, style: TextStyle(fontSize: 10.0)),
                                              ),
                                              Container(
                                                  width: situacao == STAPRC.ABE ? width070 : situacao == STAPRC.APR ? width080 : width090,
                                                  child: new Text(
                                                    DateFormat("dd/MM/yyyy").format(_prc.datGer.toDate()) +
                                                        ' por ' +
                                                        _prc.nomeusuario +
                                                        ' - ' +
                                                        _prc.nomecondicao +
                                                        ' R\$ ' +
                                                        valorController.text,
                                                    style: TextStyle(fontSize: 11.0, color: Colors.blue[800], fontWeight: FontWeight.bold),
                                                    softWrap: true,
                                                  ))
                                            ],
                                          ),

                                          // Botões do card
                                          situacao == STAPRC.ABE
                                              ? Container(
                                                  //width: 110.0,
                                                  child: Row(mainAxisAlignment: MainAxisAlignment.spaceAround, crossAxisAlignment: CrossAxisAlignment.center, children: <Widget>[
                                                  Row(
                                                    mainAxisAlignment: MainAxisAlignment.center,
                                                    children: situacao == STAPRC.ABE && !MainPage.usuario.admin
                                                        ? miniBotaoDeletarCard(situacao, _prc)
                                                        : miniBotaoAprovarCard(situacao, _prc),
                                                  ),

                                                  //TODO Comentar todo o botão abaixo?
                                                ]))
                                              : (situacao != STAPRC.ENT) && (_prc.usuario == MainPage.usuario.docId || MainPage.usuario.admin)
                                                  ? miniBotaoEntregar(situacao, _prc)
                                                  : Text('')
                                        ]))),
                                onTap: (situacao == STAPRC.ABE)
                                    ? () {
                                        Navigator.push(
                                            context,
                                            new MaterialPageRoute(
                                                builder: (BuildContext context) => new EditarNegocioPage(
                                                      processoSelecionado: _prc,
                                                    ))).then((retorno) {
                                          setState(() {
                                            if (retorno != null) {
                                              if ((retorno as Processos).deletado) {
                                                listaPedido = new List<Processos>();
                                              } else {
                                                _prc.cliente = (retorno as Processos).cliente;
                                                _prc.condicao = (retorno as Processos).condicao;
                                                editada = _prc.idePrc;
                                                doAnimation();
                                              }
                                            }
                                          });
                                        });
                                      }
                                    : // TODO listar PDF
                                    () async => await calculaTotalItensPDF(_prc)); //geraPDF(_prc));
                          })),
                  floatActionButtonPedido()
                ]));
          }),
    );
  }

  List<Widget> miniBotaoDeletarCard(STAPRC situacao, Processos prc) {
    return [
      IconButton(
        icon: Icon(
          Icons.delete,
          color: Colors.grey,
        ),
        onPressed: prc.status != "APR" || MainPage.usuario.admin
            ? () {
                showDialog(
                  context: context,
                  builder: (context) => new AlertDialog(
                        title: Text('Excluir'),
                        content: Text("Deseja realmente excluir?"),
                        actions: <Widget>[
                          new FlatButton(
                              child: Text('Não'),
                              onPressed: () {
                                Navigator.pop(context);
                              }),
                          new FlatButton(
                            child: Text('Sim'),
                            onPressed: () async {
                              prc.deletado = true;
                              await Servicos.editProcesso(prc, null);
                              setState(() {
                                listaPedido = new List<Processos>();
                              });
                              Navigator.of(context).pop();
                            },
                          )
                        ],
                      ),
                );
              }
            : null,
      )
    ];
  }

  List<Widget> miniBotaoAprovarCard(STAPRC situacao, Processos prc) {
    return [
      IconButton(
        icon: Icon(
          Icons.check_circle,
          color: situacao == STAPRC.APR ? Colors.grey : Colors.green,
        ),
        onPressed: !MainPage.usuario.admin || situacao == STAPRC.APR
            ? null
            : () {
                showDialog(
                  builder: (context) => AlertDialog(
                        title: Text('Aprovar?'),
                        actions: <Widget>[
                          new FlatButton(
                            child: Icon(
                              Icons.check_circle,
                              color: Colors.green,
                            ),
                            onPressed: () {
                              setState(() {
                                Servicos.editProcesso(
                                        new Processos(
                                          prc.idePrc,
                                          prc.numero,
                                          prc.cliente,
                                          prc.observacao,
                                          prc.condicao,
                                          prc.nomecondicao,
                                          prc.perdesc,
                                          STAPRC.APR.toString().split('.')[1],
                                          prc.datGer,
                                          Timestamp.now(),
                                          prc.dataEntrega,
                                          prc.horaEntrega,
                                          prc.minutoEntrega,
                                          false,
                                          prc.novoValor,
                                          MainPage.usuario.usuario,
                                          prc.usuario,
                                          prc.nomeusuario,
                                          prc.valortotal,
                                          prc.itens,
                                          '',
                                          //false
                                        ),
                                        null)
                                    .then((a) {
                                  editada = prc.idePrc;
                                  //tabController.index = 1;
                                });
                                Navigator.pop(context);
                                doAnimation();
                              });
                            },
                          ),
                          FlatButton(
                            child: Icon(Icons.cancel),
                            onPressed: () => Navigator.pop(context),
                          )
                        ],
                      ),
                  context: context,
                );
              },
      ),
      miniBotaoDeletarCard(situacao, prc).first,
    ];
  }

  Widget miniBotaoEntregar(STAPRC situacao, Processos prc) {
    return IconButton(
      icon: Icon(
        Icons.directions_car,
        color: Colors.red,
      ),
      onPressed: prc.usuario == MainPage.usuario.docId || MainPage.usuario.admin
          ? () {
              showDialog(
                context: context,
                builder: (context) => new AlertDialog(
                      title: Text('Pedido entregue'),
                      content: Text("Este pedido realmente foi entregue?"),
                      actions: <Widget>[
                        new FlatButton(
                            child: Text('Não'),
                            onPressed: () {
                              Navigator.pop(context);
                            }),
                        new FlatButton(
                          child: Text('Sim'),
                          onPressed: () async {
                            //prc.entregue = true;
                            prc.status = STAPRC.ENT.toString().split('.')[1];
                            await Servicos.editProcesso(prc, null);
                            setState(() {
                              listaPedido = new List<Processos>();
                            });
                            Navigator.of(context).pop();
                          },
                        )
                      ],
                    ),
              );
            }
          : null,
    );
  }

  Future<Io.File> get _localFile async {
    final path = await Diretorio().localPath;
    return Io.File('$path/redesip.pdf');
  }

  Future<Null> geraPDF(Processos prcSel) async {
    final pdf = new PdfDocument(deflate: Io.zlib.encode);
    final page = new PdfPage(pdf, pageFormat: PdfPageFormat.letter);
    final g = page.getGraphics();
    final font = new PdfFont.helvetica(pdf);
    final fontTtf = await rootBundle.load('fonts/Anton-Regular.ttf');
    PdfTtfFont ttf = new PdfTtfFont(pdf, fontTtf);

    final top = page.pageFormat.height;
    final largura = page.pageFormat.width;

    //Img.Image img = Img.decodeImage(new Io.File('test.webp').readAsBytesSync());

    ByteData png = await rootBundle.load("images/redesip.png");
    Codec codec = await instantiateImageCodec(png.buffer.asUint8List());
    var frameInfo = await codec.getNextFrame();
    var xxx = await frameInfo.image.toByteData(format: ImageByteFormat.png);
    var image2 = frameInfo.image;

    var bytes = await image2.toByteData();

    PdfImage image = new PdfImage(pdf, image: bytes.buffer.asUint8List(), width: image2.width, height: image2.height);
    g.drawImage(image, 5.0, top - 130.0, 120.0);

    /// Gráficos (criação independente por conta do fillpath)
    g.setColor(new PdfColor(0.0, 0.0, 0.0));
    g.drawRect(0.0, top - 120.0, largura, 1.0);
    g.drawRect(0.0, 40.0, largura, 1.0);
    g.fillPath();

    /// Cabeçalho
    g.setColor(new PdfColor(0.3, 0.3, 0.3));
    g.drawString(ttf, 12.0, "JJGC SUPERMERCADOS LTDA", 50.0 * PdfPageFormat.mm, top - 10.0 * PdfPageFormat.mm);
    g.drawString(ttf, 12.0, "CNPJ: 19.192.939/0001-23", 50.0 * PdfPageFormat.mm, top - 20.0 * PdfPageFormat.mm);
    g.drawString(ttf, 12.0, "Rua Castro Alves, 170 - N. SRA. DA ABADIA - Uberaba/MG, Brasil", 50.0 * PdfPageFormat.mm, top - 30.0 * PdfPageFormat.mm);
    g.drawString(ttf, 12.0, "CEP: 38.025-380", 50.0 * PdfPageFormat.mm, top - 40.0 * PdfPageFormat.mm);

    String sTipPrc = "Cliente: " + prcSel.cliente;
    g.drawString(font, 12.0, sTipPrc, 10, top - 135.0);

    Clientes cli = new Clientes(prcSel.idcliente, 0, '', '', '', '', '', '', '', '', false, null);
    cli = await Servicos.buscaClientePorID(cli.idePrc);
    String sEndereco = "Endereco: " + cli.endereco;
    if (cli.telefone.length > 1) {
      sEndereco = sEndereco + ' - ' + cli.telefone;
    }
    sEndereco = sEndereco + ' - ' + cli.cidade;
    g.drawString(font, 12.0, sEndereco, 10, top - 150.0);

    /// Retangulo cinza
    g.setColor(new PdfColor(0.8, 0.8, 0.8));
    g.drawRect(0, top - 260, largura, 20);
    g.fillPath();

    g.setColor(new PdfColor(0.8, 0.8, 0.8));
    g.drawRect(280, 45, 1, 500);
    g.fillPath();

    /// Continua cor preto
    g.setColor(new PdfColor(0.0, 0.0, 0.0));
    g.fillPath();

    String sConfirma = "Pedido: " + prcSel.numero.toString();

    String sGuarde = prcSel.status == 'ABE' ? 'Status: Aguardando aprovação' : prcSel.status == 'APR' ? 'Status: Aprovado' : 'Status: Entregue';

    String sContato = "Condição de pagamento: " + prcSel.nomecondicao + " / Valor total (R\$): " + precoItensComDescCtrl.text;

    String sObservacao = "Observacão: " + prcSel.observacao;

    //g.drawString(font, 12.0, sConfirma, 10, top - 160.0);
    sGuarde = sConfirma + " - " + sGuarde;
    g.drawString(font, 12.0, sGuarde, 10, top - 175.0);
    g.drawString(font, 12.0, sContato, 10, top - 190.0);
    if (sObservacao.length > 100) {
      // Linha 1
      g.drawString(font, 12.0, sObservacao.substring(0, 100), 10, top - 205.0);
      sObservacao = sObservacao.substring(101, sObservacao.length);
      // Linha 2
      if ((sObservacao.length > 0) && (sObservacao.length <= 100)) {
        g.drawString(font, 12.0, "   " + sObservacao.substring(0, sObservacao.length), 10, top - 2015.0);
        sObservacao = "";
      } else if (sObservacao.length > 0) {
        g.drawString(font, 12.0, "   " + sObservacao.substring(0, 100), 10, top - 215.0);
        sObservacao = sObservacao.substring(101, sObservacao.length);
      }
      // Linha 3
      if ((sObservacao.length > 0) && (sObservacao.length <= 100)) {
        g.drawString(font, 12.0, "   " + sObservacao.substring(0, sObservacao.length), 10, top - 220.0);
        sObservacao = "";
      } else if (sObservacao.length > 0) {
        g.drawString(font, 12.0, "   " + sObservacao.substring(0, 100), 10, top - 220.0);
        sObservacao = sObservacao.substring(101, sObservacao.length);
      }
    } else {
      g.drawString(font, 12.0, sObservacao, 10, top - 205.0);
    }

    int posicao = top.toInt() - 270;
    itensCarrinho.forEach((i) {
      String sProduto = i.produto;
      if (sProduto.length > 32) {
        sProduto = sProduto.substring(0, 32);
        sProduto = sProduto + "..";
      }
      auxVlrCtrl.text = i.precoTotal.toStringAsFixed(2);
      g.drawString(font, 11.0, sProduto, 2.0, posicao.toDouble());
      g.drawString(font, 11.0, i.quantidade.toString(), 225.0, posicao.toDouble());
      g.drawString(font, 11.0, auxVlrCtrl.text, 255.0, posicao.toDouble());
      posicao = posicao - 10;
    });

    g.drawString(ttf, 11.0, 'Produto', 3.0, top - 255);
    g.drawString(ttf, 11.0, 'Qtde.', 222.0, top - 255);
    g.drawString(ttf, 11.0, 'Vlr.', 262.0, top - 255);
    g.drawString(ttf, 11.0, 'Produto', 300.0, top - 255);
    g.drawString(ttf, 11.0, 'Qtde.', 556.0, top - 255);
    g.drawString(ttf, 11.0, 'Vlr.', 590.0, top - 255);

    String aVarAux = DateFormat('dd/MM/yyyy HH:mm').format(new DateTime.now());

    /// Rodapé
    g.setColor(new PdfColor(0.0, 0.0, 0.0));
    g.fillPath();
    g.drawString(
        font,
        11.0,
        "Data e hora local da visualização: " + aVarAux /*+ " / Identificação da impressão: "*/
        ,
        5.0 * PdfPageFormat.mm,
        15.0);

    final file = await _localFile;

    file.writeAsBytesSync(pdf.save());
    OpenFile.open(file.path);
  }

  Future<Null> calculaTotalItensPDF(Processos prc) async {
    precoItensComDescCtrl.text = "0.00";
    precoItensSemDescCtrl.text = "0.00";
    double vlr = 0.00;
    double vlrSemDesc = 0.00;
    double desconto = 0.00;

    await populaPedidoItens(prc);

    if ((itensCarrinho != null) && (itensCarrinho.length > 0)) {
      itensCarrinho.forEach((item) {
        vlr += item.precoTotal; // * item.quantidade;
      });

      vlrSemDesc = vlr;

      if ((prc != null) && (prc.perdesc > 0)) {
        desconto = (vlr * (prc.perdesc / 100));
        vlr = vlr - desconto;
        //vlr = stringToDouble(vlr.toStringAsFixed(2));
      }

      vlr = double.parse(vlr.toStringAsFixed(2));

      precoItensSemDescCtrl.text = vlrSemDesc > 0 ? vlrSemDesc.toStringAsFixed(2) : '0.00';
      precoItensComDescCtrl.text = vlr > 0 ? vlr.toStringAsFixed(2) : '0.00';
    }
    //Atualiza independente se calculou, pois há objetos que chamam este método e
    //para não atualizar duas vezes (uma ao alterar o objeto e outra ao calcular),
    //sempre seta o estado, independente se calculou
    //setState(() {});
    geraPDF(prc);
  }

  Future<Null> populaPedidoItens(Processos prcSel) async {
    itensCarrinho = List<ProcessosItem>();
    await Firestore.instance.collection('pedidos_itens').where('pedido', isEqualTo: prcSel.idePrc).getDocuments().then((snapshot) {
      if ((itensCarrinho == null) || (itensCarrinho.length == 0)) {
        snapshot.documents.map((DocumentSnapshot document) {
          itensCarrinho.add(new ProcessosItem.map(document.data, document.documentID));
        }).toList();
      }
    });
    return;
  }
}
