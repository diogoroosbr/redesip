import 'dart:io';
import 'dart:typed_data';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart';
import 'package:intl/intl.dart';
import 'package:path_provider/path_provider.dart';

class User {
  String _username;
  String _password;

  String get username => _username;
  String get password => _password;

  User(this._username, this._password);

  User.map(dynamic dados) {
    _username = dados['username'];
    _password = dados['password'];
  }
  Map<String, dynamic> toMap() {
    return <String, dynamic>{"username": _username, "password": _password};
  }
}

class Estados {
  String codPai;
  String sigUfs;
  String nomEst;
  int cepIni;
  int cepFin;

  Estados(this.codPai, this.sigUfs, this.nomEst, this.cepIni, this.cepFin);

  Estados.map(Map<String, dynamic> dados) {
    codPai = dados['CODPAI'];
    sigUfs = dados['SIGUFS'];
    nomEst = dados['NOMEST'];
    cepIni = dados['CEPINI'];
    cepFin = dados['CEPFIN'];
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{'CODPAI': codPai, 'SIGUFS': sigUfs, 'NOMEST': nomEst, 'CEPINI': cepIni, 'CEPFIN': cepFin};
  }
}

class EstadosSemCep {
  String codPai;
  String sigUfs;
  String nomEst;

  EstadosSemCep(this.codPai, this.sigUfs, this.nomEst);

  EstadosSemCep.map(Map<String, dynamic> dados) {
    codPai = dados['CODPAI'];
    sigUfs = dados['SIGUFS'];
    nomEst = dados['NOMEST'];
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'CODPAI': codPai,
      'SIGUFS': sigUfs,
      'NOMEST': nomEst,
    };
  }
}

class Cidades {
  String codPai;
  String sigUfs;
  String nomCid;
  //int cepIni;
  //int cepFin;

  Cidades(
    this.codPai,
    this.sigUfs,
    this.nomCid,
    /*this.cepIni, this.cepFin*/
  );

  Cidades.map(Map<String, dynamic> dados) {
    codPai = dados['CODPAI'];
    sigUfs = dados['SIGUFS'];
    nomCid = dados['NOMCID'];
    //cepIni = dados['CEPINI'];
    //cepFin = dados['CEPFIN'];
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'CODPAI': codPai,
      'SIGUFS': sigUfs,
      'NOMCID': nomCid,
      //'CEPINI': cepIni,
      //'CEPFIN': cepFin
    };
  }
}

class Produtos {
  String idePrc; //ID do produto
  //Primeiro agrupamento (código do ramo, ex.: Pecuária, Agricultura)
  int codPro;
  //Segundo agrupamento (código da classe, ex.: Culturas, Adubos)
  String codCla;
  String nomIte;
  String uniMed;
  double preco;
  double quantidade; //Campo virtual -usado apenas para calcular carrinho
  String temDec; //Tem decimais
  bool deletado;

  Produtos(this.idePrc, this.codPro, /*this.codCla, */ this.nomIte, this.preco, this.temDec, this.deletado, this.quantidade);

  Produtos.map(Map<String, dynamic> dados, String _idePrc) {
    idePrc = _idePrc;
    codPro = dados['codigo'];
    //codCla = dados['CODCLA'];
    nomIte = dados['nome'];
    preco = dados['preco'];
    //uniMed = dados['unidade'];
    temDec = dados['decimais'];
    deletado = dados['deletado'];
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'codigo': codPro,
      //'CODCLA': codCla,
      'nome': nomIte,
      'preco': preco,
      //'unidade': uniMed,
      'decimais': temDec,
      'deletado': deletado,
    };
  }
}

class Clientes {
  String idePrc;
  int codigo;
  String razaosocial;
  String nomefantasia;
  String telefone;
  String responsavel;
  String cpfcnpj;
  String cidade;
  String estado; //Tem decimais
  String endereco;
  bool deletado;
  String usuario;
  GeoPoint gps;

  Clientes(this.idePrc, this.codigo, this.razaosocial, this.nomefantasia, this.telefone, this.responsavel, this.cpfcnpj, this.estado, this.cidade, this.endereco, this.deletado,
      this.gps);

  Clientes.map(Map<String, dynamic> dadox, String _idePrc) {
    idePrc = _idePrc;
    codigo = dadox['codigo'];
    razaosocial = dadox['razaosocial'];
    nomefantasia = dadox['nomefantasia'];
    telefone = dadox['telefone'];
    responsavel = dadox['responsavel'];
    cpfcnpj = dadox['cpfcnpj'];
    estado = dadox['estado'];
    cidade = dadox['cidade'];
    endereco = dadox['endereco'];
    deletado = dadox['deletado'];
    gps = dadox['gps'];
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'codigo': codigo,
      'razaosocial': razaosocial,
      'nomefantasia': nomefantasia,
      'telefone': telefone,
      'responsavel': responsavel,
      'cpfcnpj': cpfcnpj,
      'estado': estado,
      'cidade': cidade,
      'endereco': endereco,
      'deletado': deletado,
      'gps': gps,
    };
  }
}

class Agendas {
  String ideAge;
  String status; //ABE = Aberto, ATE = Atendida, DEL = Desmarcada
  Timestamp datGer;
  Timestamp datMar;
  Timestamp datApo; //Data apontada no local
  String tipAge; //Telefonema, Visita local, Email, Videoconferência, etc.
  String idcliente;
  String cliente;
  String usuGer;
  String nomeUsuario;
  String descricao;
  GeoPoint localAtendimento;

  Agendas(this.ideAge, this.status, this.datGer, this.datMar, this.datApo, this.tipAge, this.idcliente, this.cliente, this.descricao, this.localAtendimento, this.usuGer,
      this.nomeUsuario);

  Agendas.map(Map<String, dynamic> dados, String _ideAge) {
    ideAge = _ideAge;
    status = dados['status'];
    datGer = dados['datger'];
    datMar = dados['datmar'];
    datApo = dados['datapo'];
    tipAge = dados['tipage'];
    idcliente = dados['idcliente'];
    cliente = dados['cliente'];
    descricao = dados['descricao'];
    localAtendimento = dados['localatendimento'];
    usuGer = dados['usuger'];
    nomeUsuario = dados['nomeusuario'];
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'status': status,
      'datger': datGer,
      'datmar': datMar,
      'datapo': datApo,
      'idcliente': idcliente,
      'cliente': cliente,
      'descricao': descricao,
      'localatendimento': localAtendimento,
      'usuger': usuGer,
      'nomeusuario': nomeUsuario
    };
  }
}

class Processos {
  int numero;
  String cliente; //Código do cliente
  String observacao;
  String condicao; //Condição de pagamento
  String nomecondicao; //Condição de pagamento
  double perdesc; //Percentual de desconto
  String status;
  String idePrc; //ID do pedido
  Timestamp datGer;
  Timestamp datApr; //Data admitida
  Timestamp dataEntrega;
  String horaEntrega;
  String minutoEntrega;
  bool deletado;
  bool novoValor;
  String aprovador; //Identificador único do administrador que aprovou
  String usuario;
  String nomeusuario;
  double valortotal;
  int itens;
  String idcliente;
  //bool entregue;

  Processos(
    this.idePrc,
    this.numero,
    this.cliente,
    this.observacao,
    this.condicao,
    this.nomecondicao,
    this.perdesc,
    this.status,
    this.datGer,
    this.datApr,
    this.dataEntrega,
    this.horaEntrega,
    this.minutoEntrega,
    this.deletado,
    this.novoValor,
    this.aprovador,
    this.usuario,
    this.nomeusuario,
    this.valortotal,
    this.itens,
    this.idcliente,
    //this.entregue
  );

  Processos.map(Map<String, dynamic> dados, String _idePrc) {
    idePrc = _idePrc;
    numero = dados['numero'];
    cliente = dados['cliente'];
    observacao = dados['observacao'];
    condicao = dados['condicao'];
    nomecondicao = dados['nomecondicao'];
    perdesc = double.parse(stringToDouble(dados['perdesc'].toString()));
    status = dados['status'];
    datGer = dados['geracao'];
    datApr = dados['aprovacao'];
    dataEntrega = dados['dataentrega'];
    horaEntrega = dados['horaentrega'];
    minutoEntrega = dados['minutoentrega'];
    deletado = dados['deletado'];
    novoValor = dados['novovalor'];
    aprovador = dados['aprovador'];
    usuario = dados['usuario'];
    nomeusuario = dados['nomeusuario'];
    valortotal = dados['valortotal'];
    itens = dados['itens'];
    idcliente = dados['idcliente'];
    //entregue = dados['entregue'];
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'numero': numero,
      'cliente': cliente,
      'observacao': observacao,
      'condicao': condicao,
      'nomecondicao': nomecondicao,
      'perdesc': perdesc,
      'status': status,
      'geracao': datGer,
      'aprovacao': datApr,
      'dataentrega': dataEntrega,
      'horaentrega': horaEntrega,
      'minutoentrega': minutoEntrega,
      'deletado': deletado,
      'novovalor': novoValor,
      'aprovador': aprovador,
      'usuario': usuario,
      'nomeusuario': nomeusuario,
      'valortotal': valortotal,
      'itens': itens,
      'idcliente': idcliente,
      //'entregue': entregue,
    };
  }
}

class ProcessosItem {
  String ideIte; //ID do item do pedido
  String idePrc; //ID do pedido
  String produto; //Nome do produto
  double quantidade;
  double precoTotal;
  double precounit;

  ProcessosItem(
    this.ideIte,
    this.idePrc,
    this.produto,
    this.quantidade,
    this.precoTotal,
    this.precounit,
  );

  ProcessosItem.map(Map<String, dynamic> dados, String _ideIte) {
    ideIte = _ideIte;
    idePrc = dados['pedido'];
    produto = dados['produto'];
    quantidade = dados['quantidade'];
    precoTotal = dados['precototal'];
    precounit = dados['precounit'];
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'pedido': idePrc,
      'produto': produto,
      'quantidade': quantidade,
      'precototal': precoTotal,
      'precounit': precounit,
    };
  }
}

class Pagamentos {
  String idePrc;
  String desPag;
  double percentual;
  bool deletado;

  Pagamentos(
    this.idePrc,
    this.desPag,
    this.percentual,
    this.deletado,
  );

  Pagamentos.map(Map<String, dynamic> dados, String _docId) {
    idePrc = _docId;
    desPag = dados['descricao'];
    percentual = double.parse(stringToDouble(dados['percentual'].toString()));
    deletado = dados['deletado'];
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'descricao': desPag,
      'percentual': percentual,
      'deletado': deletado,
    };
  }
}

class Ofertas {
  String idePrc;
  String desOfe;
  double percentual;
  Timestamp inicio; //vigência
  Timestamp vencimento;
  bool deletado;

  Ofertas(
    this.idePrc,
    this.desOfe,
    this.percentual,
    this.inicio,
    this.vencimento,
    this.deletado,
  );

  Ofertas.map(Map<String, dynamic> dados, String _docId) {
    idePrc = _docId;
    desOfe = dados['descricao'];
    percentual = double.parse(stringToDouble(dados['percentual'].toString()));
    inicio = dados['inicio'];
    vencimento = dados['vencimento'];
    deletado = dados['deletado'];
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'descricao': desOfe,
      'percentual': percentual,
      'inicio': inicio,
      'vencimento': vencimento,
      'deletado': deletado,
    };
  }
}

class OfertasItens {
  String ideIte; //ID do item da oferta
  String ideOfe; //ID da oferta
  String idePro; //ID do produto (caso modifiquem o nome no futuro)
  String produto; //Nome do produto
  double percentual;

  OfertasItens(
      this.ideIte,
      this.ideOfe,
      this.idePro,
      this.produto,
      this.percentual
      );

  OfertasItens.map(Map<String, dynamic> dados, String _ideIte) {
    ideIte = _ideIte;
    ideOfe = dados['oferta'];
    idePro = dados['idproduto'];
    produto = dados['produto'];
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'oferta': ideOfe,
      'idproduto': idePro,
      'produto': produto,
    };
  }
}

class Usuario {
  String docId;
  String usuario;
  String primeiroNome = "";
  String ultimoNome;
  int nroCpf;
  int nroCnp;
  Timestamp dataNasc;
  String email;
  int telefone;
  dynamic ultAce;
  String token;
  bool admin;
  bool tutorial;
  String versao;
  //String responsavel;

  Usuario.map(Map<String, dynamic> dados, String _docId) {
    docId = _docId;
    usuario = _docId; //dados['CODUID']; //Identificador único do usuário
    if (dados['PRINOM'] != null) {
      primeiroNome = dados['PRINOM'];
    } else {
      primeiroNome = "";
    }
    ultimoNome = dados['ULTNOM'];
    nroCpf = dados['NROCPF'];
    nroCnp = dados['NROCNP'];
    dataNasc = dados['DATNAS'];
    email = dados['EMAUSU'];
    telefone = dados['NROTEL'];
    if (dados['ULTACE'] != null) {
      ultAce = dados['ULTACE'];
    } else {
      ultAce = new Timestamp.now();
    }
    admin = dados['INDREP'];
    token = dados['TOKUID'];
    tutorial = dados['INDTUT'];
    versao = dados['VERSAO'];
    //responsavel = dados['NOMRES'];
  }

  Usuario(this.docId, this.usuario, this.primeiroNome, this.ultimoNome, this.nroCpf, this.nroCnp, this.dataNasc, this.email, this.telefone, this.admin, this.token, this.tutorial,
      this.versao
      //this.responsavel
      );

  Map<String, dynamic> toMap() {
    if (ultAce == null) {
      ultAce = new Timestamp.now(); //new DateFormat('dd/MM/yyyy').format(new Timestamp.now());
    }
    return <String, dynamic>{
      'CODUID': usuario,
      'PRINOM': primeiroNome,
      'ULTNOM': ultimoNome,
      'NROCPF': nroCpf,
      'NROCNP': nroCnp,
      'DATNAS': dataNasc,
      'EMAUSU': email,
      'NROTEL': telefone,
      'ULTACE': ultAce,
      'INDREP': admin,
      'TOKUID': token,
      'INDTUT': tutorial,
      'VERSAO': versao,
      //'NOMRES': responsavel,
    };
  }
}

class FiltroProcesso {
  List<Produtos> listCultura;
  //Local local;
  bool _isEmpty;
  bool get isEmpty => _isEmpty;
  bool dataAtivado;
  bool culturaAtivado;
  Timestamp marcadaInicio;
  Timestamp marcadaFinal;
  FiltroProcesso(this.dataAtivado, this.marcadaInicio, this.marcadaFinal, this.listCultura, /*this.local,*/ this.culturaAtivado) {
    _isEmpty = false;
  }
  FiltroProcesso.vazio() {
    _isEmpty = true;
    listCultura = new List<Produtos>();
    dataAtivado = false;
  }
}

class FiltroLocal {
  bool seleciona;
  Produtos item;
  FiltroLocal(this.item, this.seleciona);
}

String substituiCaractere(String str) {
  if (str == null || str.length <= 1) str = "0";
  return str.replaceAll('.', '').replaceAll('-', '').replaceAll('/', '').replaceAll('(', '').replaceAll(')', '');
}

String ifNullToInt(String str) {
  if (str == null) {
    str = "0";
  }
  return str;
}

String stringToDouble(String s) {
  if ((s == null) || (s.contains("null")) || (s.length <= 0) || (s == "") || (s == "0")) {
    return "0.00";
  } else if (!s.contains(".")) {
    return double.parse(s).toStringAsFixed(2);
  } else {
    return double.parse(s).toStringAsFixed(2);
  }
}

bool isCPF(String cpf) {
  // considera-se erro CPF's formados por uma sequencia de numeros iguais
  if (cpf.contains("00000000000") ||
      cpf.contains("11111111111") ||
      cpf.contains("22222222222") ||
      cpf.contains("33333333333") ||
      cpf.contains("44444444444") ||
      cpf.contains("55555555555") ||
      cpf.contains("66666666666") ||
      cpf.contains("77777777777") ||
      cpf.contains("88888888888") ||
      cpf.contains("99999999999") ||
      (cpf.length != 11)) return (false);

  String dig10, dig11;
  int sm, i, r, num, peso;

  // "try" - protege o codigo para eventuais erros de conversao de tipo (int)
  try {
    // Calculo do 1o. Digito Verificador
    sm = 0;
    peso = 10;
    for (i = 0; i < 9; i++) {
      // converte o i-esimo caractere do CPF em um numero:
      // por exemplo, transforma o caractere '0' no inteiro 0
      // (48 eh a posicao de '0' na tabela ASCII)
      num = int.parse(cpf[i]) - 48;
      sm = sm + (num * peso);
      peso = peso - 1;
    }

    r = 11 - (sm % 11);
    if ((r == 10) || (r == 11))
      dig10 = String.fromCharCode(0);
    else
      dig10 = String.fromCharCode(r + 48); // converte no respectivo caractere numerico

    // Calculo do 2o. Digito Verificador
    sm = 0;
    peso = 11;
    for (i = 0; i < 10; i++) {
      num = int.parse(cpf[i]) - 48;
      sm = sm + (num * peso);
      peso = peso - 1;
    }

    r = 11 - (sm % 11);
    if ((r == 10) || (r == 11))
      dig11 = String.fromCharCode(0);
    else
      dig11 = String.fromCharCode(r + 48);

    // Verifica se os digitos calculados conferem com os digitos informados.
    if ((dig10 == cpf[9]) && (dig11 == cpf[10]))
      return (true);
    else
      return (false);
  } catch (InputMismatchException) {
    return (false);
  }
}

class NumericTextFormatter extends TextInputFormatter {
  TextEditingValue formatEditUpdate(TextEditingValue oldValue, TextEditingValue newValue) {
    if (newValue.text.length == 0) {
      return newValue.copyWith(text: '');
    } else if (newValue.text.compareTo(oldValue.text) != 0) {
      int num = int.parse(newValue.text.replaceAll(',', ''));
      final f = new NumberFormat("#,###");
      final newString = f.format(num);
      return new TextEditingValue(
          selection: newValue.text.length > oldValue.text.length ? new TextSelection.fromPosition(new TextPosition(offset: newString.length)) : newValue.selection,
          text: newString);
    } else {
      return newValue;
    }
  }
}

/// Máscara de inteiro com separado de milhar.
class QtdMaskedTextController extends TextEditingController {
  QtdMaskedTextController({int initialValue = 0, this.decimalSeparator = ',', this.thousandSeparator = '.', this.rightSymbol = '', this.leftSymbol = ''}) {
    _validateConfig();

    this.addListener(() {
      this.updateValue(this.numberValue);
    });

    this.updateValue(initialValue);
  }

  final String decimalSeparator;
  final String thousandSeparator;
  final String rightSymbol;
  final String leftSymbol;

  // this is the maximum supported for double values.
  final int _maxLength = 19;

  void updateValue(int value) {
    String masked = this._applyMask(value);

    if (masked.length > _maxLength) {
      masked = masked.substring(0, _maxLength);
    }

    if (rightSymbol.length > 0) {
      masked += rightSymbol;
    }

    if (leftSymbol.length > 0) {
      masked = leftSymbol + masked;
    }

    if (masked != this.text) {
      this.text = masked;

      var cursorPosition = super.text.length - this.rightSymbol.length;
      this.selection = new TextSelection.fromPosition(new TextPosition(offset: cursorPosition));
    }
  }

  int get numberValue => int.parse(_getSanitizedText(this.text)); // / 100.0;

//  @override
//  set text(String newText) {
//    String formattedText = "";
//
//    if (newText.length <= _maxLengthForNumbers) {
//      formattedText = newText;
//    } else {
//      formattedText = newText.substring(0, _maxLengthForNumbers);
//    }
//
//    super.text = formattedText + "${this.rightSymbol}";
//
//    var cursorPosition = super.text.length - this.rightSymbol.length;
//
//    this.selection = new TextSelection.fromPosition(
//        new TextPosition(offset: cursorPosition));
//  }

  _validateConfig() {
    bool rightSymbolHasNumbers = _getOnlyNumbers(this.rightSymbol).length > 0;

    if (rightSymbolHasNumbers) {
      throw new ArgumentError("rightSymbol must not have numbers.");
    }
  }

  String _getSanitizedText(String text) {
    if (text.length == 0) return "0";

    String cleanedText = text;

    var valuesToSanitize = [this.thousandSeparator, this.decimalSeparator];

    valuesToSanitize.forEach((val) {
      cleanedText = cleanedText.replaceAll(val, '');
    });

    cleanedText = _getOnlyNumbers(cleanedText);

    return cleanedText;
  }

  String _getOnlyNumbers(String text) {
    String cleanedText = text;

    var onlyNumbersRegex = new RegExp(r'[^\d]');

    cleanedText = cleanedText.replaceAll(onlyNumbersRegex, '');

    return cleanedText;
  }

  String _applyMask(int value) {
    String textRepresentation = value.toStringAsFixed(0).replaceAll('.', this.decimalSeparator);

    List<String> numberParts = [];

    for (var i = 0; i < textRepresentation.length; i++) {
      numberParts.add(textRepresentation[i]);
    }

    const lengthsWithThousandSeparators = [3, 7, 11, 15];

    for (var i = 0; i < lengthsWithThousandSeparators.length; i++) {
      var l = lengthsWithThousandSeparators[i];

      if (numberParts.length > l) {
        numberParts.insert(numberParts.length - l, this.thousandSeparator);
      } else {
        break;
      }
    }

    String numberText = numberParts.join('');

    return numberText;
  }
}

class Diretorio {
  Future<String> get localPath async {
    final directory = await getTemporaryDirectory();
    //await directory.create();
    return directory.path;
  }

  Future<dynamic> get logo async {
    //final filename = '${await localPath}/redesip.png';
    //File file = new File('$filename');
    File file = new File('redesip.png');

    if (file.existsSync()) {
      file.exists().then((e) {
        print('>> logo existe ' + e.toString());
      });
      var image = await file.readAsBytes();
      return image;
    } else {
      file.exists().then((e) {
        print('logo nao existe mas existe ' + e.toString());
      });
      var request = await get('https://cdn2.iconfinder.com/data/icons/business-and-economy/256/business_economic_finance_interprise_company_cart-512.png');
      var bytes = request.bodyBytes; //close();

      var x = await file.writeAsBytes(bytes).then((e) async {
        await e.rename('redesip.png');
        print(' renomeou ');
      }).catchError((e) {
        print(' erro ao renomear ' + e.toString());
      });
      return x;
    }
  }

  Future<ByteData> get fonte async {
    //final filename = '${await localPath}/Anton-Regular.ttf';
    //File file = new File('$filename');
    File file = File(await rootBundle.loadString('fonts/Anton-Regular.ttf'));
    //File file = new File('Anton-Regular.ttf');

    if (await file.exists()) {
      file.exists().then((e) {
        print('fonte existe ' + e.toString());
      });
      //var image = (file.readAsBytesSync() as Uint8List).buffer.asUint8List();
      var image = (await file.readAsBytes() as Uint8List).buffer.asByteData();
      return image;
    } else {
      var request = await get('http://senior.ubyfol.com.br/Anton-Regular.ttf');
      var bytes = request.bodyBytes; //close();
      var x = (await file.writeAsBytes(bytes) as Uint8List).buffer.asByteData();
      /*var x = await file.writeAsBytes(bytes).then((e) {
        print('baixou fonte ');
      }).catchError((e) {
        print('error fonte ' + e.toString());
      });
      */
      return x;
    }
  }
}
