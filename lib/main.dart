/*import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:redesipapp/autenticacao/baseAuth.dart';
import 'package:redesipapp/pages/login/login.dart';
import 'package:redesipapp/pages/mainPage.dart';
import 'package:redesipapp/pages/usuario/insereDadosPessoais.dart';
import 'package:redesipapp/style/theme.dart';

/// Autor: Diogo Roos
/// Função: Verifica se já existe um usuário Logado
/// Parâmetros: email do usuário

void main() {
  /// Função: Trazer o status do usuário do programa
  /// Parâmetros: usuario(firebaseUser)

  Future<FirebaseUser> getUltAce() async {
    FirebaseUser ret;
    await Auth.currentUser().then((usuario) {
      if ((usuario != null)) {
        ret = usuario;
      }
    });
    return ret;
  }

  getUltAce().then((usuario) async {
    if ((usuario != null)) {
      String docId = "";
      bool cad;
      bool representante;
      await Firestore.instance
          .collection('usuarios')
          .where('usuario', isEqualTo: usuario.email)
          .getDocuments()
          .then((contagem) {
        representante = contagem.documents.isEmpty
            ? false
            : contagem.documents.first.data['representante'] ?? false;
        cad = contagem.documents.isEmpty;
        if (!cad) docId = contagem.documents.first.documentID;
      });
      runApp(new UserApp(
        startPage: 'home',
        ideUsu: usuario,
        cad: cad,
        docId: docId,
        representante: representante,
      ));
    } else {
      runApp(new UserApp(
        startPage: 'login',
      ));
    }
  });
}

/// Autor(es): Diogo Roos
/// Criação:
/// Função: Criar as rotas e direcionar o usuário para a mainpage ou pagina de Login
/// Parâmetros: ideUsu.

class UserApp extends StatelessWidget {
  UserApp(
      {this.startPage, this.ideUsu, this.cad, this.docId, this.representante});
  final String startPage;
  final FirebaseUser ideUsu;
  final bool cad;
  final String docId;
  final bool representante;

  @override
  Widget build(BuildContext context) {
    if (startPage.contains("home")) {
      if (cad) {
        return new MaterialApp(
          localizationsDelegates: [
            GlobalMaterialLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
          ],
          supportedLocales: [const Locale('pt', 'BR')],
          home: InsereDadosPage(
            ideUsu: ideUsu,
            isEditing: false,
          ),
          theme: new ThemeData(
              primarySwatch: Tema.swatchPrimario,
              primaryColor: Tema.corPrimaria),
        );
      } else {
        return new MainApp(
          idUsuario: docId,
        );
      }
    } else {
      return new MaterialApp(
          localizationsDelegates: [
            GlobalMaterialLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
          ],
          supportedLocales: [
            const Locale('pt', 'BR')
          ],
          theme: new ThemeData(
              primarySwatch: Tema.swatchPrimario,
              primaryColor: Tema.corPrimaria),
          home: new LoginPage());
    }
  }
}
*/

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:redesipapp/autenticacao/baseAuth.dart';
import 'package:redesipapp/pages/cadastros/editarCliente.dart';
import 'package:redesipapp/pages/cadastros/editarProduto.dart';
import 'package:redesipapp/pages/cadastros/listarClientes.dart';
import 'package:redesipapp/pages/cadastros/listarProdutos.dart';
import 'package:redesipapp/pages/login/cadastro.dart';
import 'package:redesipapp/pages/login/loginUsuario.dart';
import 'package:redesipapp/pages/mainPage.dart';
import 'package:redesipapp/pages/pedidos/editarNegocio.dart';
import 'package:redesipapp/pages/pedidos/editarProcesso.dart';
import 'package:redesipapp/pages/usuario/insereDadosPessoais.dart';
import 'package:redesipapp/style/theme.dart';

void main() {
  FirebaseUser fu; //Preenchido depois da chamada do getFirUsr
  String emaUsu = "";
  String priNom = "";
  String ultNom = "";
  String docIde = "";
  String ultAce = "";
  String temCad = "";
  String strPag = "";

  Future<FirebaseUser> getFirUsr() async {
    await Auth.currentUser().then((usuario) {
      if ((usuario != null)) {
        fu = usuario;
        strPag = "home";
      } else {
        strPag = "login";
      }
    });
    return fu;
  }

  /// Confere se o usuário que logou anteriormente (se for o caso) tem dados
  /// pessoais preenchidos. Se não logou, armazena apenas o último acesso (se
  /// for o caso) para decidir se mostra ou não o splashscreen
  Future<bool> getTemCad() async {
    await Firestore.instance
        .collection('usuarios')
        .where('EMAUSU', isEqualTo: fu.email)
        .getDocuments()
        .then((ideDoc) {
      docIde = ideDoc.documents.first.documentID;
      return ideDoc.documents.isNotEmpty;
    });
    return false;
  }

  getFirUsr().then((e) async {
    if (strPag == "login") {
      runApp(new UserApp(
        startPage: 'login',
        solCad: true,
      ));
    } else {
      await getTemCad().then((temCad) {
        runApp(new UserApp(
          startPage: 'home',
          solCad: temCad,
          docId: docIde,
        ));
      });
    }
  });
}

class UserApp extends StatelessWidget {
  UserApp({this.startPage, this.ideUsu, this.solCad, this.docId});

  final String startPage;
  final FirebaseUser ideUsu;
  final bool solCad;
  final String docId;

  @override
  Widget build(BuildContext context) {
    Widget root;

    if (startPage == "login") {
      root = LoginUsuarioPage();
    }
    if ((startPage == "home") && (solCad)) {
      root = InsereDadosPage(
        ideUsu: ideUsu,
        isEditing: false,
      );
    }
    if ((startPage == "home") && (!solCad)) {
      root = MainApp(
        idUsuario: docId,
      );
    }

    return new MaterialApp(
      title: 'CRM - Rede SIP',
      locale: const Locale('pt', 'BR'),
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      supportedLocales: [const Locale('pt', 'BR')],
      home: root,
      theme: new ThemeData(
          primarySwatch: Tema.swatchPrimario, primaryColor: Tema.corPrimaria),
      routes: <String, WidgetBuilder>{
        MainPage.routeName: (BuildContext context) => new MainPage(),
        ListarProdutosPage.routeName: (BuildContext context) =>
            new ListarProdutosPage(),
        ListarClientesPage.routeName: (BuildContext context) =>
            new ListarClientesPage(),
        EditarClientePage.routeName: (BuildContext context) =>
            new EditarClientePage(),
        EditarProdutoPage.routeName: (BuildContext context) =>
            new EditarProdutoPage(),
        EditarNegocioPage.routeName: (BuildContext context) =>
            new EditarNegocioPage(),
        EditarProcessoPage.routeName: (BuildContext context) =>
            new EditarProcessoPage(),
        InsereDadosPage.routeName: (BuildContext context) =>
            new InsereDadosPage(),
        LoginUsuarioPage.routeName: (BuildContext context) =>
            new LoginUsuarioPage(),
        CadastroPage.routeName: (BuildContext context) => new CadastroPage(),
//        LoginUsuarioPage.routeName: (BuildContext context) =>
//            new LoginUsuarioPage(),
      },
    );
  }
}
