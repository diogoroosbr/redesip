import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:intl/intl.dart';
import 'package:redesipapp/autenticacao/baseAuth.dart';
import 'package:redesipapp/classes.dart';
import 'package:redesipapp/pages/mainPage.dart';
import 'package:redesipapp/pages/usuario/insereDadosPessoais.dart';
import 'package:shared_preferences/shared_preferences.dart';
//import 'package:flutter_facebook_login/flutter_facebook_login.dart';

class Servicos {
  /// --------------------
  /// BLOCO DE AUTENTICAÇÃO
  /// -------------------
  /// DecideTela acontece quando se está dentro da aplicação na tela de login,
  /// quando se faz logoff, por exemplo, ou nunca logou, pois o start do app
  /// já decide na abertura e pula esta parte quando há registro prévio
  static void decideTela(BuildContext contexto, FirebaseUser user) {
    Firestore.instance
        .collection('usuarios')
        .where('EMAUSU', isEqualTo: user.email)
        .getDocuments()
        .then((usuario) {
      if (usuario.documents.isEmpty) {
        irparaInseredados(contexto, user);
      } else {
        var dados = usuario.documents.first;
        Navigator.pushReplacement(
            contexto,
            new MaterialPageRoute(
                builder: (contexto) => new MainApp(
                      idUsuario: dados.documentID,
                    )));
      }
    }).catchError((e) {});
  }

  static void irparaInseredados(BuildContext contexto, FirebaseUser user) {
    Navigator.pushReplacement(
      contexto,
      new MaterialPageRoute(
          builder: (contexto) => new InsereDadosPage(
                ideUsu: user,
                isEditing: false,
              )),
    );
  }

  static Future<String> logaPrograma(BuildContext contexto, TipoLogin tipo,
      String email, String password) async {
    String _email = "";

    await Auth.signOut();


    switch (tipo) {
      case TipoLogin.comum:
        _email = email;
        break;

      case TipoLogin.google:
        //Descarta tipo google
        _email = email;
        break;
        /*
        GoogleSignInAccount googleUser = await Auth.googleSignIn.signIn();
        /*if (await googleUser.authentication == null) {
          return "googleDesativado";
        }*/
        GoogleSignInAuthentication googleAuth;
        await googleUser.authentication.then((e) {
          googleAuth = e;
        }).catchError((e) {
          return "Erro: falha na comunicação com Google. "+e.toString();
        });
        //GoogleSignInAuthentication googleAuth = await googleUser.authentication;
        _email = googleUser.email;
        email = googleAuth.idToken;
        password = googleAuth.accessToken;
        break;
        */
    }
    TipoLogin tipoNovo;
    String primeiro = '';
    List<String> metodo;
    if (_email != null) {
      metodo =
          await Auth.firebaseAuth.fetchSignInMethodsForEmail(email: _email);
    }
    if (metodo.isEmpty)
      tipoNovo = tipo;
    else
      primeiro = metodo.first;

    //Descarta tipo google
    primeiro = 'password';

    if (primeiro == "google.com") {
      tipoNovo = TipoLogin.google;
    } else if (primeiro == 'password') {
      tipoNovo = TipoLogin.comum;
    }

    //Descarta opções
    tipoNovo = TipoLogin.comum;

    //Caso o tipo seja diferente do anterior, loga novamente
    if (tipoNovo != tipo) {
      logaPrograma(contexto, tipoNovo, email, password);
    } else {
      try {
        await Auth.handleSign(tipo, email, password).then((FirebaseUser user) {
          if (user != null) {
            decideTela(contexto, user);
          } else {
            return "Erro: dados incorretos";
          }
        });
      } catch (e) {
        if(e.toString().contains("WRONG_PASS")) {
          return "Erro: dados incorretos.";
        } else {
          return "Erro: serviço inválido";
        }
      }
      return "Ok";
    }
  }

  static Future<String> addUsuario(Usuario u) async {
    String docId;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('PRINOM', u.primeiroNome);
    prefs.setString('EMAUSU', u.email);
    prefs.setString('ULTNOM', u.ultimoNome);
    String dt = new DateFormat('dd/MM/yyyy').format(u.ultAce.toDate());
    prefs.setString('ULTACE', dt);
    prefs.setString('DOCIDE', u.docId);

    //O CODUID não é armazenado no BD. É obtido do ID do documento quando inicia o APP
    CollectionReference documentReference =
        Firestore.instance.collection("usuarios");
    await documentReference.add(u.toMap()).then((document) {
      docId = document.documentID;
    });
    return docId;
  }

  static Future<void> editUsuario(Usuario u, BuildContext contexto) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('PRINOM', u.primeiroNome);
    prefs.setString('EMAUSU', u.email);
    prefs.setString('ULTNOM', u.ultimoNome);
    String dt = new DateFormat('dd/MM/yyyy').format(u.ultAce.toDate());
    prefs.setString('ULTACE', dt);
    prefs.setString('DOCIDE', u.docId);

    await Firestore.instance
        .document('usuarios/' + u.docId)
        .updateData(u.toMap())
        .whenComplete(() {
      if (MainPage.usuario.usuario == u.usuario) MainPage.usuario = u;
    });
  }

  static Future<void> corrigeIDCliente() async {
    Firestore.instance
        .collection('pedidos')
        .where('idcliente', isEqualTo: null)
        .where('deletado', isEqualTo: false)
        .getDocuments().then((e) async {
          e.documents.forEach((x) async {

            Processos p = Processos.map(x.data, x.documentID);
            p.idcliente = await buscaClientePorNome(p.cliente);
            await Firestore.instance.document('pedidos/' + x.documentID).updateData(p.toMap());
          });
    });

  }

  /// --------------------
  /// BLOCO DE ESTADOS/CIDADES
  /// -------------------
  /// Embora seja utilizado para análise, não recebe um objeto análise porque pode
  /// ser utilizado em outros métodos
  static Future<String> achaEstado(String codPai, String sigUfs) async {
    String estado;
    var a = await Firestore.instance
        .collection('E001EST')
        .where('CODPAI', isEqualTo: codPai)
        .where('SIGUFS', isEqualTo: sigUfs)
        .getDocuments()
        .then((obs) {
      estado = obs.documents.first.data[''];
    });
    return estado;
  }

  /// --------------------
  /// BLOCO DE AUTENTICAÇÃO
  /// -------------------

  /// --------------------
  /// BLOCO DE PAGAMENTOS
  /// --------------------
  static Future<Pagamentos> addPagamento(Pagamentos p) async {
    Pagamentos retorno;

    Pagamentos existe = await existePagamento(p);
    if (existe == null) {
      var doc =
          await Firestore.instance.collection("pagamentos").add(p.toMap());
      retorno = new Pagamentos.map((await doc.get()).data, doc.documentID);
    } else {
      retorno.deletado = true;
    }
    return retorno;
  }

  static Future<Pagamentos> editPagamento(Pagamentos prod) async {
    Future<Pagamentos> f = await Firestore.instance
        .document("pagamentos/" + prod.idePrc)
        .updateData(prod.toMap())
        .then((e) {})
        .catchError((e) {});

    return f;
  }

  static Future<Pagamentos> existePagamento(Pagamentos pgto) async {
    var a = await Firestore.instance
        .collection('pagamentos')
        .where('descricao', isEqualTo: pgto.desPag.trim())
        .getDocuments();
    if (a.documents.isNotEmpty) {
      return Pagamentos.map(
          a.documents.first.data, a.documents.first.documentID);
    } else {
      return null;
    }
  }

  /// --------------------
  /// BLOCO DE OFERTAS
  /// --------------------

  static Future<Ofertas> editOferta(
      Ofertas oferta, List<OfertasItens> itens) async {
    //Quando for para aprovar/reprovar o pedido, não trata os itens
    if (itens != null) {
      await deletaOfertasItens(oferta);
      await addOfertasItens(oferta.idePrc, itens);
    }
    await Firestore.instance
        .document("ofertas/" + oferta.idePrc)
        .updateData(oferta.toMap());
    return oferta;
  }

  static Future<Null> deletaOfertasItens(Ofertas oferta) async {
    await Firestore.instance
        .collection('ofertas_itens')
        .where('oferta', isEqualTo: oferta.idePrc)
        .getDocuments()
        .then((obj) {
      obj.documents.forEach((item) async {
        await Firestore.instance
            .document("ofertas_itens/" + item.documentID)
            .delete();
      });
    });

    return null;
  }

  static Future<Null> addOfertasItens(
      String idePrc, List<OfertasItens> itens) async {
    itens.forEach((i) async {
      i.ideOfe = idePrc;
//      i.preco = i.preco.replaceAll(".", "");
//      i.preco = i.preco.replaceAll(",", ".");
      await Firestore.instance.collection("ofertas_itens").add(i.toMap());
    });
    return null;
  }

  static Future<Ofertas> addOferta(Ofertas p, List<OfertasItens> itens) async {
    //Ofertas retorno;
    var doc =
    await Firestore.instance.collection("ofertas").add(p.toMap());

    await addOfertasItens(doc.documentID, itens);

    return p;// new Ofertas.map((await doc.get()).data, doc.documentID);


    /*Ofertas existe = await existeOfertas(p);
    if (existe == null) {
      var doc =
      await Firestore.instance.collection("ofertas").add(p.toMap());
      retorno = new Ofertas.map((await doc.get()).data, doc.documentID);
    } else {
      return null;
    }
    return retorno;
    */
  }



  /// --------------------
  /// BLOCO DE OFERTAS
  /// --------------------

  /// --------------------
  /// BLOCO DE PRODUTOS
  /// --------------------
  static Future<Produtos> addProduto(Produtos p) async {
    Produtos retorno;
    var doc = await Firestore.instance.collection("produtos").add(p.toMap());

    retorno = new Produtos.map((await doc.get()).data, doc.documentID);
    return retorno;
  }

  static Future<Produtos> editProduto(Produtos prod) async {
    Future<Produtos> f = await Firestore.instance
        .document("produtos/" + prod.idePrc)
        .updateData(prod.toMap())
        .then((e) {})
        .catchError((e) {});

    return f;
  }

  /// --------------------
  /// BLOCO DE PRODUTOS
  /// --------------------

  /// --------------------
  /// BLOCO DE CLIENTES
  /// --------------------
  static Future<Clientes> addCliente(Clientes p) async {
    Clientes retorno;

    Clientes existe = await existeCliente(p);
    if (existe == null) {
      var doc = await Firestore.instance.collection("clientes").add(p.toMap());
      retorno = new Clientes.map((await doc.get()).data, doc.documentID);
    } else {
      retorno.deletado = true;
    }
    return retorno;
  }

  static Future<Clientes> editCliente(Clientes prod) async {
    Future<Clientes> f = await Firestore.instance
        .document("clientes/" + prod.idePrc)
        .updateData(prod.toMap())
        .then((e) {})
        .catchError((e) {});

    return f;
  }

  static Future<Clientes> existeCliente(Clientes analise) async {
    QuerySnapshot busca = await Firestore.instance
        .collection('clientes')
        .where('cpfcnpj', isEqualTo: analise.cpfcnpj)
        .getDocuments();
    if (busca.documents.isNotEmpty) {
      return Clientes.map(
          busca.documents.first.data, busca.documents.first.documentID);
    } else {
      return null;
    }
  }

  static Future<Clientes> buscaClientePorID(String cli) async {
    DocumentSnapshot busca = await Firestore.instance
        .collection('clientes')
        .document(cli)
        .get();

    if (await busca.exists) {
      return Clientes.map(busca.data, busca.documentID);
    } else {
      return null;
    }
  }

  static Future<String> buscaClientePorNome(String nome) async {
    QuerySnapshot busca = await Firestore.instance
        .collection('clientes')
        .where('nomefantasia', isEqualTo: nome)
        .getDocuments();
    return busca.documents.first.documentID;
  }

  /// --------------------
  /// BLOCO DE ANÁLISE
  /// -------------------

/*

  static Future<Analise> editAnalise(Analise analise) async {
    bool existe = false;

    existe = await existeAnalise(analise);
    if (!existe || analise.deletado || analise.situacao != 'solicitada') {
      await Firestore.instance
          .document(
              "E100OPO/CTV/" + MainPage.usuario.usuario + "/" + analise.id)
          .updateData(analise.toMap());
    }

    if (existe)
      return null;
    else
      return analise;
  }

  /// Não está usando - em vez disso, usar addProcesso
  static Future<Analise> addAnalise(Analise analise) async {
    bool existe = false;
    Analise retorno;
    existe = await existeAnalise(analise);
    if (!existe) {
      analise.dataSolicitada = DateTime.now();
      var doc =
          await Firestore.instance.collection("pedidos").add(analise.toMap());

      retorno = new Analise.map((await doc.get()).data, doc.documentID);
    }
    return retorno;
  }

  static Future<bool> existeAnalise(Analise analise) async {
    var a = await Firestore.instance
        .collection('pedidos')
        .where('usuario', isEqualTo: MainPage.usuario.usuario)
        .where('deletado', isEqualTo: false)
        //.where('NOMLOC', isEqualTo: analise.local)
        .where('status', isEqualTo: analise.situacao)
        .getDocuments();

    return a.documents.any((f) => f.data['numero'] == analise.local);
  }
*/

  /// --------------------
  /// BLOCO DE ANÁLISE
  /// -------------------

  /// --------------------
  /// BLOCO DE NEGÓCIOS
  /// -------------------

  static Future<Processos> editNegocios(Processos neg) async {
    //if (!existe || processo.deletado || processo.staPrc != 'C') {
    //TODO ao deletar, tem que atualizar as oportunidades?

    /*if ((neg.tipPrc != "Negócio") &&
        (neg.ideNeg == null || neg.ideNeg.length <= 0)) {
      neg.ideNeg = await obtemNegocio(neg, 0);
    }

    if (neg.tipPrc == "Negócio") {
      neg.vlrMed = await atribuiComprasSemNegocio(neg) + await somaVendas(neg);
    }*/

    await Firestore.instance
        .document("pedidos/" + neg.idePrc)
        .updateData(neg.toMap());
    //}
    return neg;
  }

  static Future<Processos> addNegocios(Processos neg) async {
    // TODO alinhar com guilherme - checar se existe negociação em andamento e,
    // caso positivo, já preencher a oportunidade com a negociação de menor valor
    // para que possa ser escalada por um .analista
    neg.datGer = Timestamp.now();

    /*if ((neg.tipPrc != "Negócio") &&
        (neg.ideNeg == null || neg.ideNeg.length <= 0)) {
      String ideNeg = await existeNegocio(neg, 1);
      neg.ideNeg = ideNeg;
      //Atualiza valor do negócio com a nova oportunidade
      await Firestore.instance.document("E100OPO/" + ideNeg).updateData(
          {"VLRMED": await atribuiComprasSemNegocio(neg) + await somaVendas(neg)});
    }

    if (neg.tipPrc == "Negócio") {
      neg.vlrMed = await atribuiComprasSemNegocio(neg) + await somaVendas(neg);
    }*/

    var doc = await Firestore.instance.collection("pedidos").add(neg.toMap());
    var x = await doc.get();
    return new Processos.map(x.data, doc.documentID);
  }

  static Future<Null> atribuirOportunidades(Processos p) async {
    Processos p1;
    Processos p2;
    /*if (p.deletado) {
      p1 = await atribuiComprasSemNegocio(p, "remove");
      p2 = await atribuiVendasSemNegocio(p, "remove");
    } else {
      p1 = await atribuiComprasSemNegocio(p, "insere");
      p2 = await atribuiVendasSemNegocio(p, "insere");
    }*/

    //Atualiza negócio
    //p.qtdRlc = p1.qtdRlc + p2.qtdRlc;
    //p.vlrMed = p1.vlrMed + p2.vlrMed;

    //Firestore.instance.document('E100OPO/' + p.idePrc).updateData({"QTDRLC": p.qtdRlc, "VLRMED": p.vlrMed});

    //TODO parou aqui 03/12/2018 - 17:04
    //Quando editar um novo valor, tem que atualizar todas as oportunidades para se destacarem no app dos clientes
    //e enviar uma notificação
  }

  static Processos pVazio = new Processos(
      "",
      0,
      "",
      "",
      "",
      "",
      0,
      "",
      Timestamp.now(),
      Timestamp.now(),
      Timestamp.now(),
      "",
      "",
      false,
      false,
      "",
      "",
      "",
      0,
      0,
      ''
      //false
      );

  static Future<String> obtemNegocio(Processos analise, int atualiza) async {
    var ideNeg = "";
    var a = await Firestore.instance
        .collection('pedidos')
        .where('cliente', isEqualTo: analise.cliente)
        //.where('TIPPRC', isEqualTo: 'Negócio')
        .where('deletado', isEqualTo: false)
        .where('status', isGreaterThan: 'ABE')
        .getDocuments()
        .then((doc) {
      doc.documents.forEach((f) {
        ideNeg = f.documentID;
      });
    });

    return ideNeg;
  }

  /// --------------------
  /// BLOCO DE PROCESSOS
  /// -------------------

  static Future<Processos> editProcesso(
      Processos processo, List<ProcessosItem> itens) async {
    //Quando for para aprovar/reprovar o pedido, não trata os itens
    if (itens != null) {
      await deletaProcessoItem(processo);
      await addProcessoItem(processo.idePrc, itens);
    }
    await Firestore.instance
        .document("pedidos/" + processo.idePrc)
        .updateData(processo.toMap());
    return processo;
  }

  static Future<Null> deletaProcessoItem(Processos p) async {
    await Firestore.instance
        .collection('pedidos_itens')
        .where('pedido', isEqualTo: p.idePrc)
        .getDocuments()
        .then((locais) {
      locais.documents.forEach((f) async {
        await Firestore.instance
            .document("pedidos_itens/" + f.documentID)
            .delete();
      });
    });

    return null;
  }

  static Future<Processos> addProcesso(
      Processos processos, List<ProcessosItem> itens) async {
    // TODO alinhar com guilherme - checar se existe negociação em andamento e,
    // caso positivo, já preencher a oportunidade com a negociação de menor valor
    // para que possa ser escalada por um analista
    bool existe = false;
    Processos retorno;
    Processos neg = await ultimoProcesso(processos);

    processos.numero = neg != null ? neg.numero + 1 : 1;

    var doc =
        await Firestore.instance.collection("pedidos").add(processos.toMap());

    await addProcessoItem(doc.documentID, itens);

    retorno = new Processos.map((await doc.get()).data, doc.documentID);

    // TODO trabalhar PDF para negócios/oportunidades fechadas
    // trabalhar atualização de valor dos negócios de modo assincrono, isto é,
    // depois que o negócio for inserido, um cálculo abaixo será executado
    // para preencher o valor da negociação após inserida
    // Quando um negócio sofrer alteração de valor, o mesmo deve passar a todas
    // as oportunidades ligadas a ele de que o valor foi alterado

    //retorno = new Processos.map((await doc.get()).data, doc.documentID);
    //}

    return retorno;
  }

  static Future<Null> addProcessoItem(
      String idePrc, List<ProcessosItem> itens) async {
    itens.forEach((i) async {
      i.idePrc = idePrc;
//      i.preco = i.preco.replaceAll(".", "");
//      i.preco = i.preco.replaceAll(",", ".");
      await Firestore.instance.collection("pedidos_itens").add(i.toMap());
    });
    return null;
  }

  //Checa novamente antes de alterar caso o usuário demore a decidir e outro assuma
  static Future<bool> existeProcesso(Processos processo) async {
    //Se estiver inserindo uma nova oportunidade, já retorna false
    if (processo == null ||
        processo.idePrc == null ||
        processo.idePrc.length <= 0) {
      return false;
    }
    var a = await Firestore.instance
        .collection('pedidos')
        .document(processo.idePrc)
        .get();

    return //a.any((f) =>
        a.data['status'] ==
            processo.status; //&& a.data['NOMITE'] == processo.nomIte);
  }

  static Future<Processos> ultimoProcesso(Processos neg) async {
    var docs = await Firestore.instance
        .collection('pedidos')
        .orderBy("numero", descending: true)
        .limit(1)
        .getDocuments();
    if (docs.documents.length > 0) {
      return Processos.map(
          docs.documents.first.data, docs.documents.first.documentID);
    }
  }

  ///
  /// BOLOCO DE Agendas
  ///
  static Future<Agendas> addAgenda(
      Agendas a) async {
    //Agendas retorno;
    var doc =
    await Firestore.instance.collection("agendas").add(a.toMap());

    //retorno = new Agendas.map((await doc.get()).data, doc.documentID);
    return a;
  }

  static Future<Agendas> atendeAgenda(Agendas a) async {

    buscaClientePorID(a.idcliente).then((e) {
      if(e.gps == null) {
        e.gps = a.localAtendimento;
        editCliente(e);
      }
    });

  }

  static Future<List<Agendas>> buscaAgendas(Usuario u) async {
    List<Agendas> lista = new List<Agendas>();

    Query agenda;

    if (u.admin != true) {
      agenda = Firestore.instance.collection('agendas').where('deletado', isLessThan: 'DEL').where('usuario', isEqualTo: u.usuario);
    } else if (u.admin == true) {
      agenda = Firestore.instance.collection('agendas').where('status', isLessThan: 'DEL');
    }

    await agenda.getDocuments().then((c) {
      c.documents.map((agenda) {
        lista.add(new Agendas.map(agenda.data, agenda.documentID));
      }).toList();
    });
    return lista;
  }

  static Future<Agendas> editAgenda(
      Agendas processo) async {
    //Quando for para aprovar/reprovar o pedido, não trata os itens
    await Firestore.instance
        .document("agendas/" + processo.ideAge)
        .updateData(processo.toMap());
    return processo;
  }

  ///----------------

  /// --------------------
  /// BLOCO DE CULTURA
  /// -------------------

  /*static Future<List<Cultura>> geraListaCultura() async {
    List<Cultura> lista = new List<Cultura>();
    await Firestore.instance.collection('produtos').getDocuments().then((c) {
      c.documents.map((cultura) {
        lista.add(new Cultura.map(cultura.data));
      }).toList();
    });
    return lista;
  }*/

  /// --------------------
  /// BLOCO DE CULTURA
  /// -------------------
  ///--------------------
  /// BLOCO DE LOCAL
  /// -------------------
  /*static Future<String> addLocal(Local local) async {
    String id;
    bool existe = false;
    existe = await existeLocal(local);
    if (!existe) {
      await Firestore.instance
          .collection("E010LOC")
          .add(local.toMap())
          .then((_local) {
        if (local.principal) removerPrincipais(_local.documentID);
        id = _local.documentID;
      });
    }
    return id;
  }

  static Future<bool> existeLocal(Local local) async {
    bool retorno = false;
    await Firestore.instance
        .collection('E010LOC')
        .where('CODUID', isEqualTo: MainPage.usuario.docId)
        .where('INDDEL', isEqualTo: false)
        .getDocuments()
        .then((locais) {
      locais.documents.forEach((f) {
        if (f.data['NOMLOC'] == local.fazenda) retorno = true;
      });
    });
    return retorno;
  }

  static Future editLocal(Local local) async {
    if (local.principal) await removerPrincipais(local.id);

    await Firestore.instance
        .document("E010LOC/" + local.id)
        .updateData(local.toMap());
  }

  static Future<bool> temLocal() async {
    bool retorno = false;
    await Firestore.instance
        .collection('E010LOC')
        .where('CODUID', isEqualTo: MainPage.usuario.docId)
        .where('INDDEL', isEqualTo: false)
        .getDocuments()
        .then((locais) => retorno = locais.documents.length >= 1);
    return retorno;
  }

  static Future<bool> tem1Local() async {
    bool retorno = false;
    await Firestore.instance
        .collection('E010LOC')
        .where('CODUID', isEqualTo: MainPage.usuario.docId)
        .getDocuments()
        .then((locais) => retorno = locais.documents.length == 1);
    return retorno;
  }

  static Future removerPrincipais(String _id) async {
    await Firestore.instance
        .collection('E010LOC')
        .where('CODUID', isEqualTo: MainPage.usuario.docId)
        .getDocuments()
        .then((locais) {
      locais.documents.forEach((local) {
        if (local.documentID != _id) {
          local.data['INDPRI'] = false;
          editLocal(new Local.map(local.data, local.documentID));
        }
      });
    });
  }

  static Future<List<String>> listadeLocais() async {
    List<String> lista;
    await Firestore.instance
        .collection('E010LOC')
        .where('CODUID', isEqualTo: MainPage.usuario.docId)
        .where('INDDEL', isEqualTo: false)
        .getDocuments()
        .then((local) {
      local.documents.forEach((locais) {
        lista.add(locais.data['NOMLOC']);
      });
    });

    return lista;
  }

  static Future<List<Local>> listadeLocaL() async {
    List<Local> lista = new List<Local>();
    await Firestore.instance
        .collection('E010LOC')
        .where('CODUID', isEqualTo: MainPage.usuario.docId)
        .where('INDDEL', isEqualTo: false)
        .getDocuments()
        .then((local) {
      local.documents.forEach((locais) {
        lista.add(new Local.map(locais.data, locais.documentID));
      });
    });

    return lista;
  }

  static Future<Local> nomeLocalPrincipal() async {
    //Local retorno = new Local('', '', '', '', '', '', '', '', '', '', false, '', false); //'';
    Local retorno =
        new Local('', '', '', '', '', '', '', '', '', false, '', false); //'';
    await Firestore.instance
        .collection('E010LOC')
        .where('CODUID', isEqualTo: MainPage.usuario.docId)
        .where('INDDEL', isEqualTo: false)
        .where('INDPRI', isEqualTo: true)
        .getDocuments()
        .then((local) {
      local.documents.forEach((f) {
        if (f.data['INDPRI'] == true) {
          retorno = Local.map(f.data, f.documentID);
          //retorno = f.data['NOMLOC'];
        }
      });
    });
    return retorno;
  }

  static Future<Local> localPrincipal() async {
    Local retorno;
    await Firestore.instance
        .collection('E010LOC')
        .where('CODUID', isEqualTo: MainPage.usuario.docId)
        .where('INDDEL', isEqualTo: false)
        .where('INDPRI', isEqualTo: true)
        .getDocuments()
        .then((local) {
      local.documents.forEach((f) {
        if (f.data['INDPRI'] == true)
          retorno = new Local.map(f.data, f.documentID);
      });
    });
    return retorno;
  }*/
}
