import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:redesipapp/classes.dart';
import 'package:redesipapp/style/primary_button.dart';

class AddEstadosScreen extends StatefulWidget {
  /// New elements will appear at the start
  AddEstadosScreen({
    this.ideUsu,
  });

  final String ideUsu;

  @override
  State<StatefulWidget> createState() => new AddEstadosState();
}

class AddEstadosState extends State<AddEstadosScreen> {
  static final GlobalKey<FormState> formKey = new GlobalKey<FormState>();
  static final GlobalKey<FormFieldState<String>> txt001 =
      new GlobalKey<FormFieldState<String>>();
  static final GlobalKey<FormFieldState<String>> txt002 =
      new GlobalKey<FormFieldState<String>>();
  static final GlobalKey<FormFieldState<String>> txt003 =
      new GlobalKey<FormFieldState<String>>();
  static final GlobalKey<FormFieldState<String>> txt004 =
      new GlobalKey<FormFieldState<String>>();
  static final GlobalKey<FormFieldState<String>> txt005 =
      new GlobalKey<FormFieldState<String>>();
  static final GlobalKey<FormFieldState<String>> txt006 =
      new GlobalKey<FormFieldState<String>>();
  static final GlobalKey<FormFieldState<String>> txt007 =
      new GlobalKey<FormFieldState<String>>();
  static final GlobalKey<FormFieldState<String>> txt008 =
      new GlobalKey<FormFieldState<String>>();
  static final GlobalKey<FormFieldState<String>> txt009 =
      new GlobalKey<FormFieldState<String>>();
  static final GlobalKey<FormFieldState<String>> txt010 =
      new GlobalKey<FormFieldState<String>>();
  static final GlobalKey<FormFieldState<String>> txt01 =
      new GlobalKey<FormFieldState<String>>();

  //AddEstadosState({Key key}) : super(key: key);

  List<Estados> l = new List<Estados>();

  void sync() async {
    String sigUfs = "";
    print('lista');
    l.forEach((data) async {
      // this is an anonymous function : a function without name
      print('lista foreach');
      await Firestore.instance.collection("E001EST").add({
        "CODPAI": data.codPai,
        "SIGUFS": data.sigUfs,
        "NOMEST": data.nomEst,
        "CEPINI": data.cepIni,
        "CEPFIN": data.cepFin
      });
      //});
    });
  }

  Future<Null> setCidades() async {
    l.add(new Estados('0055', 'TO', 'Tocantins', 77000000, 77999999));
    l.add(new Estados('0055', 'PA', 'Pará', 66000000, 68899999));
    l.add(new Estados('0055', 'RN', 'Rio Grande do Norte', 59000000, 59999999));
    l.add(new Estados('0055', 'SP', 'São Paulo', 1000000, 19999999));
    l.add(new Estados('0055', 'PI', 'Piauí', 64000000, 64999999));
    l.add(new Estados('0055', 'ES', 'Espírito Santo', 29000000, 29999999));
    l.add(new Estados('0055', 'GO', 'Goiás', 72800000, 76799999));
    l.add(new Estados('0055', 'MS', 'Mato Grosso do Sul', 79000000, 79999999));
    l.add(new Estados('0055', 'PB', 'Paraíba', 58000000, 58999999));
    l.add(new Estados('0055', 'RO', 'Rondônia', 76800000, 76999999));
    l.add(new Estados('0055', 'MG', 'Minas Gerais', 30000000, 39999999));
    l.add(new Estados('0055', 'MA', 'Maranhão', 65000000, 65999999));
    l.add(new Estados('0055', 'RJ', 'Rio de Janeiro', 20000000, 28999999));
    l.add(new Estados('0055', 'AL', 'Alagoas', 57000000, 57999999));
    l.add(new Estados('0055', 'RS', 'Rio Grande do Sul', 90000000, 99999999));
    l.add(new Estados('0055', 'SE', 'Sergipe', 49000000, 49999999));
    l.add(new Estados('0055', 'SC', 'Santa Catarina', 88000000, 89999999));
    l.add(new Estados('0055', 'AP', 'Amapá', 68900000, 68999999));
    l.add(new Estados('0055', 'PR', 'Paraná', 80000000, 87999999));
    l.add(new Estados('0055', 'AC', 'Acre', 69900000, 69999999));
    l.add(new Estados('0055', 'RR', 'Roraima', 69300000, 69399999));
    l.add(new Estados('0055', 'DF', 'Distrito Federal', 70000000, 73699999));
    l.add(new Estados('0055', 'MT', 'Mato Grosso', 78000000, 78899999));
    l.add(new Estados('0055', 'PE', 'Pernambuco', 50000000, 56999999));
    l.add(new Estados('0055', 'CE', 'Ceará', 60000000, 63999999));
    l.add(new Estados('0055', 'BA', 'Bahia', 40000000, 48999999));
    l.add(new Estados('0055', 'AM', 'Amazonas', 69000000, 69899999));

    sync();
    return null;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Enviando estados'),
        ),
        body: SingleChildScrollView(
            child: Container(
                child: PrimaryButton(
                    icon: Icon(
                      Icons.save,
                      color: Colors.white,
                      size: 40.0,
                    ),
                    onPressed: () async {
                      await setCidades();
                    }))));
  }
}
