import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:redesipapp/style/primary_button.dart';

class AddCidadesScreen extends StatefulWidget {
  /// New elements will appear at the start
  AddCidadesScreen({
    this.ideUsu,
  });

  final String ideUsu;

  @override
  State<StatefulWidget> createState() => new AddCidadesState();
}

class Cidades {
  final String desReg; //Descrição da região
  final String sigUfs; //Nome do estado
  final int codIbg;
  final String nomCid; //Nome da cidade
  final String codPai;
  Cidades(this.codIbg, this.desReg, this.sigUfs, this.nomCid, this.codPai);
}

class AddCidadesState extends State<AddCidadesScreen> {
  static final GlobalKey<FormState> formKey = new GlobalKey<FormState>();
  static final GlobalKey<FormFieldState<String>> txt001 =
      new GlobalKey<FormFieldState<String>>();
  static final GlobalKey<FormFieldState<String>> txt002 =
      new GlobalKey<FormFieldState<String>>();
  static final GlobalKey<FormFieldState<String>> txt003 =
      new GlobalKey<FormFieldState<String>>();
  static final GlobalKey<FormFieldState<String>> txt004 =
      new GlobalKey<FormFieldState<String>>();
  static final GlobalKey<FormFieldState<String>> txt005 =
      new GlobalKey<FormFieldState<String>>();
  static final GlobalKey<FormFieldState<String>> txt006 =
      new GlobalKey<FormFieldState<String>>();
  static final GlobalKey<FormFieldState<String>> txt007 =
      new GlobalKey<FormFieldState<String>>();
  static final GlobalKey<FormFieldState<String>> txt008 =
      new GlobalKey<FormFieldState<String>>();
  static final GlobalKey<FormFieldState<String>> txt009 =
      new GlobalKey<FormFieldState<String>>();
  static final GlobalKey<FormFieldState<String>> txt010 =
      new GlobalKey<FormFieldState<String>>();
  static final GlobalKey<FormFieldState<String>> txt01 =
      new GlobalKey<FormFieldState<String>>();

  //AddCidadesState({Key key}) : super(key: key);

  List<Cidades> l = new List<Cidades>();

  void sync() async {
    String sigUfs = "";
    print('lista');
    l.forEach((data) async {
      // this is an anonymous function : a function without name
      print('lista foreach');
      await Firestore.instance.runTransaction((Transaction transaction) async {
        CollectionReference ref2 = Firestore.instance.collection("E001CID");
        await ref2.add({
          "DESREG": data.desReg,
          "SIGUFS": data.sigUfs,
          "CODIBG": data.codIbg,
          "NOMCID": data.nomCid,
          "CODPAI": data.codPai
        });
      });
    });
  }

  void setCidades() async {
    l.add(new Cidades(
        5200050, '5 - Centro-Oeste', 'GO', 'Abadia De Goias', '0055'));
    l.add(new Cidades(
        3100104, '3 - Sudeste', 'MG', 'Abadia Dos Dourados', '0055'));
    l.add(new Cidades(5200100, '5 - Centro-Oeste', 'GO', 'Abadiania', '0055'));
    l.add(new Cidades(3100203, '3 - Sudeste', 'MG', 'Abaete', '0055'));
    l.add(new Cidades(1500107, '1 - Norte', 'PA', 'Abaetetuba', '0055'));
    l.add(new Cidades(2300101, '2 - Nordeste', 'CE', 'Abaiara', '0055'));
    l.add(new Cidades(2900108, '2 - Nordeste', 'BA', 'Abaira', '0055'));
    l.add(new Cidades(2900207, '2 - Nordeste', 'BA', 'Abare', '0055'));
    l.add(new Cidades(4100103, '4 - Sul', 'PR', 'Abatia', '0055'));
    l.add(new Cidades(4200051, '4 - Sul', 'SC', 'Abdon Batista', '0055'));
    l.add(new Cidades(1500131, '1 - Norte', 'PA', 'Abel Figueiredo', '0055'));
    l.add(new Cidades(4200101, '4 - Sul', 'SC', 'Abelardo Luz', '0055'));
    l.add(new Cidades(3100302, '3 - Sudeste', 'MG', 'Abre Campo', '0055'));
    l.add(new Cidades(2600054, '2 - Nordeste', 'PE', 'Abreu E Lima', '0055'));
    l.add(new Cidades(1700251, '1 - Norte', 'TO', 'Abreulandia', '0055'));
    l.add(new Cidades(3100401, '3 - Sudeste', 'MG', 'Acaiaca', '0055'));
    l.add(new Cidades(2100055, '2 - Nordeste', 'MA', 'Acailandia', '0055'));
    l.add(new Cidades(2900306, '2 - Nordeste', 'BA', 'Acajutiba', '0055'));
    l.add(new Cidades(1500206, '1 - Norte', 'PA', 'Acara', '0055'));
    l.add(new Cidades(2300150, '2 - Nordeste', 'CE', 'Acarape', '0055'));
    l.add(new Cidades(2300200, '2 - Nordeste', 'CE', 'Acarau', '0055'));
    l.add(new Cidades(2400109, '2 - Nordeste', 'RN', 'Acari', '0055'));
    l.add(new Cidades(2200053, '2 - Nordeste', 'PI', 'Acaua', '0055'));
    l.add(new Cidades(4300034, '4 - Sul', 'RS', 'Acegua', '0055'));
    l.add(new Cidades(2300309, '2 - Nordeste', 'CE', 'Acopiara', '0055'));
    l.add(new Cidades(5100102, '5 - Centro-Oeste', 'MT', 'Acorizal', '0055'));
    l.add(new Cidades(1200013, '1 - Norte', 'AC', 'Aclandia', '0055'));
    l.add(new Cidades(5200134, '5 - Centro-Oeste', 'GO', 'Acuna', '0055'));
    l.add(new Cidades(2400208, '2 - Nordeste', 'RN', 'Acu', '0055'));
    l.add(new Cidades(3100500, '3 - Sudeste', 'MG', 'Acucena', '0055'));
    l.add(new Cidades(3500105, '3 - Sudeste', 'SP', 'Adamantina', '0055'));
    l.add(new Cidades(5200159, '5 - Centro-Oeste', 'GO', 'Adelandia', '0055'));
    l.add(new Cidades(3500204, '3 - Sudeste', 'SP', 'Adolfo', '0055'));
    l.add(new Cidades(4100202, '4 - Sul', 'PR', 'Adrianopolis', '0055'));
    l.add(new Cidades(2900355, '2 - Nordeste', 'BA', 'Adustina', '0055'));
    l.add(new Cidades(
        2600104, '2 - Nordeste', 'PE', 'Afogados Da Ingazeira', '0055'));
    l.add(new Cidades(2400307, '2 - Nordeste', 'RN', 'Afonso Bezerra', '0055'));
    l.add(new Cidades(3200102, '3 - Sudeste', 'ES', 'Afonso Claudio', '0055'));
    l.add(new Cidades(2100105, '2 - Nordeste', 'MA', 'Afonso Cunha', '0055'));
    l.add(new Cidades(2600203, '2 - Nordeste', 'PE', 'Afranio', '0055'));
    l.add(new Cidades(1500305, '1 - Norte', 'PA', 'Afua', '0055'));
    l.add(new Cidades(2600302, '2 - Nordeste', 'PE', 'Agrestina', '0055'));
    l.add(new Cidades(2200103, '2 - Nordeste', 'PI', 'Agricolandia', '0055'));
    l.add(new Cidades(4200200, '4 - Sul', 'SC', 'Agrolandia', '0055'));
    l.add(new Cidades(4200309, '4 - Sul', 'SC', 'Agronomica', '0055'));
    l.add(
        new Cidades(1500347, '1 - Norte', 'PA', 'Agua Azul Do Norte', '0055'));
    l.add(new Cidades(3100609, '3 - Sudeste', 'MG', 'Agua Boa', '0055'));
    l.add(new Cidades(5100201, '5 - Centro-Oeste', 'MT', 'Agua Boa', '0055'));
    l.add(new Cidades(2200202, '2 - Nordeste', 'PI', 'Agua Branca', '0055'));
    l.add(new Cidades(2500106, '2 - Nordeste', 'PB', 'Agua Branca', '0055'));
    l.add(new Cidades(2700102, '2 - Nordeste', 'AL', 'Agua Branca', '0055'));
    l.add(new Cidades(5000203, '5 - Centro-Oeste', 'MS', 'Agua Clara', '0055'));
    l.add(new Cidades(3100708, '3 - Sudeste', 'MG', 'Agua Comprida', '0055'));
    l.add(new Cidades(4200408, '4 - Sul', 'SC', 'Agua Doce', '0055'));
    l.add(new Cidades(
        2100154, '2 - Nordeste', 'MA', 'Agua Doce Do Maranhao', '0055'));
    l.add(new Cidades(
        3200169, '3 - Sudeste', 'ES', 'Agua Doce Do Norte', '0055'));
    l.add(new Cidades(2900405, '2 - Nordeste', 'BA', 'Agua Fria', '0055'));
    l.add(new Cidades(
        5200175, '5 - Centro-Oeste', 'GO', 'Agua Fria De Goias', '0055'));
    l.add(new Cidades(5200209, '5 - Centro-Oeste', 'GO', 'Agua Limpa', '0055'));
    l.add(new Cidades(2400406, '2 - Nordeste', 'RN', 'Agua Nova', '0055'));
    l.add(new Cidades(2600401, '2 - Nordeste', 'PE', 'Agua Preta', '0055'));
    l.add(new Cidades(4300059, '4 - Sul', 'RS', 'Agua Santa', '0055'));
    l.add(new Cidades(3500303, '3 - Sudeste', 'SP', 'Aguai', '0055'));
    l.add(new Cidades(3100807, '3 - Sudeste', 'MG', 'Aguanil', '0055'));
    l.add(new Cidades(2600500, '2 - Nordeste', 'PE', 'Aguas Belas', '0055'));
    l.add(new Cidades(3500402, '3 - Sudeste', 'SP', 'Aguas Da Prata', '0055'));
    l.add(new Cidades(4200507, '4 - Sul', 'SC', 'Aguas De Chapeco', '0055'));
    l.add(
        new Cidades(3500501, '3 - Sudeste', 'SP', 'Aguas De Lindoia', '0055'));
    l.add(new Cidades(
        3500550, '3 - Sudeste', 'SP', 'Aguas De Santa Barbara', '0055'));
    l.add(new Cidades(
        3500600, '3 - Sudeste', 'SP', 'Aguas De Sao Pedro', '0055'));
    l.add(new Cidades(3100906, '3 - Sudeste', 'MG', 'Aguas Formosas', '0055'));
    l.add(new Cidades(4200556, '4 - Sul', 'SC', 'Aguas Frias', '0055'));
    l.add(new Cidades(
        5200258, '5 - Centro-Oeste', 'GO', 'Aguas Lindas De Goias', '0055'));
    l.add(new Cidades(4200606, '4 - Sul', 'SC', 'Aguas Mornas', '0055'));
    l.add(new Cidades(3101003, '3 - Sudeste', 'MG', 'Aguas Vermelhas', '0055'));
    l.add(new Cidades(4300109, '4 - Sul', 'RS', 'Agudo', '0055'));
    l.add(new Cidades(3500709, '3 - Sudeste', 'SP', 'Agudos', '0055'));
    l.add(new Cidades(4100301, '4 - Sul', 'PR', 'Agudos Do Sul', '0055'));
    l.add(new Cidades(3200136, '3 - Sudeste', 'ES', 'Aguia Branca', '0055'));
    l.add(new Cidades(2500205, '2 - Nordeste', 'PB', 'Aguiar', '0055'));
    l.add(new Cidades(1700301, '1 - Norte', 'TO', 'Aguiarnopolis', '0055'));
    l.add(new Cidades(3101102, '3 - Sudeste', 'MG', 'Aimores', '0055'));
    l.add(new Cidades(2900603, '2 - Nordeste', 'BA', 'Aiquara', '0055'));
    l.add(new Cidades(2300408, '2 - Nordeste', 'CE', 'Aiuaba', '0055'));
    l.add(new Cidades(3101201, '3 - Sudeste', 'MG', 'Aiuruoca', '0055'));
    l.add(new Cidades(4300208, '4 - Sul', 'RS', 'Ajuricaba', '0055'));
    l.add(new Cidades(3101300, '3 - Sudeste', 'MG', 'Alagoa', '0055'));
    l.add(new Cidades(2500304, '2 - Nordeste', 'PB', 'Alagoa Grande', '0055'));
    l.add(new Cidades(2500403, '2 - Nordeste', 'PB', 'Alagoa Nova', '0055'));
    l.add(new Cidades(2500502, '2 - Nordeste', 'PB', 'Alagoinha', '0055'));
    l.add(new Cidades(2600609, '2 - Nordeste', 'PE', 'Alagoinha', '0055'));
    l.add(new Cidades(
        2200251, '2 - Nordeste', 'PI', 'Alagoinha Do Piaui', '0055'));
    l.add(new Cidades(2900702, '2 - Nordeste', 'BA', 'Alagoinhas', '0055'));
    l.add(new Cidades(3500758, '3 - Sudeste', 'SP', 'Alambari', '0055'));
    l.add(new Cidades(3101409, '3 - Sudeste', 'MG', 'Albertina', '0055'));
    l.add(new Cidades(2100204, '2 - Nordeste', 'MA', 'Alcantara', '0055'));
    l.add(new Cidades(2300507, '2 - Nordeste', 'CE', 'Alcantaras', '0055'));
    l.add(new Cidades(2500536, '2 - Nordeste', 'PB', 'Alcantil', '0055'));
    l.add(
        new Cidades(5000252, '5 - Centro-Oeste', 'MS', 'Alcinopolis', '0055'));
    l.add(new Cidades(2900801, '2 - Nordeste', 'BA', 'Alcobaca', '0055'));
    l.add(new Cidades(2100303, '2 - Nordeste', 'MA', 'Aldeias Altas', '0055'));
    l.add(new Cidades(4300307, '4 - Sul', 'RS', 'Alecrim', '0055'));
    l.add(new Cidades(3200201, '3 - Sudeste', 'ES', 'Alegre', '0055'));
    l.add(new Cidades(4300406, '4 - Sul', 'RS', 'Alegrete', '0055'));
    l.add(new Cidades(
        2200277, '2 - Nordeste', 'PI', 'Alegrete Do Piaui', '0055'));
    l.add(new Cidades(4300455, '4 - Sul', 'RS', 'Alegria', '0055'));
    l.add(new Cidades(3101508, '3 - Sudeste', 'MG', 'Alem Paraiba', '0055'));
    l.add(new Cidades(1500404, '1 - Norte', 'PA', 'Alenquer', '0055'));
    l.add(new Cidades(2400505, '2 - Nordeste', 'RN', 'Alexandria', '0055'));
    l.add(new Cidades(5200308, '5 - Centro-Oeste', 'GO', 'Alexania', '0055'));
    l.add(new Cidades(3101607, '3 - Sudeste', 'MG', 'Alfenas', '0055'));
    l.add(new Cidades(3200300, '3 - Sudeste', 'ES', 'Alfredo Chaves', '0055'));
    l.add(
        new Cidades(3500808, '3 - Sudeste', 'SP', 'Alfredo Marcondes', '0055'));
    l.add(new Cidades(
        3101631, '3 - Sudeste', 'MG', 'Alfredo Vasconcelos', '0055'));
    l.add(new Cidades(4200705, '4 - Sul', 'SC', 'Alfredo Wagner', '0055'));
    l.add(new Cidades(
        2500577, '2 - Nordeste', 'PB', 'Algodao De Jandaira', '0055'));
    l.add(new Cidades(2500601, '2 - Nordeste', 'PB', 'Alhandra', '0055'));
    l.add(new Cidades(2600708, '2 - Nordeste', 'PE', 'Alianca', '0055'));
    l.add(new Cidades(1700350, '1 - Norte', 'TO', 'Alianca Do To', '0055'));
    l.add(new Cidades(2900900, '2 - Nordeste', 'BA', 'Almadina', '0055'));
    l.add(new Cidades(1700400, '1 - Norte', 'TO', 'Almas', '0055'));
    l.add(new Cidades(1500503, '1 - Norte', 'PA', 'Almeirim', '0055'));
    l.add(new Cidades(3101706, '3 - Sudeste', 'MG', 'Almenara', '0055'));
    l.add(new Cidades(2400604, '2 - Nordeste', 'RN', 'Almino Afonso', '0055'));
    l.add(new Cidades(4100400, '4 - Sul', 'PR', 'Almirante Tamandare', '0055'));
    l.add(new Cidades(
        4300471, '4 - Sul', 'RS', 'Almirante Tamandare Do Sul', '0055'));
    l.add(new Cidades(5200506, '5 - Centro-Oeste', 'GO', 'Aloandia', '0055'));
    l.add(new Cidades(3101805, '3 - Sudeste', 'MG', 'Alpercata', '0055'));
    l.add(new Cidades(4300505, '4 - Sul', 'RS', 'Alpestre', '0055'));
    l.add(new Cidades(3101904, '3 - Sudeste', 'MG', 'Alpinopolis', '0055'));
    l.add(new Cidades(
        5100250, '5 - Centro-Oeste', 'MT', 'Alta Floresta', '0055'));
    l.add(new Cidades(
        1100015, '1 - Norte', 'RO', 'Alta Floresta Doeste', '0055'));
    l.add(new Cidades(3500907, '3 - Sudeste', 'SP', 'Altair', '0055'));
    l.add(new Cidades(1500602, '1 - Norte', 'PA', 'Altamira', '0055'));
    l.add(new Cidades(
        2100402, '2 - Nordeste', 'MA', 'Altamira Do Maranhao', '0055'));
    l.add(new Cidades(4100459, '4 - Sul', 'PR', 'Altamira Do Parana', '0055'));
    l.add(new Cidades(2300606, '2 - Nordeste', 'CE', 'Altaneira', '0055'));
    l.add(new Cidades(3102001, '3 - Sudeste', 'MG', 'Alterosa', '0055'));
    l.add(new Cidades(2600807, '2 - Nordeste', 'PE', 'Altinho', '0055'));
    l.add(new Cidades(3501004, '3 - Sudeste', 'SP', 'Altinopolis', '0055'));
    l.add(new Cidades(1400050, '1 - Norte', 'RR', 'Alto Alegre', '0055'));
    l.add(new Cidades(3501103, '3 - Sudeste', 'SP', 'Alto Alegre', '0055'));
    l.add(new Cidades(4300554, '4 - Sul', 'RS', 'Alto Alegre', '0055'));
    l.add(new Cidades(
        2100436, '2 - Nordeste', 'MA', 'Alto Alegre Do Maranhao', '0055'));
    l.add(new Cidades(
        2100477, '2 - Nordeste', 'MA', 'Alto Alegre Do Pindare', '0055'));
    l.add(new Cidades(
        1100379, '1 - Norte', 'RO', 'Alto Alegre Dos Parecis', '0055'));
    l.add(new Cidades(
        5100300, '5 - Centro-Oeste', 'MT', 'Alto Araguaia', '0055'));
    l.add(new Cidades(4200754, '4 - Sul', 'SC', 'Alto Bela Vista', '0055'));
    l.add(new Cidades(
        5100359, '5 - Centro-Oeste', 'MT', 'Alto Boa Vista', '0055'));
    l.add(new Cidades(3102050, '3 - Sudeste', 'MG', 'Alto Caparao', '0055'));
    l.add(new Cidades(
        2400703, '2 - Nordeste', 'RN', 'Alto Do Rodrigues', '0055'));
    l.add(new Cidades(4300570, '4 - Sul', 'RS', 'Alto Feliz', '0055'));
    l.add(
        new Cidades(5100409, '5 - Centro-Oeste', 'MT', 'Alto Garcas', '0055'));
    l.add(new Cidades(
        5200555, '5 - Centro-Oeste', 'GO', 'Alto Horizonte', '0055'));
    l.add(new Cidades(3153509, '3 - Sudeste', 'MG', 'Alto Jequitiba', '0055'));
    l.add(new Cidades(2200301, '2 - Nordeste', 'PI', 'Alto Longa', '0055'));
    l.add(new Cidades(
        5100508, '5 - Centro-Oeste', 'MT', 'Alto Paraguai', '0055'));
    l.add(new Cidades(1100403, '1 - Norte', 'RO', 'Alto Paraiso', '0055'));
    l.add(new Cidades(4128625, '4 - Sul', 'PR', 'Alto Paraiso', '0055'));
    l.add(new Cidades(
        5200605, '5 - Centro-Oeste', 'GO', 'Alto Paraiso De Goias', '0055'));
    l.add(new Cidades(4100608, '4 - Sul', 'PR', 'Alto Parana', '0055'));
    l.add(new Cidades(2100501, '2 - Nordeste', 'MA', 'Alto Parnaiba', '0055'));
    l.add(new Cidades(4100707, '4 - Sul', 'PR', 'Alto Piquiri', '0055'));
    l.add(new Cidades(3102100, '3 - Sudeste', 'MG', 'Alto Rio Doce', '0055'));
    l.add(new Cidades(3200359, '3 - Sudeste', 'ES', 'Alto Rio Novo', '0055'));
    l.add(new Cidades(2300705, '2 - Nordeste', 'CE', 'Alto Santo', '0055'));
    l.add(
        new Cidades(5100607, '5 - Centro-Oeste', 'MT', 'Alto Taquari', '0055'));
    l.add(new Cidades(4100509, '4 - Sul', 'PR', 'Altonia', '0055'));
    l.add(new Cidades(2200400, '2 - Nordeste', 'PI', 'Altos', '0055'));
    l.add(new Cidades(3501152, '3 - Sudeste', 'SP', 'Aluminio', '0055'));
    l.add(new Cidades(1300029, '1 - Norte', 'AM', 'Alvaraes', '0055'));
    l.add(new Cidades(3102209, '3 - Sudeste', 'MG', 'Alvarenga', '0055'));
    l.add(
        new Cidades(3501202, '3 - Sudeste', 'SP', 'Alvares Florence', '0055'));
    l.add(new Cidades(3501301, '3 - Sudeste', 'SP', 'Alvares Machado', '0055'));
    l.add(new Cidades(
        3501400, '3 - Sudeste', 'SP', 'Alvaro De Carvalho', '0055'));
    l.add(new Cidades(3501509, '3 - Sudeste', 'SP', 'Alvinlandia', '0055'));
    l.add(new Cidades(3102308, '3 - Sudeste', 'MG', 'Alvinopolis', '0055'));
    l.add(new Cidades(1700707, '1 - Norte', 'TO', 'Alvorada', '0055'));
    l.add(new Cidades(4300604, '4 - Sul', 'RS', 'Alvorada', '0055'));
    l.add(new Cidades(1100346, '1 - Norte', 'RO', 'Alvorada D`Oeste', '0055'));
    l.add(
        new Cidades(3102407, '3 - Sudeste', 'MG', 'Alvorada De Minas', '0055'));
    l.add(new Cidades(
        2200459, '2 - Nordeste', 'PI', 'Alvorada Do Gurgueia', '0055'));
    l.add(new Cidades(
        5200803, '5 - Centro-Oeste', 'GO', 'Alvorada Do Norte', '0055'));
    l.add(new Cidades(4100806, '4 - Sul', 'PR', 'Alvorada Do Sul', '0055'));
    l.add(new Cidades(1400027, '1 - Norte', 'RR', 'Amajari', '0055'));
    l.add(new Cidades(5000609, '5 - Centro-Oeste', 'MS', 'Amambai', '0055'));
    l.add(new Cidades(1600105, '1 - Norte', 'AP', 'Amapa', '0055'));
    l.add(new Cidades(
        2100550, '2 - Nordeste', 'MA', 'Amapa Do Maranhao', '0055'));
    l.add(new Cidades(4100905, '4 - Sul', 'PR', 'Amapora', '0055'));
    l.add(new Cidades(2600906, '2 - Nordeste', 'PE', 'Amaraji', '0055'));
    l.add(new Cidades(4300638, '4 - Sul', 'RS', 'Amaral Ferrador', '0055'));
    l.add(new Cidades(5200829, '5 - Centro-Oeste', 'GO', 'Amaralina', '0055'));
    l.add(new Cidades(2200509, '2 - Nordeste', 'PI', 'Amarante', '0055'));
    l.add(new Cidades(
        2100600, '2 - Nordeste', 'MA', 'Amarante Do Maranhao', '0055'));
    l.add(new Cidades(2901007, '2 - Nordeste', 'BA', 'Amargosa', '0055'));
    l.add(new Cidades(1300060, '1 - Norte', 'AM', 'Amatura', '0055'));
    l.add(
        new Cidades(2901106, '2 - Nordeste', 'BA', 'Amelia Rodrigues', '0055'));
    l.add(
        new Cidades(2901155, '2 - Nordeste', 'BA', 'America Dourada', '0055'));
    l.add(new Cidades(3501608, '3 - Sudeste', 'SP', 'Americana', '0055'));
    l.add(new Cidades(
        5200852, '5 - Centro-Oeste', 'GO', 'Americano Do Brasil', '0055'));
    l.add(new Cidades(
        3501707, '3 - Sudeste', 'SP', 'Americo Brasiliense', '0055'));
    l.add(
        new Cidades(3501806, '3 - Sudeste', 'SP', 'Americo De Campos', '0055'));
    l.add(new Cidades(4300646, '4 - Sul', 'RS', 'Ametista Do Sul', '0055'));
    l.add(new Cidades(2300754, '2 - Nordeste', 'CE', 'Amontada', '0055'));
    l.add(
        new Cidades(5200902, '5 - Centro-Oeste', 'GO', 'Amorinopolis', '0055'));
    l.add(new Cidades(2500734, '2 - Nordeste', 'PB', 'Amparo', '0055'));
    l.add(new Cidades(3501905, '3 - Sudeste', 'SP', 'Amparo', '0055'));
    l.add(new Cidades(
        2800100, '2 - Nordeste', 'SE', 'Amparo De Sao Francisco', '0055'));
    l.add(new Cidades(3102506, '3 - Sudeste', 'MG', 'Amparo Do Serra', '0055'));
    l.add(new Cidades(4101002, '4 - Sul', 'PR', 'Ampere', '0055'));
    l.add(new Cidades(2700201, '2 - Nordeste', 'AL', 'Anadia', '0055'));
    l.add(new Cidades(2901205, '2 - Nordeste', 'BA', 'Anage', '0055'));
    l.add(new Cidades(4101051, '4 - Sul', 'PR', 'Anahy', '0055'));
    l.add(new Cidades(1500701, '1 - Norte', 'PA', 'Anajas', '0055'));
    l.add(new Cidades(2100709, '2 - Nordeste', 'MA', 'Anajatuba', '0055'));
    l.add(new Cidades(3502002, '3 - Sudeste', 'SP', 'Analandia', '0055'));
    l.add(new Cidades(1300086, '1 - Norte', 'AM', 'Anama', '0055'));
    l.add(new Cidades(1701002, '1 - Norte', 'TO', 'Ananas', '0055'));
    l.add(new Cidades(1500800, '1 - Norte', 'PA', 'Ananindeua', '0055'));
    l.add(new Cidades(5201108, '5 - Centro-Oeste', 'GO', 'Anapolis', '0055'));
    l.add(new Cidades(1500859, '1 - Norte', 'PA', 'Anapu', '0055'));
    l.add(new Cidades(2100808, '2 - Nordeste', 'MA', 'Anapurus', '0055'));
    l.add(new Cidades(5000708, '5 - Centro-Oeste', 'MS', 'Anastacio', '0055'));
    l.add(
        new Cidades(5000807, '5 - Centro-Oeste', 'MS', 'Anaurilandia', '0055'));
    l.add(new Cidades(3200409, '3 - Sudeste', 'ES', 'Anchieta', '0055'));
    l.add(new Cidades(4200804, '4 - Sul', 'SC', 'Anchieta', '0055'));
    l.add(new Cidades(2901304, '2 - Nordeste', 'BA', 'Andarai', '0055'));
    l.add(new Cidades(4101101, '4 - Sul', 'PR', 'Andira', '0055'));
    l.add(new Cidades(2901353, '2 - Nordeste', 'BA', 'Andorinha', '0055'));
    l.add(new Cidades(3102605, '3 - Sudeste', 'MG', 'Andradas', '0055'));
    l.add(new Cidades(3502101, '3 - Sudeste', 'SP', 'Andradina', '0055'));
    l.add(new Cidades(4300661, '4 - Sul', 'RS', 'Andre Da Rocha', '0055'));
    l.add(new Cidades(3102803, '3 - Sudeste', 'MG', 'Andrelandia', '0055'));
    l.add(new Cidades(3502200, '3 - Sudeste', 'SP', 'Angatuba', '0055'));
    l.add(new Cidades(3102852, '3 - Sudeste', 'MG', 'Angelandia', '0055'));
    l.add(new Cidades(5000856, '5 - Centro-Oeste', 'MS', 'Angelica', '0055'));
    l.add(new Cidades(2601003, '2 - Nordeste', 'PE', 'Angelim', '0055'));
    l.add(new Cidades(4200903, '4 - Sul', 'SC', 'Angelina', '0055'));
    l.add(new Cidades(2901403, '2 - Nordeste', 'BA', 'Angical', '0055'));
    l.add(
        new Cidades(2200608, '2 - Nordeste', 'PI', 'Angical Do Piaui', '0055'));
    l.add(new Cidades(1701051, '1 - Norte', 'TO', 'Angico', '0055'));
    l.add(new Cidades(2400802, '2 - Nordeste', 'RN', 'Angicos', '0055'));
    l.add(new Cidades(3300100, '3 - Sudeste', 'RJ', 'Angra Dos Reis', '0055'));
    l.add(new Cidades(2901502, '2 - Nordeste', 'BA', 'Anguera', '0055'));
    l.add(new Cidades(4101150, '4 - Sul', 'PR', 'Angulo', '0055'));
    l.add(new Cidades(5201207, '5 - Centro-Oeste', 'GO', 'Anhanguera', '0055'));
    l.add(new Cidades(3502309, '3 - Sudeste', 'SP', 'Anhembi', '0055'));
    l.add(new Cidades(3502408, '3 - Sudeste', 'SP', 'Anhumas', '0055'));
    l.add(new Cidades(5201306, '5 - Centro-Oeste', 'GO', 'Anicuns', '0055'));
    l.add(
        new Cidades(2200707, '2 - Nordeste', 'PI', 'Anisio De Abreu', '0055'));
    l.add(new Cidades(4201000, '4 - Sul', 'SC', 'Anita Garibaldi', '0055'));
    l.add(new Cidades(4201109, '4 - Sul', 'SC', 'Anitapolis', '0055'));
    l.add(new Cidades(1300102, '1 - Norte', 'AM', 'Anori', '0055'));
    l.add(new Cidades(4300703, '4 - Sul', 'RS', 'Anta Gorda', '0055'));
    l.add(new Cidades(2901601, '2 - Nordeste', 'BA', 'Antas', '0055'));
    l.add(new Cidades(4101200, '4 - Sul', 'PR', 'Antonina', '0055'));
    l.add(new Cidades(
        2300804, '2 - Nordeste', 'CE', 'Antonina Do Norte', '0055'));
    l.add(
        new Cidades(2200806, '2 - Nordeste', 'PI', 'Antonio Almeida', '0055'));
    l.add(
        new Cidades(2901700, '2 - Nordeste', 'BA', 'Antonio Cardoso', '0055'));
    l.add(new Cidades(3102902, '3 - Sudeste', 'MG', 'Antonio Carlos', '0055'));
    l.add(new Cidades(4201208, '4 - Sul', 'SC', 'Antonio Carlos', '0055'));
    l.add(new Cidades(3103009, '3 - Sudeste', 'MG', 'Antonio Dias', '0055'));
    l.add(new Cidades(
        2901809, '2 - Nordeste', 'BA', 'Antonio Goncalves', '0055'));
    l.add(
        new Cidades(5000906, '5 - Centro-Oeste', 'MS', 'Antonio Joao', '0055'));
    l.add(
        new Cidades(2400901, '2 - Nordeste', 'RN', 'Antonio Martins', '0055'));
    l.add(new Cidades(4101309, '4 - Sul', 'PR', 'Antonio Olinto', '0055'));
    l.add(new Cidades(4300802, '4 - Sul', 'RS', 'Antonio Prado', '0055'));
    l.add(new Cidades(
        3103108, '3 - Sudeste', 'MG', 'Antonio Prado De Minas', '0055'));
    l.add(new Cidades(2500775, '2 - Nordeste', 'PB', 'Aparecida', '0055'));
    l.add(new Cidades(3502507, '3 - Sudeste', 'SP', 'Aparecida', '0055'));
    l.add(
        new Cidades(3502606, '3 - Sudeste', 'SP', 'Aparecida D`Oeste', '0055'));
    l.add(new Cidades(
        5201405, '5 - Centro-Oeste', 'GO', 'Aparecida De Goiania', '0055'));
    l.add(new Cidades(
        5201454, '5 - Centro-Oeste', 'GO', 'Aparecida Do Rio Doce', '0055'));
    l.add(new Cidades(
        1701101, '1 - Norte', 'TO', 'Aparecida Do Rio Negro', '0055'));
    l.add(new Cidades(
        5001003, '5 - Centro-Oeste', 'MS', 'Aparecida Do Taboado', '0055'));
    l.add(new Cidades(3300159, '3 - Sudeste', 'RJ', 'Aperibe', '0055'));
    l.add(new Cidades(3200508, '3 - Sudeste', 'ES', 'Apiaca', '0055'));
    l.add(new Cidades(5100805, '5 - Centro-Oeste', 'MT', 'Apiacas', '0055'));
    l.add(new Cidades(3502705, '3 - Sudeste', 'SP', 'Apiai', '0055'));
    l.add(new Cidades(2100832, '2 - Nordeste', 'MA', 'Apicum-Acu', '0055'));
    l.add(new Cidades(4201257, '4 - Sul', 'SC', 'Apiuna', '0055'));
    l.add(new Cidades(2401008, '2 - Nordeste', 'RN', 'Apodi', '0055'));
    l.add(new Cidades(2901908, '2 - Nordeste', 'BA', 'Apora', '0055'));
    l.add(new Cidades(5201504, '5 - Centro-Oeste', 'GO', 'Apore', '0055'));
    l.add(new Cidades(2901957, '2 - Nordeste', 'BA', 'Apuarema', '0055'));
    l.add(new Cidades(4101408, '4 - Sul', 'PR', 'Apucarana', '0055'));
    l.add(new Cidades(1300144, '1 - Norte', 'AM', 'Apui', '0055'));
    l.add(new Cidades(2300903, '2 - Nordeste', 'CE', 'Apuiares', '0055'));
    l.add(new Cidades(2800209, '2 - Nordeste', 'SE', 'Aquidaba', '0055'));
    l.add(new Cidades(5001102, '5 - Centro-Oeste', 'MS', 'Aquidauana', '0055'));
    l.add(new Cidades(2301000, '2 - Nordeste', 'CE', 'Aquiraz', '0055'));
    l.add(new Cidades(4201273, '4 - Sul', 'SC', 'Arabuta', '0055'));
    l.add(new Cidades(2500809, '2 - Nordeste', 'PB', 'Aracagi', '0055'));
    l.add(new Cidades(3103207, '3 - Sudeste', 'MG', 'Aracai', '0055'));
    l.add(new Cidades(2800308, '2 - Nordeste', 'SE', 'Aracaju', '0055'));
    l.add(new Cidades(3502754, '3 - Sudeste', 'SP', 'Aracariguama', '0055'));
    l.add(new Cidades(2902054, '2 - Nordeste', 'BA', 'Aracas', '0055'));
    l.add(new Cidades(2301109, '2 - Nordeste', 'CE', 'Aracati', '0055'));
    l.add(new Cidades(2902005, '2 - Nordeste', 'BA', 'Aracatu', '0055'));
    l.add(new Cidades(3502804, '3 - Sudeste', 'SP', 'Aracatuba', '0055'));
    l.add(new Cidades(2902104, '2 - Nordeste', 'BA', 'Araci', '0055'));
    l.add(new Cidades(3103306, '3 - Sudeste', 'MG', 'Aracitaba', '0055'));
    l.add(new Cidades(2301208, '2 - Nordeste', 'CE', 'Aracoiaba', '0055'));
    l.add(new Cidades(2601052, '2 - Nordeste', 'PE', 'Aracoiaba', '0055'));
    l.add(new Cidades(
        3502903, '3 - Sudeste', 'SP', 'Aracoiaba Da Serra', '0055'));
    l.add(new Cidades(3200607, '3 - Sudeste', 'ES', 'Aracruz', '0055'));
    l.add(new Cidades(5201603, '5 - Centro-Oeste', 'GO', 'Aracu', '0055'));
    l.add(new Cidades(3103405, '3 - Sudeste', 'MG', 'Aracuai', '0055'));
    l.add(new Cidades(5201702, '5 - Centro-Oeste', 'GO', 'Aragarcas', '0055'));
    l.add(new Cidades(5201801, '5 - Centro-Oeste', 'GO', 'Aragoiania', '0055'));
    l.add(new Cidades(1701309, '1 - Norte', 'TO', 'Aragominas', '0055'));
    l.add(new Cidades(1701903, '1 - Norte', 'TO', 'Araguacema', '0055'));
    l.add(new Cidades(1702000, '1 - Norte', 'TO', 'Araguacu', '0055'));
    l.add(new Cidades(5101001, '5 - Centro-Oeste', 'MT', 'Araguaiana', '0055'));
    l.add(new Cidades(1702109, '1 - Norte', 'TO', 'Araguaina', '0055'));
    l.add(new Cidades(5101209, '5 - Centro-Oeste', 'MT', 'Araguainha', '0055'));
    l.add(new Cidades(1702158, '1 - Norte', 'TO', 'Araguana', '0055'));
    l.add(new Cidades(2100873, '2 - Nordeste', 'MA', 'Araguana', '0055'));
    l.add(new Cidades(5202155, '5 - Centro-Oeste', 'GO', 'Araguapaz', '0055'));
    l.add(new Cidades(3103504, '3 - Sudeste', 'MG', 'Araguari', '0055'));
    l.add(new Cidades(1702208, '1 - Norte', 'TO', 'Araguatins', '0055'));
    l.add(new Cidades(2100907, '2 - Nordeste', 'MA', 'Araioses', '0055'));
    l.add(
        new Cidades(5001243, '5 - Centro-Oeste', 'MS', 'Aral Moreira', '0055'));
    l.add(new Cidades(2902203, '2 - Nordeste', 'BA', 'Aramari', '0055'));
    l.add(new Cidades(4300851, '4 - Sul', 'RS', 'Arambare', '0055'));
    l.add(new Cidades(2100956, '2 - Nordeste', 'MA', 'Arame', '0055'));
    l.add(new Cidades(3503000, '3 - Sudeste', 'SP', 'Aramina', '0055'));
    l.add(new Cidades(3503109, '3 - Sudeste', 'SP', 'Arandu', '0055'));
    l.add(new Cidades(3103603, '3 - Sudeste', 'MG', 'Arantina', '0055'));
    l.add(new Cidades(3503158, '3 - Sudeste', 'SP', 'Arapei', '0055'));
    l.add(new Cidades(2700300, '2 - Nordeste', 'AL', 'Arapiraca', '0055'));
    l.add(new Cidades(1702307, '1 - Norte', 'TO', 'Arapoema', '0055'));
    l.add(new Cidades(3103702, '3 - Sudeste', 'MG', 'Araponga', '0055'));
    l.add(new Cidades(4101507, '4 - Sul', 'PR', 'Arapongas', '0055'));
    l.add(new Cidades(3103751, '3 - Sudeste', 'MG', 'Arapora', '0055'));
    l.add(new Cidades(4101606, '4 - Sul', 'PR', 'Arapoti', '0055'));
    l.add(new Cidades(3103801, '3 - Sudeste', 'MG', 'Arapua', '0055'));
    l.add(new Cidades(4101655, '4 - Sul', 'PR', 'Arapua', '0055'));
    l.add(new Cidades(5101258, '5 - Centro-Oeste', 'MT', 'Araputanga', '0055'));
    l.add(new Cidades(4201307, '4 - Sul', 'SC', 'Araquari', '0055'));
    l.add(new Cidades(2500908, '2 - Nordeste', 'PB', 'Arara', '0055'));
    l.add(new Cidades(4201406, '4 - Sul', 'SC', 'Ararangua', '0055'));
    l.add(new Cidades(3503208, '3 - Sudeste', 'SP', 'Araraquara', '0055'));
    l.add(new Cidades(3503307, '3 - Sudeste', 'SP', 'Araras', '0055'));
    l.add(new Cidades(2301257, '2 - Nordeste', 'CE', 'Ararenda', '0055'));
    l.add(new Cidades(2101004, '2 - Nordeste', 'MA', 'Arari', '0055'));
    l.add(new Cidades(4300877, '4 - Sul', 'RS', 'Ararica', '0055'));
    l.add(new Cidades(2301307, '2 - Nordeste', 'CE', 'Araripe', '0055'));
    l.add(new Cidades(2601102, '2 - Nordeste', 'PE', 'Araripina', '0055'));
    l.add(new Cidades(3300209, '3 - Sudeste', 'RJ', 'Araruama', '0055'));
    l.add(new Cidades(2501005, '2 - Nordeste', 'PB', 'Araruna', '0055'));
    l.add(new Cidades(4101705, '4 - Sul', 'PR', 'Araruna', '0055'));
    l.add(new Cidades(2902252, '2 - Nordeste', 'BA', 'Arataca', '0055'));
    l.add(new Cidades(4300901, '4 - Sul', 'RS', 'Aratiba', '0055'));
    l.add(new Cidades(2301406, '2 - Nordeste', 'CE', 'Aratuba', '0055'));
    l.add(new Cidades(2902302, '2 - Nordeste', 'BA', 'Aratuipe', '0055'));
    l.add(new Cidades(2800407, '2 - Nordeste', 'SE', 'Araua', '0055'));
    l.add(new Cidades(4101804, '4 - Sul', 'PR', 'Araucaria', '0055'));
    l.add(new Cidades(3103900, '3 - Sudeste', 'MG', 'Araujos', '0055'));
    l.add(new Cidades(3104007, '3 - Sudeste', 'MG', 'Araxa', '0055'));
    l.add(new Cidades(3104106, '3 - Sudeste', 'MG', 'Arceburgo', '0055'));
    l.add(new Cidades(3503356, '3 - Sudeste', 'SP', 'Arco-Iris', '0055'));
    l.add(new Cidades(3104205, '3 - Sudeste', 'MG', 'Arcos', '0055'));
    l.add(new Cidades(2601201, '2 - Nordeste', 'PE', 'Arcoverde', '0055'));
    l.add(new Cidades(3104304, '3 - Sudeste', 'MG', 'Areado', '0055'));
    l.add(new Cidades(3300225, '3 - Sudeste', 'RJ', 'Areal', '0055'));
    l.add(new Cidades(3503406, '3 - Sudeste', 'SP', 'Arealva', '0055'));
    l.add(new Cidades(2501104, '2 - Nordeste', 'PB', 'Areia', '0055'));
    l.add(new Cidades(2401107, '2 - Nordeste', 'RN', 'Areia Branca', '0055'));
    l.add(new Cidades(2800506, '2 - Nordeste', 'SE', 'Areia Branca', '0055'));
    l.add(new Cidades(
        2501153, '2 - Nordeste', 'PB', 'Areia De Baraunas', '0055'));
    l.add(new Cidades(2501203, '2 - Nordeste', 'PB', 'Areial', '0055'));
    l.add(new Cidades(3503505, '3 - Sudeste', 'SP', 'Areias', '0055'));
    l.add(new Cidades(3503604, '3 - Sudeste', 'SP', 'Areiopolis', '0055'));
    l.add(new Cidades(5101308, '5 - Centro-Oeste', 'MT', 'Arenapolis', '0055'));
    l.add(new Cidades(5202353, '5 - Centro-Oeste', 'GO', 'Arenopolis', '0055'));
    l.add(new Cidades(2401206, '2 - Nordeste', 'RN', 'Ares', '0055'));
    l.add(new Cidades(3104403, '3 - Sudeste', 'MG', 'Argirita', '0055'));
    l.add(new Cidades(3104452, '3 - Sudeste', 'MG', 'Aricanduva', '0055'));
    l.add(new Cidades(3104502, '3 - Sudeste', 'MG', 'Arinos', '0055'));
    l.add(new Cidades(5101407, '5 - Centro-Oeste', 'MT', 'Aripuana', '0055'));
    l.add(new Cidades(1100023, '1 - Norte', 'RO', 'Ariquemes', '0055'));
    l.add(new Cidades(3503703, '3 - Sudeste', 'SP', 'Ariranha', '0055'));
    l.add(new Cidades(4101853, '4 - Sul', 'PR', 'Ariranha Do Ivai', '0055'));
    l.add(new Cidades(
        3300233, '3 - Sudeste', 'RJ', 'Armacao Dos Buzios', '0055'));
    l.add(new Cidades(4201505, '4 - Sul', 'SC', 'Armazem', '0055'));
    l.add(new Cidades(2301505, '2 - Nordeste', 'CE', 'Arneiroz', '0055'));
    l.add(new Cidades(2200905, '2 - Nordeste', 'PI', 'Aroazes', '0055'));
    l.add(new Cidades(2501302, '2 - Nordeste', 'PB', 'Aroeiras', '0055'));
    l.add(new Cidades(
        2200954, '2 - Nordeste', 'PI', 'Aroeiras Do Itaim', '0055'));
    l.add(new Cidades(2201002, '2 - Nordeste', 'PI', 'Arraial', '0055'));
    l.add(new Cidades(3300258, '3 - Sudeste', 'RJ', 'Arraial Do Cabo', '0055'));
    l.add(new Cidades(1702406, '1 - Norte', 'TO', 'Arraias', '0055'));
    l.add(new Cidades(4301008, '4 - Sul', 'RS', 'Arroio Do Meio', '0055'));
    l.add(new Cidades(4301073, '4 - Sul', 'RS', 'Arroio Do Padre', '0055'));
    l.add(new Cidades(4301057, '4 - Sul', 'RS', 'Arroio Do Sal', '0055'));
    l.add(new Cidades(4301206, '4 - Sul', 'RS', 'Arroio Do Tigre', '0055'));
    l.add(new Cidades(4301107, '4 - Sul', 'RS', 'Arroio Dos Ratos', '0055'));
    l.add(new Cidades(4301305, '4 - Sul', 'RS', 'Arroio Grande', '0055'));
    l.add(new Cidades(4201604, '4 - Sul', 'SC', 'Arroio Trinta', '0055'));
    l.add(new Cidades(3503802, '3 - Sudeste', 'SP', 'Artur Nogueira', '0055'));
    l.add(new Cidades(5202502, '5 - Centro-Oeste', 'GO', 'Aruana', '0055'));
    l.add(new Cidades(3503901, '3 - Sudeste', 'SP', 'Aruja', '0055'));
    l.add(new Cidades(4201653, '4 - Sul', 'SC', 'Arvoredo', '0055'));
    l.add(new Cidades(4301404, '4 - Sul', 'RS', 'Arvorezinha', '0055'));
    l.add(new Cidades(4201703, '4 - Sul', 'SC', 'Ascurra', '0055'));
    l.add(new Cidades(3503950, '3 - Sudeste', 'SP', 'Aspasia', '0055'));
    l.add(new Cidades(4101903, '4 - Sul', 'PR', 'Assai', '0055'));
    l.add(new Cidades(2301604, '2 - Nordeste', 'CE', 'Assare', '0055'));
    l.add(new Cidades(3504008, '3 - Sudeste', 'SP', 'Assis', '0055'));
    l.add(new Cidades(1200054, '1 - Norte', 'AC', 'Assis Brasil', '0055'));
    l.add(new Cidades(4102000, '4 - Sul', 'PR', 'Assis Chateaubriand', '0055'));
    l.add(new Cidades(2501351, '2 - Nordeste', 'PB', 'Assuncao', '0055'));
    l.add(new Cidades(
        2201051, '2 - Nordeste', 'PI', 'Assuncao Do Piaui', '0055'));
    l.add(new Cidades(3104601, '3 - Sudeste', 'MG', 'Astolfo Dutra', '0055'));
    l.add(new Cidades(4102109, '4 - Sul', 'PR', 'Astorga', '0055'));
    l.add(new Cidades(2700409, '2 - Nordeste', 'AL', 'Atalaia', '0055'));
    l.add(new Cidades(4102208, '4 - Sul', 'PR', 'Atalaia', '0055'));
    l.add(new Cidades(1300201, '1 - Norte', 'AM', 'Atalaia Do Norte', '0055'));
    l.add(new Cidades(4201802, '4 - Sul', 'SC', 'Atalanta', '0055'));
    l.add(new Cidades(3104700, '3 - Sudeste', 'MG', 'Ataleia', '0055'));
    l.add(new Cidades(3504107, '3 - Sudeste', 'SP', 'Atibaia', '0055'));
    l.add(new Cidades(3200706, '3 - Sudeste', 'ES', 'Atilio Vivacqua', '0055'));
    l.add(new Cidades(1702554, '1 - Norte', 'TO', 'Augustinopolis', '0055'));
    l.add(new Cidades(1500909, '1 - Norte', 'PA', 'Augusto Correa', '0055'));
    l.add(new Cidades(3104809, '3 - Sudeste', 'MG', 'Augusto De Lima', '0055'));
    l.add(new Cidades(4301503, '4 - Sul', 'RS', 'Augusto Pestana', '0055'));
    l.add(new Cidades(2401305, '2 - Nordeste', 'RN', 'Augusto Severo', '0055'));
    l.add(new Cidades(4301552, '4 - Sul', 'RS', 'Aurea', '0055'));
    l.add(new Cidades(2902401, '2 - Nordeste', 'BA', 'Aurelino Leal', '0055'));
    l.add(new Cidades(3504206, '3 - Sudeste', 'SP', 'Auriflama', '0055'));
    l.add(new Cidades(5202601, '5 - Centro-Oeste', 'GO', 'Aurilandia', '0055'));
    l.add(new Cidades(2301703, '2 - Nordeste', 'CE', 'Aurora', '0055'));
    l.add(new Cidades(4201901, '4 - Sul', 'SC', 'Aurora', '0055'));
    l.add(new Cidades(1500958, '1 - Norte', 'PA', 'Aurora Do Para', '0055'));
    l.add(new Cidades(1702703, '1 - Norte', 'TO', 'Aurora Do To', '0055'));
    l.add(new Cidades(1300300, '1 - Norte', 'AM', 'Autazes', '0055'));
    l.add(new Cidades(3504305, '3 - Sudeste', 'SP', 'Avai', '0055'));
    l.add(new Cidades(3504404, '3 - Sudeste', 'SP', 'Avanhandava', '0055'));
    l.add(new Cidades(3504503, '3 - Sudeste', 'SP', 'Avare', '0055'));
    l.add(new Cidades(1501006, '1 - Norte', 'PA', 'Aveiro', '0055'));
    l.add(new Cidades(2201101, '2 - Nordeste', 'PI', 'Avelino Lopes', '0055'));
    l.add(
        new Cidades(5202809, '5 - Centro-Oeste', 'GO', 'Avelinopolis', '0055'));
    l.add(new Cidades(2101103, '2 - Nordeste', 'MA', 'Axixa', '0055'));
    l.add(new Cidades(1702901, '1 - Norte', 'TO', 'Axixa Do To', '0055'));
    l.add(new Cidades(1703008, '1 - Norte', 'TO', 'Babaculandia', '0055'));
    l.add(new Cidades(2101202, '2 - Nordeste', 'MA', 'Bacabal', '0055'));
    l.add(new Cidades(2101251, '2 - Nordeste', 'MA', 'Bacabeira', '0055'));
    l.add(new Cidades(2101301, '2 - Nordeste', 'MA', 'Bacuri', '0055'));
    l.add(new Cidades(2101350, '2 - Nordeste', 'MA', 'Bacurituba', '0055'));
    l.add(new Cidades(3504602, '3 - Sudeste', 'SP', 'Bady Bassitt', '0055'));
    l.add(new Cidades(3104908, '3 - Sudeste', 'MG', 'Baependi', '0055'));
    l.add(new Cidades(4301602, '4 - Sul', 'RS', 'Bage', '0055'));
    l.add(new Cidades(1501105, '1 - Norte', 'PA', 'Bagre', '0055'));
    l.add(
        new Cidades(2501401, '2 - Nordeste', 'PB', 'Baia Da Traicao', '0055'));
    l.add(new Cidades(2401404, '2 - Nordeste', 'RN', 'Baia Formosa', '0055'));
    l.add(new Cidades(2902500, '2 - Nordeste', 'BA', 'Baianopolis', '0055'));
    l.add(new Cidades(1501204, '1 - Norte', 'PA', 'Baiao', '0055'));
    l.add(new Cidades(2902609, '2 - Nordeste', 'BA', 'Baixa Grande', '0055'));
    l.add(new Cidades(
        2201150, '2 - Nordeste', 'PI', 'Baixa Grande Do Ribeiro', '0055'));
    l.add(new Cidades(2301802, '2 - Nordeste', 'CE', 'Baixio', '0055'));
    l.add(new Cidades(3200805, '3 - Sudeste', 'ES', 'Baixo Guandu', '0055'));
    l.add(new Cidades(3504701, '3 - Sudeste', 'SP', 'Balbinos', '0055'));
    l.add(new Cidades(3105004, '3 - Sudeste', 'MG', 'Baldim', '0055'));
    l.add(new Cidades(5203104, '5 - Centro-Oeste', 'GO', 'Baliza', '0055'));
    l.add(new Cidades(
        4201950, '4 - Sul', 'SC', 'Balneario Arroio Do Silva', '0055'));
    l.add(new Cidades(
        4202057, '4 - Sul', 'SC', 'Balneario Barra Do Sul', '0055'));
    l.add(new Cidades(4202008, '4 - Sul', 'SC', 'Balneario Camboriu', '0055'));
    l.add(new Cidades(4202073, '4 - Sul', 'SC', 'Balneario Gaivota', '0055'));
    l.add(new Cidades(4212809, '4 - Sul', 'SC', 'Balneario Picarras', '0055'));
    l.add(new Cidades(4301636, '4 - Sul', 'RS', 'Balneario Pinhal', '0055'));
    l.add(new Cidades(4220000, '4 - Sul', 'SC', 'Balneario Rincao', '0055'));
    l.add(new Cidades(4102307, '4 - Sul', 'PR', 'Balsa Nova', '0055'));
    l.add(new Cidades(3504800, '3 - Sudeste', 'SP', 'Balsamo', '0055'));
    l.add(new Cidades(2101400, '2 - Nordeste', 'MA', 'Balsas', '0055'));
    l.add(new Cidades(3105103, '3 - Sudeste', 'MG', 'Bambui', '0055'));
    l.add(new Cidades(2301851, '2 - Nordeste', 'CE', 'Banabuiu', '0055'));
    l.add(new Cidades(3504909, '3 - Sudeste', 'SP', 'Bananal', '0055'));
    l.add(new Cidades(2501500, '2 - Nordeste', 'PB', 'Bananeiras', '0055'));
    l.add(new Cidades(3105202, '3 - Sudeste', 'MG', 'Bandeira', '0055'));
    l.add(new Cidades(3105301, '3 - Sudeste', 'MG', 'Bandeira Do Sul', '0055'));
    l.add(new Cidades(4202081, '4 - Sul', 'SC', 'Bandeirante', '0055'));
    l.add(new Cidades(4102406, '4 - Sul', 'PR', 'Bandeirantes', '0055'));
    l.add(
        new Cidades(5001508, '5 - Centro-Oeste', 'MS', 'Bandeirantes', '0055'));
    l.add(
        new Cidades(1703057, '1 - Norte', 'TO', 'Bandeirantes Do To', '0055'));
    l.add(new Cidades(1501253, '1 - Norte', 'PA', 'Bannach', '0055'));
    l.add(new Cidades(2902658, '2 - Nordeste', 'BA', 'Banzae', '0055'));
    l.add(new Cidades(4301651, '4 - Sul', 'RS', 'Barao', '0055'));
    l.add(
        new Cidades(3505005, '3 - Sudeste', 'SP', 'Barao De Antonina', '0055'));
    l.add(new Cidades(3105400, '3 - Sudeste', 'MG', 'Barao De Cocais', '0055'));
    l.add(new Cidades(4301701, '4 - Sul', 'RS', 'Barao De Cotegipe', '0055'));
    l.add(
        new Cidades(2101509, '2 - Nordeste', 'MA', 'Barao De Grajau', '0055'));
    l.add(new Cidades(
        5101605, '5 - Centro-Oeste', 'MT', 'Barao De Melgaco', '0055'));
    l.add(new Cidades(
        3105509, '3 - Sudeste', 'MG', 'Barao De Monte Alto', '0055'));
    l.add(new Cidades(4301750, '4 - Sul', 'RS', 'Barao Do Triunfo', '0055'));
    l.add(new Cidades(2401453, '2 - Nordeste', 'RN', 'Barauna', '0055'));
    l.add(new Cidades(2501534, '2 - Nordeste', 'PB', 'Barauna', '0055'));
    l.add(new Cidades(3105608, '3 - Sudeste', 'MG', 'Barbacena', '0055'));
    l.add(new Cidades(2301901, '2 - Nordeste', 'CE', 'Barbalha', '0055'));
    l.add(new Cidades(3505104, '3 - Sudeste', 'SP', 'Barbosa', '0055'));
    l.add(new Cidades(4102505, '4 - Sul', 'PR', 'Barbosa Ferraz', '0055'));
    l.add(new Cidades(1501303, '1 - Norte', 'PA', 'Barcarena', '0055'));
    l.add(new Cidades(2401503, '2 - Nordeste', 'RN', 'Barcelona', '0055'));
    l.add(new Cidades(1300409, '1 - Norte', 'AM', 'Barcelos', '0055'));
    l.add(new Cidades(3505203, '3 - Sudeste', 'SP', 'Bariri', '0055'));
    l.add(new Cidades(2902708, '2 - Nordeste', 'BA', 'Barra', '0055'));
    l.add(new Cidades(3505302, '3 - Sudeste', 'SP', 'Barra Bonita', '0055'));
    l.add(new Cidades(4202099, '4 - Sul', 'SC', 'Barra Bonita', '0055'));
    l.add(new Cidades(
        2201176, '2 - Nordeste', 'PI', 'Barra D`Alcantara', '0055'));
    l.add(
        new Cidades(2902807, '2 - Nordeste', 'BA', 'Barra Da Estiva', '0055'));
    l.add(new Cidades(
        2601300, '2 - Nordeste', 'PE', 'Barra De Guabiraba', '0055'));
    l.add(new Cidades(
        2501609, '2 - Nordeste', 'PB', 'Barra De Santa Rosa', '0055'));
    l.add(
        new Cidades(2501575, '2 - Nordeste', 'PB', 'Barra De Santana', '0055'));
    l.add(new Cidades(
        2700508, '2 - Nordeste', 'AL', 'Barra De Santo Antonio', '0055'));
    l.add(new Cidades(
        3200904, '3 - Sudeste', 'ES', 'Barra De Sao Francisco', '0055'));
    l.add(new Cidades(
        2501708, '2 - Nordeste', 'PB', 'Barra De Sao Miguel', '0055'));
    l.add(new Cidades(
        2700607, '2 - Nordeste', 'AL', 'Barra De Sao Miguel', '0055'));
    l.add(new Cidades(
        5101704, '5 - Centro-Oeste', 'MT', 'Barra Do Bugres', '0055'));
    l.add(new Cidades(3505351, '3 - Sudeste', 'SP', 'Barra Do Chapeu', '0055'));
    l.add(new Cidades(2902906, '2 - Nordeste', 'BA', 'Barra Do Choca', '0055'));
    l.add(new Cidades(2101608, '2 - Nordeste', 'MA', 'Barra Do Corda', '0055'));
    l.add(new Cidades(
        5101803, '5 - Centro-Oeste', 'MT', 'Barra Do Garcas', '0055'));
    l.add(new Cidades(4301859, '4 - Sul', 'RS', 'Barra Do Guarita', '0055'));
    l.add(new Cidades(4102703, '4 - Sul', 'PR', 'Barra Do Jacare', '0055'));
    l.add(
        new Cidades(2903003, '2 - Nordeste', 'BA', 'Barra Do Mendes', '0055'));
    l.add(new Cidades(1703073, '1 - Norte', 'TO', 'Barra Do Ouro', '0055'));
    l.add(new Cidades(3300308, '3 - Sudeste', 'RJ', 'Barra Do Pirai', '0055'));
    l.add(new Cidades(4301875, '4 - Sul', 'RS', 'Barra Do Quarai', '0055'));
    l.add(new Cidades(4301909, '4 - Sul', 'RS', 'Barra Do Ribeiro', '0055'));
    l.add(new Cidades(4301925, '4 - Sul', 'RS', 'Barra Do Rio Azul', '0055'));
    l.add(new Cidades(2903102, '2 - Nordeste', 'BA', 'Barra Do Rocha', '0055'));
    l.add(new Cidades(3505401, '3 - Sudeste', 'SP', 'Barra Do Turvo', '0055'));
    l.add(new Cidades(
        2800605, '2 - Nordeste', 'SE', 'Barra Dos Coqueiros', '0055'));
    l.add(new Cidades(4301958, '4 - Sul', 'RS', 'Barra Funda', '0055'));
    l.add(new Cidades(3105707, '3 - Sudeste', 'MG', 'Barra Longa', '0055'));
    l.add(new Cidades(3300407, '3 - Sudeste', 'RJ', 'Barra Mansa', '0055'));
    l.add(new Cidades(4202107, '4 - Sul', 'SC', 'Barra Velha', '0055'));
    l.add(new Cidades(4102604, '4 - Sul', 'PR', 'Barracao', '0055'));
    l.add(new Cidades(4301800, '4 - Sul', 'RS', 'Barracao', '0055'));
    l.add(new Cidades(2201200, '2 - Nordeste', 'PI', 'Barras', '0055'));
    l.add(new Cidades(2301950, '2 - Nordeste', 'CE', 'Barreira', '0055'));
    l.add(new Cidades(2903201, '2 - Nordeste', 'BA', 'Barreiras', '0055'));
    l.add(new Cidades(
        2201309, '2 - Nordeste', 'PI', 'Barreiras Do Piaui', '0055'));
    l.add(new Cidades(1300508, '1 - Norte', 'AM', 'Barreirinha', '0055'));
    l.add(new Cidades(2101707, '2 - Nordeste', 'MA', 'Barreirinhas', '0055'));
    l.add(new Cidades(2601409, '2 - Nordeste', 'PE', 'Barreiros', '0055'));
    l.add(new Cidades(3505500, '3 - Sudeste', 'SP', 'Barretos', '0055'));
    l.add(new Cidades(3505609, '3 - Sudeste', 'SP', 'Barrinha', '0055'));
    l.add(new Cidades(2302008, '2 - Nordeste', 'CE', 'Barro', '0055'));
    l.add(new Cidades(2903235, '2 - Nordeste', 'BA', 'Barro Alto', '0055'));
    l.add(new Cidades(5203203, '5 - Centro-Oeste', 'GO', 'Barro Alto', '0055'));
    l.add(new Cidades(2201408, '2 - Nordeste', 'PI', 'Barro Duro', '0055'));
    l.add(new Cidades(2903300, '2 - Nordeste', 'BA', 'Barro Preto', '0055'));
    l.add(new Cidades(2903276, '2 - Nordeste', 'BA', 'Barrocas', '0055'));
    l.add(new Cidades(1703107, '1 - Norte', 'TO', 'Barrolandia', '0055'));
    l.add(new Cidades(2302057, '2 - Nordeste', 'CE', 'Barroquinha', '0055'));
    l.add(new Cidades(4302006, '4 - Sul', 'RS', 'Barros Cassal', '0055'));
    l.add(new Cidades(3105905, '3 - Sudeste', 'MG', 'Barroso', '0055'));
    l.add(new Cidades(3505708, '3 - Sudeste', 'SP', 'Barueri', '0055'));
    l.add(new Cidades(3505807, '3 - Sudeste', 'SP', 'Bastos', '0055'));
    l.add(new Cidades(5001904, '5 - Centro-Oeste', 'MS', 'Bataguassu', '0055'));
    l.add(new Cidades(2201507, '2 - Nordeste', 'PI', 'Batalha', '0055'));
    l.add(new Cidades(2700706, '2 - Nordeste', 'AL', 'Batalha', '0055'));
    l.add(new Cidades(3505906, '3 - Sudeste', 'SP', 'Batatais', '0055'));
    l.add(new Cidades(5002001, '5 - Centro-Oeste', 'MS', 'Bataypora', '0055'));
    l.add(new Cidades(2302107, '2 - Nordeste', 'CE', 'Baturite', '0055'));
    l.add(new Cidades(3506003, '3 - Sudeste', 'SP', 'Bauru', '0055'));
    l.add(new Cidades(2501807, '2 - Nordeste', 'PB', 'Bayeux', '0055'));
    l.add(new Cidades(3506102, '3 - Sudeste', 'SP', 'Bebedouro', '0055'));
    l.add(new Cidades(2302206, '2 - Nordeste', 'CE', 'Beberibe', '0055'));
    l.add(new Cidades(2302305, '2 - Nordeste', 'CE', 'Bela Cruz', '0055'));
    l.add(new Cidades(5002100, '5 - Centro-Oeste', 'MS', 'Bela Vista', '0055'));
    l.add(
        new Cidades(4102752, '4 - Sul', 'PR', 'Bela Vista Da Caroba', '0055'));
    l.add(new Cidades(
        5203302, '5 - Centro-Oeste', 'GO', 'Bela Vista De Goias', '0055'));
    l.add(new Cidades(
        3106002, '3 - Sudeste', 'MG', 'Bela Vista De Minas', '0055'));
    l.add(new Cidades(
        2101772, '2 - Nordeste', 'MA', 'Bela Vista Do Maranhao', '0055'));
    l.add(
        new Cidades(4102802, '4 - Sul', 'PR', 'Bela Vista Do Paraiso', '0055'));
    l.add(new Cidades(
        2201556, '2 - Nordeste', 'PI', 'Bela Vista Do Piaui', '0055'));
    l.add(new Cidades(4202131, '4 - Sul', 'SC', 'Bela Vista Do Toldo', '0055'));
    l.add(new Cidades(2101731, '2 - Nordeste', 'MA', 'Belagua', '0055'));
    l.add(new Cidades(1501402, '1 - Norte', 'PA', 'Belem', '0055'));
    l.add(new Cidades(2501906, '2 - Nordeste', 'PB', 'Belem', '0055'));
    l.add(new Cidades(2700805, '2 - Nordeste', 'AL', 'Belem', '0055'));
    l.add(new Cidades(2601508, '2 - Nordeste', 'PE', 'Belem De Maria', '0055'));
    l.add(new Cidades(
        2601607, '2 - Nordeste', 'PE', 'Belem De Sao Francisco', '0055'));
    l.add(new Cidades(
        2502003, '2 - Nordeste', 'PB', 'Belem Do Brejo Do Cruz', '0055'));
    l.add(new Cidades(2201572, '2 - Nordeste', 'PI', 'Belem Do Piaui', '0055'));
    l.add(new Cidades(3300456, '3 - Sudeste', 'RJ', 'Belford Roxo', '0055'));
    l.add(new Cidades(3106101, '3 - Sudeste', 'MG', 'Belmiro Braga', '0055'));
    l.add(new Cidades(2903409, '2 - Nordeste', 'BA', 'Belmonte', '0055'));
    l.add(new Cidades(4202156, '4 - Sul', 'SC', 'Belmonte', '0055'));
    l.add(new Cidades(2903508, '2 - Nordeste', 'BA', 'Belo Campo', '0055'));
    l.add(new Cidades(3106200, '3 - Sudeste', 'MG', 'Belo Horizonte', '0055'));
    l.add(new Cidades(2601706, '2 - Nordeste', 'PE', 'Belo Jardim', '0055'));
    l.add(new Cidades(2700904, '2 - Nordeste', 'AL', 'Belo Monte', '0055'));
    l.add(new Cidades(3106309, '3 - Sudeste', 'MG', 'Belo Oriente', '0055'));
    l.add(new Cidades(3106408, '3 - Sudeste', 'MG', 'Belo Vale', '0055'));
    l.add(new Cidades(1501451, '1 - Norte', 'PA', 'Belterra', '0055'));
    l.add(new Cidades(2201606, '2 - Nordeste', 'PI', 'Beneditinos', '0055'));
    l.add(new Cidades(2101806, '2 - Nordeste', 'MA', 'Benedito Leite', '0055'));
    l.add(new Cidades(4202206, '4 - Sul', 'SC', 'Benedito Novo', '0055'));
    l.add(new Cidades(1501501, '1 - Norte', 'PA', 'Benevides', '0055'));
    l.add(new Cidades(1300607, '1 - Norte', 'AM', 'Benjamin Constant', '0055'));
    l.add(new Cidades(
        4302055, '4 - Sul', 'RS', 'Benjamin Constant Do Sul', '0055'));
    l.add(new Cidades(3506201, '3 - Sudeste', 'SP', 'Bento De Abreu', '0055'));
    l.add(
        new Cidades(2401602, '2 - Nordeste', 'RN', 'Bento Fernandes', '0055'));
    l.add(new Cidades(4302105, '4 - Sul', 'RS', 'Bento Goncalves', '0055'));
    l.add(new Cidades(2101905, '2 - Nordeste', 'MA', 'Bequimao', '0055'));
    l.add(new Cidades(3106507, '3 - Sudeste', 'MG', 'Berilo', '0055'));
    l.add(new Cidades(3106655, '3 - Sudeste', 'MG', 'Berizal', '0055'));
    l.add(new Cidades(
        2502052, '2 - Nordeste', 'PB', 'Bernardino Batista', '0055'));
    l.add(new Cidades(
        3506300, '3 - Sudeste', 'SP', 'Bernardino De Campos', '0055'));
    l.add(new Cidades(
        2101939, '2 - Nordeste', 'MA', 'Bernardo Do Mearim', '0055'));
    l.add(new Cidades(1703206, '1 - Norte', 'TO', 'Bernardo Sayao', '0055'));
    l.add(new Cidades(3506359, '3 - Sudeste', 'SP', 'Bertioga', '0055'));
    l.add(new Cidades(2201705, '2 - Nordeste', 'PI', 'Bertolinia', '0055'));
    l.add(new Cidades(3106606, '3 - Sudeste', 'MG', 'Bertopolis', '0055'));
    l.add(new Cidades(1300631, '1 - Norte', 'AM', 'Beruri', '0055'));
    l.add(new Cidades(2601805, '2 - Nordeste', 'PE', 'Betania', '0055'));
    l.add(
        new Cidades(2201739, '2 - Nordeste', 'PI', 'Betania Do Piaui', '0055'));
    l.add(new Cidades(3106705, '3 - Sudeste', 'MG', 'Betim', '0055'));
    l.add(new Cidades(2601904, '2 - Nordeste', 'PE', 'Bezerros', '0055'));
    l.add(new Cidades(3106804, '3 - Sudeste', 'MG', 'Bias Fortes', '0055'));
    l.add(new Cidades(3106903, '3 - Sudeste', 'MG', 'Bicas', '0055'));
    l.add(new Cidades(4202305, '4 - Sul', 'SC', 'Biguacu', '0055'));
    l.add(new Cidades(3506409, '3 - Sudeste', 'SP', 'Bilac', '0055'));
    l.add(new Cidades(3107000, '3 - Sudeste', 'MG', 'Biquinhas', '0055'));
    l.add(new Cidades(3506508, '3 - Sudeste', 'SP', 'Birigui', '0055'));
    l.add(new Cidades(3506607, '3 - Sudeste', 'SP', 'Biritiba-Mirim', '0055'));
    l.add(new Cidades(2903607, '2 - Nordeste', 'BA', 'Biritinga', '0055'));
    l.add(new Cidades(4102901, '4 - Sul', 'PR', 'Bituruna', '0055'));
    l.add(new Cidades(4202404, '4 - Sul', 'SC', 'Blumenau', '0055'));
    l.add(new Cidades(3107109, '3 - Sudeste', 'MG', 'Boa Esperanca', '0055'));
    l.add(new Cidades(3201001, '3 - Sudeste', 'ES', 'Boa Esperanca', '0055'));
    l.add(new Cidades(4103008, '4 - Sul', 'PR', 'Boa Esperanca', '0055'));
    l.add(new Cidades(
        4103024, '4 - Sul', 'PR', 'Boa Esperanca Do Iguacu', '0055'));
    l.add(new Cidades(
        3506706, '3 - Sudeste', 'SP', 'Boa Esperanca Do Sul', '0055'));
    l.add(new Cidades(2201770, '2 - Nordeste', 'PI', 'Boa Hora', '0055'));
    l.add(new Cidades(2903706, '2 - Nordeste', 'BA', 'Boa Nova', '0055'));
    l.add(new Cidades(2502102, '2 - Nordeste', 'PB', 'Boa Ventura', '0055'));
    l.add(new Cidades(
        4103040, '4 - Sul', 'PR', 'Boa Ventura De Sao Roque', '0055'));
    l.add(new Cidades(2302404, '2 - Nordeste', 'CE', 'Boa Viagem', '0055'));
    l.add(new Cidades(1400100, '1 - Norte', 'RR', 'Boa Vista', '0055'));
    l.add(new Cidades(2502151, '2 - Nordeste', 'PB', 'Boa Vista', '0055'));
    l.add(new Cidades(
        4103057, '4 - Sul', 'PR', 'Boa Vista Da Aparecida', '0055'));
    l.add(
        new Cidades(4302154, '4 - Sul', 'RS', 'Boa Vista Das Missoes', '0055'));
    l.add(new Cidades(4302204, '4 - Sul', 'RS', 'Boa Vista Do Burica', '0055'));
    l.add(
        new Cidades(4302220, '4 - Sul', 'RS', 'Boa Vista Do Cadeado', '0055'));
    l.add(new Cidades(
        2101970, '2 - Nordeste', 'MA', 'Boa Vista Do Gurupi', '0055'));
    l.add(new Cidades(4302238, '4 - Sul', 'RS', 'Boa Vista Do Incra', '0055'));
    l.add(
        new Cidades(1300680, '1 - Norte', 'AM', 'Boa Vista Do Ramos', '0055'));
    l.add(new Cidades(4302253, '4 - Sul', 'RS', 'Boa Vista Do Sul', '0055'));
    l.add(new Cidades(
        2903805, '2 - Nordeste', 'BA', 'Boa Vista Do Tupim', '0055'));
    l.add(new Cidades(2701001, '2 - Nordeste', 'AL', 'Boca Da Mata', '0055'));
    l.add(new Cidades(1300706, '1 - Norte', 'AM', 'Boca Do Ac', '0055'));
    l.add(new Cidades(2201804, '2 - Nordeste', 'PI', 'Bocaina', '0055'));
    l.add(new Cidades(3506805, '3 - Sudeste', 'SP', 'Bocaina', '0055'));
    l.add(
        new Cidades(3107208, '3 - Sudeste', 'MG', 'Bocaina De Minas', '0055'));
    l.add(new Cidades(4202438, '4 - Sul', 'SC', 'Bocaina Do Sul', '0055'));
    l.add(new Cidades(3107307, '3 - Sudeste', 'MG', 'Bocaiuva', '0055'));
    l.add(new Cidades(4103107, '4 - Sul', 'PR', 'Bocaiuva Do Sul', '0055'));
    l.add(new Cidades(2401651, '2 - Nordeste', 'RN', 'Bodo', '0055'));
    l.add(new Cidades(2602001, '2 - Nordeste', 'PE', 'Bodoco', '0055'));
    l.add(new Cidades(5002159, '5 - Centro-Oeste', 'MS', 'Bodoquena', '0055'));
    l.add(new Cidades(3506904, '3 - Sudeste', 'SP', 'Bofete', '0055'));
    l.add(new Cidades(3507001, '3 - Sudeste', 'SP', 'Boituva', '0055'));
    l.add(new Cidades(2602100, '2 - Nordeste', 'PE', 'Bom Conselho', '0055'));
    l.add(new Cidades(3107406, '3 - Sudeste', 'MG', 'Bom Despacho', '0055'));
    l.add(new Cidades(2102002, '2 - Nordeste', 'MA', 'Bom Jardim', '0055'));
    l.add(new Cidades(2602209, '2 - Nordeste', 'PE', 'Bom Jardim', '0055'));
    l.add(new Cidades(3300506, '3 - Sudeste', 'RJ', 'Bom Jardim', '0055'));
    l.add(new Cidades(4202503, '4 - Sul', 'SC', 'Bom Jardim Da Serra', '0055'));
    l.add(new Cidades(
        5203401, '5 - Centro-Oeste', 'GO', 'Bom Jardim De Goias', '0055'));
    l.add(new Cidades(
        3107505, '3 - Sudeste', 'MG', 'Bom Jardim De Minas', '0055'));
    l.add(new Cidades(2201903, '2 - Nordeste', 'PI', 'Bom Jesus', '0055'));
    l.add(new Cidades(2401701, '2 - Nordeste', 'RN', 'Bom Jesus', '0055'));
    l.add(new Cidades(2502201, '2 - Nordeste', 'PB', 'Bom Jesus', '0055'));
    l.add(new Cidades(4202537, '4 - Sul', 'SC', 'Bom Jesus', '0055'));
    l.add(new Cidades(4302303, '4 - Sul', 'RS', 'Bom Jesus', '0055'));
    l.add(new Cidades(
        2903904, '2 - Nordeste', 'BA', 'Bom Jesus Da Lapa', '0055'));
    l.add(new Cidades(
        3107604, '3 - Sudeste', 'MG', 'Bom Jesus Da Penha', '0055'));
    l.add(new Cidades(
        2903953, '2 - Nordeste', 'BA', 'Bom Jesus Da Serra', '0055'));
    l.add(new Cidades(
        2102036, '2 - Nordeste', 'MA', 'Bom Jesus Das Selvas', '0055'));
    l.add(new Cidades(
        5203500, '5 - Centro-Oeste', 'GO', 'Bom Jesus De Goias', '0055'));
    l.add(new Cidades(
        3107703, '3 - Sudeste', 'MG', 'Bom Jesus Do Amparo', '0055'));
    l.add(new Cidades(
        5101852, '5 - Centro-Oeste', 'MT', 'Bom Jesus Do Araguaia', '0055'));
    l.add(new Cidades(
        3107802, '3 - Sudeste', 'MG', 'Bom Jesus Do Galho', '0055'));
    l.add(new Cidades(
        3300605, '3 - Sudeste', 'RJ', 'Bom Jesus Do Itabapoana', '0055'));
    l.add(new Cidades(
        3201100, '3 - Sudeste', 'ES', 'Bom Jesus Do Norte', '0055'));
    l.add(new Cidades(4202578, '4 - Sul', 'SC', 'Bom Jesus Do Oeste', '0055'));
    l.add(new Cidades(4103156, '4 - Sul', 'PR', 'Bom Jesus Do Sul', '0055'));
    l.add(new Cidades(1501576, '1 - Norte', 'PA', 'Bom Jesus Do To', '0055'));
    l.add(new Cidades(1703305, '1 - Norte', 'TO', 'Bom Jesus Do To', '0055'));
    l.add(new Cidades(
        3507100, '3 - Sudeste', 'SP', 'Bom Jesus Dos Perdoes', '0055'));
    l.add(new Cidades(2102077, '2 - Nordeste', 'MA', 'Bom Lugar', '0055'));
    l.add(new Cidades(4302352, '4 - Sul', 'RS', 'Bom Principio', '0055'));
    l.add(new Cidades(
        2201919, '2 - Nordeste', 'PI', 'Bom Principio Do Piaui', '0055'));
    l.add(new Cidades(4302378, '4 - Sul', 'RS', 'Bom Progresso', '0055'));
    l.add(new Cidades(3107901, '3 - Sudeste', 'MG', 'Bom Repouso', '0055'));
    l.add(new Cidades(4202602, '4 - Sul', 'SC', 'Bom Retiro', '0055'));
    l.add(new Cidades(4302402, '4 - Sul', 'RS', 'Bom Retiro Do Sul', '0055'));
    l.add(new Cidades(2502300, '2 - Nordeste', 'PB', 'Bom Sucesso', '0055'));
    l.add(new Cidades(3108008, '3 - Sudeste', 'MG', 'Bom Sucesso', '0055'));
    l.add(new Cidades(4103206, '4 - Sul', 'PR', 'Bom Sucesso', '0055'));
    l.add(new Cidades(
        3507159, '3 - Sudeste', 'SP', 'Bom Sucesso De Itarare', '0055'));
    l.add(new Cidades(4103222, '4 - Sul', 'PR', 'Bom Sucesso Do Sul', '0055'));
    l.add(new Cidades(4202453, '4 - Sul', 'SC', 'Bombinhas', '0055'));
    l.add(new Cidades(1400159, '1 - Norte', 'RR', 'Bonfim', '0055'));
    l.add(new Cidades(3108107, '3 - Sudeste', 'MG', 'Bonfim', '0055'));
    l.add(
        new Cidades(2201929, '2 - Nordeste', 'PI', 'Bonfim Do Piaui', '0055'));
    l.add(
        new Cidades(5203559, '5 - Centro-Oeste', 'GO', 'Bonfinopolis', '0055'));
    l.add(new Cidades(
        3108206, '3 - Sudeste', 'MG', 'Bonfinopolis De Minas', '0055'));
    l.add(new Cidades(2904001, '2 - Nordeste', 'BA', 'Boninal', '0055'));
    l.add(new Cidades(1501600, '1 - Norte', 'PA', 'Bonito', '0055'));
    l.add(new Cidades(2602308, '2 - Nordeste', 'PE', 'Bonito', '0055'));
    l.add(new Cidades(2904050, '2 - Nordeste', 'BA', 'Bonito', '0055'));
    l.add(new Cidades(5002209, '5 - Centro-Oeste', 'MS', 'Bonito', '0055'));
    l.add(new Cidades(3108255, '3 - Sudeste', 'MG', 'Bonito De Minas', '0055'));
    l.add(new Cidades(
        2502409, '2 - Nordeste', 'PB', 'Bonito De Santa Fe', '0055'));
    l.add(new Cidades(5203575, '5 - Centro-Oeste', 'GO', 'Bonopolis', '0055'));
    l.add(new Cidades(2502508, '2 - Nordeste', 'PB', 'Boqueirao', '0055'));
    l.add(new Cidades(4302451, '4 - Sul', 'RS', 'Boqueirao Do Leao', '0055'));
    l.add(new Cidades(
        2201945, '2 - Nordeste', 'PI', 'Boqueirao Do Piaui', '0055'));
    l.add(new Cidades(2800670, '2 - Nordeste', 'SE', 'Boquim', '0055'));
    l.add(new Cidades(2904100, '2 - Nordeste', 'BA', 'Boquira', '0055'));
    l.add(new Cidades(3507209, '3 - Sudeste', 'SP', 'Bora', '0055'));
    l.add(new Cidades(3507308, '3 - Sudeste', 'SP', 'Boraceia', '0055'));
    l.add(new Cidades(1300805, '1 - Norte', 'AM', 'Borba', '0055'));
    l.add(new Cidades(2502706, '2 - Nordeste', 'PB', 'Borborema', '0055'));
    l.add(new Cidades(3507407, '3 - Sudeste', 'SP', 'Borborema', '0055'));
    l.add(new Cidades(3108305, '3 - Sudeste', 'MG', 'Borda Da Mata', '0055'));
    l.add(new Cidades(3507456, '3 - Sudeste', 'SP', 'Borebi', '0055'));
    l.add(new Cidades(4103305, '4 - Sul', 'PR', 'Borrazopolis', '0055'));
    l.add(new Cidades(4302501, '4 - Sul', 'RS', 'Bossoroca', '0055'));
    l.add(new Cidades(3108404, '3 - Sudeste', 'MG', 'Botelhos', '0055'));
    l.add(new Cidades(3507506, '3 - Sudeste', 'SP', 'Botucatu', '0055'));
    l.add(new Cidades(3108503, '3 - Sudeste', 'MG', 'Botumirim', '0055'));
    l.add(new Cidades(2904209, '2 - Nordeste', 'BA', 'Botupora', '0055'));
    l.add(new Cidades(4202701, '4 - Sul', 'SC', 'Botuvera', '0055'));
    l.add(new Cidades(4302584, '4 - Sul', 'RS', 'Bozano', '0055'));
    l.add(new Cidades(4202800, '4 - Sul', 'SC', 'Braco Do Norte', '0055'));
    l.add(new Cidades(4202859, '4 - Sul', 'SC', 'Braco Do Trombudo', '0055'));
    l.add(new Cidades(4302600, '4 - Sul', 'RS', 'Braga', '0055'));
    l.add(new Cidades(1501709, '1 - Norte', 'PA', 'Braganca', '0055'));
    l.add(
        new Cidades(3507605, '3 - Sudeste', 'SP', 'Braganca Paulista', '0055'));
    l.add(new Cidades(4103354, '4 - Sul', 'PR', 'Braganey', '0055'));
    l.add(new Cidades(2701100, '2 - Nordeste', 'AL', 'Branquinha', '0055'));
    l.add(new Cidades(3108701, '3 - Sudeste', 'MG', 'Bras Pires', '0055'));
    l.add(new Cidades(1501725, '1 - Norte', 'PA', 'Brasil Novo', '0055'));
    l.add(
        new Cidades(5002308, '5 - Centro-Oeste', 'MS', 'Brasilandia', '0055'));
    l.add(new Cidades(
        3108552, '3 - Sudeste', 'MG', 'Brasilandia De Minas', '0055'));
    l.add(new Cidades(4103370, '4 - Sul', 'PR', 'Brasilandia Do Sul', '0055'));
    l.add(new Cidades(1703602, '1 - Norte', 'TO', 'Brasilandia Do To', '0055'));
    l.add(new Cidades(1200104, '1 - Norte', 'AC', 'Brasileia', '0055'));
    l.add(new Cidades(2201960, '2 - Nordeste', 'PI', 'Brasileira', '0055'));
    l.add(new Cidades(5300108, '5 - Centro-Oeste', 'DF', 'Brasilia', '0055'));
    l.add(
        new Cidades(3108602, '3 - Sudeste', 'MG', 'Brasilia De Minas', '0055'));
    l.add(new Cidades(5101902, '5 - Centro-Oeste', 'MT', 'Brasnorte', '0055'));
    l.add(new Cidades(3108909, '3 - Sudeste', 'MG', 'Brasopolis', '0055'));
    l.add(new Cidades(3507704, '3 - Sudeste', 'SP', 'Brauna', '0055'));
    l.add(new Cidades(3108800, '3 - Sudeste', 'MG', 'Braunas', '0055'));
    l.add(
        new Cidades(5203609, '5 - Centro-Oeste', 'GO', 'Brazabrantes', '0055'));
    l.add(new Cidades(2602407, '2 - Nordeste', 'PE', 'Brejao', '0055'));
    l.add(new Cidades(3201159, '3 - Sudeste', 'ES', 'Brejetuba', '0055'));
    l.add(new Cidades(2401800, '2 - Nordeste', 'RN', 'Brejinho', '0055'));
    l.add(new Cidades(2602506, '2 - Nordeste', 'PE', 'Brejinho', '0055'));
    l.add(
        new Cidades(1703701, '1 - Norte', 'TO', 'Brejinho De Nazare', '0055'));
    l.add(new Cidades(2102101, '2 - Nordeste', 'MA', 'Brejo', '0055'));
    l.add(new Cidades(3507753, '3 - Sudeste', 'SP', 'Brejo Alegre', '0055'));
    l.add(new Cidades(
        2602605, '2 - Nordeste', 'PE', 'Brejo Da Madre De Deus', '0055'));
    l.add(new Cidades(2102150, '2 - Nordeste', 'MA', 'Brejo De Areia', '0055'));
    l.add(new Cidades(2502805, '2 - Nordeste', 'PB', 'Brejo Do Cruz', '0055'));
    l.add(new Cidades(2201988, '2 - Nordeste', 'PI', 'Brejo Do Piaui', '0055'));
    l.add(
        new Cidades(2502904, '2 - Nordeste', 'PB', 'Brejo Dos Santos', '0055'));
    l.add(new Cidades(2800704, '2 - Nordeste', 'SE', 'Brejo Grande', '0055'));
    l.add(new Cidades(
        1501758, '1 - Norte', 'PA', 'Brejo Grande Do Araguaia', '0055'));
    l.add(new Cidades(2302503, '2 - Nordeste', 'CE', 'Brejo Santo', '0055'));
    l.add(new Cidades(2904308, '2 - Nordeste', 'BA', 'Brejoes', '0055'));
    l.add(new Cidades(2904407, '2 - Nordeste', 'BA', 'Brejolandia', '0055'));
    l.add(new Cidades(1501782, '1 - Norte', 'PA', 'Breu Branco', '0055'));
    l.add(new Cidades(1501808, '1 - Norte', 'PA', 'Breves', '0055'));
    l.add(new Cidades(5203807, '5 - Centro-Oeste', 'GO', 'Britania', '0055'));
    l.add(new Cidades(4302659, '4 - Sul', 'RS', 'Brochier', '0055'));
    l.add(new Cidades(3507803, '3 - Sudeste', 'SP', 'Brodowski', '0055'));
    l.add(new Cidades(3507902, '3 - Sudeste', 'SP', 'Brotas', '0055'));
    l.add(new Cidades(
        2904506, '2 - Nordeste', 'BA', 'Brotas De Macaubas', '0055'));
    l.add(new Cidades(3109006, '3 - Sudeste', 'MG', 'Brumadinho', '0055'));
    l.add(new Cidades(2904605, '2 - Nordeste', 'BA', 'Brumado', '0055'));
    l.add(new Cidades(4202875, '4 - Sul', 'SC', 'Brunopolis', '0055'));
    l.add(new Cidades(4202909, '4 - Sul', 'SC', 'Brusque', '0055'));
    l.add(new Cidades(3109105, '3 - Sudeste', 'MG', 'Bueno Brandao', '0055'));
    l.add(new Cidades(3109204, '3 - Sudeste', 'MG', 'Buenopolis', '0055'));
    l.add(new Cidades(2602704, '2 - Nordeste', 'PE', 'Buenos Aires', '0055'));
    l.add(new Cidades(2904704, '2 - Nordeste', 'BA', 'Buerarema', '0055'));
    l.add(new Cidades(3109253, '3 - Sudeste', 'MG', 'Bugre', '0055'));
    l.add(new Cidades(2602803, '2 - Nordeste', 'PE', 'Buique', '0055'));
    l.add(new Cidades(1200138, '1 - Norte', 'AC', 'Bujari', '0055'));
    l.add(new Cidades(1501907, '1 - Norte', 'PA', 'Bujaru', '0055'));
    l.add(new Cidades(3508009, '3 - Sudeste', 'SP', 'Buri', '0055'));
    l.add(new Cidades(3508108, '3 - Sudeste', 'SP', 'Buritama', '0055'));
    l.add(new Cidades(2102200, '2 - Nordeste', 'MA', 'Buriti', '0055'));
    l.add(new Cidades(
        5203906, '5 - Centro-Oeste', 'GO', 'Buriti Alegre', '0055'));
    l.add(new Cidades(2102309, '2 - Nordeste', 'MA', 'Buriti Bravo', '0055'));
    l.add(new Cidades(
        5203939, '5 - Centro-Oeste', 'GO', 'Buriti De Goias', '0055'));
    l.add(new Cidades(1703800, '1 - Norte', 'TO', 'Buriti Do To', '0055'));
    l.add(
        new Cidades(2202000, '2 - Nordeste', 'PI', 'Buriti Dos Lopes', '0055'));
    l.add(new Cidades(
        2202026, '2 - Nordeste', 'PI', 'Buriti Dos Montes', '0055'));
    l.add(new Cidades(2102325, '2 - Nordeste', 'MA', 'Buriticupu', '0055'));
    l.add(new Cidades(
        5203962, '5 - Centro-Oeste', 'GO', 'Buritinopolis', '0055'));
    l.add(new Cidades(2904753, '2 - Nordeste', 'BA', 'Buritirama', '0055'));
    l.add(new Cidades(2102358, '2 - Nordeste', 'MA', 'Buritirana', '0055'));
    l.add(new Cidades(1100452, '1 - Norte', 'RO', 'Buritis', '0055'));
    l.add(new Cidades(3109303, '3 - Sudeste', 'MG', 'Buritis', '0055'));
    l.add(new Cidades(3508207, '3 - Sudeste', 'SP', 'Buritizal', '0055'));
    l.add(new Cidades(3109402, '3 - Sudeste', 'MG', 'Buritizeiro', '0055'));
    l.add(new Cidades(4302709, '4 - Sul', 'RS', 'Butia', '0055'));
    l.add(new Cidades(1300839, '1 - Norte', 'AM', 'Caapiranga', '0055'));
    l.add(new Cidades(2503001, '2 - Nordeste', 'PB', 'Caapora', '0055'));
    l.add(new Cidades(5002407, '5 - Centro-Oeste', 'MS', 'Caarapo', '0055'));
    l.add(new Cidades(2904803, '2 - Nordeste', 'BA', 'Caatiba', '0055'));
    l.add(new Cidades(2503100, '2 - Nordeste', 'PB', 'Cabaceiras', '0055'));
    l.add(new Cidades(
        2904852, '2 - Nordeste', 'BA', 'Cabaceiras Do Paraguacu', '0055'));
    l.add(
        new Cidades(3109451, '3 - Sudeste', 'MG', 'Cabeceira Grande', '0055'));
    l.add(new Cidades(5204003, '5 - Centro-Oeste', 'GO', 'Cabeceiras', '0055'));
    l.add(new Cidades(
        2202059, '2 - Nordeste', 'PI', 'Cabeceiras Do Piaui', '0055'));
    l.add(new Cidades(2503209, '2 - Nordeste', 'PB', 'Cabedelo', '0055'));
    l.add(new Cidades(1100031, '1 - Norte', 'RO', 'Cabixi', '0055'));
    l.add(new Cidades(
        2602902, '2 - Nordeste', 'PE', 'Cabo De Santo Agostinho', '0055'));
    l.add(new Cidades(3300704, '3 - Sudeste', 'RJ', 'Cabo Frio', '0055'));
    l.add(new Cidades(3109501, '3 - Sudeste', 'MG', 'Cabo Verde', '0055'));
    l.add(
        new Cidades(3508306, '3 - Sudeste', 'SP', 'Cabralia Paulista', '0055'));
    l.add(new Cidades(3508405, '3 - Sudeste', 'SP', 'Cabreuva', '0055'));
    l.add(new Cidades(2603009, '2 - Nordeste', 'PE', 'Cabrobo', '0055'));
    l.add(new Cidades(4203006, '4 - Sul', 'SC', 'Cacador', '0055'));
    l.add(new Cidades(3508504, '3 - Sudeste', 'SP', 'Cacapava', '0055'));
    l.add(new Cidades(4302808, '4 - Sul', 'RS', 'Cacapava Do Sul', '0055'));
    l.add(new Cidades(1100601, '1 - Norte', 'RO', 'Cacaulandia', '0055'));
    l.add(new Cidades(4302907, '4 - Sul', 'RS', 'Cacequi', '0055'));
    l.add(new Cidades(5102504, '5 - Centro-Oeste', 'MT', 'Caceres', '0055'));
    l.add(new Cidades(2904902, '2 - Nordeste', 'BA', 'Cachoeira', '0055'));
    l.add(new Cidades(
        5204102, '5 - Centro-Oeste', 'GO', 'Cachoeira Alta', '0055'));
    l.add(new Cidades(
        3109600, '3 - Sudeste', 'MG', 'Cachoeira Da Prata', '0055'));
    l.add(new Cidades(
        5204201, '5 - Centro-Oeste', 'GO', 'Cachoeira De Goias', '0055'));
    l.add(new Cidades(
        3109709, '3 - Sudeste', 'MG', 'Cachoeira De Minas', '0055'));
    l.add(new Cidades(
        3102704, '3 - Sudeste', 'MG', 'Cachoeira De Pajeu', '0055'));
    l.add(
        new Cidades(1502004, '1 - Norte', 'PA', 'Cachoeira Do Arari', '0055'));
    l.add(
        new Cidades(1501956, '1 - Norte', 'PA', 'Cachoeira Do Piria', '0055'));
    l.add(new Cidades(4303004, '4 - Sul', 'RS', 'Cachoeira Do Sul', '0055'));
    l.add(new Cidades(
        2503308, '2 - Nordeste', 'PB', 'Cachoeira Dos Indios', '0055'));
    l.add(
        new Cidades(3109808, '3 - Sudeste', 'MG', 'Cachoeira Dourada', '0055'));
    l.add(new Cidades(
        5204250, '5 - Centro-Oeste', 'GO', 'Cachoeira Dourada', '0055'));
    l.add(
        new Cidades(2102374, '2 - Nordeste', 'MA', 'Cachoeira Grande', '0055'));
    l.add(new Cidades(
        3508603, '3 - Sudeste', 'SP', 'Cachoeira Paulista', '0055'));
    l.add(new Cidades(
        3300803, '3 - Sudeste', 'RJ', 'Cachoeiras De Macacu', '0055'));
    l.add(new Cidades(1703826, '1 - Norte', 'TO', 'Cachoeirinha', '0055'));
    l.add(new Cidades(2603108, '2 - Nordeste', 'PE', 'Cachoeirinha', '0055'));
    l.add(new Cidades(4303103, '4 - Sul', 'RS', 'Cachoeirinha', '0055'));
    l.add(new Cidades(
        3201209, '3 - Sudeste', 'ES', 'Cachoeiro De Itapemirim', '0055'));
    l.add(
        new Cidades(2503407, '2 - Nordeste', 'PB', 'Cacimba De Areia', '0055'));
    l.add(new Cidades(
        2503506, '2 - Nordeste', 'PB', 'Cacimba De Dentro', '0055'));
    l.add(new Cidades(2503555, '2 - Nordeste', 'PB', 'Cacimbas', '0055'));
    l.add(new Cidades(2701209, '2 - Nordeste', 'AL', 'Cacimbinhas', '0055'));
    l.add(new Cidades(4303202, '4 - Sul', 'RS', 'Cacique Doble', '0055'));
    l.add(new Cidades(1100049, '1 - Norte', 'RO', 'Cacoal', '0055'));
    l.add(new Cidades(3508702, '3 - Sudeste', 'SP', 'Caconde', '0055'));
    l.add(new Cidades(5204300, '5 - Centro-Oeste', 'GO', 'Cacu', '0055'));
    l.add(new Cidades(2905008, '2 - Nordeste', 'BA', 'Cacule', '0055'));
    l.add(new Cidades(2905107, '2 - Nordeste', 'BA', 'Caem', '0055'));
    l.add(new Cidades(3109907, '3 - Sudeste', 'MG', 'Caetanopolis', '0055'));
    l.add(new Cidades(2905156, '2 - Nordeste', 'BA', 'Caetanos', '0055'));
    l.add(new Cidades(3110004, '3 - Sudeste', 'MG', 'Caete', '0055'));
    l.add(new Cidades(2603207, '2 - Nordeste', 'PE', 'Caetes', '0055'));
    l.add(new Cidades(2905206, '2 - Nordeste', 'BA', 'Caetite', '0055'));
    l.add(new Cidades(2905305, '2 - Nordeste', 'BA', 'Cafarnaum', '0055'));
    l.add(new Cidades(4103404, '4 - Sul', 'PR', 'Cafeara', '0055'));
    l.add(new Cidades(3508801, '3 - Sudeste', 'SP', 'Cafelandia', '0055'));
    l.add(new Cidades(4103453, '4 - Sul', 'PR', 'Cafelandia', '0055'));
    l.add(new Cidades(4103479, '4 - Sul', 'PR', 'Cafezal Do Sul', '0055'));
    l.add(new Cidades(3508900, '3 - Sudeste', 'SP', 'Caiabu', '0055'));
    l.add(new Cidades(3110103, '3 - Sudeste', 'MG', 'Caiana', '0055'));
    l.add(new Cidades(5204409, '5 - Centro-Oeste', 'GO', 'Caiaponia', '0055'));
    l.add(new Cidades(4303301, '4 - Sul', 'RS', 'Caibate', '0055'));
    l.add(new Cidades(4203105, '4 - Sul', 'SC', 'Caibi', '0055'));
    l.add(new Cidades(2503605, '2 - Nordeste', 'PB', 'Caicara', '0055'));
    l.add(new Cidades(4303400, '4 - Sul', 'RS', 'Caicara', '0055'));
    l.add(
        new Cidades(2401859, '2 - Nordeste', 'RN', 'Caicara Do Norte', '0055'));
    l.add(new Cidades(
        2401909, '2 - Nordeste', 'RN', 'Caicara Do Rio Do Vento', '0055'));
    l.add(new Cidades(2402006, '2 - Nordeste', 'RN', 'Caico', '0055'));
    l.add(new Cidades(3509007, '3 - Sudeste', 'SP', 'Caieiras', '0055'));
    l.add(new Cidades(2905404, '2 - Nordeste', 'BA', 'Cairu', '0055'));
    l.add(new Cidades(3509106, '3 - Sudeste', 'SP', 'Caiua', '0055'));
    l.add(new Cidades(3509205, '3 - Sudeste', 'SP', 'Cajamar', '0055'));
    l.add(new Cidades(2102408, '2 - Nordeste', 'MA', 'Cajapio', '0055'));
    l.add(new Cidades(2102507, '2 - Nordeste', 'MA', 'Cajari', '0055'));
    l.add(new Cidades(3509254, '3 - Sudeste', 'SP', 'Cajati', '0055'));
    l.add(new Cidades(2503704, '2 - Nordeste', 'PB', 'Cajazeiras', '0055'));
    l.add(new Cidades(
        2202075, '2 - Nordeste', 'PI', 'Cajazeiras Do Piaui', '0055'));
    l.add(new Cidades(2503753, '2 - Nordeste', 'PB', 'Cajazeirinhas', '0055'));
    l.add(new Cidades(3509304, '3 - Sudeste', 'SP', 'Cajobi', '0055'));
    l.add(new Cidades(2701308, '2 - Nordeste', 'AL', 'Cajueiro', '0055'));
    l.add(new Cidades(
        2202083, '2 - Nordeste', 'PI', 'Cajueiro Da Praia', '0055'));
    l.add(new Cidades(3110202, '3 - Sudeste', 'MG', 'Cajuri', '0055'));
    l.add(new Cidades(3509403, '3 - Sudeste', 'SP', 'Cajuru', '0055'));
    l.add(new Cidades(2603306, '2 - Nordeste', 'PE', 'Calcado', '0055'));
    l.add(new Cidades(1600204, '1 - Norte', 'AP', 'Calcoene', '0055'));
    l.add(new Cidades(3110301, '3 - Sudeste', 'MG', 'Caldas', '0055'));
    l.add(new Cidades(2503803, '2 - Nordeste', 'PB', 'Caldas Brandao', '0055'));
    l.add(
        new Cidades(5204508, '5 - Centro-Oeste', 'GO', 'Caldas Novas', '0055'));
    l.add(new Cidades(5204557, '5 - Centro-Oeste', 'GO', 'Caldazinha', '0055'));
    l.add(
        new Cidades(2905503, '2 - Nordeste', 'BA', 'Caldeirao Grande', '0055'));
    l.add(new Cidades(
        2202091, '2 - Nordeste', 'PI', 'Caldeirao Grande Do Piaui', '0055'));
    l.add(new Cidades(4103503, '4 - Sul', 'PR', 'California', '0055'));
    l.add(new Cidades(4203154, '4 - Sul', 'SC', 'Calmon', '0055'));
    l.add(new Cidades(2603405, '2 - Nordeste', 'PE', 'Calumbi', '0055'));
    l.add(new Cidades(2905602, '2 - Nordeste', 'BA', 'Camacan', '0055'));
    l.add(new Cidades(2905701, '2 - Nordeste', 'BA', 'Camacari', '0055'));
    l.add(new Cidades(3110400, '3 - Sudeste', 'MG', 'Camacho', '0055'));
    l.add(new Cidades(2503902, '2 - Nordeste', 'PB', 'Camalau', '0055'));
    l.add(new Cidades(2905800, '2 - Nordeste', 'BA', 'Camamu', '0055'));
    l.add(new Cidades(3110509, '3 - Sudeste', 'MG', 'Camanducaia', '0055'));
    l.add(new Cidades(5002605, '5 - Centro-Oeste', 'MS', 'Camapua', '0055'));
    l.add(new Cidades(4303509, '4 - Sul', 'RS', 'Camaqua', '0055'));
    l.add(new Cidades(2603454, '2 - Nordeste', 'PE', 'Camaragibe', '0055'));
    l.add(new Cidades(4303558, '4 - Sul', 'RS', 'Camargo', '0055'));
    l.add(new Cidades(4103602, '4 - Sul', 'PR', 'Cambara', '0055'));
    l.add(new Cidades(4303608, '4 - Sul', 'RS', 'Cambara Do Sul', '0055'));
    l.add(new Cidades(4103701, '4 - Sul', 'PR', 'Cambe', '0055'));
    l.add(new Cidades(4103800, '4 - Sul', 'PR', 'Cambira', '0055'));
    l.add(new Cidades(4203204, '4 - Sul', 'SC', 'Camboriu', '0055'));
    l.add(new Cidades(3300902, '3 - Sudeste', 'RJ', 'Cambuci', '0055'));
    l.add(new Cidades(3110608, '3 - Sudeste', 'MG', 'Cambui', '0055'));
    l.add(new Cidades(3110707, '3 - Sudeste', 'MG', 'Cambuquira', '0055'));
    l.add(new Cidades(1502103, '1 - Norte', 'PA', 'Cameta', '0055'));
    l.add(new Cidades(2302602, '2 - Nordeste', 'CE', 'Camocim', '0055'));
    l.add(new Cidades(
        2603504, '2 - Nordeste', 'PE', 'Camocim De Sao Felix', '0055'));
    l.add(new Cidades(3110806, '3 - Sudeste', 'MG', 'Campanario', '0055'));
    l.add(new Cidades(3110905, '3 - Sudeste', 'MG', 'Campanha', '0055'));
    l.add(new Cidades(2701357, '2 - Nordeste', 'AL', 'Campestre', '0055'));
    l.add(new Cidades(3111002, '3 - Sudeste', 'MG', 'Campestre', '0055'));
    l.add(new Cidades(4303673, '4 - Sul', 'RS', 'Campestre Da Serra', '0055'));
    l.add(new Cidades(
        5204607, '5 - Centro-Oeste', 'GO', 'Campestre De Goias', '0055'));
    l.add(new Cidades(
        2102556, '2 - Nordeste', 'MA', 'Campestre Do Maranhao', '0055'));
    l.add(new Cidades(4103909, '4 - Sul', 'PR', 'Campina Da Lagoa', '0055'));
    l.add(new Cidades(4303707, '4 - Sul', 'RS', 'Campina Das Missoes', '0055'));
    l.add(new Cidades(
        3509452, '3 - Sudeste', 'SP', 'Campina Do Monte Alegre', '0055'));
    l.add(new Cidades(4103958, '4 - Sul', 'PR', 'Campina Do Simao', '0055'));
    l.add(new Cidades(2504009, '2 - Nordeste', 'PB', 'Campina Grande', '0055'));
    l.add(
        new Cidades(4104006, '4 - Sul', 'PR', 'Campina Grande Do Sul', '0055'));
    l.add(new Cidades(3111101, '3 - Sudeste', 'MG', 'Campina Verde', '0055'));
    l.add(new Cidades(5204656, '5 - Centro-Oeste', 'GO', 'Campinacu', '0055'));
    l.add(
        new Cidades(5102603, '5 - Centro-Oeste', 'MT', 'Campinapolis', '0055'));
    l.add(new Cidades(3509502, '3 - Sudeste', 'SP', 'Campinas', '0055'));
    l.add(new Cidades(
        2202109, '2 - Nordeste', 'PI', 'Campinas Do Piaui', '0055'));
    l.add(new Cidades(4303806, '4 - Sul', 'RS', 'Campinas Do Sul', '0055'));
    l.add(new Cidades(5204706, '5 - Centro-Oeste', 'GO', 'Campinorte', '0055'));
    l.add(new Cidades(2701407, '2 - Nordeste', 'AL', 'Campo Alegre', '0055'));
    l.add(new Cidades(4203303, '4 - Sul', 'SC', 'Campo Alegre', '0055'));
    l.add(new Cidades(
        5204805, '5 - Centro-Oeste', 'GO', 'Campo Alegre De Goias', '0055'));
    l.add(new Cidades(
        2905909, '2 - Nordeste', 'BA', 'Campo Alegre De Lourdes', '0055'));
    l.add(new Cidades(
        2202117, '2 - Nordeste', 'PI', 'Campo Alegre Do Fidalgo', '0055'));
    l.add(new Cidades(3111150, '3 - Sudeste', 'MG', 'Campo Azul', '0055'));
    l.add(new Cidades(3111200, '3 - Sudeste', 'MG', 'Campo Belo', '0055'));
    l.add(new Cidades(4203402, '4 - Sul', 'SC', 'Campo Belo Do Sul', '0055'));
    l.add(new Cidades(4303905, '4 - Sul', 'RS', 'Campo Bom', '0055'));
    l.add(new Cidades(4104055, '4 - Sul', 'PR', 'Campo Bonito', '0055'));
    l.add(
        new Cidades(2516409, '2 - Nordeste', 'PB', 'Campo De Santana', '0055'));
    l.add(new Cidades(2801009, '2 - Nordeste', 'SE', 'Campo Do Brito', '0055'));
    l.add(new Cidades(3111309, '3 - Sudeste', 'MG', 'Campo Do Meio', '0055'));
    l.add(new Cidades(4104105, '4 - Sul', 'PR', 'Campo Do Tenente', '0055'));
    l.add(new Cidades(4203501, '4 - Sul', 'SC', 'Campo Ere', '0055'));
    l.add(new Cidades(3111408, '3 - Sudeste', 'MG', 'Campo Florido', '0055'));
    l.add(new Cidades(2906006, '2 - Nordeste', 'BA', 'Campo Formoso', '0055'));
    l.add(new Cidades(2701506, '2 - Nordeste', 'AL', 'Campo Grande', '0055'));
    l.add(
        new Cidades(5002704, '5 - Centro-Oeste', 'MS', 'Campo Grande', '0055'));
    l.add(new Cidades(
        2202133, '2 - Nordeste', 'PI', 'Campo Grande Do Piaui', '0055'));
    l.add(new Cidades(4104204, '4 - Sul', 'PR', 'Campo Largo', '0055'));
    l.add(new Cidades(
        2202174, '2 - Nordeste', 'PI', 'Campo Largo Do Piaui', '0055'));
    l.add(new Cidades(
        5204854, '5 - Centro-Oeste', 'GO', 'Campo Limpo De Goias', '0055'));
    l.add(new Cidades(
        3509601, '3 - Sudeste', 'SP', 'Campo Limpo Paulista', '0055'));
    l.add(new Cidades(4104253, '4 - Sul', 'PR', 'Campo Magro', '0055'));
    l.add(new Cidades(2202208, '2 - Nordeste', 'PI', 'Campo Maior', '0055'));
    l.add(new Cidades(4104303, '4 - Sul', 'PR', 'Campo Mourao', '0055'));
    l.add(new Cidades(4304002, '4 - Sul', 'RS', 'Campo Novo', '0055'));
    l.add(new Cidades(
        1100700, '1 - Norte', 'RO', 'Campo Novo De Rondonia', '0055'));
    l.add(new Cidades(
        5102637, '5 - Centro-Oeste', 'MT', 'Campo Novo Do Parecis', '0055'));
    l.add(new Cidades(2402105, '2 - Nordeste', 'RN', 'Campo Redondo', '0055'));
    l.add(
        new Cidades(5102678, '5 - Centro-Oeste', 'MT', 'Campo Verde', '0055'));
    l.add(new Cidades(3111507, '3 - Sudeste', 'MG', 'Campos Altos', '0055'));
    l.add(
        new Cidades(5204904, '5 - Centro-Oeste', 'GO', 'Campos Belos', '0055'));
    l.add(new Cidades(4304101, '4 - Sul', 'RS', 'Campos Borges', '0055'));
    l.add(new Cidades(
        5102686, '5 - Centro-Oeste', 'MT', 'Campos De Julio', '0055'));
    l.add(
        new Cidades(3509700, '3 - Sudeste', 'SP', 'Campos Do Jordao', '0055'));
    l.add(new Cidades(
        3301009, '3 - Sudeste', 'RJ', 'Campos Dos Goytacazes', '0055'));
    l.add(new Cidades(3111606, '3 - Sudeste', 'MG', 'Campos Gerais', '0055'));
    l.add(new Cidades(1703842, '1 - Norte', 'TO', 'Campos Lindos', '0055'));
    l.add(new Cidades(4203600, '4 - Sul', 'SC', 'Campos Novos', '0055'));
    l.add(new Cidades(
        3509809, '3 - Sudeste', 'SP', 'Campos Novos Paulista', '0055'));
    l.add(new Cidades(2302701, '2 - Nordeste', 'CE', 'Campos Sales', '0055'));
    l.add(new Cidades(
        5204953, '5 - Centro-Oeste', 'GO', 'Campos Verdes', '0055'));
    l.add(new Cidades(2603603, '2 - Nordeste', 'PE', 'Camutanga', '0055'));
    l.add(new Cidades(3111903, '3 - Sudeste', 'MG', 'Cana Verde', '0055'));
    l.add(new Cidades(3111705, '3 - Sudeste', 'MG', 'Canaa', '0055'));
    l.add(new Cidades(1502152, '1 - Norte', 'PA', 'Canaa Dos Carajas', '0055'));
    l.add(new Cidades(
        5102694, '5 - Centro-Oeste', 'MT', 'Canabrava Do Norte', '0055'));
    l.add(new Cidades(3509908, '3 - Sudeste', 'SP', 'Cananeia', '0055'));
    l.add(new Cidades(2701605, '2 - Nordeste', 'AL', 'Canapi', '0055'));
    l.add(new Cidades(2906105, '2 - Nordeste', 'BA', 'Canapolis', '0055'));
    l.add(new Cidades(3111804, '3 - Sudeste', 'MG', 'Canapolis', '0055'));
    l.add(new Cidades(2906204, '2 - Nordeste', 'BA', 'Canarana', '0055'));
    l.add(new Cidades(5102702, '5 - Centro-Oeste', 'MT', 'Canarana', '0055'));
    l.add(new Cidades(3509957, '3 - Sudeste', 'SP', 'Canas', '0055'));
    l.add(new Cidades(2202251, '2 - Nordeste', 'PI', 'Canavieira', '0055'));
    l.add(new Cidades(2906303, '2 - Nordeste', 'BA', 'Canavieiras', '0055'));
    l.add(new Cidades(2906402, '2 - Nordeste', 'BA', 'Candeal', '0055'));
    l.add(new Cidades(2906501, '2 - Nordeste', 'BA', 'Candeias', '0055'));
    l.add(new Cidades(3112000, '3 - Sudeste', 'MG', 'Candeias', '0055'));
    l.add(
        new Cidades(1100809, '1 - Norte', 'RO', 'Candeias Do Jamari', '0055'));
    l.add(new Cidades(4304200, '4 - Sul', 'RS', 'Candelaria', '0055'));
    l.add(new Cidades(2906600, '2 - Nordeste', 'BA', 'Candiba', '0055'));
    l.add(new Cidades(4104402, '4 - Sul', 'PR', 'Candido De Abreu', '0055'));
    l.add(new Cidades(4304309, '4 - Sul', 'RS', 'Candido Godoi', '0055'));
    l.add(new Cidades(2102606, '2 - Nordeste', 'MA', 'Candido Mendes', '0055'));
    l.add(new Cidades(3510005, '3 - Sudeste', 'SP', 'Candido Mota', '0055'));
    l.add(
        new Cidades(3510104, '3 - Sudeste', 'SP', 'Candido Rodrigues', '0055'));
    l.add(new Cidades(2906709, '2 - Nordeste', 'BA', 'Candido Sales', '0055'));
    l.add(new Cidades(4304358, '4 - Sul', 'RS', 'Candiota', '0055'));
    l.add(new Cidades(4104428, '4 - Sul', 'PR', 'Candoi', '0055'));
    l.add(new Cidades(4304408, '4 - Sul', 'RS', 'Canela', '0055'));
    l.add(new Cidades(4203709, '4 - Sul', 'SC', 'Canelinha', '0055'));
    l.add(new Cidades(2402204, '2 - Nordeste', 'RN', 'Canguaretama', '0055'));
    l.add(new Cidades(4304507, '4 - Sul', 'RS', 'Cangucu', '0055'));
    l.add(new Cidades(2801108, '2 - Nordeste', 'SE', 'Canhoba', '0055'));
    l.add(new Cidades(2603702, '2 - Nordeste', 'PE', 'Canhotinho', '0055'));
    l.add(new Cidades(2302800, '2 - Nordeste', 'CE', 'Caninde', '0055'));
    l.add(new Cidades(
        2801207, '2 - Nordeste', 'SE', 'Caninde De Sao Francisco', '0055'));
    l.add(new Cidades(3510153, '3 - Sudeste', 'SP', 'Canitar', '0055'));
    l.add(new Cidades(4304606, '4 - Sul', 'RS', 'Canoas', '0055'));
    l.add(new Cidades(4203808, '4 - Sul', 'SC', 'Canoinhas', '0055'));
    l.add(new Cidades(2906808, '2 - Nordeste', 'BA', 'Cansancao', '0055'));
    l.add(new Cidades(1400175, '1 - Norte', 'RR', 'Canta', '0055'));
    l.add(new Cidades(3112059, '3 - Sudeste', 'MG', 'Cantagalo', '0055'));
    l.add(new Cidades(3301108, '3 - Sudeste', 'RJ', 'Cantagalo', '0055'));
    l.add(new Cidades(4104451, '4 - Sul', 'PR', 'Cantagalo', '0055'));
    l.add(new Cidades(2102705, '2 - Nordeste', 'MA', 'Cantanhede', '0055'));
    l.add(
        new Cidades(2202307, '2 - Nordeste', 'PI', 'Canto Do Buriti', '0055'));
    l.add(new Cidades(2906824, '2 - Nordeste', 'BA', 'Canudos', '0055'));
    l.add(new Cidades(4304614, '4 - Sul', 'RS', 'Canudos Do Vale', '0055'));
    l.add(new Cidades(1300904, '1 - Norte', 'AM', 'Canutama', '0055'));
    l.add(new Cidades(1502202, '1 - Norte', 'PA', 'Capanema', '0055'));
    l.add(new Cidades(4104501, '4 - Sul', 'PR', 'Capanema', '0055'));
    l.add(new Cidades(4203253, '4 - Sul', 'SC', 'Capao Alto', '0055'));
    l.add(new Cidades(3510203, '3 - Sudeste', 'SP', 'Capao Bonito', '0055'));
    l.add(new Cidades(4304622, '4 - Sul', 'RS', 'Capao Bonito Do Sul', '0055'));
    l.add(new Cidades(4304630, '4 - Sul', 'RS', 'Capao Da Canoa', '0055'));
    l.add(new Cidades(4304655, '4 - Sul', 'RS', 'Capao Do Cipo', '0055'));
    l.add(new Cidades(4304663, '4 - Sul', 'RS', 'Capao Do Leao', '0055'));
    l.add(new Cidades(3112109, '3 - Sudeste', 'MG', 'Caparao', '0055'));
    l.add(new Cidades(2701704, '2 - Nordeste', 'AL', 'Capela', '0055'));
    l.add(new Cidades(2801306, '2 - Nordeste', 'SE', 'Capela', '0055'));
    l.add(new Cidades(4304689, '4 - Sul', 'RS', 'Capela De Santana', '0055'));
    l.add(new Cidades(3510302, '3 - Sudeste', 'SP', 'Capela Do Alto', '0055'));
    l.add(new Cidades(
        2906857, '2 - Nordeste', 'BA', 'Capela Do Alto Alegre', '0055'));
    l.add(new Cidades(3112208, '3 - Sudeste', 'MG', 'Capela Nova', '0055'));
    l.add(new Cidades(3112307, '3 - Sudeste', 'MG', 'Capelinha', '0055'));
    l.add(new Cidades(3112406, '3 - Sudeste', 'MG', 'Capetinga', '0055'));
    l.add(new Cidades(2504033, '2 - Nordeste', 'PB', 'Capim', '0055'));
    l.add(new Cidades(3112505, '3 - Sudeste', 'MG', 'Capim Branco', '0055'));
    l.add(new Cidades(2906873, '2 - Nordeste', 'BA', 'Capim Grosso', '0055'));
    l.add(new Cidades(3112604, '3 - Sudeste', 'MG', 'Capinopolis', '0055'));
    l.add(new Cidades(4203907, '4 - Sul', 'SC', 'Capinzal', '0055'));
    l.add(new Cidades(
        2102754, '2 - Nordeste', 'MA', 'Capinzal Do Norte', '0055'));
    l.add(new Cidades(2302909, '2 - Nordeste', 'CE', 'Capistrano', '0055'));
    l.add(new Cidades(4304697, '4 - Sul', 'RS', 'Capitao', '0055'));
    l.add(new Cidades(3112653, '3 - Sudeste', 'MG', 'Capitao Andrade', '0055'));
    l.add(new Cidades(
        2202406, '2 - Nordeste', 'PI', 'Capitao De Campos', '0055'));
    l.add(new Cidades(3112703, '3 - Sudeste', 'MG', 'Capitao Eneas', '0055'));
    l.add(new Cidades(
        2202455, '2 - Nordeste', 'PI', 'Capitao Gervasio Oliveira', '0055'));
    l.add(new Cidades(
        4104600, '4 - Sul', 'PR', 'Capitao Leonidas Marques', '0055'));
    l.add(new Cidades(1502301, '1 - Norte', 'PA', 'Capitao Poco', '0055'));
    l.add(new Cidades(3112802, '3 - Sudeste', 'MG', 'Capitolio', '0055'));
    l.add(new Cidades(3510401, '3 - Sudeste', 'SP', 'Capivari', '0055'));
    l.add(new Cidades(4203956, '4 - Sul', 'SC', 'Capivari De Baixo', '0055'));
    l.add(new Cidades(4304671, '4 - Sul', 'RS', 'Capivari Do Sul', '0055'));
    l.add(new Cidades(1200179, '1 - Norte', 'AC', 'Capixaba', '0055'));
    l.add(new Cidades(2603801, '2 - Nordeste', 'PE', 'Capoeiras', '0055'));
    l.add(new Cidades(3112901, '3 - Sudeste', 'MG', 'Caputira', '0055'));
    l.add(new Cidades(4304713, '4 - Sul', 'RS', 'Caraa', '0055'));
    l.add(new Cidades(1400209, '1 - Norte', 'RR', 'Caracarai', '0055'));
    l.add(new Cidades(2202505, '2 - Nordeste', 'PI', 'Caracol', '0055'));
    l.add(new Cidades(5002803, '5 - Centro-Oeste', 'MS', 'Caracol', '0055'));
    l.add(new Cidades(3510500, '3 - Sudeste', 'SP', 'Caraguatatuba', '0055'));
    l.add(new Cidades(3113008, '3 - Sudeste', 'MG', 'Carai', '0055'));
    l.add(new Cidades(2906899, '2 - Nordeste', 'BA', 'Caraibas', '0055'));
    l.add(new Cidades(4104659, '4 - Sul', 'PR', 'Carambei', '0055'));
    l.add(new Cidades(3113107, '3 - Sudeste', 'MG', 'Caranaiba', '0055'));
    l.add(new Cidades(3113206, '3 - Sudeste', 'MG', 'Carandai', '0055'));
    l.add(new Cidades(3113305, '3 - Sudeste', 'MG', 'Carangola', '0055'));
    l.add(new Cidades(3300936, '3 - Sudeste', 'RJ', 'Carapebus', '0055'));
    l.add(new Cidades(3510609, '3 - Sudeste', 'SP', 'Carapicuiba', '0055'));
    l.add(new Cidades(3113404, '3 - Sudeste', 'MG', 'Caratinga', '0055'));
    l.add(new Cidades(1301001, '1 - Norte', 'AM', 'Carauari', '0055'));
    l.add(new Cidades(2402303, '2 - Nordeste', 'RN', 'Caraubas', '0055'));
    l.add(new Cidades(2504074, '2 - Nordeste', 'PB', 'Caraubas', '0055'));
    l.add(new Cidades(
        2202539, '2 - Nordeste', 'PI', 'Caraubas Do Piaui', '0055'));
    l.add(new Cidades(2906907, '2 - Nordeste', 'BA', 'Caravelas', '0055'));
    l.add(new Cidades(4304705, '4 - Sul', 'RS', 'Carazinho', '0055'));
    l.add(new Cidades(3113503, '3 - Sudeste', 'MG', 'Carbonita', '0055'));
    l.add(
        new Cidades(2907004, '2 - Nordeste', 'BA', 'Cardeal Da Silva', '0055'));
    l.add(new Cidades(3510708, '3 - Sudeste', 'SP', 'Cardoso', '0055'));
    l.add(new Cidades(3301157, '3 - Sudeste', 'RJ', 'Cardoso Moreira', '0055'));
    l.add(new Cidades(3113602, '3 - Sudeste', 'MG', 'Careacu', '0055'));
    l.add(new Cidades(1301100, '1 - Norte', 'AM', 'Careiro', '0055'));
    l.add(new Cidades(1301159, '1 - Norte', 'AM', 'Careiro Da Varzea', '0055'));
    l.add(new Cidades(3201308, '3 - Sudeste', 'ES', 'Cariacica', '0055'));
    l.add(new Cidades(2303006, '2 - Nordeste', 'CE', 'Caridade', '0055'));
    l.add(new Cidades(
        2202554, '2 - Nordeste', 'PI', 'Caridade Do Piaui', '0055'));
    l.add(new Cidades(2907103, '2 - Nordeste', 'BA', 'Carinhanha', '0055'));
    l.add(new Cidades(2801405, '2 - Nordeste', 'SE', 'Carira', '0055'));
    l.add(new Cidades(2303105, '2 - Nordeste', 'CE', 'Carire', '0055'));
    l.add(new Cidades(1703867, '1 - Norte', 'TO', 'Cariri Do To', '0055'));
    l.add(new Cidades(2303204, '2 - Nordeste', 'CE', 'Caririacu', '0055'));
    l.add(new Cidades(2303303, '2 - Nordeste', 'CE', 'Carius', '0055'));
    l.add(new Cidades(5102793, '5 - Centro-Oeste', 'MT', 'Carlinda', '0055'));
    l.add(new Cidades(4104709, '4 - Sul', 'PR', 'Carlopolis', '0055'));
    l.add(new Cidades(4304804, '4 - Sul', 'RS', 'Carlos Barbosa', '0055'));
    l.add(new Cidades(3113701, '3 - Sudeste', 'MG', 'Carlos Chagas', '0055'));
    l.add(new Cidades(4304853, '4 - Sul', 'RS', 'Carlos Gomes', '0055'));
    l.add(new Cidades(3113800, '3 - Sudeste', 'MG', 'Carmesia', '0055'));
    l.add(new Cidades(3301207, '3 - Sudeste', 'RJ', 'Carmo', '0055'));
    l.add(new Cidades(
        3113909, '3 - Sudeste', 'MG', 'Carmo Da Cachoeira', '0055'));
    l.add(new Cidades(3114006, '3 - Sudeste', 'MG', 'Carmo Da Mata', '0055'));
    l.add(new Cidades(3114105, '3 - Sudeste', 'MG', 'Carmo De Minas', '0055'));
    l.add(new Cidades(3114204, '3 - Sudeste', 'MG', 'Carmo Do Cajuru', '0055'));
    l.add(new Cidades(
        3114303, '3 - Sudeste', 'MG', 'Carmo Do Paranaiba', '0055'));
    l.add(new Cidades(
        3114402, '3 - Sudeste', 'MG', 'Carmo Do Rio Claro', '0055'));
    l.add(new Cidades(
        5205000, '5 - Centro-Oeste', 'GO', 'Carmo Do Rio Verde', '0055'));
    l.add(new Cidades(1703883, '1 - Norte', 'TO', 'Carmolandia', '0055'));
    l.add(new Cidades(2801504, '2 - Nordeste', 'SE', 'Carmopolis', '0055'));
    l.add(new Cidades(
        3114501, '3 - Sudeste', 'MG', 'Carmopolis De Minas', '0055'));
    l.add(new Cidades(2603900, '2 - Nordeste', 'PE', 'Carnaiba', '0055'));
    l.add(new Cidades(
        2402402, '2 - Nordeste', 'RN', 'Carnauba Dos Dantas', '0055'));
    l.add(new Cidades(2402501, '2 - Nordeste', 'RN', 'Carnaubais', '0055'));
    l.add(new Cidades(2303402, '2 - Nordeste', 'CE', 'Carnaubal', '0055'));
    l.add(new Cidades(
        2603926, '2 - Nordeste', 'PE', 'Carnaubeira Da Penha', '0055'));
    l.add(new Cidades(3114550, '3 - Sudeste', 'MG', 'Carneirinho', '0055'));
    l.add(new Cidades(2701803, '2 - Nordeste', 'AL', 'Carneiros', '0055'));
    l.add(new Cidades(1400233, '1 - Norte', 'RR', 'Caroebe', '0055'));
    l.add(new Cidades(2102804, '2 - Nordeste', 'MA', 'Carolina', '0055'));
    l.add(new Cidades(2604007, '2 - Nordeste', 'PE', 'Carpina', '0055'));
    l.add(new Cidades(3114600, '3 - Sudeste', 'MG', 'Carrancas', '0055'));
    l.add(new Cidades(2504108, '2 - Nordeste', 'PB', 'Carrapateira', '0055'));
    l.add(new Cidades(1703891, '1 - Norte', 'TO', 'Carrasco Bonito', '0055'));
    l.add(new Cidades(2604106, '2 - Nordeste', 'PE', 'Caruaru', '0055'));
    l.add(new Cidades(2102903, '2 - Nordeste', 'MA', 'Carutapera', '0055'));
    l.add(new Cidades(3114709, '3 - Sudeste', 'MG', 'Carvalhopolis', '0055'));
    l.add(new Cidades(3114808, '3 - Sudeste', 'MG', 'Carvalhos', '0055'));
    l.add(new Cidades(3510807, '3 - Sudeste', 'SP', 'Casa Branca', '0055'));
    l.add(new Cidades(3114907, '3 - Sudeste', 'MG', 'Casa Grande', '0055'));
    l.add(new Cidades(2907202, '2 - Nordeste', 'BA', 'Casa Nova', '0055'));
    l.add(new Cidades(4304903, '4 - Sul', 'RS', 'Casca', '0055'));
    l.add(new Cidades(3115003, '3 - Sudeste', 'MG', 'Cascalho Rico', '0055'));
    l.add(new Cidades(2303501, '2 - Nordeste', 'CE', 'Cascavel', '0055'));
    l.add(new Cidades(4104808, '4 - Sul', 'PR', 'Cascavel', '0055'));
    l.add(new Cidades(1703909, '1 - Norte', 'TO', 'Caseara', '0055'));
    l.add(new Cidades(4304952, '4 - Sul', 'RS', 'Caseiros', '0055'));
    l.add(
        new Cidades(3301306, '3 - Sudeste', 'RJ', 'Casimiro De Abreu', '0055'));
    l.add(new Cidades(2604155, '2 - Nordeste', 'PE', 'Casinhas', '0055'));
    l.add(new Cidades(2504157, '2 - Nordeste', 'PB', 'Casserengue', '0055'));
    l.add(new Cidades(3115102, '3 - Sudeste', 'MG', 'Cassia', '0055'));
    l.add(new Cidades(
        3510906, '3 - Sudeste', 'SP', 'Cassia Dos Coqueiros', '0055'));
    l.add(
        new Cidades(5002902, '5 - Centro-Oeste', 'MS', 'Cassilandia', '0055'));
    l.add(new Cidades(1502400, '1 - Norte', 'PA', 'Castanhal', '0055'));
    l.add(
        new Cidades(5102850, '5 - Centro-Oeste', 'MT', 'Castanheira', '0055'));
    l.add(new Cidades(1100908, '1 - Norte', 'RO', 'Castanheiras', '0055'));
    l.add(
        new Cidades(5205059, '5 - Centro-Oeste', 'GO', 'Castelandia', '0055'));
    l.add(new Cidades(3201407, '3 - Sudeste', 'ES', 'Castelo', '0055'));
    l.add(
        new Cidades(2202604, '2 - Nordeste', 'PI', 'Castelo Do Piaui', '0055'));
    l.add(new Cidades(3511003, '3 - Sudeste', 'SP', 'Castilho', '0055'));
    l.add(new Cidades(4104907, '4 - Sul', 'PR', 'Castro', '0055'));
    l.add(new Cidades(2907301, '2 - Nordeste', 'BA', 'Castro Alves', '0055'));
    l.add(new Cidades(3115300, '3 - Sudeste', 'MG', 'Cataguases', '0055'));
    l.add(new Cidades(5205109, '5 - Centro-Oeste', 'GO', 'Catalao', '0055'));
    l.add(new Cidades(3511102, '3 - Sudeste', 'SP', 'Catanduva', '0055'));
    l.add(new Cidades(4105003, '4 - Sul', 'PR', 'Catanduvas', '0055'));
    l.add(new Cidades(4204004, '4 - Sul', 'SC', 'Catanduvas', '0055'));
    l.add(new Cidades(2303600, '2 - Nordeste', 'CE', 'Catarina', '0055'));
    l.add(new Cidades(3115359, '3 - Sudeste', 'MG', 'Catas Altas', '0055'));
    l.add(new Cidades(
        3115409, '3 - Sudeste', 'MG', 'Catas Altas Da Noruega', '0055'));
    l.add(new Cidades(2604205, '2 - Nordeste', 'PE', 'Catende', '0055'));
    l.add(new Cidades(3511201, '3 - Sudeste', 'SP', 'Catigua', '0055'));
    l.add(new Cidades(2504207, '2 - Nordeste', 'PB', 'Catingueira', '0055'));
    l.add(new Cidades(2907400, '2 - Nordeste', 'BA', 'Catolandia', '0055'));
    l.add(
        new Cidades(2504306, '2 - Nordeste', 'PB', 'Catole Do Rocha', '0055'));
    l.add(new Cidades(2907509, '2 - Nordeste', 'BA', 'Catu', '0055'));
    l.add(new Cidades(4305009, '4 - Sul', 'RS', 'Catuipe', '0055'));
    l.add(new Cidades(3115458, '3 - Sudeste', 'MG', 'Catuji', '0055'));
    l.add(new Cidades(2303659, '2 - Nordeste', 'CE', 'Catunda', '0055'));
    l.add(new Cidades(5205208, '5 - Centro-Oeste', 'GO', 'Caturai', '0055'));
    l.add(new Cidades(2907558, '2 - Nordeste', 'BA', 'Caturama', '0055'));
    l.add(new Cidades(2504355, '2 - Nordeste', 'PB', 'Caturite', '0055'));
    l.add(new Cidades(3115474, '3 - Sudeste', 'MG', 'Catuti', '0055'));
    l.add(new Cidades(2303709, '2 - Nordeste', 'CE', 'Caucaia', '0055'));
    l.add(new Cidades(5205307, '5 - Centro-Oeste', 'GO', 'Cavalcante', '0055'));
    l.add(new Cidades(3115508, '3 - Sudeste', 'MG', 'Caxambu', '0055'));
    l.add(new Cidades(4204103, '4 - Sul', 'SC', 'Caxambu Do Sul', '0055'));
    l.add(new Cidades(2103000, '2 - Nordeste', 'MA', 'Caxias', '0055'));
    l.add(new Cidades(4305108, '4 - Sul', 'RS', 'Caxias Do Sul', '0055'));
    l.add(new Cidades(2202653, '2 - Nordeste', 'PI', 'Caxingo', '0055'));
    l.add(new Cidades(2402600, '2 - Nordeste', 'RN', 'Ceara-Mirim', '0055'));
    l.add(new Cidades(2103109, '2 - Nordeste', 'MA', 'Cedral', '0055'));
    l.add(new Cidades(3511300, '3 - Sudeste', 'SP', 'Cedral', '0055'));
    l.add(new Cidades(2303808, '2 - Nordeste', 'CE', 'Cedro', '0055'));
    l.add(new Cidades(2604304, '2 - Nordeste', 'PE', 'Cedro', '0055'));
    l.add(new Cidades(
        2801603, '2 - Nordeste', 'SE', 'Cedro De Sao Joao', '0055'));
    l.add(new Cidades(3115607, '3 - Sudeste', 'MG', 'Cedro Do Abaete', '0055'));
    l.add(new Cidades(4204152, '4 - Sul', 'SC', 'Celso Ramos', '0055'));
    l.add(new Cidades(1704105, '1 - Norte', 'TO', 'Centenario', '0055'));
    l.add(new Cidades(4305116, '4 - Sul', 'RS', 'Centenario', '0055'));
    l.add(new Cidades(4105102, '4 - Sul', 'PR', 'Centenario Do Sul', '0055'));
    l.add(new Cidades(2907608, '2 - Nordeste', 'BA', 'Central', '0055'));
    l.add(
        new Cidades(3115706, '3 - Sudeste', 'MG', 'Central De Minas', '0055'));
    l.add(new Cidades(
        2103125, '2 - Nordeste', 'MA', 'Central Do Maranhao', '0055'));
    l.add(new Cidades(3115805, '3 - Sudeste', 'MG', 'Centralina', '0055'));
    l.add(new Cidades(
        2103158, '2 - Nordeste', 'MA', 'Centro Do Guilherme', '0055'));
    l.add(new Cidades(
        2103174, '2 - Nordeste', 'MA', 'Centro Novo Do Maranhao', '0055'));
    l.add(new Cidades(1100056, '1 - Norte', 'RO', 'Cerejeiras', '0055'));
    l.add(new Cidades(5205406, '5 - Centro-Oeste', 'GO', 'Ceres', '0055'));
    l.add(new Cidades(3511409, '3 - Sudeste', 'SP', 'Cerqueira Cesar', '0055'));
    l.add(new Cidades(3511508, '3 - Sudeste', 'SP', 'Cerquilho', '0055'));
    l.add(new Cidades(4305124, '4 - Sul', 'RS', 'Cerrito', '0055'));
    l.add(new Cidades(4105201, '4 - Sul', 'PR', 'Cerro Azul', '0055'));
    l.add(new Cidades(4305132, '4 - Sul', 'RS', 'Cerro Branco', '0055'));
    l.add(new Cidades(2402709, '2 - Nordeste', 'RN', 'Cerro Cora', '0055'));
    l.add(new Cidades(4305157, '4 - Sul', 'RS', 'Cerro Grande', '0055'));
    l.add(new Cidades(4305173, '4 - Sul', 'RS', 'Cerro Grande Do Sul', '0055'));
    l.add(new Cidades(4305207, '4 - Sul', 'RS', 'Cerro Largo', '0055'));
    l.add(new Cidades(4204178, '4 - Sul', 'SC', 'Cerro Negro', '0055'));
    l.add(new Cidades(3511607, '3 - Sudeste', 'SP', 'Cesario Lange', '0055'));
    l.add(new Cidades(4105300, '4 - Sul', 'PR', 'Ceu Azul', '0055'));
    l.add(new Cidades(5205455, '5 - Centro-Oeste', 'GO', 'Cezarina', '0055'));
    l.add(new Cidades(2604403, '2 - Nordeste', 'PE', 'Cha De Alegria', '0055'));
    l.add(new Cidades(2604502, '2 - Nordeste', 'PE', 'Cha Grande', '0055'));
    l.add(new Cidades(2701902, '2 - Nordeste', 'AL', 'Cha Preta', '0055'));
    l.add(new Cidades(3115904, '3 - Sudeste', 'MG', 'Chacara', '0055'));
    l.add(new Cidades(3116001, '3 - Sudeste', 'MG', 'Chale', '0055'));
    l.add(new Cidades(4305306, '4 - Sul', 'RS', 'Chapada', '0055'));
    l.add(new Cidades(
        1705102, '1 - Norte', 'TO', 'Chapada Da Natividade', '0055'));
    l.add(new Cidades(1704600, '1 - Norte', 'TO', 'Chapada De Areia', '0055'));
    l.add(
        new Cidades(3116100, '3 - Sudeste', 'MG', 'Chapada Do Norte', '0055'));
    l.add(new Cidades(
        5103007, '5 - Centro-Oeste', 'MT', 'Chapada Dos Guimaraes', '0055'));
    l.add(new Cidades(3116159, '3 - Sudeste', 'MG', 'Chapada Gaucha', '0055'));
    l.add(new Cidades(
        5205471, '5 - Centro-Oeste', 'GO', 'Chapadao Do Ceu', '0055'));
    l.add(new Cidades(4204194, '4 - Sul', 'SC', 'Chapadao Do Lageado', '0055'));
    l.add(new Cidades(
        5002951, '5 - Centro-Oeste', 'MS', 'Chapadao Do Sul', '0055'));
    l.add(new Cidades(2103208, '2 - Nordeste', 'MA', 'Chapadinha', '0055'));
    l.add(new Cidades(4204202, '4 - Sul', 'SC', 'Chapeco', '0055'));
    l.add(new Cidades(3511706, '3 - Sudeste', 'SP', 'Charqueada', '0055'));
    l.add(new Cidades(4305355, '4 - Sul', 'RS', 'Charqueadas', '0055'));
    l.add(new Cidades(4305371, '4 - Sul', 'RS', 'Charrua', '0055'));
    l.add(new Cidades(2303907, '2 - Nordeste', 'CE', 'Chaval', '0055'));
    l.add(new Cidades(3557204, '3 - Sudeste', 'SP', 'Chavantes', '0055'));
    l.add(new Cidades(1502509, '1 - Norte', 'PA', 'Chaves', '0055'));
    l.add(new Cidades(3116209, '3 - Sudeste', 'MG', 'Chiador', '0055'));
    l.add(new Cidades(4305405, '4 - Sul', 'RS', 'Chiapetta', '0055'));
    l.add(new Cidades(4105409, '4 - Sul', 'PR', 'Chopinzinho', '0055'));
    l.add(new Cidades(2303931, '2 - Nordeste', 'CE', 'Choro', '0055'));
    l.add(new Cidades(2303956, '2 - Nordeste', 'CE', 'Chorozinho', '0055'));
    l.add(new Cidades(2907707, '2 - Nordeste', 'BA', 'Chorrocho', '0055'));
    l.add(new Cidades(4305439, '4 - Sul', 'RS', 'Chui', '0055'));
    l.add(new Cidades(1100924, '1 - Norte', 'RO', 'Chupinguaia', '0055'));
    l.add(new Cidades(4305447, '4 - Sul', 'RS', 'Chuvisca', '0055'));
    l.add(new Cidades(4105508, '4 - Sul', 'PR', 'Cianorte', '0055'));
    l.add(new Cidades(2907806, '2 - Nordeste', 'BA', 'Cicero Dantas', '0055'));
    l.add(new Cidades(4105607, '4 - Sul', 'PR', 'Cidade Gaucha', '0055'));
    l.add(new Cidades(
        5205497, '5 - Centro-Oeste', 'GO', 'Cidade Ocidental', '0055'));
    l.add(new Cidades(2103257, '2 - Nordeste', 'MA', 'Cidelandia', '0055'));
    l.add(new Cidades(4305454, '4 - Sul', 'RS', 'Cidreira', '0055'));
    l.add(new Cidades(2907905, '2 - Nordeste', 'BA', 'Cipo', '0055'));
    l.add(new Cidades(3116308, '3 - Sudeste', 'MG', 'Cipotanea', '0055'));
    l.add(new Cidades(4305504, '4 - Sul', 'RS', 'Ciriaco', '0055'));
    l.add(new Cidades(3116407, '3 - Sudeste', 'MG', 'Claraval', '0055'));
    l.add(
        new Cidades(3116506, '3 - Sudeste', 'MG', 'Claro Dos Pocoes', '0055'));
    l.add(new Cidades(5103056, '5 - Centro-Oeste', 'MT', 'Claudia', '0055'));
    l.add(new Cidades(3116605, '3 - Sudeste', 'MG', 'Claudio', '0055'));
    l.add(new Cidades(3511904, '3 - Sudeste', 'SP', 'Clementina', '0055'));
    l.add(new Cidades(4105706, '4 - Sul', 'PR', 'Clevelandia', '0055'));
    l.add(new Cidades(2908002, '2 - Nordeste', 'BA', 'Coaraci', '0055'));
    l.add(new Cidades(1301209, '1 - Norte', 'AM', 'Coari', '0055'));
    l.add(new Cidades(2202703, '2 - Nordeste', 'PI', 'Cocal', '0055'));
    l.add(new Cidades(2202711, '2 - Nordeste', 'PI', 'Cocal De Telha', '0055'));
    l.add(new Cidades(4204251, '4 - Sul', 'SC', 'Cocal Do Sul', '0055'));
    l.add(
        new Cidades(2202729, '2 - Nordeste', 'PI', 'Cocal Dos Alves', '0055'));
    l.add(new Cidades(5103106, '5 - Centro-Oeste', 'MT', 'Cocalinho', '0055'));
    l.add(new Cidades(
        5205513, '5 - Centro-Oeste', 'GO', 'Cocalzinho De Goias', '0055'));
    l.add(new Cidades(2908101, '2 - Nordeste', 'BA', 'Cocos', '0055'));
    l.add(new Cidades(1301308, '1 - Norte', 'AM', 'Codajas', '0055'));
    l.add(new Cidades(2103307, '2 - Nordeste', 'MA', 'Codo', '0055'));
    l.add(new Cidades(2103406, '2 - Nordeste', 'MA', 'Coelho Neto', '0055'));
    l.add(new Cidades(3116704, '3 - Sudeste', 'MG', 'Coimbra', '0055'));
    l.add(new Cidades(2702009, '2 - Nordeste', 'AL', 'Coite Do Noia', '0055'));
    l.add(new Cidades(2202737, '2 - Nordeste', 'PI', 'Coivaras', '0055'));
    l.add(new Cidades(1502608, '1 - Norte', 'PA', 'Colares', '0055'));
    l.add(new Cidades(3201506, '3 - Sudeste', 'ES', 'Colatina', '0055'));
    l.add(new Cidades(5103205, '5 - Centro-Oeste', 'MT', 'Colider', '0055'));
    l.add(new Cidades(3512001, '3 - Sudeste', 'SP', 'Colina', '0055'));
    l.add(new Cidades(2103505, '2 - Nordeste', 'MA', 'Colinas', '0055'));
    l.add(new Cidades(4305587, '4 - Sul', 'RS', 'Colinas', '0055'));
    l.add(new Cidades(
        5205521, '5 - Centro-Oeste', 'GO', 'Colinas Do Sul', '0055'));
    l.add(new Cidades(1705508, '1 - Norte', 'TO', 'Colinas Do To', '0055'));
    l.add(new Cidades(1716703, '1 - Norte', 'TO', 'Colmeia', '0055'));
    l.add(new Cidades(5103254, '5 - Centro-Oeste', 'MT', 'Colniza', '0055'));
    l.add(new Cidades(3512100, '3 - Sudeste', 'SP', 'Colombia', '0055'));
    l.add(new Cidades(4105805, '4 - Sul', 'PR', 'Colombo', '0055'));
    l.add(new Cidades(
        2202752, '2 - Nordeste', 'PI', 'Colonia Do Gurgueia', '0055'));
    l.add(
        new Cidades(2202778, '2 - Nordeste', 'PI', 'Colonia Do Piaui', '0055'));
    l.add(new Cidades(
        2702108, '2 - Nordeste', 'AL', 'Colonia Leopoldina', '0055'));
    l.add(new Cidades(4105904, '4 - Sul', 'PR', 'Colorado', '0055'));
    l.add(new Cidades(4305603, '4 - Sul', 'RS', 'Colorado', '0055'));
    l.add(new Cidades(1100064, '1 - Norte', 'RO', 'Colorado Do Oeste', '0055'));
    l.add(new Cidades(3116803, '3 - Sudeste', 'MG', 'Coluna', '0055'));
    l.add(new Cidades(1705557, '1 - Norte', 'TO', 'Combinado', '0055'));
    l.add(
        new Cidades(3116902, '3 - Sudeste', 'MG', 'Comendador Gomes', '0055'));
    l.add(new Cidades(
        3300951, '3 - Sudeste', 'RJ', 'Comendador Levy Gasparian', '0055'));
    l.add(new Cidades(3117009, '3 - Sudeste', 'MG', 'Comercinho', '0055'));
    l.add(new Cidades(5103304, '5 - Centro-Oeste', 'MT', 'Comodoro', '0055'));
    l.add(new Cidades(2504405, '2 - Nordeste', 'PB', 'Conceicao', '0055'));
    l.add(new Cidades(
        3117108, '3 - Sudeste', 'MG', 'Conceicao Da Aparecida', '0055'));
    l.add(new Cidades(
        3201605, '3 - Sudeste', 'ES', 'Conceicao Da Barra', '0055'));
    l.add(new Cidades(
        3115201, '3 - Sudeste', 'MG', 'Conceicao Da Barra De Minas', '0055'));
    l.add(new Cidades(
        2908200, '2 - Nordeste', 'BA', 'Conceicao Da Feira', '0055'));
    l.add(
        new Cidades(3117306, '3 - Sudeste', 'MG', 'Conceicao Das Al', '0055'));
    l.add(new Cidades(
        3117207, '3 - Sudeste', 'MG', 'Conceicao Das Pedras', '0055'));
    l.add(new Cidades(
        3117405, '3 - Sudeste', 'MG', 'Conceicao De Ipanema', '0055'));
    l.add(new Cidades(
        3301405, '3 - Sudeste', 'RJ', 'Conceicao De Macabu', '0055'));
    l.add(new Cidades(
        2908309, '2 - Nordeste', 'BA', 'Conceicao Do Almeida', '0055'));
    l.add(new Cidades(
        1502707, '1 - Norte', 'PA', 'Conceicao Do Araguaia', '0055'));
    l.add(new Cidades(
        2202802, '2 - Nordeste', 'PI', 'Conceicao Do Caninde', '0055'));
    l.add(new Cidades(
        3201704, '3 - Sudeste', 'ES', 'Conceicao Do Castelo', '0055'));
    l.add(new Cidades(
        2908408, '2 - Nordeste', 'BA', 'Conceicao Do Coite', '0055'));
    l.add(new Cidades(
        2908507, '2 - Nordeste', 'BA', 'Conceicao Do Jacuipe', '0055'));
    l.add(new Cidades(
        2103554, '2 - Nordeste', 'MA', 'Conceicao Do Lago-Acu', '0055'));
    l.add(new Cidades(
        3117504, '3 - Sudeste', 'MG', 'Conceicao Do Mato Dentro', '0055'));
    l.add(
        new Cidades(3117603, '3 - Sudeste', 'MG', 'Conceicao Do Para', '0055'));
    l.add(new Cidades(
        3117702, '3 - Sudeste', 'MG', 'Conceicao Do Rio Verde', '0055'));
    l.add(new Cidades(1705607, '1 - Norte', 'TO', 'Conceicao Do To', '0055'));
    l.add(new Cidades(
        3117801, '3 - Sudeste', 'MG', 'Conceicao Dos Ouros', '0055'));
    l.add(new Cidades(3512209, '3 - Sudeste', 'SP', 'Conchal', '0055'));
    l.add(new Cidades(3512308, '3 - Sudeste', 'SP', 'Conchas', '0055'));
    l.add(new Cidades(4204301, '4 - Sul', 'SC', 'Concordia', '0055'));
    l.add(new Cidades(1502756, '1 - Norte', 'PA', 'Concordia Do Para', '0055'));
    l.add(new Cidades(2504504, '2 - Nordeste', 'PB', 'Condado', '0055'));
    l.add(new Cidades(2604601, '2 - Nordeste', 'PE', 'Condado', '0055'));
    l.add(new Cidades(2504603, '2 - Nordeste', 'PB', 'Conde', '0055'));
    l.add(new Cidades(2908606, '2 - Nordeste', 'BA', 'Conde', '0055'));
    l.add(new Cidades(2908705, '2 - Nordeste', 'BA', 'Condeuba', '0055'));
    l.add(new Cidades(4305702, '4 - Sul', 'RS', 'Condor', '0055'));
    l.add(new Cidades(3117836, '3 - Sudeste', 'MG', 'Conego Marinho', '0055'));
    l.add(new Cidades(3117876, '3 - Sudeste', 'MG', 'Confins', '0055'));
    l.add(new Cidades(5103353, '5 - Centro-Oeste', 'MT', 'Confresa', '0055'));
    l.add(new Cidades(2504702, '2 - Nordeste', 'PB', 'Congo', '0055'));
    l.add(new Cidades(3117900, '3 - Sudeste', 'MG', 'Congonhal', '0055'));
    l.add(new Cidades(3118007, '3 - Sudeste', 'MG', 'Congonhas', '0055'));
    l.add(new Cidades(
        3118106, '3 - Sudeste', 'MG', 'Congonhas Do Norte', '0055'));
    l.add(new Cidades(4106001, '4 - Sul', 'PR', 'Congonhinhas', '0055'));
    l.add(new Cidades(3118205, '3 - Sudeste', 'MG', 'Conquista', '0055'));
    l.add(new Cidades(
        5103361, '5 - Centro-Oeste', 'MT', 'Conquista D`Oeste', '0055'));
    l.add(new Cidades(
        3118304, '3 - Sudeste', 'MG', 'Conselheiro Lafaiete', '0055'));
    l.add(
        new Cidades(4106100, '4 - Sul', 'PR', 'Conselheiro Mairinck', '0055'));
    l.add(
        new Cidades(3118403, '3 - Sudeste', 'MG', 'Conselheiro Pena', '0055'));
    l.add(new Cidades(3118502, '3 - Sudeste', 'MG', 'Consolacao', '0055'));
    l.add(new Cidades(4305801, '4 - Sul', 'RS', 'Constantina', '0055'));
    l.add(new Cidades(3118601, '3 - Sudeste', 'MG', 'Contagem', '0055'));
    l.add(new Cidades(4106209, '4 - Sul', 'PR', 'Contenda', '0055'));
    l.add(new Cidades(
        2908804, '2 - Nordeste', 'BA', 'Contendas Do Sincora', '0055'));
    l.add(new Cidades(3118700, '3 - Sudeste', 'MG', 'Coqueiral', '0055'));
    l.add(new Cidades(4305835, '4 - Sul', 'RS', 'Coqueiro Baixo', '0055'));
    l.add(new Cidades(2702207, '2 - Nordeste', 'AL', 'Coqueiro Seco', '0055'));
    l.add(new Cidades(4305850, '4 - Sul', 'RS', 'Coqueiros Do Sul', '0055'));
    l.add(
        new Cidades(3118809, '3 - Sudeste', 'MG', 'Coracao De Jesus', '0055'));
    l.add(
        new Cidades(2908903, '2 - Nordeste', 'BA', 'Coracao De Maria', '0055'));
    l.add(new Cidades(4106308, '4 - Sul', 'PR', 'Corbelia', '0055'));
    l.add(new Cidades(3301504, '3 - Sudeste', 'RJ', 'Cordeiro', '0055'));
    l.add(new Cidades(3512407, '3 - Sudeste', 'SP', 'Cordeiropolis', '0055'));
    l.add(new Cidades(2909000, '2 - Nordeste', 'BA', 'Cordeiros', '0055'));
    l.add(new Cidades(4204350, '4 - Sul', 'SC', 'Cordilheira Alta', '0055'));
    l.add(new Cidades(3118908, '3 - Sudeste', 'MG', 'Cordisburgo', '0055'));
    l.add(new Cidades(3119005, '3 - Sudeste', 'MG', 'Cordislandia', '0055'));
    l.add(new Cidades(2304004, '2 - Nordeste', 'CE', 'Coreau', '0055'));
    l.add(new Cidades(2504801, '2 - Nordeste', 'PB', 'Coremas', '0055'));
    l.add(new Cidades(5003108, '5 - Centro-Oeste', 'MS', 'Corguinho', '0055'));
    l.add(new Cidades(2909109, '2 - Nordeste', 'BA', 'Coribe', '0055'));
    l.add(new Cidades(3119104, '3 - Sudeste', 'MG', 'Corinto', '0055'));
    l.add(new Cidades(4106407, '4 - Sul', 'PR', 'Cornelio Procopio', '0055'));
    l.add(new Cidades(3119203, '3 - Sudeste', 'MG', 'Coroaci', '0055'));
    l.add(new Cidades(3512506, '3 - Sudeste', 'SP', 'Coroados', '0055'));
    l.add(new Cidades(2103604, '2 - Nordeste', 'MA', 'Coroata', '0055'));
    l.add(new Cidades(3119302, '3 - Sudeste', 'MG', 'Coromandel', '0055'));
    l.add(new Cidades(4305871, '4 - Sul', 'RS', 'Coronel Barros', '0055'));
    l.add(new Cidades(4305900, '4 - Sul', 'RS', 'Coronel Bicaco', '0055'));
    l.add(new Cidades(
        4106456, '4 - Sul', 'PR', 'Coronel Domingos Soares', '0055'));
    l.add(
        new Cidades(2402808, '2 - Nordeste', 'RN', 'Coronel Ezequiel', '0055'));
    l.add(new Cidades(
        3119401, '3 - Sudeste', 'MG', 'Coronel Fabriciano', '0055'));
    l.add(new Cidades(4204400, '4 - Sul', 'SC', 'Coronel Freitas', '0055'));
    l.add(new Cidades(
        2402907, '2 - Nordeste', 'RN', 'Coronel Joao Pessoa', '0055'));
    l.add(
        new Cidades(2909208, '2 - Nordeste', 'BA', 'Coronel Joao Sa', '0055'));
    l.add(new Cidades(
        2202851, '2 - Nordeste', 'PI', 'Coronel Jose Dias', '0055'));
    l.add(new Cidades(3512605, '3 - Sudeste', 'SP', 'Coronel Macedo', '0055'));
    l.add(new Cidades(4204459, '4 - Sul', 'SC', 'Coronel Martins', '0055'));
    l.add(new Cidades(3119500, '3 - Sudeste', 'MG', 'Coronel Murta', '0055'));
    l.add(new Cidades(3119609, '3 - Sudeste', 'MG', 'Coronel Pacheco', '0055'));
    l.add(new Cidades(4305934, '4 - Sul', 'RS', 'Coronel Pilar', '0055'));
    l.add(new Cidades(
        5003157, '5 - Centro-Oeste', 'MS', 'Coronel Sapucaia', '0055'));
    l.add(new Cidades(4106506, '4 - Sul', 'PR', 'Coronel Vivida', '0055'));
    l.add(new Cidades(
        3119708, '3 - Sudeste', 'MG', 'Coronel Xavier Chaves', '0055'));
    l.add(new Cidades(3119807, '3 - Sudeste', 'MG', 'Corrego Danta', '0055'));
    l.add(new Cidades(
        3119906, '3 - Sudeste', 'MG', 'Corrego Do Bom Jesus', '0055'));
    l.add(new Cidades(
        5205703, '5 - Centro-Oeste', 'GO', 'Corrego Do Ouro', '0055'));
    l.add(new Cidades(3119955, '3 - Sudeste', 'MG', 'Corrego Fundo', '0055'));
    l.add(new Cidades(3120003, '3 - Sudeste', 'MG', 'Corrego Novo', '0055'));
    l.add(new Cidades(4204558, '4 - Sul', 'SC', 'Correia Pinto', '0055'));
    l.add(new Cidades(2202901, '2 - Nordeste', 'PI', 'Corrente', '0055'));
    l.add(new Cidades(2604700, '2 - Nordeste', 'PE', 'Correntes', '0055'));
    l.add(new Cidades(2909307, '2 - Nordeste', 'BA', 'Correntina', '0055'));
    l.add(new Cidades(2604809, '2 - Nordeste', 'PE', 'Cortes', '0055'));
    l.add(new Cidades(5003207, '5 - Centro-Oeste', 'MS', 'Corumba', '0055'));
    l.add(new Cidades(
        5205802, '5 - Centro-Oeste', 'GO', 'Corumba De Goias', '0055'));
    l.add(new Cidades(5205901, '5 - Centro-Oeste', 'GO', 'Corumbaiba', '0055'));
    l.add(new Cidades(3512704, '3 - Sudeste', 'SP', 'Corumbatai', '0055'));
    l.add(new Cidades(4106555, '4 - Sul', 'PR', 'Corumbatai Do Sul', '0055'));
    l.add(new Cidades(1100072, '1 - Norte', 'RO', 'Corumbiara', '0055'));
    l.add(new Cidades(4204509, '4 - Sul', 'SC', 'Corupa', '0055'));
    l.add(new Cidades(2702306, '2 - Nordeste', 'AL', 'Coruripe', '0055'));
    l.add(new Cidades(3512803, '3 - Sudeste', 'SP', 'Cosmopolis', '0055'));
    l.add(new Cidades(3512902, '3 - Sudeste', 'SP', 'Cosmorama', '0055'));
    l.add(new Cidades(1100080, '1 - Norte', 'RO', 'Costa Marques', '0055'));
    l.add(new Cidades(5003256, '5 - Centro-Oeste', 'MS', 'Costa Rica', '0055'));
    l.add(new Cidades(2909406, '2 - Nordeste', 'BA', 'Cotegipe', '0055'));
    l.add(new Cidades(3513009, '3 - Sudeste', 'SP', 'Cotia', '0055'));
    l.add(new Cidades(4305959, '4 - Sul', 'RS', 'Cotipora', '0055'));
    l.add(new Cidades(5103379, '5 - Centro-Oeste', 'MT', 'Cotriguacu', '0055'));
    l.add(new Cidades(
        3120102, '3 - Sudeste', 'MG', 'Couto De Magalhaes De Minas', '0055'));
    l.add(new Cidades(1706001, '1 - Norte', 'TO', 'Couto Magalhaes', '0055'));
    l.add(new Cidades(4305975, '4 - Sul', 'RS', 'Coxilha', '0055'));
    l.add(new Cidades(5003306, '5 - Centro-Oeste', 'MS', 'Coxim', '0055'));
    l.add(new Cidades(2504850, '2 - Nordeste', 'PB', 'Coxixola', '0055'));
    l.add(new Cidades(2702355, '2 - Nordeste', 'AL', 'Craibas', '0055'));
    l.add(new Cidades(2304103, '2 - Nordeste', 'CE', 'Crateus', '0055'));
    l.add(new Cidades(2304202, '2 - Nordeste', 'CE', 'Crato', '0055'));
    l.add(new Cidades(3513108, '3 - Sudeste', 'SP', 'Cravinhos', '0055'));
    l.add(new Cidades(2909505, '2 - Nordeste', 'BA', 'Cravolandia', '0055'));
    l.add(new Cidades(4204608, '4 - Sul', 'SC', 'Criciuma', '0055'));
    l.add(new Cidades(3120151, '3 - Sudeste', 'MG', 'Crisolita', '0055'));
    l.add(new Cidades(2909604, '2 - Nordeste', 'BA', 'Crisopolis', '0055'));
    l.add(new Cidades(4306007, '4 - Sul', 'RS', 'Crissiumal', '0055'));
    l.add(new Cidades(3120201, '3 - Sudeste', 'MG', 'Cristais', '0055'));
    l.add(
        new Cidades(3513207, '3 - Sudeste', 'SP', 'Cristais Paulista', '0055'));
    l.add(new Cidades(4306056, '4 - Sul', 'RS', 'Cristal', '0055'));
    l.add(new Cidades(4306072, '4 - Sul', 'RS', 'Cristal Do Sul', '0055'));
    l.add(new Cidades(1706100, '1 - Norte', 'TO', 'Cristalandia', '0055'));
    l.add(new Cidades(
        2203008, '2 - Nordeste', 'PI', 'Cristalandia Do Piaui', '0055'));
    l.add(new Cidades(3120300, '3 - Sudeste', 'MG', 'Cristalia', '0055'));
    l.add(new Cidades(5206206, '5 - Centro-Oeste', 'GO', 'Cristalina', '0055'));
    l.add(new Cidades(3120409, '3 - Sudeste', 'MG', 'Cristiano Otoni', '0055'));
    l.add(new Cidades(
        5206305, '5 - Centro-Oeste', 'GO', 'Cristianopolis', '0055'));
    l.add(new Cidades(3120508, '3 - Sudeste', 'MG', 'Cristina', '0055'));
    l.add(new Cidades(2801702, '2 - Nordeste', 'SE', 'Cristinapolis', '0055'));
    l.add(
        new Cidades(2203107, '2 - Nordeste', 'PI', 'Cristino Castro', '0055'));
    l.add(new Cidades(2909703, '2 - Nordeste', 'BA', 'Cristopolis', '0055'));
    l.add(new Cidades(5206404, '5 - Centro-Oeste', 'GO', 'Crixas', '0055'));
    l.add(new Cidades(1706258, '1 - Norte', 'TO', 'Crixas Do To', '0055'));
    l.add(new Cidades(2304236, '2 - Nordeste', 'CE', 'Croata', '0055'));
    l.add(new Cidades(5206503, '5 - Centro-Oeste', 'GO', 'Crominia', '0055'));
    l.add(new Cidades(3120607, '3 - Sudeste', 'MG', 'Crucilandia', '0055'));
    l.add(new Cidades(2304251, '2 - Nordeste', 'CE', 'Cruz', '0055'));
    l.add(new Cidades(4306106, '4 - Sul', 'RS', 'Cruz Alta', '0055'));
    l.add(new Cidades(2909802, '2 - Nordeste', 'BA', 'Cruz Das Almas', '0055'));
    l.add(new Cidades(
        2504900, '2 - Nordeste', 'PB', 'Cruz Do Espirito Santo', '0055'));
    l.add(new Cidades(4106803, '4 - Sul', 'PR', 'Cruz Machado', '0055'));
    l.add(new Cidades(3513306, '3 - Sudeste', 'SP', 'Cruzalia', '0055'));
    l.add(new Cidades(4306130, '4 - Sul', 'RS', 'Cruzaltense', '0055'));
    l.add(new Cidades(3513405, '3 - Sudeste', 'SP', 'Cruzeiro', '0055'));
    l.add(new Cidades(
        3120706, '3 - Sudeste', 'MG', 'Cruzeiro Da Fortaleza', '0055'));
    l.add(new Cidades(4106571, '4 - Sul', 'PR', 'Cruzeiro Do Iguacu', '0055'));
    l.add(new Cidades(4106605, '4 - Sul', 'PR', 'Cruzeiro Do Oeste', '0055'));
    l.add(new Cidades(1200203, '1 - Norte', 'AC', 'Cruzeiro Do Sul', '0055'));
    l.add(new Cidades(4106704, '4 - Sul', 'PR', 'Cruzeiro Do Sul', '0055'));
    l.add(new Cidades(4306205, '4 - Sul', 'RS', 'Cruzeiro Do Sul', '0055'));
    l.add(new Cidades(2403004, '2 - Nordeste', 'RN', 'Cruzeta', '0055'));
    l.add(new Cidades(3120805, '3 - Sudeste', 'MG', 'Cruzilia', '0055'));
    l.add(new Cidades(4106852, '4 - Sul', 'PR', 'Cruzmaltina', '0055'));
    l.add(new Cidades(3513504, '3 - Sudeste', 'SP', 'Cubatao', '0055'));
    l.add(new Cidades(2505006, '2 - Nordeste', 'PB', 'Cubati', '0055'));
    l.add(new Cidades(5103403, '5 - Centro-Oeste', 'MT', 'Cuiaba', '0055'));
    l.add(new Cidades(2505105, '2 - Nordeste', 'PB', 'Cuite', '0055'));
    l.add(new Cidades(
        2505238, '2 - Nordeste', 'PB', 'Cuite De Mamanguape', '0055'));
    l.add(new Cidades(2505204, '2 - Nordeste', 'PB', 'Cuitegi', '0055'));
    l.add(new Cidades(1100940, '1 - Norte', 'RO', 'Cujubim', '0055'));
    l.add(new Cidades(5206602, '5 - Centro-Oeste', 'GO', 'Cumari', '0055'));
    l.add(new Cidades(2604908, '2 - Nordeste', 'PE', 'Cumaru', '0055'));
    l.add(new Cidades(1502764, '1 - Norte', 'PA', 'Cumaru Do Norte', '0055'));
    l.add(new Cidades(2801900, '2 - Nordeste', 'SE', 'Cumbe', '0055'));
    l.add(new Cidades(3513603, '3 - Sudeste', 'SP', 'Cunha', '0055'));
    l.add(new Cidades(4204707, '4 - Sul', 'SC', 'Cunha Pora', '0055'));
    l.add(new Cidades(4204756, '4 - Sul', 'SC', 'Cunhatai', '0055'));
    l.add(new Cidades(3120839, '3 - Sudeste', 'MG', 'Cuparaque', '0055'));
    l.add(new Cidades(2605004, '2 - Nordeste', 'PE', 'Cupira', '0055'));
    l.add(new Cidades(2909901, '2 - Nordeste', 'BA', 'Curaca', '0055'));
    l.add(new Cidades(2203206, '2 - Nordeste', 'PI', 'Curimata', '0055'));
    l.add(new Cidades(1502772, '1 - Norte', 'PA', 'Curionopolis', '0055'));
    l.add(new Cidades(4106902, '4 - Sul', 'PR', 'Curitiba', '0055'));
    l.add(new Cidades(4204806, '4 - Sul', 'SC', 'Curitibanos', '0055'));
    l.add(new Cidades(4107009, '4 - Sul', 'PR', 'Curiuva', '0055'));
    l.add(new Cidades(2203230, '2 - Nordeste', 'PI', 'Currais', '0055'));
    l.add(new Cidades(2403103, '2 - Nordeste', 'RN', 'Currais Novos', '0055'));
    l.add(new Cidades(2505279, '2 - Nordeste', 'PB', 'Curral De Cima', '0055'));
    l.add(
        new Cidades(3120870, '3 - Sudeste', 'MG', 'Curral De Dentro', '0055'));
    l.add(new Cidades(
        2203271, '2 - Nordeste', 'PI', 'Curral Novo Do Piaui', '0055'));
    l.add(new Cidades(2505303, '2 - Nordeste', 'PB', 'Curral Velho', '0055'));
    l.add(new Cidades(1502806, '1 - Norte', 'PA', 'Curralinho', '0055'));
    l.add(new Cidades(2203255, '2 - Nordeste', 'PI', 'Curralinhos', '0055'));
    l.add(new Cidades(1502855, '1 - Norte', 'PA', 'Curua', '0055'));
    l.add(new Cidades(1502905, '1 - Norte', 'PA', 'Curuca', '0055'));
    l.add(new Cidades(2103703, '2 - Nordeste', 'MA', 'Cururupu', '0055'));
    l.add(
        new Cidades(5103437, '5 - Centro-Oeste', 'MT', 'Curvelandia', '0055'));
    l.add(new Cidades(3120904, '3 - Sudeste', 'MG', 'Curvelo', '0055'));
    l.add(new Cidades(2605103, '2 - Nordeste', 'PE', 'Custodia', '0055'));
    l.add(new Cidades(1600212, '1 - Norte', 'AP', 'Cutias', '0055'));
    l.add(
        new Cidades(5206701, '5 - Centro-Oeste', 'GO', 'Damianopolis', '0055'));
    l.add(new Cidades(2505352, '2 - Nordeste', 'PB', 'Damiao', '0055'));
    l.add(new Cidades(5206800, '5 - Centro-Oeste', 'GO', 'Damolandia', '0055'));
    l.add(new Cidades(1706506, '1 - Norte', 'TO', 'Darcinopolis', '0055'));
    l.add(new Cidades(2910008, '2 - Nordeste', 'BA', 'Dario Meira', '0055'));
    l.add(new Cidades(3121001, '3 - Sudeste', 'MG', 'Datas', '0055'));
    l.add(new Cidades(4306304, '4 - Sul', 'RS', 'David Canabarro', '0055'));
    l.add(new Cidades(2103752, '2 - Nordeste', 'MA', 'Davinopolis', '0055'));
    l.add(
        new Cidades(5206909, '5 - Centro-Oeste', 'GO', 'Davinopolis', '0055'));
    l.add(new Cidades(3121100, '3 - Sudeste', 'MG', 'Delfim Moreira', '0055'));
    l.add(new Cidades(3121209, '3 - Sudeste', 'MG', 'Delfinopolis', '0055'));
    l.add(
        new Cidades(2702405, '2 - Nordeste', 'AL', 'Delmiro Gouveia', '0055'));
    l.add(new Cidades(3121258, '3 - Sudeste', 'MG', 'Delta', '0055'));
    l.add(new Cidades(2203305, '2 - Nordeste', 'PI', 'Demerval Lobao', '0055'));
    l.add(new Cidades(5103452, '5 - Centro-Oeste', 'MT', 'Denise', '0055'));
    l.add(new Cidades(5003454, '5 - Centro-Oeste', 'MS', 'Deodapolis', '0055'));
    l.add(new Cidades(
        2304269, '2 - Nordeste', 'CE', 'Deputado Irapuan Pinheiro', '0055'));
    l.add(new Cidades(4306320, '4 - Sul', 'RS', 'Derrubadas', '0055'));
    l.add(new Cidades(3513702, '3 - Sudeste', 'SP', 'Descalvado', '0055'));
    l.add(new Cidades(4204905, '4 - Sul', 'SC', 'Descanso', '0055'));
    l.add(new Cidades(3121308, '3 - Sudeste', 'MG', 'Descoberto', '0055'));
    l.add(new Cidades(2505402, '2 - Nordeste', 'PB', 'Desterro', '0055'));
    l.add(new Cidades(
        3121407, '3 - Sudeste', 'MG', 'Desterro De Entre Rios', '0055'));
    l.add(
        new Cidades(3121506, '3 - Sudeste', 'MG', 'Desterro Do Melo', '0055'));
    l.add(
        new Cidades(4306353, '4 - Sul', 'RS', 'Dezesseis De Novembro', '0055'));
    l.add(new Cidades(3513801, '3 - Sudeste', 'SP', 'Diadema', '0055'));
    l.add(new Cidades(2505600, '2 - Nordeste', 'PB', 'Diamante', '0055'));
    l.add(new Cidades(4107157, '4 - Sul', 'PR', 'Diamante D`Oeste', '0055'));
    l.add(new Cidades(4107108, '4 - Sul', 'PR', 'Diamante Do Norte', '0055'));
    l.add(new Cidades(4107124, '4 - Sul', 'PR', 'Diamante Do Sul', '0055'));
    l.add(new Cidades(3121605, '3 - Sudeste', 'MG', 'Diamantina', '0055'));
    l.add(new Cidades(5103502, '5 - Centro-Oeste', 'MT', 'Diamantino', '0055'));
    l.add(new Cidades(1707009, '1 - Norte', 'TO', 'Dianopolis', '0055'));
    l.add(new Cidades(2910057, '2 - Nordeste', 'BA', 'Dias D`Avila', '0055'));
    l.add(
        new Cidades(4306379, '4 - Sul', 'RS', 'Dilermando De Aguiar', '0055'));
    l.add(new Cidades(
        3121704, '3 - Sudeste', 'MG', 'Diogo De Vasconcelos', '0055'));
    l.add(new Cidades(3121803, '3 - Sudeste', 'MG', 'Dionisio', '0055'));
    l.add(new Cidades(4205001, '4 - Sul', 'SC', 'Dionisio Cerqueira', '0055'));
    l.add(new Cidades(5207105, '5 - Centro-Oeste', 'GO', 'Diorama', '0055'));
    l.add(new Cidades(3513850, '3 - Sudeste', 'SP', 'Dirce Reis', '0055'));
    l.add(
        new Cidades(2203354, '2 - Nordeste', 'PI', 'Dirceu Arcoverde', '0055'));
    l.add(new Cidades(2802007, '2 - Nordeste', 'SE', 'Divina Pastora', '0055'));
    l.add(new Cidades(3121902, '3 - Sudeste', 'MG', 'Divinesia', '0055'));
    l.add(new Cidades(3122009, '3 - Sudeste', 'MG', 'Divino', '0055'));
    l.add(new Cidades(
        3122108, '3 - Sudeste', 'MG', 'Divino Das Laranjeiras', '0055'));
    l.add(new Cidades(
        3201803, '3 - Sudeste', 'ES', 'Divino De Sao Lourenco', '0055'));
    l.add(new Cidades(3513900, '3 - Sudeste', 'SP', 'Divinolandia', '0055'));
    l.add(new Cidades(
        3122207, '3 - Sudeste', 'MG', 'Divinolandia De Minas', '0055'));
    l.add(new Cidades(3122306, '3 - Sudeste', 'MG', 'Divinopolis', '0055'));
    l.add(new Cidades(
        5208301, '5 - Centro-Oeste', 'GO', 'Divinopolis De Goias', '0055'));
    l.add(new Cidades(1707108, '1 - Norte', 'TO', 'Divinopolis Do To', '0055'));
    l.add(new Cidades(3122355, '3 - Sudeste', 'MG', 'Divisa Alegre', '0055'));
    l.add(new Cidades(3122405, '3 - Sudeste', 'MG', 'Divisa Nova', '0055'));
    l.add(new Cidades(3122454, '3 - Sudeste', 'MG', 'Divisopolis', '0055'));
    l.add(new Cidades(3514007, '3 - Sudeste', 'SP', 'Dobrada', '0055'));
    l.add(new Cidades(3514106, '3 - Sudeste', 'SP', 'Dois Corregos', '0055'));
    l.add(new Cidades(4306403, '4 - Sul', 'RS', 'Dois Irmaos', '0055'));
    l.add(new Cidades(
        4306429, '4 - Sul', 'RS', 'Dois Irmaos Das Missoes', '0055'));
    l.add(new Cidades(
        5003488, '5 - Centro-Oeste', 'MS', 'Dois Irmaos Do Buriti', '0055'));
    l.add(new Cidades(1707207, '1 - Norte', 'TO', 'Dois Irmaos Do To', '0055'));
    l.add(new Cidades(4306452, '4 - Sul', 'RS', 'Dois Lajeados', '0055'));
    l.add(new Cidades(2702504, '2 - Nordeste', 'AL', 'Dois Riachos', '0055'));
    l.add(new Cidades(4107207, '4 - Sul', 'PR', 'Dois Vizinhos', '0055'));
    l.add(new Cidades(3514205, '3 - Sudeste', 'SP', 'Dolcinopolis', '0055'));
    l.add(new Cidades(5103601, '5 - Centro-Oeste', 'MT', 'Dom Aquino', '0055'));
    l.add(new Cidades(2910107, '2 - Nordeste', 'BA', 'Dom Basilio', '0055'));
    l.add(new Cidades(3122470, '3 - Sudeste', 'MG', 'Dom Bosco', '0055'));
    l.add(new Cidades(3122504, '3 - Sudeste', 'MG', 'Dom Cavati', '0055'));
    l.add(new Cidades(1502939, '1 - Norte', 'PA', 'Dom Eliseu', '0055'));
    l.add(new Cidades(
        2203404, '2 - Nordeste', 'PI', 'Dom Expedito Lopes', '0055'));
    l.add(new Cidades(4306502, '4 - Sul', 'RS', 'Dom Feliciano', '0055'));
    l.add(new Cidades(2203453, '2 - Nordeste', 'PI', 'Dom Inocencio', '0055'));
    l.add(new Cidades(3122603, '3 - Sudeste', 'MG', 'Dom Joaquim', '0055'));
    l.add(
        new Cidades(2910206, '2 - Nordeste', 'BA', 'Dom Macedo Costa', '0055'));
    l.add(new Cidades(4306601, '4 - Sul', 'RS', 'Dom Pedrito', '0055'));
    l.add(new Cidades(2103802, '2 - Nordeste', 'MA', 'Dom Pedro', '0055'));
    l.add(new Cidades(
        4306551, '4 - Sul', 'RS', 'Dom Pedro De Alcantara', '0055'));
    l.add(new Cidades(3122702, '3 - Sudeste', 'MG', 'Dom Silverio', '0055'));
    l.add(new Cidades(3122801, '3 - Sudeste', 'MG', 'Dom Vicoso', '0055'));
    l.add(
        new Cidades(3201902, '3 - Sudeste', 'ES', 'Domingos Martins', '0055'));
    l.add(
        new Cidades(2203420, '2 - Nordeste', 'PI', 'Domingos Mourao', '0055'));
    l.add(new Cidades(4205100, '4 - Sul', 'SC', 'Dona Emma', '0055'));
    l.add(new Cidades(3122900, '3 - Sudeste', 'MG', 'Dona Eusebia', '0055'));
    l.add(new Cidades(4306700, '4 - Sul', 'RS', 'Dona Francisca', '0055'));
    l.add(new Cidades(2505709, '2 - Nordeste', 'PB', 'Dona Ines', '0055'));
    l.add(new Cidades(3123007, '3 - Sudeste', 'MG', 'Dores De Campos', '0055'));
    l.add(
        new Cidades(3123106, '3 - Sudeste', 'MG', 'Dores De Guanhaes', '0055'));
    l.add(new Cidades(3123205, '3 - Sudeste', 'MG', 'Dores Do Indaia', '0055'));
    l.add(new Cidades(
        3202009, '3 - Sudeste', 'ES', 'Dores Do Rio Preto', '0055'));
    l.add(new Cidades(3123304, '3 - Sudeste', 'MG', 'Dores Do Turvo', '0055'));
    l.add(new Cidades(3123403, '3 - Sudeste', 'MG', 'Doresopolis', '0055'));
    l.add(new Cidades(2605152, '2 - Nordeste', 'PE', 'Dormentes', '0055'));
    l.add(new Cidades(4107256, '4 - Sul', 'PR', 'Douradina', '0055'));
    l.add(new Cidades(5003504, '5 - Centro-Oeste', 'MS', 'Douradina', '0055'));
    l.add(new Cidades(3514304, '3 - Sudeste', 'SP', 'Dourado', '0055'));
    l.add(new Cidades(3123502, '3 - Sudeste', 'MG', 'Douradoquara', '0055'));
    l.add(new Cidades(5003702, '5 - Centro-Oeste', 'MS', 'Dourados', '0055'));
    l.add(new Cidades(4107306, '4 - Sul', 'PR', 'Doutor Camargo', '0055'));
    l.add(new Cidades(
        4306734, '4 - Sul', 'RS', 'Doutor Mauricio Cardoso', '0055'));
    l.add(new Cidades(4205159, '4 - Sul', 'SC', 'Doutor Pedrinho', '0055'));
    l.add(new Cidades(4306759, '4 - Sul', 'RS', 'Doutor Ricardo', '0055'));
    l.add(
        new Cidades(2403202, '2 - Nordeste', 'RN', 'Doutor Severiano', '0055'));
    l.add(new Cidades(4128633, '4 - Sul', 'PR', 'Doutor Ulysses', '0055'));
    l.add(
        new Cidades(5207253, '5 - Centro-Oeste', 'GO', 'Doverlandia', '0055'));
    l.add(new Cidades(3514403, '3 - Sudeste', 'SP', 'Dracena', '0055'));
    l.add(new Cidades(3514502, '3 - Sudeste', 'SP', 'Duartina', '0055'));
    l.add(new Cidades(3301603, '3 - Sudeste', 'RJ', 'Duas Barras', '0055'));
    l.add(new Cidades(2505808, '2 - Nordeste', 'PB', 'Duas Estradas', '0055'));
    l.add(new Cidades(1707306, '1 - Norte', 'TO', 'Duere', '0055'));
    l.add(new Cidades(3514601, '3 - Sudeste', 'SP', 'Dumont', '0055'));
    l.add(new Cidades(2103901, '2 - Nordeste', 'MA', 'Duque Bacelar', '0055'));
    l.add(new Cidades(3301702, '3 - Sudeste', 'RJ', 'Duque De Caxias', '0055'));
    l.add(new Cidades(3123528, '3 - Sudeste', 'MG', 'Durande', '0055'));
    l.add(new Cidades(3514700, '3 - Sudeste', 'SP', 'Echapora', '0055'));
    l.add(new Cidades(3202108, '3 - Sudeste', 'ES', 'Ecoporanga', '0055'));
    l.add(new Cidades(5207352, '5 - Centro-Oeste', 'GO', 'Edealina', '0055'));
    l.add(new Cidades(5207402, '5 - Centro-Oeste', 'GO', 'Edeia', '0055'));
    l.add(new Cidades(1301407, '1 - Norte', 'AM', 'Eirunepe', '0055'));
    l.add(new Cidades(3514809, '3 - Sudeste', 'SP', 'Eldorado', '0055'));
    l.add(new Cidades(5003751, '5 - Centro-Oeste', 'MS', 'Eldorado', '0055'));
    l.add(new Cidades(4306767, '4 - Sul', 'RS', 'Eldorado Do Sul', '0055'));
    l.add(new Cidades(
        1502954, '1 - Norte', 'PA', 'Eldorado Dos Carajas', '0055'));
    l.add(new Cidades(2203503, '2 - Nordeste', 'PI', 'Elesbao Veloso', '0055'));
    l.add(new Cidades(3514908, '3 - Sudeste', 'SP', 'Elias Fausto', '0055'));
    l.add(new Cidades(2203602, '2 - Nordeste', 'PI', 'Eliseu Martins', '0055'));
    l.add(new Cidades(3514924, '3 - Sudeste', 'SP', 'Elisiario', '0055'));
    l.add(new Cidades(2910305, '2 - Nordeste', 'BA', 'Elisio Medrado', '0055'));
    l.add(new Cidades(3123601, '3 - Sudeste', 'MG', 'Eloi Mendes', '0055'));
    l.add(new Cidades(2505907, '2 - Nordeste', 'PB', 'Emas', '0055'));
    l.add(new Cidades(3514957, '3 - Sudeste', 'SP', 'Embauba', '0055'));
    l.add(new Cidades(3515004, '3 - Sudeste', 'SP', 'Embu', '0055'));
    l.add(new Cidades(3515103, '3 - Sudeste', 'SP', 'Embu-Guacu', '0055'));
    l.add(new Cidades(3515129, '3 - Sudeste', 'SP', 'Emilianopolis', '0055'));
    l.add(new Cidades(4306809, '4 - Sul', 'RS', 'Encantado', '0055'));
    l.add(new Cidades(2403301, '2 - Nordeste', 'RN', 'Encanto', '0055'));
    l.add(new Cidades(2910404, '2 - Nordeste', 'BA', 'Encruzilhada', '0055'));
    l.add(new Cidades(4306908, '4 - Sul', 'RS', 'Encruzilhada Do Sul', '0055'));
    l.add(new Cidades(4107405, '4 - Sul', 'PR', 'Eneas Marques', '0055'));
    l.add(new Cidades(4107504, '4 - Sul', 'PR', 'Engenheiro Beltrao', '0055'));
    l.add(
        new Cidades(3123700, '3 - Sudeste', 'MG', 'Engenheiro Caldas', '0055'));
    l.add(
        new Cidades(3515152, '3 - Sudeste', 'SP', 'Engenheiro Coelho', '0055'));
    l.add(new Cidades(
        3123809, '3 - Sudeste', 'MG', 'Engenheiro Navarro', '0055'));
    l.add(new Cidades(
        3301801, '3 - Sudeste', 'RJ', 'Engenheiro Paulo De Frontin', '0055'));
    l.add(new Cidades(4306924, '4 - Sul', 'RS', 'Engenho Velho', '0055'));
    l.add(new Cidades(3123858, '3 - Sudeste', 'MG', 'Entre Folhas', '0055'));
    l.add(new Cidades(2910503, '2 - Nordeste', 'BA', 'Entre Rios', '0055'));
    l.add(new Cidades(4205175, '4 - Sul', 'SC', 'Entre Rios', '0055'));
    l.add(new Cidades(
        3123908, '3 - Sudeste', 'MG', 'Entre Rios De Minas', '0055'));
    l.add(new Cidades(4107538, '4 - Sul', 'PR', 'Entre Rios Do Oeste', '0055'));
    l.add(new Cidades(4306957, '4 - Sul', 'RS', 'Entre Rios Do Sul', '0055'));
    l.add(new Cidades(4306932, '4 - Sul', 'RS', 'Entre-Ijuis', '0055'));
    l.add(new Cidades(1301506, '1 - Norte', 'AM', 'Envira', '0055'));
    l.add(new Cidades(1200252, '1 - Norte', 'AC', 'Epitaciolandia', '0055'));
    l.add(new Cidades(2403400, '2 - Nordeste', 'RN', 'Equador', '0055'));
    l.add(new Cidades(4306973, '4 - Sul', 'RS', 'Erebango', '0055'));
    l.add(new Cidades(4307005, '4 - Sul', 'RS', 'Erechim', '0055'));
    l.add(new Cidades(2304277, '2 - Nordeste', 'CE', 'Erere', '0055'));
    l.add(new Cidades(2900504, '2 - Nordeste', 'BA', 'Erico Cardoso', '0055'));
    l.add(new Cidades(4205191, '4 - Sul', 'SC', 'Ermo', '0055'));
    l.add(new Cidades(4307054, '4 - Sul', 'RS', 'Ernestina', '0055'));
    l.add(new Cidades(4307203, '4 - Sul', 'RS', 'Erval Grande', '0055'));
    l.add(new Cidades(4307302, '4 - Sul', 'RS', 'Erval Seco', '0055'));
    l.add(new Cidades(4205209, '4 - Sul', 'SC', 'Erval Velho', '0055'));
    l.add(new Cidades(3124005, '3 - Sudeste', 'MG', 'Ervalia', '0055'));
    l.add(new Cidades(2605202, '2 - Nordeste', 'PE', 'Escada', '0055'));
    l.add(new Cidades(4307401, '4 - Sul', 'RS', 'Esmeralda', '0055'));
    l.add(new Cidades(3124104, '3 - Sudeste', 'MG', 'Esmeraldas', '0055'));
    l.add(new Cidades(3124203, '3 - Sudeste', 'MG', 'Espera Feliz', '0055'));
    l.add(new Cidades(2506004, '2 - Nordeste', 'PB', 'Esperanca', '0055'));
    l.add(new Cidades(4307450, '4 - Sul', 'RS', 'Esperanca Do Sul', '0055'));
    l.add(new Cidades(4107520, '4 - Sul', 'PR', 'Esperanca Nova', '0055'));
    l.add(new Cidades(1707405, '1 - Norte', 'TO', 'Esperantina', '0055'));
    l.add(new Cidades(2203701, '2 - Nordeste', 'PI', 'Esperantina', '0055'));
    l.add(
        new Cidades(2104008, '2 - Nordeste', 'MA', 'Esperantinopolis', '0055'));
    l.add(new Cidades(
        4107546, '4 - Sul', 'PR', 'Espigao Alto Do Iguacu', '0055'));
    l.add(new Cidades(1100098, '1 - Norte', 'RO', 'Espigao D`Oeste', '0055'));
    l.add(new Cidades(3124302, '3 - Sudeste', 'MG', 'Espinosa', '0055'));
    l.add(new Cidades(2403509, '2 - Nordeste', 'RN', 'Espirito Santo', '0055'));
    l.add(new Cidades(
        3124401, '3 - Sudeste', 'MG', 'Espirito Santo Do Dourado', '0055'));
    l.add(new Cidades(
        3515186, '3 - Sudeste', 'SP', 'Espirito Santo Do Pinhal', '0055'));
    l.add(new Cidades(
        3515194, '3 - Sudeste', 'SP', 'Espirito Santo Do Turvo', '0055'));
    l.add(new Cidades(2910602, '2 - Nordeste', 'BA', 'Esplanada', '0055'));
    l.add(new Cidades(4307500, '4 - Sul', 'RS', 'Espumoso', '0055'));
    l.add(new Cidades(4307559, '4 - Sul', 'RS', 'Estacao', '0055'));
    l.add(new Cidades(2802106, '2 - Nordeste', 'SE', 'Estancia', '0055'));
    l.add(new Cidades(4307609, '4 - Sul', 'RS', 'Estancia Velha', '0055'));
    l.add(new Cidades(4307708, '4 - Sul', 'RS', 'Esteio', '0055'));
    l.add(new Cidades(3124500, '3 - Sudeste', 'MG', 'Estiva', '0055'));
    l.add(new Cidades(3557303, '3 - Sudeste', 'SP', 'Estiva Gerbi', '0055'));
    l.add(new Cidades(2104057, '2 - Nordeste', 'MA', 'Estreito', '0055'));
    l.add(new Cidades(4307807, '4 - Sul', 'RS', 'Estrela', '0055'));
    l.add(new Cidades(3515202, '3 - Sudeste', 'SP', 'Estrela D`Oeste', '0055'));
    l.add(new Cidades(3124609, '3 - Sudeste', 'MG', 'Estrela Dalva', '0055'));
    l.add(new Cidades(2702553, '2 - Nordeste', 'AL', 'Estrela De Al', '0055'));
    l.add(
        new Cidades(3124708, '3 - Sudeste', 'MG', 'Estrela Do Indaia', '0055'));
    l.add(
        new Cidades(3515301, '3 - Sudeste', 'SP', 'Estrela Do Norte', '0055'));
    l.add(new Cidades(
        5207501, '5 - Centro-Oeste', 'GO', 'Estrela Do Norte', '0055'));
    l.add(new Cidades(3124807, '3 - Sudeste', 'MG', 'Estrela Do Sul', '0055'));
    l.add(new Cidades(4307815, '4 - Sul', 'RS', 'Estrela Velha', '0055'));
    l.add(new Cidades(
        2910701, '2 - Nordeste', 'BA', 'Euclides Da Cunha', '0055'));
    l.add(new Cidades(
        3515350, '3 - Sudeste', 'SP', 'Euclides Da Cunha Paulista', '0055'));
    l.add(new Cidades(4307831, '4 - Sul', 'RS', 'Eugenio De Castro', '0055'));
    l.add(new Cidades(3124906, '3 - Sudeste', 'MG', 'Eugenopolis', '0055'));
    l.add(new Cidades(2910727, '2 - Nordeste', 'BA', 'Eunapolis', '0055'));
    l.add(new Cidades(2304285, '2 - Nordeste', 'CE', 'Eusebio', '0055'));
    l.add(
        new Cidades(3125002, '3 - Sudeste', 'MG', 'Ewbank Da Camara', '0055'));
    l.add(new Cidades(3125101, '3 - Sudeste', 'MG', 'Extrema', '0055'));
    l.add(new Cidades(2403608, '2 - Nordeste', 'RN', 'Extremoz', '0055'));
    l.add(new Cidades(2605301, '2 - Nordeste', 'PE', 'Exu', '0055'));
    l.add(new Cidades(2506103, '2 - Nordeste', 'PB', 'Fagundes', '0055'));
    l.add(new Cidades(4307864, '4 - Sul', 'RS', 'Fagundes Varela', '0055'));
    l.add(new Cidades(5207535, '5 - Centro-Oeste', 'GO', 'Faina', '0055'));
    l.add(new Cidades(3125200, '3 - Sudeste', 'MG', 'Fama', '0055'));
    l.add(new Cidades(3125309, '3 - Sudeste', 'MG', 'Faria Lemos', '0055'));
    l.add(new Cidades(2304301, '2 - Nordeste', 'CE', 'Farias Brito', '0055'));
    l.add(new Cidades(1503002, '1 - Norte', 'PA', 'Faro', '0055'));
    l.add(new Cidades(4107553, '4 - Sul', 'PR', 'Farol', '0055'));
    l.add(new Cidades(4307906, '4 - Sul', 'RS', 'Farroupilha', '0055'));
    l.add(new Cidades(3515400, '3 - Sudeste', 'SP', 'Fartura', '0055'));
    l.add(
        new Cidades(2203750, '2 - Nordeste', 'PI', 'Fartura Do Piaui', '0055'));
    l.add(new Cidades(1707553, '1 - Norte', 'TO', 'Fatima', '0055'));
    l.add(new Cidades(2910750, '2 - Nordeste', 'BA', 'Fatima', '0055'));
    l.add(new Cidades(
        5003801, '5 - Centro-Oeste', 'MS', 'Fatima Do Sul', '0055'));
    l.add(new Cidades(4107603, '4 - Sul', 'PR', 'Faxinal', '0055'));
    l.add(new Cidades(4308003, '4 - Sul', 'RS', 'Faxinal Do Soturno', '0055'));
    l.add(new Cidades(4205308, '4 - Sul', 'SC', 'Faxinal Dos Guedes', '0055'));
    l.add(new Cidades(4308052, '4 - Sul', 'RS', 'Faxinalzinho', '0055'));
    l.add(
        new Cidades(5207600, '5 - Centro-Oeste', 'GO', 'Fazenda Nova', '0055'));
    l.add(new Cidades(4107652, '4 - Sul', 'PR', 'Fazenda Rio Grande', '0055'));
    l.add(new Cidades(4308078, '4 - Sul', 'RS', 'Fazenda Vilanova', '0055'));
    l.add(new Cidades(1200302, '1 - Norte', 'AC', 'Feijo', '0055'));
    l.add(new Cidades(2910776, '2 - Nordeste', 'BA', 'Feira Da Mata', '0055'));
    l.add(
        new Cidades(2910800, '2 - Nordeste', 'BA', 'Feira De Santana', '0055'));
    l.add(new Cidades(2702603, '2 - Nordeste', 'AL', 'Feira Grande', '0055'));
    l.add(new Cidades(2605400, '2 - Nordeste', 'PE', 'Feira Nova', '0055'));
    l.add(new Cidades(2802205, '2 - Nordeste', 'SE', 'Feira Nova', '0055'));
    l.add(new Cidades(
        2104073, '2 - Nordeste', 'MA', 'Feira Nova Do Maranhao', '0055'));
    l.add(new Cidades(
        3125408, '3 - Sudeste', 'MG', 'Felicio Dos Santos', '0055'));
    l.add(new Cidades(2403707, '2 - Nordeste', 'RN', 'Felipe Guerra', '0055'));
    l.add(new Cidades(3125606, '3 - Sudeste', 'MG', 'Felisburgo', '0055'));
    l.add(new Cidades(3125705, '3 - Sudeste', 'MG', 'Felixlandia', '0055'));
    l.add(new Cidades(4308102, '4 - Sul', 'RS', 'Feliz', '0055'));
    l.add(new Cidades(2702702, '2 - Nordeste', 'AL', 'Feliz Deserto', '0055'));
    l.add(
        new Cidades(5103700, '5 - Centro-Oeste', 'MT', 'Feliz Natal', '0055'));
    l.add(new Cidades(4107702, '4 - Sul', 'PR', 'Fenix', '0055'));
    l.add(new Cidades(4107736, '4 - Sul', 'PR', 'Fernandes Pinheiro', '0055'));
    l.add(new Cidades(
        3125804, '3 - Sudeste', 'MG', 'Fernandes Tourinho', '0055'));
    l.add(new Cidades(
        2605459, '2 - Nordeste', 'PE', 'Fernando De Noronha', '0055'));
    l.add(
        new Cidades(2104081, '2 - Nordeste', 'MA', 'Fernando Falcao', '0055'));
    l.add(
        new Cidades(2403756, '2 - Nordeste', 'RN', 'Fernando Pedroza', '0055'));
    l.add(
        new Cidades(3515608, '3 - Sudeste', 'SP', 'Fernando Prestes', '0055'));
    l.add(new Cidades(3515509, '3 - Sudeste', 'SP', 'Fernandopolis', '0055'));
    l.add(new Cidades(3515657, '3 - Sudeste', 'SP', 'Fernao', '0055'));
    l.add(new Cidades(
        3515707, '3 - Sudeste', 'SP', 'Ferraz De Vasconcelos', '0055'));
    l.add(new Cidades(1600238, '1 - Norte', 'AP', 'Ferreira Gomes', '0055'));
    l.add(new Cidades(2605509, '2 - Nordeste', 'PE', 'Ferreiros', '0055'));
    l.add(new Cidades(3125903, '3 - Sudeste', 'MG', 'Ferros', '0055'));
    l.add(new Cidades(3125952, '3 - Sudeste', 'MG', 'Fervedouro', '0055'));
    l.add(new Cidades(4107751, '4 - Sul', 'PR', 'Figueira', '0055'));
    l.add(new Cidades(5003900, '5 - Centro-Oeste', 'MS', 'Figueirao', '0055'));
    l.add(new Cidades(1707652, '1 - Norte', 'TO', 'Figueiropolis', '0055'));
    l.add(new Cidades(
        5103809, '5 - Centro-Oeste', 'MT', 'Figueiropolis D`Oeste', '0055'));
    l.add(new Cidades(1707702, '1 - Norte', 'TO', 'Filadelfia', '0055'));
    l.add(new Cidades(2910859, '2 - Nordeste', 'BA', 'Filadelfia', '0055'));
    l.add(new Cidades(2910909, '2 - Nordeste', 'BA', 'Firmino Alves', '0055'));
    l.add(
        new Cidades(5207808, '5 - Centro-Oeste', 'GO', 'Firminopolis', '0055'));
    l.add(new Cidades(2702801, '2 - Nordeste', 'AL', 'Flexeiras', '0055'));
    l.add(
        new Cidades(4107850, '4 - Sul', 'PR', 'Flor Da Serra Do Sul', '0055'));
    l.add(new Cidades(4205357, '4 - Sul', 'SC', 'Flor Do Sertao', '0055'));
    l.add(new Cidades(3515806, '3 - Sudeste', 'SP', 'Flora Rica', '0055'));
    l.add(new Cidades(4107801, '4 - Sul', 'PR', 'Florai', '0055'));
    l.add(new Cidades(2403806, '2 - Nordeste', 'RN', 'Florania', '0055'));
    l.add(new Cidades(3515905, '3 - Sudeste', 'SP', 'Floreal', '0055'));
    l.add(new Cidades(2605608, '2 - Nordeste', 'PE', 'Flores', '0055'));
    l.add(new Cidades(4308201, '4 - Sul', 'RS', 'Flores Da Cunha', '0055'));
    l.add(new Cidades(
        5207907, '5 - Centro-Oeste', 'GO', 'Flores De Goias', '0055'));
    l.add(
        new Cidades(2203800, '2 - Nordeste', 'PI', 'Flores Do Piaui', '0055'));
    l.add(new Cidades(2605707, '2 - Nordeste', 'PE', 'Floresta', '0055'));
    l.add(new Cidades(4107900, '4 - Sul', 'PR', 'Floresta', '0055'));
    l.add(new Cidades(2911006, '2 - Nordeste', 'BA', 'Floresta Azul', '0055'));
    l.add(new Cidades(
        1503044, '1 - Norte', 'PA', 'Floresta Do Araguaia', '0055'));
    l.add(new Cidades(
        2203859, '2 - Nordeste', 'PI', 'Floresta Do Piaui', '0055'));
    l.add(new Cidades(3126000, '3 - Sudeste', 'MG', 'Florestal', '0055'));
    l.add(new Cidades(4108007, '4 - Sul', 'PR', 'Florestopolis', '0055'));
    l.add(new Cidades(2203909, '2 - Nordeste', 'PI', 'Floriano', '0055'));
    l.add(new Cidades(4308250, '4 - Sul', 'RS', 'Floriano Peixoto', '0055'));
    l.add(new Cidades(4205407, '4 - Sul', 'SC', 'Florianopolis', '0055'));
    l.add(new Cidades(4108106, '4 - Sul', 'PR', 'Florida', '0055'));
    l.add(
        new Cidades(3516002, '3 - Sudeste', 'SP', 'Florida Paulista', '0055'));
    l.add(new Cidades(3516101, '3 - Sudeste', 'SP', 'Florinia', '0055'));
    l.add(new Cidades(1301605, '1 - Norte', 'AM', 'Fonte Boa', '0055'));
    l.add(new Cidades(4308300, '4 - Sul', 'RS', 'Fontoura Xavier', '0055'));
    l.add(new Cidades(3126109, '3 - Sudeste', 'MG', 'Formiga', '0055'));
    l.add(new Cidades(4308409, '4 - Sul', 'RS', 'Formigueiro', '0055'));
    l.add(new Cidades(5208004, '5 - Centro-Oeste', 'GO', 'Formosa', '0055'));
    l.add(new Cidades(
        2104099, '2 - Nordeste', 'MA', 'Formosa Da Serra Negra', '0055'));
    l.add(new Cidades(4108205, '4 - Sul', 'PR', 'Formosa Do Oeste', '0055'));
    l.add(new Cidades(
        2911105, '2 - Nordeste', 'BA', 'Formosa Do Rio Preto', '0055'));
    l.add(new Cidades(4205431, '4 - Sul', 'SC', 'Formosa Do Sul', '0055'));
    l.add(new Cidades(3126208, '3 - Sudeste', 'MG', 'Formoso', '0055'));
    l.add(new Cidades(5208103, '5 - Centro-Oeste', 'GO', 'Formoso', '0055'));
    l.add(
        new Cidades(1708205, '1 - Norte', 'TO', 'Formoso Do Araguaia', '0055'));
    l.add(new Cidades(4308433, '4 - Sul', 'RS', 'Forquetinha', '0055'));
    l.add(new Cidades(2304350, '2 - Nordeste', 'CE', 'Forquilha', '0055'));
    l.add(new Cidades(4205456, '4 - Sul', 'SC', 'Forquilhinha', '0055'));
    l.add(new Cidades(2304400, '2 - Nordeste', 'CE', 'Fortaleza', '0055'));
    l.add(new Cidades(
        3126307, '3 - Sudeste', 'MG', 'Fortaleza De Minas', '0055'));
    l.add(new Cidades(
        1708254, '1 - Norte', 'TO', 'Fortaleza Do Tabocao', '0055'));
    l.add(new Cidades(
        2104107, '2 - Nordeste', 'MA', 'Fortaleza Dos Nogueiras', '0055'));
    l.add(new Cidades(4308458, '4 - Sul', 'RS', 'Fortaleza Dos Valos', '0055'));
    l.add(new Cidades(2304459, '2 - Nordeste', 'CE', 'Fortim', '0055'));
    l.add(new Cidades(2104206, '2 - Nordeste', 'MA', 'Fortuna', '0055'));
    l.add(
        new Cidades(3126406, '3 - Sudeste', 'MG', 'Fortuna De Minas', '0055'));
    l.add(new Cidades(4108304, '4 - Sul', 'PR', 'Foz Do Iguacu', '0055'));
    l.add(new Cidades(4108452, '4 - Sul', 'PR', 'Foz Do Jordao', '0055'));
    l.add(new Cidades(4205506, '4 - Sul', 'SC', 'Fraiburgo', '0055'));
    l.add(new Cidades(3516200, '3 - Sudeste', 'SP', 'Franca', '0055'));
    l.add(new Cidades(2204006, '2 - Nordeste', 'PI', 'Francinopolis', '0055'));
    l.add(new Cidades(4108320, '4 - Sul', 'PR', 'Francisco Alves', '0055'));
    l.add(
        new Cidades(2204105, '2 - Nordeste', 'PI', 'Francisco Ayres', '0055'));
    l.add(
        new Cidades(3126505, '3 - Sudeste', 'MG', 'Francisco Badaro', '0055'));
    l.add(new Cidades(4108403, '4 - Sul', 'PR', 'Francisco Beltrao', '0055'));
    l.add(
        new Cidades(2403905, '2 - Nordeste', 'RN', 'Francisco Dantas', '0055'));
    l.add(
        new Cidades(3126604, '3 - Sudeste', 'MG', 'Francisco Dumont', '0055'));
    l.add(
        new Cidades(2204154, '2 - Nordeste', 'PI', 'Francisco Macedo', '0055'));
    l.add(
        new Cidades(3516309, '3 - Sudeste', 'SP', 'Francisco Morato', '0055'));
    l.add(new Cidades(3126703, '3 - Sudeste', 'MG', 'Francisco Sa', '0055'));
    l.add(
        new Cidades(2204204, '2 - Nordeste', 'PI', 'Francisco Santos', '0055'));
    l.add(new Cidades(3126752, '3 - Sudeste', 'MG', 'Franciscopolis', '0055'));
    l.add(new Cidades(3516408, '3 - Sudeste', 'SP', 'Franco Da Rocha', '0055'));
    l.add(new Cidades(2304509, '2 - Nordeste', 'CE', 'Frecheirinha', '0055'));
    l.add(
        new Cidades(4308508, '4 - Sul', 'RS', 'Frederico Westphalen', '0055'));
    l.add(new Cidades(3126802, '3 - Sudeste', 'MG', 'Frei Gaspar', '0055'));
    l.add(new Cidades(3126901, '3 - Sudeste', 'MG', 'Frei Inocencio', '0055'));
    l.add(new Cidades(3126950, '3 - Sudeste', 'MG', 'Frei Lagonegro', '0055'));
    l.add(new Cidades(2506202, '2 - Nordeste', 'PB', 'Frei Martinho', '0055'));
    l.add(
        new Cidades(2605806, '2 - Nordeste', 'PE', 'Frei Miguelinho', '0055'));
    l.add(new Cidades(2802304, '2 - Nordeste', 'SE', 'Frei Paulo', '0055'));
    l.add(new Cidades(4205555, '4 - Sul', 'SC', 'Frei Rogerio', '0055'));
    l.add(new Cidades(3127008, '3 - Sudeste', 'MG', 'Fronteira', '0055'));
    l.add(new Cidades(
        3127057, '3 - Sudeste', 'MG', 'Fronteira Dos Vales', '0055'));
    l.add(new Cidades(2204303, '2 - Nordeste', 'PI', 'Fronteiras', '0055'));
    l.add(new Cidades(3127073, '3 - Sudeste', 'MG', 'Fruta De Leite', '0055'));
    l.add(new Cidades(3127107, '3 - Sudeste', 'MG', 'Frutal', '0055'));
    l.add(new Cidades(2404002, '2 - Nordeste', 'RN', 'Frutuoso Gomes', '0055'));
    l.add(new Cidades(3202207, '3 - Sudeste', 'ES', 'Fundao', '0055'));
    l.add(new Cidades(3127206, '3 - Sudeste', 'MG', 'Funilandia', '0055'));
    l.add(
        new Cidades(3516507, '3 - Sudeste', 'SP', 'Gabriel Monteiro', '0055'));
    l.add(new Cidades(2506251, '2 - Nordeste', 'PB', 'Gado Bravo', '0055'));
    l.add(new Cidades(3516606, '3 - Sudeste', 'SP', 'Galia', '0055'));
    l.add(new Cidades(3127305, '3 - Sudeste', 'MG', 'Galileia', '0055'));
    l.add(new Cidades(2404101, '2 - Nordeste', 'RN', 'Galinhos', '0055'));
    l.add(new Cidades(4205605, '4 - Sul', 'SC', 'Galvao', '0055'));
    l.add(new Cidades(2605905, '2 - Nordeste', 'PE', 'Gameleira', '0055'));
    l.add(new Cidades(
        5208152, '5 - Centro-Oeste', 'GO', 'Gameleira De Goias', '0055'));
    l.add(new Cidades(3127339, '3 - Sudeste', 'MG', 'Gameleiras', '0055'));
    l.add(new Cidades(2911204, '2 - Nordeste', 'BA', 'Gandu', '0055'));
    l.add(new Cidades(2606002, '2 - Nordeste', 'PE', 'Garanhuns', '0055'));
    l.add(new Cidades(2802403, '2 - Nordeste', 'SE', 'Gararu', '0055'));
    l.add(new Cidades(3516705, '3 - Sudeste', 'SP', 'Garca', '0055'));
    l.add(new Cidades(4308607, '4 - Sul', 'RS', 'Garibaldi', '0055'));
    l.add(new Cidades(4205704, '4 - Sul', 'SC', 'Garopaba', '0055'));
    l.add(new Cidades(1503077, '1 - Norte', 'PA', 'Garrafao Do Norte', '0055'));
    l.add(new Cidades(4308656, '4 - Sul', 'RS', 'Garruchos', '0055'));
    l.add(new Cidades(4205803, '4 - Sul', 'SC', 'Garuva', '0055'));
    l.add(new Cidades(4205902, '4 - Sul', 'SC', 'Gaspar', '0055'));
    l.add(new Cidades(3516804, '3 - Sudeste', 'SP', 'Gastao Vidigal', '0055'));
    l.add(new Cidades(
        5103858, '5 - Centro-Oeste', 'MT', 'Gaucha Do Norte', '0055'));
    l.add(new Cidades(4308706, '4 - Sul', 'RS', 'Gaurama', '0055'));
    l.add(new Cidades(2911253, '2 - Nordeste', 'BA', 'Gaviao', '0055'));
    l.add(new Cidades(3516853, '3 - Sudeste', 'SP', 'Gaviao Peixoto', '0055'));
    l.add(new Cidades(2204352, '2 - Nordeste', 'PI', 'Geminiano', '0055'));
    l.add(new Cidades(4308805, '4 - Sul', 'RS', 'General Camara', '0055'));
    l.add(new Cidades(4108502, '4 - Sul', 'PR', 'General Carneiro', '0055'));
    l.add(new Cidades(
        5103908, '5 - Centro-Oeste', 'MT', 'General Carneiro', '0055'));
    l.add(
        new Cidades(2802502, '2 - Nordeste', 'SE', 'General Maynard', '0055'));
    l.add(new Cidades(3516903, '3 - Sudeste', 'SP', 'General Salgado', '0055'));
    l.add(
        new Cidades(2304608, '2 - Nordeste', 'CE', 'General Sampaio', '0055'));
    l.add(new Cidades(4308854, '4 - Sul', 'RS', 'Gentil', '0055'));
    l.add(new Cidades(2911303, '2 - Nordeste', 'BA', 'Gentio Do Ouro', '0055'));
    l.add(new Cidades(3517000, '3 - Sudeste', 'SP', 'Getulina', '0055'));
    l.add(new Cidades(4308904, '4 - Sul', 'RS', 'Getulio Vargas', '0055'));
    l.add(new Cidades(2204402, '2 - Nordeste', 'PI', 'Gilbues', '0055'));
    l.add(new Cidades(
        2702900, '2 - Nordeste', 'AL', 'Girau Do Ponciano', '0055'));
    l.add(new Cidades(4309001, '4 - Sul', 'RS', 'Girua', '0055'));
    l.add(new Cidades(3127354, '3 - Sudeste', 'MG', 'Glaucilandia', '0055'));
    l.add(new Cidades(3517109, '3 - Sudeste', 'SP', 'Glicerio', '0055'));
    l.add(new Cidades(2911402, '2 - Nordeste', 'BA', 'Gloria', '0055'));
    l.add(new Cidades(
        5103957, '5 - Centro-Oeste', 'MT', 'Gloria D`Oeste', '0055'));
    l.add(new Cidades(
        5004007, '5 - Centro-Oeste', 'MS', 'Gloria De Dourados', '0055'));
    l.add(
        new Cidades(2606101, '2 - Nordeste', 'PE', 'Gloria Do Goita', '0055'));
    l.add(new Cidades(4309050, '4 - Sul', 'RS', 'Glorinha', '0055'));
    l.add(
        new Cidades(2104305, '2 - Nordeste', 'MA', 'Godofredo Viana', '0055'));
    l.add(new Cidades(4108551, '4 - Sul', 'PR', 'Godoy Moreira', '0055'));
    l.add(new Cidades(3127370, '3 - Sudeste', 'MG', 'Goiabeira', '0055'));
    l.add(new Cidades(2606200, '2 - Nordeste', 'PE', 'Goiana', '0055'));
    l.add(new Cidades(3127388, '3 - Sudeste', 'MG', 'Goiana', '0055'));
    l.add(
        new Cidades(5208400, '5 - Centro-Oeste', 'GO', 'Goianapolis', '0055'));
    l.add(new Cidades(5208509, '5 - Centro-Oeste', 'GO', 'Goiandira', '0055'));
    l.add(new Cidades(5208608, '5 - Centro-Oeste', 'GO', 'Goianesia', '0055'));
    l.add(new Cidades(1503093, '1 - Norte', 'PA', 'Goianesia Do Para', '0055'));
    l.add(new Cidades(5208707, '5 - Centro-Oeste', 'GO', 'Goiania', '0055'));
    l.add(new Cidades(2404200, '2 - Nordeste', 'RN', 'Goianinha', '0055'));
    l.add(new Cidades(5208806, '5 - Centro-Oeste', 'GO', 'Goianira', '0055'));
    l.add(new Cidades(1708304, '1 - Norte', 'TO', 'Goianorte', '0055'));
    l.add(new Cidades(5208905, '5 - Centro-Oeste', 'GO', 'Goias', '0055'));
    l.add(new Cidades(1709005, '1 - Norte', 'TO', 'Goiatins', '0055'));
    l.add(new Cidades(5209101, '5 - Centro-Oeste', 'GO', 'Goiatuba', '0055'));
    l.add(new Cidades(4108601, '4 - Sul', 'PR', 'Goioere', '0055'));
    l.add(new Cidades(4108650, '4 - Sul', 'PR', 'Goioxim', '0055'));
    l.add(new Cidades(3127404, '3 - Sudeste', 'MG', 'Goncalves', '0055'));
    l.add(new Cidades(2104404, '2 - Nordeste', 'MA', 'Goncalves Dias', '0055'));
    l.add(new Cidades(2911501, '2 - Nordeste', 'BA', 'Gongogi', '0055'));
    l.add(new Cidades(3127503, '3 - Sudeste', 'MG', 'Gonzaga', '0055'));
    l.add(new Cidades(3127602, '3 - Sudeste', 'MG', 'Gouveia', '0055'));
    l.add(
        new Cidades(5209150, '5 - Centro-Oeste', 'GO', 'Gouvelandia', '0055'));
    l.add(new Cidades(
        2104503, '2 - Nordeste', 'MA', 'Governador Archer', '0055'));
    l.add(new Cidades(
        4206009, '4 - Sul', 'SC', 'Governador Celso Ramos', '0055'));
    l.add(new Cidades(
        2404309, '2 - Nordeste', 'RN', 'Governador Dix-Sept Rosado', '0055'));
    l.add(new Cidades(
        2104552, '2 - Nordeste', 'MA', 'Governador Edison Lobao', '0055'));
    l.add(new Cidades(
        2104602, '2 - Nordeste', 'MA', 'Governador Eugenio Barros', '0055'));
    l.add(new Cidades(
        1101005, '1 - Norte', 'RO', 'Governador Jorge Teixeira', '0055'));
    l.add(new Cidades(
        3202256, '3 - Sudeste', 'ES', 'Governador Lindenberg', '0055'));
    l.add(new Cidades(
        2104628, '2 - Nordeste', 'MA', 'Governador Luiz Rocha', '0055'));
    l.add(new Cidades(
        2911600, '2 - Nordeste', 'BA', 'Governador Mangabeira', '0055'));
    l.add(new Cidades(
        2104651, '2 - Nordeste', 'MA', 'Governador Newton Bello', '0055'));
    l.add(new Cidades(
        2104677, '2 - Nordeste', 'MA', 'Governador Nunes Freire', '0055'));
    l.add(new Cidades(
        3127701, '3 - Sudeste', 'MG', 'Governador Valadares', '0055'));
    l.add(new Cidades(2304657, '2 - Nordeste', 'CE', 'Graca', '0055'));
    l.add(new Cidades(2104701, '2 - Nordeste', 'MA', 'Graca Aranha', '0055'));
    l.add(new Cidades(2802601, '2 - Nordeste', 'SE', 'Gracho Cardoso', '0055'));
    l.add(new Cidades(2104800, '2 - Nordeste', 'MA', 'Grajau', '0055'));
    l.add(new Cidades(4309100, '4 - Sul', 'RS', 'Gramado', '0055'));
    l.add(
        new Cidades(4309126, '4 - Sul', 'RS', 'Gramado Dos Loureiros', '0055'));
    l.add(new Cidades(4309159, '4 - Sul', 'RS', 'Gramado Xavier', '0055'));
    l.add(new Cidades(4108700, '4 - Sul', 'PR', 'Grandes Rios', '0055'));
    l.add(new Cidades(2606309, '2 - Nordeste', 'PE', 'Granito', '0055'));
    l.add(new Cidades(2304707, '2 - Nordeste', 'CE', 'Granja', '0055'));
    l.add(new Cidades(2304806, '2 - Nordeste', 'CE', 'Granjeiro', '0055'));
    l.add(new Cidades(3127800, '3 - Sudeste', 'MG', 'Grao Mogol', '0055'));
    l.add(new Cidades(4206108, '4 - Sul', 'SC', 'Grao Para', '0055'));
    l.add(new Cidades(2606408, '2 - Nordeste', 'PE', 'Gravata', '0055'));
    l.add(new Cidades(4309209, '4 - Sul', 'RS', 'Gravatai', '0055'));
    l.add(new Cidades(4206207, '4 - Sul', 'SC', 'Gravatal', '0055'));
    l.add(new Cidades(2304905, '2 - Nordeste', 'CE', 'Groairas', '0055'));
    l.add(new Cidades(2404408, '2 - Nordeste', 'RN', 'Grossos', '0055'));
    l.add(new Cidades(3127909, '3 - Sudeste', 'MG', 'Grupiara', '0055'));
    l.add(new Cidades(4309258, '4 - Sul', 'RS', 'Guabiju', '0055'));
    l.add(new Cidades(4206306, '4 - Sul', 'SC', 'Guabiruba', '0055'));
    l.add(new Cidades(3202306, '3 - Sudeste', 'ES', 'Guacui', '0055'));
    l.add(new Cidades(2204501, '2 - Nordeste', 'PI', 'Guadalupe', '0055'));
    l.add(new Cidades(4309308, '4 - Sul', 'RS', 'Guaiba', '0055'));
    l.add(new Cidades(3517208, '3 - Sudeste', 'SP', 'Guaicara', '0055'));
    l.add(new Cidades(3517307, '3 - Sudeste', 'SP', 'Guaimbe', '0055'));
    l.add(new Cidades(3517406, '3 - Sudeste', 'SP', 'Guaira', '0055'));
    l.add(new Cidades(4108809, '4 - Sul', 'PR', 'Guaira', '0055'));
    l.add(new Cidades(4108908, '4 - Sul', 'PR', 'Guairaca', '0055'));
    l.add(new Cidades(2304954, '2 - Nordeste', 'CE', 'Guaiuba', '0055'));
    l.add(new Cidades(1301654, '1 - Norte', 'AM', 'Guajara', '0055'));
    l.add(new Cidades(1100106, '1 - Norte', 'RO', 'Guajara-Mirim', '0055'));
    l.add(new Cidades(2911659, '2 - Nordeste', 'BA', 'Guajeru', '0055'));
    l.add(new Cidades(2404507, '2 - Nordeste', 'RN', 'Guamare', '0055'));
    l.add(new Cidades(4108957, '4 - Sul', 'PR', 'Guamiranga', '0055'));
    l.add(new Cidades(2911709, '2 - Nordeste', 'BA', 'Guanambi', '0055'));
    l.add(new Cidades(3128006, '3 - Sudeste', 'MG', 'Guanhaes', '0055'));
    l.add(new Cidades(3128105, '3 - Sudeste', 'MG', 'Guape', '0055'));
    l.add(new Cidades(3517505, '3 - Sudeste', 'SP', 'Guapiacu', '0055'));
    l.add(new Cidades(3517604, '3 - Sudeste', 'SP', 'Guapiara', '0055'));
    l.add(new Cidades(3301850, '3 - Sudeste', 'RJ', 'Guapimirim', '0055'));
    l.add(new Cidades(4109005, '4 - Sul', 'PR', 'Guapirama', '0055'));
    l.add(new Cidades(5209200, '5 - Centro-Oeste', 'GO', 'Guapo', '0055'));
    l.add(new Cidades(4309407, '4 - Sul', 'RS', 'Guapore', '0055'));
    l.add(new Cidades(4109104, '4 - Sul', 'PR', 'Guaporema', '0055'));
    l.add(new Cidades(3517703, '3 - Sudeste', 'SP', 'Guara', '0055'));
    l.add(new Cidades(2506301, '2 - Nordeste', 'PB', 'Guarabira', '0055'));
    l.add(new Cidades(3517802, '3 - Sudeste', 'SP', 'Guaracai', '0055'));
    l.add(new Cidades(3517901, '3 - Sudeste', 'SP', 'Guaraci', '0055'));
    l.add(new Cidades(4109203, '4 - Sul', 'PR', 'Guaraci', '0055'));
    l.add(new Cidades(3128204, '3 - Sudeste', 'MG', 'Guaraciaba', '0055'));
    l.add(new Cidades(4206405, '4 - Sul', 'SC', 'Guaraciaba', '0055'));
    l.add(new Cidades(
        2305001, '2 - Nordeste', 'CE', 'Guaraciaba Do Norte', '0055'));
    l.add(new Cidades(3128253, '3 - Sudeste', 'MG', 'Guaraciama', '0055'));
    l.add(new Cidades(1709302, '1 - Norte', 'TO', 'Guarai', '0055'));
    l.add(new Cidades(5209291, '5 - Centro-Oeste', 'GO', 'Guaraita', '0055'));
    l.add(new Cidades(2305100, '2 - Nordeste', 'CE', 'Guaramiranga', '0055'));
    l.add(new Cidades(4206504, '4 - Sul', 'SC', 'Guaramirim', '0055'));
    l.add(new Cidades(3128303, '3 - Sudeste', 'MG', 'Guaranesia', '0055'));
    l.add(new Cidades(3128402, '3 - Sudeste', 'MG', 'Guarani', '0055'));
    l.add(new Cidades(3518008, '3 - Sudeste', 'SP', 'Guarani D`Oeste', '0055'));
    l.add(new Cidades(4309506, '4 - Sul', 'RS', 'Guarani Das Missoes', '0055'));
    l.add(new Cidades(
        5209408, '5 - Centro-Oeste', 'GO', 'Guarani De Goias', '0055'));
    l.add(new Cidades(4109302, '4 - Sul', 'PR', 'Guaraniacu', '0055'));
    l.add(new Cidades(3518107, '3 - Sudeste', 'SP', 'Guaranta', '0055'));
    l.add(new Cidades(
        5104104, '5 - Centro-Oeste', 'MT', 'Guaranta Do Norte', '0055'));
    l.add(new Cidades(3202405, '3 - Sudeste', 'ES', 'Guarapari', '0055'));
    l.add(new Cidades(4109401, '4 - Sul', 'PR', 'Guarapuava', '0055'));
    l.add(new Cidades(4109500, '4 - Sul', 'PR', 'Guaraquecaba', '0055'));
    l.add(new Cidades(3128501, '3 - Sudeste', 'MG', 'Guarara', '0055'));
    l.add(new Cidades(3518206, '3 - Sudeste', 'SP', 'Guararapes', '0055'));
    l.add(new Cidades(3518305, '3 - Sudeste', 'SP', 'Guararema', '0055'));
    l.add(new Cidades(2911808, '2 - Nordeste', 'BA', 'Guaratinga', '0055'));
    l.add(new Cidades(3518404, '3 - Sudeste', 'SP', 'Guaratingueta', '0055'));
    l.add(new Cidades(4109609, '4 - Sul', 'PR', 'Guaratuba', '0055'));
    l.add(new Cidades(3128600, '3 - Sudeste', 'MG', 'Guarda-Mor', '0055'));
    l.add(new Cidades(3518503, '3 - Sudeste', 'SP', 'Guarei', '0055'));
    l.add(new Cidades(3518602, '3 - Sudeste', 'SP', 'Guariba', '0055'));
    l.add(new Cidades(2204550, '2 - Nordeste', 'PI', 'Guaribas', '0055'));
    l.add(new Cidades(5209457, '5 - Centro-Oeste', 'GO', 'Guarinos', '0055'));
    l.add(new Cidades(3518701, '3 - Sudeste', 'SP', 'Guaruja', '0055'));
    l.add(new Cidades(4206603, '4 - Sul', 'SC', 'Guaruja Do Sul', '0055'));
    l.add(new Cidades(3518800, '3 - Sudeste', 'SP', 'Guarulhos', '0055'));
    l.add(new Cidades(4206652, '4 - Sul', 'SC', 'Guatambu', '0055'));
    l.add(new Cidades(3518859, '3 - Sudeste', 'SP', 'Guatapara', '0055'));
    l.add(new Cidades(3128709, '3 - Sudeste', 'MG', 'Guaxupe', '0055'));
    l.add(new Cidades(
        5004106, '5 - Centro-Oeste', 'MS', 'Guia Lopes Da Laguna', '0055'));
    l.add(new Cidades(3128808, '3 - Sudeste', 'MG', 'Guidoval', '0055'));
    l.add(new Cidades(2104909, '2 - Nordeste', 'MA', 'Guimaraes', '0055'));
    l.add(new Cidades(3128907, '3 - Sudeste', 'MG', 'Guimarania', '0055'));
    l.add(new Cidades(5104203, '5 - Centro-Oeste', 'MT', 'Guiratinga', '0055'));
    l.add(new Cidades(3129004, '3 - Sudeste', 'MG', 'Guiricema', '0055'));
    l.add(new Cidades(3129103, '3 - Sudeste', 'MG', 'Gurinhata', '0055'));
    l.add(new Cidades(2506400, '2 - Nordeste', 'PB', 'Gurinhem', '0055'));
    l.add(new Cidades(2506509, '2 - Nordeste', 'PB', 'Gurjao', '0055'));
    l.add(new Cidades(1503101, '1 - Norte', 'PA', 'Gurupa', '0055'));
    l.add(new Cidades(1709500, '1 - Norte', 'TO', 'Gurupi', '0055'));
    l.add(new Cidades(3518909, '3 - Sudeste', 'SP', 'Guzolandia', '0055'));
    l.add(new Cidades(4309555, '4 - Sul', 'RS', 'Harmonia', '0055'));
    l.add(new Cidades(5209606, '5 - Centro-Oeste', 'GO', 'Heitorai', '0055'));
    l.add(new Cidades(3129202, '3 - Sudeste', 'MG', 'Heliodora', '0055'));
    l.add(new Cidades(2911857, '2 - Nordeste', 'BA', 'Heliopolis', '0055'));
    l.add(new Cidades(3519006, '3 - Sudeste', 'SP', 'Herculandia', '0055'));
    l.add(new Cidades(4307104, '4 - Sul', 'RS', 'Herval', '0055'));
    l.add(new Cidades(4206702, '4 - Sul', 'SC', 'Herval D`Oeste', '0055'));
    l.add(new Cidades(4309571, '4 - Sul', 'RS', 'Herveiras', '0055'));
    l.add(new Cidades(2305209, '2 - Nordeste', 'CE', 'Hidrolandia', '0055'));
    l.add(
        new Cidades(5209705, '5 - Centro-Oeste', 'GO', 'Hidrolandia', '0055'));
    l.add(new Cidades(5209804, '5 - Centro-Oeste', 'GO', 'Hidrolina', '0055'));
    l.add(new Cidades(3519055, '3 - Sudeste', 'SP', 'Holambra', '0055'));
    l.add(new Cidades(4109658, '4 - Sul', 'PR', 'Honorio Serpa', '0055'));
    l.add(new Cidades(2305233, '2 - Nordeste', 'CE', 'Horizonte', '0055'));
    l.add(new Cidades(4309605, '4 - Sul', 'RS', 'Horizontina', '0055'));
    l.add(new Cidades(3519071, '3 - Sudeste', 'SP', 'Hortolandia', '0055'));
    l.add(new Cidades(2204600, '2 - Nordeste', 'PI', 'Hugo Napoleao', '0055'));
    l.add(new Cidades(4309654, '4 - Sul', 'RS', 'Hulha Negra', '0055'));
    l.add(new Cidades(1301704, '1 - Norte', 'AM', 'Humaita', '0055'));
    l.add(new Cidades(4309704, '4 - Sul', 'RS', 'Humaita', '0055'));
    l.add(new Cidades(
        2105005, '2 - Nordeste', 'MA', 'Humberto De Campos', '0055'));
    l.add(new Cidades(3519105, '3 - Sudeste', 'SP', 'Iacanga', '0055'));
    l.add(new Cidades(5209903, '5 - Centro-Oeste', 'GO', 'Iaciara', '0055'));
    l.add(new Cidades(3519204, '3 - Sudeste', 'SP', 'Iacri', '0055'));
    l.add(new Cidades(2911907, '2 - Nordeste', 'BA', 'Iacu', '0055'));
    l.add(new Cidades(3129301, '3 - Sudeste', 'MG', 'Iapu', '0055'));
    l.add(new Cidades(3519253, '3 - Sudeste', 'SP', 'Iaras', '0055'));
    l.add(new Cidades(2606507, '2 - Nordeste', 'PE', 'Iati', '0055'));
    l.add(new Cidades(4109708, '4 - Sul', 'PR', 'Ibaiti', '0055'));
    l.add(new Cidades(4309753, '4 - Sul', 'RS', 'Ibarama', '0055'));
    l.add(new Cidades(2305266, '2 - Nordeste', 'CE', 'Ibaretama', '0055'));
    l.add(new Cidades(3519303, '3 - Sudeste', 'SP', 'Ibate', '0055'));
    l.add(new Cidades(2703007, '2 - Nordeste', 'AL', 'Ibateguara', '0055'));
    l.add(new Cidades(3202454, '3 - Sudeste', 'ES', 'Ibatiba', '0055'));
    l.add(new Cidades(4109757, '4 - Sul', 'PR', 'Ibema', '0055'));
    l.add(new Cidades(3129400, '3 - Sudeste', 'MG', 'Ibertioga', '0055'));
    l.add(new Cidades(3129509, '3 - Sudeste', 'MG', 'Ibia', '0055'));
    l.add(new Cidades(4309803, '4 - Sul', 'RS', 'Ibiaca', '0055'));
    l.add(new Cidades(3129608, '3 - Sudeste', 'MG', 'Ibiai', '0055'));
    l.add(new Cidades(4206751, '4 - Sul', 'SC', 'Ibiam', '0055'));
    l.add(new Cidades(2305308, '2 - Nordeste', 'CE', 'Ibiapina', '0055'));
    l.add(new Cidades(2506608, '2 - Nordeste', 'PB', 'Ibiara', '0055'));
    l.add(new Cidades(2912004, '2 - Nordeste', 'BA', 'Ibiassuce', '0055'));
    l.add(new Cidades(2912103, '2 - Nordeste', 'BA', 'Ibicarai', '0055'));
    l.add(new Cidades(4206801, '4 - Sul', 'SC', 'Ibicare', '0055'));
    l.add(new Cidades(2912202, '2 - Nordeste', 'BA', 'Ibicoara', '0055'));
    l.add(new Cidades(2912301, '2 - Nordeste', 'BA', 'Ibicui', '0055'));
    l.add(new Cidades(2305332, '2 - Nordeste', 'CE', 'Ibicuitinga', '0055'));
    l.add(new Cidades(2606606, '2 - Nordeste', 'PE', 'Ibimirim', '0055'));
    l.add(new Cidades(2912400, '2 - Nordeste', 'BA', 'Ibipeba', '0055'));
    l.add(new Cidades(2912509, '2 - Nordeste', 'BA', 'Ibipitanga', '0055'));
    l.add(new Cidades(4109807, '4 - Sul', 'PR', 'Ibipora', '0055'));
    l.add(new Cidades(2912608, '2 - Nordeste', 'BA', 'Ibiquera', '0055'));
    l.add(new Cidades(3519402, '3 - Sudeste', 'SP', 'Ibira', '0055'));
    l.add(new Cidades(3129657, '3 - Sudeste', 'MG', 'Ibiracatu', '0055'));
    l.add(new Cidades(3129707, '3 - Sudeste', 'MG', 'Ibiraci', '0055'));
    l.add(new Cidades(3202504, '3 - Sudeste', 'ES', 'Ibiracu', '0055'));
    l.add(new Cidades(4309902, '4 - Sul', 'RS', 'Ibiraiaras', '0055'));
    l.add(new Cidades(2606705, '2 - Nordeste', 'PE', 'Ibirajuba', '0055'));
    l.add(new Cidades(4206900, '4 - Sul', 'SC', 'Ibirama', '0055'));
    l.add(new Cidades(2912707, '2 - Nordeste', 'BA', 'Ibirapitanga', '0055'));
    l.add(new Cidades(2912806, '2 - Nordeste', 'BA', 'Ibirapua', '0055'));
    l.add(new Cidades(4309951, '4 - Sul', 'RS', 'Ibirapuita', '0055'));
    l.add(new Cidades(3519501, '3 - Sudeste', 'SP', 'Ibirarema', '0055'));
    l.add(new Cidades(2912905, '2 - Nordeste', 'BA', 'Ibirataia', '0055'));
    l.add(new Cidades(3129806, '3 - Sudeste', 'MG', 'Ibirite', '0055'));
    l.add(new Cidades(4310009, '4 - Sul', 'RS', 'Ibiruba', '0055'));
    l.add(new Cidades(2913002, '2 - Nordeste', 'BA', 'Ibitiara', '0055'));
    l.add(new Cidades(3519600, '3 - Sudeste', 'SP', 'Ibitinga', '0055'));
    l.add(new Cidades(3202553, '3 - Sudeste', 'ES', 'Ibitirama', '0055'));
    l.add(new Cidades(2913101, '2 - Nordeste', 'BA', 'Ibitita', '0055'));
    l.add(
        new Cidades(3129905, '3 - Sudeste', 'MG', 'Ibitiura De Minas', '0055'));
    l.add(new Cidades(3130002, '3 - Sudeste', 'MG', 'Ibituruna', '0055'));
    l.add(new Cidades(3519709, '3 - Sudeste', 'SP', 'Ibiuna', '0055'));
    l.add(new Cidades(2913200, '2 - Nordeste', 'BA', 'Ibotirama', '0055'));
    l.add(new Cidades(2305357, '2 - Nordeste', 'CE', 'Icapui', '0055'));
    l.add(new Cidades(4207007, '4 - Sul', 'SC', 'Icara', '0055'));
    l.add(new Cidades(3130051, '3 - Sudeste', 'MG', 'Icarai De Minas', '0055'));
    l.add(new Cidades(4109906, '4 - Sul', 'PR', 'Icaraima', '0055'));
    l.add(new Cidades(2105104, '2 - Nordeste', 'MA', 'Icatu', '0055'));
    l.add(new Cidades(3519808, '3 - Sudeste', 'SP', 'Icem', '0055'));
    l.add(new Cidades(2913309, '2 - Nordeste', 'BA', 'Ichu', '0055'));
    l.add(new Cidades(2305407, '2 - Nordeste', 'CE', 'Ico', '0055'));
    l.add(new Cidades(3202603, '3 - Sudeste', 'ES', 'Iconha', '0055'));
    l.add(new Cidades(2404606, '2 - Nordeste', 'RN', 'Ielmo Marinho', '0055'));
    l.add(new Cidades(3519907, '3 - Sudeste', 'SP', 'Iepe', '0055'));
    l.add(new Cidades(2703106, '2 - Nordeste', 'AL', 'Igaci', '0055'));
    l.add(new Cidades(2913408, '2 - Nordeste', 'BA', 'Igapora', '0055'));
    l.add(
        new Cidades(3520004, '3 - Sudeste', 'SP', 'Igaracu Do Tiete', '0055'));
    l.add(new Cidades(2502607, '2 - Nordeste', 'PB', 'Igaracy', '0055'));
    l.add(new Cidades(3520103, '3 - Sudeste', 'SP', 'Igarapava', '0055'));
    l.add(new Cidades(3130101, '3 - Sudeste', 'MG', 'Igarape', '0055'));
    l.add(
        new Cidades(2105153, '2 - Nordeste', 'MA', 'Igarape Do Meio', '0055'));
    l.add(new Cidades(2105203, '2 - Nordeste', 'MA', 'Igarape Grande', '0055'));
    l.add(new Cidades(1503200, '1 - Norte', 'PA', 'Igarape-Acu', '0055'));
    l.add(new Cidades(1503309, '1 - Norte', 'PA', 'Igarape-Miri', '0055'));
    l.add(new Cidades(2606804, '2 - Nordeste', 'PE', 'Igarassu', '0055'));
    l.add(new Cidades(3520202, '3 - Sudeste', 'SP', 'Igarata', '0055'));
    l.add(new Cidades(3130200, '3 - Sudeste', 'MG', 'Igaratinga', '0055'));
    l.add(new Cidades(2913457, '2 - Nordeste', 'BA', 'Igrapiuna', '0055'));
    l.add(new Cidades(2703205, '2 - Nordeste', 'AL', 'Igreja Nova', '0055'));
    l.add(new Cidades(4310108, '4 - Sul', 'RS', 'Igrejinha', '0055'));
    l.add(new Cidades(3301876, '3 - Sudeste', 'RJ', 'Iguaba Grande', '0055'));
    l.add(new Cidades(2913507, '2 - Nordeste', 'BA', 'Iguai', '0055'));
    l.add(new Cidades(3520301, '3 - Sudeste', 'SP', 'Iguape', '0055'));
    l.add(new Cidades(2606903, '2 - Nordeste', 'PE', 'Iguaraci', '0055'));
    l.add(new Cidades(4110003, '4 - Sul', 'PR', 'Iguaracu', '0055'));
    l.add(new Cidades(3130309, '3 - Sudeste', 'MG', 'Iguatama', '0055'));
    l.add(new Cidades(5004304, '5 - Centro-Oeste', 'MS', 'Iguatemi', '0055'));
    l.add(new Cidades(2305506, '2 - Nordeste', 'CE', 'Iguatu', '0055'));
    l.add(new Cidades(4110052, '4 - Sul', 'PR', 'Iguatu', '0055'));
    l.add(new Cidades(3130408, '3 - Sudeste', 'MG', 'Ijaci', '0055'));
    l.add(new Cidades(4310207, '4 - Sul', 'RS', 'Ijui', '0055'));
    l.add(new Cidades(3520426, '3 - Sudeste', 'SP', 'Ilha Comprida', '0055'));
    l.add(
        new Cidades(2802700, '2 - Nordeste', 'SE', 'Ilha Das Flores', '0055'));
    l.add(new Cidades(
        2607604, '2 - Nordeste', 'PE', 'Ilha De Itamaraca', '0055'));
    l.add(new Cidades(2204659, '2 - Nordeste', 'PI', 'Ilha Grande', '0055'));
    l.add(new Cidades(3520442, '3 - Sudeste', 'SP', 'Ilha Solteira', '0055'));
    l.add(new Cidades(3520400, '3 - Sudeste', 'SP', 'Ilhabela', '0055'));
    l.add(new Cidades(2913606, '2 - Nordeste', 'BA', 'Ilheus', '0055'));
    l.add(new Cidades(4207106, '4 - Sul', 'SC', 'Ilhota', '0055'));
    l.add(new Cidades(3130507, '3 - Sudeste', 'MG', 'Ilicinea', '0055'));
    l.add(new Cidades(4310306, '4 - Sul', 'RS', 'Ilopolis', '0055'));
    l.add(new Cidades(2506707, '2 - Nordeste', 'PB', 'Imaculada', '0055'));
    l.add(new Cidades(4207205, '4 - Sul', 'SC', 'Imarui', '0055'));
    l.add(new Cidades(4110078, '4 - Sul', 'PR', 'Imbau', '0055'));
    l.add(new Cidades(4310330, '4 - Sul', 'RS', 'Imbe', '0055'));
    l.add(new Cidades(3130556, '3 - Sudeste', 'MG', 'Imbe De Minas', '0055'));
    l.add(new Cidades(4207304, '4 - Sul', 'SC', 'Imbituba', '0055'));
    l.add(new Cidades(4110102, '4 - Sul', 'PR', 'Imbituva', '0055'));
    l.add(new Cidades(4207403, '4 - Sul', 'SC', 'Imbuia', '0055'));
    l.add(new Cidades(4310363, '4 - Sul', 'RS', 'Imigrante', '0055'));
    l.add(new Cidades(2105302, '2 - Nordeste', 'MA', 'Imperatriz', '0055'));
    l.add(new Cidades(4110201, '4 - Sul', 'PR', 'Inacio Martins', '0055'));
    l.add(
        new Cidades(5209937, '5 - Centro-Oeste', 'GO', 'Inaciolandia', '0055'));
    l.add(new Cidades(2607000, '2 - Nordeste', 'PE', 'Inaja', '0055'));
    l.add(new Cidades(4110300, '4 - Sul', 'PR', 'Inaja', '0055'));
    l.add(new Cidades(3130606, '3 - Sudeste', 'MG', 'Inconfidentes', '0055'));
    l.add(new Cidades(3130655, '3 - Sudeste', 'MG', 'Indaiabira', '0055'));
    l.add(new Cidades(4207502, '4 - Sul', 'SC', 'Indaial', '0055'));
    l.add(new Cidades(3520509, '3 - Sudeste', 'SP', 'Indaiatuba', '0055'));
    l.add(new Cidades(2305605, '2 - Nordeste', 'CE', 'Independencia', '0055'));
    l.add(new Cidades(4310405, '4 - Sul', 'RS', 'Independencia', '0055'));
    l.add(new Cidades(3520608, '3 - Sudeste', 'SP', 'Indiana', '0055'));
    l.add(new Cidades(3130705, '3 - Sudeste', 'MG', 'Indianopolis', '0055'));
    l.add(new Cidades(4110409, '4 - Sul', 'PR', 'Indianopolis', '0055'));
    l.add(new Cidades(3520707, '3 - Sudeste', 'SP', 'Indiapora', '0055'));
    l.add(new Cidades(5209952, '5 - Centro-Oeste', 'GO', 'Indiara', '0055'));
    l.add(new Cidades(2802809, '2 - Nordeste', 'SE', 'Indiaroba', '0055'));
    l.add(new Cidades(5104500, '5 - Centro-Oeste', 'MT', 'Indiavai', '0055'));
    l.add(new Cidades(2506806, '2 - Nordeste', 'PB', 'Inga', '0055'));
    l.add(new Cidades(3130804, '3 - Sudeste', 'MG', 'Ingai', '0055'));
    l.add(new Cidades(2607109, '2 - Nordeste', 'PE', 'Ingazeira', '0055'));
    l.add(new Cidades(4310413, '4 - Sul', 'RS', 'Inhacora', '0055'));
    l.add(new Cidades(2913705, '2 - Nordeste', 'BA', 'Inhambupe', '0055'));
    l.add(new Cidades(1503408, '1 - Norte', 'PA', 'Inhangapi', '0055'));
    l.add(new Cidades(2703304, '2 - Nordeste', 'AL', 'Inhapi', '0055'));
    l.add(new Cidades(3130903, '3 - Sudeste', 'MG', 'Inhapim', '0055'));
    l.add(new Cidades(3131000, '3 - Sudeste', 'MG', 'Inhauma', '0055'));
    l.add(new Cidades(2204709, '2 - Nordeste', 'PI', 'Inhuma', '0055'));
    l.add(new Cidades(5210000, '5 - Centro-Oeste', 'GO', 'Inhumas', '0055'));
    l.add(new Cidades(3131109, '3 - Sudeste', 'MG', 'Inimutaba', '0055'));
    l.add(new Cidades(5004403, '5 - Centro-Oeste', 'MS', 'Inocencia', '0055'));
    l.add(new Cidades(3520806, '3 - Sudeste', 'SP', 'Inubia Paulista', '0055'));
    l.add(new Cidades(4207577, '4 - Sul', 'SC', 'Iomere', '0055'));
    l.add(new Cidades(3131158, '3 - Sudeste', 'MG', 'Ipaba', '0055'));
    l.add(new Cidades(5210109, '5 - Centro-Oeste', 'GO', 'Ipameri', '0055'));
    l.add(new Cidades(3131208, '3 - Sudeste', 'MG', 'Ipanema', '0055'));
    l.add(new Cidades(2404705, '2 - Nordeste', 'RN', 'Ipanguacu', '0055'));
    l.add(new Cidades(2305654, '2 - Nordeste', 'CE', 'Ipaporanga', '0055'));
    l.add(new Cidades(3131307, '3 - Sudeste', 'MG', 'Ipatinga', '0055'));
    l.add(new Cidades(2305704, '2 - Nordeste', 'CE', 'Ipaumirim', '0055'));
    l.add(new Cidades(3520905, '3 - Sudeste', 'SP', 'Ipaussu', '0055'));
    l.add(new Cidades(4310439, '4 - Sul', 'RS', 'Ipe', '0055'));
    l.add(new Cidades(2913804, '2 - Nordeste', 'BA', 'Ipecaeta', '0055'));
    l.add(new Cidades(3521002, '3 - Sudeste', 'SP', 'Ipero', '0055'));
    l.add(new Cidades(3521101, '3 - Sudeste', 'SP', 'Ipeuna', '0055'));
    l.add(new Cidades(3131406, '3 - Sudeste', 'MG', 'Ipiacu', '0055'));
    l.add(new Cidades(2913903, '2 - Nordeste', 'BA', 'Ipiau', '0055'));
    l.add(new Cidades(3521150, '3 - Sudeste', 'SP', 'Ipigua', '0055'));
    l.add(new Cidades(2914000, '2 - Nordeste', 'BA', 'Ipira', '0055'));
    l.add(new Cidades(4207601, '4 - Sul', 'SC', 'Ipira', '0055'));
    l.add(new Cidades(4110508, '4 - Sul', 'PR', 'Ipiranga', '0055'));
    l.add(new Cidades(
        5210158, '5 - Centro-Oeste', 'GO', 'Ipiranga De Goias', '0055'));
    l.add(new Cidades(
        5104526, '5 - Centro-Oeste', 'MT', 'Ipiranga Do Norte', '0055'));
    l.add(new Cidades(
        2204808, '2 - Nordeste', 'PI', 'Ipiranga Do Piaui', '0055'));
    l.add(new Cidades(4310462, '4 - Sul', 'RS', 'Ipiranga Do Sul', '0055'));
    l.add(new Cidades(1301803, '1 - Norte', 'AM', 'Ipixuna', '0055'));
    l.add(new Cidades(1503457, '1 - Norte', 'PA', 'Ipixuna Do Para', '0055'));
    l.add(new Cidades(2607208, '2 - Nordeste', 'PE', 'Ipojuca', '0055'));
    l.add(new Cidades(4110607, '4 - Sul', 'PR', 'Ipora', '0055'));
    l.add(new Cidades(5210208, '5 - Centro-Oeste', 'GO', 'Ipora', '0055'));
    l.add(new Cidades(4207650, '4 - Sul', 'SC', 'Ipora Do Oeste', '0055'));
    l.add(new Cidades(3521200, '3 - Sudeste', 'SP', 'Iporanga', '0055'));
    l.add(new Cidades(2305803, '2 - Nordeste', 'CE', 'Ipu', '0055'));
    l.add(new Cidades(3521309, '3 - Sudeste', 'SP', 'Ipua', '0055'));
    l.add(new Cidades(4207684, '4 - Sul', 'SC', 'Ipuacu', '0055'));
    l.add(new Cidades(2607307, '2 - Nordeste', 'PE', 'Ipubi', '0055'));
    l.add(new Cidades(2404804, '2 - Nordeste', 'RN', 'Ipueira', '0055'));
    l.add(new Cidades(1709807, '1 - Norte', 'TO', 'Ipueiras', '0055'));
    l.add(new Cidades(2305902, '2 - Nordeste', 'CE', 'Ipueiras', '0055'));
    l.add(new Cidades(3131505, '3 - Sudeste', 'MG', 'Ipuiuna', '0055'));
    l.add(new Cidades(4207700, '4 - Sul', 'SC', 'Ipumirim', '0055'));
    l.add(new Cidades(2914109, '2 - Nordeste', 'BA', 'Ipupiara', '0055'));
    l.add(new Cidades(1400282, '1 - Norte', 'RR', 'Iracema', '0055'));
    l.add(new Cidades(2306009, '2 - Nordeste', 'CE', 'Iracema', '0055'));
    l.add(new Cidades(4110656, '4 - Sul', 'PR', 'Iracema Do Oeste', '0055'));
    l.add(new Cidades(3521408, '3 - Sudeste', 'SP', 'Iracemapolis', '0055'));
    l.add(new Cidades(4207759, '4 - Sul', 'SC', 'Iraceminha', '0055'));
    l.add(new Cidades(4310504, '4 - Sul', 'RS', 'Irai', '0055'));
    l.add(new Cidades(3131604, '3 - Sudeste', 'MG', 'Irai De Minas', '0055'));
    l.add(new Cidades(2914208, '2 - Nordeste', 'BA', 'Irajuba', '0055'));
    l.add(new Cidades(2914307, '2 - Nordeste', 'BA', 'Iramaia', '0055'));
    l.add(new Cidades(1301852, '1 - Norte', 'AM', 'Iranduba', '0055'));
    l.add(new Cidades(4207809, '4 - Sul', 'SC', 'Irani', '0055'));
    l.add(new Cidades(3521507, '3 - Sudeste', 'SP', 'Irapua', '0055'));
    l.add(new Cidades(3521606, '3 - Sudeste', 'SP', 'Irapuru', '0055'));
    l.add(new Cidades(2914406, '2 - Nordeste', 'BA', 'Iraquara', '0055'));
    l.add(new Cidades(2914505, '2 - Nordeste', 'BA', 'Irara', '0055'));
    l.add(new Cidades(4110706, '4 - Sul', 'PR', 'Irati', '0055'));
    l.add(new Cidades(4207858, '4 - Sul', 'SC', 'Irati', '0055'));
    l.add(new Cidades(2306108, '2 - Nordeste', 'CE', 'Iraucuba', '0055'));
    l.add(new Cidades(2914604, '2 - Nordeste', 'BA', 'Irece', '0055'));
    l.add(new Cidades(4110805, '4 - Sul', 'PR', 'Iretama', '0055'));
    l.add(new Cidades(4207908, '4 - Sul', 'SC', 'Irineopolis', '0055'));
    l.add(new Cidades(1503507, '1 - Norte', 'PA', 'Irituia', '0055'));
    l.add(new Cidades(3202652, '3 - Sudeste', 'ES', 'Irupi', '0055'));
    l.add(new Cidades(2204907, '2 - Nordeste', 'PI', 'Isaias Coelho', '0055'));
    l.add(
        new Cidades(5210307, '5 - Centro-Oeste', 'GO', 'Israelandia', '0055'));
    l.add(new Cidades(4208005, '4 - Sul', 'SC', 'Ita', '0055'));
    l.add(new Cidades(4310538, '4 - Sul', 'RS', 'Itaara', '0055'));
    l.add(new Cidades(2506905, '2 - Nordeste', 'PB', 'Itabaiana', '0055'));
    l.add(new Cidades(2802908, '2 - Nordeste', 'SE', 'Itabaiana', '0055'));
    l.add(new Cidades(2803005, '2 - Nordeste', 'SE', 'Itabaianinha', '0055'));
    l.add(new Cidades(2914653, '2 - Nordeste', 'BA', 'Itabela', '0055'));
    l.add(new Cidades(3521705, '3 - Sudeste', 'SP', 'Itabera', '0055'));
    l.add(new Cidades(2914703, '2 - Nordeste', 'BA', 'Itaberaba', '0055'));
    l.add(new Cidades(5210406, '5 - Centro-Oeste', 'GO', 'Itaberai', '0055'));
    l.add(new Cidades(2803104, '2 - Nordeste', 'SE', 'Itabi', '0055'));
    l.add(new Cidades(3131703, '3 - Sudeste', 'MG', 'Itabira', '0055'));
    l.add(new Cidades(3131802, '3 - Sudeste', 'MG', 'Itabirinha', '0055'));
    l.add(new Cidades(3131901, '3 - Sudeste', 'MG', 'Itabirito', '0055'));
    l.add(new Cidades(3301900, '3 - Sudeste', 'RJ', 'Itaborai', '0055'));
    l.add(new Cidades(2914802, '2 - Nordeste', 'BA', 'Itabuna', '0055'));
    l.add(new Cidades(1710508, '1 - Norte', 'TO', 'Itacaja', '0055'));
    l.add(new Cidades(3132008, '3 - Sudeste', 'MG', 'Itacambira', '0055'));
    l.add(new Cidades(3132107, '3 - Sudeste', 'MG', 'Itacarambi', '0055'));
    l.add(new Cidades(2914901, '2 - Nordeste', 'BA', 'Itacare', '0055'));
    l.add(new Cidades(1301902, '1 - Norte', 'AM', 'Itacoatiara', '0055'));
    l.add(new Cidades(2607406, '2 - Nordeste', 'PE', 'Itacuruba', '0055'));
    l.add(new Cidades(4310553, '4 - Sul', 'RS', 'Itacurubi', '0055'));
    l.add(new Cidades(2915007, '2 - Nordeste', 'BA', 'Itaete', '0055'));
    l.add(new Cidades(2915106, '2 - Nordeste', 'BA', 'Itagi', '0055'));
    l.add(new Cidades(2915205, '2 - Nordeste', 'BA', 'Itagiba', '0055'));
    l.add(new Cidades(2915304, '2 - Nordeste', 'BA', 'Itagimirim', '0055'));
    l.add(new Cidades(3202702, '3 - Sudeste', 'ES', 'Itaguacu', '0055'));
    l.add(new Cidades(2915353, '2 - Nordeste', 'BA', 'Itaguacu Da Ba', '0055'));
    l.add(new Cidades(3302007, '3 - Sudeste', 'RJ', 'Itaguai', '0055'));
    l.add(new Cidades(4110904, '4 - Sul', 'PR', 'Itaguaje', '0055'));
    l.add(new Cidades(3132206, '3 - Sudeste', 'MG', 'Itaguara', '0055'));
    l.add(new Cidades(5210562, '5 - Centro-Oeste', 'GO', 'Itaguari', '0055'));
    l.add(new Cidades(5210604, '5 - Centro-Oeste', 'GO', 'Itaguaru', '0055'));
    l.add(new Cidades(1710706, '1 - Norte', 'TO', 'Itaguatins', '0055'));
    l.add(new Cidades(3521804, '3 - Sudeste', 'SP', 'Itai', '0055'));
    l.add(new Cidades(2607505, '2 - Nordeste', 'PE', 'Itaiba', '0055'));
    l.add(new Cidades(2306207, '2 - Nordeste', 'CE', 'Itaicaba', '0055'));
    l.add(new Cidades(2205003, '2 - Nordeste', 'PI', 'Itainopolis', '0055'));
    l.add(new Cidades(4208104, '4 - Sul', 'SC', 'Itaiopolis', '0055'));
    l.add(new Cidades(
        2105351, '2 - Nordeste', 'MA', 'Itaipava Do Grajau', '0055'));
    l.add(new Cidades(3132305, '3 - Sudeste', 'MG', 'Itaipe', '0055'));
    l.add(new Cidades(4110953, '4 - Sul', 'PR', 'Itaipulandia', '0055'));
    l.add(new Cidades(2306256, '2 - Nordeste', 'CE', 'Itaitinga', '0055'));
    l.add(new Cidades(1503606, '1 - Norte', 'PA', 'Itaituba', '0055'));
    l.add(new Cidades(2404853, '2 - Nordeste', 'RN', 'Itaja', '0055'));
    l.add(new Cidades(5210802, '5 - Centro-Oeste', 'GO', 'Itaja', '0055'));
    l.add(new Cidades(4208203, '4 - Sul', 'SC', 'Itajai', '0055'));
    l.add(new Cidades(3521903, '3 - Sudeste', 'SP', 'Itajobi', '0055'));
    l.add(new Cidades(3522000, '3 - Sudeste', 'SP', 'Itaju', '0055'));
    l.add(
        new Cidades(2915403, '2 - Nordeste', 'BA', 'Itaju Do Colonia', '0055'));
    l.add(new Cidades(3132404, '3 - Sudeste', 'MG', 'Itajuba', '0055'));
    l.add(new Cidades(2915502, '2 - Nordeste', 'BA', 'Itajuipe', '0055'));
    l.add(new Cidades(3302056, '3 - Sudeste', 'RJ', 'Italva', '0055'));
    l.add(new Cidades(2915601, '2 - Nordeste', 'BA', 'Itamaraju', '0055'));
    l.add(new Cidades(3132503, '3 - Sudeste', 'MG', 'Itamarandiba', '0055'));
    l.add(new Cidades(1301951, '1 - Norte', 'AM', 'Itamarati', '0055'));
    l.add(new Cidades(
        3132602, '3 - Sudeste', 'MG', 'Itamarati De Minas', '0055'));
    l.add(new Cidades(2915700, '2 - Nordeste', 'BA', 'Itamari', '0055'));
    l.add(new Cidades(3132701, '3 - Sudeste', 'MG', 'Itambacuri', '0055'));
    l.add(new Cidades(4111001, '4 - Sul', 'PR', 'Itambaraca', '0055'));
    l.add(new Cidades(2607653, '2 - Nordeste', 'PE', 'Itambe', '0055'));
    l.add(new Cidades(2915809, '2 - Nordeste', 'BA', 'Itambe', '0055'));
    l.add(new Cidades(4111100, '4 - Sul', 'PR', 'Itambe', '0055'));
    l.add(new Cidades(
        3132800, '3 - Sudeste', 'MG', 'Itambe Do Mato Dentro', '0055'));
    l.add(new Cidades(3132909, '3 - Sudeste', 'MG', 'Itamogi', '0055'));
    l.add(new Cidades(3133006, '3 - Sudeste', 'MG', 'Itamonte', '0055'));
    l.add(new Cidades(2915908, '2 - Nordeste', 'BA', 'Itanagra', '0055'));
    l.add(new Cidades(3522109, '3 - Sudeste', 'SP', 'Itanhaem', '0055'));
    l.add(new Cidades(3133105, '3 - Sudeste', 'MG', 'Itanhandu', '0055'));
    l.add(new Cidades(5104542, '5 - Centro-Oeste', 'MT', 'Itanhanga', '0055'));
    l.add(new Cidades(2916005, '2 - Nordeste', 'BA', 'Itanhem', '0055'));
    l.add(new Cidades(3133204, '3 - Sudeste', 'MG', 'Itanhomi', '0055'));
    l.add(new Cidades(3133303, '3 - Sudeste', 'MG', 'Itaobim', '0055'));
    l.add(new Cidades(3522158, '3 - Sudeste', 'SP', 'Itaoca', '0055'));
    l.add(new Cidades(3302106, '3 - Sudeste', 'RJ', 'Itaocara', '0055'));
    l.add(new Cidades(5210901, '5 - Centro-Oeste', 'GO', 'Itapaci', '0055'));
    l.add(new Cidades(2306306, '2 - Nordeste', 'CE', 'Itapage', '0055'));
    l.add(new Cidades(3133402, '3 - Sudeste', 'MG', 'Itapagipe', '0055'));
    l.add(new Cidades(2916104, '2 - Nordeste', 'BA', 'Itaparica', '0055'));
    l.add(new Cidades(2916203, '2 - Nordeste', 'BA', 'Itape', '0055'));
    l.add(new Cidades(2916302, '2 - Nordeste', 'BA', 'Itapebi', '0055'));
    l.add(new Cidades(3133501, '3 - Sudeste', 'MG', 'Itapecerica', '0055'));
    l.add(new Cidades(
        3522208, '3 - Sudeste', 'SP', 'Itapecerica Da Serra', '0055'));
    l.add(
        new Cidades(2105401, '2 - Nordeste', 'MA', 'Itapecuru Mirim', '0055'));
    l.add(new Cidades(4111209, '4 - Sul', 'PR', 'Itapejara D`Oeste', '0055'));
    l.add(new Cidades(4208302, '4 - Sul', 'SC', 'Itapema', '0055'));
    l.add(new Cidades(3202801, '3 - Sudeste', 'ES', 'Itapemirim', '0055'));
    l.add(new Cidades(4111258, '4 - Sul', 'PR', 'Itaperucu', '0055'));
    l.add(new Cidades(3302205, '3 - Sudeste', 'RJ', 'Itaperuna', '0055'));
    l.add(new Cidades(2607703, '2 - Nordeste', 'PE', 'Itapetim', '0055'));
    l.add(new Cidades(2916401, '2 - Nordeste', 'BA', 'Itapetinga', '0055'));
    l.add(new Cidades(3522307, '3 - Sudeste', 'SP', 'Itapetininga', '0055'));
    l.add(new Cidades(3133600, '3 - Sudeste', 'MG', 'Itapeva', '0055'));
    l.add(new Cidades(3522406, '3 - Sudeste', 'SP', 'Itapeva', '0055'));
    l.add(new Cidades(3522505, '3 - Sudeste', 'SP', 'Itapevi', '0055'));
    l.add(new Cidades(2916500, '2 - Nordeste', 'BA', 'Itapicuru', '0055'));
    l.add(new Cidades(2306405, '2 - Nordeste', 'CE', 'Itapipoca', '0055'));
    l.add(new Cidades(3522604, '3 - Sudeste', 'SP', 'Itapira', '0055'));
    l.add(new Cidades(1302009, '1 - Norte', 'AM', 'Itapiranga', '0055'));
    l.add(new Cidades(4208401, '4 - Sul', 'SC', 'Itapiranga', '0055'));
    l.add(new Cidades(5211008, '5 - Centro-Oeste', 'GO', 'Itapirapua', '0055'));
    l.add(new Cidades(
        3522653, '3 - Sudeste', 'SP', 'Itapirapua Paulista', '0055'));
    l.add(new Cidades(1710904, '1 - Norte', 'TO', 'Itapiratins', '0055'));
    l.add(new Cidades(2607752, '2 - Nordeste', 'PE', 'Itapissuma', '0055'));
    l.add(new Cidades(2916609, '2 - Nordeste', 'BA', 'Itapitanga', '0055'));
    l.add(new Cidades(2306504, '2 - Nordeste', 'CE', 'Itapiuna', '0055'));
    l.add(new Cidades(4208450, '4 - Sul', 'SC', 'Itapoa', '0055'));
    l.add(new Cidades(3522703, '3 - Sudeste', 'SP', 'Itapolis', '0055'));
    l.add(new Cidades(5004502, '5 - Centro-Oeste', 'MS', 'Itapora', '0055'));
    l.add(new Cidades(1711100, '1 - Norte', 'TO', 'Itapora Do To', '0055'));
    l.add(new Cidades(2507002, '2 - Nordeste', 'PB', 'Itaporanga', '0055'));
    l.add(new Cidades(3522802, '3 - Sudeste', 'SP', 'Itaporanga', '0055'));
    l.add(new Cidades(
        2803203, '2 - Nordeste', 'SE', 'Itaporanga D`Ajuda', '0055'));
    l.add(new Cidades(2507101, '2 - Nordeste', 'PB', 'Itapororoca', '0055'));
    l.add(new Cidades(1101104, '1 - Norte', 'RO', 'Itapua Do Oeste', '0055'));
    l.add(new Cidades(4310579, '4 - Sul', 'RS', 'Itapuca', '0055'));
    l.add(new Cidades(3522901, '3 - Sudeste', 'SP', 'Itapui', '0055'));
    l.add(new Cidades(3523008, '3 - Sudeste', 'SP', 'Itapura', '0055'));
    l.add(new Cidades(5211206, '5 - Centro-Oeste', 'GO', 'Itapuranga', '0055'));
    l.add(new Cidades(3523107, '3 - Sudeste', 'SP', 'Itaquaquecetuba', '0055'));
    l.add(new Cidades(2916708, '2 - Nordeste', 'BA', 'Itaquara', '0055'));
    l.add(new Cidades(4310603, '4 - Sul', 'RS', 'Itaqui', '0055'));
    l.add(new Cidades(5004601, '5 - Centro-Oeste', 'MS', 'Itaquirai', '0055'));
    l.add(new Cidades(2607802, '2 - Nordeste', 'PE', 'Itaquitinga', '0055'));
    l.add(new Cidades(3202900, '3 - Sudeste', 'ES', 'Itarana', '0055'));
    l.add(new Cidades(2916807, '2 - Nordeste', 'BA', 'Itarantim', '0055'));
    l.add(new Cidades(3523206, '3 - Sudeste', 'SP', 'Itarare', '0055'));
    l.add(new Cidades(2306553, '2 - Nordeste', 'CE', 'Itarema', '0055'));
    l.add(new Cidades(3523305, '3 - Sudeste', 'SP', 'Itariri', '0055'));
    l.add(new Cidades(5211305, '5 - Centro-Oeste', 'GO', 'Itaruma', '0055'));
    l.add(new Cidades(4310652, '4 - Sul', 'RS', 'Itati', '0055'));
    l.add(new Cidades(3302254, '3 - Sudeste', 'RJ', 'Itatiaia', '0055'));
    l.add(new Cidades(3133709, '3 - Sudeste', 'MG', 'Itatiaiucu', '0055'));
    l.add(new Cidades(3523404, '3 - Sudeste', 'SP', 'Itatiba', '0055'));
    l.add(new Cidades(4310702, '4 - Sul', 'RS', 'Itatiba Do Sul', '0055'));
    l.add(new Cidades(2916856, '2 - Nordeste', 'BA', 'Itatim', '0055'));
    l.add(new Cidades(3523503, '3 - Sudeste', 'SP', 'Itatinga', '0055'));
    l.add(new Cidades(2306603, '2 - Nordeste', 'CE', 'Itatira', '0055'));
    l.add(new Cidades(2507200, '2 - Nordeste', 'PB', 'Itatuba', '0055'));
    l.add(new Cidades(2404903, '2 - Nordeste', 'RN', 'Itau', '0055'));
    l.add(new Cidades(3133758, '3 - Sudeste', 'MG', 'Itau De Minas', '0055'));
    l.add(new Cidades(5104559, '5 - Centro-Oeste', 'MT', 'Itauba', '0055'));
    l.add(new Cidades(1600253, '1 - Norte', 'AP', 'Itaubal', '0055'));
    l.add(new Cidades(5211404, '5 - Centro-Oeste', 'GO', 'Itaucu', '0055'));
    l.add(new Cidades(2205102, '2 - Nordeste', 'PI', 'Itaueira', '0055'));
    l.add(new Cidades(3133808, '3 - Sudeste', 'MG', 'Itauna', '0055'));
    l.add(new Cidades(4111308, '4 - Sul', 'PR', 'Itauna Do Sul', '0055'));
    l.add(new Cidades(3133907, '3 - Sudeste', 'MG', 'Itaverava', '0055'));
    l.add(new Cidades(3134004, '3 - Sudeste', 'MG', 'Itinga', '0055'));
    l.add(new Cidades(
        2105427, '2 - Nordeste', 'MA', 'Itinga Do Maranhao', '0055'));
    l.add(new Cidades(5104609, '5 - Centro-Oeste', 'MT', 'Itiquira', '0055'));
    l.add(new Cidades(3523602, '3 - Sudeste', 'SP', 'Itirapina', '0055'));
    l.add(new Cidades(3523701, '3 - Sudeste', 'SP', 'Itirapua', '0055'));
    l.add(new Cidades(2916906, '2 - Nordeste', 'BA', 'Itirucu', '0055'));
    l.add(new Cidades(2917003, '2 - Nordeste', 'BA', 'Itiuba', '0055'));
    l.add(new Cidades(3523800, '3 - Sudeste', 'SP', 'Itobi', '0055'));
    l.add(new Cidades(2917102, '2 - Nordeste', 'BA', 'Itororo', '0055'));
    l.add(new Cidades(3523909, '3 - Sudeste', 'SP', 'Itu', '0055'));
    l.add(new Cidades(2917201, '2 - Nordeste', 'BA', 'Ituacu', '0055'));
    l.add(new Cidades(2917300, '2 - Nordeste', 'BA', 'Itubera', '0055'));
    l.add(new Cidades(3134103, '3 - Sudeste', 'MG', 'Itueta', '0055'));
    l.add(new Cidades(3134202, '3 - Sudeste', 'MG', 'Ituiutaba', '0055'));
    l.add(new Cidades(5211503, '5 - Centro-Oeste', 'GO', 'Itumbiara', '0055'));
    l.add(new Cidades(3134301, '3 - Sudeste', 'MG', 'Itumirim', '0055'));
    l.add(new Cidades(3524006, '3 - Sudeste', 'SP', 'Itupeva', '0055'));
    l.add(new Cidades(1503705, '1 - Norte', 'PA', 'Itupiranga', '0055'));
    l.add(new Cidades(4208500, '4 - Sul', 'SC', 'Ituporanga', '0055'));
    l.add(new Cidades(3134400, '3 - Sudeste', 'MG', 'Iturama', '0055'));
    l.add(new Cidades(3134509, '3 - Sudeste', 'MG', 'Itutinga', '0055'));
    l.add(new Cidades(3524105, '3 - Sudeste', 'SP', 'Ituverava', '0055'));
    l.add(new Cidades(2917334, '2 - Nordeste', 'BA', 'Iuiu', '0055'));
    l.add(new Cidades(3203007, '3 - Sudeste', 'ES', 'Iuna', '0055'));
    l.add(new Cidades(4111407, '4 - Sul', 'PR', 'Ivai', '0055'));
    l.add(new Cidades(4111506, '4 - Sul', 'PR', 'Ivaipora', '0055'));
    l.add(new Cidades(4111555, '4 - Sul', 'PR', 'Ivate', '0055'));
    l.add(new Cidades(4111605, '4 - Sul', 'PR', 'Ivatuba', '0055'));
    l.add(new Cidades(5004700, '5 - Centro-Oeste', 'MS', 'Ivinhema', '0055'));
    l.add(new Cidades(5211602, '5 - Centro-Oeste', 'GO', 'Ivolandia', '0055'));
    l.add(new Cidades(4310751, '4 - Sul', 'RS', 'Ivora', '0055'));
    l.add(new Cidades(4310801, '4 - Sul', 'RS', 'Ivoti', '0055'));
    l.add(new Cidades(
        2607901, '2 - Nordeste', 'PE', 'Jaboatao Dos Guararapes', '0055'));
    l.add(new Cidades(4208609, '4 - Sul', 'SC', 'Jabora', '0055'));
    l.add(new Cidades(2917359, '2 - Nordeste', 'BA', 'Jaborandi', '0055'));
    l.add(new Cidades(3524204, '3 - Sudeste', 'SP', 'Jaborandi', '0055'));
    l.add(new Cidades(4111704, '4 - Sul', 'PR', 'Jaboti', '0055'));
    l.add(new Cidades(4310850, '4 - Sul', 'RS', 'Jaboticaba', '0055'));
    l.add(new Cidades(3524303, '3 - Sudeste', 'SP', 'Jaboticabal', '0055'));
    l.add(new Cidades(3134608, '3 - Sudeste', 'MG', 'Jaboticatubas', '0055'));
    l.add(new Cidades(2405009, '2 - Nordeste', 'RN', 'Jacana', '0055'));
    l.add(new Cidades(2917409, '2 - Nordeste', 'BA', 'Jacaraci', '0055'));
    l.add(new Cidades(2507309, '2 - Nordeste', 'PB', 'Jacarau', '0055'));
    l.add(new Cidades(
        2703403, '2 - Nordeste', 'AL', 'Jacare Dos Homens', '0055'));
    l.add(new Cidades(1503754, '1 - Norte', 'PA', 'Jacareacanga', '0055'));
    l.add(new Cidades(3524402, '3 - Sudeste', 'SP', 'Jacarei', '0055'));
    l.add(new Cidades(4111803, '4 - Sul', 'PR', 'Jacarezinho', '0055'));
    l.add(new Cidades(3524501, '3 - Sudeste', 'SP', 'Jaci', '0055'));
    l.add(new Cidades(5104807, '5 - Centro-Oeste', 'MT', 'Jaciara', '0055'));
    l.add(new Cidades(3134707, '3 - Sudeste', 'MG', 'Jacinto', '0055'));
    l.add(new Cidades(4208708, '4 - Sul', 'SC', 'Jacinto Machado', '0055'));
    l.add(new Cidades(2917508, '2 - Nordeste', 'BA', 'Jacobina', '0055'));
    l.add(new Cidades(
        2205151, '2 - Nordeste', 'PI', 'Jacobina Do Piaui', '0055'));
    l.add(new Cidades(3134806, '3 - Sudeste', 'MG', 'Jacui', '0055'));
    l.add(new Cidades(2703502, '2 - Nordeste', 'AL', 'Jacuipe', '0055'));
    l.add(new Cidades(4310876, '4 - Sul', 'RS', 'Jacuizinho', '0055'));
    l.add(new Cidades(1503804, '1 - Norte', 'PA', 'Jacunda', '0055'));
    l.add(new Cidades(3524600, '3 - Sudeste', 'SP', 'Jacupiranga', '0055'));
    l.add(new Cidades(3134905, '3 - Sudeste', 'MG', 'Jacutinga', '0055'));
    l.add(new Cidades(4310900, '4 - Sul', 'RS', 'Jacutinga', '0055'));
    l.add(new Cidades(4111902, '4 - Sul', 'PR', 'Jaguapita', '0055'));
    l.add(new Cidades(2917607, '2 - Nordeste', 'BA', 'Jaguaquara', '0055'));
    l.add(new Cidades(3135001, '3 - Sudeste', 'MG', 'Jaguaracu', '0055'));
    l.add(new Cidades(4311007, '4 - Sul', 'RS', 'Jaguarao', '0055'));
    l.add(new Cidades(2917706, '2 - Nordeste', 'BA', 'Jaguarari', '0055'));
    l.add(new Cidades(3203056, '3 - Sudeste', 'ES', 'Jaguare', '0055'));
    l.add(new Cidades(2306702, '2 - Nordeste', 'CE', 'Jaguaretama', '0055'));
    l.add(new Cidades(4311106, '4 - Sul', 'RS', 'Jaguari', '0055'));
    l.add(new Cidades(4112009, '4 - Sul', 'PR', 'Jaguariaiva', '0055'));
    l.add(new Cidades(2306801, '2 - Nordeste', 'CE', 'Jaguaribara', '0055'));
    l.add(new Cidades(2306900, '2 - Nordeste', 'CE', 'Jaguaribe', '0055'));
    l.add(new Cidades(2917805, '2 - Nordeste', 'BA', 'Jaguaripe', '0055'));
    l.add(new Cidades(3524709, '3 - Sudeste', 'SP', 'Jaguariuna', '0055'));
    l.add(new Cidades(2307007, '2 - Nordeste', 'CE', 'Jaguaruana', '0055'));
    l.add(new Cidades(4208807, '4 - Sul', 'SC', 'Jaguaruna', '0055'));
    l.add(new Cidades(3135050, '3 - Sudeste', 'MG', 'Jaiba', '0055'));
    l.add(new Cidades(2205201, '2 - Nordeste', 'PI', 'Jaicos', '0055'));
    l.add(new Cidades(3524808, '3 - Sudeste', 'SP', 'Jales', '0055'));
    l.add(new Cidades(3524907, '3 - Sudeste', 'SP', 'Jambeiro', '0055'));
    l.add(new Cidades(3135076, '3 - Sudeste', 'MG', 'Jampruca', '0055'));
    l.add(new Cidades(3135100, '3 - Sudeste', 'MG', 'Janauba', '0055'));
    l.add(new Cidades(5211701, '5 - Centro-Oeste', 'GO', 'Jandaia', '0055'));
    l.add(new Cidades(4112108, '4 - Sul', 'PR', 'Jandaia Do Sul', '0055'));
    l.add(new Cidades(2405108, '2 - Nordeste', 'RN', 'Jandaira', '0055'));
    l.add(new Cidades(2917904, '2 - Nordeste', 'BA', 'Jandaira', '0055'));
    l.add(new Cidades(3525003, '3 - Sudeste', 'SP', 'Jandira', '0055'));
    l.add(new Cidades(2405207, '2 - Nordeste', 'RN', 'Janduis', '0055'));
    l.add(new Cidades(5104906, '5 - Centro-Oeste', 'MT', 'Jangada', '0055'));
    l.add(new Cidades(4112207, '4 - Sul', 'PR', 'Janiopolis', '0055'));
    l.add(new Cidades(3135209, '3 - Sudeste', 'MG', 'Januaria', '0055'));
    l.add(new Cidades(2405306, '2 - Nordeste', 'RN', 'Januario Cicco', '0055'));
    l.add(new Cidades(3135308, '3 - Sudeste', 'MG', 'Japaraiba', '0055'));
    l.add(new Cidades(2703601, '2 - Nordeste', 'AL', 'Japaratinga', '0055'));
    l.add(new Cidades(2803302, '2 - Nordeste', 'SE', 'Japaratuba', '0055'));
    l.add(new Cidades(3302270, '3 - Sudeste', 'RJ', 'Japeri', '0055'));
    l.add(new Cidades(2405405, '2 - Nordeste', 'RN', 'Japi', '0055'));
    l.add(new Cidades(4112306, '4 - Sul', 'PR', 'Japira', '0055'));
    l.add(new Cidades(2803401, '2 - Nordeste', 'SE', 'Japoata', '0055'));
    l.add(new Cidades(3135357, '3 - Sudeste', 'MG', 'Japonvar', '0055'));
    l.add(new Cidades(5004809, '5 - Centro-Oeste', 'MS', 'Japora', '0055'));
    l.add(new Cidades(1302108, '1 - Norte', 'AM', 'Japura', '0055'));
    l.add(new Cidades(4112405, '4 - Sul', 'PR', 'Japura', '0055'));
    l.add(new Cidades(2607950, '2 - Nordeste', 'PE', 'Jaqueira', '0055'));
    l.add(new Cidades(4311122, '4 - Sul', 'RS', 'Jaquirana', '0055'));
    l.add(new Cidades(5211800, '5 - Centro-Oeste', 'GO', 'Jaragua', '0055'));
    l.add(new Cidades(4208906, '4 - Sul', 'SC', 'Jaragua Do Sul', '0055'));
    l.add(new Cidades(5004908, '5 - Centro-Oeste', 'MS', 'Jaraguari', '0055'));
    l.add(new Cidades(2703700, '2 - Nordeste', 'AL', 'Jaramataia', '0055'));
    l.add(new Cidades(2307106, '2 - Nordeste', 'CE', 'Jardim', '0055'));
    l.add(new Cidades(5005004, '5 - Centro-Oeste', 'MS', 'Jardim', '0055'));
    l.add(new Cidades(4112504, '4 - Sul', 'PR', 'Jardim Alegre', '0055'));
    l.add(new Cidades(
        2405504, '2 - Nordeste', 'RN', 'Jardim De Angicos', '0055'));
    l.add(new Cidades(
        2405603, '2 - Nordeste', 'RN', 'Jardim De Piranhas', '0055'));
    l.add(
        new Cidades(2205250, '2 - Nordeste', 'PI', 'Jardim Do Mulato', '0055'));
    l.add(
        new Cidades(2405702, '2 - Nordeste', 'RN', 'Jardim Do Serido', '0055'));
    l.add(new Cidades(4112603, '4 - Sul', 'PR', 'Jardim Olinda', '0055'));
    l.add(new Cidades(3525102, '3 - Sudeste', 'SP', 'Jardinopolis', '0055'));
    l.add(new Cidades(4208955, '4 - Sul', 'SC', 'Jardinopolis', '0055'));
    l.add(new Cidades(4311130, '4 - Sul', 'RS', 'Jari', '0055'));
    l.add(new Cidades(3525201, '3 - Sudeste', 'SP', 'Jarinu', '0055'));
    l.add(new Cidades(1100114, '1 - Norte', 'RO', 'Jaru', '0055'));
    l.add(new Cidades(5211909, '5 - Centro-Oeste', 'GO', 'Jatai', '0055'));
    l.add(new Cidades(4112702, '4 - Sul', 'PR', 'Jataizinho', '0055'));
    l.add(new Cidades(2608008, '2 - Nordeste', 'PE', 'Jatauba', '0055'));
    l.add(new Cidades(5005103, '5 - Centro-Oeste', 'MS', 'Jatei', '0055'));
    l.add(new Cidades(2307205, '2 - Nordeste', 'CE', 'Jati', '0055'));
    l.add(new Cidades(2105450, '2 - Nordeste', 'MA', 'Jatoba', '0055'));
    l.add(new Cidades(2608057, '2 - Nordeste', 'PE', 'Jatoba', '0055'));
    l.add(
        new Cidades(2205276, '2 - Nordeste', 'PI', 'Jatoba Do Piaui', '0055'));
    l.add(new Cidades(3525300, '3 - Sudeste', 'SP', 'Jau', '0055'));
    l.add(new Cidades(1711506, '1 - Norte', 'TO', 'Jau Do To', '0055'));
    l.add(new Cidades(5212006, '5 - Centro-Oeste', 'GO', 'Jaupaci', '0055'));
    l.add(new Cidades(5105002, '5 - Centro-Oeste', 'MT', 'Jauru', '0055'));
    l.add(new Cidades(3135407, '3 - Sudeste', 'MG', 'Jeceaba', '0055'));
    l.add(
        new Cidades(3135456, '3 - Sudeste', 'MG', 'Jenipapo De Minas', '0055'));
    l.add(new Cidades(
        2105476, '2 - Nordeste', 'MA', 'Jenipapo Dos Vieiras', '0055'));
    l.add(new Cidades(3135506, '3 - Sudeste', 'MG', 'Jequeri', '0055'));
    l.add(
        new Cidades(2703759, '2 - Nordeste', 'AL', 'Jequia Da Praia', '0055'));
    l.add(new Cidades(2918001, '2 - Nordeste', 'BA', 'Jequie', '0055'));
    l.add(new Cidades(3135605, '3 - Sudeste', 'MG', 'Jequitai', '0055'));
    l.add(new Cidades(3135704, '3 - Sudeste', 'MG', 'Jequitiba', '0055'));
    l.add(new Cidades(3135803, '3 - Sudeste', 'MG', 'Jequitinhonha', '0055'));
    l.add(new Cidades(2918100, '2 - Nordeste', 'BA', 'Jeremoabo', '0055'));
    l.add(new Cidades(2507408, '2 - Nordeste', 'PB', 'Jerico', '0055'));
    l.add(new Cidades(3525409, '3 - Sudeste', 'SP', 'Jeriquara', '0055'));
    l.add(
        new Cidades(3203106, '3 - Sudeste', 'ES', 'Jeronimo Monteiro', '0055'));
    l.add(new Cidades(2205300, '2 - Nordeste', 'PI', 'Jerumenha', '0055'));
    l.add(new Cidades(3135902, '3 - Sudeste', 'MG', 'Jesuania', '0055'));
    l.add(new Cidades(4112751, '4 - Sul', 'PR', 'Jesuitas', '0055'));
    l.add(new Cidades(5212055, '5 - Centro-Oeste', 'GO', 'Jesupolis', '0055'));
    l.add(new Cidades(1100122, '1 - Norte', 'RO', 'Ji-Parana', '0055'));
    l.add(new Cidades(
        2307254, '2 - Nordeste', 'CE', 'Jijoca De Jericoacoara', '0055'));
    l.add(new Cidades(2918209, '2 - Nordeste', 'BA', 'Jiquirica', '0055'));
    l.add(new Cidades(2918308, '2 - Nordeste', 'BA', 'Jitauna', '0055'));
    l.add(new Cidades(4209003, '4 - Sul', 'SC', 'Joacaba', '0055'));
    l.add(new Cidades(3136009, '3 - Sudeste', 'MG', 'Joaima', '0055'));
    l.add(new Cidades(3136108, '3 - Sudeste', 'MG', 'Joanesia', '0055'));
    l.add(new Cidades(3525508, '3 - Sudeste', 'SP', 'Joanopolis', '0055'));
    l.add(new Cidades(2608107, '2 - Nordeste', 'PE', 'Joao Alfredo', '0055'));
    l.add(new Cidades(2405801, '2 - Nordeste', 'RN', 'Joao Camara', '0055'));
    l.add(new Cidades(2205359, '2 - Nordeste', 'PI', 'Joao Costa', '0055'));
    l.add(new Cidades(2405900, '2 - Nordeste', 'RN', 'Joao Dias', '0055'));
    l.add(new Cidades(2918357, '2 - Nordeste', 'BA', 'Joao Dourado', '0055'));
    l.add(new Cidades(2105500, '2 - Nordeste', 'MA', 'Joao Lisboa', '0055'));
    l.add(new Cidades(3136207, '3 - Sudeste', 'MG', 'Joao Monlevade', '0055'));
    l.add(new Cidades(3203130, '3 - Sudeste', 'ES', 'Joao Neiva', '0055'));
    l.add(new Cidades(2507507, '2 - Nordeste', 'PB', 'Joao Pessoa', '0055'));
    l.add(new Cidades(3136306, '3 - Sudeste', 'MG', 'Joao Pinheiro', '0055'));
    l.add(new Cidades(3525607, '3 - Sudeste', 'SP', 'Joao Ramalho', '0055'));
    l.add(new Cidades(3136405, '3 - Sudeste', 'MG', 'Joaquim Felicio', '0055'));
    l.add(new Cidades(2703809, '2 - Nordeste', 'AL', 'Joaquim Gomes', '0055'));
    l.add(new Cidades(2608206, '2 - Nordeste', 'PE', 'Joaquim Nabuco', '0055'));
    l.add(new Cidades(2205409, '2 - Nordeste', 'PI', 'Joaquim Pires', '0055'));
    l.add(new Cidades(4112801, '4 - Sul', 'PR', 'Joaquim Tavora', '0055'));
    l.add(new Cidades(2205458, '2 - Nordeste', 'PI', 'Joca Marques', '0055'));
    l.add(new Cidades(4311155, '4 - Sul', 'RS', 'Joia', '0055'));
    l.add(new Cidades(4209102, '4 - Sul', 'SC', 'Joinville', '0055'));
    l.add(new Cidades(3136504, '3 - Sudeste', 'MG', 'Jordania', '0055'));
    l.add(new Cidades(1200328, '1 - Norte', 'AC', 'Jordao', '0055'));
    l.add(new Cidades(4209151, '4 - Sul', 'SC', 'Jose Boiteux', '0055'));
    l.add(new Cidades(3525706, '3 - Sudeste', 'SP', 'Jose Bonifacio', '0055'));
    l.add(new Cidades(2406007, '2 - Nordeste', 'RN', 'Jose Da Penha', '0055'));
    l.add(
        new Cidades(2205508, '2 - Nordeste', 'PI', 'Jose De Freitas', '0055'));
    l.add(new Cidades(
        3136520, '3 - Sudeste', 'MG', 'Jose Goncalves De Minas', '0055'));
    l.add(new Cidades(3136553, '3 - Sudeste', 'MG', 'Jose Raydan', '0055'));
    l.add(new Cidades(2105609, '2 - Nordeste', 'MA', 'Joselandia', '0055'));
    l.add(new Cidades(3136579, '3 - Sudeste', 'MG', 'Josenopolis', '0055'));
    l.add(new Cidades(5212105, '5 - Centro-Oeste', 'GO', 'Joviania', '0055'));
    l.add(new Cidades(5105101, '5 - Centro-Oeste', 'MT', 'Juara', '0055'));
    l.add(new Cidades(2507606, '2 - Nordeste', 'PB', 'Juarez Tavora', '0055'));
    l.add(new Cidades(1711803, '1 - Norte', 'TO', 'Juarina', '0055'));
    l.add(new Cidades(3136652, '3 - Sudeste', 'MG', 'Juatuba', '0055'));
    l.add(new Cidades(2507705, '2 - Nordeste', 'PB', 'Juazeirinho', '0055'));
    l.add(new Cidades(2918407, '2 - Nordeste', 'BA', 'Juazeiro', '0055'));
    l.add(new Cidades(
        2307304, '2 - Nordeste', 'CE', 'Juazeiro Do Norte', '0055'));
    l.add(new Cidades(
        2205516, '2 - Nordeste', 'PI', 'Juazeiro Do Piaui', '0055'));
    l.add(new Cidades(2307403, '2 - Nordeste', 'CE', 'Jucas', '0055'));
    l.add(new Cidades(2608255, '2 - Nordeste', 'PE', 'Jucati', '0055'));
    l.add(new Cidades(2918456, '2 - Nordeste', 'BA', 'Jucurucu', '0055'));
    l.add(new Cidades(2406106, '2 - Nordeste', 'RN', 'Jucurutu', '0055'));
    l.add(new Cidades(5105150, '5 - Centro-Oeste', 'MT', 'Juina', '0055'));
    l.add(new Cidades(3136702, '3 - Sudeste', 'MG', 'Juiz De Fora', '0055'));
    l.add(new Cidades(2205524, '2 - Nordeste', 'PI', 'Julio Borges', '0055'));
    l.add(new Cidades(4311205, '4 - Sul', 'RS', 'Julio De Castilhos', '0055'));
    l.add(new Cidades(3525805, '3 - Sudeste', 'SP', 'Julio Mesquita', '0055'));
    l.add(new Cidades(3525854, '3 - Sudeste', 'SP', 'Jumirim', '0055'));
    l.add(new Cidades(
        2105658, '2 - Nordeste', 'MA', 'Junco Do Maranhao', '0055'));
    l.add(
        new Cidades(2507804, '2 - Nordeste', 'PB', 'Junco Do Serido', '0055'));
    l.add(new Cidades(2406155, '2 - Nordeste', 'RN', 'Jundia', '0055'));
    l.add(new Cidades(2703908, '2 - Nordeste', 'AL', 'Jundia', '0055'));
    l.add(new Cidades(3525904, '3 - Sudeste', 'SP', 'Jundiai', '0055'));
    l.add(new Cidades(4112900, '4 - Sul', 'PR', 'Jundiai Do Sul', '0055'));
    l.add(new Cidades(2704005, '2 - Nordeste', 'AL', 'Junqueiro', '0055'));
    l.add(new Cidades(3526001, '3 - Sudeste', 'SP', 'Junqueiropolis', '0055'));
    l.add(new Cidades(2608305, '2 - Nordeste', 'PE', 'Jupi', '0055'));
    l.add(new Cidades(4209177, '4 - Sul', 'SC', 'Jupia', '0055'));
    l.add(new Cidades(3526100, '3 - Sudeste', 'SP', 'Juquia', '0055'));
    l.add(new Cidades(3526209, '3 - Sudeste', 'SP', 'Juquitiba', '0055'));
    l.add(new Cidades(3136801, '3 - Sudeste', 'MG', 'Juramento', '0055'));
    l.add(new Cidades(4112959, '4 - Sul', 'PR', 'Juranda', '0055'));
    l.add(new Cidades(2205532, '2 - Nordeste', 'PI', 'Jurema', '0055'));
    l.add(new Cidades(2608404, '2 - Nordeste', 'PE', 'Jurema', '0055'));
    l.add(new Cidades(2507903, '2 - Nordeste', 'PB', 'Juripiranga', '0055'));
    l.add(new Cidades(2508000, '2 - Nordeste', 'PB', 'Juru', '0055'));
    l.add(new Cidades(1302207, '1 - Norte', 'AM', 'Jurua', '0055'));
    l.add(new Cidades(3136900, '3 - Sudeste', 'MG', 'Juruaia', '0055'));
    l.add(new Cidades(5105176, '5 - Centro-Oeste', 'MT', 'Juruena', '0055'));
    l.add(new Cidades(1503903, '1 - Norte', 'PA', 'Juruti', '0055'));
    l.add(new Cidades(5105200, '5 - Centro-Oeste', 'MT', 'Juscimeira', '0055'));
    l.add(new Cidades(2918506, '2 - Nordeste', 'BA', 'Jussara', '0055'));
    l.add(new Cidades(4113007, '4 - Sul', 'PR', 'Jussara', '0055'));
    l.add(new Cidades(5212204, '5 - Centro-Oeste', 'GO', 'Jussara', '0055'));
    l.add(new Cidades(2918555, '2 - Nordeste', 'BA', 'Jussari', '0055'));
    l.add(new Cidades(2918605, '2 - Nordeste', 'BA', 'Jussiape', '0055'));
    l.add(new Cidades(1302306, '1 - Norte', 'AM', 'Jutai', '0055'));
    l.add(new Cidades(5005152, '5 - Centro-Oeste', 'MS', 'Juti', '0055'));
    l.add(new Cidades(3136959, '3 - Sudeste', 'MG', 'Juvenilia', '0055'));
    l.add(new Cidades(4113106, '4 - Sul', 'PR', 'Kalore', '0055'));
    l.add(new Cidades(1302405, '1 - Norte', 'AM', 'Labrea', '0055'));
    l.add(new Cidades(4209201, '4 - Sul', 'SC', 'Lacerdopolis', '0055'));
    l.add(new Cidades(3137007, '3 - Sudeste', 'MG', 'Ladainha', '0055'));
    l.add(new Cidades(5005202, '5 - Centro-Oeste', 'MS', 'Ladario', '0055'));
    l.add(new Cidades(
        2918704, '2 - Nordeste', 'BA', 'Lafaiete Coutinho', '0055'));
    l.add(new Cidades(3137106, '3 - Sudeste', 'MG', 'Lagamar', '0055'));
    l.add(new Cidades(2803500, '2 - Nordeste', 'SE', 'Lagarto', '0055'));
    l.add(new Cidades(4209300, '4 - Sul', 'SC', 'Lages', '0055'));
    l.add(new Cidades(2105708, '2 - Nordeste', 'MA', 'Lago Da Pedra', '0055'));
    l.add(new Cidades(2105807, '2 - Nordeste', 'MA', 'Lago Do Junco', '0055'));
    l.add(new Cidades(
        2105948, '2 - Nordeste', 'MA', 'Lago Dos Rodrigues', '0055'));
    l.add(new Cidades(2105906, '2 - Nordeste', 'MA', 'Lago Verde', '0055'));
    l.add(new Cidades(2508109, '2 - Nordeste', 'PB', 'Lagoa', '0055'));
    l.add(new Cidades(2205557, '2 - Nordeste', 'PI', 'Lagoa Alegre', '0055'));
    l.add(new Cidades(4311239, '4 - Sul', 'RS', 'Lagoa Bonita Do Sul', '0055'));
    l.add(new Cidades(2406205, '2 - Nordeste', 'RN', 'Lagoa D`Anta', '0055'));
    l.add(new Cidades(2704104, '2 - Nordeste', 'AL', 'Lagoa Da Canoa', '0055'));
    l.add(new Cidades(1711902, '1 - Norte', 'TO', 'Lagoa Da Confusao', '0055'));
    l.add(new Cidades(3137205, '3 - Sudeste', 'MG', 'Lagoa Da Prata', '0055'));
    l.add(
        new Cidades(2508208, '2 - Nordeste', 'PB', 'Lagoa De Dentro', '0055'));
    l.add(
        new Cidades(2406304, '2 - Nordeste', 'RN', 'Lagoa De Pedras', '0055'));
    l.add(new Cidades(
        2205573, '2 - Nordeste', 'PI', 'Lagoa De Sao Francisco', '0055'));
    l.add(
        new Cidades(2406403, '2 - Nordeste', 'RN', 'Lagoa De Velhos', '0055'));
    l.add(new Cidades(
        2205565, '2 - Nordeste', 'PI', 'Lagoa Do Barro Do Piaui', '0055'));
    l.add(new Cidades(2608453, '2 - Nordeste', 'PE', 'Lagoa Do Carro', '0055'));
    l.add(
        new Cidades(2608503, '2 - Nordeste', 'PE', 'Lagoa Do Itaenga', '0055'));
    l.add(new Cidades(2105922, '2 - Nordeste', 'MA', 'Lagoa Do Mato', '0055'));
    l.add(new Cidades(2608602, '2 - Nordeste', 'PE', 'Lagoa Do Ouro', '0055'));
    l.add(new Cidades(2205581, '2 - Nordeste', 'PI', 'Lagoa Do Piaui', '0055'));
    l.add(new Cidades(2205599, '2 - Nordeste', 'PI', 'Lagoa Do Sitio', '0055'));
    l.add(new Cidades(1711951, '1 - Norte', 'TO', 'Lagoa Do To', '0055'));
    l.add(
        new Cidades(2608701, '2 - Nordeste', 'PE', 'Lagoa Dos Gatos', '0055'));
    l.add(new Cidades(3137304, '3 - Sudeste', 'MG', 'Lagoa Dos Patos', '0055'));
    l.add(
        new Cidades(4311270, '4 - Sul', 'RS', 'Lagoa Dos Tres Cantos', '0055'));
    l.add(new Cidades(3137403, '3 - Sudeste', 'MG', 'Lagoa Dourada', '0055'));
    l.add(new Cidades(3137502, '3 - Sudeste', 'MG', 'Lagoa Formosa', '0055'));
    l.add(new Cidades(2608750, '2 - Nordeste', 'PE', 'Lagoa Grande', '0055'));
    l.add(new Cidades(3137536, '3 - Sudeste', 'MG', 'Lagoa Grande', '0055'));
    l.add(new Cidades(
        2105963, '2 - Nordeste', 'MA', 'Lagoa Grande Do Maranhao', '0055'));
    l.add(new Cidades(2406502, '2 - Nordeste', 'RN', 'Lagoa Nova', '0055'));
    l.add(new Cidades(2918753, '2 - Nordeste', 'BA', 'Lagoa Real', '0055'));
    l.add(new Cidades(2406601, '2 - Nordeste', 'RN', 'Lagoa Salgada', '0055'));
    l.add(new Cidades(3137601, '3 - Sudeste', 'MG', 'Lagoa Santa', '0055'));
    l.add(
        new Cidades(5212253, '5 - Centro-Oeste', 'GO', 'Lagoa Santa', '0055'));
    l.add(new Cidades(2508307, '2 - Nordeste', 'PB', 'Lagoa Seca', '0055'));
    l.add(new Cidades(4311304, '4 - Sul', 'RS', 'Lagoa Vermelha', '0055'));
    l.add(new Cidades(4311254, '4 - Sul', 'RS', 'Lagoao', '0055'));
    l.add(new Cidades(3526308, '3 - Sudeste', 'SP', 'Lagoinha', '0055'));
    l.add(new Cidades(
        2205540, '2 - Nordeste', 'PI', 'Lagoinha Do Piaui', '0055'));
    l.add(new Cidades(4209409, '4 - Sul', 'SC', 'Laguna', '0055'));
    l.add(new Cidades(
        5005251, '5 - Centro-Oeste', 'MS', 'Laguna Carapa', '0055'));
    l.add(new Cidades(2918803, '2 - Nordeste', 'BA', 'Laje', '0055'));
    l.add(new Cidades(3302304, '3 - Sudeste', 'RJ', 'Laje Do Muriae', '0055'));
    l.add(new Cidades(1712009, '1 - Norte', 'TO', 'Lajeado', '0055'));
    l.add(new Cidades(4311403, '4 - Sul', 'RS', 'Lajeado', '0055'));
    l.add(new Cidades(4311429, '4 - Sul', 'RS', 'Lajeado Do Bugre', '0055'));
    l.add(new Cidades(4209458, '4 - Sul', 'SC', 'Lajeado Grande', '0055'));
    l.add(new Cidades(2105989, '2 - Nordeste', 'MA', 'Lajeado Novo', '0055'));
    l.add(new Cidades(2918902, '2 - Nordeste', 'BA', 'Lajedao', '0055'));
    l.add(new Cidades(2919009, '2 - Nordeste', 'BA', 'Lajedinho', '0055'));
    l.add(new Cidades(2608800, '2 - Nordeste', 'PE', 'Lajedo', '0055'));
    l.add(new Cidades(
        2919058, '2 - Nordeste', 'BA', 'Lajedo Do Tabocal', '0055'));
    l.add(new Cidades(2406700, '2 - Nordeste', 'RN', 'Lajes', '0055'));
    l.add(new Cidades(2406809, '2 - Nordeste', 'RN', 'Lajes Pintadas', '0055'));
    l.add(new Cidades(3137700, '3 - Sudeste', 'MG', 'Lajinha', '0055'));
    l.add(new Cidades(2919108, '2 - Nordeste', 'BA', 'Lamarao', '0055'));
    l.add(new Cidades(3137809, '3 - Sudeste', 'MG', 'Lambari', '0055'));
    l.add(new Cidades(
        5105234, '5 - Centro-Oeste', 'MT', 'Lambari D`Oeste', '0055'));
    l.add(new Cidades(3137908, '3 - Sudeste', 'MG', 'Lamim', '0055'));
    l.add(new Cidades(2205607, '2 - Nordeste', 'PI', 'Landri Sales', '0055'));
    l.add(new Cidades(4113205, '4 - Sul', 'PR', 'Lapa', '0055'));
    l.add(new Cidades(2919157, '2 - Nordeste', 'BA', 'Lapao', '0055'));
    l.add(
        new Cidades(3203163, '3 - Sudeste', 'ES', 'Laranja Da Terra', '0055'));
    l.add(new Cidades(3138005, '3 - Sudeste', 'MG', 'Laranjal', '0055'));
    l.add(new Cidades(4113254, '4 - Sul', 'PR', 'Laranjal', '0055'));
    l.add(new Cidades(1600279, '1 - Norte', 'AP', 'Laranjal Do Jari', '0055'));
    l.add(
        new Cidades(3526407, '3 - Sudeste', 'SP', 'Laranjal Paulista', '0055'));
    l.add(new Cidades(2803609, '2 - Nordeste', 'SE', 'Laranjeiras', '0055'));
    l.add(new Cidades(4113304, '4 - Sul', 'PR', 'Laranjeiras Do Sul', '0055'));
    l.add(new Cidades(3138104, '3 - Sudeste', 'MG', 'Lassance', '0055'));
    l.add(new Cidades(2508406, '2 - Nordeste', 'PB', 'Lastro', '0055'));
    l.add(new Cidades(4209508, '4 - Sul', 'SC', 'Laurentino', '0055'));
    l.add(
        new Cidades(2919207, '2 - Nordeste', 'BA', 'Lauro De Freitas', '0055'));
    l.add(new Cidades(4209607, '4 - Sul', 'SC', 'Lauro Muller', '0055'));
    l.add(new Cidades(1712157, '1 - Norte', 'TO', 'Lavandeira', '0055'));
    l.add(new Cidades(3526506, '3 - Sudeste', 'SP', 'Lavinia', '0055'));
    l.add(new Cidades(3138203, '3 - Sudeste', 'MG', 'Lavras', '0055'));
    l.add(new Cidades(
        2307502, '2 - Nordeste', 'CE', 'Lavras Da Mangabeira', '0055'));
    l.add(new Cidades(4311502, '4 - Sul', 'RS', 'Lavras Do Sul', '0055'));
    l.add(new Cidades(3526605, '3 - Sudeste', 'SP', 'Lavrinhas', '0055'));
    l.add(
        new Cidades(3138302, '3 - Sudeste', 'MG', 'Leandro Ferreira', '0055'));
    l.add(new Cidades(4209706, '4 - Sul', 'SC', 'Lebon Regis', '0055'));
    l.add(new Cidades(3526704, '3 - Sudeste', 'SP', 'Leme', '0055'));
    l.add(new Cidades(3138351, '3 - Sudeste', 'MG', 'Leme Do Prado', '0055'));
    l.add(new Cidades(2919306, '2 - Nordeste', 'BA', 'Lencois', '0055'));
    l.add(
        new Cidades(3526803, '3 - Sudeste', 'SP', 'Lencois Paulista', '0055'));
    l.add(new Cidades(4209805, '4 - Sul', 'SC', 'Leoberto Leal', '0055'));
    l.add(new Cidades(3138401, '3 - Sudeste', 'MG', 'Leopoldina', '0055'));
    l.add(new Cidades(
        5212303, '5 - Centro-Oeste', 'GO', 'Leopoldo De Bulhoes', '0055'));
    l.add(new Cidades(4113403, '4 - Sul', 'PR', 'Leopolis', '0055'));
    l.add(new Cidades(4311601, '4 - Sul', 'RS', 'Liberato Salzano', '0055'));
    l.add(new Cidades(3138500, '3 - Sudeste', 'MG', 'Liberdade', '0055'));
    l.add(new Cidades(
        2919405, '2 - Nordeste', 'BA', 'Licinio De Almeida', '0055'));
    l.add(new Cidades(4113429, '4 - Sul', 'PR', 'Lidianopolis', '0055'));
    l.add(new Cidades(2106003, '2 - Nordeste', 'MA', 'Lima Campos', '0055'));
    l.add(new Cidades(3138609, '3 - Sudeste', 'MG', 'Lima Duarte', '0055'));
    l.add(new Cidades(3526902, '3 - Sudeste', 'SP', 'Limeira', '0055'));
    l.add(
        new Cidades(3138625, '3 - Sudeste', 'MG', 'Limeira Do Oeste', '0055'));
    l.add(new Cidades(2608909, '2 - Nordeste', 'PE', 'Limoeiro', '0055'));
    l.add(new Cidades(
        2704203, '2 - Nordeste', 'AL', 'Limoeiro De Anadia', '0055'));
    l.add(new Cidades(1504000, '1 - Norte', 'PA', 'Limoeiro Do Ajuru', '0055'));
    l.add(new Cidades(
        2307601, '2 - Nordeste', 'CE', 'Limoeiro Do Norte', '0055'));
    l.add(new Cidades(4113452, '4 - Sul', 'PR', 'Lindoeste', '0055'));
    l.add(new Cidades(3527009, '3 - Sudeste', 'SP', 'Lindoia', '0055'));
    l.add(new Cidades(4209854, '4 - Sul', 'SC', 'Lindoia Do Sul', '0055'));
    l.add(new Cidades(4311627, '4 - Sul', 'RS', 'Lindolfo Collor', '0055'));
    l.add(new Cidades(4311643, '4 - Sul', 'RS', 'Linha Nova', '0055'));
    l.add(new Cidades(3203205, '3 - Sudeste', 'ES', 'Linhares', '0055'));
    l.add(new Cidades(3527108, '3 - Sudeste', 'SP', 'Lins', '0055'));
    l.add(new Cidades(2508505, '2 - Nordeste', 'PB', 'Livramento', '0055'));
    l.add(new Cidades(
        2919504, '2 - Nordeste', 'BA', 'Livramento De Nossa Senhora', '0055'));
    l.add(new Cidades(1712405, '1 - Norte', 'TO', 'Lizarda', '0055'));
    l.add(new Cidades(4113502, '4 - Sul', 'PR', 'Loanda', '0055'));
    l.add(new Cidades(4113601, '4 - Sul', 'PR', 'Lobato', '0055'));
    l.add(new Cidades(2508554, '2 - Nordeste', 'PB', 'Logradouro', '0055'));
    l.add(new Cidades(4113700, '4 - Sul', 'PR', 'Londrina', '0055'));
    l.add(new Cidades(3138658, '3 - Sudeste', 'MG', 'Lontra', '0055'));
    l.add(new Cidades(4209904, '4 - Sul', 'SC', 'Lontras', '0055'));
    l.add(new Cidades(3527207, '3 - Sudeste', 'SP', 'Lorena', '0055'));
    l.add(new Cidades(2106102, '2 - Nordeste', 'MA', 'Loreto', '0055'));
    l.add(new Cidades(3527256, '3 - Sudeste', 'SP', 'Lourdes', '0055'));
    l.add(new Cidades(3527306, '3 - Sudeste', 'SP', 'Louveira', '0055'));
    l.add(new Cidades(
        5105259, '5 - Centro-Oeste', 'MT', 'Lucas Do Rio Verde', '0055'));
    l.add(new Cidades(3527405, '3 - Sudeste', 'SP', 'Lucelia', '0055'));
    l.add(new Cidades(2508604, '2 - Nordeste', 'PB', 'Lucena', '0055'));
    l.add(new Cidades(3527504, '3 - Sudeste', 'SP', 'Lucianopolis', '0055'));
    l.add(new Cidades(5105309, '5 - Centro-Oeste', 'MT', 'Luciara', '0055'));
    l.add(new Cidades(2406908, '2 - Nordeste', 'RN', 'Lucrecia', '0055'));
    l.add(new Cidades(3527603, '3 - Sudeste', 'SP', 'Luis Antonio', '0055'));
    l.add(new Cidades(2205706, '2 - Nordeste', 'PI', 'Luis Correia', '0055'));
    l.add(new Cidades(2106201, '2 - Nordeste', 'MA', 'Luis Domingues', '0055'));
    l.add(new Cidades(
        2919553, '2 - Nordeste', 'BA', 'Luis Eduardo Magalhaes', '0055'));
    l.add(new Cidades(2407005, '2 - Nordeste', 'RN', 'Luis Gomes', '0055'));
    l.add(new Cidades(3138674, '3 - Sudeste', 'MG', 'Luisburgo', '0055'));
    l.add(new Cidades(3138682, '3 - Sudeste', 'MG', 'Luislandia', '0055'));
    l.add(new Cidades(4210001, '4 - Sul', 'SC', 'Luiz Alves', '0055'));
    l.add(new Cidades(4113734, '4 - Sul', 'PR', 'Luiziana', '0055'));
    l.add(new Cidades(3527702, '3 - Sudeste', 'SP', 'Luiziania', '0055'));
    l.add(new Cidades(3138708, '3 - Sudeste', 'MG', 'Luminarias', '0055'));
    l.add(new Cidades(4113759, '4 - Sul', 'PR', 'Lunardelli', '0055'));
    l.add(new Cidades(3527801, '3 - Sudeste', 'SP', 'Lupercio', '0055'));
    l.add(new Cidades(4113809, '4 - Sul', 'PR', 'Lupionopolis', '0055'));
    l.add(new Cidades(3527900, '3 - Sudeste', 'SP', 'Lutecia', '0055'));
    l.add(new Cidades(3138807, '3 - Sudeste', 'MG', 'Luz', '0055'));
    l.add(new Cidades(4210035, '4 - Sul', 'SC', 'Luzerna', '0055'));
    l.add(new Cidades(5212501, '5 - Centro-Oeste', 'GO', 'Luziania', '0055'));
    l.add(new Cidades(2205805, '2 - Nordeste', 'PI', 'Luzilandia', '0055'));
    l.add(new Cidades(1712454, '1 - Norte', 'TO', 'Luzinopolis', '0055'));
    l.add(new Cidades(3302403, '3 - Sudeste', 'RJ', 'Macae', '0055'));
    l.add(new Cidades(2407104, '2 - Nordeste', 'RN', 'Macaiba', '0055'));
    l.add(new Cidades(2919603, '2 - Nordeste', 'BA', 'Macajuba', '0055'));
    l.add(new Cidades(4311718, '4 - Sul', 'RS', 'Macambara', '0055'));
    l.add(new Cidades(2803708, '2 - Nordeste', 'SE', 'Macambira', '0055'));
    l.add(new Cidades(1600303, '1 - Norte', 'AP', 'Macapa', '0055'));
    l.add(new Cidades(2609006, '2 - Nordeste', 'PE', 'Macaparana', '0055'));
    l.add(new Cidades(2919702, '2 - Nordeste', 'BA', 'Macarani', '0055'));
    l.add(new Cidades(3528007, '3 - Sudeste', 'SP', 'Macatuba', '0055'));
    l.add(new Cidades(2407203, '2 - Nordeste', 'RN', 'Macau', '0055'));
    l.add(new Cidades(3528106, '3 - Sudeste', 'SP', 'Macaubal', '0055'));
    l.add(new Cidades(2919801, '2 - Nordeste', 'BA', 'Macaubas', '0055'));
    l.add(new Cidades(3528205, '3 - Sudeste', 'SP', 'Macedonia', '0055'));
    l.add(new Cidades(2704302, '2 - Nordeste', 'AL', 'Maceio', '0055'));
    l.add(new Cidades(3138906, '3 - Sudeste', 'MG', 'Machacalis', '0055'));
    l.add(new Cidades(4311700, '4 - Sul', 'RS', 'Machadinho', '0055'));
    l.add(
        new Cidades(1100130, '1 - Norte', 'RO', 'Machadinho D`Oeste', '0055'));
    l.add(new Cidades(3139003, '3 - Sudeste', 'MG', 'Machado', '0055'));
    l.add(new Cidades(2609105, '2 - Nordeste', 'PE', 'Machados', '0055'));
    l.add(new Cidades(4210050, '4 - Sul', 'SC', 'Macieira', '0055'));
    l.add(new Cidades(3302452, '3 - Sudeste', 'RJ', 'Macuco', '0055'));
    l.add(new Cidades(2919900, '2 - Nordeste', 'BA', 'Macurure', '0055'));
    l.add(new Cidades(2307635, '2 - Nordeste', 'CE', 'Madalena', '0055'));
    l.add(new Cidades(2205854, '2 - Nordeste', 'PI', 'Madeiro', '0055'));
    l.add(new Cidades(2919926, '2 - Nordeste', 'BA', 'Madre De Deus', '0055'));
    l.add(new Cidades(
        3139102, '3 - Sudeste', 'MG', 'Madre De Deus De Minas', '0055'));
    l.add(new Cidades(2508703, '2 - Nordeste', 'PB', 'Mae D`Agua', '0055'));
    l.add(new Cidades(1504059, '1 - Norte', 'PA', 'Mae Do Rio', '0055'));
    l.add(new Cidades(2919959, '2 - Nordeste', 'BA', 'Maetinga', '0055'));
    l.add(new Cidades(4210100, '4 - Sul', 'SC', 'Mafra', '0055'));
    l.add(new Cidades(1504109, '1 - Norte', 'PA', 'Magalhaes Barata', '0055'));
    l.add(new Cidades(
        2106300, '2 - Nordeste', 'MA', 'Magalhaes De Almeida', '0055'));
    l.add(new Cidades(3528304, '3 - Sudeste', 'SP', 'Magda', '0055'));
    l.add(new Cidades(3302502, '3 - Sudeste', 'RJ', 'Mage', '0055'));
    l.add(new Cidades(2920007, '2 - Nordeste', 'BA', 'Maiquinique', '0055'));
    l.add(new Cidades(2920106, '2 - Nordeste', 'BA', 'Mairi', '0055'));
    l.add(new Cidades(3528403, '3 - Sudeste', 'SP', 'Mairinque', '0055'));
    l.add(new Cidades(3528502, '3 - Sudeste', 'SP', 'Mairipora', '0055'));
    l.add(
        new Cidades(5212600, '5 - Centro-Oeste', 'GO', 'Mairipotaba', '0055'));
    l.add(new Cidades(4210209, '4 - Sul', 'SC', 'Major Gercino', '0055'));
    l.add(new Cidades(2704401, '2 - Nordeste', 'AL', 'Major Isidoro', '0055'));
    l.add(new Cidades(2407252, '2 - Nordeste', 'RN', 'Major Sales', '0055'));
    l.add(new Cidades(4210308, '4 - Sul', 'SC', 'Major Vieira', '0055'));
    l.add(new Cidades(3139201, '3 - Sudeste', 'MG', 'Malacacheta', '0055'));
    l.add(new Cidades(2920205, '2 - Nordeste', 'BA', 'Malhada', '0055'));
    l.add(new Cidades(
        2920304, '2 - Nordeste', 'BA', 'Malhada De Pedras', '0055'));
    l.add(
        new Cidades(2803807, '2 - Nordeste', 'SE', 'Malhada Dos Bois', '0055'));
    l.add(new Cidades(2803906, '2 - Nordeste', 'SE', 'Malhador', '0055'));
    l.add(new Cidades(4113908, '4 - Sul', 'PR', 'Mallet', '0055'));
    l.add(new Cidades(2508802, '2 - Nordeste', 'PB', 'Malta', '0055'));
    l.add(new Cidades(2508901, '2 - Nordeste', 'PB', 'Mamanguape', '0055'));
    l.add(new Cidades(5212709, '5 - Centro-Oeste', 'GO', 'Mambai', '0055'));
    l.add(new Cidades(4114005, '4 - Sul', 'PR', 'Mambore', '0055'));
    l.add(new Cidades(3139250, '3 - Sudeste', 'MG', 'Mamonas', '0055'));
    l.add(new Cidades(4311734, '4 - Sul', 'RS', 'Mampituba', '0055'));
    l.add(new Cidades(1302504, '1 - Norte', 'AM', 'Manacapuru', '0055'));
    l.add(new Cidades(2509008, '2 - Nordeste', 'PB', 'Manaira', '0055'));
    l.add(new Cidades(1302553, '1 - Norte', 'AM', 'Manaquiri', '0055'));
    l.add(new Cidades(2609154, '2 - Nordeste', 'PE', 'Manari', '0055'));
    l.add(new Cidades(1302603, '1 - Norte', 'AM', 'Manaus', '0055'));
    l.add(new Cidades(1200336, '1 - Norte', 'AC', 'Mancio Lima', '0055'));
    l.add(new Cidades(4114104, '4 - Sul', 'PR', 'Mandaguacu', '0055'));
    l.add(new Cidades(4114203, '4 - Sul', 'PR', 'Mandaguari', '0055'));
    l.add(new Cidades(4114302, '4 - Sul', 'PR', 'Mandirituba', '0055'));
    l.add(new Cidades(3528601, '3 - Sudeste', 'SP', 'Manduri', '0055'));
    l.add(new Cidades(4114351, '4 - Sul', 'PR', 'Manfrinopolis', '0055'));
    l.add(new Cidades(3139300, '3 - Sudeste', 'MG', 'Manga', '0055'));
    l.add(new Cidades(3302601, '3 - Sudeste', 'RJ', 'Mangaratiba', '0055'));
    l.add(new Cidades(4114401, '4 - Sul', 'PR', 'Mangueirinha', '0055'));
    l.add(new Cidades(3139409, '3 - Sudeste', 'MG', 'Manhuacu', '0055'));
    l.add(new Cidades(3139508, '3 - Sudeste', 'MG', 'Manhumirim', '0055'));
    l.add(new Cidades(1302702, '1 - Norte', 'AM', 'Manicore', '0055'));
    l.add(new Cidades(2205904, '2 - Nordeste', 'PI', 'Manoel Emidio', '0055'));
    l.add(new Cidades(4114500, '4 - Sul', 'PR', 'Manoel Ribas', '0055'));
    l.add(new Cidades(1200344, '1 - Norte', 'AC', 'Manoel Urbano', '0055'));
    l.add(new Cidades(4311759, '4 - Sul', 'RS', 'Manoel Viana', '0055'));
    l.add(
        new Cidades(2920403, '2 - Nordeste', 'BA', 'Manoel Vitorino', '0055'));
    l.add(new Cidades(2920452, '2 - Nordeste', 'BA', 'Mansidao', '0055'));
    l.add(new Cidades(3139607, '3 - Sudeste', 'MG', 'Mantena', '0055'));
    l.add(new Cidades(3203304, '3 - Sudeste', 'ES', 'Mantenopolis', '0055'));
    l.add(new Cidades(4311775, '4 - Sul', 'RS', 'Maquine', '0055'));
    l.add(new Cidades(3139805, '3 - Sudeste', 'MG', 'Mar De Espanha', '0055'));
    l.add(new Cidades(2704906, '2 - Nordeste', 'AL', 'Mar Vermelho', '0055'));
    l.add(new Cidades(5212808, '5 - Centro-Oeste', 'GO', 'Mara Rosa', '0055'));
    l.add(new Cidades(1302801, '1 - Norte', 'AM', 'Maraa', '0055'));
    l.add(new Cidades(1504208, '1 - Norte', 'PA', 'Maraba', '0055'));
    l.add(new Cidades(3528700, '3 - Sudeste', 'SP', 'Maraba Paulista', '0055'));
    l.add(new Cidades(2106326, '2 - Nordeste', 'MA', 'Maracacume', '0055'));
    l.add(new Cidades(3528809, '3 - Sudeste', 'SP', 'Maracai', '0055'));
    l.add(new Cidades(4210407, '4 - Sul', 'SC', 'Maracaja', '0055'));
    l.add(new Cidades(5005400, '5 - Centro-Oeste', 'MS', 'Maracaju', '0055'));
    l.add(new Cidades(1504307, '1 - Norte', 'PA', 'Maracana', '0055'));
    l.add(new Cidades(2307650, '2 - Nordeste', 'CE', 'Maracanau', '0055'));
    l.add(new Cidades(2920502, '2 - Nordeste', 'BA', 'Maracas', '0055'));
    l.add(new Cidades(2704500, '2 - Nordeste', 'AL', 'Maragogi', '0055'));
    l.add(new Cidades(2920601, '2 - Nordeste', 'BA', 'Maragogipe', '0055'));
    l.add(new Cidades(2609204, '2 - Nordeste', 'PE', 'Maraial', '0055'));
    l.add(new Cidades(2106359, '2 - Nordeste', 'MA', 'Maraja Do Sena', '0055'));
    l.add(new Cidades(2307700, '2 - Nordeste', 'CE', 'Maranguape', '0055'));
    l.add(new Cidades(2106375, '2 - Nordeste', 'MA', 'Maranhaozinho', '0055'));
    l.add(new Cidades(1504406, '1 - Norte', 'PA', 'Marapanim', '0055'));
    l.add(new Cidades(3528858, '3 - Sudeste', 'SP', 'Marapoama', '0055'));
    l.add(new Cidades(4311791, '4 - Sul', 'RS', 'Marata', '0055'));
    l.add(new Cidades(3203320, '3 - Sudeste', 'ES', 'Marataizes', '0055'));
    l.add(new Cidades(2920700, '2 - Nordeste', 'BA', 'Marau', '0055'));
    l.add(new Cidades(4311809, '4 - Sul', 'RS', 'Marau', '0055'));
    l.add(new Cidades(2704609, '2 - Nordeste', 'AL', 'Maravilha', '0055'));
    l.add(new Cidades(4210506, '4 - Sul', 'SC', 'Maravilha', '0055'));
    l.add(new Cidades(3139706, '3 - Sudeste', 'MG', 'Maravilhas', '0055'));
    l.add(new Cidades(2509057, '2 - Nordeste', 'PB', 'Marcacao', '0055'));
    l.add(
        new Cidades(5105580, '5 - Centro-Oeste', 'MT', 'Marcelandia', '0055'));
    l.add(new Cidades(4311908, '4 - Sul', 'RS', 'Marcelino Ramos', '0055'));
    l.add(
        new Cidades(2407302, '2 - Nordeste', 'RN', 'Marcelino Vieira', '0055'));
    l.add(new Cidades(
        2920809, '2 - Nordeste', 'BA', 'Marcionilio Souza', '0055'));
    l.add(new Cidades(2307809, '2 - Nordeste', 'CE', 'Marco', '0055'));
    l.add(new Cidades(2205953, '2 - Nordeste', 'PI', 'Marcolandia', '0055'));
    l.add(new Cidades(2206001, '2 - Nordeste', 'PI', 'Marcos Parente', '0055'));
    l.add(new Cidades(
        4114609, '4 - Sul', 'PR', 'Marechal Candido Rondon', '0055'));
    l.add(
        new Cidades(2704708, '2 - Nordeste', 'AL', 'Marechal Deodoro', '0055'));
    l.add(
        new Cidades(3203346, '3 - Sudeste', 'ES', 'Marechal Floriano', '0055'));
    l.add(new Cidades(
        1200351, '1 - Norte', 'AC', 'Marechal Thaumaturgo', '0055'));
    l.add(new Cidades(4210555, '4 - Sul', 'SC', 'Marema', '0055'));
    l.add(new Cidades(2509107, '2 - Nordeste', 'PB', 'Mari', '0055'));
    l.add(new Cidades(3139904, '3 - Sudeste', 'MG', 'Maria Da Fe', '0055'));
    l.add(new Cidades(4114708, '4 - Sul', 'PR', 'Maria Helena', '0055'));
    l.add(new Cidades(4114807, '4 - Sul', 'PR', 'Marialva', '0055'));
    l.add(new Cidades(3140001, '3 - Sudeste', 'MG', 'Mariana', '0055'));
    l.add(new Cidades(4311981, '4 - Sul', 'RS', 'Mariana Pimentel', '0055'));
    l.add(new Cidades(4312005, '4 - Sul', 'RS', 'Mariano Moro', '0055'));
    l.add(
        new Cidades(1712504, '1 - Norte', 'TO', 'Marianopolis Do To', '0055'));
    l.add(new Cidades(3528908, '3 - Sudeste', 'SP', 'Mariapolis', '0055'));
    l.add(new Cidades(2704807, '2 - Nordeste', 'AL', 'Maribondo', '0055'));
    l.add(new Cidades(3302700, '3 - Sudeste', 'RJ', 'Marica', '0055'));
    l.add(new Cidades(3140100, '3 - Sudeste', 'MG', 'Marilac', '0055'));
    l.add(new Cidades(3203353, '3 - Sudeste', 'ES', 'Marilandia', '0055'));
    l.add(new Cidades(4114906, '4 - Sul', 'PR', 'Marilandia Do Sul', '0055'));
    l.add(new Cidades(4115002, '4 - Sul', 'PR', 'Marilena', '0055'));
    l.add(new Cidades(3529005, '3 - Sudeste', 'SP', 'Marilia', '0055'));
    l.add(new Cidades(4115101, '4 - Sul', 'PR', 'Mariluz', '0055'));
    l.add(new Cidades(4115200, '4 - Sul', 'PR', 'Maringa', '0055'));
    l.add(new Cidades(3529104, '3 - Sudeste', 'SP', 'Marinopolis', '0055'));
    l.add(new Cidades(3140159, '3 - Sudeste', 'MG', 'Mario Campos', '0055'));
    l.add(new Cidades(4115309, '4 - Sul', 'PR', 'Mariopolis', '0055'));
    l.add(new Cidades(4115358, '4 - Sul', 'PR', 'Maripa', '0055'));
    l.add(new Cidades(3140209, '3 - Sudeste', 'MG', 'Maripa De Minas', '0055'));
    l.add(new Cidades(1504422, '1 - Norte', 'PA', 'Marituba', '0055'));
    l.add(new Cidades(2509156, '2 - Nordeste', 'PB', 'Marizopolis', '0055'));
    l.add(new Cidades(3140308, '3 - Sudeste', 'MG', 'Marlieria', '0055'));
    l.add(new Cidades(4115408, '4 - Sul', 'PR', 'Marmeleiro', '0055'));
    l.add(new Cidades(3140407, '3 - Sudeste', 'MG', 'Marmelopolis', '0055'));
    l.add(new Cidades(4312054, '4 - Sul', 'RS', 'Marques De Souza', '0055'));
    l.add(new Cidades(4115457, '4 - Sul', 'PR', 'Marquinho', '0055'));
    l.add(new Cidades(3140506, '3 - Sudeste', 'MG', 'Martinho Campos', '0055'));
    l.add(new Cidades(2307908, '2 - Nordeste', 'CE', 'Martinopole', '0055'));
    l.add(new Cidades(3529203, '3 - Sudeste', 'SP', 'Martinopolis', '0055'));
    l.add(new Cidades(2407401, '2 - Nordeste', 'RN', 'Martins', '0055'));
    l.add(new Cidades(3140530, '3 - Sudeste', 'MG', 'Martins Soares', '0055'));
    l.add(new Cidades(2804003, '2 - Nordeste', 'SE', 'Maruim', '0055'));
    l.add(new Cidades(4115507, '4 - Sul', 'PR', 'Marumbi', '0055'));
    l.add(new Cidades(5212907, '5 - Centro-Oeste', 'GO', 'Marzagao', '0055'));
    l.add(new Cidades(2920908, '2 - Nordeste', 'BA', 'Mascote', '0055'));
    l.add(new Cidades(2308005, '2 - Nordeste', 'CE', 'Massape', '0055'));
    l.add(
        new Cidades(2206050, '2 - Nordeste', 'PI', 'Massape Do Piaui', '0055'));
    l.add(new Cidades(2509206, '2 - Nordeste', 'PB', 'Massaranduba', '0055'));
    l.add(new Cidades(4210605, '4 - Sul', 'SC', 'Massaranduba', '0055'));
    l.add(new Cidades(4312104, '4 - Sul', 'RS', 'Mata', '0055'));
    l.add(
        new Cidades(2921005, '2 - Nordeste', 'BA', 'Mata De Sao Joao', '0055'));
    l.add(new Cidades(2705002, '2 - Nordeste', 'AL', 'Mata Grande', '0055'));
    l.add(new Cidades(2106409, '2 - Nordeste', 'MA', 'Mata Roma', '0055'));
    l.add(new Cidades(3140555, '3 - Sudeste', 'MG', 'Mata Verde', '0055'));
    l.add(new Cidades(3529302, '3 - Sudeste', 'SP', 'Matao', '0055'));
    l.add(new Cidades(2509305, '2 - Nordeste', 'PB', 'Mataraca', '0055'));
    l.add(new Cidades(1712702, '1 - Norte', 'TO', 'Mateiros', '0055'));
    l.add(new Cidades(4115606, '4 - Sul', 'PR', 'Matelandia', '0055'));
    l.add(new Cidades(3140605, '3 - Sudeste', 'MG', 'Materlandia', '0055'));
    l.add(new Cidades(3140704, '3 - Sudeste', 'MG', 'Mateus Leme', '0055'));
    l.add(new Cidades(3171501, '3 - Sudeste', 'MG', 'Mathias Lobato', '0055'));
    l.add(new Cidades(3140803, '3 - Sudeste', 'MG', 'Matias Barbosa', '0055'));
    l.add(new Cidades(3140852, '3 - Sudeste', 'MG', 'Matias Cardoso', '0055'));
    l.add(new Cidades(2206100, '2 - Nordeste', 'PI', 'Matias Olimpio', '0055'));
    l.add(new Cidades(2921054, '2 - Nordeste', 'BA', 'Matina', '0055'));
    l.add(new Cidades(2106508, '2 - Nordeste', 'MA', 'Matinha', '0055'));
    l.add(new Cidades(2509339, '2 - Nordeste', 'PB', 'Matinhas', '0055'));
    l.add(new Cidades(4115705, '4 - Sul', 'PR', 'Matinhos', '0055'));
    l.add(new Cidades(3140902, '3 - Sudeste', 'MG', 'Matipo', '0055'));
    l.add(new Cidades(4312138, '4 - Sul', 'RS', 'Mato Castelhano', '0055'));
    l.add(new Cidades(2509370, '2 - Nordeste', 'PB', 'Mt', '0055'));
    l.add(new Cidades(4312153, '4 - Sul', 'RS', 'Mato Leitao', '0055'));
    l.add(new Cidades(4312179, '4 - Sul', 'RS', 'Mato Queimado', '0055'));
    l.add(new Cidades(4115739, '4 - Sul', 'PR', 'Mato Rico', '0055'));
    l.add(new Cidades(3141009, '3 - Sudeste', 'MG', 'Mato Verde', '0055'));
    l.add(new Cidades(2106607, '2 - Nordeste', 'MA', 'Matoes', '0055'));
    l.add(
        new Cidades(2106631, '2 - Nordeste', 'MA', 'Matoes Do Norte', '0055'));
    l.add(new Cidades(4210704, '4 - Sul', 'SC', 'Matos Costa', '0055'));
    l.add(new Cidades(3141108, '3 - Sudeste', 'MG', 'Matozinhos', '0055'));
    l.add(new Cidades(5212956, '5 - Centro-Oeste', 'GO', 'Matrincha', '0055'));
    l.add(new Cidades(
        2705101, '2 - Nordeste', 'AL', 'Matriz De Camaragibe', '0055'));
    l.add(new Cidades(5105606, '5 - Centro-Oeste', 'MT', 'Matupa', '0055'));
    l.add(new Cidades(2509396, '2 - Nordeste', 'PB', 'Matureia', '0055'));
    l.add(new Cidades(3141207, '3 - Sudeste', 'MG', 'Matutina', '0055'));
    l.add(new Cidades(3529401, '3 - Sudeste', 'SP', 'Maua', '0055'));
    l.add(new Cidades(4115754, '4 - Sul', 'PR', 'Maua Da Serra', '0055'));
    l.add(new Cidades(1302900, '1 - Norte', 'AM', 'Maues', '0055'));
    l.add(
        new Cidades(5213004, '5 - Centro-Oeste', 'GO', 'Maurilandia', '0055'));
    l.add(new Cidades(1712801, '1 - Norte', 'TO', 'Maurilandia Do To', '0055'));
    l.add(new Cidades(2308104, '2 - Nordeste', 'CE', 'Mauriti', '0055'));
    l.add(new Cidades(2407500, '2 - Nordeste', 'RN', 'Maxaranguape', '0055'));
    l.add(new Cidades(
        4312203, '4 - Sul', 'RS', 'Maximiliano De Almeida', '0055'));
    l.add(new Cidades(1600402, '1 - Norte', 'AP', 'Mazagao', '0055'));
    l.add(new Cidades(3141306, '3 - Sudeste', 'MG', 'Medeiros', '0055'));
    l.add(new Cidades(2921104, '2 - Nordeste', 'BA', 'Medeiros Neto', '0055'));
    l.add(new Cidades(4115804, '4 - Sul', 'PR', 'Medianeira', '0055'));
    l.add(new Cidades(1504455, '1 - Norte', 'PA', 'Medicilandia', '0055'));
    l.add(new Cidades(3141405, '3 - Sudeste', 'MG', 'Medina', '0055'));
    l.add(new Cidades(4210803, '4 - Sul', 'SC', 'Meleiro', '0055'));
    l.add(new Cidades(1504505, '1 - Norte', 'PA', 'Melgaco', '0055'));
    l.add(new Cidades(3302809, '3 - Sudeste', 'RJ', 'Mendes', '0055'));
    l.add(new Cidades(3141504, '3 - Sudeste', 'MG', 'Mendes Pimentel', '0055'));
    l.add(new Cidades(3529500, '3 - Sudeste', 'SP', 'Mendonca', '0055'));
    l.add(new Cidades(4115853, '4 - Sul', 'PR', 'Mercedes', '0055'));
    l.add(new Cidades(3141603, '3 - Sudeste', 'MG', 'Merces', '0055'));
    l.add(new Cidades(3529609, '3 - Sudeste', 'SP', 'Meridiano', '0055'));
    l.add(new Cidades(2308203, '2 - Nordeste', 'CE', 'Meruoca', '0055'));
    l.add(new Cidades(3529658, '3 - Sudeste', 'SP', 'Mesopolis', '0055'));
    l.add(new Cidades(3141702, '3 - Sudeste', 'MG', 'Mesquita', '0055'));
    l.add(new Cidades(3302858, '3 - Sudeste', 'RJ', 'Mesquita', '0055'));
    l.add(new Cidades(2705200, '2 - Nordeste', 'AL', 'Messias', '0055'));
    l.add(
        new Cidades(2407609, '2 - Nordeste', 'RN', 'Messias Targino', '0055'));
    l.add(new Cidades(2206209, '2 - Nordeste', 'PI', 'Miguel Alves', '0055'));
    l.add(new Cidades(2921203, '2 - Nordeste', 'BA', 'Miguel Calmon', '0055'));
    l.add(new Cidades(2206308, '2 - Nordeste', 'PI', 'Miguel Leao', '0055'));
    l.add(new Cidades(3302908, '3 - Sudeste', 'RJ', 'Miguel Pereira', '0055'));
    l.add(new Cidades(3529708, '3 - Sudeste', 'SP', 'Miguelopolis', '0055'));
    l.add(new Cidades(2308302, '2 - Nordeste', 'CE', 'Milagres', '0055'));
    l.add(new Cidades(2921302, '2 - Nordeste', 'BA', 'Milagres', '0055'));
    l.add(new Cidades(
        2106672, '2 - Nordeste', 'MA', 'Milagres Do Maranhao', '0055'));
    l.add(new Cidades(2308351, '2 - Nordeste', 'CE', 'Milha', '0055'));
    l.add(new Cidades(2206357, '2 - Nordeste', 'PI', 'Milton Brandao', '0055'));
    l.add(new Cidades(
        5213053, '5 - Centro-Oeste', 'GO', 'Mimoso De Goias', '0055'));
    l.add(new Cidades(3203403, '3 - Sudeste', 'ES', 'Mimoso Do Sul', '0055'));
    l.add(new Cidades(5213087, '5 - Centro-Oeste', 'GO', 'Minacu', '0055'));
    l.add(new Cidades(
        2705309, '2 - Nordeste', 'AL', 'Minador Do Negrao', '0055'));
    l.add(new Cidades(4312252, '4 - Sul', 'RS', 'Minas Do Leao', '0055'));
    l.add(new Cidades(3141801, '3 - Sudeste', 'MG', 'Minas Novas', '0055'));
    l.add(new Cidades(3141900, '3 - Sudeste', 'MG', 'Minduri', '0055'));
    l.add(new Cidades(5213103, '5 - Centro-Oeste', 'GO', 'Mineiros', '0055'));
    l.add(
        new Cidades(3529807, '3 - Sudeste', 'SP', 'Mineiros Do Tiete', '0055'));
    l.add(
        new Cidades(1101203, '1 - Norte', 'RO', 'Ministro Andreazza', '0055'));
    l.add(new Cidades(3530003, '3 - Sudeste', 'SP', 'Mira Estrela', '0055'));
    l.add(new Cidades(3142007, '3 - Sudeste', 'MG', 'Mirabela', '0055'));
    l.add(new Cidades(3529906, '3 - Sudeste', 'SP', 'Miracatu', '0055'));
    l.add(new Cidades(3303005, '3 - Sudeste', 'RJ', 'Miracema', '0055'));
    l.add(new Cidades(1713205, '1 - Norte', 'TO', 'Miracema Do To', '0055'));
    l.add(new Cidades(2106706, '2 - Nordeste', 'MA', 'Mirador', '0055'));
    l.add(new Cidades(4115903, '4 - Sul', 'PR', 'Mirador', '0055'));
    l.add(new Cidades(3142106, '3 - Sudeste', 'MG', 'Miradouro', '0055'));
    l.add(new Cidades(4312302, '4 - Sul', 'RS', 'Miraguai', '0055'));
    l.add(new Cidades(3142205, '3 - Sudeste', 'MG', 'Mirai', '0055'));
    l.add(new Cidades(2308377, '2 - Nordeste', 'CE', 'Miraima', '0055'));
    l.add(new Cidades(5005608, '5 - Centro-Oeste', 'MS', 'Miranda', '0055'));
    l.add(
        new Cidades(2106755, '2 - Nordeste', 'MA', 'Miranda Do Norte', '0055'));
    l.add(new Cidades(2609303, '2 - Nordeste', 'PE', 'Mirandiba', '0055'));
    l.add(new Cidades(3530102, '3 - Sudeste', 'SP', 'Mirandopolis', '0055'));
    l.add(new Cidades(2921401, '2 - Nordeste', 'BA', 'Mirangaba', '0055'));
    l.add(new Cidades(1713304, '1 - Norte', 'TO', 'Miranorte', '0055'));
    l.add(new Cidades(2921450, '2 - Nordeste', 'BA', 'Mirante', '0055'));
    l.add(new Cidades(1101302, '1 - Norte', 'RO', 'Mirante Da Serra', '0055'));
    l.add(new Cidades(
        3530201, '3 - Sudeste', 'SP', 'Mirante Do Paranapanema', '0055'));
    l.add(new Cidades(4116000, '4 - Sul', 'PR', 'Miraselva', '0055'));
    l.add(new Cidades(3530300, '3 - Sudeste', 'SP', 'Mirassol', '0055'));
    l.add(new Cidades(
        5105622, '5 - Centro-Oeste', 'MT', 'Mirassol D`Oeste', '0055'));
    l.add(new Cidades(3530409, '3 - Sudeste', 'SP', 'Mirassolandia', '0055'));
    l.add(new Cidades(3142254, '3 - Sudeste', 'MG', 'Miravania', '0055'));
    l.add(new Cidades(4210852, '4 - Sul', 'SC', 'Mirim Doce', '0055'));
    l.add(new Cidades(2106805, '2 - Nordeste', 'MA', 'Mirinzal', '0055'));
    l.add(new Cidades(4116059, '4 - Sul', 'PR', 'Missal', '0055'));
    l.add(new Cidades(2308401, '2 - Nordeste', 'CE', 'Missao Velha', '0055'));
    l.add(new Cidades(1504604, '1 - Norte', 'PA', 'Mocajuba', '0055'));
    l.add(new Cidades(3530508, '3 - Sudeste', 'SP', 'Mococa', '0055'));
    l.add(new Cidades(4210902, '4 - Sul', 'SC', 'Modelo', '0055'));
    l.add(new Cidades(3142304, '3 - Sudeste', 'MG', 'Moeda', '0055'));
    l.add(new Cidades(3142403, '3 - Sudeste', 'MG', 'Moema', '0055'));
    l.add(new Cidades(2509404, '2 - Nordeste', 'PB', 'Mogeiro', '0055'));
    l.add(new Cidades(3530607, '3 - Sudeste', 'SP', 'Mogi Das Cruzes', '0055'));
    l.add(new Cidades(3530706, '3 - Sudeste', 'SP', 'Mogi Guacu', '0055'));
    l.add(new Cidades(5213400, '5 - Centro-Oeste', 'GO', 'Moipora', '0055'));
    l.add(new Cidades(2804102, '2 - Nordeste', 'SE', 'Moita Bonita', '0055'));
    l.add(new Cidades(3530805, '3 - Sudeste', 'SP', 'Moji Mirim', '0055'));
    l.add(new Cidades(1504703, '1 - Norte', 'PA', 'Moju', '0055'));
    l.add(new Cidades(1504752, '1 - Norte', 'PA', 'Mojui Dos Campos', '0055'));
    l.add(new Cidades(2308500, '2 - Nordeste', 'CE', 'Mombaca', '0055'));
    l.add(new Cidades(3530904, '3 - Sudeste', 'SP', 'Mombuca', '0055'));
    l.add(new Cidades(2106904, '2 - Nordeste', 'MA', 'Moncao', '0055'));
    l.add(new Cidades(3531001, '3 - Sudeste', 'SP', 'Moncoes', '0055'));
    l.add(new Cidades(4211009, '4 - Sul', 'SC', 'Mondai', '0055'));
    l.add(new Cidades(3531100, '3 - Sudeste', 'SP', 'Mongagua', '0055'));
    l.add(new Cidades(3142502, '3 - Sudeste', 'MG', 'Monjolos', '0055'));
    l.add(new Cidades(2206407, '2 - Nordeste', 'PI', 'Monsenhor Gil', '0055'));
    l.add(new Cidades(
        2206506, '2 - Nordeste', 'PI', 'Monsenhor Hipolito', '0055'));
    l.add(new Cidades(3142601, '3 - Sudeste', 'MG', 'Monsenhor Paulo', '0055'));
    l.add(
        new Cidades(2308609, '2 - Nordeste', 'CE', 'Monsenhor Tabosa', '0055'));
    l.add(new Cidades(2509503, '2 - Nordeste', 'PB', 'Montadas', '0055'));
    l.add(new Cidades(3142700, '3 - Sudeste', 'MG', 'Montalvania', '0055'));
    l.add(new Cidades(3203502, '3 - Sudeste', 'ES', 'Montanha', '0055'));
    l.add(new Cidades(2407708, '2 - Nordeste', 'RN', 'Montanhas', '0055'));
    l.add(new Cidades(4312351, '4 - Sul', 'RS', 'Montauri', '0055'));
    l.add(new Cidades(1504802, '1 - Norte', 'PA', 'Monte Alegre', '0055'));
    l.add(new Cidades(2407807, '2 - Nordeste', 'RN', 'Monte Alegre', '0055'));
    l.add(new Cidades(
        5213509, '5 - Centro-Oeste', 'GO', 'Monte Alegre De Goias', '0055'));
    l.add(new Cidades(
        3142809, '3 - Sudeste', 'MG', 'Monte Alegre De Minas', '0055'));
    l.add(new Cidades(
        2804201, '2 - Nordeste', 'SE', 'Monte Alegre De Se', '0055'));
    l.add(new Cidades(
        2206605, '2 - Nordeste', 'PI', 'Monte Alegre Do Piaui', '0055'));
    l.add(new Cidades(
        3531209, '3 - Sudeste', 'SP', 'Monte Alegre Do Sul', '0055'));
    l.add(new Cidades(
        4312377, '4 - Sul', 'RS', 'Monte Alegre Dos Campos', '0055'));
    l.add(new Cidades(3531308, '3 - Sudeste', 'SP', 'Monte Alto', '0055'));
    l.add(new Cidades(3531407, '3 - Sudeste', 'SP', 'Monte Aprazivel', '0055'));
    l.add(new Cidades(3142908, '3 - Sudeste', 'MG', 'Monte Azul', '0055'));
    l.add(new Cidades(
        3531506, '3 - Sudeste', 'SP', 'Monte Azul Paulista', '0055'));
    l.add(new Cidades(3143005, '3 - Sudeste', 'MG', 'Monte Belo', '0055'));
    l.add(new Cidades(4312385, '4 - Sul', 'RS', 'Monte Belo Do Sul', '0055'));
    l.add(new Cidades(4211058, '4 - Sul', 'SC', 'Monte Carlo', '0055'));
    l.add(new Cidades(3143104, '3 - Sudeste', 'MG', 'Monte Carmelo', '0055'));
    l.add(new Cidades(3531605, '3 - Sudeste', 'SP', 'Monte Castelo', '0055'));
    l.add(new Cidades(4211108, '4 - Sul', 'SC', 'Monte Castelo', '0055'));
    l.add(new Cidades(
        2407906, '2 - Nordeste', 'RN', 'Monte Das Gameleiras', '0055'));
    l.add(new Cidades(1713601, '1 - Norte', 'TO', 'Monte Do Carmo', '0055'));
    l.add(new Cidades(3143153, '3 - Sudeste', 'MG', 'Monte Formoso', '0055'));
    l.add(new Cidades(2509602, '2 - Nordeste', 'PB', 'Monte Horebe', '0055'));
    l.add(new Cidades(3531803, '3 - Sudeste', 'SP', 'Monte Mor', '0055'));
    l.add(new Cidades(1101401, '1 - Norte', 'RO', 'Monte Negro', '0055'));
    l.add(new Cidades(2921500, '2 - Nordeste', 'BA', 'Monte Santo', '0055'));
    l.add(new Cidades(
        3143203, '3 - Sudeste', 'MG', 'Monte Santo De Minas', '0055'));
    l.add(new Cidades(1713700, '1 - Norte', 'TO', 'Monte Santo Do To', '0055'));
    l.add(new Cidades(3143401, '3 - Sudeste', 'MG', 'Monte Siao', '0055'));
    l.add(new Cidades(2509701, '2 - Nordeste', 'PB', 'Monteiro', '0055'));
    l.add(new Cidades(3531704, '3 - Sudeste', 'SP', 'Monteiro Lobato', '0055'));
    l.add(new Cidades(2705408, '2 - Nordeste', 'AL', 'Monteiropolis', '0055'));
    l.add(new Cidades(4312401, '4 - Sul', 'RS', 'Montenegro', '0055'));
    l.add(new Cidades(2107001, '2 - Nordeste', 'MA', 'Montes Altos', '0055'));
    l.add(new Cidades(3143302, '3 - Sudeste', 'MG', 'Montes Claros', '0055'));
    l.add(new Cidades(
        5213707, '5 - Centro-Oeste', 'GO', 'Montes Claros De Goias', '0055'));
    l.add(new Cidades(3143450, '3 - Sudeste', 'MG', 'Montezuma', '0055'));
    l.add(new Cidades(5213756, '5 - Centro-Oeste', 'GO', 'Montividiu', '0055'));
    l.add(new Cidades(
        5213772, '5 - Centro-Oeste', 'GO', 'Montividiu Do Norte', '0055'));
    l.add(new Cidades(2308708, '2 - Nordeste', 'CE', 'Morada Nova', '0055'));
    l.add(new Cidades(
        3143500, '3 - Sudeste', 'MG', 'Morada Nova De Minas', '0055'));
    l.add(new Cidades(2308807, '2 - Nordeste', 'CE', 'Moraujo', '0055'));
    l.add(new Cidades(2614303, '2 - Nordeste', 'PE', 'Moreilandia', '0055'));
    l.add(new Cidades(4116109, '4 - Sul', 'PR', 'Moreira Sales', '0055'));
    l.add(new Cidades(2609402, '2 - Nordeste', 'PE', 'Moreno', '0055'));
    l.add(new Cidades(4312427, '4 - Sul', 'RS', 'Mormaco', '0055'));
    l.add(new Cidades(2921609, '2 - Nordeste', 'BA', 'Morpara', '0055'));
    l.add(new Cidades(4116208, '4 - Sul', 'PR', 'Morretes', '0055'));
    l.add(new Cidades(2308906, '2 - Nordeste', 'CE', 'Morrinhos', '0055'));
    l.add(new Cidades(5213806, '5 - Centro-Oeste', 'GO', 'Morrinhos', '0055'));
    l.add(new Cidades(4312443, '4 - Sul', 'RS', 'Morrinhos Do Sul', '0055'));
    l.add(new Cidades(3531902, '3 - Sudeste', 'SP', 'Morro Agudo', '0055'));
    l.add(new Cidades(
        5213855, '5 - Centro-Oeste', 'GO', 'Morro Agudo De Goias', '0055'));
    l.add(new Cidades(
        2206654, '2 - Nordeste', 'PI', 'Morro Cabeca No Tempo', '0055'));
    l.add(new Cidades(4211207, '4 - Sul', 'SC', 'Morro Da Fumaca', '0055'));
    l.add(new Cidades(3143609, '3 - Sudeste', 'MG', 'Morro Da Garca', '0055'));
    l.add(
        new Cidades(2921708, '2 - Nordeste', 'BA', 'Morro Do Chapeu', '0055'));
    l.add(new Cidades(
        2206670, '2 - Nordeste', 'PI', 'Morro Do Chapeu Do Piaui', '0055'));
    l.add(new Cidades(3143708, '3 - Sudeste', 'MG', 'Morro Do Pilar', '0055'));
    l.add(new Cidades(4211256, '4 - Sul', 'SC', 'Morro Grande', '0055'));
    l.add(new Cidades(4312450, '4 - Sul', 'RS', 'Morro Redondo', '0055'));
    l.add(new Cidades(4312476, '4 - Sul', 'RS', 'Morro Reuter', '0055'));
    l.add(new Cidades(2107100, '2 - Nordeste', 'MA', 'Morros', '0055'));
    l.add(new Cidades(2921807, '2 - Nordeste', 'BA', 'Mortugaba', '0055'));
    l.add(new Cidades(3532009, '3 - Sudeste', 'SP', 'Morungaba', '0055'));
    l.add(new Cidades(5213905, '5 - Centro-Oeste', 'GO', 'Mossamedes', '0055'));
    l.add(new Cidades(2408003, '2 - Nordeste', 'RN', 'Mossoro', '0055'));
    l.add(new Cidades(4312500, '4 - Sul', 'RS', 'Mostardas', '0055'));
    l.add(new Cidades(3532058, '3 - Sudeste', 'SP', 'Motuca', '0055'));
    l.add(
        new Cidades(5214002, '5 - Centro-Oeste', 'GO', 'Mozarlandia', '0055'));
    l.add(new Cidades(1504901, '1 - Norte', 'PA', 'Muana', '0055'));
    l.add(new Cidades(1400308, '1 - Norte', 'RR', 'Mucajai', '0055'));
    l.add(new Cidades(2309003, '2 - Nordeste', 'CE', 'Mucambo', '0055'));
    l.add(new Cidades(2921906, '2 - Nordeste', 'BA', 'Mucuge', '0055'));
    l.add(new Cidades(4312609, '4 - Sul', 'RS', 'Mucum', '0055'));
    l.add(new Cidades(2922003, '2 - Nordeste', 'BA', 'Mucuri', '0055'));
    l.add(new Cidades(3203601, '3 - Sudeste', 'ES', 'Mucurici', '0055'));
    l.add(new Cidades(4312617, '4 - Sul', 'RS', 'Muitos Capoes', '0055'));
    l.add(new Cidades(4312625, '4 - Sul', 'RS', 'Muliterno', '0055'));
    l.add(new Cidades(2309102, '2 - Nordeste', 'CE', 'Mulungu', '0055'));
    l.add(new Cidades(2509800, '2 - Nordeste', 'PB', 'Mulungu', '0055'));
    l.add(
        new Cidades(2922052, '2 - Nordeste', 'BA', 'Mulungu Do Morro', '0055'));
    l.add(new Cidades(2922102, '2 - Nordeste', 'BA', 'Mundo Novo', '0055'));
    l.add(new Cidades(5005681, '5 - Centro-Oeste', 'MS', 'Mundo Novo', '0055'));
    l.add(new Cidades(5214051, '5 - Centro-Oeste', 'GO', 'Mundo Novo', '0055'));
    l.add(new Cidades(3143807, '3 - Sudeste', 'MG', 'Munhoz', '0055'));
    l.add(new Cidades(4116307, '4 - Sul', 'PR', 'Munhoz De Melo', '0055'));
    l.add(new Cidades(2922201, '2 - Nordeste', 'BA', 'Muniz Ferreira', '0055'));
    l.add(new Cidades(3203700, '3 - Sudeste', 'ES', 'Muniz Freire', '0055'));
    l.add(new Cidades(
        2922250, '2 - Nordeste', 'BA', 'Muquem De Sao Francisco', '0055'));
    l.add(new Cidades(3203809, '3 - Sudeste', 'ES', 'Muqui', '0055'));
    l.add(new Cidades(3143906, '3 - Sudeste', 'MG', 'Muriae', '0055'));
    l.add(new Cidades(2804300, '2 - Nordeste', 'SE', 'Muribeca', '0055'));
    l.add(new Cidades(2705507, '2 - Nordeste', 'AL', 'Murici', '0055'));
    l.add(new Cidades(
        2206696, '2 - Nordeste', 'PI', 'Murici Dos Portelas', '0055'));
    l.add(new Cidades(1713957, '1 - Norte', 'TO', 'Muricilandia', '0055'));
    l.add(new Cidades(2922300, '2 - Nordeste', 'BA', 'Muritiba', '0055'));
    l.add(
        new Cidades(3532108, '3 - Sudeste', 'SP', 'Murutinga Do Sul', '0055'));
    l.add(new Cidades(2922409, '2 - Nordeste', 'BA', 'Mutuipe', '0055'));
    l.add(new Cidades(3144003, '3 - Sudeste', 'MG', 'Mutum', '0055'));
    l.add(
        new Cidades(5214101, '5 - Centro-Oeste', 'GO', 'Mutunopolis', '0055'));
    l.add(new Cidades(3144102, '3 - Sudeste', 'MG', 'Muzambinho', '0055'));
    l.add(new Cidades(3144201, '3 - Sudeste', 'MG', 'Nacip Raydan', '0055'));
    l.add(new Cidades(3532157, '3 - Sudeste', 'SP', 'Nantes', '0055'));
    l.add(new Cidades(3144300, '3 - Sudeste', 'MG', 'Nanuque', '0055'));
    l.add(new Cidades(4312658, '4 - Sul', 'RS', 'Nao-Me-Toque', '0055'));
    l.add(new Cidades(3144359, '3 - Sudeste', 'MG', 'Naque', '0055'));
    l.add(new Cidades(3532207, '3 - Sudeste', 'SP', 'Narandiba', '0055'));
    l.add(new Cidades(2408102, '2 - Nordeste', 'RN', 'Natal', '0055'));
    l.add(new Cidades(3144375, '3 - Sudeste', 'MG', 'Natalandia', '0055'));
    l.add(new Cidades(3144409, '3 - Sudeste', 'MG', 'Natercia', '0055'));
    l.add(new Cidades(1714203, '1 - Norte', 'TO', 'Natividade', '0055'));
    l.add(new Cidades(3303104, '3 - Sudeste', 'RJ', 'Natividade', '0055'));
    l.add(new Cidades(
        3532306, '3 - Sudeste', 'SP', 'Natividade Da Serra', '0055'));
    l.add(new Cidades(2509909, '2 - Nordeste', 'PB', 'Natuba', '0055'));
    l.add(new Cidades(4211306, '4 - Sul', 'SC', 'Navegantes', '0055'));
    l.add(new Cidades(5005707, '5 - Centro-Oeste', 'MS', 'Navirai', '0055'));
    l.add(new Cidades(1714302, '1 - Norte', 'TO', 'Nazare', '0055'));
    l.add(new Cidades(2922508, '2 - Nordeste', 'BA', 'Nazare', '0055'));
    l.add(new Cidades(2609501, '2 - Nordeste', 'PE', 'Nazare Da Mata', '0055'));
    l.add(
        new Cidades(2206704, '2 - Nordeste', 'PI', 'Nazare Do Piaui', '0055'));
    l.add(new Cidades(3532405, '3 - Sudeste', 'SP', 'Nazare Paulista', '0055'));
    l.add(new Cidades(3144508, '3 - Sudeste', 'MG', 'Nazareno', '0055'));
    l.add(new Cidades(2510006, '2 - Nordeste', 'PB', 'Nazarezinho', '0055'));
    l.add(new Cidades(2206720, '2 - Nordeste', 'PI', 'Nazaria', '0055'));
    l.add(new Cidades(5214408, '5 - Centro-Oeste', 'GO', 'Nazario', '0055'));
    l.add(new Cidades(2804409, '2 - Nordeste', 'SE', 'Neopolis', '0055'));
    l.add(new Cidades(3144607, '3 - Sudeste', 'MG', 'Nepomuceno', '0055'));
    l.add(new Cidades(5214507, '5 - Centro-Oeste', 'GO', 'Neropolis', '0055'));
    l.add(new Cidades(3532504, '3 - Sudeste', 'SP', 'Neves Paulista', '0055'));
    l.add(new Cidades(1303007, '1 - Norte', 'AM', 'Nhamunda', '0055'));
    l.add(new Cidades(3532603, '3 - Sudeste', 'SP', 'Nhandeara', '0055'));
    l.add(new Cidades(4312674, '4 - Sul', 'RS', 'Nicolau Vergueiro', '0055'));
    l.add(new Cidades(2922607, '2 - Nordeste', 'BA', 'Nilo Pecanha', '0055'));
    l.add(new Cidades(3303203, '3 - Sudeste', 'RJ', 'Nilopolis', '0055'));
    l.add(new Cidades(2107209, '2 - Nordeste', 'MA', 'Nina Rodrigues', '0055'));
    l.add(new Cidades(3144656, '3 - Sudeste', 'MG', 'Ninheira', '0055'));
    l.add(new Cidades(5005806, '5 - Centro-Oeste', 'MS', 'Nioaque', '0055'));
    l.add(new Cidades(3532702, '3 - Sudeste', 'SP', 'Nipoa', '0055'));
    l.add(
        new Cidades(5214606, '5 - Centro-Oeste', 'GO', 'Niquelandia', '0055'));
    l.add(new Cidades(2408201, '2 - Nordeste', 'RN', 'Nisia Floresta', '0055'));
    l.add(new Cidades(3303302, '3 - Sudeste', 'RJ', 'Niteroi', '0055'));
    l.add(new Cidades(5105903, '5 - Centro-Oeste', 'MT', 'Nobres', '0055'));
    l.add(new Cidades(4312708, '4 - Sul', 'RS', 'Nonoai', '0055'));
    l.add(new Cidades(2922656, '2 - Nordeste', 'BA', 'Nordestina', '0055'));
    l.add(new Cidades(1400407, '1 - Norte', 'RR', 'Normandia', '0055'));
    l.add(
        new Cidades(5106000, '5 - Centro-Oeste', 'MT', 'Nortelandia', '0055'));
    l.add(new Cidades(
        2804458, '2 - Nordeste', 'SE', 'Nossa Senhora Aparecida', '0055'));
    l.add(new Cidades(
        2804508, '2 - Nordeste', 'SE', 'Nossa Senhora Da Gloria', '0055'));
    l.add(new Cidades(
        2804607, '2 - Nordeste', 'SE', 'Nossa Senhora Das Dores', '0055'));
    l.add(new Cidades(
        4116406, '4 - Sul', 'PR', 'Nossa Senhora Das Gracas', '0055'));
    l.add(new Cidades(
        2804706, '2 - Nordeste', 'SE', 'Nossa Senhora De Lourdes', '0055'));
    l.add(new Cidades(
        2206753, '2 - Nordeste', 'PI', 'Nossa Senhora De Nazare', '0055'));
    l.add(new Cidades(5106109, '5 - Centro-Oeste', 'MT',
        'Nossa Senhora Do Livramento', '0055'));
    l.add(new Cidades(
        2804805, '2 - Nordeste', 'SE', 'Nossa Senhora Do Socorro', '0055'));
    l.add(new Cidades(
        2206803, '2 - Nordeste', 'PI', 'Nossa Senhora Dos Remedios', '0055'));
    l.add(new Cidades(3532801, '3 - Sudeste', 'SP', 'Nova Alianca', '0055'));
    l.add(
        new Cidades(4116505, '4 - Sul', 'PR', 'Nova Alianca Do Ivai', '0055'));
    l.add(new Cidades(4312757, '4 - Sul', 'RS', 'Nova Alvorada', '0055'));
    l.add(new Cidades(
        5006002, '5 - Centro-Oeste', 'MS', 'Nova Alvorada Do Sul', '0055'));
    l.add(
        new Cidades(5214705, '5 - Centro-Oeste', 'GO', 'Nova America', '0055'));
    l.add(new Cidades(
        4116604, '4 - Sul', 'PR', 'Nova America Da Colina', '0055'));
    l.add(new Cidades(
        5006200, '5 - Centro-Oeste', 'MS', 'Nova Andradina', '0055'));
    l.add(new Cidades(4312807, '4 - Sul', 'RS', 'Nova Araca', '0055'));
    l.add(new Cidades(4116703, '4 - Sul', 'PR', 'Nova Aurora', '0055'));
    l.add(
        new Cidades(5214804, '5 - Centro-Oeste', 'GO', 'Nova Aurora', '0055'));
    l.add(new Cidades(
        5106158, '5 - Centro-Oeste', 'MT', 'Nova Bandeirantes', '0055'));
    l.add(new Cidades(4312906, '4 - Sul', 'RS', 'Nova Bassano', '0055'));
    l.add(new Cidades(3144672, '3 - Sudeste', 'MG', 'Nova Belem', '0055'));
    l.add(new Cidades(4312955, '4 - Sul', 'RS', 'Nova Boa Vista', '0055'));
    l.add(new Cidades(
        5106208, '5 - Centro-Oeste', 'MT', 'Nova Brasilandia', '0055'));
    l.add(new Cidades(
        1100148, '1 - Norte', 'RO', 'Nova Brasilandia D`Oeste', '0055'));
    l.add(new Cidades(4313003, '4 - Sul', 'RS', 'Nova Brescia', '0055'));
    l.add(new Cidades(3532827, '3 - Sudeste', 'SP', 'Nova Campina', '0055'));
    l.add(new Cidades(2922706, '2 - Nordeste', 'BA', 'Nova Canaa', '0055'));
    l.add(new Cidades(
        5106216, '5 - Centro-Oeste', 'MT', 'Nova Canaa Do Norte', '0055'));
    l.add(new Cidades(
        3532843, '3 - Sudeste', 'SP', 'Nova Canaa Paulista', '0055'));
    l.add(new Cidades(4313011, '4 - Sul', 'RS', 'Nova Candelaria', '0055'));
    l.add(new Cidades(4116802, '4 - Sul', 'PR', 'Nova Cantu', '0055'));
    l.add(new Cidades(3532868, '3 - Sudeste', 'SP', 'Nova Castilho', '0055'));
    l.add(new Cidades(2107258, '2 - Nordeste', 'MA', 'Nova Colinas', '0055'));
    l.add(
        new Cidades(5214838, '5 - Centro-Oeste', 'GO', 'Nova Crixas', '0055'));
    l.add(new Cidades(2408300, '2 - Nordeste', 'RN', 'Nova Cruz', '0055'));
    l.add(new Cidades(3144706, '3 - Sudeste', 'MG', 'Nova Era', '0055'));
    l.add(new Cidades(4211405, '4 - Sul', 'SC', 'Nova Erechim', '0055'));
    l.add(new Cidades(4116901, '4 - Sul', 'PR', 'Nova Esperanca', '0055'));
    l.add(new Cidades(
        1504950, '1 - Norte', 'PA', 'Nova Esperanca Do Piria', '0055'));
    l.add(new Cidades(
        4116950, '4 - Sul', 'PR', 'Nova Esperanca Do Sudoeste', '0055'));
    l.add(
        new Cidades(4313037, '4 - Sul', 'RS', 'Nova Esperanca Do Sul', '0055'));
    l.add(new Cidades(3532900, '3 - Sudeste', 'SP', 'Nova Europa', '0055'));
    l.add(new Cidades(2922730, '2 - Nordeste', 'BA', 'Nova Fatima', '0055'));
    l.add(new Cidades(4117008, '4 - Sul', 'PR', 'Nova Fatima', '0055'));
    l.add(new Cidades(2510105, '2 - Nordeste', 'PB', 'Nova Floresta', '0055'));
    l.add(new Cidades(3303401, '3 - Sudeste', 'RJ', 'Nova Friburgo', '0055'));
    l.add(
        new Cidades(5214861, '5 - Centro-Oeste', 'GO', 'Nova Gloria', '0055'));
    l.add(new Cidades(3533007, '3 - Sudeste', 'SP', 'Nova Granada', '0055'));
    l.add(
        new Cidades(5108808, '5 - Centro-Oeste', 'MT', 'Nova Guarita', '0055'));
    l.add(
        new Cidades(3533106, '3 - Sudeste', 'SP', 'Nova Guataporanga', '0055'));
    l.add(new Cidades(4313060, '4 - Sul', 'RS', 'Nova Hartz', '0055'));
    l.add(new Cidades(2922755, '2 - Nordeste', 'BA', 'Nova Ibia', '0055'));
    l.add(new Cidades(3303500, '3 - Sudeste', 'RJ', 'Nova Iguacu', '0055'));
    l.add(new Cidades(
        5214879, '5 - Centro-Oeste', 'GO', 'Nova Iguacu De Goias', '0055'));
    l.add(new Cidades(
        3533205, '3 - Sudeste', 'SP', 'Nova Independencia', '0055'));
    l.add(new Cidades(2107308, '2 - Nordeste', 'MA', 'Nova Iorque', '0055'));
    l.add(new Cidades(1504976, '1 - Norte', 'PA', 'Nova Ipixuna', '0055'));
    l.add(new Cidades(4211454, '4 - Sul', 'SC', 'Nova Itaberaba', '0055'));
    l.add(new Cidades(2922805, '2 - Nordeste', 'BA', 'Nova Itarana', '0055'));
    l.add(
        new Cidades(5106182, '5 - Centro-Oeste', 'MT', 'Nova Lacerda', '0055'));
    l.add(new Cidades(4117057, '4 - Sul', 'PR', 'Nova Laranjeiras', '0055'));
    l.add(new Cidades(3144805, '3 - Sudeste', 'MG', 'Nova Lima', '0055'));
    l.add(new Cidades(4117107, '4 - Sul', 'PR', 'Nova Londrina', '0055'));
    l.add(new Cidades(3533304, '3 - Sudeste', 'SP', 'Nova Luzitania', '0055'));
    l.add(new Cidades(1100338, '1 - Norte', 'RO', 'Nova Mamore', '0055'));
    l.add(new Cidades(
        5108857, '5 - Centro-Oeste', 'MT', 'Nova Marilandia', '0055'));
    l.add(
        new Cidades(5108907, '5 - Centro-Oeste', 'MT', 'Nova Maringa', '0055'));
    l.add(new Cidades(3144904, '3 - Sudeste', 'MG', 'Nova Modica', '0055'));
    l.add(new Cidades(
        5108956, '5 - Centro-Oeste', 'MT', 'Nova Monte Verde', '0055'));
    l.add(new Cidades(5106224, '5 - Centro-Oeste', 'MT', 'Nova Mutum', '0055'));
    l.add(
        new Cidades(5106174, '5 - Centro-Oeste', 'MT', 'Nova Nazare', '0055'));
    l.add(new Cidades(3533403, '3 - Sudeste', 'SP', 'Nova Odessa', '0055'));
    l.add(new Cidades(4117206, '4 - Sul', 'PR', 'Nova Olimpia', '0055'));
    l.add(
        new Cidades(5106232, '5 - Centro-Oeste', 'MT', 'Nova Olimpia', '0055'));
    l.add(new Cidades(1714880, '1 - Norte', 'TO', 'Nova Olinda', '0055'));
    l.add(new Cidades(2309201, '2 - Nordeste', 'CE', 'Nova Olinda', '0055'));
    l.add(new Cidades(2510204, '2 - Nordeste', 'PB', 'Nova Olinda', '0055'));
    l.add(new Cidades(
        2107357, '2 - Nordeste', 'MA', 'Nova Olinda Do Maranhao', '0055'));
    l.add(new Cidades(
        1303106, '1 - Norte', 'AM', 'Nova Olinda Do Norte', '0055'));
    l.add(new Cidades(4313086, '4 - Sul', 'RS', 'Nova Padua', '0055'));
    l.add(new Cidades(4313102, '4 - Sul', 'RS', 'Nova Palma', '0055'));
    l.add(new Cidades(2510303, '2 - Nordeste', 'PB', 'Nova Palmeira', '0055'));
    l.add(new Cidades(4313201, '4 - Sul', 'RS', 'Nova Petropolis', '0055'));
    l.add(new Cidades(3145000, '3 - Sudeste', 'MG', 'Nova Ponte', '0055'));
    l.add(
        new Cidades(3145059, '3 - Sudeste', 'MG', 'Nova Porteirinha', '0055'));
    l.add(new Cidades(4313300, '4 - Sul', 'RS', 'Nova Prata', '0055'));
    l.add(
        new Cidades(4117255, '4 - Sul', 'PR', 'Nova Prata Do Iguacu', '0055'));
    l.add(new Cidades(4313334, '4 - Sul', 'RS', 'Nova Ramada', '0055'));
    l.add(new Cidades(2922854, '2 - Nordeste', 'BA', 'Nova Redencao', '0055'));
    l.add(new Cidades(3145109, '3 - Sudeste', 'MG', 'Nova Resende', '0055'));
    l.add(new Cidades(5214903, '5 - Centro-Oeste', 'GO', 'Nova Roma', '0055'));
    l.add(new Cidades(4313359, '4 - Sul', 'RS', 'Nova Roma Do Sul', '0055'));
    l.add(new Cidades(1715002, '1 - Norte', 'TO', 'Nova Rosalandia', '0055'));
    l.add(new Cidades(2309300, '2 - Nordeste', 'CE', 'Nova Russas', '0055'));
    l.add(new Cidades(4117214, '4 - Sul', 'PR', 'Nova Santa Barbara', '0055'));
    l.add(new Cidades(
        5106190, '5 - Centro-Oeste', 'MT', 'Nova Santa Helena', '0055'));
    l.add(
        new Cidades(2207959, '2 - Nordeste', 'PI', 'Nova Santa Rita', '0055'));
    l.add(new Cidades(4313375, '4 - Sul', 'RS', 'Nova Santa Rita', '0055'));
    l.add(new Cidades(4117222, '4 - Sul', 'PR', 'Nova Santa Rosa', '0055'));
    l.add(new Cidades(3145208, '3 - Sudeste', 'MG', 'Nova Serrana', '0055'));
    l.add(new Cidades(2922904, '2 - Nordeste', 'BA', 'Nova Soure', '0055'));
    l.add(new Cidades(4117271, '4 - Sul', 'PR', 'Nova Tebas', '0055'));
    l.add(new Cidades(1505007, '1 - Norte', 'PA', 'Nova Timboteua', '0055'));
    l.add(new Cidades(4211504, '4 - Sul', 'SC', 'Nova Trento', '0055'));
    l.add(
        new Cidades(5106240, '5 - Centro-Oeste', 'MT', 'Nova Ubirata', '0055'));
    l.add(new Cidades(1101435, '1 - Norte', 'RO', 'Nova Uniao', '0055'));
    l.add(new Cidades(3136603, '3 - Sudeste', 'MG', 'Nova Uniao', '0055'));
    l.add(new Cidades(3203908, '3 - Sudeste', 'ES', 'Nova Venecia', '0055'));
    l.add(new Cidades(4211603, '4 - Sul', 'SC', 'Nova Veneza', '0055'));
    l.add(
        new Cidades(5215009, '5 - Centro-Oeste', 'GO', 'Nova Veneza', '0055'));
    l.add(new Cidades(2923001, '2 - Nordeste', 'BA', 'Nova Vicosa', '0055'));
    l.add(new Cidades(
        5106257, '5 - Centro-Oeste', 'MT', 'Nova Xavantina', '0055'));
    l.add(new Cidades(3533254, '3 - Sudeste', 'SP', 'Novais', '0055'));
    l.add(new Cidades(1715101, '1 - Norte', 'TO', 'Novo Acordo', '0055'));
    l.add(new Cidades(1303205, '1 - Norte', 'AM', 'Novo Airao', '0055'));
    l.add(new Cidades(1715150, '1 - Norte', 'TO', 'Novo Alegre', '0055'));
    l.add(new Cidades(1303304, '1 - Norte', 'AM', 'Novo Aripuana', '0055'));
    l.add(new Cidades(4313490, '4 - Sul', 'RS', 'Novo Barreiro', '0055'));
    l.add(
        new Cidades(5215207, '5 - Centro-Oeste', 'GO', 'Novo Brasil', '0055'));
    l.add(new Cidades(4313391, '4 - Sul', 'RS', 'Novo Cabrais', '0055'));
    l.add(new Cidades(3145307, '3 - Sudeste', 'MG', 'Novo Cruzeiro', '0055'));
    l.add(new Cidades(5215231, '5 - Centro-Oeste', 'GO', 'Novo Gama', '0055'));
    l.add(new Cidades(4313409, '4 - Sul', 'RS', 'Novo Hamburgo', '0055'));
    l.add(new Cidades(2923035, '2 - Nordeste', 'BA', 'Novo Horizonte', '0055'));
    l.add(new Cidades(3533502, '3 - Sudeste', 'SP', 'Novo Horizonte', '0055'));
    l.add(new Cidades(4211652, '4 - Sul', 'SC', 'Novo Horizonte', '0055'));
    l.add(new Cidades(
        5106273, '5 - Centro-Oeste', 'MT', 'Novo Horizonte Do Norte', '0055'));
    l.add(new Cidades(
        1100502, '1 - Norte', 'RO', 'Novo Horizonte Do Oeste', '0055'));
    l.add(new Cidades(
        5006259, '5 - Centro-Oeste', 'MS', 'Novo Horizonte Do Sul', '0055'));
    l.add(new Cidades(4117297, '4 - Sul', 'PR', 'Novo Itacolomi', '0055'));
    l.add(new Cidades(1715259, '1 - Norte', 'TO', 'Novo Jardim', '0055'));
    l.add(new Cidades(2705606, '2 - Nordeste', 'AL', 'Novo Lino', '0055'));
    l.add(new Cidades(4313425, '4 - Sul', 'RS', 'Novo Machado', '0055'));
    l.add(new Cidades(5106265, '5 - Centro-Oeste', 'MT', 'Novo Mundo', '0055'));
    l.add(new Cidades(2309409, '2 - Nordeste', 'CE', 'Novo Oriente', '0055'));
    l.add(new Cidades(
        3145356, '3 - Sudeste', 'MG', 'Novo Oriente De Minas', '0055'));
    l.add(new Cidades(
        2206902, '2 - Nordeste', 'PI', 'Novo Oriente Do Piaui', '0055'));
    l.add(new Cidades(
        5215256, '5 - Centro-Oeste', 'GO', 'Novo Planalto', '0055'));
    l.add(new Cidades(1505031, '1 - Norte', 'PA', 'Novo Progresso', '0055'));
    l.add(new Cidades(1505064, '1 - Norte', 'PA', 'Novo Repartimento', '0055'));
    l.add(new Cidades(
        2206951, '2 - Nordeste', 'PI', 'Novo Santo Antonio', '0055'));
    l.add(new Cidades(
        5106315, '5 - Centro-Oeste', 'MT', 'Novo Santo Antonio', '0055'));
    l.add(new Cidades(
        5106281, '5 - Centro-Oeste', 'MT', 'Novo Sao Joaquim', '0055'));
    l.add(new Cidades(4313441, '4 - Sul', 'RS', 'Novo Tiradentes', '0055'));
    l.add(new Cidades(2923050, '2 - Nordeste', 'BA', 'Novo Triunfo', '0055'));
    l.add(new Cidades(4313466, '4 - Sul', 'RS', 'Novo Xingu', '0055'));
    l.add(new Cidades(3145372, '3 - Sudeste', 'MG', 'Novorizonte', '0055'));
    l.add(new Cidades(3533601, '3 - Sudeste', 'SP', 'Nuporanga', '0055'));
    l.add(new Cidades(1505106, '1 - Norte', 'PA', 'Obidos', '0055'));
    l.add(new Cidades(2309458, '2 - Nordeste', 'CE', 'Ocara', '0055'));
    l.add(new Cidades(3533700, '3 - Sudeste', 'SP', 'Ocaucu', '0055'));
    l.add(new Cidades(2207009, '2 - Nordeste', 'PI', 'Oeiras', '0055'));
    l.add(new Cidades(1505205, '1 - Norte', 'PA', 'Oeiras Do Para', '0055'));
    l.add(new Cidades(1600501, '1 - Norte', 'AP', 'Oiapoque', '0055'));
    l.add(new Cidades(3145406, '3 - Sudeste', 'MG', 'Olaria', '0055'));
    l.add(new Cidades(3533809, '3 - Sudeste', 'SP', 'Oleo', '0055'));
    l.add(new Cidades(2510402, '2 - Nordeste', 'PB', 'Olho D`Agua', '0055'));
    l.add(new Cidades(
        2107407, '2 - Nordeste', 'MA', 'Olho D`Agua Das Cunhas', '0055'));
    l.add(new Cidades(
        2705705, '2 - Nordeste', 'AL', 'Olho D`Agua Das Flores', '0055'));
    l.add(new Cidades(
        2705804, '2 - Nordeste', 'AL', 'Olho D`Agua Do Casado', '0055'));
    l.add(new Cidades(
        2207108, '2 - Nordeste', 'PI', 'Olho D`Agua Do Piaui', '0055'));
    l.add(new Cidades(
        2705903, '2 - Nordeste', 'AL', 'Olho D`Agua Grande', '0055'));
    l.add(new Cidades(
        2408409, '2 - Nordeste', 'RN', 'Olho-D`Agua Do Borges', '0055'));
    l.add(new Cidades(3145455, '3 - Sudeste', 'MG', 'Olhos-D`Agua', '0055'));
    l.add(new Cidades(3533908, '3 - Sudeste', 'SP', 'Olimpia', '0055'));
    l.add(new Cidades(3145505, '3 - Sudeste', 'MG', 'Olimpio Noronha', '0055'));
    l.add(new Cidades(2609600, '2 - Nordeste', 'PE', 'Olinda', '0055'));
    l.add(new Cidades(
        2107456, '2 - Nordeste', 'MA', 'Olinda Nova Do Maranhao', '0055'));
    l.add(new Cidades(2923100, '2 - Nordeste', 'BA', 'Olindina', '0055'));
    l.add(new Cidades(2510501, '2 - Nordeste', 'PB', 'Olivedos', '0055'));
    l.add(new Cidades(3145604, '3 - Sudeste', 'MG', 'Oliveira', '0055'));
    l.add(
        new Cidades(1715507, '1 - Norte', 'TO', 'Oliveira De Fatima', '0055'));
    l.add(new Cidades(
        2923209, '2 - Nordeste', 'BA', 'Oliveira Dos Brejinhos', '0055'));
    l.add(new Cidades(3145703, '3 - Sudeste', 'MG', 'Oliveira Fortes', '0055'));
    l.add(new Cidades(2706000, '2 - Nordeste', 'AL', 'Olivenca', '0055'));
    l.add(
        new Cidades(3145802, '3 - Sudeste', 'MG', 'Onca De Pitangui', '0055'));
    l.add(new Cidades(3534005, '3 - Sudeste', 'SP', 'Onda Verde', '0055'));
    l.add(new Cidades(3145851, '3 - Sudeste', 'MG', 'Oratorios', '0055'));
    l.add(new Cidades(3534104, '3 - Sudeste', 'SP', 'Oriente', '0055'));
    l.add(new Cidades(3534203, '3 - Sudeste', 'SP', 'Orindiuva', '0055'));
    l.add(new Cidades(1505304, '1 - Norte', 'PA', 'Oriximina', '0055'));
    l.add(new Cidades(3145877, '3 - Sudeste', 'MG', 'Orizania', '0055'));
    l.add(new Cidades(5215306, '5 - Centro-Oeste', 'GO', 'Orizona', '0055'));
    l.add(new Cidades(3534302, '3 - Sudeste', 'SP', 'Orlandia', '0055'));
    l.add(new Cidades(4211702, '4 - Sul', 'SC', 'Orleans', '0055'));
    l.add(new Cidades(2609709, '2 - Nordeste', 'PE', 'Orobo', '0055'));
    l.add(new Cidades(2609808, '2 - Nordeste', 'PE', 'Oroco', '0055'));
    l.add(new Cidades(2309508, '2 - Nordeste', 'CE', 'Oros', '0055'));
    l.add(new Cidades(4117305, '4 - Sul', 'PR', 'Ortigueira', '0055'));
    l.add(new Cidades(3534401, '3 - Sudeste', 'SP', 'Osasco', '0055'));
    l.add(new Cidades(3534500, '3 - Sudeste', 'SP', 'Oscar Bressane', '0055'));
    l.add(new Cidades(4313508, '4 - Sul', 'RS', 'Osorio', '0055'));
    l.add(new Cidades(3534609, '3 - Sudeste', 'SP', 'Osvaldo Cruz', '0055'));
    l.add(new Cidades(4211751, '4 - Sul', 'SC', 'Otacilio Costa', '0055'));
    l.add(new Cidades(1505403, '1 - Norte', 'PA', 'Ourem', '0055'));
    l.add(new Cidades(2923308, '2 - Nordeste', 'BA', 'Ouricangas', '0055'));
    l.add(new Cidades(2609907, '2 - Nordeste', 'PE', 'Ouricuri', '0055'));
    l.add(
        new Cidades(1505437, '1 - Norte', 'PA', 'Ourilandia Do Norte', '0055'));
    l.add(new Cidades(3534708, '3 - Sudeste', 'SP', 'Ourinhos', '0055'));
    l.add(new Cidades(4117404, '4 - Sul', 'PR', 'Ourizona', '0055'));
    l.add(new Cidades(4211801, '4 - Sul', 'SC', 'Ouro', '0055'));
    l.add(new Cidades(2408508, '2 - Nordeste', 'RN', 'Ouro Branco', '0055'));
    l.add(new Cidades(2706109, '2 - Nordeste', 'AL', 'Ouro Branco', '0055'));
    l.add(new Cidades(3145901, '3 - Sudeste', 'MG', 'Ouro Branco', '0055'));
    l.add(new Cidades(3146008, '3 - Sudeste', 'MG', 'Ouro Fino', '0055'));
    l.add(new Cidades(3146107, '3 - Sudeste', 'MG', 'Ouro Preto', '0055'));
    l.add(
        new Cidades(1100155, '1 - Norte', 'RO', 'Ouro Preto Do Oeste', '0055'));
    l.add(new Cidades(2510600, '2 - Nordeste', 'PB', 'Ouro Velho', '0055'));
    l.add(new Cidades(3534807, '3 - Sudeste', 'SP', 'Ouro Verde', '0055'));
    l.add(new Cidades(4211850, '4 - Sul', 'SC', 'Ouro Verde', '0055'));
    l.add(new Cidades(
        5215405, '5 - Centro-Oeste', 'GO', 'Ouro Verde De Goias', '0055'));
    l.add(new Cidades(
        3146206, '3 - Sudeste', 'MG', 'Ouro Verde De Minas', '0055'));
    l.add(new Cidades(4117453, '4 - Sul', 'PR', 'Ouro Verde Do Oeste', '0055'));
    l.add(new Cidades(3534757, '3 - Sudeste', 'SP', 'Ouroeste', '0055'));
    l.add(new Cidades(2923357, '2 - Nordeste', 'BA', 'Ourolandia', '0055'));
    l.add(new Cidades(5215504, '5 - Centro-Oeste', 'GO', 'Ouvidor', '0055'));
    l.add(new Cidades(3534906, '3 - Sudeste', 'SP', 'Pacaembu', '0055'));
    l.add(new Cidades(1505486, '1 - Norte', 'PA', 'Pacaja', '0055'));
    l.add(new Cidades(2309607, '2 - Nordeste', 'CE', 'Pacajus', '0055'));
    l.add(new Cidades(1400456, '1 - Norte', 'RR', 'Pacaraima', '0055'));
    l.add(new Cidades(2309706, '2 - Nordeste', 'CE', 'Pacatuba', '0055'));
    l.add(new Cidades(2804904, '2 - Nordeste', 'SE', 'Pacatuba', '0055'));
    l.add(new Cidades(2107506, '2 - Nordeste', 'MA', 'Paco Do Lumiar', '0055'));
    l.add(new Cidades(2309805, '2 - Nordeste', 'CE', 'Pacoti', '0055'));
    l.add(new Cidades(2309904, '2 - Nordeste', 'CE', 'Pacuja', '0055'));
    l.add(new Cidades(
        5215603, '5 - Centro-Oeste', 'GO', 'Padre Bernardo', '0055'));
    l.add(new Cidades(3146255, '3 - Sudeste', 'MG', 'Padre Carvalho', '0055'));
    l.add(new Cidades(2207207, '2 - Nordeste', 'PI', 'Padre Marcos', '0055'));
    l.add(new Cidades(3146305, '3 - Sudeste', 'MG', 'Padre Paraiso', '0055'));
    l.add(new Cidades(2207306, '2 - Nordeste', 'PI', 'Paes Landim', '0055'));
    l.add(new Cidades(3146552, '3 - Sudeste', 'MG', 'Pai Pedro', '0055'));
    l.add(new Cidades(4211876, '4 - Sul', 'SC', 'Paial', '0055'));
    l.add(new Cidades(4117503, '4 - Sul', 'PR', 'Paicandu', '0055'));
    l.add(new Cidades(4313607, '4 - Sul', 'RS', 'Paim Filho', '0055'));
    l.add(new Cidades(3146404, '3 - Sudeste', 'MG', 'Paineiras', '0055'));
    l.add(new Cidades(4211892, '4 - Sul', 'SC', 'Painel', '0055'));
    l.add(new Cidades(3146503, '3 - Sudeste', 'MG', 'Pains', '0055'));
    l.add(new Cidades(3146602, '3 - Sudeste', 'MG', 'Paiva', '0055'));
    l.add(new Cidades(2207355, '2 - Nordeste', 'PI', 'Pajeu Do Piaui', '0055'));
    l.add(new Cidades(2706208, '2 - Nordeste', 'AL', 'Palestina', '0055'));
    l.add(new Cidades(3535002, '3 - Sudeste', 'SP', 'Palestina', '0055'));
    l.add(new Cidades(
        5215652, '5 - Centro-Oeste', 'GO', 'Palestina De Goias', '0055'));
    l.add(new Cidades(1505494, '1 - Norte', 'PA', 'Palestina Do Para', '0055'));
    l.add(new Cidades(2310001, '2 - Nordeste', 'CE', 'Palhano', '0055'));
    l.add(new Cidades(4211900, '4 - Sul', 'SC', 'Palhoca', '0055'));
    l.add(new Cidades(3146701, '3 - Sudeste', 'MG', 'Palma', '0055'));
    l.add(new Cidades(4212007, '4 - Sul', 'SC', 'Palma Sola', '0055'));
    l.add(new Cidades(2310100, '2 - Nordeste', 'CE', 'Palmacia', '0055'));
    l.add(new Cidades(2610004, '2 - Nordeste', 'PE', 'Palmares', '0055'));
    l.add(new Cidades(4313656, '4 - Sul', 'RS', 'Palmares Do Sul', '0055'));
    l.add(
        new Cidades(3535101, '3 - Sudeste', 'SP', 'Palmares Paulista', '0055'));
    l.add(new Cidades(1721000, '1 - Norte', 'TO', 'Palmas', '0055'));
    l.add(new Cidades(4117602, '4 - Sul', 'PR', 'Palmas', '0055'));
    l.add(new Cidades(
        2923407, '2 - Nordeste', 'BA', 'Palmas De Monte Alto', '0055'));
    l.add(new Cidades(4117701, '4 - Sul', 'PR', 'Palmeira', '0055'));
    l.add(new Cidades(4212056, '4 - Sul', 'SC', 'Palmeira', '0055'));
    l.add(
        new Cidades(3535200, '3 - Sudeste', 'SP', 'Palmeira D`Oeste', '0055'));
    l.add(
        new Cidades(4313706, '4 - Sul', 'RS', 'Palmeira Das Missoes', '0055'));
    l.add(new Cidades(
        2207405, '2 - Nordeste', 'PI', 'Palmeira Do Piaui', '0055'));
    l.add(new Cidades(
        2706307, '2 - Nordeste', 'AL', 'Palmeira Dos Indios', '0055'));
    l.add(new Cidades(2207504, '2 - Nordeste', 'PI', 'Palmeirais', '0055'));
    l.add(new Cidades(2107605, '2 - Nordeste', 'MA', 'Palmeirandia', '0055'));
    l.add(new Cidades(1715705, '1 - Norte', 'TO', 'Palmeirante', '0055'));
    l.add(new Cidades(2923506, '2 - Nordeste', 'BA', 'Palmeiras', '0055'));
    l.add(new Cidades(
        5215702, '5 - Centro-Oeste', 'GO', 'Palmeiras De Goias', '0055'));
    l.add(new Cidades(1713809, '1 - Norte', 'TO', 'Palmeiras Do To', '0055'));
    l.add(new Cidades(2610103, '2 - Nordeste', 'PE', 'Palmeirina', '0055'));
    l.add(new Cidades(1715754, '1 - Norte', 'TO', 'Palmeiropolis', '0055'));
    l.add(new Cidades(5215801, '5 - Centro-Oeste', 'GO', 'Palmelo', '0055'));
    l.add(
        new Cidades(5215900, '5 - Centro-Oeste', 'GO', 'Palminopolis', '0055'));
    l.add(new Cidades(3535309, '3 - Sudeste', 'SP', 'Palmital', '0055'));
    l.add(new Cidades(4117800, '4 - Sul', 'PR', 'Palmital', '0055'));
    l.add(new Cidades(4313805, '4 - Sul', 'RS', 'Palmitinho', '0055'));
    l.add(new Cidades(4212106, '4 - Sul', 'SC', 'Palmitos', '0055'));
    l.add(new Cidades(3146750, '3 - Sudeste', 'MG', 'Palmopolis', '0055'));
    l.add(new Cidades(4117909, '4 - Sul', 'PR', 'Palotina', '0055'));
    l.add(new Cidades(5216007, '5 - Centro-Oeste', 'GO', 'Panama', '0055'));
    l.add(new Cidades(4313904, '4 - Sul', 'RS', 'Panambi', '0055'));
    l.add(new Cidades(3204005, '3 - Sudeste', 'ES', 'Pancas', '0055'));
    l.add(new Cidades(2610202, '2 - Nordeste', 'PE', 'Panelas', '0055'));
    l.add(new Cidades(3535408, '3 - Sudeste', 'SP', 'Panorama', '0055'));
    l.add(new Cidades(4313953, '4 - Sul', 'RS', 'Pantano Grande', '0055'));
    l.add(new Cidades(2706406, '2 - Nordeste', 'AL', 'Pao De Acucar', '0055'));
    l.add(new Cidades(3146909, '3 - Sudeste', 'MG', 'Papagaios', '0055'));
    l.add(new Cidades(4212205, '4 - Sul', 'SC', 'Papanduva', '0055'));
    l.add(new Cidades(2207553, '2 - Nordeste', 'PI', 'Paqueta', '0055'));
    l.add(new Cidades(3147105, '3 - Sudeste', 'MG', 'Para De Minas', '0055'));
    l.add(new Cidades(3303609, '3 - Sudeste', 'RJ', 'Paracambi', '0055'));
    l.add(new Cidades(3147006, '3 - Sudeste', 'MG', 'Paracatu', '0055'));
    l.add(new Cidades(2310209, '2 - Nordeste', 'CE', 'Paracuru', '0055'));
    l.add(new Cidades(1505502, '1 - Norte', 'PA', 'Paragominas', '0055'));
    l.add(new Cidades(3147204, '3 - Sudeste', 'MG', 'Paraguacu', '0055'));
    l.add(new Cidades(
        3535507, '3 - Sudeste', 'SP', 'Paraguacu Paulista', '0055'));
    l.add(new Cidades(4314001, '4 - Sul', 'RS', 'Parai', '0055'));
    l.add(new Cidades(3303708, '3 - Sudeste', 'RJ', 'Paraiba Do Sul', '0055'));
    l.add(new Cidades(2107704, '2 - Nordeste', 'MA', 'Paraibano', '0055'));
    l.add(new Cidades(3535606, '3 - Sudeste', 'SP', 'Paraibuna', '0055'));
    l.add(new Cidades(2310258, '2 - Nordeste', 'CE', 'Paraipaba', '0055'));
    l.add(new Cidades(3535705, '3 - Sudeste', 'SP', 'Paraiso', '0055'));
    l.add(new Cidades(4212239, '4 - Sul', 'SC', 'Paraiso', '0055'));
    l.add(new Cidades(
        5006275, '5 - Centro-Oeste', 'MS', 'Paraiso Das Aguas', '0055'));
    l.add(new Cidades(4118006, '4 - Sul', 'PR', 'Paraiso Do Norte', '0055'));
    l.add(new Cidades(4314027, '4 - Sul', 'RS', 'Paraiso Do Sul', '0055'));
    l.add(new Cidades(1716109, '1 - Norte', 'TO', 'Paraiso Do To', '0055'));
    l.add(new Cidades(3147303, '3 - Sudeste', 'MG', 'Paraisopolis', '0055'));
    l.add(new Cidades(2310308, '2 - Nordeste', 'CE', 'Parambu', '0055'));
    l.add(new Cidades(2923605, '2 - Nordeste', 'BA', 'Paramirim', '0055'));
    l.add(new Cidades(2310407, '2 - Nordeste', 'CE', 'Paramoti', '0055'));
    l.add(new Cidades(1716208, '1 - Norte', 'TO', 'Parana', '0055'));
    l.add(new Cidades(2408607, '2 - Nordeste', 'RN', 'Parana', '0055'));
    l.add(new Cidades(4118105, '4 - Sul', 'PR', 'Paranacity', '0055'));
    l.add(new Cidades(4118204, '4 - Sul', 'PR', 'Paranagua', '0055'));
    l.add(new Cidades(5006309, '5 - Centro-Oeste', 'MS', 'Paranaiba', '0055'));
    l.add(
        new Cidades(5216304, '5 - Centro-Oeste', 'GO', 'Paranaiguara', '0055'));
    l.add(new Cidades(5106299, '5 - Centro-Oeste', 'MT', 'Paranaita', '0055'));
    l.add(new Cidades(3535804, '3 - Sudeste', 'SP', 'Paranapanema', '0055'));
    l.add(new Cidades(4118303, '4 - Sul', 'PR', 'Paranapoema', '0055'));
    l.add(new Cidades(3535903, '3 - Sudeste', 'SP', 'Paranapua', '0055'));
    l.add(new Cidades(2610301, '2 - Nordeste', 'PE', 'Paranatama', '0055'));
    l.add(
        new Cidades(5106307, '5 - Centro-Oeste', 'MT', 'Paranatinga', '0055'));
    l.add(new Cidades(4118402, '4 - Sul', 'PR', 'Paranavai', '0055'));
    l.add(new Cidades(5006358, '5 - Centro-Oeste', 'MS', 'Paranhos', '0055'));
    l.add(new Cidades(3147402, '3 - Sudeste', 'MG', 'Paraopeba', '0055'));
    l.add(new Cidades(3536000, '3 - Sudeste', 'SP', 'Parapua', '0055'));
    l.add(new Cidades(2510659, '2 - Nordeste', 'PB', 'Parari', '0055'));
    l.add(new Cidades(2923704, '2 - Nordeste', 'BA', 'Paratinga', '0055'));
    l.add(new Cidades(3303807, '3 - Sudeste', 'RJ', 'Paraty', '0055'));
    l.add(new Cidades(2408706, '2 - Nordeste', 'RN', 'Parau', '0055'));
    l.add(new Cidades(1505536, '1 - Norte', 'PA', 'Parauapebas', '0055'));
    l.add(new Cidades(5216403, '5 - Centro-Oeste', 'GO', 'Parauna', '0055'));
    l.add(new Cidades(2408805, '2 - Nordeste', 'RN', 'Parazinho', '0055'));
    l.add(new Cidades(3536109, '3 - Sudeste', 'SP', 'Pardinho', '0055'));
    l.add(new Cidades(4314035, '4 - Sul', 'RS', 'Pareci Novo', '0055'));
    l.add(new Cidades(1101450, '1 - Norte', 'RO', 'Parecis', '0055'));
    l.add(new Cidades(2408904, '2 - Nordeste', 'RN', 'Parelhas', '0055'));
    l.add(new Cidades(2706422, '2 - Nordeste', 'AL', 'Pariconha', '0055'));
    l.add(new Cidades(1303403, '1 - Norte', 'AM', 'Parintins', '0055'));
    l.add(new Cidades(2923803, '2 - Nordeste', 'BA', 'Paripiranga', '0055'));
    l.add(new Cidades(2706448, '2 - Nordeste', 'AL', 'Paripueira', '0055'));
    l.add(new Cidades(3536208, '3 - Sudeste', 'SP', 'Pariquera-Acu', '0055'));
    l.add(new Cidades(3536257, '3 - Sudeste', 'SP', 'Parisi', '0055'));
    l.add(new Cidades(2207603, '2 - Nordeste', 'PI', 'Parnagua', '0055'));
    l.add(new Cidades(2207702, '2 - Nordeste', 'PI', 'Parnaiba', '0055'));
    l.add(new Cidades(2403251, '2 - Nordeste', 'RN', 'Parnamirim', '0055'));
    l.add(new Cidades(2610400, '2 - Nordeste', 'PE', 'Parnamirim', '0055'));
    l.add(new Cidades(2107803, '2 - Nordeste', 'MA', 'Parnarama', '0055'));
    l.add(new Cidades(4314050, '4 - Sul', 'RS', 'Parobe', '0055'));
    l.add(new Cidades(2409100, '2 - Nordeste', 'RN', 'Passa E Fica', '0055'));
    l.add(new Cidades(3147600, '3 - Sudeste', 'MG', 'Passa Quatro', '0055'));
    l.add(new Cidades(4314068, '4 - Sul', 'RS', 'Passa Sete', '0055'));
    l.add(new Cidades(3147709, '3 - Sudeste', 'MG', 'Passa Tempo', '0055'));
    l.add(new Cidades(3147808, '3 - Sudeste', 'MG', 'Passa-Vinte', '0055'));
    l.add(new Cidades(3147501, '3 - Sudeste', 'MG', 'Passabem', '0055'));
    l.add(new Cidades(2409209, '2 - Nordeste', 'RN', 'Passagem', '0055'));
    l.add(new Cidades(2510709, '2 - Nordeste', 'PB', 'Passagem', '0055'));
    l.add(
        new Cidades(2107902, '2 - Nordeste', 'MA', 'Passagem Franca', '0055'));
    l.add(new Cidades(
        2207751, '2 - Nordeste', 'PI', 'Passagem Franca Do Piaui', '0055'));
    l.add(new Cidades(2610509, '2 - Nordeste', 'PE', 'Passira', '0055'));
    l.add(new Cidades(
        2706505, '2 - Nordeste', 'AL', 'Passo De Camaragibe', '0055'));
    l.add(new Cidades(4212254, '4 - Sul', 'SC', 'Passo De Torres', '0055'));
    l.add(new Cidades(4314076, '4 - Sul', 'RS', 'Passo Do Sobrado', '0055'));
    l.add(new Cidades(4314100, '4 - Sul', 'RS', 'Passo Fundo', '0055'));
    l.add(new Cidades(3147907, '3 - Sudeste', 'MG', 'Passos', '0055'));
    l.add(new Cidades(4212270, '4 - Sul', 'SC', 'Passos Maia', '0055'));
    l.add(new Cidades(2108009, '2 - Nordeste', 'MA', 'Pastos Bons', '0055'));
    l.add(new Cidades(3147956, '3 - Sudeste', 'MG', 'Patis', '0055'));
    l.add(new Cidades(4118451, '4 - Sul', 'PR', 'Pato Bragado', '0055'));
    l.add(new Cidades(4118501, '4 - Sul', 'PR', 'Pato Branco', '0055'));
    l.add(new Cidades(2510808, '2 - Nordeste', 'PB', 'Patos', '0055'));
    l.add(new Cidades(3148004, '3 - Sudeste', 'MG', 'Patos De Minas', '0055'));
    l.add(new Cidades(2207777, '2 - Nordeste', 'PI', 'Patos Do Piaui', '0055'));
    l.add(new Cidades(3148103, '3 - Sudeste', 'MG', 'Patrocinio', '0055'));
    l.add(new Cidades(
        3148202, '3 - Sudeste', 'MG', 'Patrocinio Do Muriae', '0055'));
    l.add(new Cidades(
        3536307, '3 - Sudeste', 'SP', 'Patrocinio Paulista', '0055'));
    l.add(new Cidades(2409308, '2 - Nordeste', 'RN', 'Patu', '0055'));
    l.add(new Cidades(3303856, '3 - Sudeste', 'RJ', 'Paty Do Alferes', '0055'));
    l.add(new Cidades(2923902, '2 - Nordeste', 'BA', 'Pau Brasil', '0055'));
    l.add(new Cidades(1505551, '1 - Norte', 'PA', 'Pau D`Arco', '0055'));
    l.add(new Cidades(1716307, '1 - Norte', 'TO', 'Pau D`Arco', '0055'));
    l.add(new Cidades(
        2207793, '2 - Nordeste', 'PI', 'Pau D`Arco Do Piaui', '0055'));
    l.add(new Cidades(2409407, '2 - Nordeste', 'RN', 'Pau Dos Ferros', '0055'));
    l.add(new Cidades(2610608, '2 - Nordeste', 'PE', 'Paudalho', '0055'));
    l.add(new Cidades(1303502, '1 - Norte', 'AM', 'Pauini', '0055'));
    l.add(new Cidades(3148301, '3 - Sudeste', 'MG', 'Paula Candido', '0055'));
    l.add(new Cidades(4118600, '4 - Sul', 'PR', 'Paula Freitas', '0055'));
    l.add(new Cidades(3536406, '3 - Sudeste', 'SP', 'Pauliceia', '0055'));
    l.add(new Cidades(3536505, '3 - Sudeste', 'SP', 'Paulinia', '0055'));
    l.add(new Cidades(2108058, '2 - Nordeste', 'MA', 'Paulino Neves', '0055'));
    l.add(new Cidades(2510907, '2 - Nordeste', 'PB', 'Paulista', '0055'));
    l.add(new Cidades(2610707, '2 - Nordeste', 'PE', 'Paulista', '0055'));
    l.add(new Cidades(2207801, '2 - Nordeste', 'PI', 'Paulistana', '0055'));
    l.add(new Cidades(3536570, '3 - Sudeste', 'SP', 'Paulistania', '0055'));
    l.add(new Cidades(3148400, '3 - Sudeste', 'MG', 'Paulistas', '0055'));
    l.add(new Cidades(2924009, '2 - Nordeste', 'BA', 'Paulo Afonso', '0055'));
    l.add(new Cidades(4314134, '4 - Sul', 'RS', 'Paulo Bento', '0055'));
    l.add(new Cidades(3536604, '3 - Sudeste', 'SP', 'Paulo De Faria', '0055'));
    l.add(new Cidades(4118709, '4 - Sul', 'PR', 'Paulo Frontin', '0055'));
    l.add(new Cidades(2706604, '2 - Nordeste', 'AL', 'Paulo Jacinto', '0055'));
    l.add(new Cidades(4212304, '4 - Sul', 'SC', 'Paulo Lopes', '0055'));
    l.add(new Cidades(2108108, '2 - Nordeste', 'MA', 'Paulo Ramos', '0055'));
    l.add(new Cidades(3148509, '3 - Sudeste', 'MG', 'Pavao', '0055'));
    l.add(new Cidades(4314159, '4 - Sul', 'RS', 'Paverama', '0055'));
    l.add(new Cidades(2207850, '2 - Nordeste', 'PI', 'Pavussu', '0055'));
    l.add(new Cidades(2924058, '2 - Nordeste', 'BA', 'Pe De Serra', '0055'));
    l.add(new Cidades(4118808, '4 - Sul', 'PR', 'Peabiru', '0055'));
    l.add(new Cidades(3148608, '3 - Sudeste', 'MG', 'Pecanha', '0055'));
    l.add(new Cidades(3536703, '3 - Sudeste', 'SP', 'Pederneiras', '0055'));
    l.add(new Cidades(2610806, '2 - Nordeste', 'PE', 'Pedra', '0055'));
    l.add(new Cidades(3148707, '3 - Sudeste', 'MG', 'Pedra Azul', '0055'));
    l.add(new Cidades(3536802, '3 - Sudeste', 'SP', 'Pedra Bela', '0055'));
    l.add(new Cidades(3148756, '3 - Sudeste', 'MG', 'Pedra Bonita', '0055'));
    l.add(new Cidades(2310506, '2 - Nordeste', 'CE', 'Pedra Branca', '0055'));
    l.add(new Cidades(2511004, '2 - Nordeste', 'PB', 'Pedra Branca', '0055'));
    l.add(new Cidades(
        1600154, '1 - Norte', 'AP', 'Pedra Branca Do Amapari', '0055'));
    l.add(new Cidades(3148806, '3 - Sudeste', 'MG', 'Pedra Do Anta', '0055'));
    l.add(new Cidades(3148905, '3 - Sudeste', 'MG', 'Pedra Do Indaia', '0055'));
    l.add(new Cidades(3149002, '3 - Sudeste', 'MG', 'Pedra Dourada', '0055'));
    l.add(new Cidades(2409506, '2 - Nordeste', 'RN', 'Pedra Grande', '0055'));
    l.add(new Cidades(2511103, '2 - Nordeste', 'PB', 'Pedra Lavrada', '0055'));
    l.add(new Cidades(2805000, '2 - Nordeste', 'SE', 'Pedra Mole', '0055'));
    l.add(new Cidades(2409605, '2 - Nordeste', 'RN', 'Pedra Preta', '0055'));
    l.add(
        new Cidades(5106372, '5 - Centro-Oeste', 'MT', 'Pedra Preta', '0055'));
    l.add(new Cidades(3149101, '3 - Sudeste', 'MG', 'Pedralva', '0055'));
    l.add(new Cidades(3536901, '3 - Sudeste', 'SP', 'Pedranopolis', '0055'));
    l.add(new Cidades(2924108, '2 - Nordeste', 'BA', 'Pedrao', '0055'));
    l.add(new Cidades(4314175, '4 - Sul', 'RS', 'Pedras Altas', '0055'));
    l.add(new Cidades(2511202, '2 - Nordeste', 'PB', 'Pedras De Fogo', '0055'));
    l.add(new Cidades(
        3149150, '3 - Sudeste', 'MG', 'Pedras De Maria Da Cruz', '0055'));
    l.add(new Cidades(4212403, '4 - Sul', 'SC', 'Pedras Grandes', '0055'));
    l.add(new Cidades(3537008, '3 - Sudeste', 'SP', 'Pedregulho', '0055'));
    l.add(new Cidades(3537107, '3 - Sudeste', 'SP', 'Pedreira', '0055'));
    l.add(new Cidades(2108207, '2 - Nordeste', 'MA', 'Pedreiras', '0055'));
    l.add(new Cidades(2805109, '2 - Nordeste', 'SE', 'Pedrinhas', '0055'));
    l.add(new Cidades(
        3537156, '3 - Sudeste', 'SP', 'Pedrinhas Paulista', '0055'));
    l.add(new Cidades(3149200, '3 - Sudeste', 'MG', 'Pedrinopolis', '0055'));
    l.add(new Cidades(1716505, '1 - Norte', 'TO', 'Pedro Afonso', '0055'));
    l.add(
        new Cidades(2924207, '2 - Nordeste', 'BA', 'Pedro Alexandre', '0055'));
    l.add(new Cidades(2409704, '2 - Nordeste', 'RN', 'Pedro Avelino', '0055'));
    l.add(new Cidades(3204054, '3 - Sudeste', 'ES', 'Pedro Canario', '0055'));
    l.add(new Cidades(3537206, '3 - Sudeste', 'SP', 'Pedro De Toledo', '0055'));
    l.add(
        new Cidades(2108256, '2 - Nordeste', 'MA', 'Pedro Do Rosario', '0055'));
    l.add(
        new Cidades(5006408, '5 - Centro-Oeste', 'MS', 'Pedro Gomes', '0055'));
    l.add(new Cidades(2207900, '2 - Nordeste', 'PI', 'Pedro Ii', '0055'));
    l.add(
        new Cidades(2207934, '2 - Nordeste', 'PI', 'Pedro Laurentino', '0055'));
    l.add(new Cidades(3149309, '3 - Sudeste', 'MG', 'Pedro Leopoldo', '0055'));
    l.add(new Cidades(4314209, '4 - Sul', 'RS', 'Pedro Osorio', '0055'));
    l.add(new Cidades(2512721, '2 - Nordeste', 'PB', 'Pedro Regis', '0055'));
    l.add(new Cidades(3149408, '3 - Sudeste', 'MG', 'Pedro Teixeira', '0055'));
    l.add(new Cidades(2409803, '2 - Nordeste', 'RN', 'Pedro Velho', '0055'));
    l.add(new Cidades(1716604, '1 - Norte', 'TO', 'Peixe', '0055'));
    l.add(new Cidades(1505601, '1 - Norte', 'PA', 'Peixe-Boi', '0055'));
    l.add(new Cidades(
        5106422, '5 - Centro-Oeste', 'MT', 'Peixoto De Azevedo', '0055'));
    l.add(new Cidades(4314308, '4 - Sul', 'RS', 'Pejucara', '0055'));
    l.add(new Cidades(4314407, '4 - Sul', 'RS', 'Pelotas', '0055'));
    l.add(new Cidades(2310605, '2 - Nordeste', 'CE', 'Penaforte', '0055'));
    l.add(new Cidades(2108306, '2 - Nordeste', 'MA', 'Penalva', '0055'));
    l.add(new Cidades(3537305, '3 - Sudeste', 'SP', 'Penapolis', '0055'));
    l.add(new Cidades(2409902, '2 - Nordeste', 'RN', 'Pendencias', '0055'));
    l.add(new Cidades(2706703, '2 - Nordeste', 'AL', 'Penedo', '0055'));
    l.add(new Cidades(4212502, '4 - Sul', 'SC', 'Penha', '0055'));
    l.add(new Cidades(2310704, '2 - Nordeste', 'CE', 'Pentecoste', '0055'));
    l.add(new Cidades(3149507, '3 - Sudeste', 'MG', 'Pequeri', '0055'));
    l.add(new Cidades(3149606, '3 - Sudeste', 'MG', 'Pequi', '0055'));
    l.add(new Cidades(1716653, '1 - Norte', 'TO', 'Pequizeiro', '0055'));
    l.add(new Cidades(3149705, '3 - Sudeste', 'MG', 'Perdigao', '0055'));
    l.add(new Cidades(3149804, '3 - Sudeste', 'MG', 'Perdizes', '0055'));
    l.add(new Cidades(3149903, '3 - Sudeste', 'MG', 'Perdoes', '0055'));
    l.add(new Cidades(3537404, '3 - Sudeste', 'SP', 'Pereira Barreto', '0055'));
    l.add(new Cidades(3537503, '3 - Sudeste', 'SP', 'Pereiras', '0055'));
    l.add(new Cidades(2310803, '2 - Nordeste', 'CE', 'Pereiro', '0055'));
    l.add(new Cidades(2108405, '2 - Nordeste', 'MA', 'Peri Mirim', '0055'));
    l.add(new Cidades(3149952, '3 - Sudeste', 'MG', 'Periquito', '0055'));
    l.add(new Cidades(4212601, '4 - Sul', 'SC', 'Peritiba', '0055'));
    l.add(new Cidades(2108454, '2 - Nordeste', 'MA', 'Peritoro', '0055'));
    l.add(new Cidades(4118857, '4 - Sul', 'PR', 'Perobal', '0055'));
    l.add(new Cidades(4118907, '4 - Sul', 'PR', 'Perola', '0055'));
    l.add(new Cidades(4119004, '4 - Sul', 'PR', 'Perola D`Oeste', '0055'));
    l.add(new Cidades(5216452, '5 - Centro-Oeste', 'GO', 'Perolandia', '0055'));
    l.add(new Cidades(3537602, '3 - Sudeste', 'SP', 'Peruibe', '0055'));
    l.add(new Cidades(3150000, '3 - Sudeste', 'MG', 'Pescador', '0055'));
    l.add(new Cidades(4212650, '4 - Sul', 'SC', 'Pescaria Brava', '0055'));
    l.add(new Cidades(2610905, '2 - Nordeste', 'PE', 'Pesqueira', '0055'));
    l.add(new Cidades(2611002, '2 - Nordeste', 'PE', 'Petrolandia', '0055'));
    l.add(new Cidades(4212700, '4 - Sul', 'SC', 'Petrolandia', '0055'));
    l.add(new Cidades(2611101, '2 - Nordeste', 'PE', 'Petrolina', '0055'));
    l.add(new Cidades(
        5216809, '5 - Centro-Oeste', 'GO', 'Petrolina De Goias', '0055'));
    l.add(new Cidades(3303906, '3 - Sudeste', 'RJ', 'Petropolis', '0055'));
    l.add(new Cidades(2706802, '2 - Nordeste', 'AL', 'Piacabucu', '0055'));
    l.add(new Cidades(3537701, '3 - Sudeste', 'SP', 'Piacatu', '0055'));
    l.add(new Cidades(2511301, '2 - Nordeste', 'PB', 'Pianco', '0055'));
    l.add(new Cidades(2924306, '2 - Nordeste', 'BA', 'Piata', '0055'));
    l.add(new Cidades(3150109, '3 - Sudeste', 'MG', 'Piau', '0055'));
    l.add(new Cidades(4314423, '4 - Sul', 'RS', 'Picada Cafe', '0055'));
    l.add(new Cidades(1505635, '1 - Norte', 'PA', 'Picarra', '0055'));
    l.add(new Cidades(2208007, '2 - Nordeste', 'PI', 'Picos', '0055'));
    l.add(new Cidades(2511400, '2 - Nordeste', 'PB', 'Picui', '0055'));
    l.add(new Cidades(3537800, '3 - Sudeste', 'SP', 'Piedade', '0055'));
    l.add(new Cidades(
        3150158, '3 - Sudeste', 'MG', 'Piedade De Caratinga', '0055'));
    l.add(new Cidades(
        3150208, '3 - Sudeste', 'MG', 'Piedade De Ponte Nova', '0055'));
    l.add(new Cidades(
        3150307, '3 - Sudeste', 'MG', 'Piedade Do Rio Grande', '0055'));
    l.add(new Cidades(
        3150406, '3 - Sudeste', 'MG', 'Piedade Dos Gerais', '0055'));
    l.add(new Cidades(4119103, '4 - Sul', 'PR', 'Pien', '0055'));
    l.add(new Cidades(2924405, '2 - Nordeste', 'BA', 'Pilao Arcado', '0055'));
    l.add(new Cidades(2511509, '2 - Nordeste', 'PB', 'Pilar', '0055'));
    l.add(new Cidades(2706901, '2 - Nordeste', 'AL', 'Pilar', '0055'));
    l.add(new Cidades(
        5216908, '5 - Centro-Oeste', 'GO', 'Pilar De Goias', '0055'));
    l.add(new Cidades(3537909, '3 - Sudeste', 'SP', 'Pilar Do Sul', '0055'));
    l.add(new Cidades(2410009, '2 - Nordeste', 'RN', 'Piloes', '0055'));
    l.add(new Cidades(2511608, '2 - Nordeste', 'PB', 'Piloes', '0055'));
    l.add(new Cidades(2511707, '2 - Nordeste', 'PB', 'Piloezinhos', '0055'));
    l.add(new Cidades(3150505, '3 - Sudeste', 'MG', 'Pimenta', '0055'));
    l.add(new Cidades(1100189, '1 - Norte', 'RO', 'Pimenta Bueno', '0055'));
    l.add(new Cidades(2208106, '2 - Nordeste', 'PI', 'Pimenteiras', '0055'));
    l.add(new Cidades(
        1101468, '1 - Norte', 'RO', 'Pimenteiras Do Oeste', '0055'));
    l.add(new Cidades(2924504, '2 - Nordeste', 'BA', 'Pindai', '0055'));
    l.add(new Cidades(3538006, '3 - Sudeste', 'SP', 'Pindamonhangaba', '0055'));
    l.add(new Cidades(2108504, '2 - Nordeste', 'MA', 'Pindare-Mirim', '0055'));
    l.add(new Cidades(2707008, '2 - Nordeste', 'AL', 'Pindoba', '0055'));
    l.add(new Cidades(2924603, '2 - Nordeste', 'BA', 'Pindobacu', '0055'));
    l.add(new Cidades(3538105, '3 - Sudeste', 'SP', 'Pindorama', '0055'));
    l.add(new Cidades(1717008, '1 - Norte', 'TO', 'Pindorama Do To', '0055'));
    l.add(new Cidades(2310852, '2 - Nordeste', 'CE', 'Pindoretama', '0055'));
    l.add(new Cidades(3150539, '3 - Sudeste', 'MG', 'Pingo-D`Agua', '0055'));
    l.add(new Cidades(4119152, '4 - Sul', 'PR', 'Pinhais', '0055'));
    l.add(new Cidades(4314456, '4 - Sul', 'RS', 'Pinhal', '0055'));
    l.add(new Cidades(4314464, '4 - Sul', 'RS', 'Pinhal Da Serra', '0055'));
    l.add(new Cidades(4119251, '4 - Sul', 'PR', 'Pinhal De Sao Bento', '0055'));
    l.add(new Cidades(4314472, '4 - Sul', 'RS', 'Pinhal Grande', '0055'));
    l.add(new Cidades(4119202, '4 - Sul', 'PR', 'Pinhalao', '0055'));
    l.add(new Cidades(3538204, '3 - Sudeste', 'SP', 'Pinhalzinho', '0055'));
    l.add(new Cidades(4212908, '4 - Sul', 'SC', 'Pinhalzinho', '0055'));
    l.add(new Cidades(2805208, '2 - Nordeste', 'SE', 'Pinhao', '0055'));
    l.add(new Cidades(4119301, '4 - Sul', 'PR', 'Pinhao', '0055'));
    l.add(new Cidades(3303955, '3 - Sudeste', 'RJ', 'Pinheiral', '0055'));
    l.add(new Cidades(4314498, '4 - Sul', 'RS', 'Pinheirinho Do Vale', '0055'));
    l.add(new Cidades(2108603, '2 - Nordeste', 'MA', 'Pinheiro', '0055'));
    l.add(new Cidades(4314506, '4 - Sul', 'RS', 'Pinheiro Machado', '0055'));
    l.add(new Cidades(4213005, '4 - Sul', 'SC', 'Pinheiro Preto', '0055'));
    l.add(new Cidades(3204104, '3 - Sudeste', 'ES', 'Pinheiros', '0055'));
    l.add(new Cidades(2924652, '2 - Nordeste', 'BA', 'Pintadas', '0055'));
    l.add(new Cidades(4314548, '4 - Sul', 'RS', 'Pinto Bandeira', '0055'));
    l.add(new Cidades(3150570, '3 - Sudeste', 'MG', 'Pintopolis', '0055'));
    l.add(new Cidades(2208205, '2 - Nordeste', 'PI', 'Pio Ix', '0055'));
    l.add(new Cidades(2108702, '2 - Nordeste', 'MA', 'Pio Xii', '0055'));
    l.add(new Cidades(3538303, '3 - Sudeste', 'SP', 'Piquerobi', '0055'));
    l.add(
        new Cidades(2310902, '2 - Nordeste', 'CE', 'Piquet Carneiro', '0055'));
    l.add(new Cidades(3538501, '3 - Sudeste', 'SP', 'Piquete', '0055'));
    l.add(new Cidades(3538600, '3 - Sudeste', 'SP', 'Piracaia', '0055'));
    l.add(
        new Cidades(5217104, '5 - Centro-Oeste', 'GO', 'Piracanjuba', '0055'));
    l.add(new Cidades(3150604, '3 - Sudeste', 'MG', 'Piracema', '0055'));
    l.add(new Cidades(3538709, '3 - Sudeste', 'SP', 'Piracicaba', '0055'));
    l.add(new Cidades(2208304, '2 - Nordeste', 'PI', 'Piracuruca', '0055'));
    l.add(new Cidades(3304003, '3 - Sudeste', 'RJ', 'Pirai', '0055'));
    l.add(new Cidades(2924678, '2 - Nordeste', 'BA', 'Pirai Do Norte', '0055'));
    l.add(new Cidades(4119400, '4 - Sul', 'PR', 'Pirai Do Sul', '0055'));
    l.add(new Cidades(3538808, '3 - Sudeste', 'SP', 'Piraju', '0055'));
    l.add(new Cidades(3150703, '3 - Sudeste', 'MG', 'Pirajuba', '0055'));
    l.add(new Cidades(3538907, '3 - Sudeste', 'SP', 'Pirajui', '0055'));
    l.add(new Cidades(2805307, '2 - Nordeste', 'SE', 'Pirambu', '0055'));
    l.add(new Cidades(3150802, '3 - Sudeste', 'MG', 'Piranga', '0055'));
    l.add(new Cidades(3539004, '3 - Sudeste', 'SP', 'Pirangi', '0055'));
    l.add(new Cidades(3150901, '3 - Sudeste', 'MG', 'Pirangucu', '0055'));
    l.add(new Cidades(3151008, '3 - Sudeste', 'MG', 'Piranguinho', '0055'));
    l.add(new Cidades(2707107, '2 - Nordeste', 'AL', 'Piranhas', '0055'));
    l.add(new Cidades(5217203, '5 - Centro-Oeste', 'GO', 'Piranhas', '0055'));
    l.add(new Cidades(2108801, '2 - Nordeste', 'MA', 'Pirapemas', '0055'));
    l.add(new Cidades(3151107, '3 - Sudeste', 'MG', 'Pirapetinga', '0055'));
    l.add(new Cidades(4314555, '4 - Sul', 'RS', 'Pirapo', '0055'));
    l.add(new Cidades(3151206, '3 - Sudeste', 'MG', 'Pirapora', '0055'));
    l.add(new Cidades(
        3539103, '3 - Sudeste', 'SP', 'Pirapora Do Bom Jesus', '0055'));
    l.add(new Cidades(3539202, '3 - Sudeste', 'SP', 'Pirapozinho', '0055'));
    l.add(new Cidades(4119509, '4 - Sul', 'PR', 'Piraquara', '0055'));
    l.add(new Cidades(1717206, '1 - Norte', 'TO', 'Piraque', '0055'));
    l.add(new Cidades(3539301, '3 - Sudeste', 'SP', 'Pirassununga', '0055'));
    l.add(new Cidades(4314605, '4 - Sul', 'RS', 'Piratini', '0055'));
    l.add(new Cidades(3539400, '3 - Sudeste', 'SP', 'Piratininga', '0055'));
    l.add(new Cidades(4213104, '4 - Sul', 'SC', 'Piratuba', '0055'));
    l.add(new Cidades(3151305, '3 - Sudeste', 'MG', 'Pirauba', '0055'));
    l.add(
        new Cidades(5217302, '5 - Centro-Oeste', 'GO', 'Pirenopolis', '0055'));
    l.add(
        new Cidades(5217401, '5 - Centro-Oeste', 'GO', 'Pires Do Rio', '0055'));
    l.add(new Cidades(2310951, '2 - Nordeste', 'CE', 'Pires Ferreira', '0055'));
    l.add(new Cidades(2924702, '2 - Nordeste', 'BA', 'Piripa', '0055'));
    l.add(new Cidades(2208403, '2 - Nordeste', 'PI', 'Piripiri', '0055'));
    l.add(new Cidades(2924801, '2 - Nordeste', 'BA', 'Piritiba', '0055'));
    l.add(new Cidades(2511806, '2 - Nordeste', 'PB', 'Pirpirituba', '0055'));
    l.add(new Cidades(4119608, '4 - Sul', 'PR', 'Pitanga', '0055'));
    l.add(new Cidades(3539509, '3 - Sudeste', 'SP', 'Pitangueiras', '0055'));
    l.add(new Cidades(4119657, '4 - Sul', 'PR', 'Pitangueiras', '0055'));
    l.add(new Cidades(3151404, '3 - Sudeste', 'MG', 'Pitangui', '0055'));
    l.add(new Cidades(2511905, '2 - Nordeste', 'PB', 'Pitimbu', '0055'));
    l.add(new Cidades(1717503, '1 - Norte', 'TO', 'Pium', '0055'));
    l.add(new Cidades(3204203, '3 - Sudeste', 'ES', 'Piuma', '0055'));
    l.add(new Cidades(3151503, '3 - Sudeste', 'MG', 'Piumhi', '0055'));
    l.add(new Cidades(1505650, '1 - Norte', 'PA', 'Placas', '0055'));
    l.add(new Cidades(1200385, '1 - Norte', 'AC', 'Placido De Castro', '0055'));
    l.add(new Cidades(5217609, '5 - Centro-Oeste', 'GO', 'Planaltina', '0055'));
    l.add(
        new Cidades(4119707, '4 - Sul', 'PR', 'Planaltina Do Parana', '0055'));
    l.add(new Cidades(2924900, '2 - Nordeste', 'BA', 'Planaltino', '0055'));
    l.add(new Cidades(2925006, '2 - Nordeste', 'BA', 'Planalto', '0055'));
    l.add(new Cidades(3539608, '3 - Sudeste', 'SP', 'Planalto', '0055'));
    l.add(new Cidades(4119806, '4 - Sul', 'PR', 'Planalto', '0055'));
    l.add(new Cidades(4314704, '4 - Sul', 'RS', 'Planalto', '0055'));
    l.add(new Cidades(4213153, '4 - Sul', 'SC', 'Planalto Alegre', '0055'));
    l.add(new Cidades(
        5106455, '5 - Centro-Oeste', 'MT', 'Planalto Da Serra', '0055'));
    l.add(new Cidades(3151602, '3 - Sudeste', 'MG', 'Planura', '0055'));
    l.add(new Cidades(3539707, '3 - Sudeste', 'SP', 'Platina', '0055'));
    l.add(new Cidades(3539806, '3 - Sudeste', 'SP', 'Poa', '0055'));
    l.add(new Cidades(2611200, '2 - Nordeste', 'PE', 'Pocao', '0055'));
    l.add(
        new Cidades(2108900, '2 - Nordeste', 'MA', 'Pocao De Pedras', '0055'));
    l.add(new Cidades(2512002, '2 - Nordeste', 'PB', 'Pocinhos', '0055'));
    l.add(new Cidades(2410108, '2 - Nordeste', 'RN', 'Poco Branco', '0055'));
    l.add(new Cidades(2512036, '2 - Nordeste', 'PB', 'Poco Dantas', '0055'));
    l.add(new Cidades(4314753, '4 - Sul', 'RS', 'Poco Das Antas', '0055'));
    l.add(new Cidades(
        2707206, '2 - Nordeste', 'AL', 'Poco Das Trincheiras', '0055'));
    l.add(new Cidades(
        2512077, '2 - Nordeste', 'PB', 'Poco De Jose De Moura', '0055'));
    l.add(new Cidades(3151701, '3 - Sudeste', 'MG', 'Poco Fundo', '0055'));
    l.add(new Cidades(2805406, '2 - Nordeste', 'SE', 'Poco Redondo', '0055'));
    l.add(new Cidades(2805505, '2 - Nordeste', 'SE', 'Poco Verde', '0055'));
    l.add(new Cidades(2925105, '2 - Nordeste', 'BA', 'Pocoes', '0055'));
    l.add(new Cidades(5106505, '5 - Centro-Oeste', 'MT', 'Pocone', '0055'));
    l.add(new Cidades(3151800, '3 - Sudeste', 'MG', 'Pocos De Caldas', '0055'));
    l.add(new Cidades(3151909, '3 - Sudeste', 'MG', 'Pocrane', '0055'));
    l.add(new Cidades(2925204, '2 - Nordeste', 'BA', 'Pojuca', '0055'));
    l.add(new Cidades(3539905, '3 - Sudeste', 'SP', 'Poloni', '0055'));
    l.add(new Cidades(2512101, '2 - Nordeste', 'PB', 'Pombal', '0055'));
    l.add(new Cidades(2611309, '2 - Nordeste', 'PE', 'Pombos', '0055'));
    l.add(new Cidades(4213203, '4 - Sul', 'SC', 'Pomerode', '0055'));
    l.add(new Cidades(3540002, '3 - Sudeste', 'SP', 'Pompeia', '0055'));
    l.add(new Cidades(3152006, '3 - Sudeste', 'MG', 'Pompeu', '0055'));
    l.add(new Cidades(3540101, '3 - Sudeste', 'SP', 'Pongai', '0055'));
    l.add(new Cidades(1505700, '1 - Norte', 'PA', 'Ponta De Pedras', '0055'));
    l.add(new Cidades(4119905, '4 - Sul', 'PR', 'Ponta Grossa', '0055'));
    l.add(new Cidades(5006606, '5 - Centro-Oeste', 'MS', 'Ponta Pora', '0055'));
    l.add(new Cidades(3540200, '3 - Sudeste', 'SP', 'Pontal', '0055'));
    l.add(new Cidades(
        5106653, '5 - Centro-Oeste', 'MT', 'Pontal Do Araguaia', '0055'));
    l.add(new Cidades(4119954, '4 - Sul', 'PR', 'Pontal Do Parana', '0055'));
    l.add(new Cidades(5217708, '5 - Centro-Oeste', 'GO', 'Pontalina', '0055'));
    l.add(new Cidades(3540259, '3 - Sudeste', 'SP', 'Pontalinda', '0055'));
    l.add(new Cidades(4314779, '4 - Sul', 'RS', 'Pontao', '0055'));
    l.add(new Cidades(4213302, '4 - Sul', 'SC', 'Ponte Alta', '0055'));
    l.add(new Cidades(
        1717800, '1 - Norte', 'TO', 'Ponte Alta Do Bom Jesus', '0055'));
    l.add(new Cidades(4213351, '4 - Sul', 'SC', 'Ponte Alta Do Norte', '0055'));
    l.add(new Cidades(1717909, '1 - Norte', 'TO', 'Ponte Alta Do To', '0055'));
    l.add(
        new Cidades(5106703, '5 - Centro-Oeste', 'MT', 'Ponte Branca', '0055'));
    l.add(new Cidades(3152105, '3 - Sudeste', 'MG', 'Ponte Nova', '0055'));
    l.add(new Cidades(4314787, '4 - Sul', 'RS', 'Ponte Preta', '0055'));
    l.add(new Cidades(4213401, '4 - Sul', 'SC', 'Ponte Serrada', '0055'));
    l.add(new Cidades(
        5106752, '5 - Centro-Oeste', 'MT', 'Pontes E Lacerda', '0055'));
    l.add(new Cidades(3540309, '3 - Sudeste', 'SP', 'Pontes Gestal', '0055'));
    l.add(new Cidades(3204252, '3 - Sudeste', 'ES', 'Ponto Belo', '0055'));
    l.add(new Cidades(3152131, '3 - Sudeste', 'MG', 'Ponto Chique', '0055'));
    l.add(new Cidades(
        3152170, '3 - Sudeste', 'MG', 'Ponto Dos Volantes', '0055'));
    l.add(new Cidades(2925253, '2 - Nordeste', 'BA', 'Ponto Novo', '0055'));
    l.add(new Cidades(3540408, '3 - Sudeste', 'SP', 'Populina', '0055'));
    l.add(new Cidades(2311009, '2 - Nordeste', 'CE', 'Poranga', '0055'));
    l.add(new Cidades(3540507, '3 - Sudeste', 'SP', 'Porangaba', '0055'));
    l.add(new Cidades(5218003, '5 - Centro-Oeste', 'GO', 'Porangatu', '0055'));
    l.add(new Cidades(3304102, '3 - Sudeste', 'RJ', 'Porciuncula', '0055'));
    l.add(new Cidades(4120002, '4 - Sul', 'PR', 'Porecatu', '0055'));
    l.add(new Cidades(2410207, '2 - Nordeste', 'RN', 'Portalegre', '0055'));
    l.add(new Cidades(4314803, '4 - Sul', 'RS', 'Portao', '0055'));
    l.add(new Cidades(5218052, '5 - Centro-Oeste', 'GO', 'Porteirao', '0055'));
    l.add(new Cidades(2311108, '2 - Nordeste', 'CE', 'Porteiras', '0055'));
    l.add(new Cidades(3152204, '3 - Sudeste', 'MG', 'Porteirinha', '0055'));
    l.add(new Cidades(1505809, '1 - Norte', 'PA', 'Portel', '0055'));
    l.add(
        new Cidades(5218102, '5 - Centro-Oeste', 'GO', 'Portelandia', '0055'));
    l.add(new Cidades(2208502, '2 - Nordeste', 'PI', 'Porto', '0055'));
    l.add(new Cidades(1200807, '1 - Norte', 'AC', 'Porto Ac', '0055'));
    l.add(new Cidades(4314902, '4 - Sul', 'RS', 'Porto Alegre', '0055'));
    l.add(new Cidades(
        5106778, '5 - Centro-Oeste', 'MT', 'Porto Alegre Do Norte', '0055'));
    l.add(new Cidades(
        2208551, '2 - Nordeste', 'PI', 'Porto Alegre Do Piaui', '0055'));
    l.add(
        new Cidades(1718006, '1 - Norte', 'TO', 'Porto Alegre Do To', '0055'));
    l.add(new Cidades(4120101, '4 - Sul', 'PR', 'Porto Am', '0055'));
    l.add(new Cidades(4120150, '4 - Sul', 'PR', 'Porto Barreiro', '0055'));
    l.add(new Cidades(4213500, '4 - Sul', 'SC', 'Porto Belo', '0055'));
    l.add(new Cidades(2707305, '2 - Nordeste', 'AL', 'Porto Calvo', '0055'));
    l.add(new Cidades(2805604, '2 - Nordeste', 'SE', 'Porto Da Folha', '0055'));
    l.add(new Cidades(1505908, '1 - Norte', 'PA', 'Porto De Moz', '0055'));
    l.add(
        new Cidades(2707404, '2 - Nordeste', 'AL', 'Porto De Pedras', '0055'));
    l.add(
        new Cidades(2410256, '2 - Nordeste', 'RN', 'Porto Do Mangue', '0055'));
    l.add(new Cidades(
        5106802, '5 - Centro-Oeste', 'MT', 'Porto Dos Gauchos', '0055'));
    l.add(new Cidades(
        5106828, '5 - Centro-Oeste', 'MT', 'Porto Esperidiao', '0055'));
    l.add(new Cidades(
        5106851, '5 - Centro-Oeste', 'MT', 'Porto Estrela', '0055'));
    l.add(new Cidades(3540606, '3 - Sudeste', 'SP', 'Porto Feliz', '0055'));
    l.add(new Cidades(3540705, '3 - Sudeste', 'SP', 'Porto Ferreira', '0055'));
    l.add(new Cidades(3152303, '3 - Sudeste', 'MG', 'Porto Firme', '0055'));
    l.add(new Cidades(2109007, '2 - Nordeste', 'MA', 'Porto Franco', '0055'));
    l.add(new Cidades(1600535, '1 - Norte', 'AP', 'Porto Grande', '0055'));
    l.add(new Cidades(4315008, '4 - Sul', 'RS', 'Porto Lucena', '0055'));
    l.add(new Cidades(4315057, '4 - Sul', 'RS', 'Porto Maua', '0055'));
    l.add(new Cidades(
        5006903, '5 - Centro-Oeste', 'MS', 'Porto Murtinho', '0055'));
    l.add(new Cidades(1718204, '1 - Norte', 'TO', 'Porto Nacional', '0055'));
    l.add(new Cidades(3304110, '3 - Sudeste', 'RJ', 'Porto Real', '0055'));
    l.add(new Cidades(
        2707503, '2 - Nordeste', 'AL', 'Porto Real Do Colegio', '0055'));
    l.add(new Cidades(4120200, '4 - Sul', 'PR', 'Porto Rico', '0055'));
    l.add(new Cidades(
        2109056, '2 - Nordeste', 'MA', 'Porto Rico Do Maranhao', '0055'));
    l.add(new Cidades(2925303, '2 - Nordeste', 'BA', 'Porto Seguro', '0055'));
    l.add(new Cidades(4213609, '4 - Sul', 'SC', 'Porto Uniao', '0055'));
    l.add(new Cidades(1100205, '1 - Norte', 'RO', 'Porto Velho', '0055'));
    l.add(new Cidades(4315073, '4 - Sul', 'RS', 'Porto Vera Cruz', '0055'));
    l.add(new Cidades(4120309, '4 - Sul', 'PR', 'Porto Vitoria', '0055'));
    l.add(new Cidades(1200393, '1 - Norte', 'AC', 'Porto Walter', '0055'));
    l.add(new Cidades(4315107, '4 - Sul', 'RS', 'Porto Xavier', '0055'));
    l.add(new Cidades(5218300, '5 - Centro-Oeste', 'GO', 'Posse', '0055'));
    l.add(new Cidades(3152402, '3 - Sudeste', 'MG', 'Pote', '0055'));
    l.add(new Cidades(2311207, '2 - Nordeste', 'CE', 'Potengi', '0055'));
    l.add(new Cidades(3540754, '3 - Sudeste', 'SP', 'Potim', '0055'));
    l.add(new Cidades(2925402, '2 - Nordeste', 'BA', 'Potiragua', '0055'));
    l.add(new Cidades(3540804, '3 - Sudeste', 'SP', 'Potirendaba', '0055'));
    l.add(new Cidades(2311231, '2 - Nordeste', 'CE', 'Potiretama', '0055'));
    l.add(new Cidades(3152501, '3 - Sudeste', 'MG', 'Pouso Alegre', '0055'));
    l.add(new Cidades(3152600, '3 - Sudeste', 'MG', 'Pouso Alto', '0055'));
    l.add(new Cidades(4315131, '4 - Sul', 'RS', 'Pouso Novo', '0055'));
    l.add(new Cidades(4213708, '4 - Sul', 'SC', 'Pouso Redondo', '0055'));
    l.add(new Cidades(5107008, '5 - Centro-Oeste', 'MT', 'Poxoreo', '0055'));
    l.add(new Cidades(3540853, '3 - Sudeste', 'SP', 'Pracinha', '0055'));
    l.add(new Cidades(1600550, '1 - Norte', 'AP', 'Pracuuba', '0055'));
    l.add(new Cidades(2925501, '2 - Nordeste', 'BA', 'Prado', '0055'));
    l.add(new Cidades(4120333, '4 - Sul', 'PR', 'Prado Ferreira', '0055'));
    l.add(new Cidades(3540903, '3 - Sudeste', 'SP', 'Pradopolis', '0055'));
    l.add(new Cidades(3152709, '3 - Sudeste', 'MG', 'Prados', '0055'));
    l.add(new Cidades(3541000, '3 - Sudeste', 'SP', 'Praia Grande', '0055'));
    l.add(new Cidades(4213807, '4 - Sul', 'SC', 'Praia Grande', '0055'));
    l.add(new Cidades(1718303, '1 - Norte', 'TO', 'Praia Norte', '0055'));
    l.add(new Cidades(1506005, '1 - Norte', 'PA', 'Prainha', '0055'));
    l.add(new Cidades(4120358, '4 - Sul', 'PR', 'Pranchita', '0055'));
    l.add(new Cidades(2512200, '2 - Nordeste', 'PB', 'Prata', '0055'));
    l.add(new Cidades(3152808, '3 - Sudeste', 'MG', 'Prata', '0055'));
    l.add(new Cidades(2208601, '2 - Nordeste', 'PI', 'Prata Do Piaui', '0055'));
    l.add(new Cidades(3541059, '3 - Sudeste', 'SP', 'Pratania', '0055'));
    l.add(new Cidades(3152907, '3 - Sudeste', 'MG', 'Pratapolis', '0055'));
    l.add(new Cidades(3153004, '3 - Sudeste', 'MG', 'Pratinha', '0055'));
    l.add(
        new Cidades(3541109, '3 - Sudeste', 'SP', 'Presidente Alves', '0055'));
    l.add(new Cidades(
        3153103, '3 - Sudeste', 'MG', 'Presidente Bernardes', '0055'));
    l.add(new Cidades(
        3541208, '3 - Sudeste', 'SP', 'Presidente Bernardes', '0055'));
    l.add(new Cidades(
        4213906, '4 - Sul', 'SC', 'Presidente Castello Branco', '0055'));
    l.add(new Cidades(
        4120408, '4 - Sul', 'PR', 'Presidente Castelo Branco', '0055'));
    l.add(
        new Cidades(2109106, '2 - Nordeste', 'MA', 'Presidente Dutra', '0055'));
    l.add(
        new Cidades(2925600, '2 - Nordeste', 'BA', 'Presidente Dutra', '0055'));
    l.add(new Cidades(
        3541307, '3 - Sudeste', 'SP', 'Presidente Epitacio', '0055'));
    l.add(new Cidades(
        1303536, '1 - Norte', 'AM', 'Presidente Figueiredo', '0055'));
    l.add(new Cidades(4214003, '4 - Sul', 'SC', 'Presidente Getulio', '0055'));
    l.add(new Cidades(
        2925709, '2 - Nordeste', 'BA', 'Presidente Janio Quadros', '0055'));
    l.add(new Cidades(
        2109205, '2 - Nordeste', 'MA', 'Presidente Juscelino', '0055'));
    l.add(new Cidades(
        2410306, '2 - Nordeste', 'RN', 'Presidente Juscelino', '0055'));
    l.add(new Cidades(
        3153202, '3 - Sudeste', 'MG', 'Presidente Juscelino', '0055'));
    l.add(
        new Cidades(1718402, '1 - Norte', 'TO', 'Presidente Kennedy', '0055'));
    l.add(new Cidades(
        3204302, '3 - Sudeste', 'ES', 'Presidente Kennedy', '0055'));
    l.add(new Cidades(
        3153301, '3 - Sudeste', 'MG', 'Presidente Kubitschek', '0055'));
    l.add(new Cidades(4315149, '4 - Sul', 'RS', 'Presidente Lucena', '0055'));
    l.add(new Cidades(1100254, '1 - Norte', 'RO', 'Presidente Medici', '0055'));
    l.add(new Cidades(
        2109239, '2 - Nordeste', 'MA', 'Presidente Medici', '0055'));
    l.add(new Cidades(4214102, '4 - Sul', 'SC', 'Presidente Nereu', '0055'));
    l.add(new Cidades(
        3153400, '3 - Sudeste', 'MG', 'Presidente Olegario', '0055'));
    l.add(new Cidades(
        3541406, '3 - Sudeste', 'SP', 'Presidente Prudente', '0055'));
    l.add(new Cidades(
        2109270, '2 - Nordeste', 'MA', 'Presidente Sarney', '0055'));
    l.add(new Cidades(
        2925758, '2 - Nordeste', 'BA', 'Presidente Tancredo Neves', '0055'));
    l.add(new Cidades(
        2109304, '2 - Nordeste', 'MA', 'Presidente Vargas', '0055'));
    l.add(new Cidades(
        3541505, '3 - Sudeste', 'SP', 'Presidente Venceslau', '0055'));
    l.add(new Cidades(1506104, '1 - Norte', 'PA', 'Primavera', '0055'));
    l.add(new Cidades(2611408, '2 - Nordeste', 'PE', 'Primavera', '0055'));
    l.add(new Cidades(
        1101476, '1 - Norte', 'RO', 'Primavera De Rondonia', '0055'));
    l.add(new Cidades(
        5107040, '5 - Centro-Oeste', 'MT', 'Primavera Do Leste', '0055'));
    l.add(new Cidades(2109403, '2 - Nordeste', 'MA', 'Primeira Cruz', '0055'));
    l.add(new Cidades(4120507, '4 - Sul', 'PR', 'Primeiro De Maio', '0055'));
    l.add(new Cidades(4214151, '4 - Sul', 'SC', 'Princesa', '0055'));
    l.add(
        new Cidades(2512309, '2 - Nordeste', 'PB', 'Princesa Isabel', '0055'));
    l.add(new Cidades(
        5218391, '5 - Centro-Oeste', 'GO', 'Professor Jamil', '0055'));
    l.add(new Cidades(4315156, '4 - Sul', 'RS', 'Progresso', '0055'));
    l.add(new Cidades(3541604, '3 - Sudeste', 'SP', 'Promissao', '0055'));
    l.add(new Cidades(2805703, '2 - Nordeste', 'SE', 'Propria', '0055'));
    l.add(new Cidades(4315172, '4 - Sul', 'RS', 'Protasio Alves', '0055'));
    l.add(new Cidades(
        3153608, '3 - Sudeste', 'MG', 'Prudente De Morais', '0055'));
    l.add(new Cidades(4120606, '4 - Sul', 'PR', 'Prudentopolis', '0055'));
    l.add(new Cidades(1718451, '1 - Norte', 'TO', 'Pugmil', '0055'));
    l.add(new Cidades(2410405, '2 - Nordeste', 'RN', 'Pureza', '0055'));
    l.add(new Cidades(4315206, '4 - Sul', 'RS', 'Putinga', '0055'));
    l.add(new Cidades(2512408, '2 - Nordeste', 'PB', 'Puxinana', '0055'));
    l.add(new Cidades(3541653, '3 - Sudeste', 'SP', 'Quadra', '0055'));
    l.add(new Cidades(4315305, '4 - Sul', 'RS', 'Quarai', '0055'));
    l.add(new Cidades(3153707, '3 - Sudeste', 'MG', 'Quartel Geral', '0055'));
    l.add(new Cidades(4120655, '4 - Sul', 'PR', 'Quarto Centenario', '0055'));
    l.add(new Cidades(3541703, '3 - Sudeste', 'SP', 'Quata', '0055'));
    l.add(new Cidades(4120705, '4 - Sul', 'PR', 'Quatigua', '0055'));
    l.add(new Cidades(1506112, '1 - Norte', 'PA', 'Quatipuru', '0055'));
    l.add(new Cidades(3304128, '3 - Sudeste', 'RJ', 'Quatis', '0055'));
    l.add(new Cidades(4120804, '4 - Sul', 'PR', 'Quatro Barras', '0055'));
    l.add(new Cidades(4315313, '4 - Sul', 'RS', 'Quatro Irmaos', '0055'));
    l.add(new Cidades(4120853, '4 - Sul', 'PR', 'Quatro Pontes', '0055'));
    l.add(new Cidades(2707602, '2 - Nordeste', 'AL', 'Quebrangulo', '0055'));
    l.add(new Cidades(4120903, '4 - Sul', 'PR', 'Quedas Do Iguacu', '0055'));
    l.add(new Cidades(2208650, '2 - Nordeste', 'PI', 'Queimada Nova', '0055'));
    l.add(new Cidades(2512507, '2 - Nordeste', 'PB', 'Queimadas', '0055'));
    l.add(new Cidades(2925808, '2 - Nordeste', 'BA', 'Queimadas', '0055'));
    l.add(new Cidades(3304144, '3 - Sudeste', 'RJ', 'Queimados', '0055'));
    l.add(new Cidades(3541802, '3 - Sudeste', 'SP', 'Queiroz', '0055'));
    l.add(new Cidades(3541901, '3 - Sudeste', 'SP', 'Queluz', '0055'));
    l.add(new Cidades(3153806, '3 - Sudeste', 'MG', 'Queluzito', '0055'));
    l.add(new Cidades(5107065, '5 - Centro-Oeste', 'MT', 'Querencia', '0055'));
    l.add(new Cidades(4121000, '4 - Sul', 'PR', 'Querencia Do Norte', '0055'));
    l.add(new Cidades(4315321, '4 - Sul', 'RS', 'Quevedos', '0055'));
    l.add(new Cidades(2925907, '2 - Nordeste', 'BA', 'Quijingue', '0055'));
    l.add(new Cidades(4214201, '4 - Sul', 'SC', 'Quilombo', '0055'));
    l.add(new Cidades(4121109, '4 - Sul', 'PR', 'Quinta Do Sol', '0055'));
    l.add(new Cidades(3542008, '3 - Sudeste', 'SP', 'Quintana', '0055'));
    l.add(new Cidades(4315354, '4 - Sul', 'RS', 'Quinze De Novembro', '0055'));
    l.add(new Cidades(2611507, '2 - Nordeste', 'PE', 'Quipapa', '0055'));
    l.add(
        new Cidades(5218508, '5 - Centro-Oeste', 'GO', 'Quirinopolis', '0055'));
    l.add(new Cidades(3304151, '3 - Sudeste', 'RJ', 'Quissama', '0055'));
    l.add(new Cidades(4121208, '4 - Sul', 'PR', 'Quitandinha', '0055'));
    l.add(
        new Cidades(2311264, '2 - Nordeste', 'CE', 'Quiterianopolis', '0055'));
    l.add(new Cidades(2512606, '2 - Nordeste', 'PB', 'Quixaba', '0055'));
    l.add(new Cidades(2611533, '2 - Nordeste', 'PE', 'Quixaba', '0055'));
    l.add(new Cidades(2925931, '2 - Nordeste', 'BA', 'Quixabeira', '0055'));
    l.add(new Cidades(2311306, '2 - Nordeste', 'CE', 'Quixada', '0055'));
    l.add(new Cidades(2311355, '2 - Nordeste', 'CE', 'Quixelo', '0055'));
    l.add(new Cidades(2311405, '2 - Nordeste', 'CE', 'Quixeramobim', '0055'));
    l.add(new Cidades(2311504, '2 - Nordeste', 'CE', 'Quixere', '0055'));
    l.add(
        new Cidades(2410504, '2 - Nordeste', 'RN', 'Rafael Fernandes', '0055'));
    l.add(new Cidades(2410603, '2 - Nordeste', 'RN', 'Rafael Godeiro', '0055'));
    l.add(
        new Cidades(2925956, '2 - Nordeste', 'BA', 'Rafael Jambeiro', '0055'));
    l.add(new Cidades(3542107, '3 - Sudeste', 'SP', 'Rafard', '0055'));
    l.add(new Cidades(4121257, '4 - Sul', 'PR', 'Ramilandia', '0055'));
    l.add(new Cidades(3542206, '3 - Sudeste', 'SP', 'Rancharia', '0055'));
    l.add(new Cidades(4121307, '4 - Sul', 'PR', 'Rancho Alegre', '0055'));
    l.add(
        new Cidades(4121356, '4 - Sul', 'PR', 'Rancho Alegre D`Oeste', '0055'));
    l.add(new Cidades(4214300, '4 - Sul', 'SC', 'Rancho Queimado', '0055'));
    l.add(new Cidades(2109452, '2 - Nordeste', 'MA', 'Raposa', '0055'));
    l.add(new Cidades(3153905, '3 - Sudeste', 'MG', 'Raposos', '0055'));
    l.add(new Cidades(3154002, '3 - Sudeste', 'MG', 'Raul Soares', '0055'));
    l.add(new Cidades(4121406, '4 - Sul', 'PR', 'Realeza', '0055'));
    l.add(new Cidades(4121505, '4 - Sul', 'PR', 'Reboucas', '0055'));
    l.add(new Cidades(2611606, '2 - Nordeste', 'PE', 'Recife', '0055'));
    l.add(new Cidades(3154101, '3 - Sudeste', 'MG', 'Recreio', '0055'));
    l.add(new Cidades(1718501, '1 - Norte', 'TO', 'Recursolandia', '0055'));
    l.add(new Cidades(1506138, '1 - Norte', 'PA', 'Redencao', '0055'));
    l.add(new Cidades(2311603, '2 - Nordeste', 'CE', 'Redencao', '0055'));
    l.add(
        new Cidades(3542305, '3 - Sudeste', 'SP', 'Redencao Da Serra', '0055'));
    l.add(new Cidades(
        2208700, '2 - Nordeste', 'PI', 'Redencao Do Gurgueia', '0055'));
    l.add(new Cidades(4315404, '4 - Sul', 'RS', 'Redentora', '0055'));
    l.add(new Cidades(3154150, '3 - Sudeste', 'MG', 'Reduto', '0055'));
    l.add(new Cidades(2208809, '2 - Nordeste', 'PI', 'Regeneracao', '0055'));
    l.add(new Cidades(3542404, '3 - Sudeste', 'SP', 'Regente Feijo', '0055'));
    l.add(new Cidades(3542503, '3 - Sudeste', 'SP', 'Reginopolis', '0055'));
    l.add(new Cidades(3542602, '3 - Sudeste', 'SP', 'Registro', '0055'));
    l.add(new Cidades(4315453, '4 - Sul', 'RS', 'Relvado', '0055'));
    l.add(new Cidades(2926004, '2 - Nordeste', 'BA', 'Remanso', '0055'));
    l.add(new Cidades(2512705, '2 - Nordeste', 'PB', 'Remigio', '0055'));
    l.add(new Cidades(4121604, '4 - Sul', 'PR', 'Renascenca', '0055'));
    l.add(new Cidades(2311702, '2 - Nordeste', 'CE', 'Reriutaba', '0055'));
    l.add(new Cidades(3304201, '3 - Sudeste', 'RJ', 'Resende', '0055'));
    l.add(new Cidades(3154200, '3 - Sudeste', 'MG', 'Resende Costa', '0055'));
    l.add(new Cidades(4121703, '4 - Sul', 'PR', 'Reserva', '0055'));
    l.add(new Cidades(
        5107156, '5 - Centro-Oeste', 'MT', 'Reserva Do Cabacal', '0055'));
    l.add(new Cidades(4121752, '4 - Sul', 'PR', 'Reserva Do Iguacu', '0055'));
    l.add(new Cidades(3154309, '3 - Sudeste', 'MG', 'Resplendor', '0055'));
    l.add(new Cidades(3154408, '3 - Sudeste', 'MG', 'Ressaquinha', '0055'));
    l.add(new Cidades(3542701, '3 - Sudeste', 'SP', 'Restinga', '0055'));
    l.add(new Cidades(4315503, '4 - Sul', 'RS', 'Restinga Seca', '0055'));
    l.add(new Cidades(2926103, '2 - Nordeste', 'BA', 'Retirolandia', '0055'));
    l.add(new Cidades(2109502, '2 - Nordeste', 'MA', 'Riachao', '0055'));
    l.add(new Cidades(2512747, '2 - Nordeste', 'PB', 'Riachao', '0055'));
    l.add(new Cidades(
        2926202, '2 - Nordeste', 'BA', 'Riachao Das Neves', '0055'));
    l.add(new Cidades(
        2512754, '2 - Nordeste', 'PB', 'Riachao Do Bacamarte', '0055'));
    l.add(new Cidades(
        2805802, '2 - Nordeste', 'SE', 'Riachao Do Dantas', '0055'));
    l.add(new Cidades(
        2926301, '2 - Nordeste', 'BA', 'Riachao Do Jacuipe', '0055'));
    l.add(
        new Cidades(2512762, '2 - Nordeste', 'PB', 'Riachao Do Poco', '0055'));
    l.add(new Cidades(1718550, '1 - Norte', 'TO', 'Riachinho', '0055'));
    l.add(new Cidades(3154457, '3 - Sudeste', 'MG', 'Riachinho', '0055'));
    l.add(new Cidades(2410702, '2 - Nordeste', 'RN', 'Riacho Da Cruz', '0055'));
    l.add(
        new Cidades(2611705, '2 - Nordeste', 'PE', 'Riacho Das Almas', '0055'));
    l.add(new Cidades(
        2410801, '2 - Nordeste', 'RN', 'Riacho De Santana', '0055'));
    l.add(new Cidades(
        2926400, '2 - Nordeste', 'BA', 'Riacho De Santana', '0055'));
    l.add(new Cidades(
        2512788, '2 - Nordeste', 'PB', 'Riacho De Santo Antonio', '0055'));
    l.add(new Cidades(
        2512804, '2 - Nordeste', 'PB', 'Riacho Dos Cavalos', '0055'));
    l.add(new Cidades(
        3154507, '3 - Sudeste', 'MG', 'Riacho Dos Machados', '0055'));
    l.add(new Cidades(2208858, '2 - Nordeste', 'PI', 'Riacho Frio', '0055'));
    l.add(new Cidades(2410900, '2 - Nordeste', 'RN', 'Riachuelo', '0055'));
    l.add(new Cidades(2805901, '2 - Nordeste', 'SE', 'Riachuelo', '0055'));
    l.add(new Cidades(5218607, '5 - Centro-Oeste', 'GO', 'Rialma', '0055'));
    l.add(new Cidades(5218706, '5 - Centro-Oeste', 'GO', 'Rianapolis', '0055'));
    l.add(
        new Cidades(2109551, '2 - Nordeste', 'MA', 'Ribamar Fiquene', '0055'));
    l.add(new Cidades(
        5007109, '5 - Centro-Oeste', 'MS', 'Ribas Do Rio Pardo', '0055'));
    l.add(new Cidades(3542800, '3 - Sudeste', 'SP', 'Ribeira', '0055'));
    l.add(new Cidades(
        2926509, '2 - Nordeste', 'BA', 'Ribeira Do Amparo', '0055'));
    l.add(
        new Cidades(2208874, '2 - Nordeste', 'PI', 'Ribeira Do Piaui', '0055'));
    l.add(new Cidades(
        2926608, '2 - Nordeste', 'BA', 'Ribeira Do Pombal', '0055'));
    l.add(new Cidades(2611804, '2 - Nordeste', 'PE', 'Ribeirao', '0055'));
    l.add(new Cidades(3542909, '3 - Sudeste', 'SP', 'Ribeirao Bonito', '0055'));
    l.add(new Cidades(3543006, '3 - Sudeste', 'SP', 'Ribeirao Branco', '0055'));
    l.add(new Cidades(
        5107180, '5 - Centro-Oeste', 'MT', 'Ribeirao Cascalheira', '0055'));
    l.add(new Cidades(4121802, '4 - Sul', 'PR', 'Ribeirao Claro', '0055'));
    l.add(
        new Cidades(3543105, '3 - Sudeste', 'SP', 'Ribeirao Corrente', '0055'));
    l.add(new Cidades(
        3154606, '3 - Sudeste', 'MG', 'Ribeirao Das Neves', '0055'));
    l.add(new Cidades(
        2926657, '2 - Nordeste', 'BA', 'Ribeirao Do Largo', '0055'));
    l.add(new Cidades(4121901, '4 - Sul', 'PR', 'Ribeirao Do Pinhal', '0055'));
    l.add(new Cidades(3543204, '3 - Sudeste', 'SP', 'Ribeirao Do Sul', '0055'));
    l.add(new Cidades(
        3543238, '3 - Sudeste', 'SP', 'Ribeirao Dos Indios', '0055'));
    l.add(new Cidades(3543253, '3 - Sudeste', 'SP', 'Ribeirao Grande', '0055'));
    l.add(new Cidades(3543303, '3 - Sudeste', 'SP', 'Ribeirao Pires', '0055'));
    l.add(new Cidades(3543402, '3 - Sudeste', 'SP', 'Ribeirao Preto', '0055'));
    l.add(
        new Cidades(3154705, '3 - Sudeste', 'MG', 'Ribeirao Vermelho', '0055'));
    l.add(new Cidades(
        5107198, '5 - Centro-Oeste', 'MT', 'Ribeiraozinho', '0055'));
    l.add(new Cidades(
        2208908, '2 - Nordeste', 'PI', 'Ribeiro Goncalves', '0055'));
    l.add(new Cidades(2806008, '2 - Nordeste', 'SE', 'Ribeiropolis', '0055'));
    l.add(new Cidades(3543600, '3 - Sudeste', 'SP', 'Rifaina', '0055'));
    l.add(new Cidades(3543709, '3 - Sudeste', 'SP', 'Rincao', '0055'));
    l.add(new Cidades(3543808, '3 - Sudeste', 'SP', 'Rinopolis', '0055'));
    l.add(new Cidades(3154804, '3 - Sudeste', 'MG', 'Rio Acima', '0055'));
    l.add(new Cidades(4122008, '4 - Sul', 'PR', 'Rio Azul', '0055'));
    l.add(new Cidades(3204351, '3 - Sudeste', 'ES', 'Rio Bananal', '0055'));
    l.add(new Cidades(4122107, '4 - Sul', 'PR', 'Rio Bom', '0055'));
    l.add(new Cidades(3304300, '3 - Sudeste', 'RJ', 'Rio Bonito', '0055'));
    l.add(
        new Cidades(4122156, '4 - Sul', 'PR', 'Rio Bonito Do Iguacu', '0055'));
    l.add(new Cidades(1200401, '1 - Norte', 'AC', 'Rio Branco', '0055'));
    l.add(new Cidades(5107206, '5 - Centro-Oeste', 'MT', 'Rio Branco', '0055'));
    l.add(new Cidades(4122172, '4 - Sul', 'PR', 'Rio Branco Do Ivai', '0055'));
    l.add(new Cidades(4122206, '4 - Sul', 'PR', 'Rio Branco Do Sul', '0055'));
    l.add(new Cidades(
        5007208, '5 - Centro-Oeste', 'MS', 'Rio Brilhante', '0055'));
    l.add(new Cidades(3154903, '3 - Sudeste', 'MG', 'Rio Casca', '0055'));
    l.add(new Cidades(3304409, '3 - Sudeste', 'RJ', 'Rio Claro', '0055'));
    l.add(new Cidades(3543907, '3 - Sudeste', 'SP', 'Rio Claro', '0055'));
    l.add(new Cidades(1100262, '1 - Norte', 'RO', 'Rio Crespo', '0055'));
    l.add(new Cidades(1718659, '1 - Norte', 'TO', 'Rio Da Conceicao', '0055'));
    l.add(new Cidades(4214409, '4 - Sul', 'SC', 'Rio Das Antas', '0055'));
    l.add(new Cidades(3304508, '3 - Sudeste', 'RJ', 'Rio Das Flores', '0055'));
    l.add(new Cidades(3304524, '3 - Sudeste', 'RJ', 'Rio Das Ostras', '0055'));
    l.add(new Cidades(3544004, '3 - Sudeste', 'SP', 'Rio Das Pedras', '0055'));
    l.add(new Cidades(2926707, '2 - Nordeste', 'BA', 'Rio De Contas', '0055'));
    l.add(new Cidades(3304557, '3 - Sudeste', 'RJ', 'Rj', '0055'));
    l.add(new Cidades(2926806, '2 - Nordeste', 'BA', 'Rio Do Antonio', '0055'));
    l.add(new Cidades(4214508, '4 - Sul', 'SC', 'Rio Do Campo', '0055'));
    l.add(new Cidades(2408953, '2 - Nordeste', 'RN', 'Rio Do Fogo', '0055'));
    l.add(new Cidades(4214607, '4 - Sul', 'SC', 'Rio Do Oeste', '0055'));
    l.add(new Cidades(2926905, '2 - Nordeste', 'BA', 'Rio Do Pires', '0055'));
    l.add(new Cidades(3155108, '3 - Sudeste', 'MG', 'Rio Do Prado', '0055'));
    l.add(new Cidades(4214805, '4 - Sul', 'SC', 'Rio Do Sul', '0055'));
    l.add(new Cidades(3155009, '3 - Sudeste', 'MG', 'Rio Doce', '0055'));
    l.add(new Cidades(1718709, '1 - Norte', 'TO', 'Rio Dos Bois', '0055'));
    l.add(new Cidades(4214706, '4 - Sul', 'SC', 'Rio Dos Cedros', '0055'));
    l.add(new Cidades(4315552, '4 - Sul', 'RS', 'Rio Dos Indios', '0055'));
    l.add(new Cidades(3155207, '3 - Sudeste', 'MG', 'Rio Espera', '0055'));
    l.add(new Cidades(2611903, '2 - Nordeste', 'PE', 'Rio Formoso', '0055'));
    l.add(new Cidades(4214904, '4 - Sul', 'SC', 'Rio Fortuna', '0055'));
    l.add(new Cidades(4315602, '4 - Sul', 'RS', 'Rio Grande', '0055'));
    l.add(new Cidades(
        3544103, '3 - Sudeste', 'SP', 'Rio Grande Da Serra', '0055'));
    l.add(new Cidades(
        2209005, '2 - Nordeste', 'PI', 'Rio Grande Do Piaui', '0055'));
    l.add(new Cidades(2707701, '2 - Nordeste', 'AL', 'Rio Largo', '0055'));
    l.add(new Cidades(3155306, '3 - Sudeste', 'MG', 'Rio Manso', '0055'));
    l.add(new Cidades(1506161, '1 - Norte', 'PA', 'Rio Maria', '0055'));
    l.add(new Cidades(4215000, '4 - Sul', 'SC', 'Rio Negrinho', '0055'));
    l.add(new Cidades(4122305, '4 - Sul', 'PR', 'Rio Negro', '0055'));
    l.add(new Cidades(5007307, '5 - Centro-Oeste', 'MS', 'Rio Negro', '0055'));
    l.add(new Cidades(3155405, '3 - Sudeste', 'MG', 'Rio Novo', '0055'));
    l.add(new Cidades(3204401, '3 - Sudeste', 'ES', 'Rio Novo Do Sul', '0055'));
    l.add(new Cidades(3155504, '3 - Sudeste', 'MG', 'Rio Paranaiba', '0055'));
    l.add(new Cidades(4315701, '4 - Sul', 'RS', 'Rio Pardo', '0055'));
    l.add(new Cidades(
        3155603, '3 - Sudeste', 'MG', 'Rio Pardo De Minas', '0055'));
    l.add(new Cidades(3155702, '3 - Sudeste', 'MG', 'Rio Piracicaba', '0055'));
    l.add(new Cidades(3155801, '3 - Sudeste', 'MG', 'Rio Pomba', '0055'));
    l.add(new Cidades(3155900, '3 - Sudeste', 'MG', 'Rio Preto', '0055'));
    l.add(new Cidades(1303569, '1 - Norte', 'AM', 'Rio Preto Da Eva', '0055'));
    l.add(new Cidades(5218789, '5 - Centro-Oeste', 'GO', 'Rio Quente', '0055'));
    l.add(new Cidades(2927002, '2 - Nordeste', 'BA', 'Rio Real', '0055'));
    l.add(new Cidades(4215059, '4 - Sul', 'SC', 'Rio Rufino', '0055'));
    l.add(new Cidades(1718758, '1 - Norte', 'TO', 'Rio Sono', '0055'));
    l.add(new Cidades(2512903, '2 - Nordeste', 'PB', 'Rio Tinto', '0055'));
    l.add(new Cidades(5218805, '5 - Centro-Oeste', 'GO', 'Rio Verde', '0055'));
    l.add(new Cidades(
        5007406, '5 - Centro-Oeste', 'MS', 'Rio Verde De Mt', '0055'));
    l.add(new Cidades(3156007, '3 - Sudeste', 'MG', 'Rio Vermelho', '0055'));
    l.add(new Cidades(3544202, '3 - Sudeste', 'SP', 'Riolandia', '0055'));
    l.add(new Cidades(4315750, '4 - Sul', 'RS', 'Riozinho', '0055'));
    l.add(new Cidades(4215075, '4 - Sul', 'SC', 'Riqueza', '0055'));
    l.add(new Cidades(3156106, '3 - Sudeste', 'MG', 'Ritapolis', '0055'));
    l.add(new Cidades(3543501, '3 - Sudeste', 'SP', 'Riversul', '0055'));
    l.add(new Cidades(4315800, '4 - Sul', 'RS', 'Roca Sales', '0055'));
    l.add(new Cidades(5007505, '5 - Centro-Oeste', 'MS', 'Rochedo', '0055'));
    l.add(
        new Cidades(3156205, '3 - Sudeste', 'MG', 'Rochedo De Minas', '0055'));
    l.add(new Cidades(4215109, '4 - Sul', 'SC', 'Rodeio', '0055'));
    l.add(new Cidades(4315909, '4 - Sul', 'RS', 'Rodeio Bonito', '0055'));
    l.add(new Cidades(3156304, '3 - Sudeste', 'MG', 'Rodeiro', '0055'));
    l.add(new Cidades(2927101, '2 - Nordeste', 'BA', 'Rodelas', '0055'));
    l.add(new Cidades(
        2411007, '2 - Nordeste', 'RN', 'Rodolfo Fernandes', '0055'));
    l.add(new Cidades(1200427, '1 - Norte', 'AC', 'Rodrigues Alves', '0055'));
    l.add(new Cidades(4315958, '4 - Sul', 'RS', 'Rolador', '0055'));
    l.add(new Cidades(4122404, '4 - Sul', 'PR', 'Rolandia', '0055'));
    l.add(new Cidades(4316006, '4 - Sul', 'RS', 'Rolante', '0055'));
    l.add(new Cidades(1100288, '1 - Norte', 'RO', 'Rolim De Moura', '0055'));
    l.add(new Cidades(3156403, '3 - Sudeste', 'MG', 'Romaria', '0055'));
    l.add(new Cidades(4215208, '4 - Sul', 'SC', 'Romelandia', '0055'));
    l.add(new Cidades(4122503, '4 - Sul', 'PR', 'Roncador', '0055'));
    l.add(new Cidades(4316105, '4 - Sul', 'RS', 'Ronda Alta', '0055'));
    l.add(new Cidades(4316204, '4 - Sul', 'RS', 'Rondinha', '0055'));
    l.add(
        new Cidades(5107578, '5 - Centro-Oeste', 'MT', 'Rondolandia', '0055'));
    l.add(new Cidades(4122602, '4 - Sul', 'PR', 'Rondon', '0055'));
    l.add(new Cidades(1506187, '1 - Norte', 'PA', 'Rondon Do Para', '0055'));
    l.add(
        new Cidades(5107602, '5 - Centro-Oeste', 'MT', 'Rondonopolis', '0055'));
    l.add(new Cidades(4316303, '4 - Sul', 'RS', 'Roque Gonzales', '0055'));
    l.add(new Cidades(1400472, '1 - Norte', 'RR', 'Rorainopolis', '0055'));
    l.add(new Cidades(3544251, '3 - Sudeste', 'SP', 'Rosana', '0055'));
    l.add(new Cidades(2109601, '2 - Nordeste', 'MA', 'Rosario', '0055'));
    l.add(new Cidades(
        3156452, '3 - Sudeste', 'MG', 'Rosario Da Limeira', '0055'));
    l.add(new Cidades(
        2806107, '2 - Nordeste', 'SE', 'Rosario Do Catete', '0055'));
    l.add(new Cidades(4122651, '4 - Sul', 'PR', 'Rosario Do Ivai', '0055'));
    l.add(new Cidades(4316402, '4 - Sul', 'RS', 'Rosario Do Sul', '0055'));
    l.add(new Cidades(
        5107701, '5 - Centro-Oeste', 'MT', 'Rosario Oeste', '0055'));
    l.add(new Cidades(3544301, '3 - Sudeste', 'SP', 'Roseira', '0055'));
    l.add(new Cidades(2707800, '2 - Nordeste', 'AL', 'Roteiro', '0055'));
    l.add(new Cidades(3156502, '3 - Sudeste', 'MG', 'Rubelita', '0055'));
    l.add(new Cidades(3544400, '3 - Sudeste', 'SP', 'Rubiacea', '0055'));
    l.add(new Cidades(5218904, '5 - Centro-Oeste', 'GO', 'Rubiataba', '0055'));
    l.add(new Cidades(3156601, '3 - Sudeste', 'MG', 'Rubim', '0055'));
    l.add(new Cidades(3544509, '3 - Sudeste', 'SP', 'Rubineia', '0055'));
    l.add(new Cidades(1506195, '1 - Norte', 'PA', 'Ruropolis', '0055'));
    l.add(new Cidades(2311801, '2 - Nordeste', 'CE', 'Russas', '0055'));
    l.add(new Cidades(2411106, '2 - Nordeste', 'RN', 'Ruy Barbosa', '0055'));
    l.add(new Cidades(2927200, '2 - Nordeste', 'BA', 'Ruy Barbosa', '0055'));
    l.add(new Cidades(3156700, '3 - Sudeste', 'MG', 'Sabara', '0055'));
    l.add(new Cidades(4122701, '4 - Sul', 'PR', 'Sabaudia', '0055'));
    l.add(new Cidades(3544608, '3 - Sudeste', 'SP', 'Sabino', '0055'));
    l.add(new Cidades(3156809, '3 - Sudeste', 'MG', 'Sabinopolis', '0055'));
    l.add(new Cidades(2311900, '2 - Nordeste', 'CE', 'Saboeiro', '0055'));
    l.add(new Cidades(3156908, '3 - Sudeste', 'MG', 'Sacramento', '0055'));
    l.add(new Cidades(4316428, '4 - Sul', 'RS', 'Sagrada Familia', '0055'));
    l.add(new Cidades(3544707, '3 - Sudeste', 'SP', 'Sagres', '0055'));
    l.add(new Cidades(2612000, '2 - Nordeste', 'PE', 'Saire', '0055'));
    l.add(new Cidades(4316436, '4 - Sul', 'RS', 'Saldanha Marinho', '0055'));
    l.add(new Cidades(3544806, '3 - Sudeste', 'SP', 'Sales', '0055'));
    l.add(new Cidades(3544905, '3 - Sudeste', 'SP', 'Sales Oliveira', '0055'));
    l.add(new Cidades(3545001, '3 - Sudeste', 'SP', 'Salesopolis', '0055'));
    l.add(new Cidades(4215307, '4 - Sul', 'SC', 'Salete', '0055'));
    l.add(new Cidades(2513000, '2 - Nordeste', 'PB', 'Salgadinho', '0055'));
    l.add(new Cidades(2612109, '2 - Nordeste', 'PE', 'Salgadinho', '0055'));
    l.add(new Cidades(2806206, '2 - Nordeste', 'SE', 'Salgado', '0055'));
    l.add(new Cidades(
        2513109, '2 - Nordeste', 'PB', 'Salgado De Sao Felix', '0055'));
    l.add(new Cidades(4122800, '4 - Sul', 'PR', 'Salgado Filho', '0055'));
    l.add(new Cidades(2612208, '2 - Nordeste', 'PE', 'Salgueiro', '0055'));
    l.add(new Cidades(3157005, '3 - Sudeste', 'MG', 'Salinas', '0055'));
    l.add(new Cidades(
        2927309, '2 - Nordeste', 'BA', 'Salinas Da Margarida', '0055'));
    l.add(new Cidades(1506203, '1 - Norte', 'PA', 'Salinopolis', '0055'));
    l.add(new Cidades(2311959, '2 - Nordeste', 'CE', 'Salitre', '0055'));
    l.add(new Cidades(3545100, '3 - Sudeste', 'SP', 'Salmourao', '0055'));
    l.add(new Cidades(2612307, '2 - Nordeste', 'PE', 'Saloa', '0055'));
    l.add(new Cidades(3545159, '3 - Sudeste', 'SP', 'Saltinho', '0055'));
    l.add(new Cidades(4215356, '4 - Sul', 'SC', 'Saltinho', '0055'));
    l.add(new Cidades(3545209, '3 - Sudeste', 'SP', 'Salto', '0055'));
    l.add(new Cidades(3157104, '3 - Sudeste', 'MG', 'Salto Da Divisa', '0055'));
    l.add(
        new Cidades(3545308, '3 - Sudeste', 'SP', 'Salto De Pirapora', '0055'));
    l.add(
        new Cidades(5107750, '5 - Centro-Oeste', 'MT', 'Salto Do Ceu', '0055'));
    l.add(new Cidades(4122909, '4 - Sul', 'PR', 'Salto Do Itarare', '0055'));
    l.add(new Cidades(4316451, '4 - Sul', 'RS', 'Salto Do Jacui', '0055'));
    l.add(new Cidades(4123006, '4 - Sul', 'PR', 'Salto Do Lontra', '0055'));
    l.add(new Cidades(3545407, '3 - Sudeste', 'SP', 'Salto Grande', '0055'));
    l.add(new Cidades(4215406, '4 - Sul', 'SC', 'Salto Veloso', '0055'));
    l.add(new Cidades(2927408, '2 - Nordeste', 'BA', 'Salvador', '0055'));
    l.add(
        new Cidades(4316477, '4 - Sul', 'RS', 'Salvador Das Missoes', '0055'));
    l.add(new Cidades(4316501, '4 - Sul', 'RS', 'Salvador Do Sul', '0055'));
    l.add(new Cidades(1506302, '1 - Norte', 'PA', 'Salvaterra', '0055'));
    l.add(new Cidades(2109700, '2 - Nordeste', 'MA', 'Sambaiba', '0055'));
    l.add(new Cidades(1718808, '1 - Norte', 'TO', 'Sampaio', '0055'));
    l.add(new Cidades(4316600, '4 - Sul', 'RS', 'Sananduva', '0055'));
    l.add(new Cidades(
        5219001, '5 - Centro-Oeste', 'GO', 'Sanclerlandia', '0055'));
    l.add(new Cidades(1718840, '1 - Norte', 'TO', 'Sandolandia', '0055'));
    l.add(new Cidades(3545506, '3 - Sudeste', 'SP', 'Sandovalina', '0055'));
    l.add(new Cidades(4215455, '4 - Sul', 'SC', 'Sangao', '0055'));
    l.add(new Cidades(2612406, '2 - Nordeste', 'PE', 'Sanharo', '0055'));
    l.add(new Cidades(
        4317103, '4 - Sul', 'RS', 'Sant` Ana Do Livramento', '0055'));
    l.add(new Cidades(3545605, '3 - Sudeste', 'SP', 'Santa Adelia', '0055'));
    l.add(new Cidades(3545704, '3 - Sudeste', 'SP', 'Santa Albertina', '0055'));
    l.add(new Cidades(4123105, '4 - Sul', 'PR', 'Santa Amelia', '0055'));
    l.add(new Cidades(2927507, '2 - Nordeste', 'BA', 'Santa Barbara', '0055'));
    l.add(new Cidades(3157203, '3 - Sudeste', 'MG', 'Santa Barbara', '0055'));
    l.add(new Cidades(
        3545803, '3 - Sudeste', 'SP', 'Santa Barbara D`Oeste', '0055'));
    l.add(new Cidades(
        5219100, '5 - Centro-Oeste', 'GO', 'Santa Barbara De Goias', '0055'));
    l.add(new Cidades(
        3157252, '3 - Sudeste', 'MG', 'Santa Barbara Do Leste', '0055'));
    l.add(new Cidades(
        3157278, '3 - Sudeste', 'MG', 'Santa Barbara Do Monte Verde', '0055'));
    l.add(new Cidades(
        1506351, '1 - Norte', 'PA', 'Santa Barbara Do Para', '0055'));
    l.add(
        new Cidades(4316709, '4 - Sul', 'RS', 'Santa Barbara Do Sul', '0055'));
    l.add(new Cidades(
        3157302, '3 - Sudeste', 'MG', 'Santa Barbara Do Tugurio', '0055'));
    l.add(new Cidades(3546009, '3 - Sudeste', 'SP', 'Santa Branca', '0055'));
    l.add(new Cidades(2927606, '2 - Nordeste', 'BA', 'Santa Brigida', '0055'));
    l.add(
        new Cidades(5107248, '5 - Centro-Oeste', 'MT', 'Santa Carmem', '0055'));
    l.add(new Cidades(2513158, '2 - Nordeste', 'PB', 'Santa Cecilia', '0055'));
    l.add(new Cidades(4215505, '4 - Sul', 'SC', 'Santa Cecilia', '0055'));
    l.add(new Cidades(
        4123204, '4 - Sul', 'PR', 'Santa Cecilia Do Pavao', '0055'));
    l.add(
        new Cidades(4316733, '4 - Sul', 'RS', 'Santa Cecilia Do Sul', '0055'));
    l.add(new Cidades(
        3546108, '3 - Sudeste', 'SP', 'Santa Clara D`Oeste', '0055'));
    l.add(new Cidades(4316758, '4 - Sul', 'RS', 'Santa Clara Do Sul', '0055'));
    l.add(new Cidades(2411205, '2 - Nordeste', 'RN', 'Santa Cruz', '0055'));
    l.add(new Cidades(2513208, '2 - Nordeste', 'PB', 'Santa Cruz', '0055'));
    l.add(new Cidades(2612455, '2 - Nordeste', 'PE', 'Santa Cruz', '0055'));
    l.add(new Cidades(
        2927705, '2 - Nordeste', 'BA', 'Santa Cruz Cabralia', '0055'));
    l.add(new Cidades(
        2612471, '2 - Nordeste', 'PE', 'Santa Cruz Da Baixa Verde', '0055'));
    l.add(new Cidades(
        3546207, '3 - Sudeste', 'SP', 'Santa Cruz Da Conceicao', '0055'));
    l.add(new Cidades(
        3546256, '3 - Sudeste', 'SP', 'Santa Cruz Da Esperanca', '0055'));
    l.add(new Cidades(
        2927804, '2 - Nordeste', 'BA', 'Santa Cruz Da Vitoria', '0055'));
    l.add(new Cidades(
        3546306, '3 - Sudeste', 'SP', 'Santa Cruz Das Palmeiras', '0055'));
    l.add(new Cidades(
        5219209, '5 - Centro-Oeste', 'GO', 'Santa Cruz De Goias', '0055'));
    l.add(new Cidades(
        3157336, '3 - Sudeste', 'MG', 'Santa Cruz De Minas', '0055'));
    l.add(new Cidades(
        4123303, '4 - Sul', 'PR', 'Santa Cruz De Monte Castelo', '0055'));
    l.add(new Cidades(
        3157377, '3 - Sudeste', 'MG', 'Santa Cruz De Salinas', '0055'));
    l.add(
        new Cidades(1506401, '1 - Norte', 'PA', 'Santa Cruz Do Arari', '0055'));
    l.add(new Cidades(
        2612505, '2 - Nordeste', 'PE', 'Santa Cruz Do Capibaribe', '0055'));
    l.add(new Cidades(
        3157401, '3 - Sudeste', 'MG', 'Santa Cruz Do Escalvado', '0055'));
    l.add(new Cidades(
        2209104, '2 - Nordeste', 'PI', 'Santa Cruz Do Piaui', '0055'));
    l.add(new Cidades(
        3546405, '3 - Sudeste', 'SP', 'Santa Cruz Do Rio Pardo', '0055'));
    l.add(new Cidades(4316808, '4 - Sul', 'RS', 'Santa Cruz Do Sul', '0055'));
    l.add(new Cidades(
        5107743, '5 - Centro-Oeste', 'MT', 'Santa Cruz Do Xingu', '0055'));
    l.add(new Cidades(
        2209153, '2 - Nordeste', 'PI', 'Santa Cruz Dos Milagres', '0055'));
    l.add(new Cidades(
        3157500, '3 - Sudeste', 'MG', 'Santa Efigenia De Minas', '0055'));
    l.add(new Cidades(3546504, '3 - Sudeste', 'SP', 'Santa Ernestina', '0055'));
    l.add(new Cidades(4123402, '4 - Sul', 'PR', 'Santa Fe', '0055'));
    l.add(new Cidades(
        5219258, '5 - Centro-Oeste', 'GO', 'Santa Fe De Goias', '0055'));
    l.add(
        new Cidades(3157609, '3 - Sudeste', 'MG', 'Santa Fe De Minas', '0055'));
    l.add(new Cidades(
        1718865, '1 - Norte', 'TO', 'Santa Fe Do Araguaia', '0055'));
    l.add(new Cidades(3546603, '3 - Sudeste', 'SP', 'Santa Fe Do Sul', '0055'));
    l.add(new Cidades(2209203, '2 - Nordeste', 'PI', 'Santa Filomena', '0055'));
    l.add(new Cidades(2612554, '2 - Nordeste', 'PE', 'Santa Filomena', '0055'));
    l.add(new Cidades(
        2109759, '2 - Nordeste', 'MA', 'Santa Filomena Do Maranhao', '0055'));
    l.add(new Cidades(3546702, '3 - Sudeste', 'SP', 'Santa Gertrudes', '0055'));
    l.add(new Cidades(2109809, '2 - Nordeste', 'MA', 'Santa Helena', '0055'));
    l.add(new Cidades(2513307, '2 - Nordeste', 'PB', 'Santa Helena', '0055'));
    l.add(new Cidades(4123501, '4 - Sul', 'PR', 'Santa Helena', '0055'));
    l.add(new Cidades(4215554, '4 - Sul', 'SC', 'Santa Helena', '0055'));
    l.add(new Cidades(
        5219308, '5 - Centro-Oeste', 'GO', 'Santa Helena De Goias', '0055'));
    l.add(new Cidades(
        3157658, '3 - Sudeste', 'MG', 'Santa Helena De Minas', '0055'));
    l.add(new Cidades(2109908, '2 - Nordeste', 'MA', 'Santa Ines', '0055'));
    l.add(new Cidades(2513356, '2 - Nordeste', 'PB', 'Santa Ines', '0055'));
    l.add(new Cidades(2927903, '2 - Nordeste', 'BA', 'Santa Ines', '0055'));
    l.add(new Cidades(4123600, '4 - Sul', 'PR', 'Santa Ines', '0055'));
    l.add(new Cidades(3546801, '3 - Sudeste', 'SP', 'Santa Isabel', '0055'));
    l.add(
        new Cidades(5219357, '5 - Centro-Oeste', 'GO', 'Santa Isabel', '0055'));
    l.add(
        new Cidades(4123709, '4 - Sul', 'PR', 'Santa Isabel Do Ivai', '0055'));
    l.add(new Cidades(
        1506500, '1 - Norte', 'PA', 'Santa Isabel Do Para', '0055'));
    l.add(new Cidades(
        1303601, '1 - Norte', 'AM', 'Santa Isabel Do Rio Negro', '0055'));
    l.add(
        new Cidades(4123808, '4 - Sul', 'PR', 'Santa Izabel Do Oeste', '0055'));
    l.add(new Cidades(3157708, '3 - Sudeste', 'MG', 'Santa Juliana', '0055'));
    l.add(
        new Cidades(3204500, '3 - Sudeste', 'ES', 'Santa Leopoldina', '0055'));
    l.add(new Cidades(3546900, '3 - Sudeste', 'SP', 'Santa Lucia', '0055'));
    l.add(new Cidades(4123824, '4 - Sul', 'PR', 'Santa Lucia', '0055'));
    l.add(new Cidades(2209302, '2 - Nordeste', 'PI', 'Santa Luz', '0055'));
    l.add(new Cidades(2110005, '2 - Nordeste', 'MA', 'Santa Luzia', '0055'));
    l.add(new Cidades(2513406, '2 - Nordeste', 'PB', 'Santa Luzia', '0055'));
    l.add(new Cidades(2928059, '2 - Nordeste', 'BA', 'Santa Luzia', '0055'));
    l.add(new Cidades(3157807, '3 - Sudeste', 'MG', 'Santa Luzia', '0055'));
    l.add(
        new Cidades(1100296, '1 - Norte', 'RO', 'Santa Luzia D`Oeste', '0055'));
    l.add(new Cidades(
        2806305, '2 - Nordeste', 'SE', 'Santa Luzia Do Itanhy', '0055'));
    l.add(new Cidades(
        2707909, '2 - Nordeste', 'AL', 'Santa Luzia Do Norte', '0055'));
    l.add(
        new Cidades(1506559, '1 - Norte', 'PA', 'Santa Luzia Do Para', '0055'));
    l.add(new Cidades(
        2110039, '2 - Nordeste', 'MA', 'Santa Luzia Do Parua', '0055'));
    l.add(new Cidades(3157906, '3 - Sudeste', 'MG', 'Santa Margarida', '0055'));
    l.add(new Cidades(
        4316972, '4 - Sul', 'RS', 'Santa Margarida Do Sul', '0055'));
    l.add(new Cidades(2409332, '2 - Nordeste', 'RN', 'Santa Maria', '0055'));
    l.add(new Cidades(4316907, '4 - Sul', 'RS', 'Santa Maria', '0055'));
    l.add(new Cidades(
        2612604, '2 - Nordeste', 'PE', 'Santa Maria Da Boa Vista', '0055'));
    l.add(new Cidades(
        3547007, '3 - Sudeste', 'SP', 'Santa Maria Da Serra', '0055'));
    l.add(new Cidades(
        2928109, '2 - Nordeste', 'BA', 'Santa Maria Da Vitoria', '0055'));
    l.add(new Cidades(
        1506583, '1 - Norte', 'PA', 'Santa Maria Das Barreiras', '0055'));
    l.add(new Cidades(
        3158003, '3 - Sudeste', 'MG', 'Santa Maria De Itabira', '0055'));
    l.add(new Cidades(
        3204559, '3 - Sudeste', 'ES', 'Santa Maria De Jetiba', '0055'));
    l.add(new Cidades(
        2612703, '2 - Nordeste', 'PE', 'Santa Maria Do Cambuca', '0055'));
    l.add(
        new Cidades(4316956, '4 - Sul', 'RS', 'Santa Maria Do Herval', '0055'));
    l.add(
        new Cidades(4123857, '4 - Sul', 'PR', 'Santa Maria Do Oeste', '0055'));
    l.add(
        new Cidades(1506609, '1 - Norte', 'PA', 'Santa Maria Do Para', '0055'));
    l.add(new Cidades(
        3158102, '3 - Sudeste', 'MG', 'Santa Maria Do Salto', '0055'));
    l.add(new Cidades(
        3158201, '3 - Sudeste', 'MG', 'Santa Maria Do Suacui', '0055'));
    l.add(new Cidades(1718881, '1 - Norte', 'TO', 'Santa Maria Do To', '0055'));
    l.add(new Cidades(
        3304607, '3 - Sudeste', 'RJ', 'Santa Maria Madalena', '0055'));
    l.add(new Cidades(4123907, '4 - Sul', 'PR', 'Santa Mariana', '0055'));
    l.add(new Cidades(3547106, '3 - Sudeste', 'SP', 'Santa Mercedes', '0055'));
    l.add(new Cidades(4123956, '4 - Sul', 'PR', 'Santa Monica', '0055'));
    l.add(new Cidades(2312205, '2 - Nordeste', 'CE', 'Santa Quiteria', '0055'));
    l.add(new Cidades(
        2110104, '2 - Nordeste', 'MA', 'Santa Quiteria Do Maranhao', '0055'));
    l.add(new Cidades(2110203, '2 - Nordeste', 'MA', 'Santa Rita', '0055'));
    l.add(new Cidades(2513703, '2 - Nordeste', 'PB', 'Santa Rita', '0055'));
    l.add(new Cidades(
        3547403, '3 - Sudeste', 'SP', 'Santa Rita D`Oeste', '0055'));
    l.add(new Cidades(
        3159209, '3 - Sudeste', 'MG', 'Santa Rita De Caldas', '0055'));
    l.add(new Cidades(
        2928406, '2 - Nordeste', 'BA', 'Santa Rita De Cassia', '0055'));
    l.add(new Cidades(
        3159407, '3 - Sudeste', 'MG', 'Santa Rita De Ibitipoca', '0055'));
    l.add(new Cidades(
        3159308, '3 - Sudeste', 'MG', 'Santa Rita De Jacutinga', '0055'));
    l.add(new Cidades(
        3159357, '3 - Sudeste', 'MG', 'Santa Rita De Minas', '0055'));
    l.add(new Cidades(
        5219407, '5 - Centro-Oeste', 'GO', 'Santa Rita Do Araguaia', '0055'));
    l.add(new Cidades(
        3159506, '3 - Sudeste', 'MG', 'Santa Rita Do Itueto', '0055'));
    l.add(new Cidades(5219456, '5 - Centro-Oeste', 'GO',
        'Santa Rita Do Novo Destino', '0055'));
    l.add(new Cidades(
        5007554, '5 - Centro-Oeste', 'MS', 'Santa Rita Do Pardo', '0055'));
    l.add(new Cidades(
        3547502, '3 - Sudeste', 'SP', 'Santa Rita Do Passa Quatro', '0055'));
    l.add(new Cidades(
        3159605, '3 - Sudeste', 'MG', 'Santa Rita Do Sapucai', '0055'));
    l.add(new Cidades(1718899, '1 - Norte', 'TO', 'Santa Rita Do To', '0055'));
    l.add(new Cidades(
        5107768, '5 - Centro-Oeste', 'MT', 'Santa Rita Do Trivelato', '0055'));
    l.add(new Cidades(4317202, '4 - Sul', 'RS', 'Santa Rosa', '0055'));
    l.add(new Cidades(
        3159704, '3 - Sudeste', 'MG', 'Santa Rosa Da Serra', '0055'));
    l.add(new Cidades(
        5219506, '5 - Centro-Oeste', 'GO', 'Santa Rosa De Goias', '0055'));
    l.add(new Cidades(
        2806503, '2 - Nordeste', 'SE', 'Santa Rosa De Lima', '0055'));
    l.add(new Cidades(4215604, '4 - Sul', 'SC', 'Santa Rosa De Lima', '0055'));
    l.add(new Cidades(
        3547601, '3 - Sudeste', 'SP', 'Santa Rosa De Viterbo', '0055'));
    l.add(new Cidades(
        2209377, '2 - Nordeste', 'PI', 'Santa Rosa Do Piaui', '0055'));
    l.add(
        new Cidades(1200435, '1 - Norte', 'AC', 'Santa Rosa Do Purus', '0055'));
    l.add(new Cidades(4215653, '4 - Sul', 'SC', 'Santa Rosa Do Sul', '0055'));
    l.add(new Cidades(1718907, '1 - Norte', 'TO', 'Santa Rosa Do To', '0055'));
    l.add(new Cidades(3547650, '3 - Sudeste', 'SP', 'Santa Salete', '0055'));
    l.add(new Cidades(3204609, '3 - Sudeste', 'ES', 'Santa Teresa', '0055'));
    l.add(
        new Cidades(2513802, '2 - Nordeste', 'PB', 'Santa Teresinha', '0055'));
    l.add(
        new Cidades(2928505, '2 - Nordeste', 'BA', 'Santa Teresinha', '0055'));
    l.add(new Cidades(4317251, '4 - Sul', 'RS', 'Santa Tereza', '0055'));
    l.add(new Cidades(
        5219605, '5 - Centro-Oeste', 'GO', 'Santa Tereza De Goias', '0055'));
    l.add(
        new Cidades(4124020, '4 - Sul', 'PR', 'Santa Tereza Do Oeste', '0055'));
    l.add(
        new Cidades(1719004, '1 - Norte', 'TO', 'Santa Tereza Do To', '0055'));
    l.add(
        new Cidades(2612802, '2 - Nordeste', 'PE', 'Santa Terezinha', '0055'));
    l.add(new Cidades(4215679, '4 - Sul', 'SC', 'Santa Terezinha', '0055'));
    l.add(new Cidades(
        5107776, '5 - Centro-Oeste', 'MT', 'Santa Terezinha', '0055'));
    l.add(new Cidades(
        5219704, '5 - Centro-Oeste', 'GO', 'Santa Terezinha De Goias', '0055'));
    l.add(new Cidades(
        4124053, '4 - Sul', 'PR', 'Santa Terezinha De Itaipu', '0055'));
    l.add(new Cidades(
        4215687, '4 - Sul', 'SC', 'Santa Terezinha Do Progresso', '0055'));
    l.add(new Cidades(
        1720002, '1 - Norte', 'TO', 'Santa Terezinha Do To', '0055'));
    l.add(new Cidades(3159803, '3 - Sudeste', 'MG', 'Santa Vitoria', '0055'));
    l.add(new Cidades(
        4317301, '4 - Sul', 'RS', 'Santa Vitoria Do Palmar', '0055'));
    l.add(new Cidades(2928000, '2 - Nordeste', 'BA', 'Santaluz', '0055'));
    l.add(new Cidades(1600600, '1 - Norte', 'AP', 'Santana', '0055'));
    l.add(new Cidades(2928208, '2 - Nordeste', 'BA', 'Santana', '0055'));
    l.add(
        new Cidades(4317004, '4 - Sul', 'RS', 'Santana Da Boa Vista', '0055'));
    l.add(new Cidades(
        3547205, '3 - Sudeste', 'SP', 'Santana Da Ponte Pensa', '0055'));
    l.add(
        new Cidades(3158300, '3 - Sudeste', 'MG', 'Santana Da Vargem', '0055'));
    l.add(new Cidades(
        3158409, '3 - Sudeste', 'MG', 'Santana De Cataguases', '0055'));
    l.add(new Cidades(
        2513505, '2 - Nordeste', 'PB', 'Santana De Mangueira', '0055'));
    l.add(new Cidades(
        3547304, '3 - Sudeste', 'SP', 'Santana De Parnaiba', '0055'));
    l.add(new Cidades(
        3158508, '3 - Sudeste', 'MG', 'Santana De Pirapama', '0055'));
    l.add(new Cidades(
        2312007, '2 - Nordeste', 'CE', 'Santana Do Acarau', '0055'));
    l.add(
        new Cidades(1506708, '1 - Norte', 'PA', 'Santana Do Araguaia', '0055'));
    l.add(new Cidades(
        2312106, '2 - Nordeste', 'CE', 'Santana Do Cariri', '0055'));
    l.add(new Cidades(
        3158607, '3 - Sudeste', 'MG', 'Santana Do Deserto', '0055'));
    l.add(new Cidades(
        3158706, '3 - Sudeste', 'MG', 'Santana Do Garambeu', '0055'));
    l.add(new Cidades(
        2708006, '2 - Nordeste', 'AL', 'Santana Do Ipanema', '0055'));
    l.add(new Cidades(4124004, '4 - Sul', 'PR', 'Santana Do Itarare', '0055'));
    l.add(
        new Cidades(3158805, '3 - Sudeste', 'MG', 'Santana Do Jacare', '0055'));
    l.add(new Cidades(
        3158904, '3 - Sudeste', 'MG', 'Santana Do Manhuacu', '0055'));
    l.add(new Cidades(
        2110237, '2 - Nordeste', 'MA', 'Santana Do Maranhao', '0055'));
    l.add(
        new Cidades(2411403, '2 - Nordeste', 'RN', 'Santana Do Matos', '0055'));
    l.add(new Cidades(
        2708105, '2 - Nordeste', 'AL', 'Santana Do Mundau', '0055'));
    l.add(new Cidades(
        3158953, '3 - Sudeste', 'MG', 'Santana Do Paraiso', '0055'));
    l.add(
        new Cidades(2209351, '2 - Nordeste', 'PI', 'Santana Do Piaui', '0055'));
    l.add(
        new Cidades(3159001, '3 - Sudeste', 'MG', 'Santana Do Riacho', '0055'));
    l.add(new Cidades(
        2806404, '2 - Nordeste', 'SE', 'Santana Do Sao Francisco', '0055'));
    l.add(new Cidades(
        2411429, '2 - Nordeste', 'RN', 'Santana Do Serido', '0055'));
    l.add(new Cidades(
        2513604, '2 - Nordeste', 'PB', 'Santana Dos Garrotes', '0055'));
    l.add(new Cidades(
        3159100, '3 - Sudeste', 'MG', 'Santana Dos Montes', '0055'));
    l.add(new Cidades(2928307, '2 - Nordeste', 'BA', 'Santanopolis', '0055'));
    l.add(new Cidades(1506807, '1 - Norte', 'PA', 'Santarem', '0055'));
    l.add(new Cidades(2513653, '2 - Nordeste', 'PB', 'Santarem', '0055'));
    l.add(new Cidades(1506906, '1 - Norte', 'PA', 'Santarem Novo', '0055'));
    l.add(new Cidades(4317400, '4 - Sul', 'RS', 'Santiago', '0055'));
    l.add(new Cidades(4215695, '4 - Sul', 'SC', 'Santiago Do Sul', '0055'));
    l.add(
        new Cidades(5107263, '5 - Centro-Oeste', 'MT', 'Santo Afonso', '0055'));
    l.add(new Cidades(2928604, '2 - Nordeste', 'BA', 'Santo Amaro', '0055'));
    l.add(new Cidades(
        4215703, '4 - Sul', 'SC', 'Santo Amaro Da Imperatriz', '0055'));
    l.add(new Cidades(
        2806602, '2 - Nordeste', 'SE', 'Santo Amaro Das Brotas', '0055'));
    l.add(new Cidades(
        2110278, '2 - Nordeste', 'MA', 'Santo Amaro Do Maranhao', '0055'));
    l.add(new Cidades(3547700, '3 - Sudeste', 'SP', 'Santo Anastacio', '0055'));
    l.add(new Cidades(2513851, '2 - Nordeste', 'PB', 'Santo Andre', '0055'));
    l.add(new Cidades(3547809, '3 - Sudeste', 'SP', 'Santo Andre', '0055'));
    l.add(new Cidades(4317509, '4 - Sul', 'RS', 'Santo Angelo', '0055'));
    l.add(new Cidades(2411502, '2 - Nordeste', 'RN', 'Santo Antonio', '0055'));
    l.add(new Cidades(
        3547908, '3 - Sudeste', 'SP', 'Santo Antonio Da Alegria', '0055'));
    l.add(new Cidades(
        5219712, '5 - Centro-Oeste', 'GO', 'Santo Antonio Da Barra', '0055'));
    l.add(new Cidades(
        4317608, '4 - Sul', 'RS', 'Santo Antonio Da Patrulha', '0055'));
    l.add(new Cidades(
        4124103, '4 - Sul', 'PR', 'Santo Antonio Da Platina', '0055'));
    l.add(new Cidades(
        4317707, '4 - Sul', 'RS', 'Santo Antonio Das Missoes', '0055'));
    l.add(new Cidades(
        5219738, '5 - Centro-Oeste', 'GO', 'Santo Antonio De Goias', '0055'));
    l.add(new Cidades(
        2928703, '2 - Nordeste', 'BA', 'Santo Antonio De Jesus', '0055'));
    l.add(new Cidades(
        2209401, '2 - Nordeste', 'PI', 'Santo Antonio De Lisboa', '0055'));
    l.add(new Cidades(
        3304706, '3 - Sudeste', 'RJ', 'Santo Antonio De Padua', '0055'));
    l.add(new Cidades(
        3548005, '3 - Sudeste', 'SP', 'Santo Antonio De Posse', '0055'));
    l.add(new Cidades(
        3159902, '3 - Sudeste', 'MG', 'Santo Antonio Do Amparo', '0055'));
    l.add(new Cidades(
        3548054, '3 - Sudeste', 'SP', 'Santo Antonio Do Aracangua', '0055'));
    l.add(new Cidades(
        3160009, '3 - Sudeste', 'MG', 'Santo Antonio Do Aventureiro', '0055'));
    l.add(new Cidades(
        4124202, '4 - Sul', 'PR', 'Santo Antonio Do Caiua', '0055'));
    l.add(new Cidades(5219753, '5 - Centro-Oeste', 'GO',
        'Santo Antonio Do Descoberto', '0055'));
    l.add(new Cidades(
        3160108, '3 - Sudeste', 'MG', 'Santo Antonio Do Grama', '0055'));
    l.add(new Cidades(
        1303700, '1 - Norte', 'AM', 'Santo Antonio Do Ica', '0055'));
    l.add(new Cidades(
        3160207, '3 - Sudeste', 'MG', 'Santo Antonio Do Itambe', '0055'));
    l.add(new Cidades(
        3160306, '3 - Sudeste', 'MG', 'Santo Antonio Do Jacinto', '0055'));
    l.add(new Cidades(
        3548104, '3 - Sudeste', 'SP', 'Santo Antonio Do Jardim', '0055'));
    l.add(new Cidades(
        5107792, '5 - Centro-Oeste', 'MT', 'Santo Antonio Do Leste', '0055'));
    l.add(new Cidades(5107800, '5 - Centro-Oeste', 'MT',
        'Santo Antonio Do Leverger', '0055'));
    l.add(new Cidades(
        3160405, '3 - Sudeste', 'MG', 'Santo Antonio Do Monte', '0055'));
    l.add(new Cidades(
        4317558, '4 - Sul', 'RS', 'Santo Antonio Do Palma', '0055'));
    l.add(new Cidades(
        4124301, '4 - Sul', 'PR', 'Santo Antonio Do Paraiso', '0055'));
    l.add(new Cidades(
        3548203, '3 - Sudeste', 'SP', 'Santo Antonio Do Pinhal', '0055'));
    l.add(new Cidades(
        4317756, '4 - Sul', 'RS', 'Santo Antonio Do Planalto', '0055'));
    l.add(new Cidades(
        3160454, '3 - Sudeste', 'MG', 'Santo Antonio Do Retiro', '0055'));
    l.add(new Cidades(
        3160504, '3 - Sudeste', 'MG', 'Santo Antonio Do Rio Abaixo', '0055'));
    l.add(new Cidades(
        4124400, '4 - Sul', 'PR', 'Santo Antonio Do Sudoeste', '0055'));
    l.add(new Cidades(
        1507003, '1 - Norte', 'PA', 'Santo Antonio Do Taua', '0055'));
    l.add(new Cidades(
        2110302, '2 - Nordeste', 'MA', 'Santo Antonio Dos Lopes', '0055'));
    l.add(new Cidades(
        2209450, '2 - Nordeste', 'PI', 'Santo Antonio Dos Milagres', '0055'));
    l.add(new Cidades(4317806, '4 - Sul', 'RS', 'Santo Augusto', '0055'));
    l.add(new Cidades(4317905, '4 - Sul', 'RS', 'Santo Cristo', '0055'));
    l.add(new Cidades(2928802, '2 - Nordeste', 'BA', 'Santo Estevao', '0055'));
    l.add(new Cidades(3548302, '3 - Sudeste', 'SP', 'Santo Expedito', '0055'));
    l.add(
        new Cidades(4317954, '4 - Sul', 'RS', 'Santo Expedito Do Sul', '0055'));
    l.add(new Cidades(3160603, '3 - Sudeste', 'MG', 'Santo Hipolito', '0055'));
    l.add(new Cidades(4124509, '4 - Sul', 'PR', 'Santo Inacio', '0055'));
    l.add(new Cidades(
        2209500, '2 - Nordeste', 'PI', 'Santo Inacio Do Piaui', '0055'));
    l.add(new Cidades(
        3548401, '3 - Sudeste', 'SP', 'Santopolis Do Aguapei', '0055'));
    l.add(new Cidades(3548500, '3 - Sudeste', 'SP', 'Santos', '0055'));
    l.add(new Cidades(3160702, '3 - Sudeste', 'MG', 'Santos Dumont', '0055'));
    l.add(new Cidades(2312304, '2 - Nordeste', 'CE', 'Sao Benedito', '0055'));
    l.add(new Cidades(
        2110401, '2 - Nordeste', 'MA', 'Sao Benedito Do Rio Preto', '0055'));
    l.add(new Cidades(
        2612901, '2 - Nordeste', 'PE', 'Sao Benedito Do Sul', '0055'));
    l.add(new Cidades(2513927, '2 - Nordeste', 'PB', 'Sao Bentinho', '0055'));
    l.add(new Cidades(2110500, '2 - Nordeste', 'MA', 'Sao Bento', '0055'));
    l.add(new Cidades(2513901, '2 - Nordeste', 'PB', 'Sao Bento', '0055'));
    l.add(new Cidades(3160801, '3 - Sudeste', 'MG', 'Sao Bento Abade', '0055'));
    l.add(new Cidades(
        2411601, '2 - Nordeste', 'RN', 'Sao Bento Do Norte', '0055'));
    l.add(new Cidades(
        3548609, '3 - Sudeste', 'SP', 'Sao Bento Do Sapucai', '0055'));
    l.add(new Cidades(4215802, '4 - Sul', 'SC', 'Sao Bento Do Sul', '0055'));
    l.add(new Cidades(1720101, '1 - Norte', 'TO', 'Sao Bento Do To', '0055'));
    l.add(new Cidades(
        2411700, '2 - Nordeste', 'RN', 'Sao Bento Do Trairi', '0055'));
    l.add(
        new Cidades(2613008, '2 - Nordeste', 'PE', 'Sao Bento Do Una', '0055'));
    l.add(new Cidades(4215752, '4 - Sul', 'SC', 'Sao Bernardino', '0055'));
    l.add(new Cidades(2110609, '2 - Nordeste', 'MA', 'Sao Bernardo', '0055'));
    l.add(new Cidades(
        3548708, '3 - Sudeste', 'SP', 'Sao Bernardo Do Campo', '0055'));
    l.add(new Cidades(4215901, '4 - Sul', 'SC', 'Sao Bonifacio', '0055'));
    l.add(new Cidades(4318002, '4 - Sul', 'RS', 'Sao Borja', '0055'));
    l.add(new Cidades(2708204, '2 - Nordeste', 'AL', 'Sao Bras', '0055'));
    l.add(new Cidades(
        3160900, '3 - Sudeste', 'MG', 'Sao Bras Do Suacui', '0055'));
    l.add(new Cidades(
        2209559, '2 - Nordeste', 'PI', 'Sao Braz Do Piaui', '0055'));
    l.add(new Cidades(
        1507102, '1 - Norte', 'PA', 'Sao Caetano De Odivelas', '0055'));
    l.add(new Cidades(
        3548807, '3 - Sudeste', 'SP', 'Sao Caetano Do Sul', '0055'));
    l.add(new Cidades(2613107, '2 - Nordeste', 'PE', 'Sao Caitano', '0055'));
    l.add(new Cidades(3548906, '3 - Sudeste', 'SP', 'Sao Carlos', '0055'));
    l.add(new Cidades(4216008, '4 - Sul', 'SC', 'Sao Carlos', '0055'));
    l.add(new Cidades(4124608, '4 - Sul', 'PR', 'Sao Carlos Do Ivai', '0055'));
    l.add(new Cidades(2806701, '2 - Nordeste', 'SE', 'Sao Cristovao', '0055'));
    l.add(
        new Cidades(4216057, '4 - Sul', 'SC', 'Sao Cristovao Do Sul', '0055'));
    l.add(new Cidades(2928901, '2 - Nordeste', 'BA', 'Sao Desiderio', '0055'));
    l.add(new Cidades(2513968, '2 - Nordeste', 'PB', 'Sao Domingos', '0055'));
    l.add(new Cidades(2806800, '2 - Nordeste', 'SE', 'Sao Domingos', '0055'));
    l.add(new Cidades(2928950, '2 - Nordeste', 'BA', 'Sao Domingos', '0055'));
    l.add(new Cidades(4216107, '4 - Sul', 'SC', 'Sao Domingos', '0055'));
    l.add(
        new Cidades(5219803, '5 - Centro-Oeste', 'GO', 'Sao Domingos', '0055'));
    l.add(new Cidades(
        3160959, '3 - Sudeste', 'MG', 'Sao Domingos Das Dores', '0055'));
    l.add(new Cidades(
        1507151, '1 - Norte', 'PA', 'Sao Domingos Do Araguaia', '0055'));
    l.add(new Cidades(
        2110658, '2 - Nordeste', 'MA', 'Sao Domingos Do Azeitao', '0055'));
    l.add(new Cidades(
        1507201, '1 - Norte', 'PA', 'Sao Domingos Do Capim', '0055'));
    l.add(new Cidades(
        2513943, '2 - Nordeste', 'PB', 'Sao Domingos Do Cariri', '0055'));
    l.add(new Cidades(
        2110708, '2 - Nordeste', 'MA', 'Sao Domingos Do Maranhao', '0055'));
    l.add(new Cidades(
        3204658, '3 - Sudeste', 'ES', 'Sao Domingos Do Norte', '0055'));
    l.add(new Cidades(
        3161007, '3 - Sudeste', 'MG', 'Sao Domingos Do Prata', '0055'));
    l.add(new Cidades(4318051, '4 - Sul', 'RS', 'Sao Domingos Do Sul', '0055'));
    l.add(new Cidades(2929107, '2 - Nordeste', 'BA', 'Sao Felipe', '0055'));
    l.add(
        new Cidades(1101484, '1 - Norte', 'RO', 'Sao Felipe D`Oeste', '0055'));
    l.add(new Cidades(2929008, '2 - Nordeste', 'BA', 'Sao Felix', '0055'));
    l.add(new Cidades(
        2110807, '2 - Nordeste', 'MA', 'Sao Felix De Balsas', '0055'));
    l.add(new Cidades(
        3161056, '3 - Sudeste', 'MG', 'Sao Felix De Minas', '0055'));
    l.add(new Cidades(
        5107859, '5 - Centro-Oeste', 'MT', 'Sao Felix Do Araguaia', '0055'));
    l.add(new Cidades(
        2929057, '2 - Nordeste', 'BA', 'Sao Felix Do Coribe', '0055'));
    l.add(new Cidades(
        2209609, '2 - Nordeste', 'PI', 'Sao Felix Do Piaui', '0055'));
    l.add(new Cidades(1720150, '1 - Norte', 'TO', 'Sao Felix Do To', '0055'));
    l.add(
        new Cidades(1507300, '1 - Norte', 'PA', 'Sao Felix Do Xingu', '0055'));
    l.add(new Cidades(2411809, '2 - Nordeste', 'RN', 'Sao Fernando', '0055'));
    l.add(new Cidades(3304805, '3 - Sudeste', 'RJ', 'Sao Fidelis', '0055'));
    l.add(new Cidades(2513984, '2 - Nordeste', 'PB', 'Sao Francisco', '0055'));
    l.add(new Cidades(2806909, '2 - Nordeste', 'SE', 'Sao Francisco', '0055'));
    l.add(new Cidades(3161106, '3 - Sudeste', 'MG', 'Sao Francisco', '0055'));
    l.add(new Cidades(3549003, '3 - Sudeste', 'SP', 'Sao Francisco', '0055'));
    l.add(new Cidades(
        4318101, '4 - Sul', 'RS', 'Sao Francisco De Assis', '0055'));
    l.add(new Cidades(2209658, '2 - Nordeste', 'PI',
        'Sao Francisco De Assis Do Piaui', '0055'));
    l.add(new Cidades(
        5219902, '5 - Centro-Oeste', 'GO', 'Sao Francisco De Goias', '0055'));
    l.add(new Cidades(
        3304755, '3 - Sudeste', 'RJ', 'Sao Francisco De Itabapoana', '0055'));
    l.add(new Cidades(
        3161205, '3 - Sudeste', 'MG', 'Sao Francisco De Paula', '0055'));
    l.add(new Cidades(
        4318200, '4 - Sul', 'RS', 'Sao Francisco De Paula', '0055'));
    l.add(new Cidades(
        3161304, '3 - Sudeste', 'MG', 'Sao Francisco De Sales', '0055'));
    l.add(new Cidades(
        2110856, '2 - Nordeste', 'MA', 'Sao Francisco Do Brejao', '0055'));
    l.add(new Cidades(
        2929206, '2 - Nordeste', 'BA', 'Sao Francisco Do Conde', '0055'));
    l.add(new Cidades(
        3161403, '3 - Sudeste', 'MG', 'Sao Francisco Do Gloria', '0055'));
    l.add(new Cidades(
        1101492, '1 - Norte', 'RO', 'Sao Francisco Do Guapore', '0055'));
    l.add(new Cidades(
        2110906, '2 - Nordeste', 'MA', 'Sao Francisco Do Maranhao', '0055'));
    l.add(new Cidades(
        2411908, '2 - Nordeste', 'RN', 'Sao Francisco Do Oeste', '0055'));
    l.add(new Cidades(
        1507409, '1 - Norte', 'PA', 'Sao Francisco Do Para', '0055'));
    l.add(new Cidades(
        2209708, '2 - Nordeste', 'PI', 'Sao Francisco Do Piaui', '0055'));
    l.add(
        new Cidades(4216206, '4 - Sul', 'SC', 'Sao Francisco Do Sul', '0055'));
    l.add(new Cidades(2929255, '2 - Nordeste', 'BA', 'Sao Gabriel', '0055'));
    l.add(new Cidades(4318309, '4 - Sul', 'RS', 'Sao Gabriel', '0055'));
    l.add(new Cidades(
        1303809, '1 - Norte', 'AM', 'Sao Gabriel Da Cachoeira', '0055'));
    l.add(new Cidades(
        3204708, '3 - Sudeste', 'ES', 'Sao Gabriel Da Palha', '0055'));
    l.add(new Cidades(
        5007695, '5 - Centro-Oeste', 'MS', 'Sao Gabriel Do Oeste', '0055'));
    l.add(new Cidades(3161502, '3 - Sudeste', 'MG', 'Sao Geraldo', '0055'));
    l.add(new Cidades(
        3161601, '3 - Sudeste', 'MG', 'Sao Geraldo Da Piedade', '0055'));
    l.add(new Cidades(
        1507458, '1 - Norte', 'PA', 'Sao Geraldo Do Araguaia', '0055'));
    l.add(new Cidades(
        3161650, '3 - Sudeste', 'MG', 'Sao Geraldo Do Baixio', '0055'));
    l.add(new Cidades(3304904, '3 - Sudeste', 'RJ', 'Sao Goncalo', '0055'));
    l.add(new Cidades(
        3161700, '3 - Sudeste', 'MG', 'Sao Goncalo Do Abaete', '0055'));
    l.add(new Cidades(
        2312403, '2 - Nordeste', 'CE', 'Sao Goncalo Do Amarante', '0055'));
    l.add(new Cidades(
        2412005, '2 - Nordeste', 'RN', 'Sao Goncalo Do Amarante', '0055'));
    l.add(new Cidades(
        2209757, '2 - Nordeste', 'PI', 'Sao Goncalo Do Gurgueia', '0055'));
    l.add(new Cidades(
        3161809, '3 - Sudeste', 'MG', 'Sao Goncalo Do Para', '0055'));
    l.add(new Cidades(
        2209807, '2 - Nordeste', 'PI', 'Sao Goncalo Do Piaui', '0055'));
    l.add(new Cidades(
        3161908, '3 - Sudeste', 'MG', 'Sao Goncalo Do Rio Abaixo', '0055'));
    l.add(new Cidades(
        3125507, '3 - Sudeste', 'MG', 'Sao Goncalo Do Rio Preto', '0055'));
    l.add(new Cidades(
        3162005, '3 - Sudeste', 'MG', 'Sao Goncalo Do Sapucai', '0055'));
    l.add(new Cidades(
        2929305, '2 - Nordeste', 'BA', 'Sao Goncalo Dos Campos', '0055'));
    l.add(new Cidades(3162104, '3 - Sudeste', 'MG', 'Sao Gotardo', '0055'));
    l.add(new Cidades(4318408, '4 - Sul', 'RS', 'Sao Jeronimo', '0055'));
    l.add(
        new Cidades(4124707, '4 - Sul', 'PR', 'Sao Jeronimo Da Serra', '0055'));
    l.add(new Cidades(2613206, '2 - Nordeste', 'PE', 'Sao Joao', '0055'));
    l.add(new Cidades(4124806, '4 - Sul', 'PR', 'Sao Joao', '0055'));
    l.add(
        new Cidades(2111003, '2 - Nordeste', 'MA', 'Sao Joao Batista', '0055'));
    l.add(new Cidades(4216305, '4 - Sul', 'SC', 'Sao Joao Batista', '0055'));
    l.add(new Cidades(
        3162203, '3 - Sudeste', 'MG', 'Sao Joao Batista Do Gloria', '0055'));
    l.add(new Cidades(
        5220009, '5 - Centro-Oeste', 'GO', 'Sao Joao D`Alianca', '0055'));
    l.add(
        new Cidades(1400506, '1 - Norte', 'RR', 'Sao Joao Da Baliza', '0055'));
    l.add(
        new Cidades(3305000, '3 - Sudeste', 'RJ', 'Sao Joao Da Barra', '0055'));
    l.add(new Cidades(
        3549102, '3 - Sudeste', 'SP', 'Sao Joao Da Boa Vista', '0055'));
    l.add(new Cidades(
        2209856, '2 - Nordeste', 'PI', 'Sao Joao Da Canabrava', '0055'));
    l.add(new Cidades(
        2209872, '2 - Nordeste', 'PI', 'Sao Joao Da Fronteira', '0055'));
    l.add(
        new Cidades(3162252, '3 - Sudeste', 'MG', 'Sao Joao Da Lagoa', '0055'));
    l.add(
        new Cidades(3162302, '3 - Sudeste', 'MG', 'Sao Joao Da Mata', '0055'));
    l.add(new Cidades(
        5220058, '5 - Centro-Oeste', 'GO', 'Sao Joao Da Parauna', '0055'));
    l.add(new Cidades(1507466, '1 - Norte', 'PA', 'Sao Joao Da Ponta', '0055'));
    l.add(
        new Cidades(3162401, '3 - Sudeste', 'MG', 'Sao Joao Da Ponte', '0055'));
    l.add(new Cidades(
        2209906, '2 - Nordeste', 'PI', 'Sao Joao Da Serra', '0055'));
    l.add(new Cidades(4318424, '4 - Sul', 'RS', 'Sao Joao Da Urtiga', '0055'));
    l.add(new Cidades(
        2209955, '2 - Nordeste', 'PI', 'Sao Joao Da Varjota', '0055'));
    l.add(new Cidades(
        3549201, '3 - Sudeste', 'SP', 'Sao Joao Das Duas Pontes', '0055'));
    l.add(new Cidades(
        3162450, '3 - Sudeste', 'MG', 'Sao Joao Das Missoes', '0055'));
    l.add(new Cidades(
        3549250, '3 - Sudeste', 'SP', 'Sao Joao De Iracema', '0055'));
    l.add(new Cidades(
        3305109, '3 - Sudeste', 'RJ', 'Sao Joao De Meriti', '0055'));
    l.add(
        new Cidades(1507474, '1 - Norte', 'PA', 'Sao Joao De Pirabas', '0055'));
    l.add(
        new Cidades(3162500, '3 - Sudeste', 'MG', 'Sao Joao Del Rei', '0055'));
    l.add(new Cidades(
        1507508, '1 - Norte', 'PA', 'Sao Joao Do Araguaia', '0055'));
    l.add(new Cidades(
        2209971, '2 - Nordeste', 'PI', 'Sao Joao Do Arraial', '0055'));
    l.add(new Cidades(4124905, '4 - Sul', 'PR', 'Sao Joao Do Caiua', '0055'));
    l.add(new Cidades(
        2514008, '2 - Nordeste', 'PB', 'Sao Joao Do Cariri', '0055'));
    l.add(
        new Cidades(2111029, '2 - Nordeste', 'MA', 'Sao Joao Do Caru', '0055'));
    l.add(
        new Cidades(4216354, '4 - Sul', 'SC', 'Sao Joao Do Itaperiu', '0055'));
    l.add(new Cidades(4125001, '4 - Sul', 'PR', 'Sao Joao Do Ivai', '0055'));
    l.add(new Cidades(
        2312502, '2 - Nordeste', 'CE', 'Sao Joao Do Jaguaribe', '0055'));
    l.add(new Cidades(
        3162559, '3 - Sudeste', 'MG', 'Sao Joao Do Manhuacu', '0055'));
    l.add(new Cidades(
        3162575, '3 - Sudeste', 'MG', 'Sao Joao Do Manteninha', '0055'));
    l.add(new Cidades(4216255, '4 - Sul', 'SC', 'Sao Joao Do Oeste', '0055'));
    l.add(new Cidades(
        3162609, '3 - Sudeste', 'MG', 'Sao Joao Do Oriente', '0055'));
    l.add(
        new Cidades(3162658, '3 - Sudeste', 'MG', 'Sao Joao Do Pacui', '0055'));
    l.add(new Cidades(
        2111052, '2 - Nordeste', 'MA', 'Sao Joao Do Paraiso', '0055'));
    l.add(new Cidades(
        3162708, '3 - Sudeste', 'MG', 'Sao Joao Do Paraiso', '0055'));
    l.add(new Cidades(
        3549300, '3 - Sudeste', 'SP', 'Sao Joao Do Pau D`Alho', '0055'));
    l.add(new Cidades(
        2210003, '2 - Nordeste', 'PI', 'Sao Joao Do Piaui', '0055'));
    l.add(
        new Cidades(4318432, '4 - Sul', 'RS', 'Sao Joao Do Polesine', '0055'));
    l.add(new Cidades(
        2500700, '2 - Nordeste', 'PB', 'Sao Joao Do Rio Do Peixe', '0055'));
    l.add(new Cidades(
        2412104, '2 - Nordeste', 'RN', 'Sao Joao Do Sabugi', '0055'));
    l.add(new Cidades(
        2111078, '2 - Nordeste', 'MA', 'Sao Joao Do Soter', '0055'));
    l.add(new Cidades(4216404, '4 - Sul', 'SC', 'Sao Joao Do Sul', '0055'));
    l.add(new Cidades(
        2514107, '2 - Nordeste', 'PB', 'Sao Joao Do Tigre', '0055'));
    l.add(new Cidades(4125100, '4 - Sul', 'PR', 'Sao Joao Do Triunfo', '0055'));
    l.add(new Cidades(
        2111102, '2 - Nordeste', 'MA', 'Sao Joao Dos Patos', '0055'));
    l.add(new Cidades(
        3162807, '3 - Sudeste', 'MG', 'Sao Joao Evangelista', '0055'));
    l.add(new Cidades(
        3162906, '3 - Sudeste', 'MG', 'Sao Joao Nepomuceno', '0055'));
    l.add(new Cidades(4216503, '4 - Sul', 'SC', 'Sao Joaquim', '0055'));
    l.add(new Cidades(
        3549409, '3 - Sudeste', 'SP', 'Sao Joaquim Da Barra', '0055'));
    l.add(new Cidades(
        3162922, '3 - Sudeste', 'MG', 'Sao Joaquim De Bicas', '0055'));
    l.add(new Cidades(
        2613305, '2 - Nordeste', 'PE', 'Sao Joaquim Do Monte', '0055'));
    l.add(new Cidades(4318440, '4 - Sul', 'RS', 'Sao Jorge', '0055'));
    l.add(new Cidades(4125209, '4 - Sul', 'PR', 'Sao Jorge D`Oeste', '0055'));
    l.add(new Cidades(4125308, '4 - Sul', 'PR', 'Sao Jorge Do Ivai', '0055'));
    l.add(new Cidades(
        4125357, '4 - Sul', 'PR', 'Sao Jorge Do Patrocinio', '0055'));
    l.add(new Cidades(4216602, '4 - Sul', 'SC', 'Sao Jose', '0055'));
    l.add(
        new Cidades(3162948, '3 - Sudeste', 'MG', 'Sao Jose Da Barra', '0055'));
    l.add(new Cidades(
        3549508, '3 - Sudeste', 'SP', 'Sao Jose Da Bela Vista', '0055'));
    l.add(
        new Cidades(4125407, '4 - Sul', 'PR', 'Sao Jose Da Boa Vista', '0055'));
    l.add(new Cidades(
        2613404, '2 - Nordeste', 'PE', 'Sao Jose Da Coroa Grande', '0055'));
    l.add(new Cidades(
        2514206, '2 - Nordeste', 'PB', 'Sao Jose Da Lagoa Tapada', '0055'));
    l.add(
        new Cidades(2708303, '2 - Nordeste', 'AL', 'Sao Jose Da Laje', '0055'));
    l.add(
        new Cidades(3162955, '3 - Sudeste', 'MG', 'Sao Jose Da Lapa', '0055'));
    l.add(new Cidades(
        3163003, '3 - Sudeste', 'MG', 'Sao Jose Da Safira', '0055'));
    l.add(new Cidades(
        2708402, '2 - Nordeste', 'AL', 'Sao Jose Da Tapera', '0055'));
    l.add(new Cidades(
        3163102, '3 - Sudeste', 'MG', 'Sao Jose Da Varginha', '0055'));
    l.add(new Cidades(
        2929354, '2 - Nordeste', 'BA', 'Sao Jose Da Vitoria', '0055'));
    l.add(
        new Cidades(4318457, '4 - Sul', 'RS', 'Sao Jose Das Missoes', '0055'));
    l.add(new Cidades(
        4125456, '4 - Sul', 'PR', 'Sao Jose Das Palmeiras', '0055'));
    l.add(new Cidades(
        2514305, '2 - Nordeste', 'PB', 'Sao Jose De Caiana', '0055'));
    l.add(new Cidades(
        2514404, '2 - Nordeste', 'PB', 'Sao Jose De Espinharas', '0055'));
    l.add(new Cidades(
        2412203, '2 - Nordeste', 'RN', 'Sao Jose De Mipibu', '0055'));
    l.add(new Cidades(
        2514503, '2 - Nordeste', 'PB', 'Sao Jose De Piranhas', '0055'));
    l.add(new Cidades(
        2514552, '2 - Nordeste', 'PB', 'Sao Jose De Princesa', '0055'));
    l.add(new Cidades(
        2111201, '2 - Nordeste', 'MA', 'Sao Jose De Ribamar', '0055'));
    l.add(new Cidades(3305133, '3 - Sudeste', 'RJ', 'Sao Jose De Uba', '0055'));
    l.add(new Cidades(
        3163201, '3 - Sudeste', 'MG', 'Sao Jose Do Alegre', '0055'));
    l.add(new Cidades(
        3549607, '3 - Sudeste', 'SP', 'Sao Jose Do Barreiro', '0055'));
    l.add(new Cidades(
        2613503, '2 - Nordeste', 'PE', 'Sao Jose Do Belmonte', '0055'));
    l.add(new Cidades(
        2514602, '2 - Nordeste', 'PB', 'Sao Jose Do Bonfim', '0055'));
    l.add(new Cidades(
        2514651, '2 - Nordeste', 'PB', 'Sao Jose Do Brejo Do Cruz', '0055'));
    l.add(new Cidades(
        3204807, '3 - Sudeste', 'ES', 'Sao Jose Do Calcado', '0055'));
    l.add(new Cidades(
        2412302, '2 - Nordeste', 'RN', 'Sao Jose Do Campestre', '0055'));
    l.add(new Cidades(4216701, '4 - Sul', 'SC', 'Sao Jose Do Cedro', '0055'));
    l.add(new Cidades(4216800, '4 - Sul', 'SC', 'Sao Jose Do Cerrito', '0055'));
    l.add(new Cidades(
        2210052, '2 - Nordeste', 'PI', 'Sao Jose Do Divino', '0055'));
    l.add(new Cidades(
        3163300, '3 - Sudeste', 'MG', 'Sao Jose Do Divino', '0055'));
    l.add(new Cidades(
        2613602, '2 - Nordeste', 'PE', 'Sao Jose Do Egito', '0055'));
    l.add(new Cidades(
        3163409, '3 - Sudeste', 'MG', 'Sao Jose Do Goiabal', '0055'));
    l.add(new Cidades(4318465, '4 - Sul', 'RS', 'Sao Jose Do Herval', '0055'));
    l.add(
        new Cidades(4318481, '4 - Sul', 'RS', 'Sao Jose Do Hortencio', '0055'));
    l.add(
        new Cidades(4318499, '4 - Sul', 'RS', 'Sao Jose Do Inhacora', '0055'));
    l.add(new Cidades(
        2929370, '2 - Nordeste', 'BA', 'Sao Jose Do Jacuipe', '0055'));
    l.add(new Cidades(
        3163508, '3 - Sudeste', 'MG', 'Sao Jose Do Jacuri', '0055'));
    l.add(new Cidades(
        3163607, '3 - Sudeste', 'MG', 'Sao Jose Do Mantimento', '0055'));
    l.add(new Cidades(4318507, '4 - Sul', 'RS', 'Sao Jose Do Norte', '0055'));
    l.add(new Cidades(4318606, '4 - Sul', 'RS', 'Sao Jose Do Ouro', '0055'));
    l.add(new Cidades(
        2210102, '2 - Nordeste', 'PI', 'Sao Jose Do Peixe', '0055'));
    l.add(new Cidades(
        2210201, '2 - Nordeste', 'PI', 'Sao Jose Do Piaui', '0055'));
    l.add(new Cidades(
        5107297, '5 - Centro-Oeste', 'MT', 'Sao Jose Do Povo', '0055'));
    l.add(new Cidades(
        5107305, '5 - Centro-Oeste', 'MT', 'Sao Jose Do Rio Claro', '0055'));
    l.add(new Cidades(
        3549706, '3 - Sudeste', 'SP', 'Sao Jose Do Rio Pardo', '0055'));
    l.add(new Cidades(
        3549805, '3 - Sudeste', 'SP', 'Sao Jose Do Rio Preto', '0055'));
    l.add(new Cidades(
        2514701, '2 - Nordeste', 'PB', 'Sao Jose Do Sabugi', '0055'));
    l.add(new Cidades(
        2412401, '2 - Nordeste', 'RN', 'Sao Jose Do Serido', '0055'));
    l.add(new Cidades(4318614, '4 - Sul', 'RS', 'Sao Jose Do Sul', '0055'));
    l.add(new Cidades(
        3305158, '3 - Sudeste', 'RJ', 'Sao Jose Do Vale Do Rio Preto', '0055'));
    l.add(new Cidades(
        5107354, '5 - Centro-Oeste', 'MT', 'Sao Jose Do Xingu', '0055'));
    l.add(
        new Cidades(4318622, '4 - Sul', 'RS', 'Sao Jose Dos Ausentes', '0055'));
    l.add(new Cidades(
        2111250, '2 - Nordeste', 'MA', 'Sao Jose Dos Basilios', '0055'));
    l.add(new Cidades(
        3549904, '3 - Sudeste', 'SP', 'Sao Jose Dos Campos', '0055'));
    l.add(new Cidades(
        2514800, '2 - Nordeste', 'PB', 'Sao Jose Dos Cordeiros', '0055'));
    l.add(
        new Cidades(4125506, '4 - Sul', 'PR', 'Sao Jose Dos Pinhais', '0055'));
    l.add(new Cidades(5107107, '5 - Centro-Oeste', 'MT',
        'Sao Jose Dos Quatro Marcos', '0055'));
    l.add(new Cidades(
        2514453, '2 - Nordeste', 'PB', 'Sao Jose Dos Ramos', '0055'));
    l.add(new Cidades(2210300, '2 - Nordeste', 'PI', 'Sao Juliao', '0055'));
    l.add(new Cidades(4318705, '4 - Sul', 'RS', 'Sao Leopoldo', '0055'));
    l.add(new Cidades(3163706, '3 - Sudeste', 'MG', 'Sao Lourenco', '0055'));
    l.add(new Cidades(
        2613701, '2 - Nordeste', 'PE', 'Sao Lourenco Da Mata', '0055'));
    l.add(new Cidades(
        3549953, '3 - Sudeste', 'SP', 'Sao Lourenco Da Serra', '0055'));
    l.add(
        new Cidades(4216909, '4 - Sul', 'SC', 'Sao Lourenco Do Oeste', '0055'));
    l.add(new Cidades(
        2210359, '2 - Nordeste', 'PI', 'Sao Lourenco Do Piaui', '0055'));
    l.add(new Cidades(4318804, '4 - Sul', 'RS', 'Sao Lourenco Do Sul', '0055'));
    l.add(new Cidades(4217006, '4 - Sul', 'SC', 'Sao Ludgero', '0055'));
    l.add(new Cidades(2111300, '2 - Nordeste', 'MA', 'Sao Luis', '0055'));
    l.add(new Cidades(
        5220108, '5 - Centro-Oeste', 'GO', 'Sao Luis De Montes Belos', '0055'));
    l.add(
        new Cidades(2312601, '2 - Nordeste', 'CE', 'Sao Luis Do Curu', '0055'));
    l.add(new Cidades(
        3550001, '3 - Sudeste', 'SP', 'Sao Luis Do Paraitinga', '0055'));
    l.add(new Cidades(
        2210375, '2 - Nordeste', 'PI', 'Sao Luis Do Piaui', '0055'));
    l.add(new Cidades(
        2708501, '2 - Nordeste', 'AL', 'Sao Luis Do Quitunde', '0055'));
    l.add(new Cidades(
        2111409, '2 - Nordeste', 'MA', 'Sao Luis Gonzaga Do Maranhao', '0055'));
    l.add(new Cidades(1400605, '1 - Norte', 'RR', 'Sao Luiz', '0055'));
    l.add(new Cidades(
        5220157, '5 - Centro-Oeste', 'GO', 'Sao Luiz Do Norte', '0055'));
    l.add(new Cidades(4318903, '4 - Sul', 'RS', 'Sao Luiz Gonzaga', '0055'));
    l.add(new Cidades(2514909, '2 - Nordeste', 'PB', 'Sao Mamede', '0055'));
    l.add(
        new Cidades(4125555, '4 - Sul', 'PR', 'Sao Manoel Do Parana', '0055'));
    l.add(new Cidades(3550100, '3 - Sudeste', 'SP', 'Sao Manuel', '0055'));
    l.add(new Cidades(4319000, '4 - Sul', 'RS', 'Sao Marcos', '0055'));
    l.add(new Cidades(4217105, '4 - Sul', 'SC', 'Sao Martinho', '0055'));
    l.add(new Cidades(4319109, '4 - Sul', 'RS', 'Sao Martinho', '0055'));
    l.add(
        new Cidades(4319125, '4 - Sul', 'RS', 'Sao Martinho Da Serra', '0055'));
    l.add(new Cidades(3204906, '3 - Sudeste', 'ES', 'Sao Mateus', '0055'));
    l.add(new Cidades(
        2111508, '2 - Nordeste', 'MA', 'Sao Mateus Do Maranhao', '0055'));
    l.add(new Cidades(4125605, '4 - Sul', 'PR', 'Sao Mateus Do Sul', '0055'));
    l.add(new Cidades(2412500, '2 - Nordeste', 'RN', 'Sao Miguel', '0055'));
    l.add(new Cidades(
        3550209, '3 - Sudeste', 'SP', 'Sao Miguel Arcanjo', '0055'));
    l.add(new Cidades(
        2210383, '2 - Nordeste', 'PI', 'Sao Miguel Da Baixa Grande', '0055'));
    l.add(new Cidades(
        4217154, '4 - Sul', 'SC', 'Sao Miguel Da Boa Vista', '0055'));
    l.add(new Cidades(
        2929404, '2 - Nordeste', 'BA', 'Sao Miguel Das Matas', '0055'));
    l.add(new Cidades(
        4319158, '4 - Sul', 'RS', 'Sao Miguel Das Missoes', '0055'));
    l.add(new Cidades(
        2515005, '2 - Nordeste', 'PB', 'Sao Miguel De Taipu', '0055'));
    l.add(new Cidades(
        2807006, '2 - Nordeste', 'SE', 'Sao Miguel Do Aleixo', '0055'));
    l.add(new Cidades(
        3163805, '3 - Sudeste', 'MG', 'Sao Miguel Do Anta', '0055'));
    l.add(new Cidades(
        5220207, '5 - Centro-Oeste', 'GO', 'Sao Miguel Do Araguaia', '0055'));
    l.add(new Cidades(
        2210391, '2 - Nordeste', 'PI', 'Sao Miguel Do Fidalgo', '0055'));
    l.add(new Cidades(
        2412559, '2 - Nordeste', 'RN', 'Sao Miguel Do Gostoso', '0055'));
    l.add(
        new Cidades(1507607, '1 - Norte', 'PA', 'Sao Miguel Do Guama', '0055'));
    l.add(new Cidades(
        1100320, '1 - Norte', 'RO', 'Sao Miguel Do Guapore', '0055'));
    l.add(
        new Cidades(4125704, '4 - Sul', 'PR', 'Sao Miguel Do Iguacu', '0055'));
    l.add(new Cidades(4217204, '4 - Sul', 'SC', 'Sao Miguel Do Oeste', '0055'));
    l.add(new Cidades(5220264, '5 - Centro-Oeste', 'GO',
        'Sao Miguel Do Passa Quatro', '0055'));
    l.add(new Cidades(
        2210409, '2 - Nordeste', 'PI', 'Sao Miguel Do Tapuio', '0055'));
    l.add(new Cidades(1720200, '1 - Norte', 'TO', 'Sao Miguel Do To', '0055'));
    l.add(new Cidades(
        2708600, '2 - Nordeste', 'AL', 'Sao Miguel Dos Campos', '0055'));
    l.add(new Cidades(
        2708709, '2 - Nordeste', 'AL', 'Sao Miguel Dos Milagres', '0055'));
    l.add(new Cidades(4319208, '4 - Sul', 'RS', 'Sao Nicolau', '0055'));
    l.add(
        new Cidades(5220280, '5 - Centro-Oeste', 'GO', 'Sao Patricio', '0055'));
    l.add(new Cidades(3550308, '3 - Sudeste', 'SP', 'Sao Paulo', '0055'));
    l.add(
        new Cidades(4319307, '4 - Sul', 'RS', 'Sao Paulo Das Missoes', '0055'));
    l.add(new Cidades(
        1303908, '1 - Norte', 'AM', 'Sao Paulo De Olivenca', '0055'));
    l.add(new Cidades(
        2412609, '2 - Nordeste', 'RN', 'Sao Paulo Do Potengi', '0055'));
    l.add(new Cidades(2412708, '2 - Nordeste', 'RN', 'Sao Pedro', '0055'));
    l.add(new Cidades(3550407, '3 - Sudeste', 'SP', 'Sao Pedro', '0055'));
    l.add(new Cidades(
        2111532, '2 - Nordeste', 'MA', 'Sao Pedro Da Agua Branca', '0055'));
    l.add(new Cidades(
        3305208, '3 - Sudeste', 'RJ', 'Sao Pedro Da Aldeia', '0055'));
    l.add(new Cidades(
        5107404, '5 - Centro-Oeste', 'MT', 'Sao Pedro Da Cipa', '0055'));
    l.add(new Cidades(4319356, '4 - Sul', 'RS', 'Sao Pedro Da Serra', '0055'));
    l.add(new Cidades(
        3163904, '3 - Sudeste', 'MG', 'Sao Pedro Da Uniao', '0055'));
    l.add(
        new Cidades(4319364, '4 - Sul', 'RS', 'Sao Pedro Das Missoes', '0055'));
    l.add(new Cidades(
        4217253, '4 - Sul', 'SC', 'Sao Pedro De Alcantara', '0055'));
    l.add(new Cidades(4319372, '4 - Sul', 'RS', 'Sao Pedro Do Butia', '0055'));
    l.add(new Cidades(4125753, '4 - Sul', 'PR', 'Sao Pedro Do Iguacu', '0055'));
    l.add(new Cidades(4125803, '4 - Sul', 'PR', 'Sao Pedro Do Ivai', '0055'));
    l.add(new Cidades(4125902, '4 - Sul', 'PR', 'Sao Pedro Do Parana', '0055'));
    l.add(new Cidades(
        2210508, '2 - Nordeste', 'PI', 'Sao Pedro Do Piaui', '0055'));
    l.add(new Cidades(
        3164100, '3 - Sudeste', 'MG', 'Sao Pedro Do Suacui', '0055'));
    l.add(new Cidades(4319406, '4 - Sul', 'RS', 'Sao Pedro Do Sul', '0055'));
    l.add(new Cidades(
        3550506, '3 - Sudeste', 'SP', 'Sao Pedro Do Turvo', '0055'));
    l.add(new Cidades(
        2111573, '2 - Nordeste', 'MA', 'Sao Pedro Dos Crentes', '0055'));
    l.add(new Cidades(
        3164001, '3 - Sudeste', 'MG', 'Sao Pedro Dos Ferros', '0055'));
    l.add(new Cidades(2412807, '2 - Nordeste', 'RN', 'Sao Rafael', '0055'));
    l.add(new Cidades(
        2111607, '2 - Nordeste', 'MA', 'Sao Raimundo Das Mangabeiras', '0055'));
    l.add(new Cidades(
        2111631, '2 - Nordeste', 'MA', 'Sao Raimundo Do Doca Bezerra', '0055'));
    l.add(new Cidades(
        2210607, '2 - Nordeste', 'PI', 'Sao Raimundo Nonato', '0055'));
    l.add(new Cidades(2111672, '2 - Nordeste', 'MA', 'Sao Roberto', '0055'));
    l.add(new Cidades(3164209, '3 - Sudeste', 'MG', 'Sao Romao', '0055'));
    l.add(new Cidades(3550605, '3 - Sudeste', 'SP', 'Sao Roque', '0055'));
    l.add(new Cidades(
        3164308, '3 - Sudeste', 'MG', 'Sao Roque De Minas', '0055'));
    l.add(new Cidades(
        3204955, '3 - Sudeste', 'ES', 'Sao Roque Do Canaa', '0055'));
    l.add(
        new Cidades(1720259, '1 - Norte', 'TO', 'Sao Salvador Do To', '0055'));
    l.add(new Cidades(2708808, '2 - Nordeste', 'AL', 'Sao Sebastiao', '0055'));
    l.add(new Cidades(3550704, '3 - Sudeste', 'SP', 'Sao Sebastiao', '0055'));
    l.add(new Cidades(
        4126009, '4 - Sul', 'PR', 'Sao Sebastiao Da Amoreira', '0055'));
    l.add(new Cidades(
        3164407, '3 - Sudeste', 'MG', 'Sao Sebastiao Da Bela Vista', '0055'));
    l.add(new Cidades(
        1507706, '1 - Norte', 'PA', 'Sao Sebastiao Da Boa Vista', '0055'));
    l.add(new Cidades(
        3550803, '3 - Sudeste', 'SP', 'Sao Sebastiao Da Grama', '0055'));
    l.add(new Cidades(3164431, '3 - Sudeste', 'MG',
        'Sao Sebastiao Da Vargem Alegre', '0055'));
    l.add(new Cidades(2515104, '2 - Nordeste', 'PB',
        'Sao Sebastiao De Lagoa De Roca', '0055'));
    l.add(new Cidades(
        3305307, '3 - Sudeste', 'RJ', 'Sao Sebastiao Do Alto', '0055'));
    l.add(new Cidades(
        3164472, '3 - Sudeste', 'MG', 'Sao Sebastiao Do Anta', '0055'));
    l.add(
        new Cidades(4319505, '4 - Sul', 'RS', 'Sao Sebastiao Do Cai', '0055'));
    l.add(new Cidades(
        3164506, '3 - Sudeste', 'MG', 'Sao Sebastiao Do Maranhao', '0055'));
    l.add(new Cidades(
        3164605, '3 - Sudeste', 'MG', 'Sao Sebastiao Do Oeste', '0055'));
    l.add(new Cidades(
        3164704, '3 - Sudeste', 'MG', 'Sao Sebastiao Do Paraiso', '0055'));
    l.add(new Cidades(
        2929503, '2 - Nordeste', 'BA', 'Sao Sebastiao Do Passe', '0055'));
    l.add(new Cidades(
        3164803, '3 - Sudeste', 'MG', 'Sao Sebastiao Do Rio Preto', '0055'));
    l.add(new Cidades(
        3164902, '3 - Sudeste', 'MG', 'Sao Sebastiao Do Rio Verde', '0055'));
    l.add(
        new Cidades(1720309, '1 - Norte', 'TO', 'Sao Sebastiao Do To', '0055'));
    l.add(new Cidades(
        1303957, '1 - Norte', 'AM', 'Sao Sebastiao Do Uatuma', '0055'));
    l.add(new Cidades(
        2515203, '2 - Nordeste', 'PB', 'Sao Sebastiao Do Umbuzeiro', '0055'));
    l.add(new Cidades(4319604, '4 - Sul', 'RS', 'Sao Sepe', '0055'));
    l.add(new Cidades(3550902, '3 - Sudeste', 'SP', 'Sao Simao', '0055'));
    l.add(new Cidades(5220405, '5 - Centro-Oeste', 'GO', 'Sao Simao', '0055'));
    l.add(new Cidades(
        3165206, '3 - Sudeste', 'MG', 'Sao Thome Das Letras', '0055'));
    l.add(new Cidades(3165008, '3 - Sudeste', 'MG', 'Sao Tiago', '0055'));
    l.add(new Cidades(
        3165107, '3 - Sudeste', 'MG', 'Sao Tomas De Aquino', '0055'));
    l.add(new Cidades(2412906, '2 - Nordeste', 'RN', 'Sao Tome', '0055'));
    l.add(new Cidades(4126108, '4 - Sul', 'PR', 'Sao Tome', '0055'));
    l.add(new Cidades(4319703, '4 - Sul', 'RS', 'Sao Valentim', '0055'));
    l.add(new Cidades(4319711, '4 - Sul', 'RS', 'Sao Valentim Do Sul', '0055'));
    l.add(new Cidades(
        1720499, '1 - Norte', 'TO', 'Sao Valerio Da Natividade', '0055'));
    l.add(new Cidades(4319737, '4 - Sul', 'RS', 'Sao Valerio Do Sul', '0055'));
    l.add(new Cidades(4319752, '4 - Sul', 'RS', 'Sao Vendelino', '0055'));
    l.add(new Cidades(2413003, '2 - Nordeste', 'RN', 'Sao Vicente', '0055'));
    l.add(new Cidades(3551009, '3 - Sudeste', 'SP', 'Sao Vicente', '0055'));
    l.add(new Cidades(
        3165305, '3 - Sudeste', 'MG', 'Sao Vicente De Minas', '0055'));
    l.add(new Cidades(4319802, '4 - Sul', 'RS', 'Sao Vicente Do Sul', '0055'));
    l.add(new Cidades(
        2111706, '2 - Nordeste', 'MA', 'Sao Vicente Ferrer', '0055'));
    l.add(new Cidades(
        2613800, '2 - Nordeste', 'PE', 'Sao Vicente Ferrer', '0055'));
    l.add(new Cidades(2515302, '2 - Nordeste', 'PB', 'Sape', '0055'));
    l.add(new Cidades(2929602, '2 - Nordeste', 'BA', 'Sapeacu', '0055'));
    l.add(new Cidades(5107875, '5 - Centro-Oeste', 'MT', 'Sapezal', '0055'));
    l.add(new Cidades(4319901, '4 - Sul', 'RS', 'Sapiranga', '0055'));
    l.add(new Cidades(4126207, '4 - Sul', 'PR', 'Sapopema', '0055'));
    l.add(new Cidades(3165404, '3 - Sudeste', 'MG', 'Sapucai-Mirim', '0055'));
    l.add(new Cidades(1507755, '1 - Norte', 'PA', 'Sapucaia', '0055'));
    l.add(new Cidades(3305406, '3 - Sudeste', 'RJ', 'Sapucaia', '0055'));
    l.add(new Cidades(4320008, '4 - Sul', 'RS', 'Sapucaia Do Sul', '0055'));
    l.add(new Cidades(3305505, '3 - Sudeste', 'RJ', 'Saquarema', '0055'));
    l.add(new Cidades(4126256, '4 - Sul', 'PR', 'Sarandi', '0055'));
    l.add(new Cidades(4320107, '4 - Sul', 'RS', 'Sarandi', '0055'));
    l.add(new Cidades(3551108, '3 - Sudeste', 'SP', 'Sarapui', '0055'));
    l.add(new Cidades(3165503, '3 - Sudeste', 'MG', 'Sardoa', '0055'));
    l.add(new Cidades(3551207, '3 - Sudeste', 'SP', 'Sarutaia', '0055'));
    l.add(new Cidades(3165537, '3 - Sudeste', 'MG', 'Sarzedo', '0055'));
    l.add(new Cidades(2929701, '2 - Nordeste', 'BA', 'Satiro Dias', '0055'));
    l.add(new Cidades(2708907, '2 - Nordeste', 'AL', 'Satuba', '0055'));
    l.add(new Cidades(2111722, '2 - Nordeste', 'MA', 'Satubinha', '0055'));
    l.add(new Cidades(2929750, '2 - Nordeste', 'BA', 'Saubara', '0055'));
    l.add(new Cidades(4126272, '4 - Sul', 'PR', 'Saudade Do Iguacu', '0055'));
    l.add(new Cidades(4217303, '4 - Sul', 'SC', 'Saudades', '0055'));
    l.add(new Cidades(2929800, '2 - Nordeste', 'BA', 'Saude', '0055'));
    l.add(new Cidades(4217402, '4 - Sul', 'SC', 'Schroeder', '0055'));
    l.add(new Cidades(2929909, '2 - Nordeste', 'BA', 'Seabra', '0055'));
    l.add(new Cidades(4217501, '4 - Sul', 'SC', 'Seara', '0055'));
    l.add(new Cidades(
        3551306, '3 - Sudeste', 'SP', 'Sebastianopolis Do Sul', '0055'));
    l.add(
        new Cidades(2210623, '2 - Nordeste', 'PI', 'Sebastiao Barros', '0055'));
    l.add(new Cidades(
        2930006, '2 - Nordeste', 'BA', 'Sebastiao Laranjeiras', '0055'));
    l.add(new Cidades(2210631, '2 - Nordeste', 'PI', 'Sebastiao Leal', '0055'));
    l.add(new Cidades(4320206, '4 - Sul', 'RS', 'Seberi', '0055'));
    l.add(new Cidades(4320230, '4 - Sul', 'RS', 'Sede Nova', '0055'));
    l.add(new Cidades(4320263, '4 - Sul', 'RS', 'Segredo', '0055'));
    l.add(new Cidades(4320305, '4 - Sul', 'RS', 'Selbach', '0055'));
    l.add(new Cidades(5007802, '5 - Centro-Oeste', 'MS', 'Selviria', '0055'));
    l.add(new Cidades(3165560, '3 - Sudeste', 'MG', 'Sem-Peixe', '0055'));
    l.add(new Cidades(1200500, '1 - Norte', 'AC', 'Sena Madureira', '0055'));
    l.add(new Cidades(
        2111748, '2 - Nordeste', 'MA', 'Senador Alexandre Costa', '0055'));
    l.add(new Cidades(3165578, '3 - Sudeste', 'MG', 'Senador Amaral', '0055'));
    l.add(new Cidades(
        5220454, '5 - Centro-Oeste', 'GO', 'Senador Canedo', '0055'));
    l.add(new Cidades(3165602, '3 - Sudeste', 'MG', 'Senador Cortes', '0055'));
    l.add(new Cidades(
        2413102, '2 - Nordeste', 'RN', 'Senador Eloi De Souza', '0055'));
    l.add(new Cidades(3165701, '3 - Sudeste', 'MG', 'Senador Firmino', '0055'));
    l.add(new Cidades(
        2413201, '2 - Nordeste', 'RN', 'Senador Georgino Avelino', '0055'));
    l.add(new Cidades(1200450, '1 - Norte', 'AC', 'Senador Guiomard', '0055'));
    l.add(new Cidades(
        3165800, '3 - Sudeste', 'MG', 'Senador Jose Bento', '0055'));
    l.add(new Cidades(
        1507805, '1 - Norte', 'PA', 'Senador Jose Porfirio', '0055'));
    l.add(new Cidades(
        2111763, '2 - Nordeste', 'MA', 'Senador La Rocque', '0055'));
    l.add(new Cidades(
        3165909, '3 - Sudeste', 'MG', 'Senador Modestino Goncalves', '0055'));
    l.add(new Cidades(2312700, '2 - Nordeste', 'CE', 'Senador Pompeu', '0055'));
    l.add(new Cidades(
        2708956, '2 - Nordeste', 'AL', 'Senador Rui Palmeira', '0055'));
    l.add(new Cidades(2312809, '2 - Nordeste', 'CE', 'Senador Sa', '0055'));
    l.add(
        new Cidades(4320321, '4 - Sul', 'RS', 'Senador Salgado Filho', '0055'));
    l.add(new Cidades(4126306, '4 - Sul', 'PR', 'Senges', '0055'));
    l.add(
        new Cidades(2930105, '2 - Nordeste', 'BA', 'Senhor Do Bonfim', '0055'));
    l.add(new Cidades(
        3166006, '3 - Sudeste', 'MG', 'Senhora De Oliveira', '0055'));
    l.add(
        new Cidades(3166105, '3 - Sudeste', 'MG', 'Senhora Do Porto', '0055'));
    l.add(new Cidades(
        3166204, '3 - Sudeste', 'MG', 'Senhora Dos Remedios', '0055'));
    l.add(new Cidades(4320354, '4 - Sul', 'RS', 'Sentinela Do Sul', '0055'));
    l.add(new Cidades(2930204, '2 - Nordeste', 'BA', 'Sento Se', '0055'));
    l.add(new Cidades(4320404, '4 - Sul', 'RS', 'Serafina Correa', '0055'));
    l.add(new Cidades(3166303, '3 - Sudeste', 'MG', 'Sericita', '0055'));
    l.add(new Cidades(2515401, '2 - Nordeste', 'PB', 'Serido', '0055'));
    l.add(new Cidades(1101500, '1 - Norte', 'RO', 'Seringueiras', '0055'));
    l.add(new Cidades(4320453, '4 - Sul', 'RS', 'Serio', '0055'));
    l.add(new Cidades(3166402, '3 - Sudeste', 'MG', 'Seritinga', '0055'));
    l.add(new Cidades(3305554, '3 - Sudeste', 'RJ', 'Seropedica', '0055'));
    l.add(new Cidades(3205002, '3 - Sudeste', 'ES', 'Serra', '0055'));
    l.add(new Cidades(4217550, '4 - Sul', 'SC', 'Serra Alta', '0055'));
    l.add(new Cidades(3551405, '3 - Sudeste', 'SP', 'Serra Azul', '0055'));
    l.add(new Cidades(
        3166501, '3 - Sudeste', 'MG', 'Serra Azul De Minas', '0055'));
    l.add(new Cidades(2515500, '2 - Nordeste', 'PB', 'Serra Branca', '0055'));
    l.add(new Cidades(2515609, '2 - Nordeste', 'PB', 'Serra Da Raiz', '0055'));
    l.add(
        new Cidades(3166600, '3 - Sudeste', 'MG', 'Serra Da Saudade', '0055'));
    l.add(new Cidades(
        2413300, '2 - Nordeste', 'RN', 'Serra De Sao Bento', '0055'));
    l.add(new Cidades(2413359, '2 - Nordeste', 'RN', 'Serra Do Mel', '0055'));
    l.add(new Cidades(1600055, '1 - Norte', 'AP', 'Serra Do Navio', '0055'));
    l.add(
        new Cidades(2930154, '2 - Nordeste', 'BA', 'Serra Do Ramalho', '0055'));
    l.add(
        new Cidades(3166808, '3 - Sudeste', 'MG', 'Serra Do Salitre', '0055'));
    l.add(
        new Cidades(3166709, '3 - Sudeste', 'MG', 'Serra Dos Aimores', '0055'));
    l.add(new Cidades(2930303, '2 - Nordeste', 'BA', 'Serra Dourada', '0055'));
    l.add(new Cidades(2515708, '2 - Nordeste', 'PB', 'Serra Grande', '0055'));
    l.add(new Cidades(3551603, '3 - Sudeste', 'SP', 'Serra Negra', '0055'));
    l.add(new Cidades(
        2413409, '2 - Nordeste', 'RN', 'Serra Negra Do Norte', '0055'));
    l.add(new Cidades(
        5107883, '5 - Centro-Oeste', 'MT', 'Serra Nova Dourada', '0055'));
    l.add(new Cidades(2930402, '2 - Nordeste', 'BA', 'Serra Preta', '0055'));
    l.add(new Cidades(2515807, '2 - Nordeste', 'PB', 'Serra Redonda', '0055'));
    l.add(new Cidades(2613909, '2 - Nordeste', 'PE', 'Serra Talhada', '0055'));
    l.add(new Cidades(3551504, '3 - Sudeste', 'SP', 'Serrana', '0055'));
    l.add(new Cidades(3166907, '3 - Sudeste', 'MG', 'Serrania', '0055'));
    l.add(new Cidades(
        2111789, '2 - Nordeste', 'MA', 'Serrano Do Maranhao', '0055'));
    l.add(
        new Cidades(5220504, '5 - Centro-Oeste', 'GO', 'Serranopolis', '0055'));
    l.add(new Cidades(
        3166956, '3 - Sudeste', 'MG', 'Serranopolis De Minas', '0055'));
    l.add(new Cidades(
        4126355, '4 - Sul', 'PR', 'Serranopolis Do Iguacu', '0055'));
    l.add(new Cidades(3167004, '3 - Sudeste', 'MG', 'Serranos', '0055'));
    l.add(new Cidades(2515906, '2 - Nordeste', 'PB', 'Serraria', '0055'));
    l.add(new Cidades(2413508, '2 - Nordeste', 'RN', 'Serrinha', '0055'));
    l.add(new Cidades(2930501, '2 - Nordeste', 'BA', 'Serrinha', '0055'));
    l.add(new Cidades(
        2413557, '2 - Nordeste', 'RN', 'Serrinha Dos Pintos', '0055'));
    l.add(new Cidades(2614006, '2 - Nordeste', 'PE', 'Serrita', '0055'));
    l.add(new Cidades(3167103, '3 - Sudeste', 'MG', 'Serro', '0055'));
    l.add(new Cidades(2930600, '2 - Nordeste', 'BA', 'Serrolandia', '0055'));
    l.add(new Cidades(4126405, '4 - Sul', 'PR', 'Sertaneja', '0055'));
    l.add(new Cidades(2614105, '2 - Nordeste', 'PE', 'Sertania', '0055'));
    l.add(new Cidades(4126504, '4 - Sul', 'PR', 'Sertanopolis', '0055'));
    l.add(new Cidades(4320503, '4 - Sul', 'RS', 'Sertao', '0055'));
    l.add(new Cidades(4320552, '4 - Sul', 'RS', 'Sertao Santana', '0055'));
    l.add(new Cidades(2515930, '2 - Nordeste', 'PB', 'Sertaozinho', '0055'));
    l.add(new Cidades(3551702, '3 - Sudeste', 'SP', 'Sertaozinho', '0055'));
    l.add(new Cidades(3551801, '3 - Sudeste', 'SP', 'Sete Barras', '0055'));
    l.add(new Cidades(4320578, '4 - Sul', 'RS', 'Sete De Setembro', '0055'));
    l.add(new Cidades(3167202, '3 - Sudeste', 'MG', 'Sete Lagoas', '0055'));
    l.add(
        new Cidades(5007703, '5 - Centro-Oeste', 'MS', 'Sete Quedas', '0055'));
    l.add(new Cidades(3165552, '3 - Sudeste', 'MG', 'Setubinha', '0055'));
    l.add(
        new Cidades(4320602, '4 - Sul', 'RS', 'Severiano De Almeida', '0055'));
    l.add(new Cidades(2413607, '2 - Nordeste', 'RN', 'Severiano Melo', '0055'));
    l.add(new Cidades(3551900, '3 - Sudeste', 'SP', 'Severinia', '0055'));
    l.add(new Cidades(4217600, '4 - Sul', 'SC', 'Sideropolis', '0055'));
    l.add(
        new Cidades(5007901, '5 - Centro-Oeste', 'MS', 'Sidrolandia', '0055'));
    l.add(new Cidades(
        2210656, '2 - Nordeste', 'PI', 'Sigefredo Pacheco', '0055'));
    l.add(new Cidades(3305604, '3 - Sudeste', 'RJ', 'Silva Jardim', '0055'));
    l.add(new Cidades(5220603, '5 - Centro-Oeste', 'GO', 'Silvania', '0055'));
    l.add(new Cidades(1720655, '1 - Norte', 'TO', 'Silvanopolis', '0055'));
    l.add(new Cidades(4320651, '4 - Sul', 'RS', 'Silveira Martins', '0055'));
    l.add(new Cidades(3167301, '3 - Sudeste', 'MG', 'Silveirania', '0055'));
    l.add(new Cidades(3552007, '3 - Sudeste', 'SP', 'Silveiras', '0055'));
    l.add(new Cidades(1304005, '1 - Norte', 'AM', 'Silves', '0055'));
    l.add(new Cidades(3167400, '3 - Sudeste', 'MG', 'Silvianopolis', '0055'));
    l.add(new Cidades(2807105, '2 - Nordeste', 'SE', 'Simao Dias', '0055'));
    l.add(new Cidades(3167509, '3 - Sudeste', 'MG', 'Simao Pereira', '0055'));
    l.add(new Cidades(2210706, '2 - Nordeste', 'PI', 'Simoes', '0055'));
    l.add(new Cidades(2930709, '2 - Nordeste', 'BA', 'Simoes Filho', '0055'));
    l.add(new Cidades(5220686, '5 - Centro-Oeste', 'GO', 'Simolandia', '0055'));
    l.add(new Cidades(3167608, '3 - Sudeste', 'MG', 'Simonesia', '0055'));
    l.add(
        new Cidades(2210805, '2 - Nordeste', 'PI', 'Simplicio Mendes', '0055'));
    l.add(new Cidades(4320677, '4 - Sul', 'RS', 'Sinimbu', '0055'));
    l.add(new Cidades(5107909, '5 - Centro-Oeste', 'MT', 'Sinop', '0055'));
    l.add(new Cidades(4126603, '4 - Sul', 'PR', 'Siqueira Campos', '0055'));
    l.add(new Cidades(2614204, '2 - Nordeste', 'PE', 'Sirinhaem', '0055'));
    l.add(new Cidades(2807204, '2 - Nordeste', 'SE', 'Siriri', '0055'));
    l.add(new Cidades(
        5220702, '5 - Centro-Oeste', 'GO', 'Sitio D`Abadia', '0055'));
    l.add(new Cidades(2930758, '2 - Nordeste', 'BA', 'Sitio Do Mato', '0055'));
    l.add(
        new Cidades(2930766, '2 - Nordeste', 'BA', 'Sitio Do Quinto', '0055'));
    l.add(new Cidades(2111805, '2 - Nordeste', 'MA', 'Sitio Novo', '0055'));
    l.add(new Cidades(2413706, '2 - Nordeste', 'RN', 'Sitio Novo', '0055'));
    l.add(new Cidades(1720804, '1 - Norte', 'TO', 'Sitio Novo Do To', '0055'));
    l.add(new Cidades(2930774, '2 - Nordeste', 'BA', 'Sobradinho', '0055'));
    l.add(new Cidades(4320701, '4 - Sul', 'RS', 'Sobradinho', '0055'));
    l.add(new Cidades(2515971, '2 - Nordeste', 'PB', 'Sobrado', '0055'));
    l.add(new Cidades(2312908, '2 - Nordeste', 'CE', 'Sobral', '0055'));
    l.add(new Cidades(3167707, '3 - Sudeste', 'MG', 'Sobralia', '0055'));
    l.add(new Cidades(3552106, '3 - Sudeste', 'SP', 'Socorro', '0055'));
    l.add(
        new Cidades(2210904, '2 - Nordeste', 'PI', 'Socorro Do Piaui', '0055'));
    l.add(new Cidades(2516003, '2 - Nordeste', 'PB', 'Solanea', '0055'));
    l.add(new Cidades(2516102, '2 - Nordeste', 'PB', 'Soledade', '0055'));
    l.add(new Cidades(4320800, '4 - Sul', 'RS', 'Soledade', '0055'));
    l.add(
        new Cidades(3167806, '3 - Sudeste', 'MG', 'Soledade De Minas', '0055'));
    l.add(new Cidades(2614402, '2 - Nordeste', 'PE', 'Solidao', '0055'));
    l.add(new Cidades(2313005, '2 - Nordeste', 'CE', 'Solonopole', '0055'));
    l.add(new Cidades(4217709, '4 - Sul', 'SC', 'Sombrio', '0055'));
    l.add(new Cidades(5007935, '5 - Centro-Oeste', 'MS', 'Sonora', '0055'));
    l.add(new Cidades(3205010, '3 - Sudeste', 'ES', 'Sooretama', '0055'));
    l.add(new Cidades(3552205, '3 - Sudeste', 'SP', 'Sorocaba', '0055'));
    l.add(new Cidades(5107925, '5 - Centro-Oeste', 'MT', 'Sorriso', '0055'));
    l.add(new Cidades(2516151, '2 - Nordeste', 'PB', 'Sossego', '0055'));
    l.add(new Cidades(1507904, '1 - Norte', 'PA', 'Soure', '0055'));
    l.add(new Cidades(2516201, '2 - Nordeste', 'PB', 'Sousa', '0055'));
    l.add(new Cidades(2930808, '2 - Nordeste', 'BA', 'Souto Soares', '0055'));
    l.add(new Cidades(1720853, '1 - Norte', 'TO', 'Sucupira', '0055'));
    l.add(new Cidades(
        2111904, '2 - Nordeste', 'MA', 'Sucupira Do Norte', '0055'));
    l.add(new Cidades(
        2111953, '2 - Nordeste', 'MA', 'Sucupira Do Riachao', '0055'));
    l.add(new Cidades(3552304, '3 - Sudeste', 'SP', 'Sud Mennucci', '0055'));
    l.add(new Cidades(4217758, '4 - Sul', 'SC', 'Sul Brasil', '0055'));
    l.add(new Cidades(4126652, '4 - Sul', 'PR', 'Sulina', '0055'));
    l.add(new Cidades(3552403, '3 - Sudeste', 'SP', 'Sumare', '0055'));
    l.add(new Cidades(2516300, '2 - Nordeste', 'PB', 'Sume', '0055'));
    l.add(new Cidades(3305703, '3 - Sudeste', 'RJ', 'Sumidouro', '0055'));
    l.add(new Cidades(2614501, '2 - Nordeste', 'PE', 'Surubim', '0055'));
    l.add(new Cidades(2210938, '2 - Nordeste', 'PI', 'Sussuapara', '0055'));
    l.add(new Cidades(3552551, '3 - Sudeste', 'SP', 'Suzanapolis', '0055'));
    l.add(new Cidades(3552502, '3 - Sudeste', 'SP', 'Suzano', '0055'));
    l.add(new Cidades(4320859, '4 - Sul', 'RS', 'Tabai', '0055'));
    l.add(new Cidades(5107941, '5 - Centro-Oeste', 'MT', 'Tabapora', '0055'));
    l.add(new Cidades(3552601, '3 - Sudeste', 'SP', 'Tabapua', '0055'));
    l.add(new Cidades(1304062, '1 - Norte', 'AM', 'Tabatinga', '0055'));
    l.add(new Cidades(3552700, '3 - Sudeste', 'SP', 'Tabatinga', '0055'));
    l.add(new Cidades(2614600, '2 - Nordeste', 'PE', 'Tabira', '0055'));
    l.add(new Cidades(3552809, '3 - Sudeste', 'SP', 'Taboao Da Serra', '0055'));
    l.add(new Cidades(
        2930907, '2 - Nordeste', 'BA', 'Tabocas Do Brejo Velho', '0055'));
    l.add(
        new Cidades(2413805, '2 - Nordeste', 'RN', 'Taboleiro Grande', '0055'));
    l.add(new Cidades(3167905, '3 - Sudeste', 'MG', 'Tabuleiro', '0055'));
    l.add(new Cidades(
        2313104, '2 - Nordeste', 'CE', 'Tabuleiro Do Norte', '0055'));
    l.add(new Cidades(2614709, '2 - Nordeste', 'PE', 'Tacaimbo', '0055'));
    l.add(new Cidades(2614808, '2 - Nordeste', 'PE', 'Tacaratu', '0055'));
    l.add(new Cidades(3552908, '3 - Sudeste', 'SP', 'Taciba', '0055'));
    l.add(new Cidades(5007950, '5 - Centro-Oeste', 'MS', 'Tacuru', '0055'));
    l.add(new Cidades(3553005, '3 - Sudeste', 'SP', 'Taguai', '0055'));
    l.add(new Cidades(1720903, '1 - Norte', 'TO', 'Taguatinga', '0055'));
    l.add(new Cidades(3553104, '3 - Sudeste', 'SP', 'Taiacu', '0055'));
    l.add(new Cidades(1507953, '1 - Norte', 'PA', 'Tailandia', '0055'));
    l.add(new Cidades(4217808, '4 - Sul', 'SC', 'Taio', '0055'));
    l.add(new Cidades(3168002, '3 - Sudeste', 'MG', 'Taiobeiras', '0055'));
    l.add(new Cidades(1720937, '1 - Norte', 'TO', 'Taipas Do To', '0055'));
    l.add(new Cidades(2413904, '2 - Nordeste', 'RN', 'Taipu', '0055'));
    l.add(new Cidades(3553203, '3 - Sudeste', 'SP', 'Taiuva', '0055'));
    l.add(new Cidades(1720978, '1 - Norte', 'TO', 'Talisma', '0055'));
    l.add(new Cidades(2614857, '2 - Nordeste', 'PE', 'Tamandare', '0055'));
    l.add(new Cidades(4126678, '4 - Sul', 'PR', 'Tamarana', '0055'));
    l.add(new Cidades(3553302, '3 - Sudeste', 'SP', 'Tambau', '0055'));
    l.add(new Cidades(4126702, '4 - Sul', 'PR', 'Tamboara', '0055'));
    l.add(new Cidades(2313203, '2 - Nordeste', 'CE', 'Tamboril', '0055'));
    l.add(new Cidades(
        2210953, '2 - Nordeste', 'PI', 'Tamboril Do Piaui', '0055'));
    l.add(new Cidades(3553401, '3 - Sudeste', 'SP', 'Tanabi', '0055'));
    l.add(new Cidades(2414001, '2 - Nordeste', 'RN', 'Tangara', '0055'));
    l.add(new Cidades(4217907, '4 - Sul', 'SC', 'Tangara', '0055'));
    l.add(new Cidades(
        5107958, '5 - Centro-Oeste', 'MT', 'Tangara Da Serra', '0055'));
    l.add(new Cidades(3305752, '3 - Sudeste', 'RJ', 'Tangua', '0055'));
    l.add(new Cidades(2931004, '2 - Nordeste', 'BA', 'Tanhacu', '0055'));
    l.add(new Cidades(2709004, '2 - Nordeste', 'AL', 'Tanque D`Arca', '0055'));
    l.add(
        new Cidades(2210979, '2 - Nordeste', 'PI', 'Tanque Do Piaui', '0055'));
    l.add(new Cidades(2931053, '2 - Nordeste', 'BA', 'Tanque Novo', '0055'));
    l.add(new Cidades(2931103, '2 - Nordeste', 'BA', 'Tanquinho', '0055'));
    l.add(new Cidades(3168051, '3 - Sudeste', 'MG', 'Taparuba', '0055'));
    l.add(new Cidades(1304104, '1 - Norte', 'AM', 'Tapaua', '0055'));
    l.add(new Cidades(4126801, '4 - Sul', 'PR', 'Tapejara', '0055'));
    l.add(new Cidades(4320909, '4 - Sul', 'RS', 'Tapejara', '0055'));
    l.add(new Cidades(4321006, '4 - Sul', 'RS', 'Tapera', '0055'));
    l.add(new Cidades(2516508, '2 - Nordeste', 'PB', 'Taperoa', '0055'));
    l.add(new Cidades(2931202, '2 - Nordeste', 'BA', 'Taperoa', '0055'));
    l.add(new Cidades(4321105, '4 - Sul', 'RS', 'Tapes', '0055'));
    l.add(new Cidades(3168101, '3 - Sudeste', 'MG', 'Tapira', '0055'));
    l.add(new Cidades(4126900, '4 - Sul', 'PR', 'Tapira', '0055'));
    l.add(new Cidades(3168200, '3 - Sudeste', 'MG', 'Tapirai', '0055'));
    l.add(new Cidades(3553500, '3 - Sudeste', 'SP', 'Tapirai', '0055'));
    l.add(new Cidades(2931301, '2 - Nordeste', 'BA', 'Tapiramuta', '0055'));
    l.add(new Cidades(3553609, '3 - Sudeste', 'SP', 'Tapiratiba', '0055'));
    l.add(new Cidades(5108006, '5 - Centro-Oeste', 'MT', 'Tapurah', '0055'));
    l.add(new Cidades(4321204, '4 - Sul', 'RS', 'Taquara', '0055'));
    l.add(new Cidades(
        3168309, '3 - Sudeste', 'MG', 'Taquaracu De Minas', '0055'));
    l.add(new Cidades(3553658, '3 - Sudeste', 'SP', 'Taquaral', '0055'));
    l.add(new Cidades(
        5221007, '5 - Centro-Oeste', 'GO', 'Taquaral De Goias', '0055'));
    l.add(new Cidades(2709103, '2 - Nordeste', 'AL', 'Taquarana', '0055'));
    l.add(new Cidades(4321303, '4 - Sul', 'RS', 'Taquari', '0055'));
    l.add(new Cidades(3553708, '3 - Sudeste', 'SP', 'Taquaritinga', '0055'));
    l.add(new Cidades(
        2615003, '2 - Nordeste', 'PE', 'Taquaritinga Do Norte', '0055'));
    l.add(new Cidades(3553807, '3 - Sudeste', 'SP', 'Taquarituba', '0055'));
    l.add(new Cidades(3553856, '3 - Sudeste', 'SP', 'Taquarivai', '0055'));
    l.add(new Cidades(4321329, '4 - Sul', 'RS', 'Taquarucu Do Sul', '0055'));
    l.add(new Cidades(5007976, '5 - Centro-Oeste', 'MS', 'Taquarussu', '0055'));
    l.add(new Cidades(3553906, '3 - Sudeste', 'SP', 'Tarabai', '0055'));
    l.add(new Cidades(1200609, '1 - Norte', 'AC', 'Tarauaca', '0055'));
    l.add(new Cidades(2313252, '2 - Nordeste', 'CE', 'Tarrafas', '0055'));
    l.add(new Cidades(1600709, '1 - Norte', 'AP', 'Tartarugalzinho', '0055'));
    l.add(new Cidades(3553955, '3 - Sudeste', 'SP', 'Taruma', '0055'));
    l.add(new Cidades(3168408, '3 - Sudeste', 'MG', 'Tarumirim', '0055'));
    l.add(new Cidades(2112001, '2 - Nordeste', 'MA', 'Tasso Fragoso', '0055'));
    l.add(new Cidades(3554003, '3 - Sudeste', 'SP', 'Tatui', '0055'));
    l.add(new Cidades(2313302, '2 - Nordeste', 'CE', 'Taua', '0055'));
    l.add(new Cidades(3554102, '3 - Sudeste', 'SP', 'Taubate', '0055'));
    l.add(new Cidades(2516607, '2 - Nordeste', 'PB', 'Tavares', '0055'));
    l.add(new Cidades(4321352, '4 - Sul', 'RS', 'Tavares', '0055'));
    l.add(new Cidades(1304203, '1 - Norte', 'AM', 'Tefe', '0055'));
    l.add(new Cidades(2516706, '2 - Nordeste', 'PB', 'Teixeira', '0055'));
    l.add(new Cidades(
        2931350, '2 - Nordeste', 'BA', 'Teixeira De Freitas', '0055'));
    l.add(new Cidades(4127007, '4 - Sul', 'PR', 'Teixeira Soares', '0055'));
    l.add(new Cidades(3168507, '3 - Sudeste', 'MG', 'Teixeiras', '0055'));
    l.add(new Cidades(1101559, '1 - Norte', 'RO', 'Teixeiropolis', '0055'));
    l.add(new Cidades(2313351, '2 - Nordeste', 'CE', 'Tejucuoca', '0055'));
    l.add(new Cidades(3554201, '3 - Sudeste', 'SP', 'Tejupa', '0055'));
    l.add(new Cidades(4127106, '4 - Sul', 'PR', 'Telemaco Borba', '0055'));
    l.add(new Cidades(2807303, '2 - Nordeste', 'SE', 'Telha', '0055'));
    l.add(
        new Cidades(2414100, '2 - Nordeste', 'RN', 'Tenente Ananias', '0055'));
    l.add(new Cidades(
        2414159, '2 - Nordeste', 'RN', 'Tenente Laurentino Cruz', '0055'));
    l.add(new Cidades(4321402, '4 - Sul', 'RS', 'Tenente Portela', '0055'));
    l.add(new Cidades(2516755, '2 - Nordeste', 'PB', 'Tenorio', '0055'));
    l.add(
        new Cidades(2931400, '2 - Nordeste', 'BA', 'Teodoro Sampaio', '0055'));
    l.add(new Cidades(3554300, '3 - Sudeste', 'SP', 'Teodoro Sampaio', '0055'));
    l.add(new Cidades(2931509, '2 - Nordeste', 'BA', 'Teofilandia', '0055'));
    l.add(new Cidades(3168606, '3 - Sudeste', 'MG', 'Teofilo Otoni', '0055'));
    l.add(new Cidades(2931608, '2 - Nordeste', 'BA', 'Teolandia', '0055'));
    l.add(
        new Cidades(2709152, '2 - Nordeste', 'AL', 'Teotonio Vilela', '0055'));
    l.add(new Cidades(5008008, '5 - Centro-Oeste', 'MS', 'Terenos', '0055'));
    l.add(new Cidades(2211001, '2 - Nordeste', 'PI', 'Teresina', '0055'));
    l.add(new Cidades(
        5221080, '5 - Centro-Oeste', 'GO', 'Teresina De Goias', '0055'));
    l.add(new Cidades(3305802, '3 - Sudeste', 'RJ', 'Teresopolis', '0055'));
    l.add(new Cidades(2615102, '2 - Nordeste', 'PE', 'Terezinha', '0055'));
    l.add(new Cidades(
        5221197, '5 - Centro-Oeste', 'GO', 'Terezopolis De Goias', '0055'));
    l.add(new Cidades(1507961, '1 - Norte', 'PA', 'Terra Alta', '0055'));
    l.add(new Cidades(4127205, '4 - Sul', 'PR', 'Terra Boa', '0055'));
    l.add(new Cidades(4321436, '4 - Sul', 'RS', 'Terra De Areia', '0055'));
    l.add(new Cidades(2615201, '2 - Nordeste', 'PE', 'Terra Nova', '0055'));
    l.add(new Cidades(2931707, '2 - Nordeste', 'BA', 'Terra Nova', '0055'));
    l.add(new Cidades(
        5108055, '5 - Centro-Oeste', 'MT', 'Terra Nova Do Norte', '0055'));
    l.add(new Cidades(4127304, '4 - Sul', 'PR', 'Terra Rica', '0055'));
    l.add(new Cidades(3554409, '3 - Sudeste', 'SP', 'Terra Roxa', '0055'));
    l.add(new Cidades(4127403, '4 - Sul', 'PR', 'Terra Roxa', '0055'));
    l.add(new Cidades(1507979, '1 - Norte', 'PA', 'Terra Santa', '0055'));
    l.add(new Cidades(5108105, '5 - Centro-Oeste', 'MT', 'Tesouro', '0055'));
    l.add(new Cidades(4321451, '4 - Sul', 'RS', 'Teutonia', '0055'));
    l.add(new Cidades(1101609, '1 - Norte', 'RO', 'Theobroma', '0055'));
    l.add(new Cidades(2313401, '2 - Nordeste', 'CE', 'Tiangua', '0055'));
    l.add(new Cidades(4127502, '4 - Sul', 'PR', 'Tibagi', '0055'));
    l.add(new Cidades(2411056, '2 - Nordeste', 'RN', 'Tibau', '0055'));
    l.add(new Cidades(2414209, '2 - Nordeste', 'RN', 'Tibau Do Sul', '0055'));
    l.add(new Cidades(3554508, '3 - Sudeste', 'SP', 'Tiete', '0055'));
    l.add(new Cidades(4217956, '4 - Sul', 'SC', 'Tigrinhos', '0055'));
    l.add(new Cidades(4218004, '4 - Sul', 'SC', 'Tijucas', '0055'));
    l.add(new Cidades(4127601, '4 - Sul', 'PR', 'Tijucas Do Sul', '0055'));
    l.add(new Cidades(2615300, '2 - Nordeste', 'PE', 'Timbauba', '0055'));
    l.add(new Cidades(
        2414308, '2 - Nordeste', 'RN', 'Timbauba Dos Batistas', '0055'));
    l.add(new Cidades(4218103, '4 - Sul', 'SC', 'Timbe Do Sul', '0055'));
    l.add(new Cidades(2112100, '2 - Nordeste', 'MA', 'Timbiras', '0055'));
    l.add(new Cidades(4218202, '4 - Sul', 'SC', 'Timbo', '0055'));
    l.add(new Cidades(4218251, '4 - Sul', 'SC', 'Timbo Grande', '0055'));
    l.add(new Cidades(3554607, '3 - Sudeste', 'SP', 'Timburi', '0055'));
    l.add(new Cidades(2112209, '2 - Nordeste', 'MA', 'Timon', '0055'));
    l.add(new Cidades(3168705, '3 - Sudeste', 'MG', 'Timoteo', '0055'));
    l.add(new Cidades(4321469, '4 - Sul', 'RS', 'Tio Hugo', '0055'));
    l.add(new Cidades(3168804, '3 - Sudeste', 'MG', 'Tiradentes', '0055'));
    l.add(new Cidades(4321477, '4 - Sul', 'RS', 'Tiradentes Do Sul', '0055'));
    l.add(new Cidades(3168903, '3 - Sudeste', 'MG', 'Tiros', '0055'));
    l.add(new Cidades(2807402, '2 - Nordeste', 'SE', 'Tobias Barreto', '0055'));
    l.add(new Cidades(1721109, '1 - Norte', 'TO', 'Tocantinia', '0055'));
    l.add(new Cidades(1721208, '1 - Norte', 'TO', 'Tocantinopolis', '0055'));
    l.add(new Cidades(3169000, '3 - Sudeste', 'MG', 'To', '0055'));
    l.add(new Cidades(3169059, '3 - Sudeste', 'MG', 'Tocos Do Moji', '0055'));
    l.add(new Cidades(3169109, '3 - Sudeste', 'MG', 'Toledo', '0055'));
    l.add(new Cidades(4127700, '4 - Sul', 'PR', 'Toledo', '0055'));
    l.add(new Cidades(2807501, '2 - Nordeste', 'SE', 'Tomar Do Geru', '0055'));
    l.add(new Cidades(4127809, '4 - Sul', 'PR', 'Tomazina', '0055'));
    l.add(new Cidades(3169208, '3 - Sudeste', 'MG', 'Tombos', '0055'));
    l.add(new Cidades(1508001, '1 - Norte', 'PA', 'Tome-Acu', '0055'));
    l.add(new Cidades(1304237, '1 - Norte', 'AM', 'Tonantins', '0055'));
    l.add(new Cidades(2615409, '2 - Nordeste', 'PE', 'Toritama', '0055'));
    l.add(new Cidades(5108204, '5 - Centro-Oeste', 'MT', 'Torixoreu', '0055'));
    l.add(new Cidades(4321493, '4 - Sul', 'RS', 'Toropi', '0055'));
    l.add(new Cidades(3554656, '3 - Sudeste', 'SP', 'Torre De Pedra', '0055'));
    l.add(new Cidades(4321501, '4 - Sul', 'RS', 'Torres', '0055'));
    l.add(new Cidades(3554706, '3 - Sudeste', 'SP', 'Torrinha', '0055'));
    l.add(new Cidades(2414407, '2 - Nordeste', 'RN', 'Touros', '0055'));
    l.add(new Cidades(3554755, '3 - Sudeste', 'SP', 'Trabiju', '0055'));
    l.add(new Cidades(1508035, '1 - Norte', 'PA', 'Tracuateua', '0055'));
    l.add(new Cidades(2615508, '2 - Nordeste', 'PE', 'Tracunhaem', '0055'));
    l.add(new Cidades(2709202, '2 - Nordeste', 'AL', 'Traipu', '0055'));
    l.add(new Cidades(1508050, '1 - Norte', 'PA', 'Trairao', '0055'));
    l.add(new Cidades(2313500, '2 - Nordeste', 'CE', 'Trairi', '0055'));
    l.add(
        new Cidades(3305901, '3 - Sudeste', 'RJ', 'Trajano De Moraes', '0055'));
    l.add(new Cidades(4321600, '4 - Sul', 'RS', 'Tramandai', '0055'));
    l.add(new Cidades(4321626, '4 - Sul', 'RS', 'Travesseiro', '0055'));
    l.add(new Cidades(2931806, '2 - Nordeste', 'BA', 'Tremedal', '0055'));
    l.add(new Cidades(3554805, '3 - Sudeste', 'SP', 'Tremembe', '0055'));
    l.add(new Cidades(4321634, '4 - Sul', 'RS', 'Tres Arroios', '0055'));
    l.add(new Cidades(4218301, '4 - Sul', 'SC', 'Tres Barras', '0055'));
    l.add(
        new Cidades(4127858, '4 - Sul', 'PR', 'Tres Barras Do Parana', '0055'));
    l.add(new Cidades(4321667, '4 - Sul', 'RS', 'Tres Cachoeiras', '0055'));
    l.add(new Cidades(3169307, '3 - Sudeste', 'MG', 'Tres Coracoes', '0055'));
    l.add(new Cidades(4321709, '4 - Sul', 'RS', 'Tres Coroas', '0055'));
    l.add(new Cidades(4321808, '4 - Sul', 'RS', 'Tres De Maio', '0055'));
    l.add(new Cidades(4321832, '4 - Sul', 'RS', 'Tres Forquilhas', '0055'));
    l.add(new Cidades(3554904, '3 - Sudeste', 'SP', 'Tres Fronteiras', '0055'));
    l.add(
        new Cidades(5008305, '5 - Centro-Oeste', 'MS', 'Tres Lagoas', '0055'));
    l.add(new Cidades(3169356, '3 - Sudeste', 'MG', 'Tres Marias', '0055'));
    l.add(new Cidades(4321857, '4 - Sul', 'RS', 'Tres Palmeiras', '0055'));
    l.add(new Cidades(4321907, '4 - Sul', 'RS', 'Tres Passos', '0055'));
    l.add(new Cidades(3169406, '3 - Sudeste', 'MG', 'Tres Pontas', '0055'));
    l.add(
        new Cidades(5221304, '5 - Centro-Oeste', 'GO', 'Tres Ranchos', '0055'));
    l.add(new Cidades(3306008, '3 - Sudeste', 'RJ', 'Tres Rios', '0055'));
    l.add(new Cidades(4218350, '4 - Sul', 'SC', 'Treviso', '0055'));
    l.add(new Cidades(4218400, '4 - Sul', 'SC', 'Treze De Maio', '0055'));
    l.add(new Cidades(4218509, '4 - Sul', 'SC', 'Treze Tilias', '0055'));
    l.add(new Cidades(2615607, '2 - Nordeste', 'PE', 'Trindade', '0055'));
    l.add(new Cidades(5221403, '5 - Centro-Oeste', 'GO', 'Trindade', '0055'));
    l.add(new Cidades(4321956, '4 - Sul', 'RS', 'Trindade Do Sul', '0055'));
    l.add(new Cidades(2516805, '2 - Nordeste', 'PB', 'Triunfo', '0055'));
    l.add(new Cidades(2615706, '2 - Nordeste', 'PE', 'Triunfo', '0055'));
    l.add(new Cidades(4322004, '4 - Sul', 'RS', 'Triunfo', '0055'));
    l.add(
        new Cidades(2414456, '2 - Nordeste', 'RN', 'Triunfo Potiguar', '0055'));
    l.add(new Cidades(
        2112233, '2 - Nordeste', 'MA', 'Trizidela Do Vale', '0055'));
    l.add(new Cidades(5221452, '5 - Centro-Oeste', 'GO', 'Trombas', '0055'));
    l.add(new Cidades(4218608, '4 - Sul', 'SC', 'Trombudo Central', '0055'));
    l.add(new Cidades(4218707, '4 - Sul', 'SC', 'Tubarao', '0055'));
    l.add(new Cidades(2931905, '2 - Nordeste', 'BA', 'Tucano', '0055'));
    l.add(new Cidades(1508084, '1 - Norte', 'PA', 'Tucuma', '0055'));
    l.add(new Cidades(4322103, '4 - Sul', 'RS', 'Tucunduva', '0055'));
    l.add(new Cidades(1508100, '1 - Norte', 'PA', 'Tucurui', '0055'));
    l.add(new Cidades(2112274, '2 - Nordeste', 'MA', 'Tufilandia', '0055'));
    l.add(new Cidades(3554953, '3 - Sudeste', 'SP', 'Tuiuti', '0055'));
    l.add(new Cidades(3169505, '3 - Sudeste', 'MG', 'Tumiritinga', '0055'));
    l.add(new Cidades(4218756, '4 - Sul', 'SC', 'Tunapolis', '0055'));
    l.add(new Cidades(4322152, '4 - Sul', 'RS', 'Tunas', '0055'));
    l.add(new Cidades(4127882, '4 - Sul', 'PR', 'Tunas Do Parana', '0055'));
    l.add(new Cidades(4127908, '4 - Sul', 'PR', 'Tuneiras Do Oeste', '0055'));
    l.add(new Cidades(2112308, '2 - Nordeste', 'MA', 'Tuntum', '0055'));
    l.add(new Cidades(3555000, '3 - Sudeste', 'SP', 'Tupa', '0055'));
    l.add(new Cidades(3169604, '3 - Sudeste', 'MG', 'Tupaciguara', '0055'));
    l.add(new Cidades(2615805, '2 - Nordeste', 'PE', 'Tupanatinga', '0055'));
    l.add(new Cidades(4322186, '4 - Sul', 'RS', 'Tupanci Do Sul', '0055'));
    l.add(new Cidades(4322202, '4 - Sul', 'RS', 'Tupancireta', '0055'));
    l.add(new Cidades(4322251, '4 - Sul', 'RS', 'Tupandi', '0055'));
    l.add(new Cidades(4322301, '4 - Sul', 'RS', 'Tuparendi', '0055'));
    l.add(new Cidades(2615904, '2 - Nordeste', 'PE', 'Tuparetama', '0055'));
    l.add(new Cidades(4127957, '4 - Sul', 'PR', 'Tupassi', '0055'));
    l.add(new Cidades(3555109, '3 - Sudeste', 'SP', 'Tupi Paulista', '0055'));
    l.add(new Cidades(1721257, '1 - Norte', 'TO', 'Tupirama', '0055'));
    l.add(new Cidades(1721307, '1 - Norte', 'TO', 'Tupiratins', '0055'));
    l.add(new Cidades(2112407, '2 - Nordeste', 'MA', 'Turiacu', '0055'));
    l.add(new Cidades(2112456, '2 - Nordeste', 'MA', 'Turilandia', '0055'));
    l.add(new Cidades(3555208, '3 - Sudeste', 'SP', 'Turiuba', '0055'));
    l.add(new Cidades(3169703, '3 - Sudeste', 'MG', 'Turmalina', '0055'));
    l.add(new Cidades(3555307, '3 - Sudeste', 'SP', 'Turmalina', '0055'));
    l.add(new Cidades(4322327, '4 - Sul', 'RS', 'Turucu', '0055'));
    l.add(new Cidades(2313559, '2 - Nordeste', 'CE', 'Tururu', '0055'));
    l.add(new Cidades(5221502, '5 - Centro-Oeste', 'GO', 'Turvania', '0055'));
    l.add(
        new Cidades(5221551, '5 - Centro-Oeste', 'GO', 'Turvelandia', '0055'));
    l.add(new Cidades(4127965, '4 - Sul', 'PR', 'Turvo', '0055'));
    l.add(new Cidades(4218806, '4 - Sul', 'SC', 'Turvo', '0055'));
    l.add(new Cidades(3169802, '3 - Sudeste', 'MG', 'Turvolandia', '0055'));
    l.add(new Cidades(2112506, '2 - Nordeste', 'MA', 'Tutoia', '0055'));
    l.add(new Cidades(1304260, '1 - Norte', 'AM', 'Uarini', '0055'));
    l.add(new Cidades(2932002, '2 - Nordeste', 'BA', 'Uaua', '0055'));
    l.add(new Cidades(3169901, '3 - Sudeste', 'MG', 'Uba', '0055'));
    l.add(new Cidades(3170008, '3 - Sudeste', 'MG', 'Ubai', '0055'));
    l.add(new Cidades(2932101, '2 - Nordeste', 'BA', 'Ubaira', '0055'));
    l.add(new Cidades(2932200, '2 - Nordeste', 'BA', 'Ubaitaba', '0055'));
    l.add(new Cidades(2313609, '2 - Nordeste', 'CE', 'Ubajara', '0055'));
    l.add(new Cidades(3170057, '3 - Sudeste', 'MG', 'Ubaporanga', '0055'));
    l.add(new Cidades(3555356, '3 - Sudeste', 'SP', 'Ubarana', '0055'));
    l.add(new Cidades(2932309, '2 - Nordeste', 'BA', 'Ubata', '0055'));
    l.add(new Cidades(3555406, '3 - Sudeste', 'SP', 'Ubatuba', '0055'));
    l.add(new Cidades(3170107, '3 - Sudeste', 'MG', 'Uberaba', '0055'));
    l.add(new Cidades(3170206, '3 - Sudeste', 'MG', 'Uberlandia', '0055'));
    l.add(new Cidades(3555505, '3 - Sudeste', 'SP', 'Ubirajara', '0055'));
    l.add(new Cidades(4128005, '4 - Sul', 'PR', 'Ubirata', '0055'));
    l.add(new Cidades(4322343, '4 - Sul', 'RS', 'Ubiretama', '0055'));
    l.add(new Cidades(3555604, '3 - Sudeste', 'SP', 'Uchoa', '0055'));
    l.add(new Cidades(2932408, '2 - Nordeste', 'BA', 'Uibai', '0055'));
    l.add(new Cidades(1400704, '1 - Norte', 'RR', 'Uiramuta', '0055'));
    l.add(new Cidades(5221577, '5 - Centro-Oeste', 'GO', 'Uirapuru', '0055'));
    l.add(new Cidades(2516904, '2 - Nordeste', 'PB', 'Uirauna', '0055'));
    l.add(new Cidades(1508126, '1 - Norte', 'PA', 'Ulianopolis', '0055'));
    l.add(new Cidades(2313708, '2 - Nordeste', 'CE', 'Umari', '0055'));
    l.add(new Cidades(2414506, '2 - Nordeste', 'RN', 'Umarizal', '0055'));
    l.add(new Cidades(2807600, '2 - Nordeste', 'SE', 'Umbauba', '0055'));
    l.add(new Cidades(2932457, '2 - Nordeste', 'BA', 'Umburanas', '0055'));
    l.add(new Cidades(3170305, '3 - Sudeste', 'MG', 'Umburatiba', '0055'));
    l.add(new Cidades(2517001, '2 - Nordeste', 'PB', 'Umbuzeiro', '0055'));
    l.add(new Cidades(2313757, '2 - Nordeste', 'CE', 'Umirim', '0055'));
    l.add(new Cidades(4128104, '4 - Sul', 'PR', 'Umuarama', '0055'));
    l.add(new Cidades(2932507, '2 - Nordeste', 'BA', 'Una', '0055'));
    l.add(new Cidades(3170404, '3 - Sudeste', 'MG', 'Unai', '0055'));
    l.add(new Cidades(2211100, '2 - Nordeste', 'PI', 'Uniao', '0055'));
    l.add(new Cidades(4322350, '4 - Sul', 'RS', 'Uniao Da Serra', '0055'));
    l.add(new Cidades(4128203, '4 - Sul', 'PR', 'Uniao Da Vitoria', '0055'));
    l.add(new Cidades(3170438, '3 - Sudeste', 'MG', 'Uniao De Minas', '0055'));
    l.add(new Cidades(4218855, '4 - Sul', 'SC', 'Uniao Do Oeste', '0055'));
    l.add(
        new Cidades(5108303, '5 - Centro-Oeste', 'MT', 'Uniao Do Sul', '0055'));
    l.add(new Cidades(
        2709301, '2 - Nordeste', 'AL', 'Uniao Dos Palmares', '0055'));
    l.add(new Cidades(3555703, '3 - Sudeste', 'SP', 'Uniao Paulista', '0055'));
    l.add(new Cidades(4128302, '4 - Sul', 'PR', 'Uniflor', '0055'));
    l.add(new Cidades(4322376, '4 - Sul', 'RS', 'Unistalda', '0055'));
    l.add(new Cidades(2414605, '2 - Nordeste', 'RN', 'Upanema', '0055'));
    l.add(new Cidades(4128401, '4 - Sul', 'PR', 'Urai', '0055'));
    l.add(new Cidades(2932606, '2 - Nordeste', 'BA', 'Urandi', '0055'));
    l.add(new Cidades(3555802, '3 - Sudeste', 'SP', 'Urania', '0055'));
    l.add(new Cidades(2112605, '2 - Nordeste', 'MA', 'Urbano Santos', '0055'));
    l.add(new Cidades(3555901, '3 - Sudeste', 'SP', 'Uru', '0055'));
    l.add(new Cidades(5221601, '5 - Centro-Oeste', 'GO', 'Uruacu', '0055'));
    l.add(new Cidades(5221700, '5 - Centro-Oeste', 'GO', 'Uruana', '0055'));
    l.add(new Cidades(3170479, '3 - Sudeste', 'MG', 'Uruana De Minas', '0055'));
    l.add(new Cidades(1508159, '1 - Norte', 'PA', 'Uruara', '0055'));
    l.add(new Cidades(4218905, '4 - Sul', 'SC', 'Urubici', '0055'));
    l.add(new Cidades(2313807, '2 - Nordeste', 'CE', 'Uruburetama', '0055'));
    l.add(new Cidades(3170503, '3 - Sudeste', 'MG', 'Urucania', '0055'));
    l.add(new Cidades(1304302, '1 - Norte', 'AM', 'Urucara', '0055'));
    l.add(new Cidades(2932705, '2 - Nordeste', 'BA', 'Urucuca', '0055'));
    l.add(new Cidades(2211209, '2 - Nordeste', 'PI', 'Urucui', '0055'));
    l.add(new Cidades(3170529, '3 - Sudeste', 'MG', 'Urucuia', '0055'));
    l.add(new Cidades(1304401, '1 - Norte', 'AM', 'Urucurituba', '0055'));
    l.add(new Cidades(4322400, '4 - Sul', 'RS', 'Uruguaiana', '0055'));
    l.add(new Cidades(2313906, '2 - Nordeste', 'CE', 'Uruoca', '0055'));
    l.add(new Cidades(1101708, '1 - Norte', 'RO', 'Urupa', '0055'));
    l.add(new Cidades(4218954, '4 - Sul', 'SC', 'Urupema', '0055'));
    l.add(new Cidades(3556008, '3 - Sudeste', 'SP', 'Urupes', '0055'));
    l.add(new Cidades(4219002, '4 - Sul', 'SC', 'Urussanga', '0055'));
    l.add(new Cidades(5221809, '5 - Centro-Oeste', 'GO', 'Urutai', '0055'));
    l.add(new Cidades(2932804, '2 - Nordeste', 'BA', 'Utinga', '0055'));
    l.add(new Cidades(4322509, '4 - Sul', 'RS', 'Vacaria', '0055'));
    l.add(new Cidades(
        5108352, '5 - Centro-Oeste', 'MT', 'Vale De Sao Domingos', '0055'));
    l.add(new Cidades(1101757, '1 - Norte', 'RO', 'Vale Do Anari', '0055'));
    l.add(new Cidades(1101807, '1 - Norte', 'RO', 'Vale Do Paraiso', '0055'));
    l.add(new Cidades(4322533, '4 - Sul', 'RS', 'Vale Do Sol', '0055'));
    l.add(new Cidades(4322541, '4 - Sul', 'RS', 'Vale Real', '0055'));
    l.add(new Cidades(4322525, '4 - Sul', 'RS', 'Vale Verde', '0055'));
    l.add(new Cidades(2932903, '2 - Nordeste', 'BA', 'Valenca', '0055'));
    l.add(new Cidades(3306107, '3 - Sudeste', 'RJ', 'Valenca', '0055'));
    l.add(
        new Cidades(2211308, '2 - Nordeste', 'PI', 'Valenca Do Piaui', '0055'));
    l.add(new Cidades(2933000, '2 - Nordeste', 'BA', 'Valente', '0055'));
    l.add(new Cidades(3556107, '3 - Sudeste', 'SP', 'Valentim Gentil', '0055'));
    l.add(new Cidades(3556206, '3 - Sudeste', 'SP', 'Valinhos', '0055'));
    l.add(new Cidades(3556305, '3 - Sudeste', 'SP', 'Valparaiso', '0055'));
    l.add(new Cidades(
        5221858, '5 - Centro-Oeste', 'GO', 'Valparaiso De Goias', '0055'));
    l.add(new Cidades(4322558, '4 - Sul', 'RS', 'Vanini', '0055'));
    l.add(new Cidades(4219101, '4 - Sul', 'SC', 'Vargeao', '0055'));
    l.add(new Cidades(3556354, '3 - Sudeste', 'SP', 'Vargem', '0055'));
    l.add(new Cidades(4219150, '4 - Sul', 'SC', 'Vargem', '0055'));
    l.add(new Cidades(3170578, '3 - Sudeste', 'MG', 'Vargem Alegre', '0055'));
    l.add(new Cidades(3205036, '3 - Sudeste', 'ES', 'Vargem Alta', '0055'));
    l.add(new Cidades(3170602, '3 - Sudeste', 'MG', 'Vargem Bonita', '0055'));
    l.add(new Cidades(4219176, '4 - Sul', 'SC', 'Vargem Bonita', '0055'));
    l.add(new Cidades(2112704, '2 - Nordeste', 'MA', 'Vargem Grande', '0055'));
    l.add(new Cidades(
        3170651, '3 - Sudeste', 'MG', 'Vargem Grande Do Rio Pardo', '0055'));
    l.add(new Cidades(
        3556404, '3 - Sudeste', 'SP', 'Vargem Grande Do Sul', '0055'));
    l.add(new Cidades(
        3556453, '3 - Sudeste', 'SP', 'Vargem Grande Paulista', '0055'));
    l.add(new Cidades(3170701, '3 - Sudeste', 'MG', 'Varginha', '0055'));
    l.add(new Cidades(5221908, '5 - Centro-Oeste', 'GO', 'Varjao', '0055'));
    l.add(new Cidades(3170750, '3 - Sudeste', 'MG', 'Varjao De Minas', '0055'));
    l.add(new Cidades(2313955, '2 - Nordeste', 'CE', 'Varjota', '0055'));
    l.add(new Cidades(3306156, '3 - Sudeste', 'RJ', 'Varre-Sai', '0055'));
    l.add(new Cidades(2414704, '2 - Nordeste', 'RN', 'Varzea', '0055'));
    l.add(new Cidades(2517100, '2 - Nordeste', 'PB', 'Varzea', '0055'));
    l.add(new Cidades(2314003, '2 - Nordeste', 'CE', 'Varzea Alegre', '0055'));
    l.add(new Cidades(2211357, '2 - Nordeste', 'PI', 'Varzea Branca', '0055'));
    l.add(new Cidades(3170800, '3 - Sudeste', 'MG', 'Varzea Da Palma', '0055'));
    l.add(new Cidades(2933059, '2 - Nordeste', 'BA', 'Varzea Da Roca', '0055'));
    l.add(new Cidades(2933109, '2 - Nordeste', 'BA', 'Varzea Do Poco', '0055'));
    l.add(new Cidades(2211407, '2 - Nordeste', 'PI', 'Varzea Grande', '0055'));
    l.add(new Cidades(
        5108402, '5 - Centro-Oeste', 'MT', 'Varzea Grande', '0055'));
    l.add(new Cidades(2933158, '2 - Nordeste', 'BA', 'Varzea Nova', '0055'));
    l.add(new Cidades(3556503, '3 - Sudeste', 'SP', 'Varzea Paulista', '0055'));
    l.add(new Cidades(2933174, '2 - Nordeste', 'BA', 'Varzedo', '0055'));
    l.add(new Cidades(3170909, '3 - Sudeste', 'MG', 'Varzelandia', '0055'));
    l.add(new Cidades(3306206, '3 - Sudeste', 'RJ', 'Vassouras', '0055'));
    l.add(new Cidades(3171006, '3 - Sudeste', 'MG', 'Vazante', '0055'));
    l.add(new Cidades(4322608, '4 - Sul', 'RS', 'Venancio Aires', '0055'));
    l.add(new Cidades(
        3205069, '3 - Sudeste', 'ES', 'Venda Nova Do Imigrante', '0055'));
    l.add(new Cidades(2414753, '2 - Nordeste', 'RN', 'Venha-Ver', '0055'));
    l.add(new Cidades(4128534, '4 - Sul', 'PR', 'Ventania', '0055'));
    l.add(new Cidades(2616001, '2 - Nordeste', 'PE', 'Venturosa', '0055'));
    l.add(new Cidades(5108501, '5 - Centro-Oeste', 'MT', 'Vera', '0055'));
    l.add(new Cidades(2414803, '2 - Nordeste', 'RN', 'Vera Cruz', '0055'));
    l.add(new Cidades(2933208, '2 - Nordeste', 'BA', 'Vera Cruz', '0055'));
    l.add(new Cidades(3556602, '3 - Sudeste', 'SP', 'Vera Cruz', '0055'));
    l.add(new Cidades(4322707, '4 - Sul', 'RS', 'Vera Cruz', '0055'));
    l.add(new Cidades(4128559, '4 - Sul', 'PR', 'Vera Cruz Do Oeste', '0055'));
    l.add(new Cidades(2211506, '2 - Nordeste', 'PI', 'Vera Mendes', '0055'));
    l.add(new Cidades(4322806, '4 - Sul', 'RS', 'Veranopolis', '0055'));
    l.add(new Cidades(2616100, '2 - Nordeste', 'PE', 'Verdejante', '0055'));
    l.add(new Cidades(3171030, '3 - Sudeste', 'MG', 'Verdelandia', '0055'));
    l.add(new Cidades(4128609, '4 - Sul', 'PR', 'Vere', '0055'));
    l.add(new Cidades(2933257, '2 - Nordeste', 'BA', 'Vereda', '0055'));
    l.add(new Cidades(3171071, '3 - Sudeste', 'MG', 'Veredinha', '0055'));
    l.add(new Cidades(3171105, '3 - Sudeste', 'MG', 'Verissimo', '0055'));
    l.add(new Cidades(3171154, '3 - Sudeste', 'MG', 'Vermelho Novo', '0055'));
    l.add(new Cidades(
        2616183, '2 - Nordeste', 'PE', 'Vertente Do Lerio', '0055'));
    l.add(new Cidades(2616209, '2 - Nordeste', 'PE', 'Vertentes', '0055'));
    l.add(new Cidades(3171204, '3 - Sudeste', 'MG', 'Vespasiano', '0055'));
    l.add(new Cidades(4322855, '4 - Sul', 'RS', 'Vespasiano Correa', '0055'));
    l.add(new Cidades(4322905, '4 - Sul', 'RS', 'Viadutos', '0055'));
    l.add(new Cidades(4323002, '4 - Sul', 'RS', 'Viamao', '0055'));
    l.add(new Cidades(2112803, '2 - Nordeste', 'MA', 'Viana', '0055'));
    l.add(new Cidades(3205101, '3 - Sudeste', 'ES', 'Viana', '0055'));
    l.add(new Cidades(5222005, '5 - Centro-Oeste', 'GO', 'Vianopolis', '0055'));
    l.add(new Cidades(2616308, '2 - Nordeste', 'PE', 'Vicencia', '0055'));
    l.add(new Cidades(4323101, '4 - Sul', 'RS', 'Vicente Dutra', '0055'));
    l.add(new Cidades(5008404, '5 - Centro-Oeste', 'MS', 'Vicentina', '0055'));
    l.add(new Cidades(
        5222054, '5 - Centro-Oeste', 'GO', 'Vicentinopolis', '0055'));
    l.add(new Cidades(2414902, '2 - Nordeste', 'RN', 'Vicosa', '0055'));
    l.add(new Cidades(2709400, '2 - Nordeste', 'AL', 'Vicosa', '0055'));
    l.add(new Cidades(3171303, '3 - Sudeste', 'MG', 'Vicosa', '0055'));
    l.add(
        new Cidades(2314102, '2 - Nordeste', 'CE', 'Vicosa Do Ceara', '0055'));
    l.add(new Cidades(4323200, '4 - Sul', 'RS', 'Victor Graeff', '0055'));
    l.add(new Cidades(4219200, '4 - Sul', 'SC', 'Vidal Ramos', '0055'));
    l.add(new Cidades(4219309, '4 - Sul', 'SC', 'Videira', '0055'));
    l.add(new Cidades(3171402, '3 - Sudeste', 'MG', 'Vieiras', '0055'));
    l.add(new Cidades(2517209, '2 - Nordeste', 'PB', 'Vieiropolis', '0055'));
    l.add(new Cidades(1508209, '1 - Norte', 'PA', 'Vigia', '0055'));
    l.add(new Cidades(5105507, '5 - Centro-Oeste', 'MT',
        'Vila Bela Da Santissima Trindade', '0055'));
    l.add(new Cidades(5222203, '5 - Centro-Oeste', 'GO', 'Vila Boa', '0055'));
    l.add(new Cidades(2415008, '2 - Nordeste', 'RN', 'Vila Flor', '0055'));
    l.add(new Cidades(4323309, '4 - Sul', 'RS', 'Vila Flores', '0055'));
    l.add(new Cidades(4323358, '4 - Sul', 'RS', 'Vila Langaro', '0055'));
    l.add(new Cidades(4323408, '4 - Sul', 'RS', 'Vila Maria', '0055'));
    l.add(new Cidades(
        2211605, '2 - Nordeste', 'PI', 'Vila Nova Do Piaui', '0055'));
    l.add(new Cidades(4323457, '4 - Sul', 'RS', 'Vila Nova Do Sul', '0055'));
    l.add(new Cidades(
        2112852, '2 - Nordeste', 'MA', 'Vila Nova Dos Martirios', '0055'));
    l.add(new Cidades(3205150, '3 - Sudeste', 'ES', 'Vila Pavao', '0055'));
    l.add(new Cidades(
        5222302, '5 - Centro-Oeste', 'GO', 'Vila Propicio', '0055'));
    l.add(new Cidades(5108600, '5 - Centro-Oeste', 'MT', 'Vila Rica', '0055'));
    l.add(new Cidades(3205176, '3 - Sudeste', 'ES', 'Vila Valerio', '0055'));
    l.add(new Cidades(3205200, '3 - Sudeste', 'ES', 'Vila Velha', '0055'));
    l.add(new Cidades(1100304, '1 - Norte', 'RO', 'Vilhena', '0055'));
    l.add(new Cidades(3556701, '3 - Sudeste', 'SP', 'Vinhedo', '0055'));
    l.add(new Cidades(3556800, '3 - Sudeste', 'SP', 'Viradouro', '0055'));
    l.add(new Cidades(3171600, '3 - Sudeste', 'MG', 'Virgem Da Lapa', '0055'));
    l.add(new Cidades(3171709, '3 - Sudeste', 'MG', 'Virginia', '0055'));
    l.add(new Cidades(3171808, '3 - Sudeste', 'MG', 'Virginopolis', '0055'));
    l.add(new Cidades(3171907, '3 - Sudeste', 'MG', 'Virgolandia', '0055'));
    l.add(new Cidades(4128658, '4 - Sul', 'PR', 'Virmond', '0055'));
    l.add(new Cidades(
        3172004, '3 - Sudeste', 'MG', 'Visconde Do Rio Branco', '0055'));
    l.add(new Cidades(1508308, '1 - Norte', 'PA', 'Viseu', '0055'));
    l.add(new Cidades(4323507, '4 - Sul', 'RS', 'Vista Alegre', '0055'));
    l.add(new Cidades(
        3556909, '3 - Sudeste', 'SP', 'Vista Alegre Do Alto', '0055'));
    l.add(
        new Cidades(4323606, '4 - Sul', 'RS', 'Vista Alegre Do Prata', '0055'));
    l.add(new Cidades(4323705, '4 - Sul', 'RS', 'Vista Gaucha', '0055'));
    l.add(new Cidades(2505501, '2 - Nordeste', 'PB', 'Vista Serrana', '0055'));
    l.add(new Cidades(4219358, '4 - Sul', 'SC', 'Vitor Meireles', '0055'));
    l.add(new Cidades(3205309, '3 - Sudeste', 'ES', 'Vitoria', '0055'));
    l.add(new Cidades(3556958, '3 - Sudeste', 'SP', 'Vitoria Brasil', '0055'));
    l.add(new Cidades(
        2933307, '2 - Nordeste', 'BA', 'Vitoria Da Conquista', '0055'));
    l.add(new Cidades(4323754, '4 - Sul', 'RS', 'Vitoria Das Missoes', '0055'));
    l.add(new Cidades(
        2616407, '2 - Nordeste', 'PE', 'Vitoria De Santo Antao', '0055'));
    l.add(new Cidades(1600808, '1 - Norte', 'AP', 'Vitoria Do Jari', '0055'));
    l.add(new Cidades(
        2112902, '2 - Nordeste', 'MA', 'Vitoria Do Mearim', '0055'));
    l.add(new Cidades(1508357, '1 - Norte', 'PA', 'Vitoria Do Xingu', '0055'));
    l.add(new Cidades(4128708, '4 - Sul', 'PR', 'Vitorino', '0055'));
    l.add(
        new Cidades(2113009, '2 - Nordeste', 'MA', 'Vitorino Freire', '0055'));
    l.add(new Cidades(3172103, '3 - Sudeste', 'MG', 'Volta Grande', '0055'));
    l.add(new Cidades(3306305, '3 - Sudeste', 'RJ', 'Volta Redonda', '0055'));
    l.add(new Cidades(3557006, '3 - Sudeste', 'SP', 'Votorantim', '0055'));
    l.add(new Cidades(3557105, '3 - Sudeste', 'SP', 'Votuporanga', '0055'));
    l.add(new Cidades(2933406, '2 - Nordeste', 'BA', 'Wagner', '0055'));
    l.add(new Cidades(2211704, '2 - Nordeste', 'PI', 'Wall Ferraz', '0055'));
    l.add(new Cidades(1722081, '1 - Norte', 'TO', 'Wanderlandia', '0055'));
    l.add(new Cidades(2933455, '2 - Nordeste', 'BA', 'Wanderley', '0055'));
    l.add(new Cidades(3172202, '3 - Sudeste', 'MG', 'Wenceslau Braz', '0055'));
    l.add(new Cidades(4128500, '4 - Sul', 'PR', 'Wenceslau Braz', '0055'));
    l.add(new Cidades(
        2933505, '2 - Nordeste', 'BA', 'Wenceslau Guimaraes', '0055'));
    l.add(new Cidades(4323770, '4 - Sul', 'RS', 'Westfalia', '0055'));
    l.add(new Cidades(4219408, '4 - Sul', 'SC', 'Witmarsum', '0055'));
    l.add(new Cidades(1722107, '1 - Norte', 'TO', 'Xambioa', '0055'));
    l.add(new Cidades(4128807, '4 - Sul', 'PR', 'Xambre', '0055'));
    l.add(new Cidades(4323804, '4 - Sul', 'RS', 'Xangri-La', '0055'));
    l.add(new Cidades(4219507, '4 - Sul', 'SC', 'Xanxere', '0055'));
    l.add(new Cidades(1200708, '1 - Norte', 'AC', 'Xapuri', '0055'));
    l.add(new Cidades(4219606, '4 - Sul', 'SC', 'Xavantina', '0055'));
    l.add(new Cidades(4219705, '4 - Sul', 'SC', 'Xaxim', '0055'));
    l.add(new Cidades(2616506, '2 - Nordeste', 'PE', 'Xexeu', '0055'));
    l.add(new Cidades(1508407, '1 - Norte', 'PA', 'Xinguara', '0055'));
    l.add(new Cidades(2933604, '2 - Nordeste', 'BA', 'Xique-Xique', '0055'));
    l.add(new Cidades(2517407, '2 - Nordeste', 'PB', 'Zabele', '0055'));
    l.add(new Cidades(3557154, '3 - Sudeste', 'SP', 'Zacarias', '0055'));
    l.add(new Cidades(2114007, '2 - Nordeste', 'MA', 'Ze Doca', '0055'));
    l.add(new Cidades(4219853, '4 - Sul', 'SC', 'Zortea', '0055'));

    await sync();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Enviando cidades'),
        ),
        body: SingleChildScrollView(
            child: Container(
                child: PrimaryButton(
                    icon: Icon(
                      Icons.save,
                      color: Colors.white,
                      size: 40.0,
                    ),
                    onPressed: () {}))));
  }

  @override
  void initState() {
    super.initState();
    setCidades();
  }

/*
  String dropdown1Value = 'Venda';

  List<String> nomIte = ['Qual é o item?', 'Soja', 'Milho', 'Feijão', 'Sorgo'];
  String nomIteSel = 'Qual é o item?';

  List<String> tipOpo = [
    'Qual é a oportunidade?',
    'Venda',
    'Compra',
    'Troca',
    'Sugestão de pacote'
  ];
  String tipOpoSel = 'Qual é a oportunidade?';
  //final TextEditingController tipOpoCtr = new TextEditingController();

  //Tipo de entrega para troca
  List<String> tipEnt = [
    'Estado de entrega',
    'AC',
    'AL',
    'AP',
    'AM',
    'BA',
    'CE',
    'DF',
    'ES',
    'GO',
    'MA',
    'MT',
    'MS',
    'MG',
    'PA',
    'PB',
    'PR',
    'PE',
    'PI',
    'RJ',
    'RN',
    'RS',
    'RO',
    'RR',
    'SC',
    'SP',
    'SE',
    'TO'
  ];
  String tipEntSel = 'Estado de entrega';
  List<Cidades> lstCid = new List<Cidades>();

  //Locais
  List<String> tipLoc = [
    'Local de entrega',
    'Local de produção',
    'No fabricante'
  ];
  String tipLocSel = 'Local de entrega';
  //Tipo de entrega para compra
  bool pnlVen = false;
  bool pnlCpr = false;
  bool pnlTrc = false;
  bool pnlBtn = false;



  @override
  Widget build(BuildContext context) {
    print('val ' + tipOpoSel);
    if (tipOpoSel.toLowerCase().contains('venda')) {
      pnlCpr = false;
      pnlTrc = false;
      pnlVen = true;
      pnlBtn = true;
    } else if (tipOpoSel.toLowerCase().contains('compra')) {
      pnlVen = false;
      pnlTrc = false;
      pnlCpr = true;
      pnlBtn = true;
    } else if (tipOpoSel.toLowerCase().contains('troca')) {
      pnlVen = false;
      pnlCpr = false;
      pnlTrc = true;
      pnlBtn = true;
    } else {
      pnlVen = false;
      pnlCpr = false;
      pnlTrc = false;
      pnlBtn = false;
    }

    Color habBtn() {
      if (pnlBtn)return Colors.blue;
      elsereturn Colors.grey;
    }

    Future<Null> getLstCid(String nomEst) async {
      await Firestore.instance  .collection("E001CID")  .where('SIGUFS', isEqualTo: "MG")  .getDocuments()  .then((e) async {e.documents.forEach((h) async {});
      });
    }

    //A lista é criada aqui porque a atualização onChange do Dropdown atualiza o status
    List<Widget> submitWidgets() {
      if (pnlVen) {return [  new FlatButton(child: new Text('Sync'), onPressed: sync),  new TextFormField(    key: txt001,    autocorrect: false,    keyboardType: TextInputType.text,    style: Theme.of(context).textTheme.body2,    decoration: new InputDecoration(labelText: 'Produto'),    validator: (val) => val.isEmpty ? 'Informe o nome do item' : null,  ),  new TextFormField(    key: txt002,    keyboardType: TextInputType.number,    style: Theme.of(context).textTheme.body2,    decoration: new InputDecoration(labelText: 'Quantidade do item'),    validator: (val) => val.isEmpty        ? 'Informe a quantidade do item a ser vendido'        : null,  ),  new TextFormField(    key: txt003,    keyboardType: TextInputType.number,    style: Theme.of(context).textTheme.body2,    decoration:        new InputDecoration(labelText: 'Valor unitário desejado'),    validator: (val) =>        val.isEmpty ? 'Informe o valor de uma unidade' : null,  ),  new DropdownButton(      value: tipEntSel,      items: tipEnt.map((String val) {        return new DropdownMenuItem<String>(          value: val,          child: new Text(val),          //key: txt001,        );      }).toList(),      hint: Text("Estado de entrega"),      onChanged: (newVal) {        this.setState(() {          tipEntSel = newVal;          getLstCid(newVal);        });      }),  new TextFormField(    key: txt004,    style: Theme.of(context).textTheme.body2,    decoration: new InputDecoration(labelText: 'Cidade de entrega'),    validator: (val) =>        val.isEmpty ? 'Em que cidade o produto será entregue?' : null,  ),];
      }
      if (pnlTrc) {return [  new TextFormField(    key: txt001,    style: Theme.of(context).textTheme.body2,    decoration: new InputDecoration(labelText: 'Produto'),    validator: (val) => val.isEmpty ? 'Informe o produto' : null,  ),  new TextFormField(    key: txt002,    style: Theme.of(context).textTheme.body2,    decoration: new InputDecoration(labelText: 'Quantidade sacas'),    validator: (val) =>        val.isEmpty ? 'Informe a quantidade de sacas' : null,  ),  new TextFormField(    //key: txt003,    style: Theme.of(context).textTheme.body2,    decoration: new InputDecoration(labelText: 'Valor total'),    validator: (val) =>        val.isEmpty ? 'Informe o valor para esta quantidade' : null,  ),  new DropdownButton(      value: tipLocSel,      items: tipLoc.map((String val) {        return new DropdownMenuItem<String>(          value: val,          child: new Text(val),          //key: txt001,        );      }).toList(),      hint: Text("Local de entrega"),      onChanged: (newVal) {        this.setState(() {          tipLocSel = newVal;        });      }),];
      }
      if (pnlCpr) {return [  new TextFormField(    key: txt001,    style: Theme.of(context).textTheme.body2,    decoration: new InputDecoration(labelText: 'Produto'),    validator: (val) => val.isEmpty ? 'Informe o produto' : null,  ),  new TextFormField(    key: txt002,    style: Theme.of(context).textTheme.body2,    decoration: new InputDecoration(labelText: 'Quantidade sacas'),    validator: (val) =>        val.isEmpty ? 'Informe a quantidade de sacas' : null,  ),  new TextFormField(    key: txt003,    style: Theme.of(context).textTheme.body2,    decoration: new InputDecoration(labelText: 'Valor total'),    validator: (val) =>        val.isEmpty ? 'Informe o valor para esta quantidade' : null,  ),];
      }

      return [new Opacity(  opacity: 0.0,)
      ];
      }

      return new Scaffold(
      body: Padding(padding: new EdgeInsets.all(16.0),child: new Form(  key: formKey,  autovalidate: false,  child: new ListView(    children: [      new DropdownButton(          value: tipOpoSel,          items: tipOpo.map((String val) {            return new DropdownMenuItem<String>(              value: val,              child: new Text(val),              //key: txt001,            );          }).toList(),          hint: Text("De que é a oportunidade?"),          onChanged: (newVal) {            this.setState(() {              tipOpoSel = newVal;            });          }),      new Column(children: submitWidgets())    ],  ),),
      ),
      floatingActionButton: new FloatingActionButton(backgroundColor: habBtn(),child: new Icon(Icons.save),onPressed: () {  //print('ideUsu ' + RootPage().ideUsu + ' - ' + UserApp().baseAuthClass.ideUsu);  print('ideUsu ' + widget.ideUsu);  if (pnlBtn) {    if (formKey.currentState.validate()) {      var res001, res002, res003, res004, res005 = "";      if (txt001 != null && txt001.currentState != null)        res001 = txt001.currentState.value;      if (txt002 != null && txt002.currentState != null)        res002 = txt002.currentState.value;      if (txt003 != null && txt003.currentState != null)        res003 = txt003.currentState.value;      if (txt004 != null && txt004.currentState != null)        res004 = txt004.currentState.value;
      Firestore.instance          .runTransaction((Transaction transaction) async {        CollectionReference ref2 = Firestore.instance            .collection("E100OPO/CTV/" + widget.ideUsu);        await ref2.add({          "TIPPRC": "VENDA",          "NOMPRO": res001,          "QTDSAC": strToDou(res002),          "VLRSAC": strToDou(res003),          "ESTENT": tipLocSel,          "CIDENT": res004,          //STATUS: C-CADASTRADO, A-ANDAMENTO, F-FINALIZADO          "STAPRC": "C"        });      });
      Navigator.pop(context);    }  }},
      ),
      );
      }

      double strToDou(String x) {
      double ret = 0.0;
      if ((x != null) && (x.length > 0)) {
      ret = double.parse(x);
      }
      return ret;
      }
      */
}
